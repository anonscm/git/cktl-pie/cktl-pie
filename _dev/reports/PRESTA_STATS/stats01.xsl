<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE xsl:stylesheet
[
<!ENTITY  nbsp  "&#160;">
<!ENTITY  space  "&#x20;">
<!ENTITY  br  "&#x2028;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template name="stats" match="stats" >
	
	 <fo:table inline-progression-dimension="190mm" table-layout="fixed" space-before="0pt" border-width="0.5pt" border-style="solid">
	      <fo:table-column column-width="100mm" border="0.5pt solid"/>		  
	      <fo:table-column column-width="45mm" border="0.5pt solid"/>
	      <fo:table-column column-width="45mm" border="0.5pt solid"/>

	      <fo:table-header font-weight="bold">
	         <fo:table-row >
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center"><fo:block><xsl:value-of select="metadata/headerlib"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center"><fo:block>Quantit�</fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center" ><fo:block>Montant</fo:block></fo:table-cell>
			 </fo:table-row>
			</fo:table-header>
	      <fo:table-body font-size="8pt">
					<xsl:apply-templates select="stat" />
		  </fo:table-body>
	 </fo:table>
   </xsl:template>
   

   <xsl:template name="stat" match="stat" >
	         <fo:table-row border="0.5pt solid" keep-together="always">
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="left"><fo:block><xsl:value-of select="lib"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"><fo:block><xsl:value-of select="format-number(sum1, '# ###,##')"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"><fo:block><xsl:value-of select="format-number(sum2, '# ###,##')"/></fo:block></fo:table-cell>
			 </fo:table-row>	
   </xsl:template>   
	
<!-- TPL reportstats -->
<xsl:template name="reportstats" match="reportstats">

    <fo:static-content flow-name="xsl-region-end">
    </fo:static-content>

    <fo:static-content flow-name="xsl-region-after">
	<fo:block text-align="right" font-size="5pt" font-style="italic" writing-mode="bottom"><xsl:value-of select="dateimpression"/></fo:block>
	<fo:block text-align="right" font-size="5pt" font-style="italic" writing-mode="bottom"><xsl:value-of select="metadata/appalias"/></fo:block>
    </fo:static-content>

     <fo:static-content flow-name="xsl-region-before">
	</fo:static-content>

    <fo:static-content flow-name="xsl-region-start">
		<fo:table inline-progression-dimension="190mm" table-layout="fixed" space-before="0pt" padding="0pt">
		 <fo:table-column  column-width="100%"/>
         <fo:table-body>
            <fo:table-row>
                <fo:table-cell>
                    <fo:block><xsl:value-of select="metadata/header"/></fo:block>
                </fo:table-cell>
			</fo:table-row>		
         </fo:table-body>
		</fo:table>
    </fo:static-content>

	<fo:flow flow-name="xsl-region-body" font-family="Arial, Helvetica, sans-serif" font-size="9pt" >
		<fo:table inline-progression-dimension="190mm" table-layout="fixed" space-before="0pt" padding="0pt">
		 <fo:table-column  column-width="100%"/>
         <fo:table-body>		
			
<!-- le corps de la page -->
			<fo:table-row >
				<fo:table-cell>
				      <xsl:apply-templates select="stats" />
				</fo:table-cell>
			</fo:table-row>		
<!--/ le corps de la page -->			

			<fo:table-row  keep-with-next="always" >
				<fo:table-cell display-align="after">
            	 <fo:table inline-progression-dimension="190mm" table-layout="fixed" space-before="0pt" border-width="0.5pt" border-style="solid">
        	      <fo:table-column column-width="100mm" border="0.5pt solid"/>		  
        	      <fo:table-column column-width="45mm" border="0.5pt solid"/>
        	      <fo:table-column column-width="45mm" border="0.5pt solid"/>
            	      <fo:table-body font-weight="bold">
            	         <fo:table-row >
            				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="right" display-align="center"><fo:block>Totaux :</fo:block></fo:table-cell>
            				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="right" display-align="center"><fo:block><xsl:value-of select="format-number(metadata/totalq, '# ###,##')"/></fo:block></fo:table-cell>
            				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="right" display-align="center" ><fo:block><xsl:value-of select="format-number(metadata/totalm, '# ###,##')"/></fo:block></fo:table-cell>
            			 </fo:table-row>
            			</fo:table-body>
                	 </fo:table>
				</fo:table-cell>
			</fo:table-row>	

         </fo:table-body>
      </fo:table>			

	</fo:flow>

   </xsl:template>
	
</xsl:stylesheet>
