<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE xsl:stylesheet
[
<!ENTITY  nbsp "&#160;">
<!ENTITY  space "&#x20;">
<!ENTITY  br "&#x2028;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

	<!-- d�finition des patterns pour la fonction format-number -->
	<xsl:decimal-format decimal-separator="," grouping-separator=" "/>

<xsl:template name="write-date">
	<!--le format doit toujours �tre YYYY-MM-DD HH:MM:SS-->
    <xsl:param name="date" />
    <xsl:if test="$date!=''">    
    	<xsl:value-of select="concat(substring(normalize-space($date),9,2),'/',substring(normalize-space($date),6,2),'/',substring(normalize-space($date),1,4))"/>
    </xsl:if>
</xsl:template>

<!-- Affiche une chaine sur un nombre max de caract�re. Si la taille de la chaine d�passe maxlength, on r�duit la taille de la chaine et on met les caract�res ... � la fin -->
<xsl:template name="write-string-max">
    <xsl:param name="string" />
	<xsl:param name="maxlength" />
	<xsl:choose>
		<xsl:when test="string-length(normalize-space($string))>number($maxlength)">
			<xsl:value-of select="concat(substring(normalize-space($string),0,(number($maxlength)-3)),'...')"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$string"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!--*******************************************************************************-->
<xsl:include href="stats01.xsl"/> 
<!--******************************************************************************* -->
   
<!-- TPL root -->
   <xsl:template name="root" match="/">
      <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
         <fo:layout-master-set>
	            <fo:simple-page-master master-name="reportstats" margin-top="1cm" margin-bottom="1cm" page-width="21cm" page-height="29.7cm" margin-left="1cm" margin-right="1cm">
	               <fo:region-before extent="30mm" margin="0pt" />
	               <fo:region-after extent="20mm" />
	               <fo:region-start />
	               <fo:region-end />
	               <fo:region-body margin-bottom="2cm" margin-top="3cm" margin-left="0cm" margin-right="0cm" />
	            </fo:simple-page-master>
			</fo:layout-master-set>		
    
		 
         <fo:page-sequence master-reference="reportstats" >
			   <xsl:apply-templates select="reportstats"/>
         </fo:page-sequence>
		 
      </fo:root>
   </xsl:template>
 
</xsl:stylesheet>