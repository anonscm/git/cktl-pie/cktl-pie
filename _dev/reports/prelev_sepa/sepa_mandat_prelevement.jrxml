<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="sepa_mandat_prelevement" pageWidth="595" pageHeight="842" columnWidth="535" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="RUM_SEPA_SDD_MANDAT" class="java.lang.String">
		<defaultValueExpression><![CDATA["FR44ZZZ214214-E2014-MAN001004"]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT mandat.rum AS MANDAT_RUM,
          mandat.libelle AS MANDAT_LIBELLE,
          mandat.c_type_prelevement AS MANDAT_TYPE_PRELEVEMENT,
          mandat.d_mandat_signature AS MANDAT_DATE_SIGNATURE,
          param.creancier_ics AS CREANCIER_ICS,
          debiteur.pers_libelle || ' ' || initcap(debiteur.PERS_LC) as DEBITEUR_IDENTITE,
          debiteurAdresseDetails.adr_adresse1 AS DEBITEUR_ADRESSE1,
          debiteurAdresseDetails.adr_adresse2 AS DEBITEUR_ADRESSE2,
          debiteurAdresseDetails.adr_bp AS DEBITEUR_BP,
	  debiteurAdresseDetails.ville AS DEBITEUR_VILLE,
          debiteurAdresseDetails.code_postal AS DEBITEUR_CP,
          debiteurAdressePays.ll_pays AS DEBITEUR_PAYS,
          debiteurAdresseDetails.cp_etranger AS DEBITEUR_CP_ETRANGER,
          grhum.format_iban(debiteurRib.iban) AS DEBITEUR_IBAN,
          grhum.format_bic(debiteurRib.bic) AS DEBITEUR_BIC,
	  tiersDebiteur.pers_libelle || ' ' || initcap(tiersDebiteur.PERS_LC) as TIERS_DEBITEUR_IDENTITE,
	  creancier.pers_libelle || ' ' || initcap(creancier.PERS_LC) as CREANCIER_IDENTITE,
          creancierAdresseDetails.adr_adresse1 AS CREANCIER_ADRESSE1,
          creancierAdresseDetails.adr_adresse2 AS CREANCIER_ADRESSE2,
          creancierAdresseDetails.adr_bp AS CREANCIER_BP,
          creancierAdresseDetails.ville AS CREANCIER_VILLE,
          creancierAdresseDetails.code_postal AS CREANCIER_CP,
          creancierAdressePays.ll_pays AS CREANCIER_PAYS,
          creancierAdresseDetails.cp_etranger AS CREANCIER_CP_ETRANGER,
          jefy_admin.api_jasper_param.get_param_admin('MAIN_LOGO_URL',to_number(to_char(SYSDATE,'yyyy'))) as LOGO_URL
     FROM maracuja.SEPA_SDD_MANDAT mandat
     JOIN maracuja.SEPA_SDD_PARAM param ON mandat.id_sepa_sdd_param = mandat.id_sepa_sdd_param
     JOIN grhum.PERSONNE debiteur ON mandat.debiteur_pers_id = debiteur.pers_id
     JOIN grhum.ADRESSE debiteurAdresseDetails ON mandat.debiteur_adr_ordre = debiteurAdresseDetails.adr_ordre
     JOIN grhum.PAYS debiteurAdressePays ON debiteurAdresseDetails.c_pays = debiteurAdressePays.c_pays
     JOIN grhum.RIBFOUR_ULR debiteurRib ON mandat.debiteur_rib_ordre = debiteurRib.rib_ordre
     JOIN grhum.PERSONNE creancier ON mandat.creancier_pers_id = creancier.pers_id
     JOIN (SELECT pers_id, max(adr_ordre) adr_ordre
             FROM grhum.REPART_PERSONNE_ADRESSE
            WHERE rpa_principal = 'O' AND rpa_valide = 'O'
            GROUP BY pers_id) creancierAdresse ON creancier.pers_id = creancierAdresse.pers_id
     JOIN grhum.ADRESSE creancierAdresseDetails ON creancierAdresse.adr_ordre = creancierAdresseDetails.adr_ordre
     JOIN grhum.PAYS creancierAdressePays ON creancierAdresseDetails.c_pays = creancierAdressePays.c_pays
LEFT JOIN grhum.PERSONNE tiersDebiteur ON mandat.tiers_debiteur_pers_id = tiersDebiteur.pers_id
    WHERE mandat.rum in ($P!{RUM_SEPA_SDD_MANDAT})
order by mandat.rum]]>
	</queryString>
	<field name="MANDAT_RUM" class="java.lang.String"/>
	<field name="MANDAT_LIBELLE" class="java.lang.String"/>
	<field name="MANDAT_TYPE_PRELEVEMENT" class="java.lang.String"/>
	<field name="MANDAT_DATE_SIGNATURE" class="java.lang.String"/>
	<field name="CREANCIER_ICS" class="java.lang.String"/>
	<field name="DEBITEUR_IDENTITE" class="java.lang.String"/>
	<field name="DEBITEUR_ADRESSE1" class="java.lang.String"/>
	<field name="DEBITEUR_ADRESSE2" class="java.lang.String"/>
	<field name="DEBITEUR_BP" class="java.lang.String"/>
	<field name="DEBITEUR_VILLE" class="java.lang.String"/>
	<field name="DEBITEUR_CP" class="java.lang.String"/>
	<field name="DEBITEUR_PAYS" class="java.lang.String"/>
	<field name="DEBITEUR_CP_ETRANGER" class="java.lang.String"/>
	<field name="DEBITEUR_IBAN" class="java.lang.String"/>
	<field name="DEBITEUR_BIC" class="java.lang.String"/>
	<field name="TIERS_DEBITEUR_IDENTITE" class="java.lang.String"/>
	<field name="CREANCIER_IDENTITE" class="java.lang.String"/>
	<field name="CREANCIER_ADRESSE1" class="java.lang.String"/>
	<field name="CREANCIER_ADRESSE2" class="java.lang.String"/>
	<field name="CREANCIER_BP" class="java.lang.String"/>
	<field name="CREANCIER_VILLE" class="java.lang.String"/>
	<field name="CREANCIER_CP" class="java.lang.String"/>
	<field name="CREANCIER_PAYS" class="java.lang.String"/>
	<field name="CREANCIER_CP_ETRANGER" class="java.lang.String"/>
	<field name="LOGO_URL" class="java.lang.String"/>
	<group name="mandat" isStartNewPage="true" isReprintHeaderOnEachPage="true">
		<groupExpression><![CDATA[$F{MANDAT_RUM}]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="802" splitType="Stretch">
			<rectangle>
				<reportElement key="rectangle-15" x="0" y="372" width="535" height="30"/>
			</rectangle>
			<rectangle>
				<reportElement key="rectangle-7" x="281" y="217" width="254" height="18"/>
			</rectangle>
			<staticText>
				<reportElement key="staticText-4" x="281" y="217" width="254" height="18"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[Désignation du créancier]]></text>
			</staticText>
			<rectangle>
				<reportElement key="rectangle-17" x="0" y="747" width="535" height="32"/>
			</rectangle>
			<elementGroup>
				<rectangle>
					<reportElement key="rectangle-16" x="0" y="626" width="535" height="44">
						<printWhenExpression><![CDATA[Boolean.valueOf($F{TIERS_DEBITEUR_IDENTITE} != null && !"".equals( $F{TIERS_DEBITEUR_IDENTITE}.trim() ))]]></printWhenExpression>
					</reportElement>
				</rectangle>
			</elementGroup>
			<rectangle>
				<reportElement key="rectangle-14" x="0" y="480" width="535" height="51"/>
			</rectangle>
			<rectangle>
				<reportElement key="rectangle-4" x="0" y="235" width="254" height="123"/>
			</rectangle>
			<rectangle>
				<reportElement key="rectangle-1" mode="Opaque" x="0" y="18" width="535" height="57"/>
			</rectangle>
			<rectangle>
				<reportElement key="rectangle-2" x="0" y="81" width="535" height="21"/>
			</rectangle>
			<staticText>
				<reportElement key="staticText-1" x="5" y="81" width="104" height="21"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true" isItalic="false" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Type de contrat :]]></text>
			</staticText>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-1" x="109" y="81" width="404" height="21"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{MANDAT_LIBELLE}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-2" x="2" y="115" width="377" height="81"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["En signant ce formulaire de mandat, vous autorisez (A) " + $F{CREANCIER_IDENTITE}
+ " à envoyer des instructions à votre banque pour débiter votre compte, "
+ "et (B) votre banque à débiter votre compte conformément aux instructions de "
+ $F{CREANCIER_IDENTITE} + ". Vous bénéficiez du droit d'être remboursé par votre banque "
+ "selon les conditions décrites dans la convention que vous avez passée avec elle. "
+ "Une demande de remboursement doit être présentée : "
+ "dans les 8 semaines suivant la date de débit de votre compte pour un prélèvement autorisé."]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-2" x="379" y="114" width="145" height="23"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[Identifiant créancier SEPA]]></text>
			</staticText>
			<rectangle>
				<reportElement key="rectangle-3" x="379" y="145" width="146" height="44"/>
			</rectangle>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-3" x="379" y="145" width="145" height="44"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{CREANCIER_ICS}]]></textFieldExpression>
			</textField>
			<rectangle>
				<reportElement key="rectangle-5" x="281" y="235" width="254" height="123"/>
			</rectangle>
			<rectangle>
				<reportElement key="rectangle-6" x="0" y="217" width="254" height="18"/>
			</rectangle>
			<staticText>
				<reportElement key="staticText-3" x="0" y="220" width="254" height="12"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[Désignation du titulaire du compte à débiter]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-5" x="89" y="54" width="173" height="17"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Référence unique du mandat :]]></text>
			</staticText>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-4" x="264" y="54" width="270" height="17"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{MANDAT_RUM}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-6" x="0" y="32" width="535" height="18"/>
				<textElement textAlignment="Center">
					<font size="14" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[MANDAT DE PRELEVEMENT SEPA]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-7" x="0" y="372" width="535" height="30"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[Désignation du compte à débiter]]></text>
			</staticText>
			<rectangle>
				<reportElement key="rectangle-10" x="0" y="402" width="535" height="56"/>
			</rectangle>
			<rectangle>
				<reportElement key="rectangle-8" x="292" y="489" width="16" height="16"/>
			</rectangle>
			<line>
				<reportElement key="line-1" x="292" y="489" width="16" height="16">
					<printWhenExpression><![CDATA[Boolean.valueOf("R".equals( $F{MANDAT_TYPE_PRELEVEMENT} ))]]></printWhenExpression>
				</reportElement>
				<graphicElement>
					<pen lineWidth="1.0"/>
				</graphicElement>
			</line>
			<line direction="BottomUp">
				<reportElement key="line-2" x="292" y="489" width="16" height="16">
					<printWhenExpression><![CDATA[Boolean.valueOf("R".equals( $F{MANDAT_TYPE_PRELEVEMENT} ))]]></printWhenExpression>
				</reportElement>
			</line>
			<staticText>
				<reportElement key="staticText-8" x="9" y="489" width="113" height="18"/>
				<textElement verticalAlignment="Middle">
					<font isBold="true" isItalic="false" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Type de paiement :]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-9" x="135" y="488" width="153" height="16"/>
				<textElement textAlignment="Left" verticalAlignment="Middle"/>
				<text><![CDATA[Paiement récurrent / répétitif]]></text>
			</staticText>
			<rectangle>
				<reportElement key="rectangle-12" x="292" y="511" width="16" height="16"/>
			</rectangle>
			<elementGroup>
				<line>
					<reportElement key="line-3" x="293" y="512" width="16" height="16">
						<printWhenExpression><![CDATA[Boolean.valueOf("P".equals( $F{MANDAT_TYPE_PRELEVEMENT} ))]]></printWhenExpression>
					</reportElement>
				</line>
				<line direction="BottomUp">
					<reportElement key="line-4" x="292" y="511" width="16" height="16">
						<printWhenExpression><![CDATA[Boolean.valueOf("P".equals( $F{MANDAT_TYPE_PRELEVEMENT} ))]]></printWhenExpression>
					</reportElement>
				</line>
			</elementGroup>
			<staticText>
				<reportElement key="staticText-10" x="134" y="510" width="154" height="16"/>
				<textElement textAlignment="Left" verticalAlignment="Middle"/>
				<text><![CDATA[Paiement ponctuel]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-11" x="3" y="552" width="88" height="22"/>
				<textElement textAlignment="Left" verticalAlignment="Middle"/>
				<text><![CDATA[Signé à :]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-12" x="2" y="579" width="89" height="22"/>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Le (JJ/MM/AAAA) :]]></text>
			</staticText>
			<textField isBlankWhenNull="false">
				<reportElement key="textField" x="10" y="242" width="237" height="22"/>
				<textElement/>
				<textFieldExpression><![CDATA[$F{DEBITEUR_IDENTITE}]]></textFieldExpression>
			</textField>
			<rectangle>
				<reportElement key="rectangle-13" x="316" y="552" width="218" height="56"/>
			</rectangle>
			<staticText>
				<reportElement key="staticText-23" x="238" y="552" width="66" height="35"/>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Signature :]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-24" x="13" y="406" width="240" height="18"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isUnderline="true"/>
				</textElement>
				<text><![CDATA[Identification internationale (IBAN)]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-25" x="261" y="406" width="234" height="18"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isUnderline="true"/>
				</textElement>
				<text><![CDATA[Identification internationale de la banque]]></text>
			</staticText>
			<elementGroup>
				<staticText>
					<reportElement key="staticText-26" x="0" y="625" width="535" height="44">
						<printWhenExpression><![CDATA[Boolean.valueOf($F{TIERS_DEBITEUR_IDENTITE} != null && !"".equals( $F{TIERS_DEBITEUR_IDENTITE}.trim() ))]]></printWhenExpression>
					</reportElement>
					<textElement/>
					<text><![CDATA[Désignation du tiers débiteur pour le compte duquel le paiement est effectué (si différent du débiteur lui-même et le cas échéant ) :]]></text>
				</staticText>
				<textField isBlankWhenNull="false">
					<reportElement key="textField" x="136" y="638" width="298" height="27">
						<printWhenExpression><![CDATA[Boolean.valueOf($F{TIERS_DEBITEUR_IDENTITE} != null && !"".equals( $F{TIERS_DEBITEUR_IDENTITE}.trim() ))]]></printWhenExpression>
					</reportElement>
					<textElement verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[$F{TIERS_DEBITEUR_IDENTITE}]]></textFieldExpression>
				</textField>
			</elementGroup>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-12" x="0" y="695" width="535" height="38"/>
				<textElement textAlignment="Justified">
					<font size="8" isItalic="true" pdfFontName="Helvetica-Oblique"/>
				</textElement>
				<textFieldExpression><![CDATA["En signant ce mandat j'autorise ma banque à effectuer sur mon compte bancaire, "
+ "si sa situation le permet, les prélèvements ordonnés par "
+ $F{CREANCIER_IDENTITE} + ". En cas de litige sur un prélèvement, je pourrai en faire "
+ "suspendre l'exécution par simple demande à ma banque. Je réglerai le différend "
+ "directement avec " + $F{CREANCIER_IDENTITE} + "."]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-28" x="4" y="751" width="528" height="25"/>
				<textElement textAlignment="Justified" verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<text><![CDATA[Les informations contenues dans le présent mandat, qui doit être complété, sont destinées à n’être utilisées par le créancier que pour la gestion de sa relation avec son client. Elles pourront donner lieu à l'exercice, par ce dernier, de ses droits d’opposition, d'accès et de rectification tels que prévus aux articles 38 et suivants de la loi n°78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés. ]]></text>
			</staticText>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-13" x="10" y="265" width="237" height="88"/>
				<textElement/>
				<textFieldExpression><![CDATA[($F{DEBITEUR_ADRESSE1} != null ? "\n"+ $F{DEBITEUR_ADRESSE1} : "")
+ ($F{DEBITEUR_ADRESSE2} != null ? "\n"+ $F{DEBITEUR_ADRESSE2} : "")
+ ($F{DEBITEUR_BP} != null ? "\n" + $F{DEBITEUR_BP} : "")
+ ($F{DEBITEUR_CP} != null ? "\n" + $F{DEBITEUR_CP} : "")
+ ($F{DEBITEUR_VILLE} != null ? "\n" + $F{DEBITEUR_VILLE} : "")
+ ($F{DEBITEUR_CP_ETRANGER} != null ? "\n" + $F{DEBITEUR_CP_ETRANGER} : "")
+ ($F{DEBITEUR_PAYS} != null ? "\n" + $F{DEBITEUR_PAYS} : "")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-14" x="290" y="266" width="237" height="88"/>
				<textElement/>
				<textFieldExpression><![CDATA[($F{CREANCIER_ADRESSE1} != null ? "\n"+ $F{CREANCIER_ADRESSE1} : "")
+ ($F{CREANCIER_ADRESSE2} != null ? "\n"+ $F{CREANCIER_ADRESSE2} : "")
+ ($F{CREANCIER_BP} != null ? "\n" + $F{CREANCIER_BP} : "")
+ ($F{CREANCIER_CP} != null ? "\n" + $F{CREANCIER_CP} : "")
+ ($F{CREANCIER_VILLE} != null ? "\n" + $F{CREANCIER_VILLE} : "")
+ ($F{CREANCIER_CP_ETRANGER} != null ? "\n" + $F{CREANCIER_CP_ETRANGER} : "")
+ ($F{CREANCIER_PAYS} != null ? "\n" + $F{CREANCIER_PAYS} : "")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-15" x="290" y="242" width="237" height="22"/>
				<textElement/>
				<textFieldExpression><![CDATA[$F{CREANCIER_IDENTITE}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-29" x="1" y="680" width="56" height="12"/>
				<textElement>
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Rappel :]]></text>
			</staticText>
			<textField isBlankWhenNull="false">
				<reportElement key="textField" x="14" y="428" width="223" height="24"/>
				<textElement/>
				<textFieldExpression><![CDATA[$F{DEBITEUR_IBAN}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField" x="279" y="428" width="215" height="23"/>
				<textElement/>
				<textFieldExpression><![CDATA[$F{DEBITEUR_BIC}]]></textFieldExpression>
			</textField>
			<image scaleImage="RetainShape">
				<reportElement key="image-1" x="10" y="25" width="73" height="46"/>
				<imageExpression><![CDATA[new URL($F{LOGO_URL})]]></imageExpression>
			</image>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
