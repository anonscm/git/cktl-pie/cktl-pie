<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="sepa_prelevements_en_attente" pageWidth="595" pageHeight="842" columnWidth="535" leftMargin="22" rightMargin="22" topMargin="20" bottomMargin="20">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="DATE_MAX" class="java.lang.String"/>
	<parameter name="DATE_MIN" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["01/01/1970"]]></defaultValueExpression>
	</parameter>
	<parameter name="UNIV" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["UNIVERSITE DE LA ROCHELLE"]]></defaultValueExpression>
	</parameter>
	<parameter name="AGENCE_COMPTABLE" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["AGENCE COMPTABLE DE L'UNIVERSITE DE LA ROCHELLE"]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT mandat.rum AS MANDAT_RUM,
  mandat.libelle  AS MANDAT_LIBELLE,
  trim(initcap(DECODE(debiteur.pers_type, 'STR','', debiteur.pers_type))
  || ' '
  || initcap(debiteur.pers_lc)
  || ' '
  || initcap(debiteur.pers_libelle)) AS DEBITEUR_IDENTITE,
  grhum.format_iban(debiteurRib.iban) AS DEBITEUR_IBAN,
  grhum.format_bic(debiteurRib.bic) AS DEBITEUR_BIC,
  echeance.id_sepa_sdd_echeance AS ECHEANCE_ID,
  to_char(to_date(echeance.d_prevue, 'YYYY-MM-DD'),'DD/MM/YYYY') as ECHEANCE_DATE_PREVUE,
  echeance.montant AS  ECHEANCE_MONTANT,
  viewEcheancier.montantEcheances AS ECHEANCIER_MONTANT,
  jefy_admin.api_jasper_param.get_param_admin('MAIN_LOGO_URL',to_number(to_char(SYSDATE,'yyyy'))) as LOGO_URL
FROM maracuja.sepa_sdd_echeance echeance
JOIN maracuja.sepa_sdd_echeancier echeancier ON echeancier.id_sepa_sdd_echeancier = echeance.id_sepa_sdd_echeancier
JOIN
  (SELECT innerEch.id_sepa_sdd_echeancier, SUM(montant) AS montantEcheances
     FROM maracuja.sepa_sdd_echeance innerEch
    GROUP BY innerEch.id_sepa_sdd_echeancier
  ) viewEcheancier ON viewEcheancier.id_sepa_sdd_echeancier = echeance.id_sepa_sdd_echeancier
JOIN maracuja.sepa_sdd_mandat mandat ON mandat.id_sepa_sdd_mandat = echeancier.id_sepa_sdd_mandat
JOIN jefy_admin.type_etat etat ON etat.tyet_id = mandat.tyet_id
JOIN grhum.RIBFOUR_ULR debiteurRib ON mandat.debiteur_rib_ordre = debiteurRib.rib_ordre
JOIN grhum.personne debiteur ON debiteur.pers_id = mandat.debiteur_pers_id
LEFT JOIN grhum.PERSONNE tiersDebiteur ON mandat.tiers_debiteur_pers_id = tiersDebiteur.pers_id
WHERE echeance.etat = 'ATTENTE'
  AND to_date(echeance.d_prevue, 'YYYY-MM-DD') BETWEEN to_date($P{DATE_MIN}, 'DD/MM/YYYY') AND to_date($P{DATE_MAX}, 'DD/MM/YYYY')
  AND etat.tyet_libelle = 'VALIDE'
  /* and mandat.id_sepa_sdd_param = <parametreJasper> */
ORDER BY echeance.d_prevue ASC, mandat.numero ASC]]>
	</queryString>
	<field name="MANDAT_RUM" class="java.lang.String"/>
	<field name="MANDAT_LIBELLE" class="java.lang.String"/>
	<field name="DEBITEUR_IDENTITE" class="java.lang.String"/>
	<field name="DEBITEUR_IBAN" class="java.lang.String"/>
	<field name="DEBITEUR_BIC" class="java.lang.String"/>
	<field name="ECHEANCE_ID" class="java.math.BigDecimal"/>
	<field name="ECHEANCE_DATE_PREVUE" class="java.lang.String"/>
	<field name="ECHEANCE_MONTANT" class="java.math.BigDecimal"/>
	<field name="ECHEANCIER_MONTANT" class="java.math.BigDecimal"/>
	<field name="LOGO_URL" class="java.lang.String"/>
	<variable name="TOTAL" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{ECHEANCE_MONTANT}]]></variableExpression>
	</variable>
	<variable name="NB_PRELEV" class="java.lang.Integer" calculation="Count">
		<variableExpression><![CDATA[$F{ECHEANCE_ID}]]></variableExpression>
	</variable>
	<group name="prelevordre">
		<groupExpression><![CDATA[$F{ECHEANCE_ID}]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band height="47" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-1" mode="Opaque" x="171" y="4" width="311" height="18" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="14" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[LISTE DES PRELEVEMENTS EN ATTENTE]]></text>
			</staticText>
			<line direction="BottomUp">
				<reportElement key="line-2" positionType="FixRelativeToBottom" mode="Opaque" x="5" y="45" width="541" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
				<graphicElement fill="Solid">
					<pen lineWidth="2.0" lineStyle="Solid"/>
				</graphicElement>
			</line>
			<textField isStretchWithOverflow="true" isBlankWhenNull="false">
				<reportElement key="textField-16" positionType="Float" stretchType="RelativeToTallestObject" mode="Opaque" x="172" y="27" width="311" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="true" isItalic="true" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-BoldOblique" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[(($P{DATE_MIN} == null || "01/01/1970".equals($P{DATE_MIN})) ?
"A prélever avant le " + $P{DATE_MAX} :
"A prélever entre le " + $P{DATE_MIN} +
" et le " + $P{DATE_MAX}
)]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-21" x="3" y="4" width="149" height="13"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{UNIV}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-22" x="3" y="20" width="148" height="20"/>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{AGENCE_COMPTABLE}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="30" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-2" mode="Opaque" x="482" y="4" width="67" height="25" forecolor="#000000" backcolor="#FFFFFF"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Bottom" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="true" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-BoldOblique" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Prélèvement / Echeancier
]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-7" mode="Opaque" x="3" y="4" width="166" height="25" forecolor="#000000" backcolor="#FFFFFF"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Bottom" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="true" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-BoldOblique" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Client / RIB]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-10" mode="Opaque" x="417" y="4" width="64" height="25" forecolor="#000000" backcolor="#FFFFFF"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Bottom" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="true" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-BoldOblique" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Date échéance]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-11" mode="Opaque" x="173" y="4" width="96" height="25" forecolor="#000000" backcolor="#FFFFFF"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Bottom" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="true" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-BoldOblique" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Référence mandat]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-12" mode="Opaque" x="271" y="4" width="144" height="25" forecolor="#000000" backcolor="#FFFFFF"/>
				<box topPadding="2" leftPadding="2" bottomPadding="2" rightPadding="2">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Bottom" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="true" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-BoldOblique" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Objet]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="35" splitType="Stretch">
			<rectangle>
				<reportElement key="rectangle-1" x="417" y="0" width="64" height="31" backcolor="#FF9999">
					<printWhenExpression><![CDATA[new Boolean(new java.text.SimpleDateFormat("dd/MM/yyyy").parse($F{ECHEANCE_DATE_PREVUE}).before(new Date()))]]></printWhenExpression>
				</reportElement>
				<graphicElement>
					<pen lineWidth="0.0" lineStyle="Solid"/>
				</graphicElement>
			</rectangle>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="3" y="1" width="166" height="10" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{DEBITEUR_IDENTITE}]]></textFieldExpression>
			</textField>
			<textField pattern="###0.00;-###0.00" isBlankWhenNull="false">
				<reportElement key="textField" mode="Opaque" x="483" y="1" width="66" height="14" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ECHEANCE_MONTANT}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement key="line-8" positionType="FixRelativeToBottom" mode="Opaque" x="2" y="34" width="546" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
				<graphicElement fill="Solid">
					<pen lineWidth="0.25" lineStyle="Solid"/>
				</graphicElement>
			</line>
			<textField isStretchWithOverflow="true" pattern="#,##0;#,##0-" isBlankWhenNull="true">
				<reportElement key="textField-8" mode="Opaque" x="173" y="1" width="96" height="32" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{MANDAT_RUM}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement key="textField-10" mode="Opaque" x="271" y="1" width="144" height="32" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{MANDAT_LIBELLE}]]></textFieldExpression>
			</textField>
			<textField pattern="###0.00;-###0.00" isBlankWhenNull="false">
				<reportElement key="textField-11" positionType="Float" mode="Opaque" x="483" y="17" width="66" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ECHEANCIER_MONTANT}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy" isBlankWhenNull="true">
				<reportElement key="textField-12" mode="Transparent" x="417" y="1" width="64" height="31" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ECHEANCE_DATE_PREVUE}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-13" x="2" y="12" width="28" height="10"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[IBAN :]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-14" x="2" y="23" width="28" height="10"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[BIC :]]></text>
			</staticText>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-19" x="32" y="12" width="137" height="10"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{DEBITEUR_IBAN}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-20" x="32" y="23" width="137" height="10"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{DEBITEUR_BIC}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="16" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-13" mode="Transparent" x="392" y="3" width="100" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER} + " / "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="" isBlankWhenNull="false">
				<reportElement key="textField-14" mode="Transparent" x="492" y="3" width="54" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["" + $V{PAGE_NUMBER} + ""]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="false">
				<reportElement key="textField-15" positionType="FixRelativeToBottom" mode="Transparent" x="5" y="3" width="111" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["Edition du " + new java.text.SimpleDateFormat("dd/MM/yyyy").format(new Date())]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band height="27" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-3" mode="Opaque" x="253" y="7" width="160" height="17" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="12" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Total :]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-4" mode="Opaque" x="8" y="7" width="122" height="17" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="12" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Nb prélèvements :]]></text>
			</staticText>
			<line direction="BottomUp">
				<reportElement key="line-1" mode="Opaque" x="80" y="2" width="467" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
				<graphicElement fill="Solid">
					<pen lineWidth="2.0" lineStyle="Solid"/>
				</graphicElement>
			</line>
			<textField evaluationTime="Report" pattern="#,##0;-#,##0" isBlankWhenNull="false">
				<reportElement key="textField-17" mode="Opaque" x="131" y="7" width="122" height="17" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="12" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{NB_PRELEV}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="#,##0.00" isBlankWhenNull="false">
				<reportElement key="textField-18" mode="Opaque" x="423" y="7" width="122" height="17" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="2" rightPadding="2">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="12" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{TOTAL}]]></textFieldExpression>
			</textField>
		</band>
	</summary>
</jasperReport>
