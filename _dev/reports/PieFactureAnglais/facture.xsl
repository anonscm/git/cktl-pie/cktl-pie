<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE xsl:stylesheet
[
<!ENTITY  nbsp "&#160;">
<!ENTITY  space "&#x20;">
<!ENTITY  br "&#x2028;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:decimal-format decimal-separator="." grouping-separator=","></xsl:decimal-format>
    <!-- Le fichier qui contient le template de creation de logo -->
    <xsl:include href="logouniv.xsl"></xsl:include>
    <xsl:template name="write-date">
        <!--le format doit toujours �tre YYYY-MM-DD HH:MM:SS-->
        <xsl:param name="date"></xsl:param>
        <xsl:if test="$date!=''">
            <xsl:value-of select="concat(substring(normalize-space($date),9,2),'/',substring(normalize-space($date),6,2),'/',substring(normalize-space($date),1,4))"></xsl:value-of>
        </xsl:if>
    </xsl:template>
    <!-- Affiche une chaine sur un nombre max de caract�re. Si la taille de la chaine d�passe maxlength, on r�duit la taille de la chaine et on met les caract�res ... � la fin -->
    <xsl:template name="write-string-max">
        <xsl:param name="string"></xsl:param>
        <xsl:param name="maxlength"></xsl:param>
        <xsl:choose>
            <xsl:when test="string-length(normalize-space($string))&gt;number($maxlength)">
                <xsl:value-of
                    select="concat(substring(normalize-space($string),0,(number($maxlength)-3)),'...')"
                />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$string"></xsl:value-of>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <!-- TPL lieu , date -->
    <xsl:template name="lieudate">
        <xsl:param name="lieuimpression"></xsl:param>
        <xsl:param name="dateimpression"></xsl:param>
        <fo:block space-before="1mm">
            Date:&nbsp;<xsl:call-template name="write-date">
                <xsl:with-param name="date" select="$dateimpression"></xsl:with-param>
            </xsl:call-template>
        </fo:block>
    </xsl:template>
    <!--*******************************************************************************-->
    <xsl:include href="facture01.xsl"></xsl:include>
    <!--******************************************************************************* -->
    <!-- TPL Adresse-->
    <xsl:template name="adresse" match="adresse">
        <fo:block linefeed-treatment="preserve">
            <xsl:value-of select="adradresse1"></xsl:value-of>
        </fo:block>
        <xsl:if test="adradresse2!=''">
            <fo:block linefeed-treatment="preserve">
                <xsl:value-of select="adradresse2"></xsl:value-of>
            </fo:block>
        </xsl:if>
        <xsl:if test="adrbp!=''">
            <fo:block linefeed-treatment="preserve">
                BP <xsl:value-of select="adrbp"></xsl:value-of>
            </fo:block>
        </xsl:if>
        <fo:block>
            <xsl:value-of select="adrcp"></xsl:value-of>&nbsp;<xsl:value-of select="adrville"></xsl:value-of>
        </fo:block>
        <fo:block>
            <xsl:value-of select="lcpays"></xsl:value-of>
        </fo:block>
        <xsl:apply-templates></xsl:apply-templates>
    </xsl:template>
    <!-- un fournisseur -->
    <xsl:template name="fournisseur" match="fournisseur">
        <fo:table inline-progression-dimension="63mm" table-layout="fixed" space-before="0pt" border-width="0.5pt" border-style="none">
            <fo:table-column width="100%"></fo:table-column>
            <fo:table-body>
                <fo:table-row height="28mm">
                    <fo:table-cell padding="2pt" display-align="before" font-size="10pt" font-weight="normal">
                        <fo:block font-size="10pt" font-weight="bold">
                            <xsl:value-of select="nom"></xsl:value-of>
                        </fo:block>
                        <xsl:apply-templates select="adresse"></xsl:apply-templates>
                    </fo:table-cell>
                </fo:table-row>
                <fo:table-row height="8mm">
                    <fo:table-cell padding="2pt" font-size="8pt" display-align="after">
                        <fo:block>
                            <xsl:if test="string-length(normalize-space(tel))&gt;0"> Ph. :&nbsp;<xsl:value-of select="tel"></xsl:value-of>&nbsp; </xsl:if>
                            <xsl:if test="string-length(normalize-space(fax))&gt;0"> Fax :&nbsp;<xsl:value-of select="fax"></xsl:value-of>
                            </xsl:if>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
    </xsl:template>
    <!-- -TPL client -->
    <xsl:template name="client" match="client">
        <fo:table table-layout="fixed" space-before="0pt" border-width="0pt 0pt 0.2pt 0.2pt" border-style="solid">
            <fo:table-column width="100%"></fo:table-column>
            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell padding="4pt" font-size="10pt" font-weight="bold" display-align="before">
                        <xsl:choose>
                            <xsl:when test="normalize-space(nommoral)=''">
                                <fo:block>Unknown customer</fo:block>
                            </xsl:when>
                            <xsl:when test="normalize-space(nommoral)!=''">
                                <fo:block>
                                    <xsl:value-of select="nommoral"></xsl:value-of>
                                </fo:block>
                            </xsl:when>
                        </xsl:choose>
                        <xsl:if test="normalize-space(nomindividu)!=''">
                            <fo:block>
                                <xsl:value-of select="nomindividu"></xsl:value-of>
                            </fo:block>
                        </xsl:if>
                    </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell padding="4pt" font-size="10pt" font-weight="bold" display-align="before">
                        <xsl:apply-templates select="adresse"></xsl:apply-templates>
                    </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell padding="4pt" font-size="8pt" display-align="after">
                        <fo:block space-before="4pt">
                            <xsl:if test="string-length(normalize-space(tel))&gt;0"> Ph. :&nbsp;<xsl:value-of select="tel"></xsl:value-of>&nbsp; </xsl:if>
                            <xsl:if test="string-length(normalize-space(fax))&gt;0"> Fax :&nbsp;<xsl:value-of select="fax"></xsl:value-of>
                            </xsl:if>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
    </xsl:template>
    <!-- -TPL Observations -->
    <xsl:template name="observations" match="observations">
        <fo:table inline-progression-dimension="63mm" table-layout="fixed" space-before="0pt" border-width="0.5pt" border-style="solid">
            <fo:table-column width="100%"></fo:table-column>
            <fo:table-body>
                <fo:table-row height="5mm">
                    <fo:table-cell font-size="10pt" font-weight="bold" text-align="center" border-bottom="0.5pt solid" padding="2pt">
                        <fo:block>Observations</fo:block>
                    </fo:table-cell>
                </fo:table-row>
                <fo:table-row height="35mm" padding="2pt" display-align="before">
                    <fo:table-cell padding="2pt" font-size="8pt">
                        <fo:block linefeed-treatment="preserve">
                            <xsl:value-of select="."></xsl:value-of>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
    </xsl:template>
    <!-- -TPL Totaux-->
    <xsl:template name="totaux" match="totaux">
        <fo:table border="0.5pt solid">
            <fo:table-column></fo:table-column>
            <fo:table-column></fo:table-column>
            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell padding="2pt" text-align="right" border-right="0.5pt solid" border-bottom="0.5pt solid">
                        <fo:block>Amount</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt" text-align="right" border-bottom="0.5pt solid">
                        <fo:block>
                            <xsl:value-of select="format-number(montanttotalht,'#,##0.00')"></xsl:value-of>&nbsp;<xsl:value-of select="../libmonnaie"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell padding="2pt" text-align="right" border-right="0.5pt solid" border-bottom="0.5pt solid">
                        <fo:block>Total VAT value</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt" text-align="right" border-bottom="0.5pt solid">
                        <fo:block>
                            <xsl:value-of select="format-number(montanttotaltva,'#,##0.00')"></xsl:value-of>&nbsp;<xsl:value-of select="../libmonnaie"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell padding="2pt" text-align="right" border-right="0.5pt solid" border-bottom="0.5pt solid">
                        <fo:block>TOTAL Amount Due</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt" text-align="right" font-weight="bold" border-bottom="0.5pt none">
                        <fo:block>
                            <xsl:value-of select="format-number(montanttotalttc,'#,##0.00')"></xsl:value-of>&nbsp;<xsl:value-of select="../libmonnaie"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
    </xsl:template>
    <!-- TPL etablissement -->
    <xsl:template name="etablissement" match="etablissement">
        <fo:table>
            <fo:table-column></fo:table-column>
            <fo:table-column column-width="50mm"></fo:table-column>
            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell padding="3pt">
                        <!--  logo -->
                        <xsl:call-template name="bs-logo"></xsl:call-template>
                        <!--  /logo -->
                    </fo:table-cell>
                    <fo:table-cell padding="3pt" font-size="8pt">
                        <!-- coordonnees etablissement-->
                        <!--<fo:block><xsl:value-of select="nom"/></fo:block>-->
                        <fo:block linefeed-treatment="preserve">
                            <xsl:value-of select="adresseuniv"></xsl:value-of>
                        </fo:block>
                        <fo:block space-before="2mm" font-size="7pt">Trade Register Number : </fo:block>
                        <fo:block font-weight="bold" font-size="7pt">
                            <xsl:value-of select="numerosiret"></xsl:value-of>
                        </fo:block>
                        <xsl:if test="string-length(normalize-space(numeroape)) &gt; 0">
                          <fo:block space-before="1mm" font-size="7pt">APE Code : </fo:block>
                          <fo:block font-weight="bold" font-size="7pt">
                            <xsl:value-of select="numeroape"></xsl:value-of>
                          </fo:block>
                        </xsl:if>
                        <fo:block space-before="1mm" font-size="7pt">VAT Number : </fo:block>
                        <fo:block font-weight="bold" font-size="7pt">
                            <xsl:value-of select="tvaintracom"></xsl:value-of>
                        </fo:block>
                        <!-- /coordonnees etablissement-->
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
    </xsl:template>
    <!-- TPL root -->
    <xsl:template name="tpl-root" match="/">
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="reportfacturepapier" margin-top="1cm" margin-bottom="1cm" page-width="21cm" page-height="29.7cm" margin-left="1cm" margin-right="1cm">
                    <fo:region-before extent="80mm" margin="0pt"></fo:region-before>
                    <fo:region-after extent="60mm"></fo:region-after>
                    <fo:region-start></fo:region-start>
                    <fo:region-end></fo:region-end>
                    <fo:region-body margin-bottom="40mm" margin-top="80mm" margin-left="0cm" margin-right="0cm"></fo:region-body>
                </fo:simple-page-master>
            </fo:layout-master-set>
            <fo:page-sequence master-reference="reportfacturepapier">
                <xsl:apply-templates select="reportfacturepapier"></xsl:apply-templates>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
    <xsl:template name="table-generique-1row_1col">
        <fo:table inline-progression-dimension="63mm" table-layout="fixed" space-before="0pt" border-width="0.5pt" border-style="solid">
            <fo:table-column></fo:table-column>
            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell></fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
    </xsl:template>
</xsl:stylesheet>
