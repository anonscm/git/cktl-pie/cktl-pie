<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE xsl:stylesheet
[
<!ENTITY  nbsp  "&#160;">
<!ENTITY  space  "&#x20;">
<!ENTITY  br  "&#x2028;">
]>



<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

<!-- TPL validation -->
	<xsl:template name="write-validation">
    <xsl:param name="libelle" />
	<xsl:param name="date" />	
    <xsl:choose > 
		<xsl:when test="$date!=''"> 
			<fo:block>Le&nbsp;<xsl:value-of select="$libelle"/>&nbsp;a valid� le devis le&nbsp;<xsl:call-template name="write-date"><xsl:with-param name="date" select="$date"/></xsl:call-template></fo:block>
		</xsl:when>
		<xsl:otherwise>Le devis n'a pas �t� valid� par le&nbsp;<xsl:value-of select="$libelle"/></xsl:otherwise>
    </xsl:choose>
</xsl:template>


<!-- TPL remiseglobale -->
<xsl:template name="remiseglobale" match="pourcentremiseglobal">
		<xsl:if test="normalize-space(.)!=''"> 
			<xsl:if test="format-number(normalize-space(.),'0,00')!='0,00'"> 
				<fo:block>Vous b�n�ficiez d'une remise de&nbsp;<xsl:value-of select="normalize-space(.)"/>% sur les prix du catalogue.&br;Cette remise est prise en compte dans les montants affich�s ici.</fo:block>
			</xsl:if>
		</xsl:if>
</xsl:template>

	   
	<!-- TPL DevisDetail-->
   <xsl:template name="devisdetail" match="devisdetail" >
	
	 <fo:table inline-progression-dimension="182mm" table-layout="fixed" space-before="0pt" border-width="0.5pt" border-style="solid">
	      <fo:table-column column-width="127mm" border="0pt solid"/>
		  <fo:table-column column-width="55mm" border="0.5pt 0pt 0pt 0pt solid"/>
	      <fo:table-body font-size="10pt">
			<fo:table-row>
				<fo:table-cell padding="6pt">
					<fo:block><xsl:value-of select="devlibelle"/></fo:block>
				</fo:table-cell>
				<fo:table-cell padding="4pt">
					<fo:table inline-progression-dimension="100%" table-layout="fixed" space-before="0pt" border-width="0pt" border-style="solid">
						<fo:table-column column-width="15mm" border="0pt solid"/>
						<fo:table-column column-width="47mm" border="0pt solid"/>						
					      <fo:table-body font-size="8pt">
							
							<fo:table-row>
								<fo:table-cell><fo:block>Type :</fo:block></fo:table-cell>
								<fo:table-cell><fo:block><xsl:value-of select="/reportdevis/typeprestation"/></fo:block>
								</fo:table-cell>
							</fo:table-row>								
							
							<fo:table-row>
								<fo:table-cell ><fo:block>N� :</fo:block></fo:table-cell>
								<fo:table-cell >
									    <xsl:choose> 
											<xsl:when test="normalize-space(devordrereadonly)!=''">
												<fo:block><xsl:value-of select="devordrereadonly"/></fo:block>
											</xsl:when>
											<xsl:otherwise>
												<fo:block>(non enregistr�)</fo:block>
											</xsl:otherwise>										
									  </xsl:choose > 
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell ><fo:block>Date :</fo:block></fo:table-cell>
								<fo:table-cell >
									    <xsl:choose > 
											<xsl:when test="normalize-space(devordrereadonly)!=''">
												<fo:block><xsl:call-template name="write-date"><xsl:with-param name="date" select="devdatedevis"/></xsl:call-template></fo:block>
											</xsl:when>	
										   </xsl:choose > 
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell ><fo:block>Cat :</fo:block></fo:table-cell>
								<fo:table-cell ><fo:block><xsl:value-of select="../catalogue/catordrereadonly"/></fo:block>
									<fo:block></fo:block>							
								</fo:table-cell>
							</fo:table-row>							
							
							<fo:table-row>
								<fo:table-cell ><fo:block>Contact :</fo:block></fo:table-cell>
								<fo:table-cell >
									<fo:block><xsl:value-of select="../personne/persnomprenom"/></fo:block>							
								</fo:table-cell>
							</fo:table-row>
							
						  </fo:table-body>								
					</fo:table>
					<!--
					<xsl:choose>
						<xsl:when test="normalize-space(devordrereadonly)!=''">
							<fo:block>Devis N� <xsl:value-of select="devordrereadonly"/>&nbsp;cr�� le&nbsp;<xsl:call-template name="write-date"><xsl:with-param name="date" select="devdatedevis"/></xsl:call-template>&nbsp;(contact&nbsp;:&nbsp;<xsl:value-of select="../personne/persnomprenom"/>)</fo:block>
						</xsl:when>
						<xsl:otherwise>
							<fo:block>Devis non enregistr�</fo:block>
						</xsl:otherwise>	
					</xsl:choose>
					-->
				</fo:table-cell>
			</fo:table-row>
		  </fo:table-body>
	 </fo:table>	
   </xsl:template>
   
<!-- TPL Devis-->
   <xsl:template name="devis" match="devis" >
	
	<xsl:apply-templates select="devisdetail" />
	
	
	
	 <fo:table inline-progression-dimension="182mm" table-layout="fixed" space-before="0pt" border-width="0.5pt" border-style="solid">
	      <fo:table-column column-width="80mm" border="0.5pt solid"/>
	      <fo:table-column column-width="32mm" border="0.5pt solid"/>
	      <fo:table-column column-width="15mm" border="0.5pt solid"/>
	      <fo:table-column column-width="20mm" border="0.5pt solid"/>		  
	      <fo:table-column column-width="15mm" border="0.5pt solid"/>
	      <fo:table-column column-width="20mm" border="0.5pt solid"/>		  

		  
		  <fo:table-header font-weight="bold">
	         <fo:table-row >
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center"><fo:block>Prestations</fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center" ><fo:block>R�f�rence</fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center" ><fo:block>Qt�</fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center" ><fo:block>Prix U. HT</fo:block></fo:table-cell>				
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center" ><fo:block>TVA</fo:block></fo:table-cell>				
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center" ><fo:block>Montant HT</fo:block></fo:table-cell>				

				
			 </fo:table-row>
			</fo:table-header>
	      <fo:table-body font-size="8pt">
					<xsl:apply-templates select="devisligne" />
		  </fo:table-body>
	 </fo:table>
   </xsl:template>

   
   <!--TPL taux -->
     <xsl:template name="taux" match="taux" >
		<xsl:value-of select="format-number(tautaux,'0,00')"/>%
   </xsl:template>
   
<!-- TPL lignes article -->
   <xsl:template name="article" match="article" >
	         <fo:table-row border="0.5pt solid">
				<fo:table-cell border="0.5pt solid" padding="2pt"><fo:block><xsl:value-of select="dligdescription"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"><fo:block><xsl:call-template name="write-string-max"><xsl:with-param name="string" select="dligreference"/><xsl:with-param name="maxlength" select="19"/></xsl:call-template></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"><fo:block><xsl:value-of select="format-number(dlignbarticles,'0,00')"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"><fo:block><xsl:value-of select="format-number(dligartht,'0,00')"/></fo:block></fo:table-cell>												
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"><fo:block><xsl:apply-templates select="taux"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"><fo:block><xsl:value-of select="format-number(prixligneht,'0,00')"/></fo:block></fo:table-cell>																
			 </fo:table-row>	
   </xsl:template>
   
<!-- TPL une ligne d'option -->
   <xsl:template name="option" match="option" >
	         <fo:table-row border="0.5pt solid">
				<fo:table-cell border="0.5pt solid" padding="2pt" padding-left="10pt"><fo:block>+Option : <xsl:value-of select="dligdescription"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"><fo:block><xsl:call-template name="write-string-max"><xsl:with-param name="string" select="dligreference"/><xsl:with-param name="maxlength" select="19"/></xsl:call-template></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"><fo:block><xsl:value-of select="format-number(dlignbarticles,'0,00')"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"><fo:block><xsl:value-of select="format-number(dligartht,'0,00')"/></fo:block></fo:table-cell>							
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"><fo:block><xsl:apply-templates select="taux"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"><fo:block><xsl:value-of select="format-number(prixligneht,'0,00')"/></fo:block></fo:table-cell>																
			 </fo:table-row>	
   </xsl:template>
   
<!-- TPL une ligne de remise -->
   <xsl:template name="remise" match="remise">
	         <fo:table-row border="0.5pt solid" font-style="italic">
				<fo:table-cell border="0.5pt solid" padding="2pt"  padding-left="10pt"><fo:block>-Remise : <xsl:value-of select="dligdescription"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"><fo:block><xsl:call-template name="write-string-max"><xsl:with-param name="string" select="dligreference"/><xsl:with-param name="maxlength" select="19"/></xsl:call-template></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"><fo:block><xsl:value-of select="format-number(dlignbarticles,'0,00')"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"><fo:block><xsl:value-of select="format-number(dligartht,'0,00')"/></fo:block></fo:table-cell>							
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"><fo:block><xsl:apply-templates select="taux"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"><fo:block><xsl:value-of select="format-number(prixligneht,'0,00')"/></fo:block></fo:table-cell>																
			 </fo:table-row>	
   </xsl:template>
   
   
<!-- TPL lignes de devis -->
   <xsl:template name="devisligne" match="devisligne" >
		<xsl:apply-templates select="article" />
			 <xsl:apply-templates select="option" />
			 <xsl:apply-templates select="remise" />
   </xsl:template>

	
	
<!-- TPL reportdevis -->
<xsl:template name="reportdevis" match="reportdevis">

    <fo:static-content flow-name="xsl-region-end">
			
    </fo:static-content>

    <fo:static-content flow-name="xsl-region-after">
			<fo:table inline-progression-dimension="182mm" table-layout="fixed" space-before="0pt" padding="3pt" border-width="0.5pt  0pt 0pt 0pt" border-style="solid">
					 <fo:table-column  column-width="100%"/>
			         <fo:table-body>		
							<fo:table-row height="20mm" >
								<fo:table-cell display-align="before" border="0pt solid">
									<fo:table inline-progression-dimension="182mm" table-layout="fixed" space-before="5pt">
											<fo:table-column column-width="182mm"/>
											<fo:table-body>
												<fo:table-row  keep-with-next="always" >
													<fo:table-cell font-size="9pt" font-style="italic">
														<fo:block><xsl:value-of select="devis/devisdetail/devcommentaireprest "/></fo:block>
													</fo:table-cell>
												</fo:table-row>	
												<fo:table-row  keep-with-next="always" >
													<fo:table-cell font-size="9pt" font-style="italic">
														<xsl:if test="metadata/showvalidation='1'">															
															<!-- infos sur etat de la validation -->
															<fo:block > <xsl:call-template name="write-validation"><xsl:with-param name="libelle" select="'client'"/><xsl:with-param name="date" select="devis/devisdetail/devdateclientvalide"/></xsl:call-template> </fo:block>
															<fo:block> <xsl:call-template name="write-validation"><xsl:with-param name="libelle" select="'prestataire'"/><xsl:with-param name="date" select="devis/devisdetail/devdateprestvalide"/></xsl:call-template> </fo:block>
															<!-- /infos sur etat de la validation -->
														</xsl:if>
														<!-- observations -->
														
														<!-- /observations -->
														<fo:block text-align="right" font-size="5pt" font-style="italic" writing-mode="bottom"><xsl:value-of select="metadata/appalias"/></fo:block>
													</fo:table-cell>
												</fo:table-row>	
											</fo:table-body>
										 </fo:table>
								</fo:table-cell>
							</fo:table-row>			
			         </fo:table-body>
					</fo:table>				
    </fo:static-content>
	

     <fo:static-content flow-name="xsl-region-before">
		
		<fo:table inline-progression-dimension="182mm" table-layout="fixed" space-before="0pt" padding="0pt">
		 <fo:table-column  column-width="100%"/>
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
		 
			      <fo:table table-layout="fixed" space-before="0pt" padding="0pt" >
			         <fo:table-column  column-width="64mm"/>
			         <fo:table-column  column-width="118mm"/>
			
			         <fo:table-body>
			            <fo:table-row height="68mm">
			               <fo:table-cell>
			<!-- Premiere colonne 1 : logo + etablissement -->
			                  <fo:table table-layout="fixed" space-before="0pt" border-width="0.5pt" border-style="solid">
			                     <fo:table-column column-width="100%" />
			                     <fo:table-body>
			                        <fo:table-row>
							          <fo:table-cell border-bottom="0.5pt solid"> 
										<xsl:apply-templates select="etablissement"/>
										</fo:table-cell>
			                        </fo:table-row>
			                        <fo:table-row height="40mm">
			                           <fo:table-cell padding="3pt" display-align="after">
										<xsl:apply-templates select="fournisseur"/>
									   </fo:table-cell>
			                        </fo:table-row>
			                     </fo:table-body>
			                  </fo:table>
			               </fo:table-cell>
			
			<!-- / logo + etablissement -->
			               <fo:table-cell height="100%">
			<!-- deuxieme colonne, s�par�e en deux -->
			                  <fo:table table-layout="fixed" space-before="0pt" border-width="0pt" border-style="solid" height="100%">
			                     <fo:table-column column-width="100%" />
			                     <fo:table-body>
			                        <fo:table-row>
										<fo:table-cell>
			<!-- titre + cadre r�f�rence -->
			                           <fo:table table-layout="fixed" space-before="0pt" border-width="1pt" border-style="none">
			                              <fo:table-column column-width="52mm" />
			                              <fo:table-column column-width="66mm" />
			                              <fo:table-body>
			                                 <fo:table-row  height="28mm">
			                                    <fo:table-cell>
			<!-- titre -->
			                                       <fo:block font-weight="bold" font-family="Arial, Helvetica, sans-serif" font-size="16pt" text-align="center">DEVIS</fo:block>
			<!-- /titre -->
			                                    </fo:table-cell>
			                                    <fo:table-cell >
													
			                                       <fo:table table-layout="fixed" space-before="0pt" border-width="0.5pt" border-style="none">
			                                          <fo:table-column column-width="66mm"/>
			                                          <fo:table-body >
															<!-- Type de prestation-->
															<!--
			                                             <fo:table-row>
			                                                <fo:table-cell  padding="3pt 3pt 3pt 3pt" font-size="12pt" font-style="italic" font-weight="bold">
			                                                   <fo:block text-align="right" linefeed-treatment="preserve"><xsl:value-of select="typeprestation"/></fo:block>
			                                                </fo:table-cell>
			                                             </fo:table-row>
															-->
															 <!-- /Type de prestation-->
														 
			                                             <fo:table-row>
			                                                <fo:table-cell text-align="right">
																<!--lieu date-->
			                                                   <xsl:call-template name="lieudate"><xsl:with-param name="lieuimpression" select="lieuimpression"/><xsl:with-param name="dateimpression" select="dateimpression"/></xsl:call-template>
															   <!-- /lieu date-->
			                                                </fo:table-cell>
			                                             </fo:table-row>
			                                          </fo:table-body>
			                                       </fo:table>
												  
			                                    </fo:table-cell>
			                                 </fo:table-row>
			                              </fo:table-body>
			                           </fo:table>
			
			<!-- / titre + cadre r�f�rence -->
				                     </fo:table-cell>
			                        </fo:table-row>
									<fo:table-row height="2mm"/>
			                        <fo:table-row height="40mm">
			                           <fo:table-cell >							
			<!--coordonn�es client -->
			                           <fo:table itable-layout="fixed" space-before="0pt" border-width="0pt" border-style="solid">
			                              <fo:table-column column-width="43mm" />
			                              <fo:table-column column-width="75mm" />
			                              <fo:table-body>
			                                 <fo:table-row>
			                                    <fo:table-cell display-align="center"  text-align="center">

			                                    </fo:table-cell>
			                                    <fo:table-cell>
													<!-- Coordonnees client -->
			                                       <fo:table table-layout="fixed" space-before="0pt" border-width="0pt" border-style="none">
													  <fo:table-column column-width="75mm" />
			                                          <fo:table-body>
			                                             <fo:table-row>
			                                                <fo:table-cell>
			                                                   <fo:block text-align="left" font-size="12pt" font-style="italic" font-weight="bold" text-decoration="none" space-after="3pt"></fo:block>
															   <xsl:apply-templates select="client"/>
			                                                </fo:table-cell>
			                                             </fo:table-row>
			                                          </fo:table-body>
			                                       </fo:table>
												   <!-- /Coordonnees client -->
			                                    </fo:table-cell>
			                                 </fo:table-row>
			                              </fo:table-body>
			                           </fo:table>
									 </fo:table-cell>
			<!-- / coordonn�es client  -->
			                        </fo:table-row>
			                     </fo:table-body>
			                  </fo:table>
			               </fo:table-cell>
			<!-- /  deuxieme colonne-->
			            </fo:table-row>
			         </fo:table-body>
			      </fo:table>
				</fo:table-cell>
			</fo:table-row>		
         </fo:table-body>
		</fo:table>
	 </fo:static-content>

    <fo:static-content flow-name="xsl-region-start">
    </fo:static-content>
	
	<fo:flow flow-name="xsl-region-body" font-family="Arial, Helvetica, sans-serif" font-size="9pt" >
		<fo:table inline-progression-dimension="182mm" table-layout="fixed" space-before="0pt" padding="0pt">
		 <fo:table-column  column-width="100%"/>
         <fo:table-body>		
			
<!-- le corps de la page -->
			<fo:table-row >
				<fo:table-cell>
				      <xsl:apply-templates select="devis" />
				</fo:table-cell>
			</fo:table-row>		
<!--/ le corps de la page -->			


			<fo:table-row  keep-with-next="always" >
				<fo:table-cell display-align="after">
					<fo:table inline-progression-dimension="182mm" table-layout="fixed" space-before="5pt" border-width="0pt" border-style="solid">
							<fo:table-column />
							<fo:table-body>
								<fo:table-row >
								
									<fo:table-cell display-align="before" border="0pt solid" >
									<!-- totaux +blabla-->
												<fo:table  inline-progression-dimension="182mm" table-layout="fixed" space-before="0pt" border-width="0pt" border-style="solid">
													<fo:table-column column-width="62mm"/>
												<fo:table-column  column-width="120mm"/>
												<fo:table-body>
													<fo:table-row keep-with-next="always" >
														<fo:table-cell display-align="before">
														</fo:table-cell>
														<fo:table-cell display-align="before">
															<fo:table>
																<fo:table-column/>
																<fo:table-body>
																	<fo:table-row height="25mm" keep-with-next="always" >
																		<fo:table-cell padding="5pt" display-align="before" text-align="right">
																			<!-- totaux -->
																				<xsl:apply-templates select="totaux"/>
																			<!-- /totaux -->
																			<fo:block/>
																																					
																		</fo:table-cell>
																	</fo:table-row>																						
																</fo:table-body>
															</fo:table>
														
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>				
									<!--totaux+blabla -->
									</fo:table-cell>
								</fo:table-row>	
								<fo:table-row>	
									<fo:table-cell text-align="right" font-style="italic">
										<xsl:apply-templates select="devis/devisdetail/pourcentremiseglobal"/>	
									</fo:table-cell>
								</fo:table-row>	
							</fo:table-body>
						 </fo:table>
				</fo:table-cell>
			</fo:table-row>	
			
			
			
         </fo:table-body>
      </fo:table>			
		
		
	</fo:flow>
	
	
	
	

   </xsl:template>

	
</xsl:stylesheet>
