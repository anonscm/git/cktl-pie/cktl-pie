<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE xsl:stylesheet
[
<!ENTITY  nbsp  "&#160;">
<!ENTITY  space  "&#x20;">
<!ENTITY  br  "&#x2028;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template name="prestations" match="prestations" >
	
	 <fo:table inline-progression-dimension="190mm" table-layout="fixed" space-before="0pt" border-width="0.5pt" border-style="solid">
	      <fo:table-column column-width="12mm" border="0.5pt solid"/>
	      <fo:table-column column-width="65mm" border="0.5pt solid"/>
	      <fo:table-column column-width="45mm" border="0.5pt solid"/>
	      <fo:table-column column-width="20mm" border="0.5pt solid"/>
	      <fo:table-column column-width="20mm" border="0.5pt solid"/>		  
	      <fo:table-column column-width="18mm" border="0.5pt solid"/>
	      <fo:table-column column-width="19mm" border="0.5pt solid"/>		  
	      <fo:table-column column-width="19mm" border="0.5pt solid"/>		  
	      <fo:table-column column-width="40mm" border="0.5pt solid"/>
	      <fo:table-column column-width="19mm" border="0.5pt solid"/>

	      <fo:table-header font-weight="bold">
	         <fo:table-row >
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center"><fo:block>No</fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center"><fo:block>Devis</fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center" ><fo:block>Client</fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center" ><fo:block>Date</fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center" ><fo:block>Etat</fo:block></fo:table-cell>				
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center" ><fo:block>Type</fo:block></fo:table-cell>				
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center" ><fo:block>Val. client</fo:block></fo:table-cell>				
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center" ><fo:block>Val. prest</fo:block></fo:table-cell>				
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center" ><fo:block>Catalogue</fo:block></fo:table-cell>				
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center" ><fo:block>Montant TTC</fo:block></fo:table-cell>				
				
			 </fo:table-row>
			</fo:table-header>
	      <fo:table-body font-size="8pt">
					<xsl:apply-templates select="prestation" />
		  </fo:table-body>
	 </fo:table>
   </xsl:template>
   

   <xsl:template name="prestation" match="prestation" >
	         <fo:table-row border="0.5pt solid" keep-together="always">
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"><fo:block><xsl:value-of select="devordre"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt"><fo:block><xsl:value-of select="devlibelle"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt"><fo:block><xsl:value-of select="persnomprenom"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"><fo:block><xsl:call-template name="write-date"><xsl:with-param name="date" select="devdatedevis"/></xsl:call-template></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt"><fo:block><xsl:value-of select="prestetatlibelle"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt"><fo:block><xsl:value-of select="ctclibellecool"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"><fo:block><xsl:call-template name="write-date"><xsl:with-param name="date" select="devdateclientvalide"/></xsl:call-template></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"><fo:block><xsl:call-template name="write-date"><xsl:with-param name="date" select="devdateprestvalide"/></xsl:call-template></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt"><fo:block><xsl:value-of select="catlibelle"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"><fo:block><xsl:value-of select="montantttc"/></fo:block></fo:table-cell>
			 </fo:table-row>	
   </xsl:template>   
	
<!-- TPL reportprestations -->
<xsl:template name="reportprestations" match="reportprestations">

    <fo:static-content flow-name="xsl-region-end">
    </fo:static-content>

    <fo:static-content flow-name="xsl-region-after">
    </fo:static-content>

     <fo:static-content flow-name="xsl-region-before">
	</fo:static-content>

    <fo:static-content flow-name="xsl-region-start">
    </fo:static-content>

	<fo:flow flow-name="xsl-region-body" font-family="Arial, Helvetica, sans-serif" font-size="9pt" >
		<fo:table inline-progression-dimension="190mm" table-layout="fixed" space-before="0pt" padding="0pt">
		 <fo:table-column  column-width="100%"/>
         <fo:table-body>		
			
<!-- le corps de la page -->
			<fo:table-row >
				<fo:table-cell>
				      <xsl:apply-templates select="prestations" />
				</fo:table-cell>
			</fo:table-row>		
<!--/ le corps de la page -->			

         </fo:table-body>
      </fo:table>			

	</fo:flow>

   </xsl:template>
	
</xsl:stylesheet>
