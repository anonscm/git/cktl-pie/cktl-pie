<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE xsl:stylesheet
[
<!ENTITY  nbsp  "&#160;">
<!ENTITY  space  "&#x20;">
<!ENTITY  br  "&#x2028;">
]>



<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

<!-- TPL validation -->
	<xsl:template name="write-validation">
    <xsl:param name="libelle" />
	<xsl:param name="date" />	
    <xsl:choose > 
		<xsl:when test="$date!=''"> 
			<fo:block>Le&nbsp;<xsl:value-of select="$libelle"/>&nbsp;a valid� le devis le&nbsp;<xsl:call-template name="write-date"><xsl:with-param name="date" select="$date"/></xsl:call-template></fo:block>
		</xsl:when>
		<xsl:otherwise>Le devis n'a pas �t� valid� par le&nbsp;<xsl:value-of select="$libelle"/></xsl:otherwise>
    </xsl:choose>
</xsl:template>


<!-- TPL remiseglobale -->
<xsl:template name="remiseglobale" match="pourcentremiseglobal">
		<xsl:if test="normalize-space(.)!=''"> 
			<xsl:if test="format-number(normalize-space(.),'0,00')!='0,00'"> 
				<fo:block>Vous b�n�ficiez d'une remise de&nbsp;<xsl:value-of select="normalize-space(.)"/>% sur les prix du catalogue.&br;Cette remise est prise en compte dans les montants affich�s ici.</fo:block>
			</xsl:if>
		</xsl:if>
</xsl:template>

	   
	<!-- TPL FacturetitreDetail-->
   <xsl:template name="facturetitredetail" match="facturetitredetail" >
	
	 <fo:table inline-progression-dimension="192mm" table-layout="fixed" space-before="0pt" border-width="0pt" border-style="solid">
	      <fo:table-column border="0pt solid"/>
	      <fo:table-body font-size="10pt">
			<fo:table-row>
				<fo:table-cell padding="6pt">
					<fo:block>Facture cumulative (imprim�e � partir de plusieurs devis)</fo:block>
				</fo:table-cell>
			</fo:table-row>
		  </fo:table-body>
	 </fo:table>	
   </xsl:template>
   
<!-- TPL Facturetitre-->
   <xsl:template name="facturetitre" match="facturetitre" >
	
	<xsl:apply-templates select="facturetitredetail" />
	
	
	
	 <fo:table inline-progression-dimension="192mm" table-layout="fixed" space-before="0pt" border-width="0.5pt" border-style="solid">
	      <fo:table-column column-width="15mm" border="0.5pt solid"/>
	      <fo:table-column column-width="78mm" border="0.5pt solid"/>
	      <fo:table-column column-width="32mm" border="0.5pt solid"/>
	      <fo:table-column column-width="15mm" border="0.5pt solid"/>
	      <fo:table-column column-width="20mm" border="0.5pt solid"/>		  
	      <fo:table-column column-width="12mm" border="0.5pt solid"/>
	      <fo:table-column column-width="20mm" border="0.5pt solid"/>		  
		  <fo:table-header font-weight="bold">
	         <fo:table-row >
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center"><fo:block># Fact</fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center"><fo:block>Prestations</fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center" ><fo:block>R�f�rence</fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center" ><fo:block>Qt�</fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center" ><fo:block>Prix U. HT [remis�]</fo:block></fo:table-cell>				
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center" ><fo:block>TVA</fo:block></fo:table-cell>				
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center" ><fo:block>Montant HT</fo:block></fo:table-cell>					
			 </fo:table-row>
			</fo:table-header>
	      <fo:table-body font-size="8pt">
					<xsl:apply-templates select="facturetitreligne" />
		  </fo:table-body>
	 </fo:table>
   </xsl:template>

   
   <!--TPL taux -->
     <xsl:template name="taux" match="taux" >
		<xsl:value-of select="format-number(tautaux,'0,00')"/>%
   </xsl:template>
   
   
<!-- TPL lignes article -->
   <xsl:template name="article" match="article" >
	         <fo:table-row border="0.5pt solid">
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"><fo:block><xsl:value-of select="ftlftordre"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt"><fo:block><xsl:value-of select="ftldescription"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"><fo:block><xsl:call-template name="write-string-max"><xsl:with-param name="string" select="ftlreference"/><xsl:with-param name="maxlength" select="19"/></xsl:call-template></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"><fo:block><xsl:value-of select="format-number(ftlnbarticles,'0,00')"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"><fo:block><xsl:value-of select="format-number(ftlartht,'0,00')"/></fo:block></fo:table-cell>	
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"><fo:block><xsl:apply-templates select="taux"/></fo:block></fo:table-cell>				
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"><fo:block><xsl:value-of select="format-number(prixligneht,'0,00')"/></fo:block></fo:table-cell>				
			 </fo:table-row>	
   </xsl:template>
   
<!-- TPL une ligne d'option -->
   <xsl:template name="option" match="option" >
	         <fo:table-row border="0.5pt solid">
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"><fo:block><xsl:value-of select="ftlftordre"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" padding-left="10pt"><fo:block>+Option : <xsl:value-of select="ftldescription"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"><fo:block><xsl:call-template name="write-string-max"><xsl:with-param name="string" select="ftlreference"/><xsl:with-param name="maxlength" select="19"/></xsl:call-template></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"><fo:block><xsl:value-of select="format-number(ftlnbarticles,'0,00')"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"><fo:block><xsl:value-of select="format-number(ftlartht,'0,00')"/></fo:block></fo:table-cell>	
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"><fo:block><xsl:apply-templates select="taux"/></fo:block></fo:table-cell>				
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"><fo:block><xsl:value-of select="format-number(prixligneht,'0,00')"/></fo:block></fo:table-cell>								
			 </fo:table-row>	
   </xsl:template>
   
<!-- TPL une ligne de remise -->
   <xsl:template name="remise" match="remise">
	         <fo:table-row border="0.5pt solid" font-style="italic">
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"><fo:block><xsl:value-of select="ftlftordre"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt"  padding-left="10pt"><fo:block>-Remise : <xsl:value-of select="ftldescription"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"><fo:block><xsl:call-template name="write-string-max"><xsl:with-param name="string" select="ftlreference"/><xsl:with-param name="maxlength" select="19"/></xsl:call-template></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"><fo:block><xsl:value-of select="format-number(ftlnbarticles,'0,00')"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"><fo:block><xsl:value-of select="format-number(ftlartht,'0,00')"/></fo:block></fo:table-cell>	
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"><fo:block><xsl:apply-templates select="taux"/></fo:block></fo:table-cell>				
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"><fo:block><xsl:value-of select="format-number(prixligneht,'0,00')"/></fo:block></fo:table-cell>								
			 </fo:table-row>	
   </xsl:template>
   
   
<!-- TPL lignes de facturetitre -->
   <xsl:template name="facturetitreligne" match="facturetitreligne" >
		<xsl:apply-templates select="article" />
			 <xsl:apply-templates select="option" />
			 <xsl:apply-templates select="remise" />
   </xsl:template>

	
	
<!-- TPL reportfacturetitre -->
<xsl:template name="reportfacturetitre" match="reportfacturetitre">

    <fo:static-content flow-name="xsl-region-end">
			
    </fo:static-content>

    <fo:static-content flow-name="xsl-region-after">
		<fo:table display-align="after"  inline-progression-dimension="192mm" table-layout="fixed" space-before="0pt" border-width="0pt" border-style="solid">
			<fo:table-column column-width="72mm"/>
			<fo:table-column  column-width="120mm"/>
			<fo:table-body>													
				<fo:table-row keep-with-next="always" >
					<fo:table-cell display-align="after" number-columns-spanned="2">
						<!-- blabla en bas � gauche-->
						<fo:table  border-width="0.5pt" border-style="solid" padding="2pt"  font-size="9pt" >
							<fo:table-column column-width="120mm" border-right="0.5pt solid"/>
							<fo:table-column  column-width="72mm"/>																
							<fo:table-body>
								<fo:table-row keep-with-next="always"  >
									<fo:table-cell text-align="center" display-align="center" border-bottom="0.5pt solid">
										<fo:block >Informations internes</fo:block>
									</fo:table-cell>
									<fo:table-cell text-align="center" display-align="center" border-bottom="0.5pt solid" >
										<fo:block>Signature et cachet de l'ordonnateur</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row keep-with-next="always" height="20mm" >
									<fo:table-cell display-align="before" >
															<fo:table font-size="8pt" padding="5pt">
																<fo:table-column column-width="25mm" />
																<fo:table-column />
																<fo:table-body>
																	<fo:table-row>
																		<fo:table-cell>
																			<fo:block>Devis associ�s&nbsp;:</fo:block>
																		</fo:table-cell>
																		<fo:table-cell>
																			<fo:block><xsl:value-of select="devisassocies"/></fo:block>
																		</fo:table-cell>
																	</fo:table-row>
																	<fo:table-row>
																		<fo:table-cell>
																			<fo:block>Ligne budg�taire&nbsp;:</fo:block>
																		</fo:table-cell>
																		<fo:table-cell>
																			<fo:block><xsl:value-of select="facturetitre/organprest/organlib"/></fo:block>
																		</fo:table-cell>
																	</fo:table-row>
																</fo:table-body>
															</fo:table>										
										
<!--
										<fo:block font-weight="bold">REGLEMENT</fo:block>
										<fo:block>Ch�que:</fo:block>
											<fo:block><xsl:value-of select="etablissement/msgreglementcheque"/> </fo:block>
											<fo:block></fo:block>
											<fo:block>Virement:</fo:block>
											<fo:block><xsl:value-of select="etablissement/msgreglementvirement"/> </fo:block>
-->
									</fo:table-cell>
									<fo:table-cell display-align="center" padding="5pt" text-align="right">
										
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>				
					</fo:table-cell>														
				</fo:table-row>
			</fo:table-body>
		</fo:table>				
			<fo:table inline-progression-dimension="192mm" table-layout="fixed" space-before="0pt" padding="3pt" border-width="0pt  0pt 0pt 0pt" border-style="solid">
					 <fo:table-column  column-width="100%"/>
			         <fo:table-body>		
							<fo:table-row height="15mm" >
								<fo:table-cell display-align="before" border="0pt solid">
									<fo:table inline-progression-dimension="192mm" table-layout="fixed" space-before="5pt">
											<fo:table-column column-width="192mm"/>
											<fo:table-body>
												<fo:table-row  keep-with-next="always" >
													<fo:table-cell font-size="9pt" font-style="italic">
														<xsl:if test="metadata/showvalidation='1'">
															<!-- infos sur etat de la validation -->
															<fo:block > <xsl:call-template name="write-validation"><xsl:with-param name="libelle" select="'client'"/><xsl:with-param name="date" select="facturetitre/facturetitredetail/devdateclientvalide"/></xsl:call-template> </fo:block>
															<fo:block> <xsl:call-template name="write-validation"><xsl:with-param name="libelle" select="'prestataire'"/><xsl:with-param name="date" select="facturetitre/facturetitredetail/devdateprestvalide"/></xsl:call-template> </fo:block>
															<!-- /infos sur etat de la validation -->
														</xsl:if>
														<!-- observations -->
														
														<!-- /observations -->
														<fo:block text-align="right" font-size="5pt" font-style="italic" writing-mode="bottom"><xsl:value-of select="metadata/appalias"/></fo:block>
													</fo:table-cell>
												</fo:table-row>	
											</fo:table-body>
										 </fo:table>
								</fo:table-cell>
							</fo:table-row>			
			         </fo:table-body>
					</fo:table>				
    </fo:static-content>
	

     <fo:static-content flow-name="xsl-region-before">
		
		<fo:table inline-progression-dimension="192mm" table-layout="fixed" space-before="0pt" padding="0pt">
		 <fo:table-column  column-width="100%"/>
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
		 
			      <fo:table table-layout="fixed" space-before="0pt" padding="0pt" >
			         <fo:table-column  column-width="82mm"/>
			         <fo:table-column  column-width="110mm"/>
			
			         <fo:table-body>
			            <fo:table-row height="70mm">
			               <fo:table-cell>
			<!-- Premiere colonne 1 : logo + etablissement -->
			                  <fo:table table-layout="fixed" space-before="0pt" border-width="0.5pt" border-style="solid">
			                     <fo:table-column column-width="100%" />
			                     <fo:table-body>
			                        <fo:table-row>
							          <fo:table-cell border-bottom="0.5pt solid"> 
										<xsl:apply-templates select="etablissement"/>
										</fo:table-cell>
			                        </fo:table-row>
			                        <fo:table-row height="40mm">
			                           <fo:table-cell padding="3pt" display-align="before" font-size="10pt">
										<!--<xsl:apply-templates select="fournisseur"/>-->
										<!-- Infos reglement-->
														<fo:block font-size="8pt" font-weight="bold">REGLEMENT</fo:block>
														<fo:block font-size="7pt" space-before="3pt"><fo:inline font-weight="bold">Ch�que</fo:inline>&nbsp;:&nbsp;<xsl:value-of select="etablissement/msgreglementcheque"/> </fo:block>
														<fo:block font-size="7pt" space-before="3pt"><fo:inline font-weight="bold">Virement</fo:inline>&nbsp;:&nbsp;<xsl:value-of select="etablissement/msgreglementvirement"/> </fo:block>
									   </fo:table-cell>
			                        </fo:table-row>
			                     </fo:table-body>
			                  </fo:table>
			               </fo:table-cell>
			
			<!-- / logo + etablissement -->
			               <fo:table-cell height="100%">
			<!-- deuxieme colonne, s�par�e en deux -->
			                  <fo:table table-layout="fixed" space-before="0pt" border-width="0pt" border-style="solid" height="100%">
			                     <fo:table-column column-width="100%" />
			                     <fo:table-body>
			                        <fo:table-row>
										<fo:table-cell>
			<!-- titre + cadre r�f�rence -->
			                           <fo:table table-layout="fixed" space-before="0pt" border-width="1pt" border-style="none">
			                              <fo:table-column column-width="10mm" />
			                              <fo:table-column column-width="100mm" />
			                              <fo:table-body>
			                                 <fo:table-row  height="28mm">
			                                    <fo:table-cell>

			                                    </fo:table-cell>
			                                    <fo:table-cell >
													
			                                       <fo:table table-layout="fixed" space-before="0pt" border-width="0.5pt" border-style="none">
			                                          <fo:table-column/>
			                                          <fo:table-body >
															<!-- Type de prestation-->
															<!--
			                                             <fo:table-row>
			                                                <fo:table-cell  padding="3pt 3pt 3pt 3pt" font-size="12pt" font-style="italic" font-weight="bold">
			                                                   <fo:block text-align="right" linefeed-treatment="preserve"><xsl:value-of select="typeprestation"/></fo:block>
			                                                </fo:table-cell>
			                                             </fo:table-row>
															-->
															 <!-- /Type de prestation-->
			                                             <fo:table-row space-after="4pt">
			                                                <fo:table-cell text-align="right">
                                                                <fo:block font-family="Arial, Helvetica, sans-serif" font-size="12pt"><xsl:call-template name="lieudate"><xsl:with-param name="lieuimpression" select="lieuimpression"/><xsl:with-param name="dateimpression" select="dateimpression"/></xsl:call-template></fo:block>
															</fo:table-cell>
														 </fo:table-row>
															 
			                                             <fo:table-row>
			                                                <fo:table-cell text-align="right">
															      <xsl:choose> 
																	<xsl:when test="normalize-space(facturetitre/facturetitredetail/ftordrereadonly)!=''">
																		<fo:block space-before="10mm" font-weight="bold" font-family="Arial, Helvetica, sans-serif" font-size="20pt" font-style="italic" text-align="right">FACTURE&nbsp;N�<xsl:value-of select="facturetitre/facturetitredetail/ftordrereadonly"/></fo:block>
																	</xsl:when>
																	<xsl:otherwise>
																		<fo:block space-before="10mm" font-weight="bold" font-family="Arial, Helvetica, sans-serif" font-size="20pt" font-style="italic" text-align="right">FACTURE&nbsp;(non&nbsp;enregistr�e)</fo:block>
																	</xsl:otherwise>					
															   </xsl:choose> 
															</fo:table-cell>
			                                             </fo:table-row>
			                                          </fo:table-body>
			                                       </fo:table>
												  
			                                    </fo:table-cell>
			                                 </fo:table-row>
			                              </fo:table-body>
			                           </fo:table>
			
			<!-- / titre + cadre r�f�rence -->
				                     </fo:table-cell>
			                        </fo:table-row>
									<fo:table-row height="2mm"/>
			                        <fo:table-row height="40mm">
			                           <fo:table-cell >							
			<!--coordonn�es client -->
			                           <fo:table itable-layout="fixed" space-before="0pt" border-width="0pt" border-style="solid">
			                              <fo:table-column column-width="18mm" />
			                              <fo:table-column column-width="100mm" />
			                              <fo:table-body>
			                                 <fo:table-row>
			                                    <fo:table-cell display-align="center"  text-align="center">

			                                    </fo:table-cell>
			                                    <fo:table-cell>
													<!-- Coordonnees client -->
			                                       <fo:table table-layout="fixed" space-before="0pt" border-width="0pt" border-style="none">
													  <fo:table-column column-width="75mm" />
			                                          <fo:table-body>
			                                             <fo:table-row>
			                                                <fo:table-cell>
			                                                   <fo:block text-align="left" font-size="12pt" font-style="italic" font-weight="bold" text-decoration="none" space-after="3pt"></fo:block>
															   <xsl:apply-templates select="client"/>
			                                                </fo:table-cell>
			                                             </fo:table-row>
			                                          </fo:table-body>
			                                       </fo:table>
												   <!-- /Coordonnees client -->
			                                    </fo:table-cell>
			                                 </fo:table-row>
			                              </fo:table-body>
			                           </fo:table>
									 </fo:table-cell>
			<!-- / coordonn�es client  -->
			                        </fo:table-row>
			                     </fo:table-body>
			                  </fo:table>
			               </fo:table-cell>
			<!-- /  deuxieme colonne-->
			            </fo:table-row>
			         </fo:table-body>
			      </fo:table>
				</fo:table-cell>
			</fo:table-row>		
         </fo:table-body>
		</fo:table>
	 </fo:static-content>

    <fo:static-content flow-name="xsl-region-start">
    </fo:static-content>
	
	<fo:flow flow-name="xsl-region-body" font-family="Arial, Helvetica, sans-serif" font-size="9pt" >
		<fo:table inline-progression-dimension="192mm" table-layout="fixed" space-before="0pt" padding="0pt">
		 <fo:table-column  column-width="100%"/>
         <fo:table-body>		
			
<!-- le corps de la page -->
			<fo:table-row >
				<fo:table-cell>
				      <xsl:apply-templates select="facturetitre" />
				</fo:table-cell>
			</fo:table-row>		
<!--/ le corps de la page -->			


			<fo:table-row  keep-with-next="always" >
				<fo:table-cell display-align="after">
					<fo:table inline-progression-dimension="192mm" table-layout="fixed" space-before="5pt" border-width="0pt" border-style="solid">
							<fo:table-column />
							<fo:table-body>
								<fo:table-row >
									<fo:table-cell display-align="before" border="0pt solid" >
												<fo:table  inline-progression-dimension="192mm" table-layout="fixed" space-before="0pt" border-width="0pt" border-style="solid">
												<fo:table-column column-width="120mm"/>
												<fo:table-column  column-width="72mm"/>
												<fo:table-body>
													<fo:table-row keep-with-next="always" >
														<fo:table-cell display-align="before">
															
															
														</fo:table-cell>
														<fo:table-cell display-align="before">
															<fo:table>
																<fo:table-column/>
																<fo:table-body>
																	<fo:table-row height="25mm" keep-with-next="always" >
																		<fo:table-cell space-before="3pt" display-align="before" text-align="right">
																			<!-- totaux -->
																				<xsl:apply-templates select="totaux"/>
																			<!-- /totaux -->
																			<fo:block/>
																																					
																		</fo:table-cell>
																	</fo:table-row>																						
																</fo:table-body>
															</fo:table>
														</fo:table-cell>
													</fo:table-row>
													
													
												</fo:table-body>
											</fo:table>				
		
									</fo:table-cell>
								</fo:table-row>														

							</fo:table-body>
						 </fo:table>
				</fo:table-cell>
			</fo:table-row>	

         </fo:table-body>
      </fo:table>			
	</fo:flow>
   </xsl:template>
</xsl:stylesheet>
