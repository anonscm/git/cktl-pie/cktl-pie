<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE xsl:stylesheet
[
<!ENTITY  nbsp  "&#160;">
<!ENTITY  space  "&#x20;">
<!ENTITY  br  "&#x2028;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template name="factures" match="factures" >
	
	 <fo:table inline-progression-dimension="190mm" table-layout="fixed" space-before="0pt" border-width="0.5pt" border-style="solid">
	      <fo:table-column column-width="15mm" border="0.5pt solid"/>
	      <fo:table-column column-width="40mm" border="0.5pt solid"/>
	      <fo:table-column column-width="87mm" border="0.5pt solid"/>
	      <fo:table-column column-width="50mm" border="0.5pt solid"/>
	      <fo:table-column column-width="20mm" border="0.5pt solid"/>		  
	      <fo:table-column column-width="20mm" border="0.5pt solid"/>		  
	      <fo:table-column column-width="20mm" border="0.5pt solid"/>		  
	      <fo:table-column column-width="25mm" border="0.5pt solid"/>

	      <fo:table-header font-weight="bold">
	         <fo:table-row >
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center"><fo:block>No</fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center"><fo:block>R�f�rence</fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center" ><fo:block>Client</fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center" ><fo:block>Fournisseur</fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center" ><fo:block>Date</fo:block></fo:table-cell>				
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center" ><fo:block>Val. client</fo:block></fo:table-cell>				
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center" ><fo:block>Val. prest</fo:block></fo:table-cell>				
				<fo:table-cell border="0.5pt solid" padding="3pt" text-align="center" display-align="center" ><fo:block>Montant HT</fo:block></fo:table-cell>				
			 </fo:table-row>
			</fo:table-header>
	      <fo:table-body font-size="8pt">
					<xsl:apply-templates select="facture" />
		  </fo:table-body>
	 </fo:table>
   </xsl:template>
   

   <xsl:template name="facture" match="facture" >
	         <fo:table-row border="0.5pt solid" keep-together="always">
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"><fo:block><xsl:value-of select="ftnum"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt"><fo:block><xsl:value-of select="ftRef"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt"><fo:block><xsl:value-of select="ftClient"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt"><fo:block><xsl:value-of select="fournisUlrPrest"/></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"><fo:block><xsl:call-template name="write-date"><xsl:with-param name="date" select="ftDate"/></xsl:call-template></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"><fo:block><xsl:call-template name="write-date"><xsl:with-param name="date" select="ftValideClient"/></xsl:call-template></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="center"><fo:block><xsl:call-template name="write-date"><xsl:with-param name="date" select="ftValidePrestataire"/></xsl:call-template></fo:block></fo:table-cell>
				<fo:table-cell border="0.5pt solid" padding="2pt" text-align="right"><fo:block><xsl:value-of select="ftMontht"/></fo:block></fo:table-cell>
			 </fo:table-row>	
   </xsl:template>   
	
<!-- TPL reportprestations -->
<xsl:template name="reportfactures" match="reportfactures">

    <fo:static-content flow-name="xsl-region-end">
    </fo:static-content>

    <fo:static-content flow-name="xsl-region-after">
    </fo:static-content>

     <fo:static-content flow-name="xsl-region-before">
	</fo:static-content>

    <fo:static-content flow-name="xsl-region-start">
    </fo:static-content>

	<fo:flow flow-name="xsl-region-body" font-family="Arial, Helvetica, sans-serif" font-size="9pt" >
		<fo:table inline-progression-dimension="190mm" table-layout="fixed" space-before="0pt" padding="0pt">
		 <fo:table-column  column-width="100%"/>
         <fo:table-body>		
			
<!-- le corps de la page -->
			<fo:table-row >
				<fo:table-cell>
				      <xsl:apply-templates select="factures" />
				</fo:table-cell>
			</fo:table-row>		
<!--/ le corps de la page -->			

         </fo:table-body>
      </fo:table>			

	</fo:flow>

   </xsl:template>
	
</xsl:stylesheet>
