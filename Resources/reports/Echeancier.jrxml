<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Echeancier" pageWidth="595" pageHeight="842" columnWidth="535" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.6105100000000008"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="124"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="signatureFonction" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Petit chef"]]></defaultValueExpression>
	</parameter>
	<parameter name="signatureIdentite" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Bobby Joe"]]></defaultValueExpression>
	</parameter>
	<parameter name="echId" class="java.lang.Integer" isForPrompting="false">
		<defaultValueExpression><![CDATA[new Integer(4608)]]></defaultValueExpression>
	</parameter>
	<parameter name="TIME_ZONE" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Europe/Paris"]]></defaultValueExpression>
	</parameter>
	<parameter name="persIdClient" class="java.lang.Integer" isForPrompting="false">
		<defaultValueExpression><![CDATA[new Integer(174)]]></defaultValueExpression>
	</parameter>
	<parameter name="intituleReferenceVariable" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="contenuReferenceVariable" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[select
  to_char(sysdate,'DD/MM/YYYY HH24:MI:SS') as NOW,
  ech.ech_id as numero_index_echeancier,
  ech.ech_libelle,
  trim(initcap(ind.c_civilite)||' '||initcap(p.pers_lc)||' '||initcap(p.pers_libelle)) as identite_client,
  det.*,

  jefy_admin.api_jasper_param.get_param_admin('MAIN_LOGO_URL',to_number(to_char(SYSDATE,'yyyy'))) as LOGO_URL
from
  jefy_echeancier.echeancier ech,
  jefy_echeancier.echeancier_detail det,
  grhum.personne p,
  grhum.individu_ulr ind
where
  ech.ech_id = $P{echId}
  and det.ech_id = ech.ech_id
  and p.pers_id = $P{persIdClient}
  and p.pers_id = ind.pers_id(+)
order by
  det.echd_numero]]>
	</queryString>
	<field name="NOW" class="java.lang.String"/>
	<field name="NUMERO_INDEX_ECHEANCIER" class="java.math.BigDecimal"/>
	<field name="ECH_LIBELLE" class="java.lang.String"/>
	<field name="IDENTITE_CLIENT" class="java.lang.String"/>
	<field name="ECHD_ID" class="java.math.BigDecimal"/>
	<field name="ECH_ID" class="java.math.BigDecimal"/>
	<field name="ECHD_NUMERO" class="java.math.BigDecimal"/>
	<field name="ECHD_MONTANT" class="java.math.BigDecimal"/>
	<field name="ECHD_DATE_PREVUE" class="java.sql.Timestamp"/>
	<field name="ECHD_COMMENTAIRE" class="java.lang.String"/>
	<field name="LOGO_URL" class="java.lang.String"/>
	<variable name="TotalMontantsEcheances" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{ECHD_MONTANT}]]></variableExpression>
	</variable>
	<group name="EcheancesGroup">
		<groupExpression><![CDATA[null]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band height="33" splitType="Stretch">
				<staticText>
					<reportElement key="staticText-8" mode="Transparent" x="304" y="15" width="74" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
					<box leftPadding="5" rightPadding="5">
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
						<font fontName="Arial" size="10" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[TOTAL :]]></text>
				</staticText>
				<textField evaluationTime="Report" pattern="#,##0.00 ¤" isBlankWhenNull="false">
					<reportElement key="textField" mode="Transparent" x="385" y="15" width="150" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
						<font fontName="Arial" size="10" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{TotalMontantsEcheances}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="114" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-1" mode="Transparent" x="116" y="20" width="406" height="47" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.25" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.25" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.25" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.25" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None">
					<font fontName="Arial" size="24" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Echéancier de paiement]]></text>
			</staticText>
			<textField evaluationTime="Report" pattern="" isBlankWhenNull="false">
				<reportElement key="textField-2" mode="Transparent" x="407" y="0" width="128" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{NOW}]]></textFieldExpression>
			</textField>
			<image scaleImage="RetainShape" hAlign="Left" vAlign="Top" isUsingCache="false" onErrorType="Blank" evaluationTime="Report">
				<reportElement key="image-1" mode="Transparent" x="0" y="0" width="101" height="88" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<graphicElement fill="Solid">
					<pen lineWidth="0.0" lineStyle="Solid"/>
				</graphicElement>
				<imageExpression><![CDATA[new URL($F{LOGO_URL})]]></imageExpression>
			</image>
		</band>
	</title>
	<pageHeader>
		<band height="112" splitType="Stretch">
			<textField pattern="#,##0;-#,##0" isBlankWhenNull="false">
				<reportElement key="textField" mode="Transparent" x="106" y="3" width="319" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="5" rightPadding="5">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{NUMERO_INDEX_ECHEANCIER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-5" mode="Transparent" x="0" y="3" width="101" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="5" rightPadding="5">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Réf. échéancier :]]></text>
			</staticText>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Transparent" x="66" y="49" width="469" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="5" rightPadding="5">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{IDENTITE_CLIENT}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-10" mode="Transparent" x="0" y="49" width="60" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="5" rightPadding="5">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Client :]]></text>
			</staticText>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-8" mode="Transparent" x="0" y="16" width="101" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="5" rightPadding="5">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["Intitulé :"]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0;-#,##0" isBlankWhenNull="false">
				<reportElement key="textField-7" mode="Transparent" x="106" y="16" width="429" height="28" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="5" rightPadding="5">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ECH_LIBELLE}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-9" mode="Transparent" x="0" y="66" width="101" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="5" rightPadding="5">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{intituleReferenceVariable}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0;-#,##0" isBlankWhenNull="false">
				<reportElement key="textField-10" mode="Transparent" x="106" y="66" width="429" height="28" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="5" rightPadding="5">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{contenuReferenceVariable}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="22" splitType="Stretch">
			<rectangle radius="0">
				<reportElement key="rectangle-2" mode="Opaque" x="0" y="0" width="535" height="17" forecolor="#000000" backcolor="#FFFFFF"/>
				<graphicElement fill="Solid">
					<pen lineWidth="0.25" lineStyle="Solid"/>
				</graphicElement>
			</rectangle>
			<staticText>
				<reportElement key="staticText-2" mode="Transparent" x="0" y="0" width="93" height="17" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="5" rightPadding="5">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None">
					<font fontName="Arial" size="10" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[N° échéance]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" mode="Transparent" x="93" y="0" width="125" height="17" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="5" rightPadding="5">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None">
					<font fontName="Arial" size="10" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Date prélèvement]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-4" mode="Transparent" x="442" y="0" width="93" height="17" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="5" rightPadding="10">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None">
					<font fontName="Arial" size="10" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Montant]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-11" mode="Transparent" x="218" y="0" width="224" height="17" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="5" rightPadding="5">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None">
					<font fontName="Arial" size="10" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Commentaire]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="17" splitType="Stretch">
			<rectangle radius="0">
				<reportElement key="rectangle-3" mode="Transparent" x="0" y="0" width="535" height="17" forecolor="#000000" backcolor="#FFFFFF"/>
				<graphicElement fill="Solid">
					<pen lineWidth="0.25" lineStyle="Solid"/>
				</graphicElement>
			</rectangle>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField" mode="Transparent" x="0" y="0" width="93" height="17" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="5" rightPadding="5">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{COLUMN_COUNT}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy" isBlankWhenNull="false">
				<reportElement key="textField" mode="Transparent" x="93" y="0" width="125" height="17" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="5" rightPadding="5">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ECHD_DATE_PREVUE}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00 ¤" isBlankWhenNull="false">
				<reportElement key="textField" mode="Transparent" x="442" y="0" width="93" height="17" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="5" rightPadding="8">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ECHD_MONTANT}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField-6" mode="Transparent" x="218" y="0" width="224" height="17" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="5" rightPadding="5">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ECHD_COMMENTAIRE}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band height="70" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-3" mode="Transparent" x="0" y="55" width="260" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="5" rightPadding="5">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{IDENTITE_CLIENT}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField-4" mode="Transparent" x="265" y="3" width="270" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="5" rightPadding="5">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{signatureFonction}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField-5" mode="Transparent" x="265" y="55" width="270" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="5" rightPadding="5">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{signatureIdentite}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField-11" mode="Transparent" x="0" y="3" width="260" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
				<box leftPadding="5" rightPadding="5">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["Le client"]]></textFieldExpression>
			</textField>
		</band>
	</columnFooter>
	<pageFooter>
		<band height="39" splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
