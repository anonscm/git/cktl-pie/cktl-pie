// NSTimeZone.java
// Copyright (c) 2000, Apple Computer, Inc. All rights reserved.

package com.webobjects.foundation;


import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamField;
import java.security.AccessController;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * NSTimeZone defines the behavior of time zones for different geopolitical regions. Consequently, these objects have names (and abbreviations, such
 * as "PST") for these regions. NSTimeZone objects also represent a temporal offset, either plus or minus, from Greenwich Mean Time (GMT).
 * <P>
 * NSTimeZone provides several ways to instantiate time zones. The class also permits you to set the default time zone within your application (
 * <CODE>setDefault</CODE>). You can access this default time zone at any time with the <CODE>getDefault</CODE> static method. With the
 * <CODE>localTimeZone</CODE> static method, you can get a relative time zone object that acts as a proxy for the default time zone for any locale in
 * which it finds itself.
 * <P>
 * Because NSTimeZone is a subclass of <CODE>java.util.TimeZone</CODE>, you can also use the <CODE>java.util.TimeZone</CODE> API with NSTimeZones.
 * <P>
 * <B>WARNING:</B> NSTimeZone is only intended to be used with NSTimestamp and NSTimestampFormatter. It produces incorrect results when used with
 * Java's date-related classes.
 * <P>
 * Some NSTimestamp methods return date objects that are automatically bound to time zone objects. These date objects use the functionality of
 * NSTimeZone to adjust dates for the proper locale. Unless you specify otherwise, objects returned from NSTimestamp are bound to the default time
 * zone for the current locale.
 * <P>
 * 
 * @see #abbreviation
 * @see #getDefault
 * @see #getID
 * @see #localTimeZone
 * @see #setDefault
 * @see NSTimestamp
 * @see NSTimestampFormatter
 */
public class NSTimeZone extends TimeZone implements Cloneable, java.io.Serializable, NSCoding {

	// RDW FIXME -- We should validate that all abbreviations, aliases and formal timezone names are unique.
	// RDW FIXME -- document what needs to be done when modifying zoneinfo.zip to make sure that this returns good things (e.g. no /Etc before, etc).

	// class constants

	/**
	 * The name for the notification posted during any invocation of NSTimeZone's <CODE>resetSystemTimeZone</CODE> method.
	 * 
	 * @deprecated
	 * @see #resetSystemTimeZone
	 * @see com.webobjects.foundation.NSNotification
	 */
	@Deprecated
	public static final String SystemTimeZoneDidChangeNotification = "NSSystemTimeZoneDidChangeNotification";

	public static final Class _CLASS = _NSUtilitiesExtra._classWithFullySpecifiedNamePrime("com.webobjects.foundation.NSTimeZone");

	private static final String __ABBR_TABLE_NAME = "com/webobjects/foundation/TimeZoneInfo/Abbreviations.table";

	private static final int __ABBR_TABLE_SIZE_IN_BYTES = 9932;

	private static final String __ALIAS_TABLE_NAME = "com/webobjects/foundation/TimeZoneInfo/Aliases.table";

	private static final int __ALIAS_TABLE_SIZE_IN_BYTES = 3092;

	private static final String __GENERIC_TZ_NAME_STEM = "Etc/GMT";

	private static final String __GMT = "GMT";

	private static final int __GMT_LENGTH = __GMT.length();

	private static final String __MINUS = "-";

	private static final String __PLUS = "+";

	private static final String __UTF8 = "UTF8";

	private static final String __ZONE_ARCHIVE_NAME = "com/webobjects/foundation/TimeZoneInfo/zoneinfo.zip";

	/**
	 * Mandatory SUID field for an evolvable <CODE>Serializable</CODE> class. Computed using the <CODE>serialver</CODE> command on the original class.
	 * 
	 * @see java.io.Serializable
	 */
	static final long serialVersionUID = -3697199310879546788L;

	private static final String SerializationNameFieldKey = "name";

	private static final String SerializationDataFieldKey = "timeZoneData";

	// class variables

	// read-only after class initialization
	private static NSDictionary<String, String> __abbreviations;

	// read-only after class initialization
	private static NSDictionary __aliases;

	// write-access explicitly synchronized
	private static NSTimeZone __defaultTimeZone;

	private static NSTimeZone __gmt;

	private static final NSNumberFormatter __hourFormatter = new NSNumberFormatter("#0;#0");

	private static final NSNumberFormatter __gmtHourFormatter = new NSNumberFormatter("00;00");

	// read-only after class initialization
	private static NSSet __knownTimeZoneNames;

	private static final NSMutableDictionary __knownTimeZones = new NSMutableDictionary();

	private static final __NSLocalTimeZone __localTimeZone = new __NSLocalTimeZone();

	private static final NSNumberFormatter __gmtMinuteFormatter = new NSNumberFormatter(":00;:00");

	private static final NSMutableDictionary __namesDataTable = new NSMutableDictionary(200);

	// all access synchronized
	private static NSTimeZone __systemTimeZone;

	private static final __NSTZPeriodComparator __tzPeriodComparator = new __NSTZPeriodComparator();

	// instance variables

	protected NSData _data = null;

	protected transient int _hashCode = 0;

	protected transient boolean _initialized = false;

	protected String _name = null;

	protected transient int _rawOffset = 0;

	protected transient NSMutableArray<__NSTZPeriod> _timeZonePeriods = null;

	protected transient int _timeZonePeriodsCount = 0;

	protected transient boolean _useDaylightTime = false;

	static {
		try {
			cktlPatchForJre18();

			__loadZipEntriesFromZoneArchive();
		} catch (Throwable e) {
			// cannot NSLog as NSLog depends on this class
			System.err.println("Exception encountered while loading zoneinfo from archive during NSTimeZone class initialization: " + e.getMessage());
			e.printStackTrace(System.err);
		}
		try {
			__initTimeZoneVariables();
		} catch (Throwable e) {
			// cannot NSLog as NSLog depends on this class
			System.err.println("Exception encountered while initializing NSTimeZone class: " + e.getMessage());
			e.printStackTrace(System.err);
		}
	}

	/**
	 * rprin 24/01/2015 Force le classLoader a charger la classe __NSTZPeriod le plus tot possible, sinon on a l'erreur suivante à la premiere
	 * utilisation de la classe NSTimezone. java.lang.ClassCircularityError: com/webobjects/foundation/NSTimeZone$__NSTZPeriod
	 */
	public static void cktlPatchForJre18() {
		System.out.println("Patch NSTimezone - JRE 1.8");
		__NSTZPeriod pourForcerLeClassLoader = new __NSTZPeriod();
	}

	// constructors

	/**
	 * Required internally to implement the <CODE>java.io.Serializable</CODE> interface and should be considered private.
	 * <P>
	 * Use the following methods to get NSTimeZone objects:
	 * <UL>
	 * <LI><CODE>timeZoneForSecondsFromGMT</CODE></LI>
	 * <LI><CODE>timeZoneWithName</CODE></LI>
	 * </UL>
	 * 
	 * @see #timeZoneWithName
	 * @see #timeZoneForSecondsFromGMT
	 * @see java.io.Serializable
	 */
	public NSTimeZone() {
		super();
	}

	/**
	 * Used internally by NSTimeZone and should be considered private.
	 * <P>
	 * Use the following methods to get NSTimeZone objects:
	 * <UL>
	 * <LI><CODE>timeZoneForSecondsFromGMT</CODE></LI>
	 * <LI><CODE>timeZoneWithName</CODE></LI>
	 * </UL>
	 * 
	 * @param aName the proposed name for the newly instantiated NSTimeZone object
	 * @param aData the proposed behavior for the newly instantiated NSTimeZone object (represented by raw binary data)
	 * @see #timeZoneWithName
	 * @see #timeZoneForSecondsFromGMT
	 */
	protected NSTimeZone(String aName, NSData aData) {
		if (aName == null || aData == null) {
			throw new IllegalArgumentException("Both parameters must be non-null");
		}
		else {
			_data = aData;
			_name = aName;
		}
	}

	// private class methods

	@SuppressWarnings("fallthrough")
	private static int __bSearchTZPeriods(NSMutableArray<__NSTZPeriod> tzPeriods, int count, double time) {
		int idx, lg;
		if (count < 4) {
			switch (count) {
			case 3:
				if (tzPeriods.objectAtIndex(2)._startTime <= time)
					return 3;
			case 2:
				if (tzPeriods.objectAtIndex(1)._startTime <= time)
					return 2;
			case 1:
				if (tzPeriods.objectAtIndex(0)._startTime <= time)
					return 1;
			}
			return 0;
		}
		if (tzPeriods.objectAtIndex(count - 1)._startTime <= time)
			return count;
		if (time <= tzPeriods.objectAtIndex(0)._startTime)
			return 0;
		lg = __log2(count);
		idx = (tzPeriods.objectAtIndex(-1 + (1 << lg))._startTime <= time) ? count - (1 << lg) : -1;
		switch (--lg) {
		case 30:
			if (tzPeriods.objectAtIndex(idx + (1 << 30))._startTime <= time)
				idx += (1 << 30);
		case 29:
			if (tzPeriods.objectAtIndex(idx + (1 << 29))._startTime <= time)
				idx += (1 << 29);
		case 28:
			if (tzPeriods.objectAtIndex(idx + (1 << 28))._startTime <= time)
				idx += (1 << 28);
		case 27:
			if (tzPeriods.objectAtIndex(idx + (1 << 27))._startTime <= time)
				idx += (1 << 27);
		case 26:
			if (tzPeriods.objectAtIndex(idx + (1 << 26))._startTime <= time)
				idx += (1 << 26);
		case 25:
			if (tzPeriods.objectAtIndex(idx + (1 << 25))._startTime <= time)
				idx += (1 << 25);
		case 24:
			if (tzPeriods.objectAtIndex(idx + (1 << 24))._startTime <= time)
				idx += (1 << 24);
		case 23:
			if (tzPeriods.objectAtIndex(idx + (1 << 23))._startTime <= time)
				idx += (1 << 23);
		case 22:
			if (tzPeriods.objectAtIndex(idx + (1 << 22))._startTime <= time)
				idx += (1 << 22);
		case 21:
			if (tzPeriods.objectAtIndex(idx + (1 << 21))._startTime <= time)
				idx += (1 << 21);
		case 20:
			if (tzPeriods.objectAtIndex(idx + (1 << 20))._startTime <= time)
				idx += (1 << 20);
		case 19:
			if (tzPeriods.objectAtIndex(idx + (1 << 19))._startTime <= time)
				idx += (1 << 19);
		case 18:
			if (tzPeriods.objectAtIndex(idx + (1 << 18))._startTime <= time)
				idx += (1 << 18);
		case 17:
			if (tzPeriods.objectAtIndex(idx + (1 << 17))._startTime <= time)
				idx += (1 << 17);
		case 16:
			if (tzPeriods.objectAtIndex(idx + (1 << 16))._startTime <= time)
				idx += (1 << 16);
		case 15:
			if (tzPeriods.objectAtIndex(idx + (1 << 15))._startTime <= time)
				idx += (1 << 15);
		case 14:
			if (tzPeriods.objectAtIndex(idx + (1 << 14))._startTime <= time)
				idx += (1 << 14);
		case 13:
			if (tzPeriods.objectAtIndex(idx + (1 << 13))._startTime <= time)
				idx += (1 << 13);
		case 12:
			if (tzPeriods.objectAtIndex(idx + (1 << 12))._startTime <= time)
				idx += (1 << 12);
		case 11:
			if (tzPeriods.objectAtIndex(idx + (1 << 11))._startTime <= time)
				idx += (1 << 11);
		case 10:
			if (tzPeriods.objectAtIndex(idx + (1 << 10))._startTime <= time)
				idx += (1 << 10);
		case 9:
			if (tzPeriods.objectAtIndex(idx + (1 << 9))._startTime <= time)
				idx += (1 << 9);
		case 8:
			if (tzPeriods.objectAtIndex(idx + (1 << 8))._startTime <= time)
				idx += (1 << 8);
		case 7:
			if (tzPeriods.objectAtIndex(idx + (1 << 7))._startTime <= time)
				idx += (1 << 7);
		case 6:
			if (tzPeriods.objectAtIndex(idx + (1 << 6))._startTime <= time)
				idx += (1 << 6);
		case 5:
			if (tzPeriods.objectAtIndex(idx + (1 << 5))._startTime <= time)
				idx += (1 << 5);
		case 4:
			if (tzPeriods.objectAtIndex(idx + (1 << 4))._startTime <= time)
				idx += (1 << 4);
		case 3:
			if (tzPeriods.objectAtIndex(idx + (1 << 3))._startTime <= time)
				idx += (1 << 3);
		case 2:
			if (tzPeriods.objectAtIndex(idx + (1 << 2))._startTime <= time)
				idx += (1 << 2);
		case 1:
			if (tzPeriods.objectAtIndex(idx + (1 << 1))._startTime <= time)
				idx += (1 << 1);
		case 0:
			if (tzPeriods.objectAtIndex(idx + (1 << 0))._startTime <= time)
				idx += (1 << 0);
		}
		return ++idx;
	}

	private static NSTimeZone __concoctFixedTimeZone(int seconds, String abbr, int isDST) {
		int abbrLen = abbr.length();
		char c;
		byte[] dataBytes = new byte[52 + abbrLen * 2 + 1]; // abbreviations encoded in UTF-8
		int i;
		NSTimeZone result = null;
		// skip tzh_reserved
		__entzcode(1, dataBytes, 20); // tzh_ttisgmtcnt
		__entzcode(1, dataBytes, 24); // tzh_ttisstdcnt
		// skip tzh_leapcnt and tzh_timecnt
		__entzcode(1, dataBytes, 36); // tzh_typecnt
		__entzcode(abbrLen + 1, dataBytes, 40); // tzh_charcnt, null-terminated
		// skip tzh_timecnt stuff
		__entzcode(seconds, dataBytes, 44); // coded GMT offset in seconds
		dataBytes[48] = (isDST > 0) ? (byte) 1 : (byte) 0; // tm_isdst
		// skip abbreviation list index
		for (i = 0; i < abbrLen; i++) { // tzh_charcnt ('\0'-terminated zone abbreviation)
			c = abbr.charAt(i);

			dataBytes[50 + i * 2] = (byte) (c >>> 8);
			dataBytes[50 + i * 2 + 1] = (byte) ((c << 8) >>> 8);
		}
		// skip everything else
		result = timeZoneWithNameAndData(abbr, new NSData(dataBytes));

		return result;
	}

	private static int __detzcode(byte[] buffer, int offset) {
		int result = ((buffer[offset + 0] & 0x80) > 0) ? ~0 : 0;

		result = (result << 8) | (buffer[offset + 0] & 0xff);
		result = (result << 8) | (buffer[offset + 1] & 0xff);
		result = (result << 8) | (buffer[offset + 2] & 0xff);
		result = (result << 8) | (buffer[offset + 3] & 0xff);

		return result;
	}

	private static void __entzcode(int value, byte[] buffer, int offset) {
		buffer[offset + 0] = (byte) ((value >> 24) & 0xff);
		buffer[offset + 1] = (byte) ((value >> 16) & 0xff);
		buffer[offset + 2] = (byte) ((value >> 8) & 0xff);
		buffer[offset + 3] = (byte) ((value >> 0) & 0xff);
	}

	@SuppressWarnings("unchecked")
	private static void __initTimeZoneVariables() {
		NSData abbrData;
		InputStream abbrTable;
		NSData aliasData;
		InputStream aliasTable;
		Object plistObject;

		// Initialize the abbreviation dictionary from the table.

		abbrTable = NSTimeZone.class.getClassLoader().getResourceAsStream(__ABBR_TABLE_NAME);

		if (abbrTable == null) {
			throw new IllegalStateException("Unable to load timezone abbreviations table, \"" + __ABBR_TABLE_NAME + "\".");
		}

		try {
			abbrData = new NSData(abbrTable, __ABBR_TABLE_SIZE_IN_BYTES);
			abbrTable.close();
			if (abbrData.length() <= 0) {
				__abbreviations = NSDictionary.emptyDictionary();
			}
			else {
				plistObject = NSPropertyListSerialization.propertyListFromData(abbrData, __UTF8);

				if (plistObject != null && plistObject instanceof NSDictionary && ((NSDictionary) plistObject).count() > 0) {
					__abbreviations = (NSDictionary<String, String>) plistObject;
				}
				else {
					__abbreviations = NSDictionary.emptyDictionary();
				}
			}
		} catch (java.io.IOException e) {
			throw new NSForwardException(e, "Unable to parse data from timezone abbreviations table, \"" + __ABBR_TABLE_NAME + "\".");
		}

		// Initialize the alias dictionary from the table

		aliasTable = _CLASS.getClassLoader().getResourceAsStream(__ALIAS_TABLE_NAME);

		if (aliasTable == null) {
			throw new IllegalStateException("Unable to load timezone aliases table, \"" + __ALIAS_TABLE_NAME + "\".");
		}

		try {
			aliasData = new NSData(aliasTable, __ALIAS_TABLE_SIZE_IN_BYTES);
			aliasTable.close();
			if (aliasData.length() <= 0) {
				__aliases = NSDictionary.emptyDictionary();
			}
			else {
				plistObject = NSPropertyListSerialization.propertyListFromData(aliasData, __UTF8);

				if (plistObject != null && plistObject instanceof NSDictionary && ((NSDictionary) plistObject).count() > 0) {
					__aliases = (NSDictionary) plistObject;
				}
				else {
					__aliases = NSDictionary.emptyDictionary();
				}
			}
		} catch (java.io.IOException e) {
			throw new NSForwardException(e, "Unable to parse data from aliases table, \"" + __ALIAS_TABLE_NAME + "\".");
		}

		// Initialize the list of known names
		NSMutableSet knownTimeZoneNames = new NSMutableSet(__abbreviations.count() + __aliases.count() + __namesDataTable.count());
		knownTimeZoneNames.addObjectsFromArray(__abbreviations.allKeys());
		knownTimeZoneNames.addObjectsFromArray(__aliases.allKeys());
		knownTimeZoneNames.addObjectsFromArray(__namesDataTable.allKeys());
		__knownTimeZoneNames = knownTimeZoneNames;
		// Initialize the __gmt class variable. Since GMT will be the most commonly accessed timezone, we keep it around for quick responses.

		__gmt = timeZoneWithName(__GMT, true);
	}

	private static int __less2(int A, int W) {
		int result;

		if (A < (1 << W)) {
			result = __less3(A, W - 4);
		}
		else {
			result = __less3(A, W + 4);
		}

		return result;
	}

	private static int __less3(int A, int X) {
		int result;

		if (A < (1 << X)) {
			result = __less4(A, X - 2);
		}
		else {
			result = __less4(A, X + 2);
		}

		return result;
	}

	private static int __less4(int A, int Y) {
		int result;

		if (A < (1 << Y)) {
			result = __less5(A, Y - 1);
		}
		else {
			result = __less5(A, Y + 1);
		}

		return result;
	}

	private static int __less5(int A, int Z) {
		int result;

		if (A < (1 << Z)) {
			result = Z - 1;
		}
		else {
			result = Z;
		}

		return result;
	}

	private static void __loadZipEntriesFromZoneArchive() {
		byte[] buffer;
		InputStream is = null;
		int numRead;
		ZipEntry ze;
		long zeSize;
		ZipInputStream zis = null;

		is = NSTimeZone.class.getClassLoader().getResourceAsStream(__ZONE_ARCHIVE_NAME);

		if (is == null) {
			is = ClassLoader.getSystemClassLoader().getResourceAsStream(__ZONE_ARCHIVE_NAME);
		}

		if (is == null) {
			throw new IllegalStateException("Unable to get input stream for the timezone archive, \"" + __ZONE_ARCHIVE_NAME + "\".");
		}
		else if (is instanceof ZipInputStream) {
			zis = (ZipInputStream) is;
		}
		else {
			try {
				zis = new ZipInputStream(is);
			} catch (Throwable e) {
				try {
					is.close();
				} catch (java.io.IOException exception) {
					if (NSLog.debugLoggingAllowedForLevelAndGroups(NSLog.DebugLevelInformational, NSLog.DebugGroupIO)) {
						NSLog.debug.appendln("Exception while closing input stream: " + exception.getMessage());
						NSLog.debug.appendln(exception);
					}
				}
				throw new NSForwardException(e, "Unable to create a ZipInputStream for the timezone archive, \"" + __ZONE_ARCHIVE_NAME + "\".");
			}
		}

		// Assumes that the zone archive contains nothing but zoneinfo files (no internal .zip's, files of any other type, etc).

		try {
			for (ze = zis.getNextEntry(); ze != null; ze = zis.getNextEntry()) {
				if (!ze.isDirectory()) {
					zeSize = ze.getSize();
					if (zeSize < Integer.MAX_VALUE) {
						buffer = new byte[((int) zeSize)];
						numRead = zis.read(buffer);

						if (numRead == -1) {
							throw new IllegalStateException("Entry \"" + ze.getName() + "\" in the timezone archive, \"" + __ZONE_ARCHIVE_NAME + "\", is empty.");
						}
						else {
							while (numRead < zeSize) {
								int i = zis.read(buffer, numRead, ((int) zeSize) - numRead);
								if (i == -1) {
									break;
								}
								else {
									numRead += i;
								}
							}

							if (numRead != zeSize) {
								throw new IllegalStateException("Entry \"" + ze.getName() + "\" in the timezone archive, \"" + __ZONE_ARCHIVE_NAME
										+ "\", is not the size recorded in the zip entry. (number of bytes read = " + numRead + ";  entry size = " + zeSize + ").");
							}
							else {
								__namesDataTable.setObjectForKey(new NSData(buffer, new NSRange(0, numRead), true), ze.getName());
							}
						}
					}
					else {
						throw new IllegalStateException("Entry \"" + ze.getName() + "\" in the timezone archive, \"" + __ZONE_ARCHIVE_NAME + "\", is too large to be used.");
					}
				}
			}
		} catch (java.io.IOException e) {
			throw NSForwardException._runtimeExceptionForThrowable(e);
		} finally {
			try {
				zis.close();
			} catch (java.io.IOException e) {
				throw NSForwardException._runtimeExceptionForThrowable(e);
			}
		}

		if (__namesDataTable.count() == 0) {
			throw new IllegalStateException("Unable to find any zoneinfo files in the timezone archive, \"" + __ZONE_ARCHIVE_NAME + "\".");
		}
		else {
			// RADAR 2891274
			__namesDataTable.removeObjectForKey("Factory");
		}
	}

	private static int __log2(int x) {
		int result = 0;

		if (x < (1 << 16)) {
			result = __less2(x, 8);
		}
		else {
			result = __less2(x, 24);
		}

		return result;
	}

	private static synchronized NSTimeZone __lookupOrCreateTimeZone(String aName) {
		NSData tzData = null;
		NSTimeZone result = (NSTimeZone) __knownTimeZones.objectForKey(aName); // Caller must verify that aName is non-null and must be a name in the __namesDataTable

		// if we have a GMT name that wasn't in the table of known names, it may a custom offset like GMT+10:15 or GMT+06
		if (result == null && aName.startsWith(__GMT)) {
			int len = aName.length();
			try {
				int seconds = -1;
				if (len > __GMT_LENGTH + 1 && len < __GMT_LENGTH + 4) {
					seconds = new Integer(aName.substring(__GMT_LENGTH + 1, __GMT_LENGTH + 3)).intValue() * 3600;
				}
				else if (len > __GMT_LENGTH + 1 && len < __GMT_LENGTH + 7) {
					seconds = new Integer(aName.substring(__GMT_LENGTH + 1, __GMT_LENGTH + 3)).intValue() * 3600 + new Integer(aName.substring(__GMT_LENGTH + 4, __GMT_LENGTH + 6)).intValue() * 60;
				}
				if (seconds >= 0) {
					switch (aName.charAt(__GMT_LENGTH)) {
					case '+':
						result = timeZoneForSecondsFromGMT(seconds);
						__knownTimeZones.setObjectForKey(result, aName);
						break;
					case '-':
						result = timeZoneForSecondsFromGMT(seconds * -1);
						__knownTimeZones.setObjectForKey(result, aName);
						break;
					default:
						// aName doesn't have the expected form, so don't try to do anything with it.
					}
				}
			} catch (Exception e) {
				// OK, couldn't parse an integer out of the name string, so give up and go on.
			}
		}

		if (result == null) {
			tzData = (NSData) __namesDataTable.objectForKey(aName);
			if (tzData != null) {
				result = new NSTimeZone(aName, tzData);
				__knownTimeZones.setObjectForKey(result, aName);
			}
		}

		return result;
	}

	private static NSMutableArray<__NSTZPeriod> __parseTimeZoneData(NSData aData) {
		int timecnt;
		int typecnt;
		int charcnt;
		int cnt;
		int i;
		int idx;
		byte[] p = aData.bytes();
		int pidx = 0;
		int len = p.length;
		double distantTime = NSTimestamp.DistantPast.getTime() / 1000D;
		int timep;
		int typep;
		int ttisp;
		int charp;
		String[] abbrs;
		__NSTZPeriod tzp;
		NSMutableArray<__NSTZPeriod> tzpp = null;
		byte type;
		byte dst;
		byte abbridx;

		if (len >= 44) { // 44 == sizeof(zoneinfo file header)
			pidx += 20; // skip reserved
			pidx += 4; // skip ttisgmtcnt
			pidx += 4; // skip ttisstdcnt
			pidx += 4; // skip leapcnt
			timecnt = __detzcode(p, pidx);
			pidx += 4;
			typecnt = __detzcode(p, pidx);
			pidx += 4;
			charcnt = __detzcode(p, pidx);
			pidx += 4;
			if (typecnt > 0 && timecnt >= 0 && charcnt >= 0 && len - 44 /* == sizeof(zoneinfo file header) */>= (4 + 1) * timecnt + (4 + 1 + 1) * typecnt + charcnt) {
				cnt = (timecnt > 0) ? timecnt : 1;
				tzpp = new NSMutableArray<__NSTZPeriod>(cnt);
				// Most slots in abbrs won't be filled
				abbrs = new String[charcnt + 1];/* new NSMutableArray(charcnt + 1) */
				timep = pidx;
				typep = timep + 4 * timecnt;
				ttisp = typep + timecnt;
				charp = ttisp + (4 + 1 + 1) * typecnt;
				for (idx = 0; idx < cnt; idx++) {
					tzp = new __NSTZPeriod();
					//tzpp.addObject(tzp);
					tzp._startTime = (timecnt > 0) ? (double) __detzcode(p, timep) : distantTime;
					timep += 4; // harmless if 0 == timecnt
					type = (timecnt > 0) ? p[typep++] : 0;
					if (typecnt <= type) {
						tzp = null;
						break;
					}
					tzp._offset = __detzcode(p, ttisp + 6 * type);
					dst = p[ttisp + 6 * type + 4];
					if (dst != 0 && dst != 1) {
						tzp = null;
						break;
					}
					tzp._isdst = (dst > 0) ? 1 : 0;
					abbridx = p[ttisp + 6 * type + 5];
					if (abbridx < 0 || charcnt < abbridx) {
						tzp = null;
						break;
					}
					if (abbrs[abbridx] == null) {
						for (i = 0; i + charp + abbridx < p.length && p[i + charp + abbridx] != '\0'; i++) {/* find the null termination char */
						}
						try {
							abbrs[abbridx] = new String(p, charp + abbridx, i, __UTF8);
						} catch (java.io.UnsupportedEncodingException e) {
							throw new NSForwardException(e, "Unable to parse timezone period abbreviation.");
						}
					}
					tzp._abbreviation = abbrs[abbridx];
					if (tzpp != null)
						tzpp.addObject(tzp);
				}

				if (tzpp != null) {
					try {
						tzpp.sortUsingComparator(__tzPeriodComparator);
					} catch (NSComparator.ComparisonException e) {
						throw NSForwardException._runtimeExceptionForThrowable(e);
					}
				}
			}
		}

		return tzpp;
	}

	private static String __replacementTimeZoneNameForName(String aName, boolean tryAbbreviation) {
		String abbrName = null;
		String aliasName = null;
		String result = null;

		if (tryAbbreviation) {
			abbrName = (aName == null) ? null : __abbreviations.objectForKey(aName);
		}

		aliasName = (abbrName == null && aName != null) ? (String) __aliases.objectForKey(aName) : null;

		if (abbrName == null) {
			result = (aliasName == null) ? aName : aliasName;
		}
		else {
			result = abbrName;
		}

		return result;
	}

	// public class methods

	/**
	 * Provides the mappings of time zone abbreviations to time zone names.
	 * <P>
	 * Any given name may be represented by several abbreviations. Conversely, more than one time zone may have the same abbreviation. For example,
	 * US/Pacific and Canada/Pacific both use the abbreviation "PST." In these cases, this method always chooses a single name to which to map the
	 * abbreviation.
	 * 
	 * @return the mappings of abbreviations to names. Never <CODE>null</CODE>.
	 * @see #getID
	 * @see #abbreviation
	 * @see #abbreviationForTimestamp
	 */
	public static NSDictionary<String, String> abbreviationDictionary() {
		return __abbreviations;
	}

	public Class classForCoder() {
		return NSTimeZone._CLASS;
	}

	/**
	 * Since NSTimeZones are immutable, there's no need to make an actual clone.
	 * 
	 * @return a reference to this same NSTimeZone object
	 */
	@Override
	public Object clone() {
		return this;
	}

	/**
	 * Provided for compliance with <CODE>com.webobjects.foundation.NSCoding</CODE>. Provides an NSTimeZone object for the decoded data. Attempts to
	 * avoid creating duplicate objects.
	 * 
	 * @param aDecoder the NSCoder object used to decode this NSTimeZone object
	 * @return the decoded NSTimeZone object
	 * @see #encodeWithCoder
	 * @see com.webobjects.foundation.NSCoding
	 */
	public static Object decodeObject(NSCoder aDecoder) {
		Object name = aDecoder.decodeObject();
		Object data = aDecoder.decodeObject();
		Object result = null;

		if (name == null || data == null) {
			throw new IllegalStateException("Unable to decode object.");
		}
		else {
			if (name instanceof String) {
				result = timeZoneWithName((String) name, true);

				if (result == null || !((NSTimeZone) result)._data.equals(data)) {
					if (data instanceof NSData) {
						result = timeZoneWithNameAndData((String) name, (NSData) data);
					}
				}
			}
		}

		return result;
	}

	/**
	 * Provides the last object given to NSTimeZone via <CODE>setDefault</CODE> or <CODE>setDefaultTimeZone</CODE>.If no default time zone has been
	 * explicitly set, this method returns the system time zone. The default time zone returned by NSTimeZone may or may not be consistent with the
	 * default time zone returned by <CODE>java.util.TimeZone</CODE>.
	 * <P>
	 * 
	 * @return the default time zone.
	 * @see #getDefault
	 * @see #setDefault
	 * @see #setDefaultTimeZone
	 */
	public static synchronized NSTimeZone defaultTimeZone() {
		if (__defaultTimeZone == null) {
			__defaultTimeZone = systemTimeZone();
		}

		return __defaultTimeZone;
	}

	/**
	 * Provides a list of all of the names and abbreviations known to NSTimeZone.
	 * 
	 * @return the Java array of all names and abbreviations. Never <CODE>null</CODE>.
	 * @see #knownTimeZoneNames
	 */
	public static String[] getAvailableIDs() {
		NSArray kTZNArray = __knownTimeZoneNames.allObjects();
		int count = kTZNArray.count();
		String[] availableIDs = new String[count];

		// We have to explicitly copy over each __knownTimeZoneNames element, because Java is unable to do something as simple as cast (Object[]) to (String[]).

		for (int i = 0; i < count; i++) {
			availableIDs[i] = (String) kTZNArray.objectAtIndex(i);
		}

		return availableIDs;
	}

	/**
	 * Returns an NSTimeZone object representing GMT.
	 * 
	 * @return an instance of NSTimeZone representing the GMT time zone.
	 */
	public static NSTimeZone getGMT() {
		return __gmt;
	}

	/**
	 * Provides the last object given to NSTimeZone via <CODE>setDefault</CODE> or <CODE>setDefaultTimeZone</CODE>. If no default time zone has been
	 * explicitly set, this method returns the system time zone. The default time zone returned by NSTimeZone may or may not be consistent with the
	 * default time zone returned by <CODE>java.util.TimeZone</CODE>.
	 * <P>
	 * 
	 * @return the default time zone. Never <CODE>null</CODE>.
	 * @see #defaultTimeZone
	 * @see #localTimeZone
	 * @see #setDefault
	 * @see #systemTimeZone
	 */
	public static TimeZone getDefault() {
		return defaultTimeZone();
	}

	/**
	 * Provides a list of all of the names and abbreviations known to NSTimeZone.
	 * 
	 * @return the NSArray of Strings representing all of the names and abbreviations
	 * @see #getAvailableIDs
	 */
	public static NSArray knownTimeZoneNames() {
		return __knownTimeZoneNames.allObjects();
	}

	// Returns an object that forwards all messages to the default time zone for your application.
	// This behavior is particularly useful for time-value objects (e.g. NSTimestamp) that are
	// archived or distributed across a network and may be interpreted in different locales.

	/**
	 * Provides an object that forwards all messages to the default time zone in the current locale for the application. This behavior is particularly
	 * useful for NSTimestamp objects that are archived or distributed across a network. Such objects may be used in different locales than the one in
	 * which they were created.
	 * 
	 * @return a special object that forwards all messages to the time zone provided by <CODE>getDefault</CODE>. Never <CODE>null</CODE>.
	 * @see #defaultTimeZone
	 * @deprecated
	 */
	@Deprecated
	public static NSTimeZone localTimeZone() {
		return __localTimeZone;
	}

	/**
	 * This method provided for backward compatibility only. Sets the system time zone to <CODE>null</CODE> and posts an
	 * <CODE>NSTimeZone.SystemTimeZoneDidChangeNotification</CODE>.
	 * 
	 * @see #systemTimeZone
	 * @deprecated
	 */
	@Deprecated
	public static synchronized void resetSystemTimeZone() {
		NSNotificationCenter.defaultCenter().postNotification(SystemTimeZoneDidChangeNotification, null, null);
		__systemTimeZone = null;
	}

	/**
	 * Changes the time zone provided by <CODE>getDefault</CODE> or <CODE>defaultTimeZone</CODE> for the application. The default time zone is not the
	 * same as the <CODE>localTimeZone</CODE>, which cannot be substituted.
	 * 
	 * @see #getDefault
	 * @see #localTimeZone
	 * @param aTZ the proposed default time zone. Must be an instance of NSTimeZone.
	 */
	public static synchronized void setDefault(TimeZone aTZ) {
		if (aTZ instanceof NSTimeZone) {
			setDefaultTimeZone((NSTimeZone) aTZ);
		}
		else {
			setDefaultTimeZone(_nstimeZoneWithTimeZone(aTZ));
		}
	}

	/**
	 * Changes the time zone provided by <CODE>getDefault</CODE> or <CODE>defaultTimeZone</CODE> for the application. The default time zone is not the
	 * same as the <CODE>localTimeZone</CODE>, which cannot be substituted.
	 * 
	 * @see #defaultTimeZone
	 * @see #setDefault
	 * @see #localTimeZone
	 * @param aTZ the proposed default time zone. Must be an instance of NSTimeZone.
	 */
	public static synchronized void setDefaultTimeZone(NSTimeZone aTZ) {
		if (aTZ instanceof __NSLocalTimeZone) {
			throw new IllegalArgumentException("Cannot set default timezone to a localTimeZone object");
		}
		__defaultTimeZone = aTZ;
	}

	/**
	 * Irrelevant for NSTimeZone objects, which are not mutable.
	 */
	@Override
	public void setID(String anID) {
		throw new IllegalStateException(_CLASS.getName() + " objects are not mutable.");
	}

	/**
	 * Irrelevant for NSTimeZone objects, which are not mutable.
	 */
	@Override
	public void setRawOffset(int offsetMillis) {
		throw new IllegalStateException(_CLASS.getName() + " objects are not mutable.");
	}

	/**
	 * Provides the time zone object representing the system's time zone. This is different from <CODE>localTimeZone</CODE> and may be different from
	 * the time zone object returned by <CODE>getDefault</CODE>.
	 * 
	 * @return the time zone currently used by the system or the GMT time zone if the current time zone can't be determined
	 * @see #getDefault
	 * @see #localTimeZone
	 * @see #resetSystemTimeZone
	 */
	public static synchronized NSTimeZone systemTimeZone() {
		if (__systemTimeZone == null) {
			String javaTimeZoneName = null;

			// get the time zone ID from the system properties
			// if the app is running in a restricted environment, this will throw.
			try {
				javaTimeZoneName = AccessController.doPrivileged(new GetPropertyAction("user.timezone"));
			} catch (Exception e) {
				// swallow the security exception so we can do something useful.
			}

			// if user.timezone isn't set, get the TimeZone class to set it
			if (javaTimeZoneName == null || javaTimeZoneName.length() == 0) {
				javaTimeZoneName = TimeZone.getDefault().getID();

				// if that didn't work use GMT
				if (javaTimeZoneName == null || javaTimeZoneName.length() == 0) {
					__systemTimeZone = __gmt;
					NSLog.err.appendln("Couldn't find the system timezone (no java default). Using GMT.");
				}
			}
			__systemTimeZone = timeZoneWithName(javaTimeZoneName, true);

			if (__systemTimeZone == null) {
				__systemTimeZone = __gmt;
				NSLog.err.appendln("Couldn't find the system timezone. Using GMT.");
			}
		}

		return __systemTimeZone;
	}

	// If the offset equates to a whole hour between -12 and +12, return the corresponding
	// Etc/GMT time zone. N.B.: The Etc/GMT zones use the POSIX convention of minutes-west-of-GMT,
	// i.e., the offset -28800 corresponds to Etc/GMT+8, offset 3600 corresponds to Etc/GMT-1.
	//
	// If the offset equates to a partial hour, or is > 12 or < -12, a fixed time zone is concocted
	// from the offset, rounded to the nearest minute, and named with the convention
	// "GMT<+-> <hh>:<mm>", where the sign of the concocted name IS THE SAME AS the sign of the offset,
	// that is, the name follows the older, non-POSIX convention. For example, the offset 4200 will produce
	// a time zone named "GMT+01:10". This is for consistency with Java, where GMT+8 has an offset of 28800
	// and GMT-8 has an offset of -28800.

	/**
	 * Provides an NSTimeZone object representing a fixed offset, specified in seconds, from Greenwich Mean Time. If <CODE>secondsOffsetFromGMT</CODE>
	 * corresponds to a whole hour between 12 and -12, this method returns an existing, well-known NSTimeZone object that follows the POSIX
	 * convention. The time zone name is "Etc/GMT<+/-><hour>", where "hour" is a whole number 1-12 and the sign (plus or minus) is the <I>opposite</I>
	 * of the sign of the offset. An offset of zero is a special case, for which the time zone name is simply "Etc/GMT".
	 * <P>
	 * If <CODE>secondsOffsetFromGMT</CODE> does not correspond to a whole hour between 12 and -12, this method creates a new time zone for the
	 * specified offset, rounded to the nearest minute. In this case, the time zone name is consistent with the Java convention rather than the POSIX
	 * convention. The time zone name is "GMT<+/-><hour>:<minute>", where the hour is always represented by two digits, and where the sign (plus or
	 * minus) is the <I>same</I> as the sign of the offset. Time zones created from offsets are always apolitical. They never use daylight savings
	 * time, and their offsets are constant throughout the year, and throughout history.
	 * <P>
	 * 
	 * @param secondsOffsetFromGMT the offset from Greenwich Mean Time in seconds to use in finding an existing, or creating an new, apolitical
	 *            NSTimeZone object.
	 * @return the corresponding time zone object.
	 * @see #timeZoneWithName
	 * @see #getID
	 */
	public static synchronized NSTimeZone timeZoneForSecondsFromGMT(int secondsOffsetFromGMT) {
		NSTimeZone result = null;

		if (secondsOffsetFromGMT == 0) {
			result = __gmt;
		}
		else {
			StringBuffer abbrev = null;
			int hour = secondsOffsetFromGMT / 3600;

			if ((secondsOffsetFromGMT % 3600) != 0 || 12 < java.lang.Math.abs(hour)) {
				int minutes = 0;
				if (0 < secondsOffsetFromGMT) {
					minutes = (secondsOffsetFromGMT - hour * 3600 + 30) / 60;
					abbrev = new StringBuffer(__GMT).append(__PLUS).append(__gmtHourFormatter.stringForObjectValue(_NSUtilities.IntegerForInt(hour))).append(
							__gmtMinuteFormatter.stringForObjectValue(_NSUtilities.IntegerForInt(minutes)));
				}
				else {
					minutes = (secondsOffsetFromGMT - hour * 3600 - 30) / 60;
					abbrev = new StringBuffer(__GMT).append(__MINUS).append(__gmtHourFormatter.stringForObjectValue(_NSUtilities.IntegerForInt(java.lang.Math.abs(hour)))).append(
							__gmtMinuteFormatter.stringForObjectValue(_NSUtilities.IntegerForInt(java.lang.Math.abs(minutes))));
				}

				result = __concoctFixedTimeZone(hour * 3600 + minutes * 60, abbrev.toString(), 0);
			}
			else {
				result = NSTimeZone.timeZoneWithName(("Etc/GMT" + (0 < secondsOffsetFromGMT ? __MINUS : __PLUS) + __hourFormatter.stringForObjectValue(_NSUtilities.IntegerForInt(java.lang.Math
						.abs(hour)))), true);
			}
		}

		return result;
	}

	/**
	 * Provides an NSTimeZone object corresponding to <CODE>aName</CODE>. Avoids creating duplicate NSTimeZone objects as much as possible. This
	 * method is the recommended way for you to create or retrieve NSTimeZone objects.
	 * <P>
	 * Invoked by:
	 * <UL>
	 * <LI><CODE>decodeObject</CODE></LI>
	 * <LI><CODE>readResolve</CODE></LI>
	 * <LI><CODE>systemTimeZone</CODE></LI>
	 * <LI><CODE>timeZoneForSecondsFromGMT</CODE></LI>
	 * </UL>
	 * 
	 * @param aName the name or abbreviation used to search for an existing NSTimeZone object. Should never be <CODE>null</CODE>.
	 * @param tryAbbreviation <CODE>true</CODE> to match <CODE>aName</CODE> against the <CODE>abbreviationDictionary</CODE>, <CODE>false</CODE>
	 *            otherwise
	 * @return an NSTimeZone object identified by <CODE>aName</CODE>, <CODE>null</CODE> if there is no match
	 * @see #abbreviation
	 * @see #abbreviationDictionary
	 * @see #abbreviationForTimestamp
	 * @see #decodeObject
	 * @see #getID
	 * @see #knownTimeZoneNames
	 * @see #readResolve
	 * @see #systemTimeZone
	 * @see #timeZoneForSecondsFromGMT
	 */
	public static synchronized NSTimeZone timeZoneWithName(String aName, boolean tryAbbreviation) {
		NSTimeZone result = null;
		String tzName;

		if (aName == null) {
			throw new IllegalArgumentException("String parameter must be non-null");
		}
		else {
			tzName = __replacementTimeZoneNameForName(aName, tryAbbreviation);
			if (tzName == null) {
				NSLog.debug.appendln("Cannot find NSTimeZone with name " + aName);
				return null;
			}
			result = __lookupOrCreateTimeZone(tzName);
		}

		return result;
	}

	/**
	 * Use <CODE>timeZoneWithName</CODE> instead of calling this method directly.
	 * <P>
	 * Invoked by:
	 * <UL>
	 * <LI><CODE>decodeObject</CODE></LI>
	 * <LI><CODE>readResolve</CODE></LI>
	 * <LI><CODE>timeZoneForSecondsFromGMT</CODE></LI>
	 * </UL>
	 * 
	 * @param aName the proposed name of the new NSTimeZone object. Should never be <CODE>null</CODE>. May be coerced into another name, if there's a
	 *            preferred name for the time zone identified.
	 * @param aData the proposed behavior (represented by raw binary data) for the new NSTimeZone object. Should never be <CODE>null</CODE>.
	 * @return a new NSTimeZone object, which may be a duplicate of an existing NSTimeZone object
	 * @see #data
	 * @see #decodeObject
	 * @see #getID
	 * @see #readResolve
	 * @see #timeZoneForSecondsFromGMT
	 * @see #timeZoneWithName
	 */
	public static synchronized NSTimeZone timeZoneWithNameAndData(String aName, NSData aData) {
		NSTimeZone result = null;
		String tzName;

		if (aName == null || aData == null) {
			throw new IllegalArgumentException("Both parameters must be non-null");
		}
		else {
			tzName = __replacementTimeZoneNameForName(aName, false);
			result = new NSTimeZone(tzName, aData);
		}

		return result;
	}

	public static NSTimeZone _nstimeZoneWithTimeZone(TimeZone aZone) {
		NSTimeZone result = null;

		if (aZone instanceof NSTimeZone) {
			result = (NSTimeZone) aZone;
		}
		else {
			result = timeZoneWithName(aZone.getID(), true);
			// if that didn't work, use the offset.
			if (result == null) {
				result = timeZoneForSecondsFromGMT(aZone.getRawOffset() / 1000);
				if (result == null) {
					throw new IllegalArgumentException("Can not construct an NSTimeZone for zone: " + aZone.toString());
				}
			}
		}
		return result;
	}

	// private instance methods

	private synchronized void _initialize() {
		if (!_initialized) {
			int i;

			_timeZonePeriods = __parseTimeZoneData(_data);
			_timeZonePeriodsCount = _timeZonePeriods.count();

			for (i = _timeZonePeriodsCount - 1; i >= 0; i--) {
				if (_timeZonePeriods.objectAtIndex(i)._isdst > 0) {
					_useDaylightTime = true;
					break;
				}
			}

			// Set _rawOffset set to something sensible. Many timezones don't have one all-seeing, all-knowing raw offset associated with them,
			// as the offset can change over time (not just for daylight saving purposes, either). We have to support getRawOffset() to
			// subclass java.util.TimeZone.
			// Consequently, _rawOffset will be zero except for generic, apolitical timezones of the form GMT+/-hrs or Etc/GMT+/-hrs.
			// In those cases, _rawOffset will be numHrs * 3600 * 1000 (the latter so that the offset is in milliseconds).

			if (_name.startsWith(__GMT)) {
				if (_name.length() > __GMT_LENGTH + 1) {
					int seconds = 0;
					switch (_name.charAt(__GMT_LENGTH)) {
					case '+':
						seconds = new Integer(_name.substring(__GMT_LENGTH + 1, __GMT_LENGTH + 3)).intValue() * 3600 + new Integer(_name.substring(__GMT_LENGTH + 4, __GMT_LENGTH + 6)).intValue()
								* 60;
						break;
					case '-':
						seconds = (new Integer(_name.substring(__GMT_LENGTH + 1, __GMT_LENGTH + 3)).intValue() * 3600 + new Integer(_name.substring(__GMT_LENGTH + 4, __GMT_LENGTH + 6)).intValue() * 60)
								* -1;
						break;
					default:
						break;
					}
					_rawOffset = seconds * 1000;
				}
			}

			// Here we take only names with the form Etc/GMT+/-<whole hour>; the Etc/GMT names are POSIX,
			// so the sign of the offset is the opposite of the sign in the name string!!
			if (_name.startsWith(__GENERIC_TZ_NAME_STEM)) {
				int stemLength = __GENERIC_TZ_NAME_STEM.length();
				int nameLength = _name.length();
				if (nameLength > stemLength + 1) {
					int seconds = 0;
					switch (_name.charAt(stemLength)) {
					case '+':
						seconds = (new Integer(_name.substring(stemLength + 1, nameLength)).intValue() * 3600) * -1;
						break;
					case '-':
						seconds = new Integer(_name.substring(stemLength + 1, nameLength)).intValue() * 3600;
						break;
					default:
						break;
					}
					_rawOffset = seconds * 1000;
				}
			}
			_initialized = true;
		}
	}

	// public instance methods

	/**
	 * Returns the abbreviation for the NSTimeZone at the current instant, such as <CODE>"EDT"</CODE> (Eastern Daylight Time). Invokes
	 * <CODE>abbreviationForTimestamp</CODE> with an NSTimestamp representing the current instant.
	 * <P>
	 * Invoked by <CODE>toString</CODE>.
	 * 
	 * @return the abbreviation for the time zone at the current instant.
	 * @see #abbreviationDictionary
	 * @see #abbreviationForTimestamp
	 * @see #toString
	 */
	public String abbreviation() {
		return abbreviationForTimestamp(new NSTimestamp());
	}

	/**
	 * Provides the abbreviation for the NSTimeZone at the instant specified by <CODE>aTimestamp</CODE>.
	 * <P>
	 * Note that the abbreviation may be different at different instants. For example, during daylight savings time, the US/Eastern time zone has an
	 * abbreviation of <CODE>"EDT"</CODE>. At other times, its abbreviation is <CODE>"EST"</CODE>.
	 * <P>
	 * Invoked by <CODE>abbreviation</CODE>.
	 * 
	 * @param aTimestamp the specified instant
	 * @return the abbreviation for the NSTimeZone at <CODE>aTimestamp</CODE>. Never <CODE>null</CODE>.
	 * @see #abbreviation
	 * @see #abbreviationDictionary
	 */
	public String abbreviationForTimestamp(NSTimestamp aTimestamp) {
		int idx;
		String result = null;

		if (!_initialized) {
			_initialize();
		}

		idx = __bSearchTZPeriods(_timeZonePeriods, _timeZonePeriodsCount, aTimestamp.getTime() / 1000L);

		if (_timeZonePeriodsCount < idx) {
			idx = _timeZonePeriodsCount;
		}
		else if (idx == 0) {
			idx = 1;
		}

		result = _timeZonePeriods.objectAtIndex(idx - 1)._abbreviation;

		// The following abbreviation haquerie is intended to reconcile the POSIX convention used by the underlying
		// time zone data to represent whole-hour absolute offsets from GMT with the convention used by Java's
		// SimpleTimeZone and formerly (pre-1990s) used by the U.S. government agency which publishes time zone data.
		//
		// Thus, the offset -28800 is associated in Java with the ID "GMT-8"(non-POSIX), and in NSTimeZone with the
		// time zone name "Etc/GMT+8" (POSIX) and abbreviation "GMT-8" (non-POSIX).
		//
		// The offset 28800 is associated in Java with the ID "GMT+8" (non-POSIX), and in NSTimeZone with the
		// time zone name "Etc/GMT-8" (POSIX) and abbreviation "GMT+8" (non-POSIX).

		if (result.startsWith(__GMT)) {
			if (result.length() > __GMT_LENGTH + 1) {
				String nonPOSIXabbr = null;
				switch (result.charAt(__GMT_LENGTH)) {
				case '+':
					nonPOSIXabbr = (__GMT + __MINUS + result.substring(__GMT_LENGTH + 1));
					break;
				case '-':
					nonPOSIXabbr = (__GMT + __PLUS + result.substring(__GMT_LENGTH + 1));
				}
				result = nonPOSIXabbr;
			}
		}

		return result;
	}

	/**
	 * Provides an opaque object representing the behavior for this NSTimeZone. Invoked by <CODE>equals</CODE>.
	 * 
	 * @return raw binary data that represents the NSTimeZone's behavior. Never <CODE>null</CODE>.
	 * @see #equals
	 */
	public NSData data() {
		return _data;
	}

	public void encodeWithCoder(NSCoder aCoder) {
		aCoder.encodeObject(_name);
		aCoder.encodeObject(_data);
	}

	/**
	 * Indicates whether this NSTimeZone has the same <CODE>data</CODE> as that of <CODE>anObject</CODE>. Invoked by:
	 * <UL>
	 * <LI><CODE>hasSameRules</CODE></LI>
	 * <LI><CODE>isEqualToTimeZone</CODE></LI>
	 * </UL>
	 * 
	 * @param anObject another NSTimeZone object against which to compare this NSTimeZone.
	 * @return <CODE>false</CODE> if they have different <CODE>data</CODE> or if <CODE>anObject</CODE> is not an instance of NSTimeZone,
	 *         <CODE>true</CODE> otherwise
	 * @see #data
	 * @see #hasSameRules
	 * @see #isEqualToTimeZone
	 */
	@Override
	public boolean equals(Object anObject) {
		return (anObject instanceof NSTimeZone) ? _data.equals(((NSTimeZone) anObject).data()) : false;
	}

	@Override
	public String getDisplayName(boolean inDaylightSavingTime, int aTZStyle, Locale aLocale) {
		return _name;
	}

	@Override
	public String getID() {
		return _name;
	}

	@Override
	public int getOffset(int anEra, int aYear, int aMonth, int aDayOfMonth, int aDayOfWeek, int milliseconds) {
		// get a calendar instance (don't think we have to worry about locale here)
		// set the calendar to UTC as timezone, then set all of these things, too (maybe "null" values don't need to be set)

		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(__GMT));
		int timeUnits = milliseconds;

		cal.clear();

		cal.set(Calendar.ERA, anEra);
		cal.set(aYear, aMonth, aDayOfMonth);
		cal.set(Calendar.DAY_OF_WEEK, aDayOfWeek);
		cal.set(Calendar.MILLISECOND, timeUnits % 1000);
		timeUnits /= 1000;
		cal.set(Calendar.SECOND, timeUnits % 60);
		timeUnits /= 60;
		cal.set(Calendar.MINUTE, timeUnits % 60);
		timeUnits /= 60;
		if (timeUnits > 23) {
			throw new IllegalArgumentException("too many milliseconds for a single day" + milliseconds);
		}
		else {
			cal.set(Calendar.HOUR_OF_DAY, timeUnits);
		}

		// Use cal.getTime() to get a Date, then make an NSTimestamp using the Date.
		// Return the result of secondsFromGMTForTimestamp() using the new NSTimestamp (and multiply by 1000 to produce milliseconds).

		return secondsFromGMTForTimestamp(new NSTimestamp(cal.getTime())) * 1000;
	}

	/**
	 * Used internally by other classes in the <CODE>com.webobjects.foundation</CODE> package and should be treated as private.
	 * 
	 * @param ts the specified instant
	 * @return the offset in milliseconds at <CODE>ts</CODE>
	 */
	int getOffset(NSTimestamp ts) {
		return secondsFromGMTForTimestamp(ts) * 1000;
	}

	/**
	 * Provides the generic difference between the NSTimeZone and Greenwich Mean Time (the offset).
	 * <P>
	 * Because NSTimeZones represent geopolitical regions, many timezones don't have one, generic offset. The offset can change over time for reasons
	 * other than daylight saving purposes. Consequently, this method will return zero except for generic, apolitical time zones of the form GMT+/-hrs
	 * or Etc/GMT+/-hrs, like those generated by <CODE>timeZoneForSecondsFromGMT</CODE>.
	 * 
	 * @return the offset in milliseconds for apolitical time zones, zero otherwise
	 * @see #timeZoneForSecondsFromGMT
	 */
	@Override
	public int getRawOffset() {
		if (!_initialized) {
			_initialize();
		}
		return _rawOffset;
	}

	@Override
	public synchronized int hashCode() {
		if (_hashCode == 0) {
			_hashCode = _data.hashCode();
		}

		return _hashCode;
	}

	/**
	 * Invokes <CODE>equals</CODE> with <CODE>aTZ</CODE>.
	 * 
	 * @param aTZ another NSTimeZone object against which to compare this NSTimeZone
	 * @return <CODE>true</CODE> if <CODE>aTimeZone</CODE> and this NSTimeZone have the same <CODE>data</CODE>, <CODE>false</CODE> otherwise
	 * @see #data
	 * @see #equals
	 */
	@Override
	public boolean hasSameRules(TimeZone aTZ) {
		return equals(aTZ);
	}

	/**
	 * Indicates whether this NSTimeZone uses daylight savings time at <CODE>aDate</CODE>.
	 * <P>
	 * Invokes <CODE>isDaylightSavingTimeForTimestamp</CODE> with an NSTimestamp representing <CODE>aDate</CODE>.
	 * 
	 * @param aDate the specified instant
	 * @return <CODE>true</CODE> if the NSTimeZone uses daylight savings time at the specified instant, <CODE>false</CODE> otherwise
	 * @see #isDaylightSavingTime
	 * @see #isDaylightSavingTimeForTimestamp
	 */
	@Override
	public boolean inDaylightTime(Date aDate) {
		return isDaylightSavingTimeForTimestamp(new NSTimestamp(aDate));
	}

	/**
	 * Indicates whether this NSTimeZone currently uses daylight savings time. Invokes <CODE>isDaylightSavingTimeForTimestamp</CODE> with an
	 * NSTimestamp representing the current instant.
	 * 
	 * @return <CODE>true</CODE> if the NSTimeZone is currently using daylight savings time, <CODE>false</CODE> otherwise
	 * @see #inDaylightTime
	 * @see #isDaylightSavingTimeForTimestamp
	 */
	public boolean isDaylightSavingTime() {
		return isDaylightSavingTimeForTimestamp(new NSTimestamp());
	}

	/**
	 * Indicates whether this NSTimeZone uses daylight savings time at <CODE>aTimestamp</CODE>.
	 * <P>
	 * Invoked by:
	 * <UL>
	 * <LI><CODE>inDaylightTime</CODE></LI>
	 * <LI><CODE>isDaylightSavingTime</CODE></LI>
	 * </UL>
	 * 
	 * @param aTimestamp the specified instant
	 * @return <CODE>true</CODE> if the NSTimeZone uses daylight savings time at the specified instant, <CODE>false</CODE> otherwise
	 * @see #inDaylightTime
	 * @see #isDaylightSavingTime
	 */
	public boolean isDaylightSavingTimeForTimestamp(NSTimestamp aTimestamp) {
		int idx;
		boolean result = false;

		if (!_initialized) {
			_initialize();
		}

		idx = __bSearchTZPeriods(_timeZonePeriods, _timeZonePeriodsCount, aTimestamp.getTime() / 1000L);

		if (_timeZonePeriodsCount < idx) {
			idx = _timeZonePeriodsCount;
		}
		else if (idx == 0) {
			idx = 1;
		}

		result = (_timeZonePeriods.objectAtIndex(idx - 1)._isdst == 0) ? false : true;

		return result;
	}

	/**
	 * Invokes <CODE>equals</CODE> with <CODE>aTimeZone</CODE>.
	 * 
	 * @param aTimeZone another NSTimeZone object against which to compare this NSTimeZone
	 * @return <CODE>true</CODE> if <CODE>aTimeZone</CODE> and this NSTimeZone have the same <CODE>data</CODE>, <CODE>false</CODE> otherwise
	 * @see #data
	 * @see #equals
	 * @deprecated Use <CODE>equals</CODE>.
	 */
	@Deprecated
	public boolean isEqualToTimeZone(NSTimeZone aTimeZone) {
		return equals(aTimeZone);
	}

	/**
	 * Invokes <CODE>getID</CODE>.
	 * 
	 * @return the name that identifies the geopolitical region that the NSTimeZone represents
	 * @see #getID
	 * @deprecated Use <CODE>getID</CODE>.
	 */
	@Deprecated
	public String name() {
		return getID();
	}

	/**
	 * Indicates the difference between the NSTimeZone and Greenwich Mean Time (the offset) at the current instant.
	 * 
	 * @return current offset in seconds
	 */
	public int secondsFromGMT() {
		int result = secondsFromGMTForTimestamp(new NSTimestamp());
		return result;
	}

	/**
	 * Indicates the difference between the NSTimeZone and Greenwich Mean Time (the offset) at the instant specified by <CODE>aTimestamp</CODE>.
	 * <P>
	 * The NSTimeZone object may change its offset from GMT at different points in the year. For example, the U.S. time zones change offsets with
	 * daylight savings time.
	 * <P>
	 * Because NSTimeZone objects represent geopolitical regions, offsets such as those for daylight savings time may change across different spans.
	 * For example, the organization which governs a region may abolish daylight savings time, though daylight savings time would be relevant for
	 * moments preceding the abolition.
	 * 
	 * @param aTimestamp an instant in time
	 * @return offset in seconds at the instant specified by <CODE>aTimestamp</CODE>
	 */
	public int secondsFromGMTForTimestamp(NSTimestamp aTimestamp) {
		return secondsFromGMTForOffsetInSeconds(aTimestamp.getTime() / 1000L);
	}

	int secondsFromGMTForOffsetInSeconds(long offset) {
		int idx;
		int result = 0;

		if (!_initialized) {
			_initialize();
		}

		idx = __bSearchTZPeriods(_timeZonePeriods, _timeZonePeriodsCount, offset);

		if (_timeZonePeriodsCount < idx) {
			idx = _timeZonePeriodsCount;
		}
		else if (idx == 0) {
			idx = 1;
		}

		result = _timeZonePeriods.objectAtIndex(idx - 1)._offset;

		return result;
	}

	@Override
	public String toString() {
		if (!_initialized) {
			_initialize();
		}

		return _name + " (" + abbreviation() + ") offset " + secondsFromGMT() + (isDaylightSavingTime() ? " (Daylight)" : "");
	}

	/**
	 * Indicates whether the NSTimeZone ever uses or has used daylight savings time. Only ever <CODE>true</CODE> for objects that represent
	 * geopolitical regions.
	 * 
	 * @return <CODE>true</CODE> if the time zone ever uses or has used daylight savings time, <CODE>false</CODE> otherwise
	 * @see #timeZoneForSecondsFromGMT
	 */
	@Override
	public boolean useDaylightTime() {
		if (!_initialized) {
			_initialize();
		}

		return _useDaylightTime;
	}

	/* Serialization support */
	private static final ObjectStreamField[] serialPersistentFields = {
			new ObjectStreamField(SerializationNameFieldKey, _NSUtilities._StringClass),
			new ObjectStreamField(SerializationDataFieldKey, NSData._CLASS)
	};

	private void writeObject(ObjectOutputStream s) throws java.io.IOException {
		ObjectOutputStream.PutField fields = s.putFields();
		synchronized (this) {
			if (this instanceof __NSLocalTimeZone) {
				fields.put(SerializationNameFieldKey, null);
				fields.put(SerializationDataFieldKey, null);
			}
			else {
				fields.put(SerializationNameFieldKey, _name);
				fields.put(SerializationDataFieldKey, _data);
			}
		}
		s.writeFields();
	}

	private void readObject(ObjectInputStream s) throws java.io.IOException, ClassNotFoundException {
		ObjectInputStream.GetField fields = null;
		fields = s.readFields();

		_name = (String) fields.get(SerializationNameFieldKey, null);
		_data = (NSData) fields.get(SerializationDataFieldKey, null);
	}

	protected Object readResolve() throws java.io.ObjectStreamException {
		Object result = null;

		if (_name != null && _data != null) {
			result = timeZoneWithName(_name, true);

			if (result == null || !((NSTimeZone) result)._data.equals(_data)) {
				result = timeZoneWithNameAndData(_name, _data);
			}
			return result;
		}
		else {
			return this; // it should be the super class of an _NSLocalTimeZone which will replace it
		}
	}

	// private inner classes

	// Each time zone data file begins with ...
	//
	// tzh_reserved (byte [20]) reserved for future use
	// tzh_ttisgmtcnt (byte [4]) coded number of transition time flags
	// tzh_ttisstdcnt (byte [4]) coded number of transition time flags
	// tzh_leapcnt (byte [4]) coded number of leap seconds
	// tzh_timecnt (byte [4]) coded number of transition times
	// tzh_typecnt (byte [4]) coded number of local time types
	// tzh_charcnt (byte [4]) coded number of abbreviation chars
	//
	// ... followed by ...
	//
	// tzh_timecnt (byte [4])s coded transition times a la time(2)
	// tzh_timecnt (unsigned byte)s types of local time starting at above
	// tzh_typecnt repetitions of
	// one (byte [4]) coded GMT offset in seconds
	// one (unsigned byte) used to set tm_isdst
	// one (unsigned byte) that's an abbreviation list index
	// tzh_charcnt (byte)s '\0'-terminated zone abbreviations
	// tzh_leapcnt repetitions of
	// one (byte [4]) coded leap second transition times
	// one (byte [4]) total correction after above
	// tzh_ttisstdcnt (byte)s indexed by type:
	// if 1, transition time is standard time;
	// if 0, transition time is wall clock time;
	// if absent, transition times are assumed to be wall clock time
	// tzh_ttisgmtcnt (byte)s indexed by type:
	// if 1, transition time is GMT;
	// if 0, transition time is local time;
	// if absent, transition times are assumed to be local time

	protected static class __NSTZPeriod extends Object {
		protected String _abbreviation;

		protected int _isdst; // unsigned, 1 bit

		protected int _offset; // signed, 31 bits

		protected double _startTime;

		protected boolean before(__NSTZPeriod aNSTZP) {
			boolean result = false;

			if (_startTime < aNSTZP._startTime) {
				result = true;
			}

			return result;
		}

		protected boolean equals(__NSTZPeriod aNSTZP) {
			boolean result = false;

			if (_startTime == aNSTZP._startTime) {
				result = true;
			}

			return result;
		}
	}

	protected static class __NSTZPeriodComparator extends NSComparator {
		protected boolean _ascending;

		public __NSTZPeriodComparator() {
			this(true);
		}

		public __NSTZPeriodComparator(boolean ascending) {
			super();
			_ascending = ascending;
		}

		@Override
		public int compare(Object object1, Object object2) throws NSComparator.ComparisonException {
			if ((object1 == null) || (object2 == null) || (!(object1 instanceof __NSTZPeriod)) || (!(object2 instanceof __NSTZPeriod))) {
				throw new NSComparator.ComparisonException("Unable to compare objects.  Objects should be instance of class __NSTZPeriod.  Comparison was made with " + object1 + " and " + object2
						+ ".");
			}

			if ((object1 == object2) || (object1.equals(object2))) {
				return NSComparator.OrderedSame;
			}

			if (((__NSTZPeriod) object1).before((__NSTZPeriod) object2)) {
				return (_ascending) ? NSComparator.OrderedAscending : NSComparator.OrderedDescending;
			}

			return (_ascending) ? NSComparator.OrderedDescending : NSComparator.OrderedAscending;
		}
	}
}

final class __NSLocalTimeZone extends NSTimeZone {

	/**
	 * Mandatory SUID field for an evolvable Serializable class. serialVersionUID is gotten by doing the serialver command on the class.
	 */
	static final long serialVersionUID = -7337583402474807484L;

	// class constants

	public static final Class _CLASS = _NSUtilitiesExtra._classWithFullySpecifiedNamePrime("com.webobjects.foundation.__NSLocalTimeZone");

	private static final String __NSLOCALTIMEZONE_TOSTRING_1 = "Local Time Zone [";

	private static final String __NSLOCALTIMEZONE_TOSTRING_2 = "]";

	// constructors

	// This should only ever be called during deserialization/unexternalization.

	public __NSLocalTimeZone() {
		super();
	}

	// public class methods

	public static Object decodeObject(NSCoder aDecoder) {
		return NSTimeZone.localTimeZone();
	}

	// public instance methods

	@Override
	public String abbreviationForTimestamp(NSTimestamp aTimestamp) {
		return NSTimeZone.defaultTimeZone().abbreviationForTimestamp(aTimestamp);
	}

	@Override
	public Class classForCoder() {
		return __NSLocalTimeZone._CLASS;
	}

	@Override
	public Object clone() {
		return this;
	}

	@Override
	public NSData data() {
		return NSTimeZone.defaultTimeZone().data();
	}

	@Override
	public void encodeWithCoder(NSCoder encoder) {/* FIXME [PJYF Jan 11 2007] Is this correct? */
	}

	@Override
	public boolean equals(Object anObject) {
		return NSTimeZone.defaultTimeZone().equals(anObject);
	}

	@Override
	public String getDisplayName(boolean inDaylightSavingTime, int aTZStyle, Locale aLocale) {
		return NSTimeZone.defaultTimeZone().getDisplayName(inDaylightSavingTime, aTZStyle, aLocale);
	}

	@Override
	public String getID() {
		return NSTimeZone.defaultTimeZone().name();
	}

	@Override
	public int getRawOffset() {
		return NSTimeZone.defaultTimeZone().getRawOffset();
	}

	@Override
	public synchronized int hashCode() {
		return NSTimeZone.defaultTimeZone().hashCode();
	}

	@Override
	public boolean isDaylightSavingTimeForTimestamp(NSTimestamp aTimestamp) {
		return NSTimeZone.defaultTimeZone().isDaylightSavingTimeForTimestamp(aTimestamp);
	}

	@Override
	protected Object readResolve() throws java.io.ObjectStreamException {
		return NSTimeZone.localTimeZone();
	}

	@Override
	int secondsFromGMTForOffsetInSeconds(long offset) {
		return NSTimeZone.defaultTimeZone().secondsFromGMTForOffsetInSeconds(offset);
	}

	@Override
	public String toString() {
		return __NSLOCALTIMEZONE_TOSTRING_1 + NSTimeZone.defaultTimeZone().toString() + __NSLOCALTIMEZONE_TOSTRING_2;
	}

	@Override
	public boolean useDaylightTime() {
		return NSTimeZone.defaultTimeZone().useDaylightTime();
	}
}

/**
 * A convenience class for retrieving the string value of a system property as a privileged action. This class is directly copied from
 * sun.security.action.GetPropertyAction in order to avoid depending on the sun.security.action package.
 * <p>
 * An instance of this class can be used as the argument of <code>AccessController.doPrivileged</code>.
 * <p>
 * The following code retrieves the value of the system property named <code>"prop"</code> as a privileged action:
 * <p>
 * 
 * <pre>
 * String s = (String) java.security.AccessController.doPrivileged(new GetPropertyAction(&quot;prop&quot;));
 * </pre>
 * 
 * @author Roland Schemers
 * @author Ken Cavanaugh
 * @see java.security.PrivilegedAction
 * @see java.security.AccessController
 */

class GetPropertyAction implements java.security.PrivilegedAction<String> {
	private String theProp;
	private String defaultVal;

	/**
	 * Constructor that takes the name of the system property whose string value needs to be determined.
	 * 
	 * @param theProp the name of the system property.
	 */
	public GetPropertyAction(String theProp) {
		this.theProp = theProp;
	}

	/**
	 * Constructor that takes the name of the system property and the default value of that property.
	 * 
	 * @param theProp the name of the system property.
	 * @param defaulVal the default value.
	 */
	public GetPropertyAction(String theProp, String defaultVal) {
		this.theProp = theProp;
		this.defaultVal = defaultVal;
	}

	/**
	 * Determines the string value of the system property whose name was specified in the constructor.
	 * 
	 * @return the string value of the system property, or the default value if there is no property with that key.
	 */
	public String run() {
		String value = System.getProperty(theProp);
		return (value == null) ? defaultVal : value;
	}
}