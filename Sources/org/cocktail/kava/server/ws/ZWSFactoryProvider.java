/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit français et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les mêmes raisons,
 seule une responsabilite restreinte pèse sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  à l'utilisation,  à la modification et/ou au
 developpement et à la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe à 
 manipuler et qui le reserve donc à des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites à charger  et  tester  l'adequation  du
 logiciel à leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systèmes et ou de leurs donnees et, plus generalement, 
 à l'utiliser et l'exploiter dans les mêmes conditions de securite. 

 Le fait que vous puissiez acceder à cet en-tête signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.server.ws;

import java.util.Hashtable;

import app.server.Session;
import app.server.Version;

import com.webobjects.appserver.WOSession;
import com.webobjects.appserver.WOWebServiceUtilities;

public abstract class ZWSFactoryProvider {

	public ZWSFactoryProvider() {
		super();
		WOWebServiceUtilities.currentWOContext().response().setContentEncoding("UTF-8");
	}

	/**
	 * Recupère la session à partir du context.
	 * 
	 * @see WOWebServiceUtilities#currentWOContext
	 */
	protected Session getMySession() {
		if (WOWebServiceUtilities.currentWOContext() == null) {
			return null;
		}
		return (Session) WOWebServiceUtilities.currentWOContext().session();
	}

	/**
	 * Se deconnecter du webService (la session est fermee).
	 * 
	 * @see WOSession#terminate
	 */
	public void deconnect() {
		getMySession().terminate();
	}

	/**
	 * Cette methode doit être implementee. Doit verifier les infos d'identification de l'application appelante.
	 * 
	 */
	public abstract void connect(String applicationAlias, String wsPassword);

	/**
	 * 
	 * Cette methode doit être implementee et renvoyer le dernier message d'exception genere par le WS.
	 */
	public abstract String getLastExceptionMessage();

	/**
	 * Cette methode doit être implementee et retourner le n° de version du WebService.
	 */
	protected abstract String getWebServiceVersion();

	/**
	 * Renvoie differentes informations sur le web service (version, date, copyright, etc.).
	 * 
	 * @return
	 */
	public Hashtable getProperties() {
		Hashtable tmpProperties = new Hashtable();
		tmpProperties.put("VersionJRE", System.getProperty("java.version"));
		tmpProperties.put("Version Application", Version.appliVersion());
		tmpProperties.put("Version Service", getWebServiceVersion());
		tmpProperties.put("Date", Version.appliDate());
		tmpProperties.put("Copyright", Version.copyright());
		return tmpProperties;
	}
}
