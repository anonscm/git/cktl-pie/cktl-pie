/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit français et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les mêmes raisons,
 seule une responsabilite restreinte pèse sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  à l'utilisation,  à la modification et/ou au
 developpement et à la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe à 
 manipuler et qui le reserve donc à des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites à charger  et  tester  l'adequation  du
 logiciel à leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systèmes et ou de leurs donnees et, plus generalement, 
 à l'utiliser et l'exploiter dans les mêmes conditions de securite. 

 Le fait que vous puissiez acceder à cet en-tête signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.server.ws;

import org.cocktail.kava.server.metier.EOCatalogue;
import org.cocktail.kava.server.metier.EOCatalogueArticle;
import org.cocktail.kava.server.metier.EOFacturePapier;
import org.cocktail.kava.server.metier.EOFournisUlr;
import org.cocktail.kava.server.metier.EOIndividuUlr;
import org.cocktail.kava.server.metier.EOPersonne;
import org.cocktail.kava.server.metier.EOPrestation;
import org.cocktail.kava.server.metier.EOUtilisateur;

import app.server.Application;
import app.server.Session;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public abstract class ZWSFactory {

	protected Application		app;
	protected Session			session;
	protected EOEditingContext	ec;
	private boolean				authenticationOk;
	private String				wSPasswd;
	private WSZException		lastException;

	private String				appAlias;
	private String				appAliasLog;

	protected abstract String getWSPassword();

	public ZWSFactory(Session session) {
		super();
		setAuthenticationOk(false);
		app = (Application) WOApplication.application();
		this.session = session;
		ec = session.ec;
	}

	/**
	 * Permet de se connecter au webService.
	 * 
	 * @param applicationAlias
	 *            Alias de l'application qui utilise le WS
	 * @param wsPassword
	 *            Mot de passe necessaire pour pouvoir utiliser le WebService
	 */
	public final void connect(String applicationAlias, String wsPassword) {
		appAlias = applicationAlias;
		appAliasLog = "[" + appAlias + "] ";
		System.out.println(appAliasLog + "Tentative de connexion au Webservice " + this.getClass().getName());
		setLastException(null);
		try {
			checkAuthentication(wsPassword);
			System.out.println(appAliasLog + "Connexion au Webservice " + this.getClass().getName() + " reussie par " + appAlias);
		}
		catch (Exception e) {
			new WSZException(e.getMessage());
		}
	}

	/**
	 * Verification de l'authentification par le consumer. A appeler lors de toutes les operations.
	 * 
	 * @param passwd
	 */
	protected final void checkAuthentication(String passwd) throws Exception {
		wSPasswd = getWSPassword();
		if ((wSPasswd == null) || (wSPasswd.equals(""))) {
			throw new Exception("Le mot de passe du WebService n'est pas defini. Impossible d'offrir le webservice " + this.getClass().getName());
		}
		else {
			// if (!CRIpto.isSamePass(passwd, wSPasswd)) {
			// throw new Exception("Erreur de mot de passe lors d'une tentative d'utilisation du webservice " + this.getClass().getName());
			// }
			if (!wSPasswd.equals(passwd)) {
				throw new Exception("Erreur de mot de passe lors d'une tentative d'utilisation du webservice " + this.getClass().getName()
						+ "\nPassword defini pour le WebService: " + wSPasswd + "\nPassword utilise pour tenter la connexion: " + passwd + "\n");
			}
		}
		setAuthenticationOk(true);
	}

	/**
	 * Renvoie le dernier message d'exception genere par cet objet, ou null s'il n'y a pas eu d'exception. Cette methode est appelee par les
	 * consumers pour recuperer les exceptions cote serveur.
	 * 
	 * @see getLastException
	 */
	public final String getLastExceptionMessage() {
		if (lastException != null) {
			return lastException.getMessage();
		}
		else {
			return null;
		}
	}

	protected EOIndividuUlr findEOIndividuUlrByPersId(EOEditingContext ec, Number persId) throws Exception {
		EOIndividuUlr tmp = (EOIndividuUlr) fetchObject(ec, EOIndividuUlr.ENTITY_NAME, EOIndividuUlr.PERS_ID_KEY + " = %@", new NSArray(persId),
				null);
		if (tmp == null) {
			throw new Exception("Le persId fourni (" + persId + ") ne correspond pas à un individu connu.");
		}
		return tmp;
	}

	protected EOIndividuUlr findEOIndividuUlr(EOEditingContext ec, Integer noIndividu) throws Exception {
		return (EOIndividuUlr) EOUtilities.objectWithPrimaryKeyValue(ec, EOIndividuUlr.ENTITY_NAME, noIndividu);
	}

	protected EOPersonne findEOPersonne(EOEditingContext ec, Integer persId) throws Exception {
		return (EOPersonne) EOUtilities.objectWithPrimaryKeyValue(ec, EOPersonne.ENTITY_NAME, persId);
	}

	protected EOUtilisateur findEOUtilisateur(EOEditingContext ec, Integer utlOrdre) throws Exception {
		if (utlOrdre == null) {
			return null;
		}
		return (EOUtilisateur) EOUtilities.objectWithPrimaryKeyValue(ec, EOUtilisateur.ENTITY_NAME, utlOrdre);
	}

	protected EOFournisUlr findEOFournisUlr(EOEditingContext ec, Integer fouOrdre) throws Exception {
		return (EOFournisUlr) EOUtilities.objectWithPrimaryKeyValue(ec, EOFournisUlr.ENTITY_NAME, fouOrdre);
	}

	protected EOCatalogue findEOCatalogue(EOEditingContext ec, Integer catId) throws Exception {
		return (EOCatalogue) EOUtilities.objectWithPrimaryKeyValue(ec, EOCatalogue.ENTITY_NAME, catId);
	}

	protected EOCatalogueArticle findEOCatalogueArticle(EOEditingContext ec, Integer caarId) throws Exception {
		return (EOCatalogueArticle) EOUtilities.objectWithPrimaryKeyValue(ec, EOCatalogueArticle.ENTITY_NAME, caarId);
	}

	protected EOPrestation findEOPrestation(EOEditingContext ec, Integer prestId) throws Exception {
		return (EOPrestation) EOUtilities.objectWithPrimaryKeyValue(ec, EOPrestation.ENTITY_NAME, prestId);
	}

	protected EOFacturePapier findEOFacturePapier(EOEditingContext ec, Integer fapId) throws Exception {
		return (EOFacturePapier) EOUtilities.objectWithPrimaryKeyValue(ec, EOFacturePapier.ENTITY_NAME, fapId);
	}

	/**
	 * Renvoie la dernière exception.
	 */
	protected final WSZException getLastException() {
		return lastException;
	}

	protected final void setLastException(WSZException exception) {
		lastException = exception;
	}

	protected void trace(Object s) {
		System.out.println("[" + appAliasLog != null ? appAliasLog : "" + "] " + s.toString());
	}

	/**
	 * Exceptions internes à cette classe.
	 */
	protected class WSZException extends Exception {
		public WSZException(String arg0) {
			super(appAliasLog + arg0);
			System.out.println("Exception : " + appAliasLog + arg0);
			printStackTrace();
			setLastException(this);
		}

		public WSZException(Exception e) {
			super(appAliasLog + e.getMessage());
			System.out.println("Exception : " + appAliasLog + e.getMessage());
			e.printStackTrace();
			setLastException(this);
		}
	}

	/**
	 * Renvoie true si la methode checkAuthentication n'a pas genere d'exception (le mot de passe fourni est ok).
	 */
	protected final boolean isAuthenticationOk() {
		return authenticationOk;
	}

	protected final void setAuthenticationOk(boolean b) {
		authenticationOk = b;
	}

	protected final NSArray fetchArray(EOEditingContext ec, String entityName, String conditionStr, NSArray params, NSArray sortOrderings) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(conditionStr, params);
		EOFetchSpecification spec = new EOFetchSpecification(entityName, qual, sortOrderings, true, true, null);
		return ec.objectsWithFetchSpecification(spec);
	}

	protected final EOEnterpriseObject fetchObject(EOEditingContext ec, String entityName, String conditionStr, NSArray params,
			NSArray sortOrderings) {
		NSArray res = fetchArray(ec, entityName, conditionStr, params, sortOrderings);
		if ((res == null) || (res.count() == 0)) {
			return null;
		}
		else {
			return (EOEnterpriseObject) res.objectAtIndex(0);
		}
	}

	/**
	 * Verifie si isAuthenticationOk() renvoie true, sinon une exception est generee.
	 * 
	 * @throws Exception
	 */
	protected final void checkAuthenticated() throws Exception {
		if (!isAuthenticationOk()) {
			throw new Exception("L'authentification n'a pas ete reussie");
		}
	}

}