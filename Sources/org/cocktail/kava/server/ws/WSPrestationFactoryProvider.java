/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.server.ws;

import java.math.BigDecimal;
import java.util.Hashtable;

import com.webobjects.appserver.WOWebServiceUtilities;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;

public class WSPrestationFactoryProvider extends ZWSFactoryProvider {

	public static final String	WS_EXTERNAL_NAME	= "WSPrestation";
	public static final String	WS_PARAM			= "WS_PRESTATION";
	public static final String	WS_PASSWORD_PARAM	= "WS_PRESTATION_PASSWORD";

	private static final String	APP_VERSION			= "2.0.1";
	private static final String	APP_VER_NUM			= "201";
	private static final String	APP_VER_DATE		= "27/06/2007";

	public WSPrestationFactoryProvider() {
		super();
		WOWebServiceUtilities.currentWOContext().response().setContentEncoding("UTF-8");
	}

	/**
	 * Donne les infos sur le WebService
	 * 
	 * @return La chaine des parametres de version du WebService
	 */
	public String checkWebService() {
		String checkString = "ws.status=YES\n";
		checkString = checkString.concat("app.version=" + APP_VERSION + "\n");
		checkString = checkString.concat("app.ver.num=" + APP_VER_NUM + "\n");
		checkString = checkString.concat("app.ver.date=" + APP_VER_DATE + "\n");
		return checkString;
	}

	protected String getWebServiceVersion() {
		return APP_VERSION;
	}

	/**
	 * Renvoie l'instance de WSPrestationFactory a utiliser. Celle-ci est stockee dans la session, et permet de conserver une unique reference,
	 * car chaque appel d'une methode de webService genere une nouvelle instance de WSPrestationFactoryProvider.
	 * 
	 */
	private WSPrestationFactory getWSPrestationFactory() {
		if (getMySession().getCurrentWSPrestationFactory() == null) {
			getMySession().setCurrentWSPrestationFactory(new WSPrestationFactory(getMySession()));
		}
		return getMySession().getCurrentWSPrestationFactory();
	}

	/**
	 * Appelle {@link WSPrestationFactory#connect}
	 * 
	 */
	public void connect(String applicationAlias, String wsPassword) {
		getWSPrestationFactory().connect(applicationAlias, wsPassword);
	}

	/**
	 * Appelle {@link WSPrestationFactory#getLastExceptionMessage}
	 */
	public String getLastExceptionMessage() {
		return getWSPrestationFactory().getLastExceptionMessage();
	}

	/**
	 * Appelle {@link WSPrestationFactory#createCurrentDevis}
	 */
	public void createCurrentDevis(Integer utlOrdre, Integer typePublicKey, Integer fouOrdre, Integer contactPersId, Integer catId,
			String devLibelle, String commentairePrest) {
		getWSPrestationFactory().createCurrentDevis(utlOrdre, typePublicKey, fouOrdre, contactPersId, catId, devLibelle, commentairePrest);
	}

	/**
	 * Appelle {@link WSPrestationFactory#createCurrentDevisComplet}
	 */
	public void createCurrentDevisComplet(Integer utlOrdre, Integer typePublicKey, Integer fouOrdre, Integer contactPersId, Integer catId,
			String devLibelle, String commentairePrest, Integer orgId, Integer tapId, Integer tcdOrdre, Integer lolfId, String pcoNum,
			Integer modOrdre) {
		getWSPrestationFactory().createCurrentDevisComplet(utlOrdre, typePublicKey, fouOrdre, contactPersId, catId, devLibelle,
				commentairePrest, orgId, tapId, tcdOrdre, lolfId, pcoNum, modOrdre);
	}

	/**
	 * Appelle {@link WSPrestationFactory#createCurrentDevisArticle}
	 */
	public void createCurrentDevisArticle(Integer utlOrdre, Integer typePublicKey, Integer fouOrdre, Integer contactPersId, Integer catId,
			String devLibelle, String commentairePrest, Integer caarId, BigDecimal quantite) {
		getWSPrestationFactory().createCurrentDevisArticle(utlOrdre, typePublicKey, fouOrdre, contactPersId, catId, devLibelle,
				commentairePrest, caarId, (BigDecimal) quantite);
	}

	/**
	 * Appelle {@link WSPrestationFactory#ajouteLignePourArticleInCurrentDevis}
	 */
	public void ajouteLignePourArticleInCurrentDevis(Integer caarId, BigDecimal quantite) {
		getWSPrestationFactory().ajouteLignePourArticleInCurrentDevis(caarId, (BigDecimal) quantite);
	}

	public void ajouteLignePourArticleInCurrentDevis(String reference, String description, BigDecimal prixHt, 
			BigDecimal prixTtc, BigDecimal prixTotalHt, BigDecimal prixTotalTtc, BigDecimal quantite) {
		getWSPrestationFactory().ajouteLignePourArticleInCurrentDevis(reference, description, prixHt, prixTtc, prixTotalHt, prixTotalTtc, quantite);
	}
	
	/**
	 * Appelle {@link WSPrestationFactory#saveChanges}
	 */
	public Hashtable saveChanges() {
		return getWSPrestationFactory().saveChanges();
	}

	/**
	 * Appelle {@link WSPrestationFactory#imprimerCurrentDevis}
	 */
	public NSData imprimerCurrentDevis() {
		return getWSPrestationFactory().imprimerCurrentDevis();
	}

	/**
	 * Appelle {@link WSPrestationFactory#getCatalogues}
	 */
	// public NSArray getCatalogues(Integer fouOrdre, String typeUser) {
	// return getWSPrestationFactory().getCatalogues(fouOrdre, typeUser);
	// }
	/**
	 * Appelle {@link WSPrestationFactory#getDevisLignes}
	 */
	// public NSArray getDevisLignes(Integer devOrdre) {
	// return getWSPrestationFactory().getDevisLignes(devOrdre);
	// }
	/**
	 * Appelle {@link WSPrestationFactory#getDevisForConvOrdre}
	 */
	// public NSArray getDevisForConvOrdre(Integer convOrdre) {
	// return getWSPrestationFactory().getDevisForConvOrdre(convOrdre);
	// }
	/**
	 * Appelle {@link WSPrestationFactory#getTypcredAutorises()}
	 */
	public java.util.Vector getTypcredAutorises() {
		return getWSPrestationFactory().getTypcredAutorises();
	}

	/**
	 * Appelle {@link WSPrestationFactory#getCatalogueArticles}
	 */
	public NSArray getCatalogueArticles(Integer catId) {
		return getWSPrestationFactory().getCatalogueArticles(catId);
	}

	/**
	 * Appelle {@link WSPrestationFactory#getDevis}
	 */
	public NSArray getDevis(Integer fouOrdre, Integer persCliPersId, Integer prestId) {
		return getWSPrestationFactory().getDevis(fouOrdre, persCliPersId, prestId);
	}

	/**
	 * Appelle {@link WSPrestationFactory#valideCurrentDevisClient}
	 */
	public void valideCurrentDevisClient(Integer utlOrdre) {
		getWSPrestationFactory().valideCurrentDevisClient(utlOrdre);
	}

	public void devalideCurrentDevisClient(Integer utlOrdre) {
		getWSPrestationFactory().devalideCurrentDevisClient(utlOrdre);
	}

	/**
	 * Appelle {@link WSPrestationFactory#valideCurrentDevisPrest}
	 */
	public void valideCurrentDevisPrest(Integer utlOrdre) {
		getWSPrestationFactory().valideCurrentDevisPrest(utlOrdre);
	}

	/**
	 * Appelle {@link WSPrestationFactory#devalideCurrentDevisPrest}
	 */
	public void devalideCurrentDevisPrest(Integer utlOrdre) {
		getWSPrestationFactory().devalideCurrentDevisPrest(utlOrdre);
	}

	/**
	 * Appelle {@link WSPrestationFactory#fermerCurrentPrestation()}
	 */
	public void fermerCurrentPrestation(Integer utlOrdre) {
		getWSPrestationFactory().fermerCurrentPrestation(utlOrdre);
	}

	/**
	 * Appelle {@link WSPrestationFactory#decloreCurrentPrestation()}
	 */
	public void decloreCurrentPrestation(Integer utlOrdre) {
		getWSPrestationFactory().decloreCurrentPrestation(utlOrdre);
	}

	/**
	 * Appelle {@link WSPrestationFactory#valideToutDevis(Integer, Integer)}
	 */
	public void valideToutDevis(Integer prestId, Integer utlOrdre) {
		getWSPrestationFactory().valideToutDevis(prestId, utlOrdre);
	}

	/**
	 * Appelle {@link WSPrestationFactory#valideToutCurrentDevis(Integer)}
	 */
	public void valideToutCurrentDevis(Integer utlOrdre) {
		getWSPrestationFactory().valideToutCurrentDevis(utlOrdre);
	}

	/**
	 * Appelle {@link WSPrestationFactory#genererFactureForCurrentDevis()}
	 */
	public void genererFactureForCurrentDevis(Integer utlOrdre) {
		getWSPrestationFactory().genererFactureForCurrentDevis(utlOrdre);
	}

	/**
	 * Appelle {@link WSPrestationFactory#chargeCurrentDevisForClient(Integer, Integer)}
	 */
	public void chargeCurrentDevisForClient(Integer prestId, Integer fouOrdre) {
		getWSPrestationFactory().chargeCurrentDevisForClient(prestId, fouOrdre);
	}

	/**
	 * Appelle {@link WSPrestationFactory#chargeCurrentDevis(Integer)}
	 */
	public void chargeCurrentDevis(Integer prestId) {
		getWSPrestationFactory().chargeCurrentDevis(prestId);
	}

	/**
	 * Appelle {@link WSPrestationFactory#affecteOrganPrestForCurrentDevis(Integer)}
	 */
	public void affecteOrganPrestForCurrentDevis(Integer orgId) {
		getWSPrestationFactory().affecteOrganPrestForCurrentDevis(orgId);
	}

	/**
	 * Appelle {@link WSPrestationFactory#affecteOrganClientForCurrentDevis(Integer)}
	 */
	public void affecteOrganClientForCurrentDevis(Integer orgId) {
		getWSPrestationFactory().affecteOrganClientForCurrentDevis(orgId);
	}

	/**
	 * Appelle {@link WSPrestationFactory#affecteDestinationPrestForCurrentDevis(Integer)}
	 */
	public void affecteDestinationPrestForCurrentDevis(Integer lolfId) {
		getWSPrestationFactory().affecteDestinationPrestForCurrentDevis(lolfId);
	}

	/**
	 * Appelle {@link WSPrestationFactory#affecteDestinationClientForCurrentDevis(Integer)}
	 */
	public void affecteDestinationClientForCurrentDevis(Integer lolfId) {
		getWSPrestationFactory().affecteDestinationClientForCurrentDevis(lolfId);
	}

	/**
	 * Appelle {@link WSPrestationFactory#affecteTypcredPrestForCurrentDevis(Integer)}
	 */
	public void affecteTypcredPrestForCurrentDevis(Integer tcdOrdre) {
		getWSPrestationFactory().affecteTypcredPrestForCurrentDevis(tcdOrdre);
	}

	/**
	 * Appelle {@link WSPrestationFactory#affecteTypcredClientForCurrentDevis(String)}
	 */
	public void affecteTypcredClientForCurrentDevis(String tcdCode) {
		getWSPrestationFactory().affecteTypcredClientForCurrentDevis(tcdCode);
	}

	/**
	 * Appelle {@link WSPrestationFactory#affecteImputationClientForCurrentDevis(String pcoNum)}
	 */
	public void affecteImputationClientForCurrentDevis(String pcoNum) {
		getWSPrestationFactory().affecteImputationClientForCurrentDevis(pcoNum);
	}

	/**
	 * Appelle {@link WSPrestationFactory#affecteUtilisateur(Integer)}
	 */
	public void affecteUtilisateur(Integer utlOrdre) {
		getWSPrestationFactory().affecteUtilisateur(utlOrdre);
	}

	/**
	 * Appelle {@link WSPrestationFactory#affecteConvention(Integer)}
	 */
	public void affecteConvention(Integer conOrdre) {
		getWSPrestationFactory().affecteConvention(conOrdre);
	}

	/**
	 * Appelle {@link WSPrestationFactory#affecteRemiseGlobale(BigDecimal)}
	 */
	public void affecteRemiseGlobale(BigDecimal remise) {
		getWSPrestationFactory().affecteRemiseGlobale(remise);
	}

	/**
	 * Appelle {@link WSPrestationFactory#viderLignesForCurrentDevis()}
	 */
	public void viderLignesForCurrentDevis() {
		getWSPrestationFactory().viderLignesForCurrentDevis();
	}

	/**
	 * Appelle {@link WSPrestationFactory#imprimerFacture(Integer)}
	 */
	public NSData imprimerFacture(Integer fapId) {
		return getWSPrestationFactory().imprimerFacture(fapId);
	}

	/**
	 * Appelle {@link WSPrestationFactory#getMontantHTForCurrentDevis()}
	 */
	public java.math.BigDecimal getMontantHTForCurrentDevis() {
		return getWSPrestationFactory().getMontantHTForCurrentDevis();
	}

	/**
	 * Appelle {@link WSPrestationFactory#getMontantTVAForCurrentDevis()}
	 */
	public java.math.BigDecimal getMontantTVAForCurrentDevis() {
		return getWSPrestationFactory().getMontantTVAForCurrentDevis();
	}

	/**
	 * Appelle {@link WSPrestationFactory#getMontantTTCForCurrentDevis()}
	 */
	public java.math.BigDecimal getMontantTTCForCurrentDevis() {
		return getWSPrestationFactory().getMontantTTCForCurrentDevis();
	}

	/**
	 * Appelle {@link WSPrestationFactory#getMontantTTCForDevis(Object[], BigDecimal[])}
	 */
	public Double getMontantTTCForDevis(Object[] articles, BigDecimal[] quantites) {
		return getWSPrestationFactory().getMontantTTCForDevis(articles, (BigDecimal[]) quantites);
	}

	/**
	 * Appelle {@link WSPrestationFactory#currentDevisValideCoteClient()}
	 */
	public Boolean currentDevisValideCoteClient() {
		return getWSPrestationFactory().currentDevisValideCoteClient();
	}

	/**
	 * Appelle {@link WSPrestationFactory#currentDevisValideCotePrestataire()}
	 */
	public Boolean currentDevisValideCotePrestataire() {
		return getWSPrestationFactory().currentDevisValideCotePrestataire();
	}

	/**
	 * Appelle {@link WSPrestationFactory#currentPrestationRealisee()}
	 */
	public Boolean currentPrestationRealisee() {
		return getWSPrestationFactory().currentPrestationRealisee();
	}

	/**
	 * Appelle {@link WSPrestationFactory#getDisponible(Integer, String)}
	 */
	public Double getDisponible(Integer orgId, String tcdCode) {
		return getWSPrestationFactory().getDisponible(orgId, tcdCode);
	}

	/**
	 * Appelle {@link WSPrestationFactory#deleteCurrentDevis()}
	 */
	public void deleteCurrentDevis(Integer utlOrdre) {
		getWSPrestationFactory().deleteCurrentDevis(utlOrdre);
	}

	/**
	 * Appelle {@link WSPrestationFactory#deleteDevis(Integer, Integer)}
	 */
	public void deleteDevis(Integer prestId, Integer fouOrdre, Integer utlOrdre) {
		getWSPrestationFactory().deleteDevis(prestId, fouOrdre, utlOrdre);
	}

	/**
	 * Appelle {@link WSPrestationFactory#deleteDevisSansControle(Integer)}
	 */
	public void deleteDevisSansControle(Integer prestId, Integer utlOrdre) {
		getWSPrestationFactory().deleteDevisSansControle(prestId, utlOrdre);
	}

	/**
	 * Appelle {@link WSPrestationFactory#getDevisEtats()}
	 */
	// public Hashtable getDevisEtats() {
	// return getWSPrestationFactory().getDevisEtats();
	// }
	/**
	 * Appelle {@link WSPrestationFactory#getEtatCurrentDevis()}
	 */
	// public String getEtatCurrentDevis() {
	// return getWSPrestationFactory().getEtatCurrentDevis();
	// }
	/**
	 * Appelle {@link WSPrestationFactory#getIdCurrentDevis()}
	 */
	public Integer getIdCurrentDevis() {
		return getWSPrestationFactory().getIdCurrentDevis();
	}

}