/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.server.ws;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.Hashtable;

import org.cocktail.application.serveur.eof.EOExercice;
import org.cocktail.application.serveur.eof.EOTypeCredit;
import org.cocktail.kava.server.factory.FactoryPrestation;
import org.cocktail.kava.server.factory.FactoryPrestationLigne;
import org.cocktail.kava.server.finder.FinderCatalogueArticle;
import org.cocktail.kava.server.finder.FinderExercice;
import org.cocktail.kava.server.finder.FinderLolfNomenclatureDepense;
import org.cocktail.kava.server.finder.FinderPlanComptable;
import org.cocktail.kava.server.finder.FinderPrestation;
import org.cocktail.kava.server.finder.FinderTauxProrata;
import org.cocktail.kava.server.finder.FinderTypeApplication;
import org.cocktail.kava.server.finder.FinderTypeArticle;
import org.cocktail.kava.server.finder.FinderTypeCreditDep;
import org.cocktail.kava.server.finder.FinderTypeCreditRec;
import org.cocktail.kava.server.finder.FinderTypeEtat;
import org.cocktail.kava.server.finder.FinderTypePublic;
import org.cocktail.kava.server.metier.EOCatalogue;
import org.cocktail.kava.server.metier.EOCatalogueArticle;
import org.cocktail.kava.server.metier.EOConvention;
import org.cocktail.kava.server.metier.EOFacturePapier;
import org.cocktail.kava.server.metier.EOFournisUlr;
import org.cocktail.kava.server.metier.EOIndividuUlr;
import org.cocktail.kava.server.metier.EOLolfNomenclatureDepense;
import org.cocktail.kava.server.metier.EOLolfNomenclatureRecette;
import org.cocktail.kava.server.metier.EOModeRecouvrement;
import org.cocktail.kava.server.metier.EOOrgan;
import org.cocktail.kava.server.metier.EOPersonne;
import org.cocktail.kava.server.metier.EOPlanComptable;
import org.cocktail.kava.server.metier.EOPrestation;
import org.cocktail.kava.server.metier.EOPrestationLigne;
import org.cocktail.kava.server.metier.EOTauxProrata;
import org.cocktail.kava.server.metier.EOTypeArticle;
import org.cocktail.kava.server.metier.EOTypePublic;
import org.cocktail.kava.server.procedures.ApiPrestation;
import org.cocktail.kava.server.procedures.GetNumerotation;

import app.server.Session;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;


public class WSPrestationFactory extends ZWSFactory {

	private EOPrestation currentPrestation;

	public final Integer TYPE_PUBLIC_INTERNE = new Integer(1);
	public final Integer TYPE_PUBLIC_EXTERNE_INTRANET = new Integer(2);
	public final Integer TYPE_PUBLIC_EXTERNE_PUBLIC = new Integer(3);
	public final Integer TYPE_PUBLIC_EXTERNE_PRIVE = new Integer(4);
	public final Integer TYPE_PUBLIC_FORMATION_CONTINUE = new Integer(5);

	private static final String ERR_COMPTE =
			"L''imputation comptable {0} n''a pas ete recuperee (inexistant pour l''exercice {1} ou invalide)";

	public WSPrestationFactory(Session session) {
		super(session);
	}

	protected String getWSPassword() {
		return app.getParam(WSPrestationFactoryProvider.WS_PASSWORD_PARAM);
	}

	private EOPrestation getCurrentPrestation() {
		return currentPrestation;
	}

	private void resetDevis() {
		ec.revert();
		if (currentPrestation != null) {
			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(currentPrestation)));
			currentPrestation = null;
		}
	}

	private void checkCurrentDevisNotNull() throws Exception {
		if (getCurrentPrestation() == null) {
			throw new Exception("Il n'y a pas de devis en cours !");
		}
	}

	public Integer getIdCurrentDevis() {
		if (getCurrentPrestation() == null) {
			return null;
		}
		return (Integer) EOUtilities.primaryKeyForObject(ec, getCurrentPrestation()).valueForKey(EOPrestation.PREST_ID_KEY);
	}

	/**
	 * recupere un devis et en fait le devis en cours (pour rajouter des lignes par exemple). <br>
	 * Le client doit etre precise pour des raisons de securite. <br>
	 *
	 * @param prestId Identifiant du devis
	 * @param fouOrdre Identifiant du fournisseur client.
	 */
	public void chargeCurrentDevisForClient(Integer prestId, Integer fouOrdre) {
		setLastException(null);
		try {
			checkAuthenticated();
			resetDevis();

			// recuperer le devis depuis la base
			EOPrestation prest = findEOPrestation(ec, prestId);
			if (prest == null) {
				throw new Exception("Le devis demande n'existe pas (cle: " + prestId + ").");
			}

			EOFournisUlr fournisUlr = findEOFournisUlr(ec, fouOrdre);
			if (fournisUlr == null) {
				throw new Exception("Le fournisseur client n'a pas ete recupere.");
			}

			if (prest.fournisUlr() != fournisUlr) {
				throw new Exception("Le fournisseur client precise ne correspond pas a celui affecte au devis.");
			}

			currentPrestation = prest;
		} catch (Exception e) {
			new WSZException(e.getMessage());
		}
	}

	/**
	 * recupere un devis et en fait le devis en cours (pour rajouter des lignes par exemple). <br>
	 *
	 * @param prestId Identifiant du devis
	 */
	public void chargeCurrentDevis(Integer prestId) {
		setLastException(null);
		try {
			checkAuthenticated();
			resetDevis();

			// recuperer le devis depuis la base
			EOPrestation dev = findEOPrestation(ec, prestId);
			if (dev == null) {
				throw new Exception("Le devis demande n'existe pas (cle: " + prestId + ").");
			}

			currentPrestation = dev;
		} catch (Exception e) {
			new WSZException(e.getMessage());
		}
	}

	/**
	 * Validation du devis cote client. Si c'est de l'interne, cela va generer l'engagement en plus. Si toutes les informations (budgetaires
	 * notamment) sont remplies. L'utilisateur doit etre renseigne avant ({@link affecteUtilisateur}) ATTENTION: saveChanges automatique !
	 *
	 * @param utlOrdre l'utilisateur qui tente de valider le devis cote client (obligatoire)
	 */
	public void valideCurrentDevisClient(Integer utlOrdre) {
		trace("Tentative de validation du devis par le client");
		setLastException(null);
		try {
			checkAuthenticated();
			checkCurrentDevisNotNull();
			saveChanges();
			ApiPrestation.validePrestationClient(session.dataBus(), ec, getCurrentPrestation(), findEOUtilisateur(ec, utlOrdre));
			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(currentPrestation)));
			trace("Succes - Devis valide par le client.");
		} catch (Exception e) {
			new WSZException(e.getMessage());
		}
	}

	/**
	 * Devalidation du devis cote client. Si c'est de l'interne, l'engagement est egalement supprime. ATTENTION: saveChanges automatique !
	 *
	 * @param utlOrdre l'utilisateur qui tente de devalider le devis cote client (obligatoire)
	 */
	public void devalideCurrentDevisClient(Integer utlOrdre) {
		trace("Tentative de devalidation du devis par le client");
		setLastException(null);
		try {
			checkAuthenticated();
			checkCurrentDevisNotNull();
			saveChanges();
			if (utlOrdre == null && currentPrestation != null && currentPrestation.prestationBudgetClient() != null &&
					currentPrestation.prestationBudgetClient().engageBudget() != null &&
					currentPrestation.prestationBudgetClient().engageBudget().utilisateur() != null) {
				ApiPrestation.devalidePrestationClient(session.dataBus(), ec, getCurrentPrestation(),
						currentPrestation.prestationBudgetClient().engageBudget().utilisateur());
			}
			else {
				ApiPrestation.devalidePrestationClient(session.dataBus(), ec, getCurrentPrestation(), findEOUtilisateur(ec, utlOrdre));
			}
			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(currentPrestation)));
			trace("Succes - Devis devalide client.");
		} catch (Exception e) {
			new WSZException(e.getMessage());
		}
	}

	/**
	 * Validation du devis cote prestataire (date de validation prest) Aucune autre incidence ATTENTION: saveChanges automatique !
	 *
	 * @param utlOrdre l'utilisateur qui tente de valider le devis cote prestataire (obligatoire)
	 */
	public void valideCurrentDevisPrest(Integer utlOrdre) {
		trace("Tentative de validation du devis par le prestataire");
		setLastException(null);
		try {
			checkAuthenticated();
			checkCurrentDevisNotNull();
			saveChanges();
			ApiPrestation.validePrestationPrestataire(session.dataBus(), ec, getCurrentPrestation(), findEOUtilisateur(ec, utlOrdre));
			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(currentPrestation)));
			trace("Succes - Devis valide par le prestataire");
		} catch (Exception e) {
			new WSZException(e.getMessage());
		}
	}

	/**
	 * Devalidation du devis cote prestataire (date de validation prest null) Aucune autre incidence ATTENTION: saveChanges automatique !
	 *
	 * @param utlOrdre l'utilisateur qui tente de devalider le devis cote prestataire (obligatoire)
	 */
	public void devalideCurrentDevisPrest(Integer utlOrdre) {
		trace("Tentative de devalidation du devis par le prestataire");
		setLastException(null);
		try {
			checkAuthenticated();
			checkCurrentDevisNotNull();
			saveChanges();
			ApiPrestation.devalidePrestationPrestataire(session.dataBus(), ec, getCurrentPrestation(), findEOUtilisateur(ec, utlOrdre));
			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(currentPrestation)));
			trace("Succes - Devis devalide par le prestataire");
		} catch (Exception e) {
			new WSZException(e.getMessage());
		}
	}

	/**
	 * Passe la prestation a realisee (necessaire pour pourvoir generer des factures a partir d'une prestation). ATTENTION: saveChanges automatique !
	 *
	 * @param utlOrdre l'utilisateur qui tente de clore le devis (obligatoire)
	 */
	public void fermerCurrentPrestation(Integer utlOrdre) {
		try {
			checkAuthenticated();
			checkCurrentDevisNotNull();
			saveChanges();
			ApiPrestation.cloturePrestation(session.dataBus(), ec, currentPrestation, findEOUtilisateur(ec, utlOrdre));
			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(currentPrestation)));
		} catch (Exception e) {
			new WSZException(e.getMessage());
		}
	}

	/**
	 * Declos une prestation close (realisee ou passee par le valideToutCurrentDevis). ATTENTION: saveChanges automatique !
	 *
	 * @param utlOrdre l'utilisateur qui tente de declore le devis (obligatoire)
	 */
	public void decloreCurrentPrestation(Integer utlOrdre) {
		try {
			checkAuthenticated();
			checkCurrentDevisNotNull();
			saveChanges();
			ApiPrestation.decloturePrestation(session.dataBus(), ec, currentPrestation, findEOUtilisateur(ec, utlOrdre));
			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(currentPrestation)));
		} catch (Exception e) {
			new WSZException(e.getMessage());
		}
	}

	/**
	 * Charge le devis en devis courant et tente de le valider totalement (3 etapes) ATTENTION: saveChanges automatique !
	 *
	 * @param prestId la prestation a valider
	 * @param utlOrdre l'utilisateur qui tente de valider le devis
	 */
	public void valideToutDevis(Integer prestId, Integer utlOrdre) {
		trace("Tentative de validation totale du devis " + prestId + " par l'agent " + utlOrdre);
		setLastException(null);
		try {
			chargeCurrentDevis(prestId);
			if (getLastException() != null) {
				return;
			}
			valideToutCurrentDevis(utlOrdre);
		} catch (Exception e) {
			new WSZException(e.getMessage());
		}
	}

	/**
	 * Tente de valider totalement le devis en cours (3 etapes) ATTENTION: saveChanges automatique !
	 *
	 * @param utlOrdre l'utilisateur qui tente de valider le devis (obligatoire)
	 */
	public void valideToutCurrentDevis(Integer utlOrdre) {
		trace("Tentative de validation totale du devis courant par l'utilisateur " + utlOrdre);
		setLastException(null);
		try {
			checkCurrentDevisNotNull();
			// validation cote client
			if (currentPrestation.prestDateValideClient() == null) {
				valideCurrentDevisClient(utlOrdre);
			}
			if (getLastException() != null) {
				return;
			}

			// validation cote prestataire
			if (currentPrestation.prestDateValidePrest() == null) {
				valideCurrentDevisPrest(utlOrdre);
			}
			if (getLastException() != null) {
				return;
			}

			// fermeture de la prestation
			if (currentPrestation.prestDateCloture() == null) {
				fermerCurrentPrestation(utlOrdre);
			}
			if (getLastException() != null) {
				return;
			}

			trace("Succes - Devis clos");
		} catch (Exception e) {
			new WSZException(e.getMessage());
		}
	}

	/**
	 * Genere la (les) facture(s) a partir de la prestation en cours. La prestation doit etre cloturee ("Fermee"). Sinon une exception est generee.
	 * ATTENTION: saveChanges automatique !
	 *
	 * @param utlOrdre Identifiant de l'utilisateur qui genere la facture (obligatoire)
	 */
	public void genererFactureForCurrentDevis(Integer utlOrdre) {
		setLastException(null);
		try {
			checkAuthenticated();
			checkCurrentDevisNotNull();
			ApiPrestation.genereFacturePapier(session.dataBus(), ec, currentPrestation, findEOUtilisateur(ec, utlOrdre));
			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(currentPrestation)));
		} catch (Exception e) {
			new WSZException(e.getMessage());
		}
	}

	/**
	 * Archive le devis en cours (ne le supprime pas, la suppression d'un devis est interdite). NE NECESSITE PAS DE SAVECHANGES, LE CHANGEMENT EST
	 * EFFECTIF DE SUITE !
	 *
	 * @param utlOrdre Identifiant de l'utilisateur qui supprime le devis (obligatoire)
	 */
	public void deleteCurrentDevis(Integer utlOrdre) {
		try {
			checkAuthenticated();
			checkCurrentDevisNotNull();
			ApiPrestation.archivePrestation(session.dataBus(), ec, currentPrestation, findEOUtilisateur(ec, utlOrdre));
			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(currentPrestation)));
		} catch (Exception e) {
			new WSZException(e.getMessage());
		}
	}

	/**
	 * Charge en devis courant et archive le devis donne (ne le supprime pas, la suppression d'un devis est interdite). NE NECESSITE PAS DE
	 * SAVECHANGES, LE CHANGEMENT EST EFFECTIF DE SUITE !
	 *
	 * @param prestId Identifiant du devis (obligatoire)
	 * @param fouOrdre Identifiant de la personne cliente (obligatoire)
	 * @param utlOrdre Identifiant de l'utilisateur qui supprime le devis (obligatoire)
	 */
	public void deleteDevis(Integer prestId, Integer fouOrdre, Integer utlOrdre) {
		setLastException(null);
		try {
			checkAuthenticated();
			if (currentPrestation != null) {
				Integer currentPrestId = (Integer) EOUtilities.primaryKeyForObject(ec, currentPrestation).valueForKey(EOPrestation.PREST_ID_KEY);
				if (currentPrestId.intValue() != prestId.intValue()) {
					throw new Exception(
							"Le devis (prestId) donne ne correspond pas au devis en cours. Impossible de realiser l'operation de suppression.");
				}
			}
			else {
				chargeCurrentDevisForClient(prestId, fouOrdre);
			}
			deleteCurrentDevis(utlOrdre);
		} catch (Exception e) {
			new WSZException(e.getMessage());
		}
	}

	/**
	 * Charge en devis courant et archive le devis donne (ne le supprime pas, la suppression d'un devis est interdite). NE NECESSITE PAS DE
	 * SAVECHANGES, LE CHANGEMENT EST EFFECTIF DE SUITE !
	 *
	 * @param devOrdre Identifiant du devis
	 * @param utlOrdre Identifiant de l'utilisateur qui supprime le devis (obligatoire)
	 */
	public void deleteDevisSansControle(Integer prestId, Integer utlOrdre) {
		setLastException(null);
		try {
			checkAuthenticated();
			if (currentPrestation != null) {
				Integer currentPrestId = (Integer) EOUtilities.primaryKeyForObject(ec, currentPrestation).valueForKey(EOPrestation.PREST_ID_KEY);
				if (currentPrestId.intValue() != prestId.intValue()) {
					throw new Exception(
							"Le devis (prestId) donne ne correspond pas au devis en cours. Impossible de realiser l'operation de suppression.");
				}
			}
			else {
				chargeCurrentDevis(prestId);
			}
			deleteCurrentDevis(utlOrdre);
		} catch (Exception e) {
			new WSZException(e.getMessage());
		}
	}

	/**
	 * Indique si le devis est deja valide cote client.
	 *
	 * @return Boolean
	 */
	public Boolean currentDevisValideCoteClient() {
		setLastException(null);
		try {
			checkAuthenticated();
			checkCurrentDevisNotNull();
			return new Boolean(currentPrestation.prestDateValideClient() != null);
		} catch (Exception e) {
			new WSZException(e.getMessage());
			return new Boolean(false);
		}
	}

	/**
	 * Indique si le devis est deja valide cote prestataire.
	 *
	 * @return Boolean
	 */
	public Boolean currentDevisValideCotePrestataire() {
		setLastException(null);
		try {
			checkAuthenticated();
			checkCurrentDevisNotNull();
			return new Boolean(currentPrestation.prestDateValidePrest() != null);
		} catch (Exception e) {
			new WSZException(e.getMessage());
			return new Boolean(false);
		}
	}

	/**
	 * Indique si le devis est deja clos.
	 *
	 * @return Boolean
	 */
	public Boolean currentPrestationRealisee() {
		setLastException(null);
		try {
			checkAuthenticated();
			checkCurrentDevisNotNull();
			return new Boolean(currentPrestation.prestDateCloture() != null);
		} catch (Exception e) {
			new WSZException(e.getMessage());
			return new Boolean(false);
		}
	}

	/**
	 * Cree un devis et l'affecte comme devis courant. (Le devis n'est pas enregistre dans la base).
	 *
	 * @param utlOrdre Utilisateur qui Cree le devis (facultatif)
	 * @param typePublicKey TYPE_PUBLIC_xxx (obligatoire)
	 * @param fouOrdre fouOrdre du client (obligatoire)
	 * @param contactPersId persId de l'individu client (contact) (facultatif)
	 * @param catId Identifiant du catalogue (obligatoire)
	 * @param devLibelle Libelle du devis (obligatoire)
	 * @param commentairePrest Commentaire a imprimer en bas de devis (facultatif)
	 */
	public void createCurrentDevis(Integer utlOrdre, Integer typePublicKey, Integer fouOrdre, Integer contactPersId, Integer catId,
			String devLibelle, String commentairePrest) {
		createCurrentDevisComplet(utlOrdre, typePublicKey, fouOrdre, contactPersId, catId, devLibelle, commentairePrest, null, null, null, null,
				null, null);
	}

	/**
	 * Cree un devis et l'affecte comme devis courant. (Le devis n'est pas enregistre dans la base).
	 * Si les donnees budgetaires ne sont pas renseignees, elles sont determinees en auto si possible
	 * a partir des infos budgetaires par defaut du catalogue.
	 *
	 * @param utlOrdre Utilisateur qui Cree le devis (facultatif)
	 * @param typePublicKey TYPE_PUBLIC_xxx (obligatoire)
	 * @param fouOrdre fouOrdre du client (obligatoire)
	 * @param contactPersId persId de l'individu client (contact) (facultatif)
	 * @param catId Identifiant du catalogue (obligatoire)
	 * @param devLibelle Libelle du devis (obligatoire)
	 * @param commentairePrest Commentaire a imprimer en bas de devis (facultatif)
	 * @param orgId Ligne budgetaire recette (facultatif)
	 * @param tapId Taux de prorata recette (facultatif)
	 * @param tcdOrdre Type de credit recette (facultatif)
	 * @param lolfId Action lolf recette (facultatif)
	 * @param pcoNum Plan comptable recette (facultatif)
	 * @param modOrdre Mode de recouvrement (facultatif)
	 */
	public void createCurrentDevisComplet(Integer utlOrdre, Integer typePublicKey, Integer fouOrdre,
			Integer contactPersId, Integer catId, String devLibelle, String commentairePrest, Integer orgId,
			Integer tapId, Integer tcdOrdre, Integer lolfId, String pcoNum,	Integer modOrdre) {
		setLastException(null);
		trace("Tentative de creation d'un devis");
		resetDevis();
		try {
			checkAuthenticated();

			EOFournisUlr fournisUlr = null;
			EOIndividuUlr individuUlr = null;
			EOCatalogue catalogue = null;

			if (typePublicKey == null) {
				throw new Exception("Le type de public est obligatoire !");
			}
			if (typePublicKey.intValue() != TYPE_PUBLIC_INTERNE.intValue()
					&& typePublicKey.intValue() != TYPE_PUBLIC_EXTERNE_INTRANET.intValue()
					&& typePublicKey.intValue() != TYPE_PUBLIC_EXTERNE_PRIVE.intValue()
					&& typePublicKey.intValue() != TYPE_PUBLIC_EXTERNE_PUBLIC.intValue()
					&& typePublicKey.intValue() != TYPE_PUBLIC_FORMATION_CONTINUE.intValue()) {
				throw new Exception("Le type de public ne correspond pas a une valeur valide !");
			}

			if (fouOrdre == null) {
				throw new Exception("L'identifiant de client (fouOrdre) est obligatoire !");
			}

			if (catId == null) {
				throw new Exception("L'identifiant de catalogue est obligatoire !");
			}

			fournisUlr = findEOFournisUlr(ec, fouOrdre);
			if (contactPersId != null) {
				individuUlr = findEOIndividuUlrByPersId(ec, contactPersId);
			}
			catalogue = findEOCatalogue(ec, catId);
			EOTypePublic typePublic = null;
			if (typePublicKey.intValue() == TYPE_PUBLIC_INTERNE.intValue()) {
				typePublic = FinderTypePublic.typePublicInterne(ec);
			}
			if (typePublicKey.intValue() == TYPE_PUBLIC_EXTERNE_INTRANET.intValue()) {
				typePublic = FinderTypePublic.typePublicExterneIntranet(ec);
			}
			if (typePublicKey.intValue() == TYPE_PUBLIC_EXTERNE_PRIVE.intValue()) {
				typePublic = FinderTypePublic.typePublicExternePrive(ec);
			}
			if (typePublicKey.intValue() == TYPE_PUBLIC_EXTERNE_PUBLIC.intValue()) {
				typePublic = FinderTypePublic.typePublicExternePublic(ec);
			}
			if (typePublicKey.intValue() == TYPE_PUBLIC_FORMATION_CONTINUE.intValue()) {
				typePublic = FinderTypePublic.typePublicFormationContinue(ec);
			}

			currentPrestation = FactoryPrestation.newObject(ec);
			currentPrestation.setUtilisateurRelationship(findEOUtilisateur(ec, utlOrdre));
			currentPrestation.setTypePublicRelationship(typePublic);
			currentPrestation.setCatalogueRelationship(catalogue);
			currentPrestation.setFournisUlrPrestRelationship(catalogue.fournisUlr());
			currentPrestation.setExerciceRelationship(FinderExercice.findExerciceEnCours(ec));
			currentPrestation.setFournisUlrRelationship(fournisUlr);
			currentPrestation.setPersonneRelationship(fournisUlr.personne());
			currentPrestation.setIndividuUlrRelationship(individuUlr);
			System.out.println("\ndevLibelle = <" + devLibelle + ">\n");
			currentPrestation.setPrestLibelle(devLibelle);
			currentPrestation.setPrestCommentairePrest(commentairePrest);
			currentPrestation.setPrestCommentaireClient(null);
			// ici pour l'instant on force les prestations internes non lasm (pas de tva)
			if (typePublic != null	&& typePublic.typeApplication().equals(
					FinderTypeApplication.typeApplicationPrestationInterne(ec))) {
				currentPrestation.setPrestApplyTva("N");
			} else {
				currentPrestation.setPrestApplyTva("O");
			}

			// infos budgetaires...
			currentPrestation.setOrganRelationship(null);
			currentPrestation.setTypeCreditRecRelationship(null);
			currentPrestation.setLolfNomenclatureRecetteRelationship(null);
			currentPrestation.setModeRecouvrementRelationship(null);
			if (orgId != null) {
				currentPrestation.setOrganRelationship(
						(EOOrgan) EOUtilities.objectWithPrimaryKeyValue(ec, EOOrgan.ENTITY_NAME, orgId));
			}
			if (tapId != null) {
				currentPrestation.setTauxProrataRelationship((EOTauxProrata) EOUtilities.objectWithPrimaryKeyValue(ec,
						EOTauxProrata.ENTITY_NAME, tapId));
			}
			if (tcdOrdre != null) {
				currentPrestation.setTypeCreditRecRelationship((EOTypeCredit) EOUtilities.objectWithPrimaryKeyValue(ec,
						EOTypeCredit.ENTITY_NAME, tcdOrdre));
			}
			if (lolfId != null) {
				currentPrestation.setLolfNomenclatureRecetteRelationship((EOLolfNomenclatureRecette)
						EOUtilities.objectWithPrimaryKeyValue(ec, EOLolfNomenclatureRecette.ENTITY_NAME, lolfId));
			}
			if (pcoNum != null) {
				currentPrestation = majCompte(currentPrestation, pcoNum);
			}
			if (modOrdre != null) {
				currentPrestation.setModeRecouvrementRelationship((EOModeRecouvrement)
						EOUtilities.objectWithPrimaryKeyValue(ec, EOModeRecouvrement.ENTITY_NAME, modOrdre));
			}

			if (catalogue != null && catalogue.cataloguePrestation() != null) {
				if (currentPrestation.organ() == null) {
					currentPrestation.setOrganRelationship(catalogue.cataloguePrestation().organRecette());
				}
				if (currentPrestation.typeCreditRec() == null) {
					NSArray a = FinderTypeCreditRec.find(ec, currentPrestation.exercice());
					if (a != null && a.count() == 1) {
						currentPrestation.setTypeCreditRecRelationship((EOTypeCredit) a.objectAtIndex(0));
					}
				}
				if (currentPrestation.lolfNomenclatureRecette() == null) {
					currentPrestation.setLolfNomenclatureRecetteRelationship(
							catalogue.cataloguePrestation().lolfNomenclatureRecette());
				}
				if (currentPrestation.planComptable() == null) {
					currentPrestation = majCompte(currentPrestation, catalogue.cataloguePrestation().pcoNumRecette());
				}
			}
			trace(getCurrentPrestation());
		} catch (Exception e) {
			e.printStackTrace();
			new WSZException(e.getMessage());
		}
	}

	private EOPrestation majCompte(EOPrestation prestation, String numeroCompte) {
		if (prestation.exercice() == null) {
			return prestation;
		}
		EOPlanComptable compte = FinderPlanComptable.findOnlyValid(ec, prestation.exercice(), numeroCompte);
		if (compte != null) {
			prestation.setPlanComptableRelationship(compte);
		}
		return prestation;
	}

	/**
	 * Cree un devis avec un article et l'affecte comme devis courant. (Le devis n'est pas enregistre dans la base).
	 *
	 * @param utlOrdre Utilisateur qui Cree le devis (facultatif)
	 * @param typePublicKey TYPE_PUBLIC_xxx (obligatoire)
	 * @param fouOrdre fouOrdre du client (obligatoire)
	 * @param contactPersId persId de l'individu client (contact) (facultatif)
	 * @param catId Identifiant du catalogue (obligatoire)
	 * @param devLibelle Libelle du devis (obligatoire)
	 * @param commentairePrest Commentaire a imprimer en bas de devis (facultatif)
	 * @param caarId Article a positionner dans ce devis (table jefy_catalogue.catalogue_article) (obligatoire)
	 * @param quantite Quantite de cet article (facultatif, si null, quantite par defaut = 1)
	 */
	public void createCurrentDevisArticle(Integer utlOrdre, Integer typePublicKey, Integer fouOrdre, Integer contactPersId, Integer catId,
			String devLibelle, String commentairePrest, Integer caarId, BigDecimal quantite) {
		createCurrentDevisComplet(utlOrdre, typePublicKey, fouOrdre, contactPersId, catId, devLibelle, commentairePrest, null, null, null, null,
				null, null);
		ajouteLignePourArticleInCurrentDevis(caarId, quantite);
	}

	/**
	 * Ajoute une ligne au devis en cours. Le devis en cours doit etre charge ou cree.
	 *
	 * @param caarId Identifiant de l'article (obligatoire)
	 * @param quantite j'crois que c'est clair (facultatif, si null, quantite par defaut = 1)
	 */
	public void ajouteLignePourArticleInCurrentDevis(Integer caarId, BigDecimal quantite) {
		setLastException(null);
		trace("Tentative d'ajout d'une ligne au devis");
		try {
			checkAuthenticated();

			if (currentPrestation == null) {
				throw new Exception("Le devis en cours est nul. Vous devez le creer ou le charger.");
			}

			EOCatalogueArticle article = findEOCatalogueArticle(ec, caarId);
			if (article == null) {
				throw new Exception("L'article correspondant au caarId=" + caarId + " n'a pas ete recupere.");
			}

			EOPrestationLigne prestationLigne = FactoryPrestationLigne.newObject(ec, currentPrestation, article);
			prestationLigne.setPrligQuantite(ec, quantite == null ? new BigDecimal(1.0) : quantite);
			prestationLigne.setPrligQuantiteReste(prestationLigne.prligQuantite());
			prestationLigne.alignementQuantiteResteDesAvenantsALArticle();
		} catch (Exception e) {
			new WSZException(e.getMessage());
		}
	}

	public void ajouteLignePourArticleInCurrentDevis(String reference, String description, BigDecimal prixHt,
			BigDecimal prixTtc, BigDecimal prixTotalHt, BigDecimal prixTotalTtc, BigDecimal quantite) {
		setLastException(null);
		trace("Tentative d'ajout d'une ligne au devis");
		try {
			checkAuthenticated();

			if (currentPrestation == null) {
				throw new Exception("Le devis en cours est nul. Vous devez le creer ou le charger.");
			}

			EOTypeArticle typeArticle = FinderTypeArticle.typeArticleArticle(ec);
			FactoryPrestationLigne.newObject(ec, currentPrestation, reference, description, prixHt,
					prixTtc, prixTotalHt, prixTotalTtc, quantite == null ? new BigDecimal(1.0) : quantite, typeArticle);
		} catch (Exception e) {
			new WSZException(e.getMessage());
		}
	}

	/**
	 * Supprime toutes les lignes du devis en cours (si le devis est valide, une exception est declenchee). Necessite un saveChanges pour prendre en
	 * compte la modif.
	 */
	public void viderLignesForCurrentDevis() {
		setLastException(null);
		try {
			checkAuthenticated();
			checkCurrentDevisNotNull();
			if (currentDevisValideCoteClient().booleanValue()) {
				throw new Exception("Devis deja valide, interdit de vider le panier !");
			}
			if (currentPrestation.prestationLignes() != null) {
				while (currentPrestation.prestationLignes().count() > 0) {
					EOPrestationLigne prlig = (EOPrestationLigne) currentPrestation.prestationLignes().objectAtIndex(0);
					currentPrestation.removeFromPrestationLignesRelationship(prlig);
					ec.deleteObject(prlig);
				}
			}
		} catch (Exception e) {
			new WSZException(e.getMessage());
		}
	}

	/**
	 * Affectation de l'utilisateur au devis en cours. Necessite un saveChanges pour prendre en compte la modif.
	 *
	 * @param utlOrdre
	 */
	public void affecteUtilisateur(Integer utlOrdre) {
		setLastException(null);
		try {
			checkAuthenticated();
			checkCurrentDevisNotNull();
			currentPrestation.setUtilisateurRelationship(findEOUtilisateur(ec, utlOrdre));
		} catch (Exception e) {
			new WSZException(e);
		}
	}

	/**
	 * Affecte une convention en recette au devis en cours. Necessite un saveChanges pour prendre en compte la modif.
	 *
	 * @param conOrdre
	 */
	public void affecteConvention(Integer conOrdre) {
		setLastException(null);
		try {
			checkAuthenticated();
			EOConvention convention = (EOConvention) EOUtilities.objectWithPrimaryKeyValue(ec, EOConvention.ENTITY_NAME, conOrdre);
			currentPrestation.setConventionRelationship(convention);
		} catch (Exception e) {
			new WSZException(e);
		}
	}

	/**
	 * Affecte une remise globale a la prestation. Necessite un saveChanges pour prendre en compte la modif.
	 *
	 * @param remise
	 */
	public void affecteRemiseGlobale(Number remise) {
		setLastException(null);
		try {
			checkAuthenticated();
			currentPrestation.setPrestRemiseGlobale(new java.math.BigDecimal(remise.doubleValue()).setScale(2,
					java.math.BigDecimal.ROUND_HALF_UP));
		} catch (Exception e) {
			new WSZException(e);
		}
	}

	/**
	 * Affecte la ligne budgetaire cote prestataire au devis (necessaire pour la validation cote prestataire).
	 * Necessite un saveChanges pour prendre en compte la modif.
	 *
	 * @param orgId
	 */
	public void affecteOrganPrestForCurrentDevis(Integer orgId) {
		setLastException(null);
		try {
			checkAuthenticated();
			checkCurrentDevisNotNull();
			EOOrgan organ = (EOOrgan) EOUtilities.objectWithPrimaryKeyValue(ec, EOOrgan.ENTITY_NAME, orgId);
			if (organ == null) {
				throw new Exception("La ligne budgetaire n'a pas ete recuperee.");
			}
			currentPrestation.setOrganRelationship(organ);
			currentPrestation.updateTauxProrata();
			if (currentPrestation.tauxProrata() == null) {
				throw new Exception("Le taux de prorata de 100% n'a pas ete recupere (inexistant ou invalide).");
			}
		} catch (Exception e) {
			new WSZException(e.getMessage());
		}
	}

	/**
	 * Affecte la ligne budgetaire cote client au devis. Necessite un saveChanges pour prendre en compte la modif.
	 *
	 * @param orgId
	 */
	public void affecteOrganClientForCurrentDevis(Integer orgId) {
		setLastException(null);
		try {
			checkAuthenticated();
			checkCurrentDevisNotNull();
			EOOrgan organ = (EOOrgan) EOUtilities.objectWithPrimaryKeyValue(ec, EOOrgan.ENTITY_NAME, orgId);
			if (organ == null) {
				throw new Exception("La ligne budgetaire n'a pas ete recuperee.");
			}
			currentPrestation.prestationBudgetClient().setOrganRelationship(organ);
			if (currentPrestation.prestationBudgetClient().tauxProrata() == null) {
				currentPrestation.prestationBudgetClient().setTauxProrataRelationship(
						FinderTauxProrata.findDefault(
								ec, currentPrestation.prestationBudgetClient().organ(), currentPrestation.exercice()));
				if (currentPrestation.prestationBudgetClient().tauxProrata() == null) {
					currentPrestation.prestationBudgetClient().setTauxProrataRelationship(
							FinderTauxProrata.findDefault(ec));
				}
			}
		} catch (Exception e) {
			new WSZException(e.getMessage());
		}
	}

	/**
	 * Affecte le type de credit cote prestataire au devis. Necessite un saveChanges pour prendre en compte la modif.
	 *
	 * @param tcdOrdre
	 */
	public void affecteTypcredPrestForCurrentDevis(Integer tcdOrdre) {
		setLastException(null);
		try {
			checkAuthenticated();
			checkCurrentDevisNotNull();
			EOTypeCredit tcd = (EOTypeCredit) EOUtilities.objectWithPrimaryKeyValue(ec, EOTypeCredit.ENTITY_NAME, tcdOrdre);
			if (tcd == null) {
				throw new Exception("Le type de credit recette n'a pas ete recupere.");
			}
			currentPrestation.setTypeCreditRecRelationship(tcd);
		} catch (Exception e) {
			new WSZException(e.getMessage());
		}
	}

	/**
	 * Affecte le type de credit cote client au devis. Necessite un saveChanges pour prendre en compte la modif.
	 *
	 * @param tcdCode
	 */
	public void affecteTypcredClientForCurrentDevis(String tcdCode) {
		setLastException(null);
		try {
			checkAuthenticated();
			checkCurrentDevisNotNull();
			EOTypeCredit tcd = FinderTypeCreditDep.find(ec, tcdCode);
			if (tcd == null) {
				throw new Exception("Le type de credit depense n'a pas ete recupere.");
			}
			currentPrestation.prestationBudgetClient().setTypeCreditDepRelationship(tcd);
		} catch (Exception e) {
			new WSZException(e.getMessage());
		}
	}

	/**
	 * Affecte la destination recette cote prestataire au devis. Necessite un saveChanges pour prendre en compte la modif.
	 *
	 * @param lolfId
	 */
	public void affecteDestinationPrestForCurrentDevis(Integer lolfId) {
		setLastException(null);
		try {
			checkAuthenticated();
			checkCurrentDevisNotNull();
			EOLolfNomenclatureRecette lolf = (EOLolfNomenclatureRecette) EOUtilities.objectWithPrimaryKeyValue(ec,
					EOLolfNomenclatureRecette.ENTITY_NAME, lolfId);
			if (lolf == null) {
				throw new Exception("La destination recette n'a pas ete recuperee.");
			}
			currentPrestation.setLolfNomenclatureRecetteRelationship(lolf);
		} catch (Exception e) {
			new WSZException(e.getMessage());
		}
	}

	/**
	 * Affecte la destination recette cote client au devis. Necessite un saveChanges pour prendre en compte la modif.
	 *
	 * @param lolfId
	 */
	public void affecteDestinationClientForCurrentDevis(Integer lolfId) {
		setLastException(null);
		try {
			checkAuthenticated();
			checkCurrentDevisNotNull();
			// EOLolfNomenclatureDepense lolf = (EOLolfNomenclatureDepense) EOUtilities.objectWithPrimaryKey(ec,
			// EOLolfNomenclatureDepense.ENTITY_NAME, new NSDictionary(new Object[] { lolfId,
			// EOUtilities.primaryKeyForObject(ec, FinderExercice.findExerciceEnCours(ec)).valueForKey(EOExercice.EXE_ORDRE_KEY) },
			// new Object[] { EOLolfNomenclatureDepense.LOLF_ID_KEY, EOLolfNomenclatureDepense.EXE_ORDRE_KEY }));
			EOLolfNomenclatureDepense lolf = FinderLolfNomenclatureDepense.find(ec, lolfId, FinderExercice.findExerciceEnCours(ec));
			if (lolf == null) {
				throw new Exception("La destination depense n'a pas ete recuperee.");
			}
			currentPrestation.prestationBudgetClient().setLolfNomenclatureDepenseRelationship(lolf);
		} catch (Exception e) {
			new WSZException(e.getMessage());
		}
	}

	/**
	 * Affecte l'imputation comptable cote client au devis. Necessite un saveChanges pour prendre en compte la modif.
	 *
	 * @param pcoNum
	 */
	public void affecteImputationClientForCurrentDevis(String pcoNum) {
		setLastException(null);
		try {
			checkAuthenticated();
			checkCurrentDevisNotNull();

			EOPlanComptable compte = FinderPlanComptable.findOnlyValid(ec, currentPrestation.exercice(), pcoNum);
			if (compte == null) {
				String exercice = currentPrestation.exercice() != null
						? currentPrestation.exercice().exeOrdre().toString()
						: "indéfini";
				throw new Exception(MessageFormat.format(ERR_COMPTE, pcoNum, exercice));
			}
			currentPrestation.prestationBudgetClient().setPcoNum(compte.pcoNum());
		} catch (Exception e) {
			new WSZException(e.getMessage());
		}
	}

	/**
	 * Renvoie le montant HT du devis charge.
	 *
	 * @return Montant HT
	 */
	public java.math.BigDecimal getMontantHTForCurrentDevis() {
		if (currentPrestation == null) {
			return null;
		}
		return currentPrestation.prestTotalHt();
	}

	/**
	 * Renvoie le montant TVA du devis charge.
	 *
	 * @return Montant TVA
	 */
	public java.math.BigDecimal getMontantTVAForCurrentDevis() {
		if (currentPrestation == null) {
			return null;
		}
		return currentPrestation.prestTotalTva();
	}

	/**
	 * Renvoie le montant TTC du devis charge.
	 *
	 * @return Montant TTC
	 */
	public java.math.BigDecimal getMontantTTCForCurrentDevis() {
		if (currentPrestation == null) {
			return null;
		}
		return currentPrestation.prestTotalTtc();
	}

	/**
	 * Imprime le devis en cours si celui-ci a ete cree ou charge. Le resultat renvoie un NSData correspondant au fichier PDF a imprimer.
	 */
	public NSData imprimerCurrentDevis() {
		setLastException(null);
		trace("Tentative d'impression du devis ");
		try {
			checkAuthenticated();
			if (currentPrestation == null) {
				throw new Exception("Aucun devis n'a ete charge !");
			}
			return session.clientSideRequestPrintDevis(currentPrestation, null);
		} catch (Exception e) {
			new WSZException(e.getMessage());
			return null;
		}
	}

	/**
	 * Retourne le montant TTC prevu pour un devis qui comporterait les articles/quantites passes. Ne Cree aucun devis, ne Cree rien dans la base !
	 *
	 * @param articles Tableau des articles qui constituent le devis (caarId) - Obligatoire
	 * @param quantites Tableau de BigDecimal des quantites des articles du premier tableau - Facultatif, toutes quantites a 1 par defaut si absent
	 * @return Le montant TTC, sans tenir compte des remises eventuelles
	 */
	public Double getMontantTTCForDevis(Object[] articles, BigDecimal[] quantites) {
		setLastException(null);
		try {
			if (articles == null) {
				throw new Exception("[WSPrestationFactory:getMontantTTCForDevis()] le tableau des articles est vide, petit probleme !");
			}
			if (quantites == null || articles.length != quantites.length) {
				throw new Exception(
						"[WSPrestationFactory:getMontantTTCForDevis()] le tableau des articles et celui des quantites ne sont pas de meme taille, petit probleme !");
			}
			if (quantites == null) {
				quantites = new BigDecimal[articles.length];
				for (int i = 0; i < articles.length; i++) {
					quantites[i] = new BigDecimal(1.0);
				}
			}

			BigDecimal totalTtc = new BigDecimal(0.0);
			for (int i = 0; i < articles.length; i++) {
				EOCatalogueArticle article = findEOCatalogueArticle(ec, new Integer(articles[i].toString()));
				if (article == null) {
					throw new Exception("L'article correspondant au caarId = " + articles[i] + " n'a pas ete recupere.");
				}
				totalTtc = totalTtc.add(article.caarPrixTtc().multiply(quantites[i]));
			}
			return new Double(totalTtc.doubleValue());
		} catch (Exception e) {
			new WSZException(e.getClass().getName() + " " + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Renvoie le disponible a partir de la fonction stockee. Se base sur l'exercice en cours, la ligne budgetaire et le type de credit.
	 *
	 * @param orgId La ligne budgetaire (obligatoire)
	 * @param tcdCode Le type de credit (obligatoire)
	 * @return Le disponible (Double)
	 */
	public Double getDisponible(Integer orgId, String tcdCode) {
		NSMutableDictionary dico = new NSMutableDictionary();
		dico.takeValueForKey(FinderExercice.findExerciceEnCours(ec).valueForKey(EOExercice.EXE_ORDRE_KEY), "010aExeOrdre");
		dico.takeValueForKey(orgId, "020aOrgId");
		dico.takeValueForKey(EOUtilities.primaryKeyForObject(ec, FinderTypeCreditDep.find(ec, tcdCode))
				.valueForKey(EOTypeCredit.TCD_ORDRE_KEY), "030aTcdOrdre");

		System.out.println(EOUtilities.primaryKeyForObject(ec, FinderTypeCreditDep.find(ec, tcdCode)));
		try {
			NSDictionary returnedDico = session.sessionPieFwk().clientSideRequestExecuteStoredProcedureNamed("GetDisponible", dico);
			if (returnedDico != null) {
				return new Double(((BigDecimal) returnedDico.valueForKey("040aDisponible")).doubleValue());
			}
			return new Double(0);
		} catch (Exception e) {
			e.printStackTrace();
			return new Double(0);
		}
	}

	/**
	 * Imprime une facture. Le resultat renvoie un NSData correspondant au fichier PDF a imprimer.
	 *
	 * @param fapId L'identifiant de la facture (obligatoire)
	 * @return
	 */
	public NSData imprimerFacture(Integer fapId) {
		setLastException(null);
		trace("Tentative d'impression de la facture " + fapId);
		try {
			checkAuthenticated();
			if (fapId == null) {
				throw new Exception("L'identifiant de facture n'a pas ete fourni pour l'impression !");
			}
			EOFacturePapier facturePapier = findEOFacturePapier(ec, fapId);
			return session.clientSideRequestPrintFacturePapier(facturePapier, null);
		} catch (Exception e) {
			new WSZException(e.getMessage());
			return null;
		}
	}

	/**
	 * recupere les devis d'un client particulier et les renvoie sous forme de NSArray de NSDictionary. <br>
	 * En specifiant prestId, on recupere un NSArray a un seul element. <br>
	 * <br>
	 * Les NSDictionary sont recuperes par {@link WSPrestationFactory#getDictionaryFromEOPrestation(EOPrestation)}. Le NSDictionary contient les cles
	 * suivantes: <br>
	 * <ul>
	 * <li>prestId : Identifiant du devis</li>
	 * <li>catId : Identifiant du catalogue</li>
	 * <li>devDateDevis : Date de creation du devis</li>
	 * <li>devDatePrestValide : Date de validation du devis par le prestataire</li>
	 * <li>devDateClientValide : Date de validation du devis par le client</li>
	 * <li>devLibelle : Libelle du devis</li>
	 * <li>fouOrdre : Identifiant du fournisseur client</li>
	 * <li>persId : Identifiant du client</li>
	 * <li>noIndividu : Identifiant du contact client</li>
	 * <li>fouOrdrePrest : Identifiant du fournisseur prestataire</li>
	 * <li>fouPersNomPrenom : Libelle du fournisseur client</li>
	 * <li>lolfId : Nomenclature Lolf (anciennement destination)</li>
	 * <li>pourcentRemiseGlobal : Pourcentage de remise accorde sur le total</li>
	 * <li>devMontantHT : Montant HT du devis</li>
	 * <li>devMontantTTC : Montant TTC du devis</li>
	 * <li>devMontantTVA : Montant de la TVA pour le devis</li>
	 * <li>prestOrgId : Identifiant de la ligne budgetaire prestataire</li>
	 * <li>clientOrgId : Identifiant de la ligne budgetaire cliente</li>
	 * <li>clientTcdCode : Code du type de credit client</li>
	 * <li>fapIds : Identifiant des factures generees a partir du devis</li>
	 * <li>prestationLignes : Les lignes de la prestation, sous forme de NSArray de NSDictionary</li>
	 * </ul>
	 * <br>
	 *
	 * @param fouOrdre fouOrdre du client, moral ou physique (obligatoire si prestId null, inutile si prestId renseigne).
	 * @param persCliPersId PersId de l'individu client contact (facultatif).
	 * @param prestId Identifiant de la prestation a recuperer (facultatif).
	 * @return NSArray de NSDictionary
	 * @see WSPrestationFactory#getDictionaryFromEOPrestation(EOPrestation)
	 */
	public NSArray getDevis(Integer fouOrdre, Integer persCliPersId, Integer prestId) {
		setLastException(null);
		trace("Recuperation de devis...");
		try {
			// verifier si mot de passe OK
			checkAuthenticated();
			NSArray tmp = FinderPrestation.find(ec, fouOrdre, persCliPersId, prestId);
			NSMutableArray res = new NSMutableArray();
			for (int i = 0; i < tmp.count(); i++) {
				res.addObject(getDictionaryFromEOPrestation((EOPrestation) tmp.objectAtIndex(i)));
			}
			return res.immutableClone();
		} catch (Exception e) {
			new WSZException(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Transforme un objet EOPrestation en NSDictionary. Le NSDictionary contient les cles suivantes: <br>
	 * <ul>
	 * <li>prestId : Identifiant du devis</li>
	 * <li>prestNumero : numero unique du devis</li>
	 * <li>catId : Identifiant du catalogue</li>
	 * <li>devDateDevis : Date de creation du devis</li>
	 * <li>devDatePrestValide : Date de validation du devis par le prestataire</li>
	 * <li>devDateClientValide : Date de validation du devis par le client</li>
	 * <li>devLibelle : Libelle du devis</li>
	 * <li>fouOrdre : Identifiant du fournisseur client</li>
	 * <li>persId : Identifiant du client</li>
	 * <li>noIndividu : Identifiant du contact client</li>
	 * <li>fouOrdrePrest : Identifiant du fournisseur prestataire</li>
	 * <li>fouPersNomPrenom : Libelle du fournisseur client</li>
	 * <li>lolfId : Nomenclature Lolf (anciennement destination)</li>
	 * <li>pourcentRemiseGlobal : Pourcentage de remise accorde sur le total</li>
	 * <li>devMontantHT : Montant HT du devis</li>
	 * <li>devMontantTTC : Montant TTC du devis</li>
	 * <li>devMontantTVA : Montant de la TVA pour le devis</li>
	 * <li>prestOrgId : Identifiant de la ligne budgetaire prestataire</li>
	 * <li>clientOrgId : Identifiant de la ligne budgetaire cliente</li>
	 * <li>clientTcdCode : Code du type de credit client</li>
	 * <li>fapIds : Identifiant des factures generees a partir du devis</li>
	 * <li>prestationLignes : Les lignes de la prestation, sous forme de NSArray de NSDictionary</li>
	 * </ul>
	 * <br>
	 *
	 * @param prest
	 * @return
	 */
	protected NSDictionary getDictionaryFromEOPrestation(EOPrestation prest) {
		NSMutableDictionary resrec = new NSMutableDictionary();
		resrec.takeValueForKey(EOUtilities.primaryKeyForObject(ec, prest).valueForKey(EOPrestation.PREST_ID_KEY), "prestId");
		resrec.takeValueForKey(prest.prestNumero(), "prestNumero");
		resrec.takeValueForKey(prest.prestDate(), "devDateDevis");
		resrec.takeValueForKey(prest.prestDateValidePrest(), "devDatePrestValide");
		resrec.takeValueForKey(prest.prestDateValideClient(), "devDateClientValide");
		resrec.takeValueForKey(prest.prestLibelle(), "devLibelle");
		NSDictionary d = null;
		if (prest.organ() != null) {
			d = EOUtilities.primaryKeyForObject(ec, prest.organ());
			if (d != null) {
				resrec.takeValueForKey(d.valueForKey(EOOrgan.ORG_ID_KEY), "prestOrgId");
			}
		}
		if (prest.fournisUlr() != null) {
			d = EOUtilities.primaryKeyForObject(ec, prest.fournisUlr());
			if (d != null) {
				resrec.takeValueForKey(d.valueForKey(EOFournisUlr.FOU_ORDRE_KEY), "fouOrdre");
			}
		}
		if (prest.personne() != null) {
			d = EOUtilities.primaryKeyForObject(ec, prest.personne());
			if (d != null) {
				resrec.takeValueForKey(d.valueForKey(EOPersonne.PERS_ID_KEY), "persId");
			}
			resrec.takeValueForKey(prest.personne_persNomPrenom(), "fouPersNomPrenom");
		}
		if (prest.individuUlr() != null) {
			d = EOUtilities.primaryKeyForObject(ec, prest.individuUlr());
			if (d != null) {
				resrec.takeValueForKey(d.valueForKey(EOIndividuUlr.NO_INDIVIDU_KEY), "noIndividu");
			}
		}
		if (prest.catalogue() != null) {
			d = EOUtilities.primaryKeyForObject(ec, prest.catalogue());
			if (d != null) {
				resrec.takeValueForKey(d.valueForKey(EOCatalogue.CAT_ID_KEY), "catId");
			}
			if (prest.catalogue().fournisUlr() != null) {
				d = EOUtilities.primaryKeyForObject(ec, prest.catalogue().fournisUlr());
				if (d != null) {
					resrec.takeValueForKey(d.valueForKey(EOFournisUlr.FOU_ORDRE_KEY), "fouOrdrePrest");
				}
			}
		}
		if (prest.lolfNomenclatureRecette() != null) {
			d = EOUtilities.primaryKeyForObject(ec, prest.lolfNomenclatureRecette());
			if (d != null) {
				resrec.takeValueForKey(d.valueForKey(EOLolfNomenclatureRecette.LOLF_ID_KEY), "lolfId");
			}
		}

		if (prest.prestationBudgetClient() != null && prest.prestationBudgetClient().organ() != null) {
			d = EOUtilities.primaryKeyForObject(ec, prest.prestationBudgetClient().organ());
			if (d != null) {
				resrec.takeValueForKey(d.valueForKey(EOOrgan.ORG_ID_KEY), "clientOrgId");
			}
		}

		if (prest.prestationBudgetClient() != null && prest.prestationBudgetClient().typeCreditDep() != null) {
			resrec.takeValueForKey(prest.prestationBudgetClient().typeCreditDep().tcdCode(), "clientTcdCode");
		}

		resrec.takeValueForKey(prest.prestRemiseGlobale(), "pourcentRemiseGlobal");

		// Ajouter des infos sur les totaux
		resrec.takeValueForKey(prest.prestTotalHt(), "devMontantHT");
		resrec.takeValueForKey(prest.prestTotalTtc(), "devMontantTTC");
		resrec.takeValueForKey(prest.prestTotalTva(), "devMontantTVA");

		// Ajouter les infos sur les factures associees
		if ((prest.facturePapiers() != null) && (prest.facturePapiers().count() > 0)) {
			NSMutableArray objs = new NSMutableArray();
			for (int i = 0; i < prest.facturePapiers().count(); i++) {
				d = EOUtilities.primaryKeyForObject(ec, (EOFacturePapier) prest.facturePapiers().objectAtIndex(i));
				if (d != null) {
					objs.addObject(d.valueForKey(EOFacturePapier.FAP_ID_KEY));
				}
			}
			resrec.takeValueForKey(objs, "fapIds");
		}

		// Ajoute les lignes du devis
		NSArray a = prest.prestationLignes();
		if (a != null && a.count() > 0) {
			NSMutableArray objs = new NSMutableArray(a.count());
			for (int i = 0; i < a.count(); i++) {
				objs.addObject(getDictionaryFromEOPrestationLigne((EOPrestationLigne) a.objectAtIndex(i)));
			}
			resrec.takeValueForKey(objs, "prestationLignes");
		}

		return resrec.immutableClone();
	}

	/**
	 * Transforme un objet EOPrestationLigne en NSDictionary.
	 *
	 * @param prlig
	 * @return
	 */
	protected NSDictionary getDictionaryFromEOPrestationLigne(EOPrestationLigne prlig) {
		NSMutableDictionary resrec = new NSMutableDictionary();
		if (prlig.catalogueArticle() != null) {
			resrec.takeValueForKey(prlig.catalogueArticle().article().typeArticle().tyarLibelle(), "cartpType");
			NSDictionary d = EOUtilities.primaryKeyForObject(ec, prlig.catalogueArticle());
			if (d != null) {
				resrec.takeValueForKey(d.valueForKey(EOCatalogueArticle.CAAR_ID_KEY), "caarId");
			}
		}
		resrec.takeValueForKey(prlig.prligArtHt(), "dligArtHt");
		resrec.takeValueForKey(prlig.prligArtTtc(), "dligArtTtc");
		resrec.takeValueForKey(prlig.prligTotalHt(), "devisLigneMontantHT");
		resrec.takeValueForKey(prlig.prligTotalTva(), "devisLigneMontantTVA");
		resrec.takeValueForKey(prlig.prligTotalTtc(), "devisLigneMontantTTC");
		resrec.takeValueForKey(prlig.prligDescription(), "dligDescription");
		resrec.takeValueForKey(prlig.prligQuantite(), "dligNbArticles");
		resrec.takeValueForKey(prlig.prligQuantiteReste(), "dligNbResteAFacturer");
		resrec.takeValueForKey(prlig.prligReference(), "dligReference");
		if (prlig.tva() != null) {
			resrec.takeValueForKey(prlig.tva().tvaTaux(), "tauCode");
		}
		return resrec.immutableClone();
	}

	/**
	 * Renvoie les articles valides d'un catalogue sous forme d'un NSArray de NSDictionary Les cles sont les suivantes: <br>
	 * <ul>
	 * <li>caarId : identifiant de l'article</li>
	 * <li>catId : identifiant du catalogue</li>
	 * <li>cartDescription : description de l'article</li>
	 * <li>cartHt : prix HT de l'article</li>
	 * <li>cartReference : Reference de l'article</li>
	 * <li>cartpType : type de l'article (A/O/R)</li>
	 * <li>tauTaux : Pourcentage de la TVA</li>
	 * <li>cartInvisible : Invisible pour le web</li>
	 * <li>cartValide : Validite de l'article</li>
	 * <li>pcoNum : Planco Recette</li>
	 * <li>cartQteMin : Dans le cas d'une remise, quantite d'articles commandes a partir de laquelle cette remise est appliquee</li>
	 * <li>cartQteMax : Dans le cas d'une remise, quantite d'articles commandes jusqu'a laquelle cette remise est appliquee</li>
	 * </ul>
	 */
	public NSArray getCatalogueArticles(Integer catId) {
		setLastException(null);
		try {
			checkAuthenticated();
			NSArray res1 = FinderCatalogueArticle.find(
					ec,
					(EOCatalogue) EOUtilities.objectWithPrimaryKeyValue(ec, EOCatalogue.ENTITY_NAME, catId), null,
					FinderTypeEtat.typeEtatValide(ec));
			NSMutableArray res = new NSMutableArray();
			for (int i = 0; i < res1.count(); i++) {
				res.addObject(getDictionaryFromEOCatalogueArticle((EOCatalogueArticle) res1.objectAtIndex(i)));
			}
			return res.immutableClone();
		} catch (Exception e) {
			new WSZException(e);
			return null;
		}
	}

	/**
	 * Transforme un objet EOCatalogueArticle en NSDictionary. <br>
	 * Les cles sont les suivantes: <br>
	 * <ul>
	 * <li>caarId : identifiant de l'article</li>
	 * <li>catId : identifiant du catalogue</li>
	 * <li>cartDescription : description de l'article</li>
	 * <li>cartHt : prix HT de l'article</li>
	 * <li>cartReference : Reference de l'article</li>
	 * <li>cartpType : type de l'article (A/O/R)</li>
	 * <li>tauTaux : Pourcentage de la TVA</li>
	 * <li>cartInvisible : Invisible pour le web</li>
	 * <li>cartValide : Validite de l'article</li>
	 * <li>pcoNum : Planco Recette</li>
	 * <li>cartQteMin : Dans le cas d'une remise, quantite d'articles commandes a partir de laquelle cette remise est appliquee</li>
	 * <li>cartQteMax : Dans le cas d'une remise, quantite d'articles commandes jusqu'a laquelle cette remise est appliquee</li>
	 * </ul>
	 * <br>
	 *
	 * @param art
	 * @return
	 */
	protected final NSDictionary getDictionaryFromEOCatalogueArticle(EOCatalogueArticle art) {
		NSMutableDictionary resrec = new NSMutableDictionary();
		NSDictionary d = null;
		if (art != null) {
			d = EOUtilities.primaryKeyForObject(ec, art);
			if (d != null) {
				resrec.takeValueForKey(d.valueForKey(EOCatalogueArticle.CAAR_ID_KEY), "caarId");
			}
		}
		if (art.catalogue() != null) {
			d = EOUtilities.primaryKeyForObject(ec, art.catalogue());
			if (d != null) {
				resrec.takeValueForKey(d.valueForKey(EOCatalogue.CAT_ID_KEY), "catId");
			}
		}
		resrec.takeValueForKey(art.article().artLibelle(), "cartDescription");
		resrec.takeValueForKey(art.caarPrixHt(), "cartHt");
		resrec.takeValueForKey(art.caarPrixTtc(), "cartTtc");
		resrec.takeValueForKey(art.caarReference(), "cartReference");
		resrec.takeValueForKey(art.article().typeArticle().tyarLibelle(), "cartpType");
		resrec.takeValueForKey(art.tva().tvaTaux(), "tauTaux");
		resrec.takeValueForKey(art.article().articlePrestation().artpInvisibleWeb(), "cartInvisible");
		resrec.takeValueForKey(art.typeEtat().tyetLibelle(), "cartValide");
		if (art.article() != null && art.article().articlePrestation() != null
				&& art.article().articlePrestation().pcoNumRecette() != null) {
			resrec.takeValueForKey(art.article().articlePrestation().pcoNumRecette(), "pcoNum");
		} else {
			resrec.takeValueForKey(null, "pcoNum");
		}
		resrec.takeValueForKey(art.article().articlePrestation().artpQteMin(), "cartQteMin");
		resrec.takeValueForKey(art.article().articlePrestation().artpQteMax(), "cartQteMax");

		return resrec.immutableClone();
	}

	/**
	 * @return Les types de credit autorises pour les prestations (vecteur de Integer)
	 */
	public java.util.Vector getTypcredAutorises() {
		return ((NSArray) new NSArray(new String[] {
				"10", "40", "50"
		})).vector();
	}

	/**
	 * Tente d'enregistrer les modifications de l'editingContext.
	 *
	 * @return L'identifiant du devis en cours (prestId) et son numero (prestNumero).
	 */
	public Hashtable saveChanges() {
		try {
			if (currentPrestation == null) {
				throw new Exception("Le devis en cours est nul. Rien a enregistrer.");
			}
			try {
				if (currentPrestation.prestNumero() == null) {
					currentPrestation.setPrestNumero(Integer.valueOf(GetNumerotation
							.get(session.dataBus(), ec, currentPrestation.exercice(), null, "PRESTATION").intValue()));
				}
				ec.saveChanges();
				trace("succes saveChanges");
			} catch (Exception e) {
				throw new Exception("Impossible d'enregistrer les modifications : " + e.getMessage());
			}
			NSDictionary dico = EOUtilities.primaryKeyForObject(ec, currentPrestation);
			if (dico != null) {
				Hashtable h = new Hashtable(2);
				h.put("prestId", (Integer) dico.valueForKey(EOPrestation.PREST_ID_KEY));
				h.put("prestNumero", currentPrestation.prestNumero());
				return h;
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			new WSZException(e.getMessage());
			return null;
		}
	}

}