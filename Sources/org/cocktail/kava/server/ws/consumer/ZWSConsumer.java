/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.server.ws.consumer;

import java.util.Iterator;
import java.util.Map;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.webservices.client.WOWebServiceClient;

/**
 * Classe abstraite pour les clients de Webservices.
 * 
 * @version 1.0.0
 */
public abstract class ZWSConsumer {

	/**
	 * operations qui doivent etre accessibles sur le WebService pour pouvoir utiliser cette classe.
	 */
	public static final String[]										REQUIREDOPERATIONS	= { "getLastExceptionMessage", "getProperties" };

	private WOWebServiceClient											wsClient;
	private String														applicationAlias;
	private String														wsPassword;
	private boolean														isConnected;
	private String														wsName;
	private String														wsUrl;

	/**
	 * On garde une reference a la session distante. Ca permet de reaffecter la session d'un autre webservice heberge sur la meme application, ce
	 * qui permet au deux webservices de se partager des infos puisqu'ils se retrouvent sur la meme session.
	 */
	private com.webobjects.webservices.client.WOWebService.SessionInfo	mySessionInfo;

	/**
	 * Constructeur.
	 * 
	 * @param pwsName
	 *            Nom du WebService
	 * @param pwsUrl
	 *            Url d'acces au WebService
	 * @param password
	 *            Mot de passe pour utiliser le WebService
	 * @param appAlias
	 *            Alias representant votre application (utilisee par le WebService pour enregistrer votre client, et les logs).
	 * @throws Exception
	 */
	public ZWSConsumer(String pwsName, String pwsUrl, String password, String appAlias) throws Exception {
		super();
		isConnected = false;
		wsName = pwsName;
		wsUrl = pwsUrl;
		applicationAlias = appAlias;
		wsPassword = password;
		wsClient = new WOWebServiceClient(new java.net.URL(wsUrl));
		checkWSRequirements();
	}

	/**
	 * Tente de se connecter au webService.
	 */
	public void connect() throws Exception {
		// Appeler une methode de base pour creer une session distante
		Object[] wsArgs = { applicationAlias, wsPassword };
		wsClient.invoke(wsName, "connect", wsArgs);
		// verifier s'il y a eu des erreurs
		throwLastWsException();
		// Memoriser la session
		mySessionInfo = wsClient.sessionInfoForServiceNamed(wsName);
		isConnected = true;
	}

	/**
	 * Se deconnecter du Webservice (a appeler eventuellement lorsque toutes les operations sont effectuees). Cette action supprime la session
	 * sur le serveur.
	 * 
	 * @throws Exception
	 */
	public void deconnect() throws Exception {
		if (!isConnected()) {
			throw new Exception("La connexion au service" + wsName + " n'est pas active. Impossible de se deconnecter.");
		}
//		wsClient.invoke(wsName, "deconnect", null);
//		throwLastWsException();
		setConnected(false);
	}

	/**
	 * Indique si on est connecte au webservice/
	 * 
	 * @return True ou False selon connecte ou pas
	 */
	public boolean isConnected() {
		return isConnected;
	}

	/**
	 * @param b
	 */
	protected void setConnected(boolean b) {
		isConnected = b;
	}

	/**
	 * recupere le message de la derniere exception generee par le webService.
	 */
	protected void throwLastWsException() throws Exception {
		String s = (String) wsClient.invoke(wsName, "getLastExceptionMessage", null);
		if (s != null) {
			throw new Exception("[" + wsName + "] " + s);
		}
	}

	/**
	 * @return
	 */
	private String[] requiredOperations() {
		return REQUIREDOPERATIONS;
	}

	/**
	 * Verifie que le WebService possede les operations requises a l'utilisation de ce client.
	 * 
	 * @throws Exception
	 */
	private void checkWSRequirements() throws Exception {
		NSDictionary dic = wsClient.operationsDictionaryForService(wsName);
		for (int i = 0; i < requiredOperations().length; i++) {
			String string = requiredOperations()[i];
			if (dic.valueForKey(string) == null) {
				throw new Exception("L'operation " + string + " n'est pas definie pour le WebService " + wsName
						+ ". Votre classe d'invocation ne peut pas heriter de ZWSInvocator.");
			}
		}
	}

	/**
	 * Genere une exception si isConnected() renvoie false.
	 */
	protected void checkConnection() throws Exception {
		if (!isConnected()) {
			throw new Exception("La connexion au service" + getWsName() + " n'est pas active");
		}
	}

	/**
	 * methode utilitaire qui convertit un tableau d'objets Map en NSArray d'objets NSDictionary.
	 * 
	 * @param objs
	 *            Tableau de Map
	 * @return array
	 */
	public NSArray arrayMapsToNSArrayNSDictionarys(Object[] objs) throws Exception {
		// On transforme le tableau de map en NSArray de NSDictionary
		NSMutableArray tmpres = new NSMutableArray();
		for (int i = 0; i < objs.length; i++) {
			if (objs[i] instanceof Map) {
				Map object = (Map) objs[i];
				tmpres.addObject(mapToNSDictionary(object));
			}
			else {
				tmpres.addObject(objs[i]);
			}
		}
		return tmpres.immutableClone();
	}

	public NSDictionary mapToNSDictionary(Object obj) throws Exception {
		Map object = (Map) obj;
		NSMutableDictionary res = new NSMutableDictionary(object.values().size());
		Iterator itKeys = object.keySet().iterator();
		Iterator itObj = object.values().iterator();
		while (itObj.hasNext()) {
			Object o = itObj.next();
			if (o instanceof Object[]) {
				o = arrayMapsToNSArrayNSDictionarys((Object[]) o);
			}
			res.setObjectForKey(o == null ? NSKeyValueCoding.NullValue : o, itKeys.next());
		}
		return res.immutableClone();
		// return new NSDictionary(object.values().toArray(), object.keySet().toArray());
	}

	/**
	 * Equivaut a wsClient.invoke(getWsName(),operationName,args);
	 * 
	 * @param operationName
	 * @param args
	 * @return Un objet resultat s'il y a lieu
	 */
	public Object wsInvoke(String operationName, Object[] args) {
		return wsClient.invoke(getWsName(), operationName, args);
	}

	/**
	 * @return String
	 */
	public String getApplicationAlias() {
		return applicationAlias;
	}

	/**
	 * @return WOWebService.SessionInfo
	 */
	public com.webobjects.webservices.client.WOWebService.SessionInfo getMySessionInfo() {
		return mySessionInfo;
	}

	/**
	 * @return L'instance du client
	 */
	public WOWebServiceClient getWsClient() {
		return wsClient;
	}

	/**
	 * @return Le nom du WebService
	 */
	public String getWsName() {
		return wsName;
	}

	/**
	 * @return L'url du WebService.
	 */
	public String getWsUrl() {
		return wsUrl;
	}

	/**
	 * @return Les infos du Webservice contenant le no de version, etc. sous forme de Map
	 */
	public Map getProperties() {
		return (Map) wsInvoke("getProperties", null);
	}
}
