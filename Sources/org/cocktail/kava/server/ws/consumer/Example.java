/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.server.ws.consumer;

import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.Vector;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;

public class Example {

	private static final String	WSHostname		= "www.univ-lr.fr";
	private static final String	WSPortNumber	= "1381";
	private static final String	WSAdaptor		= "/cgi-bin/WebObjects/PieTest.woa/ws/";

	private static final String	WSName			= "WSPrestation";
	private static final String	WSPassword		= "test";

	public Example() {
		super();

		log("Here we go...");

		try {
			// Connexion au WebServicePrestation
			String WSURL = "http://" + WSHostname + WSAdaptor + WSName + "?wsdl";
			log("Tentative de connexion au WS " + WSURL);
			WSPrestationFactoryConsumer wsp = new WSPrestationFactoryConsumer(WSName, WSURL, WSPassword, this.getClass().getName());
			log("checkWebService prestation result = \n" + wsp.checkWebService());
			wsp.connect();

			// chargement de devis sous forme de nsarray
			log("getDevis...");
			NSArray a = wsp.getDevis(new Integer(65542), null, null);
			log("a = " + a);

			// chargement d'une liste d'articles d'un catalogue
			log("getCatalogueArticles...");
			NSArray articles = wsp.getCatalogueArticles(new Integer(249));
			log("articles = " + articles);

			// recup des types de credit autorises
			log("getCatalogueArticles...");
			Vector vec = wsp.getTypcredAutorises();
			log("types de credit autorises = " + vec);

			// creation d'un devis avec 2 articles...
			log("Attention creation !!!");
			wsp.createCurrentDevisComplet(null, wsp.TYPE_PUBLIC_EXTERNE_PUBLIC, new Integer(64724), null, new Integer(3892),
					"Juste un test !!!", null, null, null, new Integer(78), null, null, null);
			wsp.ajouteLignePourArticleInCurrentDevis(new Integer(288223), new BigDecimal(20.0));
			wsp.ajouteLignePourArticleInCurrentDevis(new Integer(288224), new BigDecimal(2.5));
			Integer prestId = (Integer) wsp.saveChanges().get("prestId");
			log("cle du devis nouvellement cree: " + prestId);

			// chargement d'un devis et verifs...
			log("Chargement d'un devis et verif. devis valide client? prestataire? realise?...");
			wsp.chargeCurrentDevisForClient(prestId, new Integer(64724));
			log("Devis valide cote client ? " + (wsp.currentDevisValideCoteClient().booleanValue() ? "OUI" : "NON"));
			log("Devis valide cote prestataire ? " + (wsp.currentDevisValideCotePrestataire().booleanValue() ? "OUI" : "NON"));
			log("Devis realise ? " + (wsp.currentPrestationRealisee().booleanValue() ? "OUI" : "NON"));

			// ...puis validation totale...
			wsp.valideToutCurrentDevis(new Integer(7));
			log("Devis valide cote client ? " + (wsp.currentDevisValideCoteClient().booleanValue() ? "OUI" : "NON"));
			log("Devis valide cote prestataire ? " + (wsp.currentDevisValideCotePrestataire().booleanValue() ? "OUI" : "NON"));
			log("Devis realise ? " + (wsp.currentPrestationRealisee().booleanValue() ? "OUI" : "NON"));

			// ...puis impression devis...
			log("Impression devis...");
			NSData data = wsp.imprimerCurrentDevis();
			try {
				FileOutputStream fileOutputStream = new FileOutputStream("devis.pdf");
				data.writeToStream(fileOutputStream);
				fileOutputStream.close();
			}
			catch (java.io.IOException e) {
				e.printStackTrace();
			}

			// puis generation facture papier...
			log("Generation de la facture...");
			wsp.genererFactureForCurrentDevis(new Integer(7));

			// Evaluation d'un montant...
			log("Evaluation d'un montant : "
					+ wsp.getMontantTTCForDevis(new Integer[] { new Integer(288223), new Integer(288224) }, new BigDecimal[] {
							new BigDecimal(10.0), new BigDecimal(2.0) }));

			// Disponible sur une ligne budgetaire et un type de credit...
			log("Disponible : " + wsp.getDisponible(new Integer(102624), "10"));

			// Deconnexion du WebService (ferme la session)
			wsp.deconnect();
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		log("That's all folks :-)");
	}

	public static void main(String[] args) {
		new Example();
	}

	private void log(Object o) {
		System.out.println("[" + this.getClass().getName() + "] " + o.toString());
	}

}
