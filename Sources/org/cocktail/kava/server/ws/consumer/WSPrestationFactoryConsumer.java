/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.server.ws.consumer;

import java.math.BigDecimal;
import java.util.Hashtable;

//import org.cocktail.kava.server.metier.EOPrestation;
//import org.cocktail.kava.server.ws.WSPrestationFactory;

import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;

/**
 * Classe permettant de se connecter au WebService "WSPrestationFactoryProvider". <br>
 * Cette classe est fournie pour simplifier les appels au service web, et mapper les differentes operations fournies par le service. <br>
 * <br>
 * Les etapes des devis/prestations: creation, validation client (avec generation d'un engagement si c'est de la prestation interne), validation
 * prestataire, cleture, generation de la facture.<br>
 * <br>
 * Exemple d'utilisation :<br>
 * 
 * <code>
 * 	private static final String	WSHostname		= "localhost";<br>
 *  private static final String	WSPortNumber	= "1381";<br>
 *  private static final String	WSAdaptor		= "/cgi-bin/WebObjects/Pie.woa/ws/";<br>
 *  <br>
 *  private static final String	WSName			= "WSPrestation";<br>
 *  private static final String	WSPassword		= "xxx";<br>
 *  <br>
 *  // Connexion au WebServicePrestation<br>
 *  String WSURL = "http://" + WSHostname + ":" + WSPortNumber + WSAdaptor + WSName + "?wsdl";<br>
 *  log("Tentative de connexion au WS " + WSURL);<br>
 *  WSPrestationFactoryConsumer wsp = new WSPrestationFactoryConsumer(WSName, WSURL, WSPassword, this.getClass().getName());<br>
 *  log("checkWebService prestation result = \n" + wsp.checkWebService());<br>
 *  wsp.connect();<br>
 *  <br>
 *  // creation d'un devis avec 2 articles...<br>
 *  log("Attention creation !!!");<br>
 *  wsp.createCurrentDevisComplet(null, wsp.TYPE_PUBLIC_EXTERNE_PUBLIC, new Integer(64724), null, new Integer(3892), "Juste un test !!!",
 *  null, null, null, new Integer(78), null, null, null);<br>
 *  wsp.ajouteLignePourArticleInCurrentDevis(new Integer(288223), new BigDecimal(20.0));<br>
 *  wsp.ajouteLignePourArticleInCurrentDevis(new Integer(288224), new BigDecimal(2.5));<br>
 *  Integer prestId = wsp.saveChanges();<br>
 *  log("cle du devis nouvellement cree: " + prestId);<br>
 *  <br>
 *  // chargement d'un devis et verifs...<br>
 *  log("Chargement d'un devis et verif. devis valide client? prestataire? realise?...");<br>
 *  wsp.chargeCurrentDevisForClient(prestId, new Integer(64724));<br>
 *  log("Devis valide cote client ? " + (wsp.currentDevisValideCoteClient().booleanValue() ? "OUI" : "NON"));<br>
 *  log("Devis valide cote prestataire ? " + (wsp.currentDevisValideCotePrestataire().booleanValue() ? "OUI" : "NON"));<br>
 *  log("Devis realise ? " + (wsp.currentPrestationRealisee().booleanValue() ? "OUI" : "NON"));<br>
 *  <br>
 *  // ...puis validation totale...<br>
 *  wsp.valideToutCurrentDevis(new Integer(7));<br>
 *  <br>
 *  // ...puis impression devis...<br>
 *  log("Impression devis...");<br>
 *  NSData data = wsp.imprimerCurrentDevis();<br>
 *  try {<br>
 *  	FileOutputStream fileOutputStream = new FileOutputStream("devis.pdf");<br>
 *  	data.writeToStream(fileOutputStream);<br>
 *  	fileOutputStream.close();<br>
 *  }<br>
 *  catch (java.io.IOException e) {<br>
 *  	e.printStackTrace();<br>
 *  }<br>
 *  <br>
 *  // puis generation facture papier...<br>
 *  log("Generation de la facture...");<br>
 *  wsp.genererFactureForCurrentDevis(new Integer(7));<br>
 *  <br>
 *  // Evaluation d'un montant...<br>
 *  log("Evaluation d'un montant : "<br>
 *  + wsp.getMontantTTCForDevis(new Integer[] { new Integer(288223), new Integer(288224) }, new BigDecimal[] {
 *  new BigDecimal(10.0), new BigDecimal(2.0) }));<br>
 *  <br>
 *  // Disponible sur une ligne budgetaire et un type de credit...<br>
 *  log("Disponible : " + wsp.getDisponible(new Integer(2007), new Integer(102624), new Integer(84)));<br>
 *  <br>
 *  // Deconnexion du WebService (ferme la session)<br>
 *  wsp.deconnect();<br>
 * </code>
 * 
 * @version 1.0.0
 * @see ZWSConsumer
 */
public class WSPrestationFactoryConsumer extends ZWSConsumer {

	public final Integer	TYPE_PUBLIC_INTERNE				= new Integer(1);
	public final Integer	TYPE_PUBLIC_EXTERNE_INTRANET	= new Integer(2);
	public final Integer	TYPE_PUBLIC_EXTERNE_PUBLIC		= new Integer(3);
	public final Integer	TYPE_PUBLIC_EXTERNE_PRIVE		= new Integer(4);
	public final Integer	TYPE_PUBLIC_FORMATION_CONTINUE	= new Integer(5);

	/**
	 * Constructeur.
	 * 
	 * @param pwsName
	 *            Nom du WebService
	 * @param pwsUrl
	 *            Url d'acces au WebService
	 * @param password
	 *            Mot de passe pour utiliser le WebService
	 * @param appAlias
	 *            Alias representant votre application (utilisee par le WebService pour enregistrer votre client, et les logs).
	 * @throws Exception
	 */
	public WSPrestationFactoryConsumer(String pwsName, String pwsUrl, String password, String appAlias) throws Exception {
		super(pwsName, pwsUrl, password, appAlias);
	}

	/**
	 * Teste si le WebService tourne (exception si non) et renvoie les infos sur le WebService
	 * 
	 * @return La chaine des parametres de version du WebService, format: ws.status=YES/NO app.version="" app.ver.num="" app.ver.date=""
	 * @throws Exception
	 */
	public String checkWebService() throws Exception {
		String res = (String) wsInvoke("checkWebService", null);
		throwLastWsException();
		return res;
	}

	/**
	 * @return la version du WebService sous forme d'une chaine x.x.x (x = chiffre)
	 * @throws Exception
	 */
	public String version() throws Exception {
		String res = (String) wsInvoke("getWebServiceVersion", null);
		throwLastWsException();
		return res;
	}

	/**
	 * Cree un devis et l'affecte comme devis courant. (Le devis n'est pas enregistre dans la base).
	 * 
	 * @param utlOrdre
	 *            Utilisateur qui Cree le devis (facultatif)
	 * @param typePublicKey
	 *            TYPE_PUBLIC_xxx (obligatoire)
	 * @param fouOrdre
	 *            fouOrdre du client (obligatoire)
	 * @param contactPersId
	 *            persId de l'individu client (contact) (facultatif)
	 * @param catId
	 *            Identifiant du catalogue (obligatoire)
	 * @param devLibelle
	 *            Libelle du devis (obligatoire)
	 * @param commentairePrest
	 *            Commentaire a imprimer en bas de devis (facultatif)
	 */
	public void createCurrentDevis(Integer utlOrdre, Integer typePublicKey, Integer fouOrdre, Integer contactPersId, Integer catId,
			String devLibelle, String commentairePrest) throws Exception {
		if (!isConnected()) {
			throw new Exception("La connexion au service" + getWsName() + " n'est pas active");
		}
		Object[] wsArgs = { utlOrdre, typePublicKey, fouOrdre, contactPersId, catId, devLibelle, commentairePrest };
		getWsClient().invoke(getWsName(), "createCurrentDevis", wsArgs);
		throwLastWsException();
	}

	/**
	 * Cree un devis et l'affecte comme devis courant. (Le devis n'est pas enregistre dans la base). Si les donnees budgetaires ne sont pas
	 * renseignees, elles sont determinees en auto si possible a partir des infos budgetaires par defaut du catalogue.
	 * 
	 * @param utlOrdre
	 *            Utilisateur qui Cree le devis (facultatif)
	 * @param typePublicKey
	 *            TYPE_PUBLIC_xxx (obligatoire)
	 * @param fouOrdre
	 *            fouOrdre du client (obligatoire)
	 * @param contactPersId
	 *            persId de l'individu client (contact) (facultatif)
	 * @param catId
	 *            Identifiant du catalogue (obligatoire)
	 * @param devLibelle
	 *            Libelle du devis (obligatoire)
	 * @param commentairePrest
	 *            Commentaire a imprimer en bas de devis (facultatif)
	 * @param orgId
	 *            Ligne budgetaire recette (facultatif)
	 * @param tapId
	 *            Taux de prorata recette (facultatif)
	 * @param tcdOrdre
	 *            Type de credit recette (facultatif)
	 * @param lolfId
	 *            Action lolf recette (facultatif)
	 * @param pcoNum
	 *            Plan comptable recette (facultatif)
	 * @param modOrdre
	 *            Mode de recouvrement (facultatif)
	 */
	public void createCurrentDevisComplet(Integer utlOrdre, Integer typePublicKey, Integer fouOrdre, Integer contactPersId, Integer catId,
			String devLibelle, String commentairePrest, Integer orgId, Integer tapId, Integer tcdOrdre, Integer lolfId, String pcoNum,
			Integer modOrdre) throws Exception {
		if (!isConnected()) {
			throw new Exception("La connexion au service" + getWsName() + " n'est pas active");
		}
		Object[] wsArgs = { utlOrdre, typePublicKey, fouOrdre, contactPersId, catId, devLibelle, commentairePrest, orgId, tapId, tcdOrdre,
				lolfId, pcoNum, modOrdre };
		getWsClient().invoke(getWsName(), "createCurrentDevisComplet", wsArgs);
		throwLastWsException();
	}

	/**
	 * Cree un devis avec un article et l'affecte comme devis courant. (Le devis n'est pas enregistre dans la base).
	 * 
	 * @param utlOrdre
	 *            Utilisateur qui Cree le devis (facultatif)
	 * @param typePublicKey
	 *            TYPE_PUBLIC_xxx (obligatoire)
	 * @param fouOrdre
	 *            fouOrdre du client (obligatoire)
	 * @param contactPersId
	 *            persId de l'individu client (contact) (facultatif)
	 * @param catId
	 *            Identifiant du catalogue (obligatoire)
	 * @param devLibelle
	 *            Libelle du devis (obligatoire)
	 * @param commentairePrest
	 *            Commentaire a imprimer en bas de devis (facultatif)
	 * @param caarId
	 *            Article a positionner dans ce devis (table jefy_catalogue.catalogue_article) (obligatoire)
	 * @param quantite
	 *            Quantite de cet article (facultatif, si null, quantite par defaut = 1)
	 */
	public void createCurrentDevisArticle(Integer utlOrdre, Integer typePublicKey, Integer fouOrdre, Integer contactPersId, Integer catId,
			String devLibelle, String commentairePrest, Integer caarId, BigDecimal quantite) throws Exception {
		if (!isConnected()) {
			throw new Exception("La connexion au service" + getWsName() + " n'est pas active");
		}
		Object[] wsArgs = { utlOrdre, typePublicKey, fouOrdre, contactPersId, catId, devLibelle, commentairePrest, caarId, quantite };
		getWsClient().invoke(getWsName(), "createCurrentDevisArticle", wsArgs);
		throwLastWsException();
	}

	/**
	 * Ajoute une ligne au devis en cours. Le devis en cours doit etre charge ou cree.
	 * 
	 * @param caarId
	 *            Identifiant de l'article (obligatoire)
	 * @param quantite
	 *            j'crois que c'est clair (facultatif, si null, quantite par defaut = 1)
	 */
	public void ajouteLignePourArticleInCurrentDevis(Integer caarId, BigDecimal quantite) throws Exception {
		if (!isConnected()) {
			throw new Exception("La connexion au service" + getWsName() + " n'est pas active");
		}
		Object[] wsArgs = { caarId, quantite };
		wsInvoke("ajouteLignePourArticleInCurrentDevis", wsArgs);
		throwLastWsException();
	}

	/**
	 * Ajoute une ligne au devis en cours. Le devis en cours doit etre charge ou cree.
	 * 
	 * @param reference
	 * @param description
	 * @param prixHt
	 * @param prixTtc
	 * @param prixTotalHt
	 * @param prixTotalTtc
	 * @param quantite
	 */
	public void ajouteLignePourArticleInCurrentDevis(String reference, String description, BigDecimal prixHt, 
			BigDecimal prixTtc, BigDecimal prixTotalHt, BigDecimal prixTotalTtc, BigDecimal quantite) throws Exception {
		if (!isConnected()) {
			throw new Exception("La connexion au service" + getWsName() + " n'est pas active");
		}
		Object[] wsArgs = { reference, description, prixHt, prixTtc, prixTotalHt, prixTotalTtc, quantite };
		wsInvoke("ajouteLignePourArticleInCurrentDevis", wsArgs);
		throwLastWsException();
	}

	/**
	 * Tente d'enregistrer les modifications de l'editingContext.
	 * 
	 * @return L'identifiant du devis en cours (prestId) et son numero (prestNumero).
	 */
	public Hashtable saveChanges() throws Exception {
		if (!isConnected()) {
			throw new Exception("La connexion au service" + getWsName() + " n'est pas active");
		}
		Hashtable res = mapToNSDictionary((java.util.Map) wsInvoke("saveChanges", null)).hashtable();
		throwLastWsException();
		return res;
	}

	// public NSArray getCatalogues(Integer fouOrdre, String typeUser) throws Exception {
	// Object[] wsArgs = { fouOrdre, typeUser };
	// Object[] res = (Object[]) wsInvoke("getCatalogues", wsArgs);
	// throwLastWsException();
	// return arrayMapsToNSArrayNSDictionarys(res);
	// }
	//
	// public NSArray getDevisLignes(Integer devOrdre) throws Exception {
	// Object[] wsArgs = { devOrdre };
	// Object[] res = (Object[]) wsInvoke("getDevisLignes", wsArgs);
	// throwLastWsException();
	// return arrayMapsToNSArrayNSDictionarys(res);
	// }
	//
	// public NSArray getDevisForConvOrdre(Integer convOrdre) throws Exception {
	// Object[] wsArgs = { convOrdre };
	// Object[] res = (Object[]) wsInvoke("getDevisForConvOrdre", wsArgs);
	// throwLastWsException();
	// return arrayMapsToNSArrayNSDictionarys(res);
	// }

	/**
	 * Renvoie les articles valides d'un catalogue sous forme d'un NSArray de NSDictionary Les cles sont les suivantes: <br>
	 * <ul>
	 * <li>caarId : identifiant de l'article</li>
	 * <li>catId : identifiant du catalogue</li>
	 * <li>cartDescription : description de l'article</li>
	 * <li>cartHt : prix HT de l'article</li>
	 * <li>cartReference : Reference de l'article</li>
	 * <li>cartpType : type de l'article (A/O/R)</li>
	 * <li>tauTaux : Pourcentage de la TVA</li>
	 * <li>cartInvisible : Invisible pour le web</li>
	 * <li>cartValide : Validite de l'article</li>
	 * <li>pcoNum : Planco Recette</li>
	 * <li>cartQteMin : Dans le cas d'une remise, quantite d'articles commandes a partir de laquelle cette remise est appliquee</li>
	 * <li>cartQteMax : Dans le cas d'une remise, quantite d'articles commandes jusqu'a laquelle cette remise est appliquee</li>
	 * </ul>
	 */
	public NSArray getCatalogueArticles(Integer catId) throws Exception {
		Object[] wsArgs = { catId };
		Object[] res = (Object[]) wsInvoke("getCatalogueArticles", wsArgs);
		throwLastWsException();
		return arrayMapsToNSArrayNSDictionarys(res);
	}

	/**
	 * recupere les devis d'un client particulier et les renvoie sous forme de NSArray de NSDictionary. <br>
	 * En specifiant prestId, on recupere un NSArray a un seul element. <br>
	 * <br>
	 * Les NSDictionary sont recuperes par {@link WSPrestationFactory#getDictionaryFromEOPrestation(EOPrestation)}. Le NSDictionary contient les
	 * cles suivantes: <br>
	 * <ul>
	 * <li>prestId : Identifiant du devis</li>
	 * <li>prestNumero : numero unique du devis</li>
	 * <li>catId : Identifiant du catalogue</li>
	 * <li>devDateDevis : Date de creation du devis</li>
	 * <li>devDatePrestValide : Date de validation du devis par le prestataire</li>
	 * <li>devDateClientValide : Date de validation du devis par le client</li>
	 * <li>devLibelle : Libelle du devis</li>
	 * <li>fouOrdre : Identifiant du fournisseur client</li>
	 * <li>persId : Identifiant du client</li>
	 * <li>noIndividu : Identifiant du contact client</li>
	 * <li>fouOrdrePrest : Identifiant du fournisseur prestataire</li>
	 * <li>fouPersNomPrenom : Libelle du fournisseur client</li>
	 * <li>lolfId : Nomenclature Lolf (anciennement destination)</li>
	 * <li>pourcentRemiseGlobal : Pourcentage de remise accorde sur le total</li>
	 * <li>devMontantHT : Montant HT du devis</li>
	 * <li>devMontantTTC : Montant TTC du devis</li>
	 * <li>devMontantTVA : Montant de la TVA pour le devis</li>
	 * <li>prestOrgId : Identifiant de la ligne budgetaire prestataire</li>
	 * <li>clientOrgId : Identifiant de la ligne budgetaire cliente</li>
	 * <li>clientTcdCode : Code du type de credit client</li>
	 * <li>fapIds : Identifiant des factures generees a partir du devis</li>
	 * <li>prestationLignes : Les lignes de la prestation, sous forme de NSArray de NSDictionary</li>
	 * </ul>
	 * <br>
	 * 
	 * @param fouOrdre
	 *            fouOrdre du client, moral ou physique (obligatoire si prestId null, inutile si prestId renseigne).
	 * @param persCliPersId
	 *            PersId de l'individu client contact (facultatif).
	 * @param prestId
	 *            Identifiant de la prestation a recuperer (facultatif).
	 * @return NSArray de NSDictionary
	 */
	public NSArray getDevis(Integer fouOrdre, Integer persCliPersId, Integer prestId) throws Exception {
		if (!isConnected()) {
			throw new Exception("La connexion au service" + getWsName() + " n'est pas active");
		}
		Object[] wsArgs = { fouOrdre, persCliPersId, prestId };
		Object[] res = (Object[]) wsInvoke("getDevis", wsArgs);
		throwLastWsException();
		return arrayMapsToNSArrayNSDictionarys(res);
	}

	/**
	 * recupere un devis et en fait le devis en cours (pour rajouter des lignes par exemple). <br>
	 * Le client doit etre precise pour des raisons de securite. <br>
	 * 
	 * @param prestId
	 *            Identifiant du devis
	 * @param fouOrdre
	 *            Identifiant du fournisseur client.
	 */
	public void chargeCurrentDevisForClient(Integer prestId, Integer fouOrdre) throws Exception {
		Object[] wsArgs = { prestId, fouOrdre };
		wsInvoke("chargeCurrentDevisForClient", wsArgs);
		throwLastWsException();
	}

	/**
	 * recupere un devis et en fait le devis en cours (pour rajouter des lignes par exemple). <br>
	 * 
	 * @param prestId
	 *            Identifiant du devis
	 */
	public void chargeCurrentDevis(Integer prestId) throws Exception {
		Object[] wsArgs = { prestId };
		wsInvoke("chargeCurrentDevis", wsArgs);
		throwLastWsException();
	}

	/**
	 * Validation du devis cote client. Si c'est de l'interne, cela va generer l'engagement en plus. Si toutes les informations (budgetaires
	 * notamment) sont remplies. ATTENTION: saveChanges automatique !
	 * 
	 * @param utlOrdre
	 *            l'utilisateur qui tente de valider le devis cote client (obligatoire)
	 */
	public void valideCurrentDevisClient(Integer utlOrdre) throws Exception {
		Object[] wsArgs = { utlOrdre };
		wsInvoke("valideCurrentDevisClient", wsArgs);
		throwLastWsException();
	}

	/**
	 * Devalidation du devis cote client. Si c'est de l'interne, l'engagement est egalement supprime. ATTENTION: saveChanges automatique !
	 * 
	 * @param utlOrdre
	 *            l'utilisateur qui tente de devalider le devis cote client (obligatoire)
	 */
	public void devalideCurrentDevisClient(Integer utlOrdre) throws Exception {
		Object[] wsArgs = { utlOrdre };
		wsInvoke("devalideCurrentDevisClient", wsArgs);
		throwLastWsException();
	}

	/**
	 * Validation du devis cote prestataire (date de validation prest) Aucune autre incidence ATTENTION: saveChanges automatique !
	 * 
	 * @param utlOrdre
	 *            l'utilisateur qui tente de valider le devis cote prestataire (obligatoire)
	 */
	public void valideCurrentDevisPrest(Integer utlOrdre) throws Exception {
		Object[] wsArgs = { utlOrdre };
		wsInvoke("valideCurrentDevisPrest", wsArgs);
		throwLastWsException();
	}

	/**
	 * Devalidation du devis cote prestataire (date de validation prest null) Aucune autre incidence ATTENTION: saveChanges automatique !
	 * 
	 * @param utlOrdre
	 *            l'utilisateur qui tente de devalider le devis cote prestataire (obligatoire)
	 */
	public void devalideCurrentDevisPrest(Integer utlOrdre) throws Exception {
		Object[] wsArgs = { utlOrdre };
		wsInvoke("devalideCurrentDevisPrest", wsArgs);
		throwLastWsException();
	}

	/**
	 * Charge le devis en devis courant et tente de le valider totalement (3 etapes) ATTENTION: saveChanges automatique !
	 * 
	 * @param prestId
	 *            la prestation a valider
	 * @param utlOrdre
	 *            l'utilisateur qui tente de valider le devis
	 */
	public void valideToutDevis(Integer prestId, Integer utlOrdre) throws Exception {
		Object[] wsArgs = { prestId, utlOrdre };
		wsInvoke("valideToutDevis", wsArgs);
		throwLastWsException();
	}

	/**
	 * Tente de valider totalement le devis en cours (3 etapes) ATTENTION: saveChanges automatique !
	 * 
	 * @param utlOrdre
	 *            l'utilisateur qui tente de valider le devis
	 */
	public void valideToutCurrentDevis(Integer utlOrdre) throws Exception {
		Object[] wsArgs = { utlOrdre };
		wsInvoke("valideToutCurrentDevis", wsArgs);
		throwLastWsException();
	}

	/**
	 * Passe la prestation a realiser (necessaire pour pourvoir generer des factures a partir d'une prestation). ATTENTION: saveChanges
	 * automatique !
	 * 
	 * @param utlOrdre
	 *            l'utilisateur qui tente de clore le devis (obligatoire)
	 */
	public void fermerCurrentPrestation(Integer utlOrdre) throws Exception {
		Object[] wsArgs = { utlOrdre };
		wsInvoke("fermerCurrentPrestation", wsArgs);
		throwLastWsException();
	}

	/**
	 * Declos une prestation close (realisee ou passee par le valideToutCurrentDevis). ATTENTION: saveChanges
	 * automatique !
	 * 
	 * @param utlOrdre
	 *            l'utilisateur qui tente de declore le devis (obligatoire)
	 */
	public void decloreCurrentPrestation(Integer utlOrdre) throws Exception {
		Object[] wsArgs = { utlOrdre };
		wsInvoke("decloreCurrentPrestation", wsArgs);
		throwLastWsException();
	}

	/**
	 * Genere la (les) facture(s) a partir de la prestation en cours. La prestation doit etre cloturee ("Fermee"). Sinon une exception est
	 * generee. ATTENTION: saveChanges automatique !
	 * 
	 * @param utlOrdre
	 *            l'utilisateur qui tente de generer la facture (obligatoire)
	 */
	public void genererFactureForCurrentDevis(Integer utlOrdre) throws Exception {
		Object[] wsArgs = { utlOrdre };
		wsInvoke("genererFactureForCurrentDevis", wsArgs);
		throwLastWsException();
	}

	/**
	 * Affecte la ligne budgetaire cote prestataire au devis (necessaire pour la validation cote prestataire). Necessite un saveChanges pour
	 * prendre en compte la modif.
	 * 
	 * @param orgId
	 */
	public void affecteOrganPrestForCurrentDevis(Integer orgId) throws Exception {
		Object[] wsArgs = { orgId };
		wsInvoke("affecteOrganPrestForCurrentDevis", wsArgs);
		throwLastWsException();
	}

	/**
	 * Affecte la ligne budgetaire cote client au devis. Necessite un saveChanges pour prendre en compte la modif.
	 * 
	 * @param orgId
	 */
	public void affecteOrganClientForCurrentDevis(Integer orgId) throws Exception {
		Object[] wsArgs = { orgId };
		wsInvoke("affecteOrganClientForCurrentDevis", wsArgs);
		throwLastWsException();
	}

	/**
	 * Affecte la destination recette cote prestataire au devis. Necessite un saveChanges pour prendre en compte la modif.
	 * 
	 * @param lolfId
	 */
	public void affecteDestinationPrestForCurrentDevis(Integer lolfId) throws Exception {
		Object[] wsArgs = { lolfId };
		wsInvoke("affecteDestinationPrestForCurrentDevis", wsArgs);
		throwLastWsException();
	}

	/**
	 * Affecte la destination recette cote client au devis. Necessite un saveChanges pour prendre en compte la modif.
	 * 
	 * @param lolfId
	 */
	public void affecteDestinationClientForCurrentDevis(Integer lolfId) throws Exception {
		Object[] wsArgs = { lolfId };
		wsInvoke("affecteDestinationClientForCurrentDevis", wsArgs);
		throwLastWsException();
	}

	/**
	 * Affecte le type de credit cote prestataire au devis. Necessite un saveChanges pour prendre en compte la modif.
	 * 
	 * @param tcdOrdre
	 */
	public void affecteTypcredPrestForCurrentDevis(Integer tcdOrdre) throws Exception {
		Object[] wsArgs = { tcdOrdre };
		wsInvoke("affecteTypcredClientForCurrentDevis", wsArgs);
		throwLastWsException();
	}

	/**
	 * Affecte le type de credit cote client au devis. Necessite un saveChanges pour prendre en compte la modif.
	 * 
	 * @param tcdCode
	 */
	public void affecteTypcredClientForCurrentDevis(String tcdCode) throws Exception {
		Object[] wsArgs = { tcdCode };
		wsInvoke("affecteTypcredClientForCurrentDevis", wsArgs);
		throwLastWsException();
	}

	/**
	 * Affecte l'imputation comptable cote client au devis. Necessite un saveChanges pour prendre en compte la modif.
	 * 
	 * @param pcoNum
	 */
	public void affecteImputationClientForCurrentDevis(String pcoNum) throws Exception {
		Object[] wsArgs = { pcoNum };
		wsInvoke("affecteImputationClientForCurrentDevis", wsArgs);
		throwLastWsException();
	}

	/**
	 * Affectation de l'utilisateur au devis en cours. Necessite un saveChanges pour prendre en compte la modif.
	 * 
	 * @param utlOrdre
	 * @throws Exception
	 */
	public void affecteUtilisateur(Integer utlOrdre) throws Exception {
		Object[] wsArgs = { utlOrdre };
		wsInvoke("affecteUtilisateur", wsArgs);
		throwLastWsException();
	}

	/**
	 * Affecte une convention en recette au devis en cours. Necessite un saveChanges pour prendre en compte la modif.
	 * 
	 * @param conOrdre
	 */
	public void affecteConvention(Integer conOrdre) throws Exception {
		Object[] wsArgs = { conOrdre };
		wsInvoke("affecteConvention", wsArgs);
		throwLastWsException();
	}

	/**
	 * Affecte une remise globale a la prestation. Necessite un saveChanges pour prendre en compte la modif.
	 * 
	 * @param remise
	 */
	public void affecteRemiseGlobale(BigDecimal remise) throws Exception {
		Object[] wsArgs = { remise };
		wsInvoke("affecteRemiseGlobale", wsArgs);
		throwLastWsException();
	}

	// public NSArray getOrgansForAgtOrdre(Integer agtOrdre) throws Exception {
	// Object[] wsArgs = { agtOrdre };
	// Object[] res = (Object[]) wsInvoke("getOrgansForAgtOrdre", wsArgs);
	// throwLastWsException();
	// return arrayMapsToNSArrayNSDictionarys(res);
	// }

	/**
	 * Imprime le devis en cours si celui-ci a ete cree ou charge. Le resultat renvoie un NSData correspondant au fichier PDF a imprimer.
	 */
	/**
	 * Imprime le devis en cours si celui-ci a ete cree ou charge. Le resultat renvoie un NSData correspondant au fichier PDF a imprimer.
	 * 
	 * @return Le devis sous forme de NSData
	 * @throws Exception
	 */
	public NSData imprimerCurrentDevis() throws Exception {
		if (!isConnected()) {
			throw new Exception("La connexion au service" + getWsName() + " n'est pas active");
		}
		NSData res = (NSData) wsInvoke("imprimerCurrentDevis", null);
		throwLastWsException();
		return res;
	}

	/**
	 * Imprime le devis en cours sous forme de WOResponse format pdf si celui-ci a ete cree ou charge.
	 * 
	 * @return Le devis sous forme de WOResponse
	 */
	public WOResponse imprimerCurrentDevisAsWOResponse() {
		NSData pdfData;
		try {
			pdfData = imprimerCurrentDevis();
			WOResponse resp = new WOResponse();
			if (pdfData != null) {
				resp.setContent(pdfData);
				resp.setHeader("application/pdf", "Content-Type");
				resp.setHeader(String.valueOf(pdfData.length()), "Content-Length");
			}
			else {
				resp.setHeader("0", "Content-Length");
			}
			return resp.generateResponse();
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Imprime une facture. Le resultat renvoie un NSData correspondant au fichier PDF a imprimer.
	 * 
	 * @param fapId
	 *            L'identifiant de la facture (obligatoire)
	 * @return La facture sous forme de NSData
	 */
	public NSData imprimerFacture(Integer fapId) throws Exception {
		if (!isConnected()) {
			throw new Exception("La connexion au service" + getWsName() + " n'est pas active");
		}
		Object[] wsArgs = { fapId };
		NSData res = (NSData) wsInvoke("imprimerFacture", wsArgs);
		throwLastWsException();
		return res;
	}

	/**
	 * Imprime une facture sous forme de WOResponse format pdfr.
	 * 
	 * @param fapId
	 *            L'identifiant de la facture (obligatoire)
	 * @return La facture sous forme de WOResponse
	 */
	public WOResponse imprimerFactureAsWOResponse(Integer fapId) {
		NSData pdfData;
		try {
			pdfData = imprimerFacture(fapId);
			WOResponse resp = new WOResponse();
			if (pdfData != null) {
				resp.setContent(pdfData);
				resp.setHeader("application/pdf", "Content-Type");
				resp.setHeader(String.valueOf(pdfData.length()), "Content-Length");
			}
			else {
				resp.setHeader("0", "Content-Length");
			}
			return resp.generateResponse();
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// public NSArray getDevisArray(Object[] devOrdres) throws Exception {
	// Object[] wsArgs = { devOrdres };
	// Object[] res = (Object[]) wsInvoke("getDevisArray", wsArgs);
	// throwLastWsException();
	// return arrayMapsToNSArrayNSDictionarys(res);
	//
	// }

	/**
	 * Supprime toutes les lignes du devis en cours (si le devis est valide, une exception est declenchee). Necessite un saveChanges pour prendre
	 * en compte la modif.
	 */
	public void viderLignesForCurrentDevis() throws Exception {
		wsInvoke("viderLignesForCurrentDevis", null);
		throwLastWsException();
	}

	/**
	 * Renvoie le montant HT du devis charge.
	 * 
	 * @return Montant HT
	 */
	public java.math.BigDecimal getMontantHTForCurrentDevis() throws Exception {
		java.math.BigDecimal res = (java.math.BigDecimal) wsInvoke("getMontantHTForCurrentDevis", null);
		throwLastWsException();
		return res;
	}

	/**
	 * Renvoie le montant TVA du devis charge.
	 * 
	 * @return Montant TVA
	 */
	public java.math.BigDecimal getMontantTVAForCurrentDevis() throws Exception {
		java.math.BigDecimal res = (java.math.BigDecimal) wsInvoke("getMontantTVAForCurrentDevis", null);
		throwLastWsException();
		return res;
	}

	/**
	 * Renvoie le montant TTC du devis charge.
	 * 
	 * @return Montant TTC
	 */
	public java.math.BigDecimal getMontantTTCForCurrentDevis() throws Exception {
		java.math.BigDecimal res = (java.math.BigDecimal) wsInvoke("getMontantTTCForCurrentDevis", null);
		throwLastWsException();
		return res;
	}

	/**
	 * Indique si le devis est deja valide cote client.
	 * 
	 * @return Boolean
	 */
	public Boolean currentDevisValideCoteClient() throws Exception {
		if (!isConnected()) {
			throw new Exception("La connexion au service" + getWsName() + " n'est pas active");
		}
		Boolean res = (Boolean) wsInvoke("currentDevisValideCoteClient", null);
		throwLastWsException();
		return res;
	}

	/**
	 * Indique si le devis est deja valide cote prestataire.
	 * 
	 * @return Boolean
	 */
	public Boolean currentDevisValideCotePrestataire() throws Exception {
		if (!isConnected()) {
			throw new Exception("La connexion au service" + getWsName() + " n'est pas active");
		}
		Boolean res = (Boolean) wsInvoke("currentDevisValideCotePrestataire", null);
		throwLastWsException();
		return res;
	}

	/**
	 * Indique si le devis est deja clos.
	 * 
	 * @return Boolean
	 */
	public Boolean currentPrestationRealisee() throws Exception {
		if (!isConnected()) {
			throw new Exception("La connexion au service" + getWsName() + " n'est pas active");
		}
		Boolean res = (Boolean) wsInvoke("currentPrestationRealisee", null);
		throwLastWsException();
		return res;
	}

	/**
	 * Renvoie le disponible a partir de la fonction stockee. Se base sur l'exercice en cours, la ligne budgetaire et le type de credit.
	 * 
	 * @param orgId
	 *            La ligne budgetaire (obligatoire)
	 * @param tcdCode
	 *            Le type de credit (obligatoire)
	 * @return Le disponible (Double)
	 */
	public Double getDisponible(Integer orgId, String tcdCode) throws Exception {
		if (!isConnected()) {
			throw new Exception("La connexion au service" + getWsName() + " n'est pas active");
		}
		Object[] wsArgs = { orgId, tcdCode };
		Double res = (Double) wsInvoke("getDisponible", wsArgs);
		throwLastWsException();
		return res;
	}

	/**
	 * Retourne le montant TTC prevu pour un devis qui comporterait les articles/quantites passes. Ne cree aucun devis, ne cree rien dans la base !
	 * 
	 * @param articles
	 *            Tableau des articles qui constituent le devis (caarId) - Obligatoire
	 * @param quantites
	 *            Tableau de BigDecimal des quantites des articles du premier tableau - Facultatif, toutes quantites a 1 par defaut si absent
	 * @return Le montant TTC, sans tenir compte des remises eventuelles
	 */
	public Double getMontantTTCForDevis(Object[] articles, BigDecimal[] quantites) throws Exception {
		if (!isConnected()) {
			throw new Exception("La connexion au service" + getWsName() + " n'est pas active");
		}
		Object[] wsArgs = { articles, quantites };
		Double res = (Double) wsInvoke("getMontantTTCForDevis", wsArgs);
		throwLastWsException();
		return res;
	}

	/**
	 * Archive le devis en cours (ne le supprime pas, la suppression d'un devis est interdite). NE NECESSITE PAS DE SAVECHANGES, LE CHANGEMENT
	 * EST EFFECTIF DE SUITE !
	 * 
	 * @param utlOrdre
	 *            Identifiant de l'utilisateur qui supprime le devis (obligatoire)
	 */
	public void deleteCurrentDevis(Integer utlOrdre) throws Exception {
		if (!isConnected()) {
			throw new Exception("La connexion au service" + getWsName() + " n'est pas active");
		}
		Object[] wsArgs = { utlOrdre };
		wsInvoke("deleteCurrentDevis", wsArgs);
		throwLastWsException();
	}

	/**
	 * Charge en devis courant et archive le devis donne (ne le supprime pas, la suppression d'un devis est interdite). NE NECESSITE PAS DE
	 * SAVECHANGES, LE CHANGEMENT EST EFFECTIF DE SUITE !
	 * 
	 * @param prestId
	 *            Identifiant du devis (obligatoire)
	 * @param fouOrdre
	 *            Identifiant de la personne cliente (obligatoire)
	 * @param utlOrdre
	 *            Identifiant de l'utilisateur qui supprime le devis (obligatoire)
	 */
	public void deleteDevis(Integer prestId, Integer fouOrdre, Integer utlOrdre) throws Exception {
		if (!isConnected()) {
			throw new Exception("La connexion au service" + getWsName() + " n'est pas active");
		}
		Object[] wsArgs = { prestId, fouOrdre, utlOrdre };
		wsInvoke("deleteDevis", wsArgs);
		throwLastWsException();
	}

	/**
	 * Charge en devis courant et archive le devis donne (ne le supprime pas, la suppression d'un devis est interdite). NE NECESSITE PAS DE
	 * SAVECHANGES, LE CHANGEMENT EST EFFECTIF DE SUITE !
	 * 
	 * @param prestId
	 *            Identifiant du devis
	 * @param utlOrdre
	 *            Identifiant de l'utilisateur qui supprime le devis (obligatoire)
	 */
	public void deleteDevisSansControle(Integer prestId, Integer utlOrdre) throws Exception {
		if (!isConnected()) {
			throw new Exception("La connexion au service" + getWsName() + " n'est pas active");
		}
		Object[] wsArgs = { prestId, utlOrdre };
		wsInvoke("deleteDevisSansControle", wsArgs);
		throwLastWsException();
	}

	// public Hashtable getDevisEtats() throws Exception {
	// if (!isConnected()) {
	// throw new Exception("La connexion au service" + getWsName() + " n'est pas active");
	// }
	// Hashtable res = mapToNSDictionary((java.util.Map) wsInvoke("getDevisEtats", null)).hashtable();
	// throwLastWsException();
	// return res;
	// }
	//
	// public String getEtatCurrentDevis() throws Exception {
	// if (!isConnected()) {
	// throw new Exception("La connexion au service" + getWsName() + " n'est pas active");
	// }
	// String res = (String) wsInvoke("getEtatCurrentDevis", null);
	// throwLastWsException();
	// return res;
	// }

	/**
	 * @return Les types de credit autorises pour les prestations (vecteur de Integer)
	 */
	public java.util.Vector getTypcredAutorises() throws Exception {
		if (!isConnected()) {
			throw new Exception("La connexion au service" + getWsName() + " n'est pas active");
		}
		java.util.Vector res = (java.util.Vector) wsInvoke("getTypcredAutorises", null);
		throwLastWsException();
		return res;
	}

	public Integer getIdCurrentDevis() throws Exception {
		if (!isConnected()) {
			throw new Exception("La connexion au service" + getWsName() + " n'est pas active");
		}
		Integer id = (Integer) wsInvoke("getIdCurrentDevis", null);
		throwLastWsException();
		return id;

	}

}
