/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//		Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

package org.cocktail.application.palette;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.lang.reflect.Method;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

import org.cocktail.application.palette.models.ColumnData;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCodingAdditions;
import com.webobjects.foundation.NSMutableArray;

public class JTableViewCocktail extends JPanel {
	
    NSMutableArrayDisplayGroup data=null;
    NSMutableArray columns=null;
    JTable table=null;
   JTableCocktailModelData modelData=null;
   JScrollPane scrollPane=null;
   
   JTableCocktailSorter sorter =null;

    public JTableViewCocktail() {
        super();
        JScrollPane scrollPane1 = new javax.swing.JScrollPane();
        JTable table1 = new javax.swing.JTable();

        table1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        scrollPane1.setViewportView(table1);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(scrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(scrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
        );
    }
   
//    public void initTableViewCocktail(NSMutableArray columns,NSMutableArrayDisplayGroup lesDatas,Dimension scrollDimension,int jTableMode){
//    	removeAll();
//    	setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
//    	data =lesDatas;
//        this.columns =columns;
//        modelData = new JTableCocktailModelData();
//        sorter = new JTableCocktailSorter(modelData,this); 
//        table = new JTable(sorter);             
//        initColumnSizes(table);
//        table.setAutoResizeMode(jTableMode);
//        sorter.setTableHeader(table.getTableHeader()); 
//        table.setPreferredScrollableViewportSize(scrollDimension);
//        //Set up tool tips for column headers.
//        table.getTableHeader().setToolTipText("trier en cliquant");
//        //Create the scroll pane and add the table to it.
//        JScrollPane scrollPane = new JScrollPane(table);
//        //Add the scroll pane to this panel.
//        add(scrollPane);
//    }
    public void initTableViewCocktail(JTableViewCocktail tvc){
		removeAll();
		setLayout(new GridLayout(1,0));
		data = tvc.data;
		columns = tvc.columns;
		table = tvc.table;
		modelData = tvc.modelData;
		scrollPane = tvc.scrollPane;
		sorter = tvc.sorter;
		add(tvc);
		validate();
		updateUI();
		refresh();
    }
   
   
    public JTableViewCocktail (NSMutableArray columns,NSMutableArrayDisplayGroup lesDatas,Dimension scrollDimension,int jTableMode) {
        super(new GridLayout(1,0));
        data =lesDatas;
        this.columns =columns;
        modelData = new JTableCocktailModelData();
        sorter = new JTableCocktailSorter(modelData,this); 
        table = new JTable(sorter);             
        initColumnSizes(table);
        table.setAutoResizeMode(jTableMode);
        sorter.setTableHeader(table.getTableHeader()); 
        table.setPreferredScrollableViewportSize(scrollDimension);
        //Set up tool tips for column headers.
        table.getTableHeader().setToolTipText("trier en cliquant");
        //Create the scroll pane and add the table to it.
        scrollPane = new JScrollPane(table);
        //Add the scroll pane to this panel.
        add(scrollPane);
        }

	private void initColumnSizes(JTable table) {
    	TableColumn column = null;
        for (int i = 0; i < table.getColumnCount(); i++) {
            column = table.getColumnModel().getColumn(i);
            column.setPreferredWidth(((ColumnData)columns.objectAtIndex(i)).getM_width());
        }
    }
    

	public JTable getTable() {
		return table;
	}
	
	public void refresh(){
		initalisationDesTris();
		this.updateUI();
		modelData.fireTableDataChanged();
	}
	
	public void initalisationDesTris(){
        for (int i = 0; i < getTable().getColumnCount(); i++) {
    		sorter.setEmptyStatus(i);
        }
	}
	
	public NSMutableArrayDisplayGroup getSelectedObjects()
	{
		int lesSelected[] =null;
		NSMutableArrayDisplayGroup monNSMutableArrayDisplayGroup=new NSMutableArrayDisplayGroup();
		
		lesSelected = getTable().getSelectedRows();
		for (int i = 0; i < lesSelected.length; i++) {
			monNSMutableArrayDisplayGroup.addObject(data.objectAtIndex(lesSelected[i]));
		}
		return monNSMutableArrayDisplayGroup;
	}
	
	public void setSelectedObjects(NSMutableArrayDisplayGroup nouvelleSelection)
	{
		int tmpSelecection = 0;

		System.out.println("setSelectedObjects"+nouvelleSelection);
		for (int i = 0; i < nouvelleSelection.count(); i++) {
			tmpSelecection = data.indexOfObject(nouvelleSelection.objectAtIndex(i));
			getTable().setRowSelectionInterval(tmpSelecection, tmpSelecection);
			//System.out.println("tmpSelecection "+tmpSelecection);
		}
		this.refresh();
		super.repaint();
		getTable().repaint();
	}

	public NSMutableArrayDisplayGroup removeSelectedObjects()
	{
		int lesSelected[] =null;
		NSMutableArrayDisplayGroup monNSMutableArrayDisplayGroup=new NSMutableArrayDisplayGroup();
		lesSelected = getTable().getSelectedRows();
		for (int i = 0; i < lesSelected.length; i++) {
			monNSMutableArrayDisplayGroup.addObject(data.objectAtIndex(lesSelected[i]));
		}
		return monNSMutableArrayDisplayGroup;
	}
	// selection
	public void addDelegateSelectionListener(Object delegate,String methode){
		getTable().getSelectionModel().addListSelectionListener( new TableViewCocktailSelectionListener(this, delegate, methode));
	}

	//clavier
	public void addDelegateKeyListener(Object delegate,String methode){
		getTable().addKeyListener(new TableViewCocktailKeyAdapter(delegate,methode));
	}
	

	class TableViewCocktailKeyAdapter extends KeyAdapter {
		Object delegate = null;
		String methode = null;

		public TableViewCocktailKeyAdapter(Object delegate,String methode)
		{
			this.delegate = delegate;
			this.methode = methode;
		}

		public void keyPressed(KeyEvent evt) {
			super.keyPressed(evt);
		}

		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub
			super.keyReleased(arg0);
//			System.out.println("keyReleased");
			callMethode();

		}

		public void keyTyped(KeyEvent arg0) {
			// TODO Auto-generated method stub
			super.keyTyped(arg0);

		}



		private void callMethode()
		{
			// try du call
			try {
				Class c;
				c = delegate.getClass();
				// Recupereation de la methode getXxxxx :
				Method call = c.getMethod(methode, null);
				// appel de la methode
				call.invoke(delegate, null);

			} catch (Exception ex) {
				//	System.out.println(ex+"objet "+delegate+" methode "+methode+" inexistante");
			}

		}

		private void callMethode(String param)
		{
			// try du call
			try {
				Class c;
				c = delegate.getClass();
				// Recupereation de la methode getXxxxx :
				Class parametres [] = {param.getClass()};
				Method call = c.getMethod(methode,parametres);
				call.invoke(delegate, new Object[] { param });

			} catch (Exception ex) {
				//System.out.println(ex+"objet "+delegate+" methode "+methode+" inexistante");
			}

		}

	}

	class TableViewCocktailSelectionListener implements ListSelectionListener {

		JTableViewCocktail maTableViewCocktail;
		Object delegate = null;
		String methode = null;


		public TableViewCocktailSelectionListener(JTableViewCocktail maTableViewCocktail,Object delegate,String methode)
		{
			super();
			this.delegate = delegate;
			this.methode = methode;
			this.maTableViewCocktail = maTableViewCocktail;
		}



		public void valueChanged(ListSelectionEvent e) {
			// If cell selection is enabled, both row and column change events are fired
			/*
	 		if (e.getSource() == maTableViewCocktail.getSelectionModel()
					&& maTableViewCocktail.getRowSelectionAllowed() ) {
				// Column selection changed
				int first = e.getFirstIndex();
				int last = e.getLastIndex();
				System.out.println(" Column selection changed");
				callMethode();

			} 

			if (e.getSource() == maTableViewCocktail.getColumnModel().getSelectionModel()
					&& maTableViewCocktail.getColumnSelectionAllowed() ){
				// Row selection changed
				int first = e.getFirstIndex();
				int last = e.getLastIndex();
				System.out.println("Row selection changed");
				callMethode();
			}
			 */

			if (e.getValueIsAdjusting()) {
				// The mouse button has not yet been released
				//System.out.println("The mouse button has not yet been released");
				callMethode();
			}
		}



		private void callMethode()
		{
			// try du call
			try {
				Class c;
				c = delegate.getClass();
				// Recupereation de la methode getXxxxx :
				Method call = c.getMethod(methode, null);
				// appel de la methode
				call.invoke(delegate, null);

			} catch (Exception ex) {
				//System.out.println(ex+"objet "+delegate+" methode "+methode+" inexistante");
			}

		}

	}
    class JTableCocktailModelData extends AbstractTableModel {

        public int getColumnCount() {
            return columns.count();
        }

        public int getRowCount() {
            return data.count();
        }

    	public String getColumnName( int column) { 
    		return ((ColumnData)columns.objectAtIndex(column)).getM_title(); 

    	}


    	public String getColumnKey( int column) { 
    		return ((ColumnData)columns.objectAtIndex(column)).getM_key(); 
    	}

    	public Object getValueAt( int nRow,  int nCol) {

    		ColumnData tmp_Column = null;
    		final NSKeyValueCodingAdditions row = (NSKeyValueCodingAdditions)data.objectAtIndex(nRow);

    		if (nRow < 0 || nRow>=getRowCount())
    			return "";

    		tmp_Column = (ColumnData) columns.objectAtIndex(nCol);

    		// try du keyName()
    		try {
    			Class c;
    			c = row.getClass();
    			// Recupereation de la methode getXxxxx :
    			final Method get = c.getMethod(tmp_Column.getM_key(), null);
    			// appel de l accesseur
    			return get.invoke(row, null);
    		} catch (final Exception e) {

    		}
    		
    		// try du getKeyName()
    		try {
    			Class c;
    			c = row.getClass();
    			// Recupereation de la methode getXxxxx :
    			final Method get = c.getMethod("get"+getCapitalizedString(tmp_Column.getM_key()), null);
    			// appel de l accesseur
    			return get.invoke(row, null);
    		} catch (final Exception e) {

    		}

    		// try storedValueForKey(keyName)
    		try {
    			return row.valueForKey(tmp_Column.getM_key());
    		} catch (final Exception e) {
    			new Exception (e+"property problem !!!!");
    		}
    		
    		

    		// try storedValueForKey(keyName)
    		try {
    			return row.valueForKeyPath(tmp_Column.getM_key());
    		} catch (final Exception e) {
    			new Exception (e+"property problem !!!!");
    		}
    		
    		return "...";
    	}

    	private  String getCapitalizedString( final String str ) {
    		// Print a copy of str to standard output, with the
    		// first letter of each word in upper case.
    		char ch;       // One of the characters in str.
    		char prevCh;   // The character that comes before ch in the string.
    		int i;         // A position in str, from 0 to str.length()-1.
    		prevCh = '.';  // Prime the loop with any non-letter character.
    		String result = "";
    		for ( i = 0;  i < str.length();  i++ ) {
    			ch = str.charAt(i);
    			if ( Character.isLetter(ch)  &&  ! Character.isLetter(prevCh) )
    				//System.out.print( Character.toUpperCase(ch) );
    				result = ""+Character.toUpperCase(ch);
    			else
    				//System.out.print( ch );
    				result = result+ch;
    			prevCh = ch;
    		}
    		return result;
    	}

    	public NSArray getLesDatas() {
    		return data;
    	}

    	public void setLesDatas(NSMutableArrayDisplayGroup lesDatas) {
    		data = lesDatas;
    	}

        /*
         * JTable uses this method to determine the default renderer/
         * editor for each cell.  If we didn't implement this method,
         * then the last column would contain text ("true"/"false"),
         * rather than a check box.
         */
        public Class getColumnClass(int c) {
        	if (getValueAt(0, c) == null)
        	return " ".getClass();
        	else
            return getValueAt(0, c).getClass();
        }

        /*
         * Don't need to implement this method unless your table's
         * editable.
         */
        public boolean isCellEditable(int row, int col) {
            //Note that the data/cell address is constant,
            //no matter where the cell appears onscreen.
//            if (col < 2) {
//                return false;
//            } else {
//                return true;
//            }
            return false;
        }

        /*
         * Don't need to implement this method unless your table's
         * data can change.
         */
        public void setValueAt(Object value, int row, int col) {
//            if (DEBUG) {
//                System.out.println("Setting value at " + row + "," + col
//                                   + " to " + value
//                                   + " (an instance of "
//                                   + value.getClass() + ")");
//            }
//
//            data[row][col] = value;
//            fireTableCellUpdated(row, col);
//
//            if (DEBUG) {
//                System.out.println("New value of data:");
//                printDebugData();
//            }
        }

    }
	public NSMutableArrayDisplayGroup getData() {
		return data;
	}

	public NSMutableArray getColumns() {
		return columns;
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}
}