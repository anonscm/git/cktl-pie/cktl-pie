/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.palette;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.lang.reflect.Method;

import javax.swing.JTextField;

public class JTextFieldCocktail extends JTextField implements InterfaceJefyAdmin{
	
	
	public JTextFieldCocktail() {
		super();
	}
	
	
	public void addDelegateKeyListener(Object delegate,String methode){
		addKeyListener(new JTextFieldCocktailCocktailKeyAdapter(delegate,methode));
	}


	class JTextFieldCocktailCocktailKeyAdapter extends KeyAdapter {
		Object delegate = null;
		String methode = null;
		
		public JTextFieldCocktailCocktailKeyAdapter(Object delegate,String methode)
		{
			this.delegate = delegate;
			this.methode = methode;
		}
		
		public void keyPressed(KeyEvent evt) {

			if (evt.getKeyCode() == KeyEvent.VK_ENTER)
			callMethode();
		}

		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub
			super.keyReleased(arg0);

		}

		public void keyTyped(KeyEvent arg0) {
			// TODO Auto-generated method stub
			super.keyTyped(arg0);

		}
		
		
		
		private void callMethode()
		{
			// try du call
			try {
				Class c;
				c = delegate.getClass();
				// Recupereation de la methode getXxxxx :
				Method call = c.getMethod(methode, null);
				// appel de la methode
				call.invoke(delegate, null);

			} catch (Exception ex) {
			//	System.out.println(ex+"objet "+delegate+" methode "+methode+" inexistante");
			}

		}
		
		private void callMethode(String param)
		{
			// try du call
			try {
				Class c;
				c = delegate.getClass();
				// Recupereation de la methode getXxxxx :
				Class parametres [] = {param.getClass()};
				Method call = c.getMethod(methode,parametres);
				call.invoke(delegate, new Object[] { param });

			} catch (Exception ex) {
				//System.out.println(ex+"objet "+delegate+" methode "+methode+" inexistante");
			}

		}
		
	}
	public void setDisabledFromPrivileges() {
		this.setEnabled(false);
		
	}


	public void setEnabledFromPrivileges() {
		this.setEnabled(true);
		
	}

}
