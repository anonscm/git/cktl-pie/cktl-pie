/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.pie.client.interfaces;

import java.math.BigDecimal;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.tools.ToolsCocktailLogs;
import org.cocktail.kava.client.metier.EOBordereau;
import org.cocktail.kava.client.metier.EOFacture;
import org.cocktail.kava.client.metier.EOFacturePapier;
import org.cocktail.kava.client.metier.EOFournisUlr;
import org.cocktail.kava.client.metier.EOPrestation;
import org.cocktail.kava.client.metier.EORecette;
import org.cocktail.kava.client.metier.EORecetteCtrlPlanco;
import org.cocktail.kava.client.metier.EOTitre;

import app.client.ApplicationClient;
import app.client.FactureRecette;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderRecetteRechercheAvance extends FinderRechercheAvance {
	private FactureRecette parent;
	

	public FinderRecetteRechercheAvance(ApplicationCocktail ctrl, int alocation_x, int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);
		creationFenetre(getMonInterfaceRechercheAvance(),"RECHERCHE AVANCEE D'UNE RECETTE");
	}
	
	public void actionSelectionner() {
		app.ceerTransactionLog();
		app.afficherUnLogDansTransactionLog("Recherche des recettes en cours...", 50);
		parent.atLeastOnSearchHasBeenMade = true;
		try {
			parent.updateData();
		} catch (Throwable e) {
			e.printStackTrace();
			app.afficherUnLogDansTransactionLog("Erreur : " + e.getMessage(), 100);
			app.finirTransactionLog();
			return;
		}
		app.finirTransactionLog();
		app.fermerTransactionLog();
	}

	public void actionClean() {
		getMonInterfaceRechercheAvance().actionClean();
		parent.updateData();
	}
	
	public void setParent(FactureRecette parent) {
		this.parent = parent;
	}
	
	public NSArray getResultat() {
		NSMutableArray args = new NSMutableArray();
		try {
			if(getMonInterfaceRechercheAvance().periodeDateDebut()!=null)
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EORecette.REC_DATE_SAISIE_KEY+" >= %@", new NSArray(getMonInterfaceRechercheAvance().periodeDateDebut())));
			if(getMonInterfaceRechercheAvance().periodeDateFin()!=null)
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EORecette.REC_DATE_SAISIE_KEY+" <= %@", new NSArray(getMonInterfaceRechercheAvance().periodeDateFin())));
			if(isNotEmpty(getMonInterfaceRechercheAvance().getTfFournisseurNom().getText()))
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EORecette.FACTURE_PERSONNE_PERS_NOM_PRENOM_KEY + " caseInsensitiveLike %@", new NSArray("*" + getMonInterfaceRechercheAvance().getTfFournisseurNom().getText() + "*")));
			if(isNotEmpty(getMonInterfaceRechercheAvance().getTfFournisseurCode().getText()))
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EORecette.FACTURE_KEY+"."+EOFacture.FOURNIS_ULR_KEY + "." 
						+EOFournisUlr.FOU_CODE_KEY +" caseInsensitiveLike %@", new NSArray("*" +getMonInterfaceRechercheAvance().getTfFournisseurCode().getText() + "*")));
			if(isNotEmpty(getMonInterfaceRechercheAvance().getTfHtMax().getText()))
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EORecette.REC_HT_SAISIE_KEY+" <= %@", new NSArray(new BigDecimal(getMonInterfaceRechercheAvance().getTfHtMax().getText()))));
			if(isNotEmpty(getMonInterfaceRechercheAvance().getTfHtMin().getText()))
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EORecette.REC_HT_SAISIE_KEY+" >= %@", new NSArray(new BigDecimal(getMonInterfaceRechercheAvance().getTfHtMin().getText()))));
			if(isNotEmpty(getMonInterfaceRechercheAvance().getTfTtcMax().getText()))
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EORecette.REC_TTC_SAISIE_KEY+" <= %@", new NSArray(new BigDecimal(getMonInterfaceRechercheAvance().getTfTtcMax().getText()))));
			if(isNotEmpty(getMonInterfaceRechercheAvance().getTfTtcMin().getText()))
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EORecette.REC_TTC_SAISIE_KEY+" >= %@", new NSArray(new BigDecimal(getMonInterfaceRechercheAvance().getTfTtcMin().getText()))));
			if(isNotEmpty(getMonInterfaceRechercheAvance().getTfRefNoFact().getText()))
			{
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EORecette.FACTURE_KEY+"."+EOFacture.FACTURE_PAPIER_KEY+"."+EOFacturePapier.FAP_NUMERO_KEY+" = %@", new NSArray(new Integer(getMonInterfaceRechercheAvance().getTfRefNoFact().getText()))));
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EORecette.FACTURE_KEY+"."+EOFacture.EXERCICE_KEY+"."+EOExercice.PRIMARY_KEY_KEY+" = %@", new NSArray(((ApplicationClient)app).superviseur().currentExercice().exeExercice())));
			}
			if(isNotEmpty(getMonInterfaceRechercheAvance().getTfRefNoPrest().getText()))
			{
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EORecette.FACTURE_KEY+"."+EOFacture.FACTURE_PAPIER_KEY+"."+EOFacturePapier.PRESTATION_KEY+"."+EOPrestation.PREST_NUMERO_KEY+" = %@", new NSArray(new Integer(getMonInterfaceRechercheAvance().getTfRefNoPrest().getText()))));
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EORecette.FACTURE_KEY+"."+EOFacture.EXERCICE_KEY+"."+EOExercice.PRIMARY_KEY_KEY+" = %@", new NSArray(((ApplicationClient)app).superviseur().currentExercice().exeExercice())));
			}
			if(isEnableSearchRecette())
			{
				if(isNotEmpty(getMonInterfaceRechercheAvance().getTfRefUB().getText()))
					args.addObject(EOQualifier.qualifierWithQualifierFormat(EORecette.FACTURE_KEY+"."+EOFacture.ORGAN_KEY+"."+org.cocktail.kava.client.metier.EOOrgan.ORG_UB_KEY+" = %@", new NSArray(getMonInterfaceRechercheAvance().getTfRefUB().getText())));
				if(isNotEmpty(getMonInterfaceRechercheAvance().getTfRefCR().getText()))
					args.addObject(EOQualifier.qualifierWithQualifierFormat(EORecette.FACTURE_KEY+"."+EOFacture.ORGAN_KEY+"."+org.cocktail.kava.client.metier.EOOrgan.ORG_CR_KEY+" = %@", new NSArray(getMonInterfaceRechercheAvance().getTfRefCR().getText())));
				if(isNotEmpty(getMonInterfaceRechercheAvance().getTfRefSousCR().getText()))
					args.addObject(EOQualifier.qualifierWithQualifierFormat(EORecette.FACTURE_KEY+"."+EOFacture.ORGAN_KEY+"."+org.cocktail.kava.client.metier.EOOrgan.ORG_SOUSCR_KEY+" = %@", new NSArray(getMonInterfaceRechercheAvance().getTfRefSousCR().getText())));
				if(isNotEmpty(getMonInterfaceRechercheAvance().getTfRefNoRecette().getText()))
					args.addObject(EOQualifier.qualifierWithQualifierFormat(EORecette.REC_NUMERO_KEY+" = %@", new NSArray(new Integer(getMonInterfaceRechercheAvance().getTfRefNoRecette().getText()))));
				if(isNotEmpty(getMonInterfaceRechercheAvance().getTfRefNoTitre().getText()))
				{
					args.addObject(EOQualifier.qualifierWithQualifierFormat(EORecette.RECETTE_CTRL_PLANCOS_KEY+"."+EORecetteCtrlPlanco.TITRE_KEY+"."+EOTitre.TIT_NUMERO_KEY+" = %@", new NSArray(new Integer(getMonInterfaceRechercheAvance().getTfRefNoTitre().getText()))));
					args.addObject(EOQualifier.qualifierWithQualifierFormat(EORecette.RECETTE_CTRL_PLANCOS_KEY+"."+EORecetteCtrlPlanco.TITRE_KEY+"."+EOTitre.EXE_ORDRE_KEY+" = %@", new NSArray(((ApplicationClient)app).superviseur().currentExercice().exeExercice())));
				}
				if(isNotEmpty(getMonInterfaceRechercheAvance().getTfRefNoBordereau().getText()))
				{
					args.addObject(EOQualifier.qualifierWithQualifierFormat(EORecette.RECETTE_CTRL_PLANCOS_KEY+"."+EORecetteCtrlPlanco.TITRE_KEY+"."+EOTitre.BORDEREAU_KEY+"."+EOBordereau.BOR_NUM_KEY+" = %@", new NSArray(new Integer(getMonInterfaceRechercheAvance().getTfRefNoBordereau().getText()))));
					args.addObject(EOQualifier.qualifierWithQualifierFormat(EORecette.RECETTE_CTRL_PLANCOS_KEY+"."+EORecetteCtrlPlanco.TITRE_KEY+"."+EOTitre.EXE_ORDRE_KEY+" = %@", new NSArray(app.getCurrentExercice().exeOrdre())));
				}
			}
		} catch (Throwable e) {
			e.printStackTrace();
			return new NSArray();
		}
		return args;
	}

	


}
