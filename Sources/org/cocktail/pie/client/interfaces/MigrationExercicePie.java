/*
 * Copyright Cocktail, 2001-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.pie.client.interfaces;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTable;

import org.cocktail.application.palette.JTableViewCocktail;
import org.cocktail.application.palette.models.ColumnData;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.kava.client.finder.FinderExercice;
import org.cocktail.kava.client.finder.FinderFacturePapier;
import org.cocktail.kava.client.finder.FinderPrestation;
import org.cocktail.kava.client.finder.FinderTypeEtat;
import org.cocktail.kava.client.metier.EOCatalogue;
import org.cocktail.kava.client.metier.EOCatalogueResponsable;
import org.cocktail.kava.client.metier.EOFacturePapier;
import org.cocktail.kava.client.metier.EOFonction;
import org.cocktail.kava.client.metier.EOPersonne;
import org.cocktail.kava.client.metier.EOPrestation;
import org.cocktail.kava.client.metier.EOPrestationBascule;
import org.cocktail.kava.client.metier.EORepartStructure;
import org.cocktail.kava.client.metier.EOSecretariat;
import org.cocktail.kava.client.metier.EOStructureUlr;
import org.cocktail.kava.client.metier.EOTypePublic;
import org.cocktail.kava.client.metier.EOUtilisateur;
import org.cocktail.kava.client.qualifier.Qualifiers;
import org.cocktail.kava.client.qualifier.Qualifiers.QualifierKey;

import app.client.ApplicationClient;
import app.client.Prestation;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCodingAdditions;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class MigrationExercicePie extends JFrame implements ActionListener {
	/** Serial version ID. */
	private static final long serialVersionUID = 1L;

	private static ApplicationClient app = (ApplicationClient) EOApplication.sharedApplication();
	private InterfaceMigrationPie migr = null;
	private JTableViewCocktail tvcFacture;
	private JTableViewCocktail tvcPrestation;
	private NSMutableArrayDisplayGroup lesFactures = null;
	private NSMutableArrayDisplayGroup lesPrestation = null;
	private NSMutableArrayDisplayGroup lesEtatsPrestation = null;

	private EtatPrestation ETAT_TOUS = new EtatPrestation("Tous");
	private EtatPrestation ETAT_NON_VALIDE = new EtatPrestation("Non valide");
	private EtatPrestation ETAT_VALIDE_CLIENT = new EtatPrestation("Valide client");
	private EtatPrestation ETAT_VALIDE_PREST = new EtatPrestation("Valide prestataire");
	private EtatPrestation ETAT_VALIDE_FACTURE = new EtatPrestation("Valide & Facture");
	private EtatPrestation ETAT_ANNULER = new EtatPrestation("Annuler");

	private static String LIBELLE_PREST = "Lib / Client / Catalogue";
	private static String LIBELLE_FACT = "Lib / Client / Prestataire";

	private EOEditingContext ec = null;

	public MigrationExercicePie() {
		super("Bascule Prestation entre exercice " + app.superviseur().currentExercice().exeExercice().intValue() + " et " + (app.superviseur().currentExercice().exeExercice().intValue() + 1));
		migr = new InterfaceMigrationPie();
		getContentPane().add(migr);
		migr.getRbFacture().addActionListener(this);
		migr.getRbFacture().setEnabled(false);
		migr.getRbPrestation().addActionListener(this);
		migr.getRbPrestation().setSelected(true);
		migr.getBtFermer().addActionListener(this);
		migr.getBtFermer().setIcone("exit_16");
		migr.getBtBasculer().addActionListener(this);
		migr.getBtBasculer().setIcone("transfert");
		migr.getBtDupliquer().addActionListener(this);
		migr.getBtDupliquer().setIcone("dupliquer");
		migr.getBtDupliquer().setVisible(false);
		migr.getBtDupliquer().setEnabled(false);
		migr.getBtAnnuler().addActionListener(this);
		migr.getBtAnnuler().setIcone("cancel_16");
		migr.getBtAnnuler().setEnabled(false);
		migr.getBtAnnuler().setVisible(false);
		migr.getBtRecherche().addActionListener(this);
		migr.getBtRecherche().setIcone("loupe_16");
		migr.getBtRecherche().setText(null);
		majTableViewCocktail();
	}

	private void majPopupEtat() {
		if (lesEtatsPrestation == null)
		{
			lesEtatsPrestation = new NSMutableArrayDisplayGroup();
			lesEtatsPrestation.addObject(ETAT_TOUS);
			lesEtatsPrestation.addObject(ETAT_NON_VALIDE);
			lesEtatsPrestation.addObject(ETAT_VALIDE_CLIENT);
			lesEtatsPrestation.addObject(ETAT_VALIDE_PREST);
			lesEtatsPrestation.addObject(ETAT_VALIDE_FACTURE);
			lesEtatsPrestation.addObject(ETAT_ANNULER);
		}
		migr.getCbEtat().removeAllItems();
		migr.getCbEtat().setDisplayGroup(lesEtatsPrestation);
		migr.getCbEtat().setItemsWithKey("toString");
		migr.getCbEtat().setSelectedItem(ETAT_TOUS);
	}

	//Permet d'afficher la bonne tableView en fonction du choix Facture / Prestation
	private void majTableViewCocktail() {
		migr.getCbEtat().removeActionListener(this);
		if (tvcFacture == null)
		{
			tvcFacture = new JTableViewCocktail(cols(true), getLesFactures(), new Dimension(100, 100), JTable.AUTO_RESIZE_LAST_COLUMN);
		}
		if (tvcPrestation == null)
		{
			tvcPrestation = new JTableViewCocktail(cols(false), getLesPrestation(), new Dimension(100, 100), JTable.AUTO_RESIZE_LAST_COLUMN);
		}
		if (migr.getRbFacture().isSelected())
		{
			migr.getTvListFacture().initTableViewCocktail(tvcFacture);
			migr.getJLabelCocktail2().setText(LIBELLE_FACT);
		}
		else
		{
			migr.getTvListFacture().initTableViewCocktail(tvcPrestation);
			migr.getJLabelCocktail2().setText(LIBELLE_PREST);
		}
		migr.getTvListFacture().refresh();
		majPopupEtat();
		migr.getCbEtat().addActionListener(this);
	}

	private NSMutableArray cols(boolean isFacture) {
		NSMutableArray lesColumns = new NSMutableArray();

		if (isFacture) {
			ColumnData col = new ColumnData("No", 50, JLabel.LEFT, EOFacturePapier.FAP_NUMERO_KEY, Integer.class.getName());
			lesColumns.addObject(new ColumnData("No", 50, JLabel.LEFT, EOFacturePapier.FAP_NUMERO_KEY, Integer.class.getName()));
			lesColumns.addObject(new ColumnData("Ref", 50, JLabel.CENTER, EOFacturePapier.FAP_REF_KEY, String.class.getName()));
			lesColumns.addObject(new ColumnData("Libelle", 100, JLabel.CENTER, EOFacturePapier.FAP_LIB_KEY, String.class.getName()));
			lesColumns.addObject(new ColumnData("Client", 100, JLabel.CENTER, EOFacturePapier.PERSONNE_PERS_NOM_PRENOM_KEY, String.class.getName()));
			lesColumns.addObject(new ColumnData("Date", 80, JLabel.CENTER, EOFacturePapier.FAP_DATE_KEY, NSTimestamp.class.getName()));
			lesColumns.addObject(new ColumnData("Ht", 50, JLabel.RIGHT, EOFacturePapier.FAP_TOTAL_HT_KEY, BigDecimal.class.getName()));
			lesColumns.addObject(new ColumnData("Ttc", 50, JLabel.RIGHT, EOFacturePapier.FAP_TOTAL_TTC_KEY, BigDecimal.class.getName()));
		} else {
			lesColumns.addObject(new ColumnData("No", 50, JLabel.LEFT, EOPrestation.PREST_NUMERO_KEY, Integer.class.getName()));
			lesColumns.addObject(new ColumnData("Lib", 100, JLabel.CENTER, EOPrestation.PREST_LIBELLE_KEY, String.class.getName()));
			lesColumns.addObject(new ColumnData("Client", 100, JLabel.CENTER, EOPrestation.PERSONNE_PERS_NOM_PRENOM_KEY, String.class.getName()));
			lesColumns.addObject(new ColumnData("Date", 50, JLabel.CENTER, EOPrestation.PREST_DATE_KEY, NSTimestamp.class.getName()));
			lesColumns.addObject(new ColumnData("Facturation", 80, JLabel.CENTER, EOPrestation.PREST_DATE_FACTURATION_KEY, NSTimestamp.class.getName()));
			lesColumns.addObject(new ColumnData("Type", 50, JLabel.CENTER, EOPrestation.TYPE_PUBLIC_KEY + "." + EOTypePublic.TYPU_LIBELLE_KEY, String.class.getName()));
			lesColumns.addObject(new ColumnData("Catalogue", 100, JLabel.CENTER, EOPrestation.CATALOGUE_KEY + "." + EOCatalogue.CAT_LIBELLE_KEY, String.class.getName()));
		}
		return lesColumns;
	}

	public void open() {
		validate();
		setVisible(true);
		pack();
		ec = new EOEditingContext();
		majTableViewCocktail();
	}

	public void close() {
		if (ec != null && ec.hasChanges()) {
			ec.revert();
		}

		validate();
		setVisible(false);
		pack();
		Prestation.sharedInstance().update();
	}

	public void actionPerformed(ActionEvent arg0) {
		//		System.out.println("arg0.getSource() = "+arg0);
		if (migr.getBtFermer().equals(arg0.getSource())) {
			close();
			return;
		}
		if (migr.getRbPrestation().equals(arg0.getSource()) && migr.getRbPrestation().isSelected()) {
			majTableViewCocktail();
			return;
		}
		if (migr.getRbFacture().equals(arg0.getSource()) && migr.getRbFacture().isSelected()) {
			majTableViewCocktail();
			return;
		}
		if (migr.getBtRecherche().equals(arg0.getSource()) || (migr.getCbEtat().equals(arg0.getSource()) && arg0.getModifiers() == KeyEvent.BUTTON1_MASK)) {
			if (migr.getRbFacture().isSelected()) {
				rechercherFacture();
			} else {
				rechercherPrestation();
			}
			return;
		}

		if (migr.getBtBasculer().equals(arg0.getSource())) {
			if (migr.getRbFacture().isSelected()) {
			} else {
				basculerPrestation();
			}
			return;
		}

		app.showInfoDialogFonctionNonImplementee();
	}

	public void selectionChanged(Object arg0) {
		if (migr.getRbFacture().isSelected()) {
			rechercherFacture();
		} else {
			rechercherPrestation();
		}
		return;
	}

	private void rechercherPrestation() {
		lesPrestation.removeAllObjects();
		try {
			findPrestation();
		} catch (Throwable e) {
			app.showInfoDialog(e.getMessage());
		}
		migr.getTvListFacture().refresh();
	}

	private void findPrestation() throws Exception {
		NSMutableArray args = new NSMutableArray();
		args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PRESTATION_BASCULES_KEY + "." + EOPrestationBascule.PREST_ID_ORIGINE_BIS_KEY + " = nil", null));
		EOUtilisateur utilisateur = app.appUserInfo().utilisateur();
		if (app.canUseFonction(EOFonction.DROIT_VOIR_TOUTES_LES_PRESTATIONS, null)) {
			utilisateur = null;
		}
		if (utilisateur != null) {
			NSMutableArray orQuals = new NSMutableArray();

			// utilisateur
			orQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.UTILISATEUR_KEY + " = %@", new NSArray(utilisateur)));

			// responsable du catalogue du devis
			orQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.CATALOGUE_KEY + "." + EOCatalogue.CATALOGUE_RESPONSABLES_KEY
					+ "." + EOCatalogueResponsable.UTILISATEUR_KEY + " = %@", new NSArray(utilisateur)));

			// contact
			orQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.INDIVIDU_ULR_KEY + " = %@", new NSArray(utilisateur
					.personne().individuUlr())));

			// secretariats
			orQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PERSONNE_KEY + "." + EOPersonne.STRUCTURE_ULR_KEY + "."
					+ EOStructureUlr.SECRETARIATS_KEY + "." + EOSecretariat.INDIVIDU_ULR_KEY + " = %@", new NSArray(utilisateur.personne()
					.individuUlr())));

			// ttes personnes du groupe client
			orQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PERSONNE_KEY + "." + EOPersonne.STRUCTURE_ULR_KEY + "."
					+ EOStructureUlr.REPART_STRUCTURES_KEY + "." + EORepartStructure.PERSONNE_KEY + " = %@", new NSArray(utilisateur.personne())));

			// TODO ajouter les membres des sous-groupes du client de type 'PD'

			args.addObject(new EOOrQualifier(orQuals));
		}
		if (app.superviseur().currentExercice() != null) {
			args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.EXERCICE_KEY + " = %@", new NSArray(app.superviseur().currentExercice())));
		}
		if (migr.getTfNumero().getText() != null && !"".equals(migr.getTfNumero().getText()))
		{
			try {
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PREST_NUMERO_KEY + " =%@", new NSArray(new Integer(migr.getTfNumero().getText()))));
			} catch (NumberFormatException e) {
				throw new Exception("Le numero de la prestation doit etre un entier !!!");
			}
		}
		EtatPrestation etat = (EtatPrestation) migr.getCbEtat().getSelectedEOG();
		if (!ETAT_TOUS.equals(etat)) {
			if (ETAT_NON_VALIDE.equals(etat)) {
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.TYPE_ETAT_KEY + " =%@", new NSArray(FinderTypeEtat.typeEtatValide(ec))));
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PREST_DATE_VALIDE_CLIENT_KEY + " isEqualTo null", null));
			}

			if (ETAT_VALIDE_CLIENT.equals(etat)) {
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.TYPE_ETAT_KEY + " =%@", new NSArray(FinderTypeEtat.typeEtatValide(ec))));
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PREST_DATE_VALIDE_CLIENT_KEY + " isNotEqualTo null", null));
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PREST_DATE_VALIDE_PREST_KEY + " isEqualTo null", null));
			}

			if (ETAT_VALIDE_PREST.equals(etat)) {
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.TYPE_ETAT_KEY + " =%@", new NSArray(FinderTypeEtat.typeEtatValide(ec))));
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PREST_DATE_VALIDE_PREST_KEY + " isNotEqualTo null", null));
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PREST_DATE_CLOTURE_KEY + " isEqualTo null", null));
			}

			if (ETAT_VALIDE_FACTURE.equals(etat)) {
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.TYPE_ETAT_KEY + " =%@", new NSArray(FinderTypeEtat.typeEtatValide(ec))));
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PREST_DATE_CLOTURE_KEY + " isNotEqualTo null", null));
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.FACTURE_PAPIERS_KEY + " isNotEqualTo null", null));
			}

			if (ETAT_ANNULER.equals(etat)) {
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.TYPE_ETAT_KEY + " =%@", new NSArray(FinderTypeEtat.typeEtatAnnule(ec))));
			}
		}

		if (migr.getTfLibelle() != null && !migr.getTfLibelle().getText().equals("") && migr.getTfLibelle().getText().length() > 2) {
			NSMutableArray args2 = new NSMutableArray();
			args2.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PREST_LIBELLE_KEY + " caseInsensitiveLike %@", new NSArray("*" + migr.getTfLibelle().getText() + "*")));
			args2.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PERSONNE_PERS_NOM_PRENOM_KEY + " caseInsensitiveLike %@", new NSArray("*" + migr.getTfLibelle().getText() + "*")));
			args2.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.CATALOGUE_KEY + "." + EOCatalogue.CAT_LIBELLE_KEY + " caseInsensitiveLike %@", new NSArray("*" + migr.getTfLibelle().getText() + "*")));
			args2.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.CATALOGUE_KEY + "." + EOCatalogue.CAT_COMMENTAIRE_KEY + " caseInsensitiveLike %@", new NSArray("*" + migr.getTfLibelle().getText() + "*")));
			args.addObject(new EOOrQualifier(args2));
		}

		try {
			System.out.println("qualifier = " + args);
			lesPrestation.addObjectsFromArray(FinderPrestation.find(ec, new EOAndQualifier(args)));
		} catch (NumberFormatException e) {
			throw new Exception("Le numero de la prestation doit etre un entier !!!");
		} catch (IllegalArgumentException e) {
			throw new Exception("Aucune prestation trouvee!!!");
		}

		if (lesPrestation.count() == 0) {
			throw new Exception("Aucune prestation trouvee!!!");
		}
	}

	private void rechercherFacture() {
		lesFactures.removeAllObjects();
		try {
			findFacturation();
		} catch (Throwable e) {
			app.showInfoDialog(e.getMessage());
		}
		migr.getTvListFacture().refresh();
	}

	private void findFacturation() throws Exception {
		NSMutableArray args = new NSMutableArray();
		EOUtilisateur utilisateur = app.appUserInfo().utilisateur();
		if (app.canUseFonction(EOFonction.DROIT_VOIR_TOUTES_LES_FACTURES_PAPIER, null)) {
			utilisateur = null;
		}
		if (utilisateur != null) {
			args.addObject(EOQualifier.qualifierWithQualifierFormat(EOFacturePapier.UTILISATEUR_KEY + " = %@", new NSArray(utilisateur)));
		}
		if (app.superviseur().currentExercice() != null) {
			args.addObject(EOQualifier.qualifierWithQualifierFormat(EOFacturePapier.EXERCICE_KEY + " = %@", new NSArray(app.superviseur().currentExercice())));
		}
		if (migr.getTfNumero().getText() != null && !"".equals(migr.getTfNumero().getText())) {
			try {
				args.addObject(EOQualifier.qualifierWithQualifierFormat(EOFacturePapier.FAP_NUMERO_KEY + " = %@", new NSArray(new Integer(migr.getTfNumero().getText()))));
			} catch (NumberFormatException e) {
				throw new Exception("Le numero de la prestation doit etre un entier !!!");
			}
		}
		EtatPrestation etat = (EtatPrestation) migr.getCbEtat().getSelectedEOG();

		if (migr.getTfLibelle() != null && !migr.getTfLibelle().getText().equals("") && migr.getTfLibelle().getText().length() > 2) {
			String searchLibelleString = "*" + migr.getTfLibelle().getText() + "*";
			NSMutableArray args2 = new NSMutableArray();
			args2.addObject(EOQualifier.qualifierWithQualifierFormat(EOFacturePapier.FAP_LIB_KEY + " caseInsensitiveLike %@", new NSArray(searchLibelleString)));
			args2.addObject(EOQualifier.qualifierWithQualifierFormat(EOFacturePapier.PERSONNE_PERS_NOM_PRENOM_KEY + " caseInsensitiveLike %@", new NSArray(searchLibelleString)));
			args2.addObject(Qualifiers.getQualifierFactory(QualifierKey.FAP_FOUR_CLIENT_PRENOM_PERSONNE).build(
					EOQualifier.QualifierOperatorCaseInsensitiveLike, searchLibelleString));
			args2.addObject(Qualifiers.getQualifierFactory(QualifierKey.FAP_FOUR_CLIENT_NOM_PERSONNE).build(
					EOQualifier.QualifierOperatorCaseInsensitiveLike, searchLibelleString));
			args.addObject(new EOOrQualifier(args2));
		}

		try {
			lesFactures.addObjectsFromArray(FinderFacturePapier.find(ec, new EOAndQualifier(args)));
		} catch (NumberFormatException e) {
			throw new Exception("Le numero de la prestation doit etre un entier !!!");
		} catch (IllegalArgumentException e) {
			throw new Exception("Aucune prestation trouvee!!!");
		}

		if (lesFactures.count() == 0) {
			throw new Exception("Aucune prestation trouvee!!!");
		}
	}

	private void annulerPrestation() {
		try {
			org.cocktail.kava.client.ServerProxy.apiPrestationSoldePrestation(ec, (EOPrestation) migr.getTvListFacture().getSelectedObjects().lastObject(), app.appUserInfo().utilisateur());
		} catch (Exception e) {
			e.printStackTrace();
			app.showErrorDialog(e);
			return;
		}

		app.showInfoDialog("Prestation annulee");
		return;
	}

	private void basculerPrestation() {
		for (int i = 0; i < migr.getTvListFacture().getSelectedObjects().count(); i++) {
			EOPrestation prest = (EOPrestation) migr.getTvListFacture().getSelectedObjects().objectAtIndex(i);
			try {
				org.cocktail.kava.client.ServerProxy.apiPrestationBasculePrestation(ec, (EOPrestation) prest, app.appUserInfo().utilisateur(), FinderExercice.findExerciceEnCours(ec));
			} catch (Exception e) {
				e.printStackTrace();
				app.showErrorDialog(e);
				ec.invalidateObjectsWithGlobalIDs(globalIdForObjects(migr.getTvListFacture().getSelectedObjects()));
				lesPrestation.removeAllObjects();
				try {
					findPrestation();
				} catch (Throwable e1) {
				}
				migr.getTvListFacture().refresh();
				return;
			}
		}
		app.showInfoDialog("Prestation(s) basculee(s)");
		ec.invalidateObjectsWithGlobalIDs(globalIdForObjects(migr.getTvListFacture().getSelectedObjects()));
		lesPrestation.removeAllObjects();
		try {
			findPrestation();
		} catch (Throwable e) {
		}
		migr.getTvListFacture().refresh();
		return;
	}

	private NSArray globalIdForObjects(NSArray list) {
		NSMutableArray ids = new NSMutableArray();
		for (int i = 0; i < list.count(); i++) {
			EOGenericRecord eog = (EOGenericRecord) list.objectAtIndex(i);
			if (eog != null) {
				ids.addObject(ec.globalIDForObject(eog));
			}
		}
		return ids;
	}

	private void dupliquerPrestation() {
		try {
			org.cocktail.kava.client.ServerProxy.apiPrestationDupliquePrestation(ec, (EOPrestation) migr.getTvListFacture().getSelectedObjects().lastObject(), app.appUserInfo().utilisateur(), FinderExercice.findExerciceEnCours(ec));
		} catch (Exception e) {
			e.printStackTrace();
			app.showErrorDialog(e);
			return;
		}
		app.showInfoDialog("Prestation dupliquee");
		return;
	}

	public NSMutableArrayDisplayGroup getLesFactures() {
		if (lesFactures == null) {
			lesFactures = new NSMutableArrayDisplayGroup();
		}
		return lesFactures;
	}

	public void setLesFactures(NSMutableArrayDisplayGroup lesFactures) {
		this.lesFactures = lesFactures;
	}

	public NSMutableArrayDisplayGroup getLesPrestation() {
		if (lesPrestation == null) {
			lesPrestation = new NSMutableArrayDisplayGroup();
		}
		return lesPrestation;
	}

	public void setLesPrestation(NSMutableArrayDisplayGroup lesPrestation) {
		this.lesPrestation = lesPrestation;
	}

	class EtatPrestation implements NSKeyValueCodingAdditions {
		private String str = null;

		public EtatPrestation(String str) {
			this.str = str;
		}

		public void takeValueForKeyPath(Object arg0, String arg1) {
		}

		public Object valueForKeyPath(String arg0) {
			return null;
		}

		public void takeValueForKey(Object arg0, String arg1) {
		}

		public Object valueForKey(String arg0) {
			if ("toString".equals(arg0)) {
				return str;
			}
			return null;
		}

		public String toString() {
			return str;
		}

		public String getStr() {
			return str;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((str == null) ? 0 : str.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			EtatPrestation other = (EtatPrestation) obj;
			if (!getOuterType().equals(other.getOuterType())) {
				return false;
			}
			if (getStr() == null) {
				if (other.getStr() != null) {
					return false;
				}
			} else if (!getStr().equals(other.getStr())) {
				return false;
			}
			return true;
		}

		private MigrationExercicePie getOuterType() {
			return MigrationExercicePie.this;
		}
	}
}
