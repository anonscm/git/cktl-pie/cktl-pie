/*
 * InterfaceFactureCumulative.java
 *
 * Created on 19 novembre 2008, 14:14
 */

package org.cocktail.pie.client.interfaces;

import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.palette.JPanelCocktail;

/**
 *
 * @author  mparadot
 */
public class InterfaceFactureCumulative extends JPanelCocktail {
	private ControleurFactureCumulative responder;

    /** Creates new form InterfaceFactureCumulative */
    public InterfaceFactureCumulative() {
        initComponents();
    }

    public InterfaceFactureCumulative(ControleurFactureCumulative responder) {
        initComponents();
        this.responder = responder;
        initSuite();
    }

	/** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabelCocktail1 = new org.cocktail.application.palette.JLabelCocktail();
        jPanel2 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabelCocktail2 = new org.cocktail.application.palette.JLabelCocktail();
        jtfFiltreClient = new org.cocktail.application.palette.JTextFieldCocktail();
        jbtFindClient = new org.cocktail.application.palette.JButtonCocktail();
        jPanel6 = new javax.swing.JPanel();
        jLabelCocktail3 = new org.cocktail.application.palette.JLabelCocktail();
        jcbFiltreCatalogue = new org.cocktail.application.palette.JComboBoxCocktail();
        jPanel7 = new javax.swing.JPanel();
        jLabelCocktail4 = new org.cocktail.application.palette.JLabelCocktail();
        jbtRechercheAvancee = new org.cocktail.application.palette.JButtonCocktail();
        jbtRechercher = new org.cocktail.application.palette.JButtonCocktail();
        jPanel3 = new javax.swing.JPanel();
        jtvcListePrestations = new org.cocktail.application.palette.JTableViewCocktail();
        jPanel4 = new javax.swing.JPanel();
        jbtValider = new org.cocktail.application.palette.JButtonCocktail();
        jbtFermer = new org.cocktail.application.palette.JButtonCocktail();

        jLabelCocktail1.setBackground(javax.swing.UIManager.getDefaults().getColor("nbProgressBar.popupText.selectBackground"));
        jLabelCocktail1.setText("Sélectionnez les prestations (non facturées et clôturées) à regrouper sur une prestation cumulative, puis validez");

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabelCocktail1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 735, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(jLabelCocktail1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabelCocktail2.setText("Client");

        jtfFiltreClient.setText("jTextFieldCocktail1");

        jbtFindClient.setText("jButtonCocktail1");
        jbtFindClient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtFindClientActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel5Layout = new org.jdesktop.layout.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel5Layout.createSequentialGroup()
                        .add(jtfFiltreClient, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 103, Short.MAX_VALUE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jbtFindClient, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(61, 61, 61))
                    .add(jPanel5Layout.createSequentialGroup()
                        .add(jLabelCocktail2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(164, Short.MAX_VALUE))))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel5Layout.createSequentialGroup()
                .add(jLabelCocktail2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jtfFiltreClient, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jbtFindClient, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabelCocktail3.setText("Catalogue");

        jcbFiltreCatalogue.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jcbFiltreCatalogue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbFiltreCatalogueActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel6Layout = new org.jdesktop.layout.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jcbFiltreCatalogue, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 273, Short.MAX_VALUE)
                    .add(jLabelCocktail3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel6Layout.createSequentialGroup()
                .add(jLabelCocktail3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jcbFiltreCatalogue, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(18, Short.MAX_VALUE))
        );

        jLabelCocktail4.setText("Recherche avancée");

        jbtRechercheAvancee.setText("jButtonCocktail2");
        jbtRechercheAvancee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtRechercheAvanceeActionPerformed(evt);
            }
        });

        jbtRechercher.setText("Rechercher");
        jbtRechercher.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtRechercherActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel7Layout = new org.jdesktop.layout.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jbtRechercheAvancee, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 25, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabelCocktail4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jbtRechercher, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel7Layout.createSequentialGroup()
                .add(jLabelCocktail4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jbtRechercheAvancee, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jbtRechercher, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .add(jPanel5, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel7, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel7, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(jPanel6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(jPanel5, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout jPanel3Layout = new org.jdesktop.layout.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .add(jtvcListePrestations, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 735, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jtvcListePrestations, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 345, Short.MAX_VALUE)
        );

        jbtValider.setText("Valider");
        jbtValider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtValiderActionPerformed(evt);
            }
        });

        jbtFermer.setText("Annuler");
        jbtFermer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtFermerActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel4Layout = new org.jdesktop.layout.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(547, Short.MAX_VALUE)
                .add(jbtFermer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 97, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(jbtValider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 96, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(17, Short.MAX_VALUE)
                .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jbtValider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jbtFermer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(jPanel3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(jPanel4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jbtFindClientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtFindClientActionPerformed
        responder.actionAfficheFinderClient();
    }//GEN-LAST:event_jbtFindClientActionPerformed

    private void jbtRechercheAvanceeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtRechercheAvanceeActionPerformed
        responder.actionAfficheRechercheAvancee();
    }//GEN-LAST:event_jbtRechercheAvanceeActionPerformed

    private void jbtRechercherActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtRechercherActionPerformed
        responder.actionRechercher();
    }//GEN-LAST:event_jbtRechercherActionPerformed

    private void jbtFermerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtFermerActionPerformed
        responder.actionAnnuler();
    }//GEN-LAST:event_jbtFermerActionPerformed

    private void jbtValiderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtValiderActionPerformed
        responder.actionSelectionner();
    }//GEN-LAST:event_jbtValiderActionPerformed

    private void jcbFiltreCatalogueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbFiltreCatalogueActionPerformed
        if(evt.getModifiers()!= 0)
            responder.actionRechercher();
    }//GEN-LAST:event_jcbFiltreCatalogueActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.cocktail.application.palette.JLabelCocktail jLabelCocktail1;
    private org.cocktail.application.palette.JLabelCocktail jLabelCocktail2;
    private org.cocktail.application.palette.JLabelCocktail jLabelCocktail3;
    private org.cocktail.application.palette.JLabelCocktail jLabelCocktail4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private org.cocktail.application.palette.JButtonCocktail jbtFermer;
    private org.cocktail.application.palette.JButtonCocktail jbtFindClient;
    private org.cocktail.application.palette.JButtonCocktail jbtRechercheAvancee;
    private org.cocktail.application.palette.JButtonCocktail jbtRechercher;
    private org.cocktail.application.palette.JButtonCocktail jbtValider;
    private org.cocktail.application.palette.JComboBoxCocktail jcbFiltreCatalogue;
    private org.cocktail.application.palette.JTextFieldCocktail jtfFiltreClient;
    private org.cocktail.application.palette.JTableViewCocktail jtvcListePrestations;
    // End of variables declaration//GEN-END:variables

    public org.cocktail.application.palette.JButtonCocktail getJbtFermer() {
        return jbtFermer;
    }

    public org.cocktail.application.palette.JButtonCocktail getJbtFindClient() {
        return jbtFindClient;
    }

    public org.cocktail.application.palette.JButtonCocktail getJbtRechercheAvancee() {
        return jbtRechercheAvancee;
    }

    public org.cocktail.application.palette.JButtonCocktail getJbtRechercher() {
        return jbtRechercher;
    }

    public org.cocktail.application.palette.JButtonCocktail getJbtValider() {
        return jbtValider;
    }

    public org.cocktail.application.palette.JComboBoxCocktail getJcbFiltreCatalogue() {
        return jcbFiltreCatalogue;
    }

    public org.cocktail.application.palette.JTextFieldCocktail getJtfFiltreClient() {
        return jtfFiltreClient;
    }

    public org.cocktail.application.palette.JTableViewCocktail getJtvcListePrestations() {
        return jtvcListePrestations;
    }

    private void initSuite() {
		//Decoration des boutons
    	getJbtFermer().setIcone(IconeCocktail.FERMER);
    	getJbtFindClient().setIcone(IconeCocktail.RECHERCHER2);
        getJbtFindClient().setText("");
    	getJbtRechercheAvancee().setVisible(false);//setIcone(IconeCocktail.FILTRER);
    	jLabelCocktail4.setVisible(false);
    	getJbtRechercher().setIcone(IconeCocktail.FILTRER);
    	getJbtValider().setIcone(IconeCocktail.VALIDER);
    	//Initialisation graphique de la liste des prestation
    	getJtvcListePrestations().initTableViewCocktail(responder.getInitialisationListePrestations());
	cleanInterface();
	}

	public void cleanInterface() {
            getJtfFiltreClient().setText("");
            responder.initComboBoxCatalogue(getJcbFiltreCatalogue());
            updateUI();
	}

}
