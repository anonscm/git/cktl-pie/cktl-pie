/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.pie.client.interfaces;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.VFournisseur;
import org.cocktail.application.client.swingfinder.SwingFinderFournisseur;
import org.cocktail.application.client.swingfinder.SwingFinderOrgan;
import org.cocktail.application.client.swingfinder.nibctrl.SwingFinderEOGenericRecord;
import org.cocktail.application.palette.CalendarCocktail;

import com.webobjects.foundation.NSTimestamp;

public abstract class FinderRechercheAvance extends SwingFinderEOGenericRecord {

	public static String METHODE_ACTION_CLEAN="actionClean";
    public static final String METHODE_ACTION_SELECTIONNER = "actionSelectionner";
    public static final String METHODE_ACTION_ANNULER = "actionAnnuler";
    public static final String METHODE_ACTION_DATE_DEBUT = "actionDateDebut";
    public static final String METHODE_ACTION_DATE_FIN = "actionDateFin";
    
	private InterfaceRechercheAvance monInterfaceRechercheAvance;
	private SwingFinderFournisseur monFinderFournisseur;
	private CalendarCocktail monCalendarCocktailDebut;
	private CalendarCocktail monCalendarCocktailFin;
	private SwingFinderOrgan monFinderOrgan;
	
	private boolean open;
	private boolean enableSearchRecette;
	

	public FinderRechercheAvance(ApplicationCocktail ctrl, int alocation_x, int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);
		setModal(false);
		setOpen(false);
		setEnableSearchRecette(false);
	}
	
	public void actionAnnuler() {
		setOpen(false);
		super.actionAnnuler();
	}

	public InterfaceRechercheAvance getMonInterfaceRechercheAvance() {
		if(monInterfaceRechercheAvance==null)
			monInterfaceRechercheAvance = new InterfaceRechercheAvance(this);
		return monInterfaceRechercheAvance;
	}

	public void afficherFenetre() {
		super.afficherFenetre();
	}
	
	public SwingFinderFournisseur getMonFinderFournisseur(){
		if(monFinderFournisseur==null)
			monFinderFournisseur = new SwingFinderFournisseur(app,this,0,0,500,400,true);
		return monFinderFournisseur;
	}
	
	public CalendarCocktail getMonCalendarCocktailDebut(){
		if(monCalendarCocktailDebut==null)
			monCalendarCocktailDebut = new CalendarCocktail(app,this,0,0,150,150,true);
		return monCalendarCocktailDebut;
	}
	
	public CalendarCocktail getMonCalendarCocktailFin(){
		if(monCalendarCocktailFin==null)
			monCalendarCocktailFin = new CalendarCocktail(app,this,0,0,150,150,true);
		return monCalendarCocktailFin;
	}
	
	public SwingFinderOrgan getMonFinderOrgan(){
		if(monFinderOrgan==null)
			monFinderOrgan = new SwingFinderOrgan(app,this,0,0,500,400,true);
		return monFinderOrgan;
	}
	
	public void actionDateDebut(){
		getMonCalendarCocktailDebut().afficherFenetre();
	}
	
	public void actionDateFin(){
		getMonCalendarCocktailFin().afficherFenetre();
	}
	
	public abstract void actionClean();
	
	public void swingFinderTerminer(Object obj) {
		if(getMonCalendarCocktailDebut().equals(obj))
		{
			getMonInterfaceRechercheAvance().setPeriodeDateDebut((NSTimestamp)getMonCalendarCocktailDebut().getResultatNSTimestamp().lastObject());
		}
		if(getMonCalendarCocktailFin().equals(obj))
		{
			getMonInterfaceRechercheAvance().setPeriodeDateFin((NSTimestamp)getMonCalendarCocktailFin().getResultatNSTimestamp().lastObject());
		}
		if(getMonFinderFournisseur().equals(obj))
		{
			getMonInterfaceRechercheAvance().setFournisseur((VFournisseur)getMonFinderFournisseur().getResultat().lastObject());
		}
		if(getMonFinderOrgan().equals(obj))
		{
			getMonInterfaceRechercheAvance().setOrgan((EOOrgan)getMonFinderOrgan().getResultat().lastObject());
		}
	}
	
	public boolean isNotEmpty(String str){
		return !("".equals(str)||str==null);
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}
	
	public void setEnableSearchRecette(boolean bool){
		enableSearchRecette = bool;
		getMonInterfaceRechercheAvance().setEnableSearchRecette(enableSearchRecette);
	}

	public boolean isEnableSearchRecette() {
		return enableSearchRecette;
	}

}
