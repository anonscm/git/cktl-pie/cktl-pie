/*
 * Copyright Cocktail, 2001-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.pie.client.interfaces;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JTable;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.eof.EOTypeEtat;
import org.cocktail.application.client.eof.VFournisseur;
import org.cocktail.application.client.swingfinder.SwingFinderFournisseur;
import org.cocktail.application.client.swingfinder.nibctrl.SwingFinderEOGenericRecord;
import org.cocktail.application.palette.JComboBoxCocktail;
import org.cocktail.application.palette.JTableViewCocktail;
import org.cocktail.application.palette.models.ColumnData;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.kava.client.factory.FactoryPrestation;
import org.cocktail.kava.client.finder.FinderCatalogue;
import org.cocktail.kava.client.finder.FinderPrestation;
import org.cocktail.kava.client.finder.FinderTypeEtat;
import org.cocktail.kava.client.metier.EOCatalogue;
import org.cocktail.kava.client.metier.EOFonction;
import org.cocktail.kava.client.metier.EOIndividuUlr;
import org.cocktail.kava.client.metier.EOPrestation;
import org.cocktail.kava.client.metier.EOTypePublic;
import org.cocktail.kava.client.metier.EOUtilisateur;

import app.client.ApplicationClient;
import app.client.Superviseur;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCodingAdditions;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class ControleurFactureCumulative extends SwingFinderEOGenericRecord {
	private SwingFinderFournisseur monFinderFournisseur;
	private NSMutableArrayDisplayGroup listDatasPrestations;
	private static String ACTION_FAIRE_FACTURE_OK="okPourFaireFacture";
	private boolean faireFacture;
	private EOCatalogue catalogueTous;
	private NSMutableArrayDisplayGroup dgCatalogue;

	private static ApplicationClient myapp = (ApplicationClient) EOApplication.sharedApplication();

	public ControleurFactureCumulative() {
		super((ApplicationCocktail) EOApplication.sharedApplication(), 0, 0, 800, 600);
		setModal(false);
		creationFenetre(new InterfaceFactureCumulative(this), "Prestation cumulative");
	}

	public InterfaceFactureCumulative getMonInterfaceFactureCumulative() {
		return (InterfaceFactureCumulative) currentNib;
	}

	public void actionAfficheFinderClient(){
		getMonFinderFournisseur().afficherFenetreFinder(null);
	}

	public void actionAfficheRechercheAvancee(){

	}

	public void actionRechercher(){
		if(currentNib==null)
			return ;
		System.out.println("ControleurFactureCumulative.actionRechercher()");
		NSArray list = FinderPrestation.find(app.getAppEditingContext(),qualifierWithFiltre());
		getListDatasPrestations().removeAllObjects();
		getListDatasPrestations().addObjectsFromArray(list);
		getMonInterfaceFactureCumulative().getJtvcListePrestations().refresh();
	}

	public void actionAnnuler() {
        try {
        	masquerFenetre();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}


	public void actionSelectionner() {
		faireFacture = false;
		if(getMonInterfaceFactureCumulative().getJtvcListePrestations().getSelectedObjects().count()==0)
		{
			fenetreDeDialogueInformation("Veuillez selectionner des prestations pour faire une facture cumulative !");
			return ;
		}
		if(getMonInterfaceFactureCumulative().getJtvcListePrestations().getSelectedObjects().count()==1)
		{
			fenetreDeDialogueInformation("Veuillez selectionner plus d'une prestation pour faire une facture cumulative !");
			return ;
		}

		fenetreDeDialogueYESCancel("êtes-vous sûr de vouloir faire une facture cumulative des prestations suivantes : "+getlisteNoPrestSelected(), this, ACTION_FAIRE_FACTURE_OK);
		System.out.println("faireFacture:"+faireFacture);
		if(faireFacture)
		{
	        try {
	        	goFactureCumulative();
	        	masquerFenetre();
	        	((ApplicationClient)app).superviseur().setOngletSelected(Superviseur.ONGLET_PRESTATION);
			} catch (Throwable e) {
				e.printStackTrace();
				fenetreDeDialogueInformation("Erreur lors de la generation de la facture cumulative : "+e.getMessage());
				return;
			}
		}
	}

	private void goFactureCumulative() throws Exception {
		//appel
		boolean ok = FactoryPrestation.regrouperPrestation(app.getAppEditingContext(), getMonInterfaceFactureCumulative().getJtvcListePrestations().getSelectedObjects(), getUtilisateurConnecteeForDroit(null));
		if(!ok)
			throw new Exception("Impossible de faire le regroupement des prestations.");
		NSArray listPrestationRetour = getMonInterfaceFactureCumulative().getJtvcListePrestations().getSelectedObjects();
		System.out.println("listPrestationRetour : " + listPrestationRetour);
	}

	public void afficherFenetre() {
		getMonInterfaceFactureCumulative().cleanInterface();
		actionRechercher();
		super.afficherFenetre();
	}

	private String getlisteNoPrestSelected() {
		return getlisteNoPrest(getMonInterfaceFactureCumulative().getJtvcListePrestations().getSelectedObjects());
	}

	private String getlisteNoPrest(NSArray listPresta) {
		NSArray list =(NSArray)listPresta.valueForKey(EOPrestation.PREST_NUMERO_KEY);
		return list.componentsJoinedByString(", ");
	}

	public void okPourFaireFacture(){
		faireFacture = true;
	}

	public JTableViewCocktail getInitialisationListePrestations() {
		System.out.println("ControleurFactureCumulative.getInitialisationListePrestations()");
		NSMutableArray lesColumns = new NSMutableArray();
		lesColumns.addObject(new ColumnData( "No",50, JLabel.LEFT ,EOPrestation.PREST_NUMERO_KEY,Integer.class.getName()));
		lesColumns.addObject(new ColumnData( "Libelle de la prestation",150, JLabel.LEFT ,EOPrestation.PREST_LIBELLE_KEY,String.class.getName()));
		lesColumns.addObject(new ColumnData( "Client",80, JLabel.LEFT ,EOPrestation.PERSONNE_PERS_NOM_PRENOM_KEY,String.class.getName()));
		lesColumns.addObject(new ColumnData( "Contact",150, JLabel.LEFT ,EOPrestation.INDIVIDU_ULR_KEY+"."+EOIndividuUlr.PERSONNE_PERS_NOM_PRENOM_KEY,String.class.getName()));
		lesColumns.addObject(new ColumnData( "Date de la prestation",50, JLabel.LEFT ,EOPrestation.PREST_DATE_KEY,NSTimestamp.class.getName()));
		lesColumns.addObject(new ColumnData( "Etat",50, JLabel.LEFT ,EOPrestation.TYPE_ETAT_KEY+"."+EOTypeEtat.TYET_LIBELLE_KEY,String.class.getName()));
		lesColumns.addObject(new ColumnData( "Type",80, JLabel.LEFT ,EOPrestation.TYPE_PUBLIC_KEY+"."+EOTypePublic.TYPU_LIBELLE_KEY,String.class.getName()));
		lesColumns.addObject(new ColumnData( "Validé client",80, JLabel.LEFT ,EOPrestation.PREST_DATE_VALIDE_CLIENT_KEY,NSTimestamp.class.getName()));
		lesColumns.addObject(new ColumnData( "Validé Prestataire",80, JLabel.LEFT ,EOPrestation.PREST_DATE_VALIDE_PREST_KEY,NSTimestamp.class.getName()));
		lesColumns.addObject(new ColumnData( "Cloturé",80, JLabel.LEFT ,EOPrestation.PREST_DATE_CLOTURE_KEY,NSTimestamp.class.getName()));
		JTableViewCocktail tv = new JTableViewCocktail(lesColumns,getListDatasPrestations(),new Dimension(200,200),JTable.AUTO_RESIZE_LAST_COLUMN);
		return tv;
	}

	private NSMutableArrayDisplayGroup getListDatasPrestations() {
		if(listDatasPrestations==null)
		{
			listDatasPrestations = new NSMutableArrayDisplayGroup();
		}
		return listDatasPrestations;
	}

	private EOQualifier qualifierWithFiltre(){
		NSMutableArray args = new NSMutableArray();
		if(isNonNull(getMonInterfaceFactureCumulative().getJtfFiltreClient().getText()))
			args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PERSONNE_PERS_NOM_PRENOM_KEY+" caseInsensitiveLike %@", new NSArray(getMonInterfaceFactureCumulative().getJtfFiltreClient().getText()+"*")));
		if(isNonNull(getMonInterfaceFactureCumulative().getJcbFiltreCatalogue().getSelectedEOG()))
		{
			System.out.println("isNonNull(getMonInterfaceFactureCumulative().getJcbFiltreCatalogue().getSelectedEOG()) = " + isNonNull(getMonInterfaceFactureCumulative().getJcbFiltreCatalogue().getSelectedEOG()));
			args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.CATALOGUE_KEY+"."+EOCatalogue.CAT_LIBELLE_KEY+" = %@", new NSArray(getMonInterfaceFactureCumulative().getJcbFiltreCatalogue().getSelectedEOG().valueForKey(EOCatalogue.CAT_LIBELLE_KEY))));
		}
		args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.TYPE_ETAT_KEY+"."+EOTypeEtat.TYET_LIBELLE_KEY+" = %@", new NSArray(EOTypeEtat.ETAT_VALIDE)));
		args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PREST_DATE_FACTURATION_KEY+" = nil", null));
		args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PREST_DATE_VALIDE_CLIENT_KEY+" != nil", null));
		args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PREST_DATE_VALIDE_PREST_KEY+" != nil", null));
		args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PREST_DATE_CLOTURE_KEY+" != nil", null));
//		if(getFinderRechercheAvancee().getResultat().count()!=0)
//			args.addObject(EOQualifier.qualifierWithQualifierFormat(" = %@", getFinderRechercheAvancee().getResultat());
		args.addObject(FinderPrestation.defaultQualifierWithDroit(getUtilisateurConnecteeForDroit(EOFonction.DROIT_VOIR_TOUTES_LES_PRESTATIONS), myapp.superviseur().currentExercice()));
		return new EOAndQualifier(args);

	}

	private FinderPrestationRechercheAvance getFinderRechercheAvancee() {
		return null;
	}

	private boolean isNonNull(String str){
		return getMonInterfaceFactureCumulative().getJtfFiltreClient().getText()!=null && !"".equals(getMonInterfaceFactureCumulative().getJtfFiltreClient().getText());
	}

	private boolean isNonNull(NSKeyValueCodingAdditions obj){
		return !getCatalogueLibelleTous().catLibelle().equals(obj.valueForKey(EOCatalogue.CAT_LIBELLE_KEY));
	}

	private EOUtilisateur getUtilisateurConnecteeForDroit(String droit){
		if(droit==null)
			return ((ApplicationClient)app).appUserInfo().utilisateur();
		if (((ApplicationClient)app).canUseFonction(droit, null)) {
			return null;
		}
		return ((ApplicationClient)app).appUserInfo().utilisateur();
	}

	public void initComboBoxCatalogue(JComboBoxCocktail jcbFiltreCatalogue) {
		if(dgCatalogue!=null)
		{
			jcbFiltreCatalogue.setSelectedItemWithEOG(getCatalogueLibelleTous());
			return;
		}
		dgCatalogue = new NSMutableArrayDisplayGroup();
		dgCatalogue.addObject(getCatalogueLibelleTous());
		dgCatalogue.addObjectsFromArray(FinderCatalogue.find(app.getAppEditingContext(),getUtilisateurConnecteeForDroit(EOFonction.DROIT_VOIR_TOUS_LES_CATALOGUES), null, FinderTypeEtat.typeEtatValide(app.getAppEditingContext()), null));
		jcbFiltreCatalogue.setDisplayGroup(dgCatalogue);
		jcbFiltreCatalogue.setItemsWithKey(EOCatalogue.CAT_LIBELLE_KEY);
		jcbFiltreCatalogue.setSelectedItemWithEOG(getCatalogueLibelleTous());
	}

	private EOCatalogue getCatalogueLibelleTous(){
		if(catalogueTous==null)
		{
			catalogueTous = new EOCatalogue();
			catalogueTous.setCatLibelle("Tous");
		}
		return catalogueTous;
	}

	public SwingFinderFournisseur getMonFinderFournisseur() {
		if(monFinderFournisseur==null)
		{
			monFinderFournisseur = new SwingFinderFournisseur(app,this,0,0,640,480,true);
		}
		return monFinderFournisseur;
	}

	public void swingFinderTerminer(Object obj) {
		if(obj==getMonFinderFournisseur())
		{
			if(getMonFinderFournisseur().getResultat().lastObject()!=null)
			{
				getMonInterfaceFactureCumulative().getJtfFiltreClient().setText(((VFournisseur)getMonFinderFournisseur().getResultat().lastObject()).adrNom());
				actionRechercher();
				return;
			}
		}
		super.swingFinderTerminer(obj);
	}

}
