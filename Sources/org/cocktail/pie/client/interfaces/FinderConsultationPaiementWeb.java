/*
 * Copyright Cocktail, 2001-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.pie.client.interfaces;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.math.BigDecimal;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.swingfinder.nibctrl.SwingFinderEOGenericRecord;
import org.cocktail.application.palette.CalendarCocktail;
import org.cocktail.application.palette.JTableViewCocktail;
import org.cocktail.application.palette.models.ColumnData;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.kava.client.metier.EOFacturePapier;
import org.cocktail.kava.client.metier.EOParametres;

import app.client.ApplicationClient;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import org.cocktail.fwkcktlwebapp.common.util.SystemCtrl;

public class FinderConsultationPaiementWeb extends SwingFinderEOGenericRecord{
    protected static final String METHODE_ACTION_ANNULER = "actionAnnuler";
    protected static final String METHODE_ACTION_DATE_DEBUT = "actionDateDebut";
    protected static final String METHODE_ACTION_DATE_FIN = "actionDateFin";
    protected static final String METHODE_ACTION_FILTRE_TICKET = "actionFiltreTicket";
    protected static final String METHODE_ACTION_FETCH_FACTURE = "actionFetchFacture";
    protected static final String METHODE_ACTION_DETAIL= "actionDetail";
    protected static final String METHODE_ACTION_MAJ_FACTURE= "actionMajFacture";
	private InterfaceConsultationPaiementWeb monConsultationPaiementWeb;
	private CalendarCocktail monDateDebut;
	private CalendarCocktail monDateFin;
	private NSMutableArrayDisplayGroup listTicket;
	private NSMutableArrayDisplayGroup listFacture;



	public FinderConsultationPaiementWeb(ApplicationCocktail ctrl, int alocation_x, int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);
		creationFenetre(getMonConsultationPaiementWeb(), "Consultation des paiements par le Web");
		initTableView();
	}

	private void initTableView(){
		NSMutableArray lesColumns = new NSMutableArray();
		lesColumns.addObject(new ColumnData( "No",50, JLabel.LEFT ,EOFacturePapier.FAP_NUMERO_KEY,Integer.class.getName()));
		lesColumns.addObject(new ColumnData( "Ref",50, JLabel.CENTER ,EOFacturePapier.FAP_REF_KEY,String.class.getName()));
		lesColumns.addObject(new ColumnData( "Libelle",100, JLabel.CENTER ,EOFacturePapier.FAP_LIB_KEY,String.class.getName()));
		lesColumns.addObject(new ColumnData( "Client",100, JLabel.CENTER ,EOFacturePapier.PERSONNE_PERS_NOM_PRENOM_KEY,String.class.getName()));
		lesColumns.addObject(new ColumnData( "Date",80, JLabel.CENTER ,EOFacturePapier.FAP_DATE_KEY,NSTimestamp.class.getName()));
		lesColumns.addObject(new ColumnData( "Ttc",50, JLabel.RIGHT ,EOFacturePapier.FAP_TOTAL_TTC_KEY,BigDecimal.class.getName()));
		lesColumns.addObject(new ColumnData( "Ref. Paiement Web",100, JLabel.CENTER ,EOFacturePapier.FAP_REFERENCE_REGLEMENT_KEY,String.class.getName()));
		JTableViewCocktail v1 = new JTableViewCocktail(lesColumns,getListFacture(),new Dimension(100,100),JTable.AUTO_RESIZE_LAST_COLUMN);
		getMonConsultationPaiementWeb().getTvListeFacturePaiementWeb().initTableViewCocktail(v1);
		getMonConsultationPaiementWeb().getTvListeFacturePaiementWeb().addDelegateSelectionListener(this, METHODE_ACTION_DETAIL);
		getMonConsultationPaiementWeb().getTvListeFacturePaiementWeb().addDelegateKeyListener(this, METHODE_ACTION_DETAIL);
		NSMutableArray lesColumns2 = new NSMutableArray();
		lesColumns2.addObject(new ColumnData( "Référence",100, JLabel.LEFT ,"ibsCmd",String.class.getName()));
		lesColumns2.addObject(new ColumnData( "Date Transaction",80, JLabel.CENTER ,"dateTrans",NSTimestamp.class.getName()));
		lesColumns2.addObject(new ColumnData( "Somme",50, JLabel.RIGHT ,"ibsTotal",BigDecimal.class.getName()));
		lesColumns2.addObject(new ColumnData( "Email",100, JLabel.CENTER ,"ibsPorteur",String.class.getName()));
		lesColumns2.addObject(new ColumnData( "Essais",50, JLabel.CENTER ,"nbTentatives",Integer.class.getName()));
		lesColumns2.addObject(new ColumnData( "Refus",50, JLabel.CENTER ,"nbRefus",Integer.class.getName()));
		lesColumns2.addObject(new ColumnData( "N° d'autorisation",100, JLabel.RIGHT ,"numAuto",String.class.getName()));
		lesColumns2.addObject(new ColumnData( "N° de transaction",100, JLabel.RIGHT ,"numTrans",String.class.getName()));
		JTableViewCocktail v2 = new JTableViewCocktail(lesColumns2,getListTicket(),new Dimension(100,100),JTable.AUTO_RESIZE_LAST_COLUMN);
		getMonConsultationPaiementWeb().getTvListeTicketWeb().initTableViewCocktail(v2);
		getMonConsultationPaiementWeb().getTvListeTicketWeb().addDelegateSelectionListener(this, METHODE_ACTION_FETCH_FACTURE);
		getMonConsultationPaiementWeb().getTvListeTicketWeb().addDelegateKeyListener(this, METHODE_ACTION_FETCH_FACTURE);
		initColumnRenderers(getMonConsultationPaiementWeb().getTvListeTicketWeb().getTable(),new ColorRendererTicket());

	}

	public InterfaceConsultationPaiementWeb getMonConsultationPaiementWeb() {
		if(monConsultationPaiementWeb==null)
			monConsultationPaiementWeb = new InterfaceConsultationPaiementWeb(this);
		return monConsultationPaiementWeb;
	}

	public CalendarCocktail getMonDateDebut() {
		if(monDateDebut==null)
			monDateDebut = new CalendarCocktail(app,this,0,0,150,150,true);
		return monDateDebut;
	}

	public CalendarCocktail getMonDateFin() {
		if(monDateFin==null)
			monDateFin = new CalendarCocktail(app,this,0,0,150,150,true);
		return monDateFin;
	}

	public void actionDateDebut(){
		getMonDateDebut().afficherFenetre(getMonConsultationPaiementWeb().dateDebut());
	}

	public void actionDateFin(){
		getMonDateFin().afficherFenetre(getMonConsultationPaiementWeb().dateFin());
	}

	public void actionMajFacture(){
		if(getMonConsultationPaiementWeb().getTvListeTicketWeb().getSelectedObjects().lastObject()==null)
			return ;
		fenetreDeDialogueYESNOCancel("Etes-vous sûr de vouloir regenerer le lien entre le paiement web et la facture ?\nAttention, le client recevra la facture daté d'aujourd'huit par email !!!", this, "actionFaireMaj", "noAction");
	}

	public void actionFaireMaj(){
		EOGenericRecord obj = (EOGenericRecord)getMonConsultationPaiementWeb().getTvListeTicketWeb().getSelectedObjects().lastObject();
		String url = app.getApplicationParametre(EOParametres.PARAM_CONFIG_URL_APP_WEB)+"/wa/payboxReponse?IBS_CMD="+obj.valueForKey("ibsCmd")+"&IBS_TOTAL="+obj.valueForKey("ibsTotal");
		SystemCtrl.openFileInBrowser(url);
	}

	public void noAction(){

	}

	public NSMutableArrayDisplayGroup getListTicket() {
		if(listTicket==null)
			listTicket = new NSMutableArrayDisplayGroup();
		return listTicket;
	}

	public NSMutableArrayDisplayGroup getListFacture() {
		if(listFacture==null)
			listFacture = new NSMutableArrayDisplayGroup();
		return listFacture;
	}

	public void actionFiltreTicket(){
		getMonConsultationPaiementWeb().getBtMajFacture().setEnabled(false);
		if(getMonConsultationPaiementWeb().dateDebut()==null && getMonConsultationPaiementWeb().dateFin()==null)
			return;
		NSMutableArray args = new NSMutableArray();
		args.addObject(EOQualifier.qualifierWithQualifierFormat("idAppli like %@", new NSArray(app.getApplicationParametre(EOParametres.PARAM_CONFIG_URL_APP_WEB))));
		args.addObject(EOQualifier.qualifierWithQualifierFormat("numTrans <> nil", null));
		args.addObject(EOQualifier.qualifierWithQualifierFormat("numAuto <> '0'", null));
		if(getMonConsultationPaiementWeb().dateDebut()!=null)
			args.addObject(EOQualifier.qualifierWithQualifierFormat("dateTrans >= %@",new NSArray(getMonConsultationPaiementWeb().dateDebut())));
		if(getMonConsultationPaiementWeb().dateFin()!=null)
			args.addObject(EOQualifier.qualifierWithQualifierFormat("dateTrans <= %@",new NSArray(getMonConsultationPaiementWeb().dateFin())));
		if(getMonConsultationPaiementWeb().sommeMin()!=null)
			args.addObject(EOQualifier.qualifierWithQualifierFormat("ibsTotal >= %@",new NSArray(getMonConsultationPaiementWeb().sommeMin())));
		if(getMonConsultationPaiementWeb().sommeMax()!=null)
			args.addObject(EOQualifier.qualifierWithQualifierFormat("ibsTotal <= %@",new NSArray(getMonConsultationPaiementWeb().sommeMax())));
		if(getMonConsultationPaiementWeb().getTfEmail()!=null && getMonConsultationPaiementWeb().getTfEmail().getText().length()>3)
			args.addObject(EOQualifier.qualifierWithQualifierFormat("ibsPorteur caseInsensitiveLike %@",new NSArray("*"+getMonConsultationPaiementWeb().getTfEmail().getText()+"*")));
		NSMutableArray sort = new NSMutableArray();
		sort.addObject(EOSortOrdering.sortOrderingWithKey("dateTrans", EOSortOrdering.CompareDescending));
		getMonConsultationPaiementWeb().getTvListeTicketWeb().getData().removeAllObjects();
		getMonConsultationPaiementWeb().getTvListeTicketWeb().getData().addObjectsFromArray(app.getToolsCocktailEOF().fetchArrayWithNomTableWithQualifierWithSort(app.getAppEditingContext(), "Paybox", new EOAndQualifier(args), sort));
		getMonConsultationPaiementWeb().getTvListeTicketWeb().refresh();
		getMonConsultationPaiementWeb().getTvListeFacturePaiementWeb().getData().removeAllObjects();
		getMonConsultationPaiementWeb().getTvListeFacturePaiementWeb().refresh();
		getMonConsultationPaiementWeb().cleanInterface();
	}

	public void actionFetchFacture(){
		getMonConsultationPaiementWeb().getBtMajFacture().setEnabled(false);
		try {
			EOGenericRecord obj = (EOGenericRecord)getMonConsultationPaiementWeb().getTvListeTicketWeb().getSelectedObjects().lastObject();
			getMonConsultationPaiementWeb().getTvListeFacturePaiementWeb().getData().removeAllObjects();
			getMonConsultationPaiementWeb().getTvListeFacturePaiementWeb().refresh();
			getMonConsultationPaiementWeb().cleanInterface();
			if(obj==null)
			{
				return ;
			}
			getMonConsultationPaiementWeb().getTvListeFacturePaiementWeb().getData().removeAllObjects();
			getMonConsultationPaiementWeb().getTvListeFacturePaiementWeb().getData().addObjectsFromArray((NSArray)obj.valueForKey("facturePapier"));
			getMonConsultationPaiementWeb().getTvListeFacturePaiementWeb().refresh();
			actionDetail();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void actionDetail(){
		EOGenericRecord obj = (EOGenericRecord)getMonConsultationPaiementWeb().getTvListeTicketWeb().getSelectedObjects().lastObject();
		if(obj==null)
			return;
		EOFacturePapier fap = (EOFacturePapier) ((NSArray)obj.valueForKey("facturePapier")).lastObject();
		if(fap!=null)
		{
			getMonConsultationPaiementWeb().setDateTransaction(fap.fapDateReglement());
			getMonConsultationPaiementWeb().getLabelNoFacture().setText("" + fap.fapNumero().floatValue());
			getMonConsultationPaiementWeb().getLabelNoPrestation().setText(""+fap.prestation().prestNumero().intValue());
			getMonConsultationPaiementWeb().getLabelRefWeb().setText(fap.fapReferenceReglement());
		}
		else
			getMonConsultationPaiementWeb().getBtMajFacture().setEnabled(true);
		getMonConsultationPaiementWeb().getLabelEmailTransaction().setText((String)obj.valueForKey("ibsPorteur"));
		getMonConsultationPaiementWeb().getLabelMontantTransaction().setText(((Double)obj.valueForKey("ibsTotal")).toString());

	}

	public void afficherFenetre() {
		actionFiltreTicket();
		super.afficherFenetre();
	}

	public void initColumnRenderers(JTable table,final DefaultTableCellRenderer renderer) {
		for (int indexColone = 0; indexColone < table.getColumnModel().getColumnCount(); indexColone++) {
			if (renderer != null) {
				System.out.println("table.getColumnModel().getColumn(indexColone).getCellRenderer() = " + table.getColumnModel().getColumn(indexColone).getHeaderRenderer());
//				if(indexColone!=1)
					table.getColumnModel().getColumn(indexColone).setCellRenderer(renderer);
			}
		}
	}

	public void swingFinderTerminer(Object sender) {
		if(sender.equals(getMonDateDebut()))
			getMonConsultationPaiementWeb().setDateDebut((NSTimestamp) getMonDateDebut().getResultatNSTimestamp().lastObject());
		if(sender.equals(getMonDateFin()))
			getMonConsultationPaiementWeb().setDateFin((NSTimestamp) getMonDateFin().getResultatNSTimestamp().lastObject());
	}

	public class ColorRendererTicket extends DefaultTableCellRenderer implements TableCellRenderer {

		public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus,
				final int row, final int column) {

			JLabel label = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			if (isSelected) {
                //selectedBorder is a solid border in the color
				label.setBackground(table.getSelectionBackground());
            } else {
                //unselectedBorder is a solid border in the color
            	EOGenericRecord obj = (EOGenericRecord)getListTicket().objectAtIndex(row);
                if(obj!=null && ((NSArray)obj.valueForKey("facturePapier")).count()==0)
                	label.setBackground(new Color(200,50,50));
                else
                	label.setBackground(new Color(50,200,50));
            }
			label.setText(getTextForObjects(value));
			label.setForeground(Color.BLACK);
	        return label;
	    }
	}

	private String getTextForObjects(Object value){
		if (value instanceof NSTimestamp) {
			return ApplicationClient.formatDateToString((NSTimestamp)value,"%d/%m/%Y à %H:%M:%S");
		}
		if (value instanceof Double)  {
			Double val = (Double) value;
			return val.toString().replaceAll("\\.", ",");
		}
		return value.toString();
	}



}
