/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.server.reports;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import org.cocktail.application.serveur.eof.EODevise;
import org.cocktail.application.serveur.eof.EOExercice;
import org.cocktail.kava.server.finder.FinderDevise;
import org.cocktail.kava.server.finder.FinderParametreJefyAdmin;
import org.cocktail.kava.server.finder.FinderParametreMaracuja;
import org.cocktail.kava.server.finder.FinderParametres;
import org.cocktail.kava.server.finder.FinderPays;
import org.cocktail.kava.server.finder.FinderPlanComptable;
import org.cocktail.kava.server.finder.FinderTypeApplication;
import org.cocktail.kava.server.finder.FinderTypeEtat;
import org.cocktail.kava.server.finder.FinderTypePublic;
import org.cocktail.kava.server.metier.EOAdresse;
import org.cocktail.kava.server.metier.EOArticlePrestation;
import org.cocktail.kava.server.metier.EODepenseCtrlPlanco;
import org.cocktail.kava.server.metier.EOFacturePapier;
import org.cocktail.kava.server.metier.EOFacturePapierAdrClient;
import org.cocktail.kava.server.metier.EOFacturePapierLigne;
import org.cocktail.kava.server.metier.EOFournisUlr;
import org.cocktail.kava.server.metier.EOIndividuUlr;
import org.cocktail.kava.server.metier.EOParametreJefyAdmin;
import org.cocktail.kava.server.metier.EOParametreMaracuja;
import org.cocktail.kava.server.metier.EOParametres;
import org.cocktail.kava.server.metier.EOPersonne;
import org.cocktail.kava.server.metier.EOPersonneTelephone;
import org.cocktail.kava.server.metier.EOPiDepRec;
import org.cocktail.kava.server.metier.EOPlanComptable;
import org.cocktail.kava.server.metier.EOPrestation;
import org.cocktail.kava.server.metier.EOPrestationAdrClient;
import org.cocktail.kava.server.metier.EOPrestationLigne;
import org.cocktail.kava.server.metier.EORecette;
import org.cocktail.kava.server.metier.EORecetteCtrlPlanco;
import org.cocktail.kava.server.metier.EORepartPersonneAdresse;
import org.cocktail.kava.server.metier.EOTypeNoTel;
import org.cocktail.kava.server.metier.EOTypePublic;
import org.cocktail.kava.server.metier.EOTypeTel;
import org.cocktail.kava.server.metier.service.FournisseurService;
import org.cocktail.pieFwk.common.exception.NoResultException;

import app.server.Application;
import app.server.tools.StringTool;
import app.server.tools.ZXMLSerialize;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSComparator;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

import org.cocktail.fwkcktlwebapp.common.print.CktlPrinter;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.fwkcktlwebapp.common.util.StreamCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

public class ReportFactorySix {
	private static final String SIXID_PIE_DEVIS_PARAM = "SIXID_PIE_DEVIS";
	private static final String SIXID_PIE_FACTURE_PARAM = "SIXID_PIE_FACTURE";
	private static final String SIXID_PIE_FACTURE_ANGLAIS_PARAM = "SIXID_PIE_FACTURE_ANGLAIS";
	private static final String EXPORT_RELATION_ORGAN_STRUCTURE = "organ/structureulr/";
	private CktlPrinter printer;
	private FournisseurService fournisseurService = FournisseurService.instance();
	private static ReportFactorySix rf;

	public ReportFactorySix(Application app) throws Exception {
		super();
		try {
			printer = CktlPrinter.newDefaultInstance(app.config());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new Exception("Le pilote d'impression n'a pas ete trouve");
		}
	}

	private static void initRF(Application app) throws Exception {
		if (rf == null) {
			try {
				rf = new ReportFactorySix(app);
			} catch (Exception e1) {
				e1.printStackTrace();
				throw new Exception("Impossible de generer le fichier PDF : " + e1.getMessage());
			}
		}
	}

	public static boolean checkMaquetteExists(Application app, String maquetteId) {
		try {
			initRF(app);
			return rf.printer.checkTemplate(maquetteId);
		} catch (Exception e) {
			return false;
		}
	}

	public static NSData printDevis(Application app, EOEditingContext ec, EOPrestation prestation, NSDictionary metadata) throws Exception {
		initRF(app);
		int printJobId = -1;
		try {
			printJobId = rf.getPDFJobIdForDevis(app, ec, prestation, app.getParam(SIXID_PIE_DEVIS_PARAM), metadata);
		} catch (Exception e1) {
			e1.printStackTrace();
			throw new Exception("Impossible de generer le fichier PDF : " + e1.getMessage());
		}
		return getPdf(printJobId);
	}

	public static NSData printFacturePapier(Application app, EOEditingContext ec, EOFacturePapier facturePapier, NSDictionary metadata)
			throws Exception {
		initRF(app);
		int printJobId = -1;
		try {
			printJobId = rf.getPDFJobIdForFacturePapier(app, ec, facturePapier, app.getParam(SIXID_PIE_FACTURE_PARAM), metadata, false);
		} catch (Exception e1) {
			e1.printStackTrace();
			throw new Exception("Impossible de generer le fichier PDF : " + e1.getMessage());
		}
		return getPdf(printJobId);
	}

	public static NSData printFacturePapierAnglais(Application app, EOEditingContext ec, EOFacturePapier facturePapier, NSDictionary metadata)
			throws Exception {
		initRF(app);
		int printJobId = -1;
		try {
			printJobId = rf.getPDFJobIdForFacturePapier(app, ec, facturePapier, app.getParam(SIXID_PIE_FACTURE_ANGLAIS_PARAM), metadata, true);
		} catch (Exception e1) {
			e1.printStackTrace();
			throw new Exception("Impossible de generer le fichier PDF : " + e1.getMessage());
		}
		return getPdf(printJobId);
	}

	private static NSData getPdf(int printJobId) throws Exception {
		if ((printJobId != -1)) {
			try {
				InputStream pdfStream = rf.getPdfStream(printJobId);
				if (pdfStream == null) {
					throw new Exception("Le flux PDF est nul.");
				}
				ByteArrayOutputStream tmpByteArrayOutputStream = new ByteArrayOutputStream();
				StreamCtrl.writeContentToStream(pdfStream, tmpByteArrayOutputStream, -1);
				return new NSData(tmpByteArrayOutputStream.toByteArray());
			} catch (Exception e) {
				e.printStackTrace();
				throw new Exception("Impossible de recuperer le fichier PDF : " + e.getMessage());
			}
		}
		else {
			throw new Exception("Impossible de recuperer le fichier PDF.");
		}
	}

	private int getPDFJobIdForDevis(Application app, EOEditingContext ec, EOPrestation prestation, String maquetteID, NSDictionary metaData)
			throws Exception {
		if (prestation == null) {
			throw new Exception("Aucun devis transmis pour l'impression, rien a imprimer.");
		}
		StringWriter myStringWriter = new StringWriter();
		CktlXMLWriter myCktlXMLWriter = new CktlXMLWriter(myStringWriter);
		myCktlXMLWriter.setUseCompactMode(true);
		myCktlXMLWriter.setEscapeSpecChars(true);
		EOTypePublic typeFormationContinue = FinderTypePublic.typePublicFormationContinue(ec);
		boolean isFormationContinue = false;
		try {
			myCktlXMLWriter.startDocument();
			myCktlXMLWriter.startElement("reportdevis");

			// ajout des meta data
			NSMutableDictionary newMetadata;
			if (metaData == null) {
				newMetadata = new NSMutableDictionary();
			}
			else {
				newMetadata = metaData.mutableClone();
			}
			newMetadata.takeValueForKey(app.getParam("APP_ALIAS"), "appalias");
			if (newMetadata != null) {
				ZXMLSerialize tmpZXMLSerialize = new ZXMLSerialize(myCktlXMLWriter, new NSArray());
				tmpZXMLSerialize.wantToEscapeSpecialsChars = true;
				tmpZXMLSerialize.objectToXml(newMetadata, "metadata");
			}

			// indicateur pour signaler si c'est de la facture interne ou externe ou fc
			if (prestation.typePublic().equals(typeFormationContinue)) {
				myCktlXMLWriter.writeElement("typeprestation", "FORMATION CONTINUE");
				isFormationContinue = true;
			}
			else {
				if (prestation.typePublic().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(ec))) {
					myCktlXMLWriter.writeElement("typeprestation", "INTERNE");
				}
				else {
					myCktlXMLWriter.writeElement("typeprestation", "EXTERNE");
				}
			}
			// indicateur pour signaler le type de public
			myCktlXMLWriter.writeElement("typepublic", prestation.typePublic().typuLibelle());
			myCktlXMLWriter.writeElement("lieuimpression", FinderParametreJefyAdmin.find(ec, EOParametreJefyAdmin.PARAM_VILLE, prestation.exercice()));
			// On remplace la date du jour par la date de la prestation !!!!
			if (prestation != null && prestation.prestDate() != null)
				myCktlXMLWriter.writeElement("dateimpression", (new NSTimestampFormatter("%Y-%m-%d")).format(prestation.prestDate()));
			else
				myCktlXMLWriter.writeElement("dateimpression", (new NSTimestampFormatter("%Y-%m-%d")).format(new NSTimestamp()));

			addEtablissmentInXml(app, ec, prestation.exercice(), prestation.fournisUlrPrest(), myCktlXMLWriter, false);

			EOPrestationAdrClient currentPrestAdrClient = prestation.currentPrestationAdresseClient();
			EOAdresse currentPrestAdresse = currentPrestAdrClient != null ? currentPrestAdrClient.toAdresse() : null;
			addClientInXml(myCktlXMLWriter, ec, prestation.fournisUlr(), currentPrestAdresse,
					prestation.personne(), prestation.individuUlr());
			addFournisseurInXml(myCktlXMLWriter, ec, prestation.fournisUlrPrest());
			addTotauxInXmlForPrestation(myCktlXMLWriter, prestation);
			EODevise devise = getDeviseEnCours(ec, prestation.exercice());
			if (devise != null) {
				myCktlXMLWriter.writeElement("libmonnaie", StringTool.ifNull(devise.devLibelle(), ""));
			}

			myCktlXMLWriter.startElement("devis");
			ZXMLSerialize tmpZXMLSerialize = new ZXMLSerialize(myCktlXMLWriter, new NSArray());
			tmpZXMLSerialize.wantToEscapeSpecialsChars = true;

			tmpZXMLSerialize.objectToXml(prestation, "devisdetail");
			tmpZXMLSerialize.objectToXml(prestation.prestationBudgetClient().organ(), "organ");
			tmpZXMLSerialize.objectToXml(prestation.organ(), "organprest");
			EOPlanComptable compteDepenseBudgetClient = FinderPlanComptable.findById(
					ec, prestation.exercice(), prestation.prestationBudgetClient().pcoNum());
			tmpZXMLSerialize.objectToXml(compteDepenseBudgetClient, "plancodepense");
			tmpZXMLSerialize.objectToXml(prestation.personne().structureUlr(), "structureulr");
			if (prestation.individuUlr() != null) {
				tmpZXMLSerialize.objectToXml(prestation.individuUlr(), "individuulr");
			}
			if (prestation.catalogue() != null) {
				tmpZXMLSerialize.objectToXml(prestation.catalogue(), "catalogue");
				tmpZXMLSerialize.objectToXml(prestation.catalogue().cataloguePrestation(), "catalogueprestation");
			}
			// Ajouter les lignes + options et remises
			MyPRLIGComp dligComp = new MyPRLIGComp();
			NSMutableArray prestationLignesArticles = prestation.prestationLignes().mutableClone();
			EOQualifier.filterArrayWithQualifier(prestationLignesArticles, EOQualifier.qualifierWithQualifierFormat(
					EOPrestationLigne.PRESTATION_LIGNE_PERE_KEY + " = nil", null));
			prestationLignesArticles.sortUsingComparator(dligComp);

			for (int i = 0; i < prestationLignesArticles.count(); i++) {
				myCktlXMLWriter.startElement("devisligne");
				EOPrestationLigne tmpLig = (EOPrestationLigne) prestationLignesArticles.objectAtIndex(i);
				NSArray prestationLignesOptionsRemisesTmp = tmpLig.prestationLignes().sortedArrayUsingComparator(dligComp);
				addPrestationLigneInXml(myCktlXMLWriter, tmpLig, isFormationContinue);
				for (int j = 0; j < prestationLignesOptionsRemisesTmp.count(); j++) {
					addPrestationLigneInXml(myCktlXMLWriter, (EOPrestationLigne) prestationLignesOptionsRemisesTmp.objectAtIndex(j), isFormationContinue);
				}
				myCktlXMLWriter.endElement();
			}
			myCktlXMLWriter.endElement();// devis
			myCktlXMLWriter.endElement(); // reportdevis
			myCktlXMLWriter.close();

			byte[] xmlbytes = myStringWriter.toString().getBytes();
			ByteArrayInputStream xmlstream = new ByteArrayInputStream(xmlbytes);
			System.out.println("xmlstream = " + new String(xmlbytes));
			int jobId = genererPDF(xmlstream, xmlbytes.length, maquetteID);
			return jobId;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Erreur lors de la generation du document PDF : " + e.getMessage());
		}
	}

	private int getPDFJobIdForFacturePapier(Application app, EOEditingContext ec, EOFacturePapier facturePapier, String maquetteID,
			NSDictionary metaData, boolean anglais) throws Exception {
		if (facturePapier == null) {
			throw new Exception("Aucune facture transmise pour l'impression, rien a imprimer.");
		}
		StringWriter myStringWriter = new StringWriter();
		CktlXMLWriter myCktlXMLWriter = new CktlXMLWriter(myStringWriter);
		myCktlXMLWriter.setUseCompactMode(true);
		myCktlXMLWriter.setEscapeSpecChars(true);
		EOTypePublic typeFormationContinue = FinderTypePublic.typePublicFormationContinue(ec);
		try {
			myCktlXMLWriter.startDocument();
			myCktlXMLWriter.startElement("reportfacturepapier");

			// ajout des meta data
			NSMutableDictionary newMetadata;
			if (metaData == null) {
				newMetadata = new NSMutableDictionary();
			} else {
				newMetadata = metaData.mutableClone();
			}
			newMetadata.takeValueForKey(app.getParam("APP_ALIAS"), "appalias");
			if (newMetadata != null) {
				ZXMLSerialize tmpZXMLSerialize = new ZXMLSerialize(myCktlXMLWriter, new NSArray(EXPORT_RELATION_ORGAN_STRUCTURE));
				tmpZXMLSerialize.wantToEscapeSpecialsChars = true;
				tmpZXMLSerialize.objectToXml(newMetadata, "metadata");
			}

			myCktlXMLWriter.writeElement("lieuimpression", ville(ec, facturePapier));

			if (facturePapier.fapDate() != null) {
				myCktlXMLWriter.writeElement("dateimpression",
						(new NSTimestampFormatter("%Y-%m-%d")).format(facturePapier.fapDate()));
			} else {
				myCktlXMLWriter.writeElement("dateimpression",
						(new NSTimestampFormatter("%Y-%m-%d")).format(new NSTimestamp()));
			}

			if (facturePapier.fapDate() != null) {
				myCktlXMLWriter.writeElement("datefacture",
						(new NSTimestampFormatter("%Y-%m-%d")).format(facturePapier.fapDate()));
			} else {
				myCktlXMLWriter.writeElement("datefacture",
						(new NSTimestampFormatter("%Y-%m-%d")).format(new NSTimestamp()));
			}

			addEtablissmentInXml(app, ec, facturePapier.exercice(), facturePapier.fournisUlrPrest(), myCktlXMLWriter, anglais);

			EOFacturePapierAdrClient currentFapAdrClient = facturePapier.currentAdresseClient();
			EOAdresse currentAdresseClient = currentFapAdrClient != null ? currentFapAdrClient.toAdresse() : null;
			addClientInXml(myCktlXMLWriter, ec, facturePapier.fournisUlr(), currentAdresseClient,
					facturePapier.personne(), facturePapier.individuUlr());
			addFournisseurInXml(myCktlXMLWriter, ec, facturePapier.fournisUlrPrest());
			addTotauxInXmlForFacturePapier(myCktlXMLWriter, facturePapier);
			EODevise devise = getDeviseEnCours(ec, facturePapier.exercice());
			if (devise != null) {
				myCktlXMLWriter.writeElement("libmonnaie", StringTool.ifNull(devise.devLibelle(), ""));
			}

			myCktlXMLWriter.startElement("facturepapier");
			ZXMLSerialize tmpZXMLSerialize = new ZXMLSerialize(myCktlXMLWriter, new NSArray(EXPORT_RELATION_ORGAN_STRUCTURE));
			tmpZXMLSerialize.wantToEscapeSpecialsChars = true;

			tmpZXMLSerialize.objectToXml(facturePapier, "facturepapierdetail");
			tmpZXMLSerialize.objectToXml(facturePapier.prestation(), "prestation");
			if (facturePapier.convention() != null) {
				tmpZXMLSerialize.objectToXml(facturePapier.convention(), "convention");
			}
			if (facturePapier.facture() != null && facturePapier.facture().recettes() != null && facturePapier.facture().recettes().count() > 0) {
				EORecette recette = (EORecette) facturePapier.facture().recettes().objectAtIndex(0);
				if (recette != null && recette.recetteCtrlPlancos() != null && recette.recetteCtrlPlancos().count() > 0) {
					EORecetteCtrlPlanco rpco = (EORecetteCtrlPlanco) recette.recetteCtrlPlancos().objectAtIndex(0);
					if (rpco != null && rpco.titre() != null) {
						tmpZXMLSerialize.objectToXml(rpco.titre(), "titre");
						if (rpco.titre().bordereau() != null) {
							tmpZXMLSerialize.objectToXml(rpco.titre().bordereau(), "bordereautitre");
						}
					}
				}
			}
			if (facturePapier.typePublic().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(ec))) {
				if (facturePapier.facture() != null && facturePapier.facture().recettes() != null
						&& facturePapier.facture().recettes().count() > 0) {
					EORecette recette = (EORecette) facturePapier.facture().recettes().objectAtIndex(0);
					if (recette != null && recette.piDepRecs() != null && recette.piDepRecs().count() > 0) {
						EOPiDepRec pdr = (EOPiDepRec) recette.piDepRecs().objectAtIndex(0);
						if (pdr != null && pdr.depenseBudget() != null && pdr.depenseBudget().depenseCtrlPlancos() != null
								&& pdr.depenseBudget().depenseCtrlPlancos().count() > 0) {
							EODepenseCtrlPlanco dpco = (EODepenseCtrlPlanco) pdr.depenseBudget().depenseCtrlPlancos().objectAtIndex(0);
							if (dpco != null && dpco.mandat() != null) {
								tmpZXMLSerialize.objectToXml(dpco.mandat(), "mandat");
								if (dpco.mandat().bordereau() != null) {
									tmpZXMLSerialize.objectToXml(dpco.mandat().bordereau(), "bordereaumandat");
								}
							}
						}
					}
				}
			}
			tmpZXMLSerialize.objectToXml(facturePapier.exercice(), "exercice");
			// indicateur pour signaler si c'est de la facture interne ou externe
			if (facturePapier.typePublic().equals(typeFormationContinue)) {
				myCktlXMLWriter.writeElement("typeprestation", "FORMATION CONTINUE");
			} else {
				if (facturePapier.typePublic().typeApplication().equals(
						FinderTypeApplication.typeApplicationPrestationInterne(ec))) {
					myCktlXMLWriter.writeElement("typeprestation", "INTERNE");
				} else {
					myCktlXMLWriter.writeElement("typeprestation", "EXTERNE");
				}
			}
			// libelle de la facture
			if (facturePapier.typeEtat().equals(FinderTypeEtat.typeEtatNon(ec))) {
				if (anglais) {
					myCktlXMLWriter.writeElement("libellefacture", "PRO FORMA INVOICE No");
				} else {
					myCktlXMLWriter.writeElement("libellefacture", "FACTURE PRO FORMA No");
				}
			} else {
				if (facturePapier.typePublic().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(ec))) {
					if (anglais) {
						myCktlXMLWriter.writeElement("libellefacture", "INVOICE (internal) No");
					} else {
						myCktlXMLWriter.writeElement("libellefacture", "FACTURE sur P.I. No");
					}
				} else {
					if (anglais) {
						myCktlXMLWriter.writeElement("libellefacture", "INVOICE No");
					} else {
						myCktlXMLWriter.writeElement("libellefacture", "FACTURE No");
					}
				}
			}
			// indicateur pour signaler le type de public
			myCktlXMLWriter.writeElement("typepublic", facturePapier.typePublic().typuLibelle());

			// infos budgetaires prestataire
			tmpZXMLSerialize.objectToXml(facturePapier.organ(), "organ");
			tmpZXMLSerialize.objectToXml(facturePapier.typeCreditRec(), "typecreditrec");
			tmpZXMLSerialize.objectToXml(facturePapier.lolfNomenclatureRecette(), "lolfnomenclaturerecette");
			tmpZXMLSerialize.objectToXml(facturePapier.planComptable(), "plancomptable");
			tmpZXMLSerialize.objectToXml(facturePapier.planComptableTva(), "plancomptabletva");
			tmpZXMLSerialize.objectToXml(facturePapier.planComptableCtp(), "plancomptablectp");
			tmpZXMLSerialize.objectToXml(facturePapier.codeAnalytique(), "codeanalytique");
			tmpZXMLSerialize.objectToXml(facturePapier.convention(), "convention");

			// infos budgetaires client interne
			myCktlXMLWriter.startElement("facturepapierbudgetclient");
			if (facturePapier.engageBudget() != null && facturePapier.engageBudget().organ() != null) {
				tmpZXMLSerialize.objectToXml(facturePapier.engageBudget().organ(), "organ");
			}
			myCktlXMLWriter.endElement();

			// Ajouter les lignes + options et remises
			MyFLIGComp fligComp = new MyFLIGComp();
			NSMutableArray facturePapierLignesArticles = facturePapier.facturePapierLignes().mutableClone();
			EOQualifier.filterArrayWithQualifier(facturePapierLignesArticles, EOQualifier.qualifierWithQualifierFormat(
					EOFacturePapierLigne.FACTURE_PAPIER_LIGNE_PERE_KEY + " = nil", null));
			facturePapierLignesArticles.sortUsingComparator(fligComp);

			for (int i = 0; i < facturePapierLignesArticles.count(); i++) {
				myCktlXMLWriter.startElement("facturepapierligne");
				EOFacturePapierLigne tmpLig = (EOFacturePapierLigne) facturePapierLignesArticles.objectAtIndex(i);
				NSArray facturePapierLignesOptionsRemisesTmp = tmpLig.facturePapierLignes().sortedArrayUsingComparator(fligComp);

				addFacturePapierLigneInXml(myCktlXMLWriter, tmpLig);
				for (int j = 0; j < facturePapierLignesOptionsRemisesTmp.count(); j++) {
					addFacturePapierLigneInXml(myCktlXMLWriter, (EOFacturePapierLigne) facturePapierLignesOptionsRemisesTmp.objectAtIndex(j));
				}
				myCktlXMLWriter.endElement(); // facturepapierligne

			}
			myCktlXMLWriter.endElement(); // facturepapier
			myCktlXMLWriter.endElement(); // reportfacturepapier
			myCktlXMLWriter.close();

			byte[] xmlbytes = myStringWriter.toString().getBytes();
			ByteArrayInputStream xmlstream = new ByteArrayInputStream(xmlbytes);
			int jobId = genererPDF(xmlstream, xmlbytes.length, maquetteID);
			return jobId;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Erreur lors de la generation du document PDF : " + e.getMessage());
		}
	}

	private String ville(EOEditingContext ec, EOFacturePapier facturePapier) {
		String ville = null;
		if (parametreLocalisationPersonnalisee(ec, facturePapier.exercice()) &&	facturePapier != null) {
			try {
				EOAdresse adresseFournisseurPrest = fournisseurService.currentAdresseFacturation(
						ec, facturePapier.fournisUlrPrest());
				ville = adresseFournisseurPrest.ville();
			} catch (NoResultException nre) {
				System.out.println(nre.getMessage() + " On utilise la ville parametree dans JefyAdmin.VILLE.");
			}
		}

		if (ville == null) {
			ville = FinderParametreJefyAdmin.find(ec, EOParametreJefyAdmin.PARAM_VILLE, facturePapier.exercice());
		}

		return ville;
	}

	private boolean parametreLocalisationPersonnalisee(EOEditingContext ec, EOExercice exercice) {
		String param = FinderParametres.find(ec, EOParametres.PARAM_ADRESSE_SERVICE_FACTURE, exercice);
		if (param != null && param.equals("OUI"))
			return true;

		return false;
	}

	private EODevise getDeviseEnCours(EOEditingContext ec, EOExercice exercice) {
		String devCode = FinderParametreJefyAdmin.find(ec, EOParametreJefyAdmin.PARAM_DEVISE_DEV_CODE, exercice);
		if (devCode != null) {
			return FinderDevise.find(ec, devCode);
		}
		return null;
	}

	/**
	 * Ajoute les infos de l'etablissement dans le flux xml.
	 *
	 * @param tmCktlXMLWriter
	 * @throws IOException
	 */
	private void addEtablissmentInXml(Application app, EOEditingContext ec, EOExercice exercice, EOFournisUlr fournisPrest, CktlXMLWriter tmCktlXMLWriter,
			boolean anglais) throws IOException {

		// recuperation SIRET
		String siret = fournisseurService.findSiret(ec, fournisPrest);

		// recuperation Adresse fournisseur
		EOAdresse fournisPrestAdresse = fournisseurService.currentAdresseFacturation(ec, fournisPrest);

		tmCktlXMLWriter.startElement("etablissement");
		tmCktlXMLWriter.writeElement("nom", StringTool.replace(StringTool.ifNull(FinderParametreJefyAdmin.find(ec, EOParametreJefyAdmin.PARAM_UNIVERSITE, exercice), ""), "&",
				CktlXMLWriter.SpecChars.amp));

		if (parametreLocalisationPersonnalisee(ec, exercice) && fournisPrest != null) {
			String adresse = StringTool.replace(StringTool.ifNull(FinderParametreJefyAdmin.find(ec, EOParametreJefyAdmin.PARAM_UNIVERSITE, exercice), ""), "&",
					CktlXMLWriter.SpecChars.amp) + "\n";
			if (fournisPrestAdresse.adrAdresse1() != null)
				adresse += StringTool.replace(StringTool.ifNull(fournisPrestAdresse.adrAdresse1(), ""), "&", CktlXMLWriter.SpecChars.amp) + "\n";
			if (fournisPrestAdresse.adrAdresse2() != null)
				adresse += StringTool.replace(StringTool.ifNull(fournisPrestAdresse.adrAdresse2(), ""), "&", CktlXMLWriter.SpecChars.amp) + "\n";
			adresse += StringTool.replace(StringTool.ifNull(fournisPrestAdresse.codePostal() + " " + fournisPrestAdresse.ville(), ""), "&", CktlXMLWriter.SpecChars.amp);

			tmCktlXMLWriter.writeElement("adresseuniv", StringTool.replace(StringTool.replace(StringTool.ifNull(adresse, ""), "&", CktlXMLWriter.SpecChars.amp), "\n", CktlXMLWriter.SpecChars.br));

		} else
			tmCktlXMLWriter.writeElement("adresseuniv", StringTool.replace(StringTool.replace(StringTool.ifNull(FinderParametreJefyAdmin.find(ec,
					EOParametreJefyAdmin.PARAM_ADRESSE_UNIVERSITE, exercice), ""), "&", CktlXMLWriter.SpecChars.amp), "\n", CktlXMLWriter.SpecChars.br));

		tmCktlXMLWriter.writeElement("numerosiret", siret != null ? siret : StringTool.ifNull(FinderParametreJefyAdmin.find(ec, "NUMERO_SIRET",
				exercice), ""));
		tmCktlXMLWriter.writeElement("tvaintracom", StringTool.ifNull(FinderParametreMaracuja.find(ec, EOParametreMaracuja.PARAM_TVA_INTRACOM, exercice), ""));
		tmCktlXMLWriter.writeElement("numeroape", StringTool.ifNull(app.getParam("GRHUM_DEFAULT_APE"), ""));
		tmCktlXMLWriter.writeElement("numerodetof", StringTool.ifNull(app.getParam("GRHUM_DEFAULT_DETOF"), ""));
		String paramReglementCheque = "REGLEMENT_CHEQUE", paramReglementVirement = "REGLEMENT_VIREMENT";
		if (anglais) {
			paramReglementCheque = "REGLEMENT_CHEQUE_ANGLAIS";
			paramReglementVirement = "REGLEMENT_VIREMENT_ANGLAIS";
		}
		tmCktlXMLWriter.writeElement("msgreglementcheque", StringTool.replace(StringTool.replace(StringTool.ifNull(FinderParametreMaracuja.find(
				ec, paramReglementCheque, exercice), ""), "&", CktlXMLWriter.SpecChars.amp), "\n", CktlXMLWriter.SpecChars.br));
		tmCktlXMLWriter.writeElement("msgreglementvirement", StringTool.replace(StringTool.replace(StringTool.ifNull(FinderParametreMaracuja.find(
				ec, paramReglementVirement, exercice), ""), "&", CktlXMLWriter.SpecChars.amp), "\n", CktlXMLWriter.SpecChars.br));
		tmCktlXMLWriter.endElement();
	}

	private void addTotauxInXmlForPrestation(CktlXMLWriter myCktlXMLWriter, EOPrestation prestation) throws Exception {
		myCktlXMLWriter.startElement("totaux");
		myCktlXMLWriter.writeElement("montanttotalht", prestation.prestTotalHt().toString());
		myCktlXMLWriter.writeElement("montanttotaltva", prestation.prestTotalTva().toString());
		myCktlXMLWriter.writeElement("montanttotalttc", prestation.prestTotalTtc().toString());
		myCktlXMLWriter.endElement();
	}

	private void addTotauxInXmlForFacturePapier(CktlXMLWriter myCktlXMLWriter, EOFacturePapier facturePapier) throws Exception {
		myCktlXMLWriter.startElement("totaux");
		myCktlXMLWriter.writeElement("montanttotalht", facturePapier.fapTotalHt().toString());
		myCktlXMLWriter.writeElement("montanttotaltva", facturePapier.fapTotalTva().toString());
		myCktlXMLWriter.writeElement("montanttotalttc", facturePapier.fapTotalTtc().toString());
		myCktlXMLWriter.endElement();
	}

	/**
	 * Add fournisseur data into XML stream.
	 *
	 * @param tmCktlXMLWriter xmlWriter.
	 * @param ec editing context.
	 * @param fournisUlr fournisseur.
	 * @throws IOException if an IO exceptions occurs.
	 */
	private void addFournisseurInXml(
			CktlXMLWriter tmCktlXMLWriter, EOEditingContext ec, EOFournisUlr fournisUlr) throws IOException {
		tmCktlXMLWriter.startElement("fournisseur");
		if (fournisUlr != null) {
			EOAdresse adresseFactFour = fournisseurService.currentAdresseFacturation(ec, fournisUlr);

			tmCktlXMLWriter.writeElement(
					"nom",
					StringCtrl.replace(StringTool.ifNull(fournisUlr.personne_persNomPrenom(), ""),
							"&", CktlXMLWriter.SpecChars.amp));
			tmCktlXMLWriter.startElement("adresse");
			addAdresseInXml(tmCktlXMLWriter, ec, adresseFactFour);
			tmCktlXMLWriter.endElement();
			tmCktlXMLWriter.writeElement("tel", StringTool.ifNull(getTelForFournis(fournisUlr.personne()), ""));
			tmCktlXMLWriter.writeElement("fax", StringTool.ifNull(getFaxForFournis(fournisUlr.personne()), ""));
		}
		tmCktlXMLWriter.endElement();
	}

	/**
	 * Generate XML stream from Client data.
	 *
	 * @param tmCktlXMLWriter xml writer.
	 * @param ec editing context.
	 * @param fournisUlr fournisseur.
	 * @param fournisUlrAdresse fournisseur adresse. (can be null)
	 * @param personne personne.
	 * @param individuUlr individu ulr.
	 * @throws IOException if an IO exception occurs.
	 */
	private void addClientInXml(CktlXMLWriter tmCktlXMLWriter, EOEditingContext ec, EOFournisUlr fournisUlr,
			EOAdresse fournisUlrAdresse, EOPersonne personne, EOIndividuUlr individuUlr) throws IOException {
		tmCktlXMLWriter.startElement("client");
		if (personne != null) {
			String client = personne.persLibelle();
			if (personne.persLc() != null) {
				client = client + " " + personne.persLc();
			}
			tmCktlXMLWriter.writeElement("nommoral", StringCtrl.replace(StringTool.ifNull(client, ""), "&", CktlXMLWriter.SpecChars.amp));
		}
		if (individuUlr != null && individuUlr.personne() != null) {
			String client = individuUlr.personne().persLibelle();
			if (individuUlr.personne().persLc() != null) {
				client = client + " " + individuUlr.personne().persLc();
			}
			tmCktlXMLWriter.writeElement("nomindividu", StringCtrl.replace(StringTool.ifNull(client, ""), "&", CktlXMLWriter.SpecChars.amp));
		}
		tmCktlXMLWriter.startElement("adresse");
		if (fournisUlr != null) {
			// use specific adresse if available ; use default facturation fournisseur adresse otherwise.
			EOAdresse currentAdresse = fournisUlrAdresse != null
					? fournisUlrAdresse
					: fournisseurService.currentAdresseFacturation(ec, fournisUlr);
			addAdresseInXml(tmCktlXMLWriter, ec, currentAdresse);
		} else if (personne != null) {
			addAdresseInXml(tmCktlXMLWriter, ec, getEOAdresse(personne));
		}
		tmCktlXMLWriter.endElement();

		// TODO
		if (fournisUlr != null) {
			tmCktlXMLWriter.writeElement("tel", StringTool.ifNull(getTelForFournis(fournisUlr.personne()), ""));
			tmCktlXMLWriter.writeElement("fax", StringTool.ifNull(getFaxForFournis(fournisUlr.personne()), ""));
		} else if (personne != null) {
			tmCktlXMLWriter.writeElement("tel", StringTool.ifNull(getTelForFournis(personne), ""));
			tmCktlXMLWriter.writeElement("fax", StringTool.ifNull(getFaxForFournis(personne), ""));
		}
		tmCktlXMLWriter.endElement();
	}

	private EOAdresse getEOAdresse(EOPersonne personne) {
		if (personne == null) {
			return null;
		}
		NSArray listeAdresses = personne.repartPersonneAdresses();
		// - Recherche de l'adresse principale
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(
				EORepartPersonneAdresse.RPA_PRINCIPAL_KEY + " = %@", new NSArray("O"));
		NSArray adresse = EOQualifier.filteredArrayWithQualifier(listeAdresses, qualifier);
		if (adresse.count() > 0) {
			return ((EORepartPersonneAdresse) adresse.objectAtIndex(0)).adresse();
		}
		// - Recherche de l'adresse de fact
		qualifier = EOQualifier.qualifierWithQualifierFormat(
				EORepartPersonneAdresse.TADR_CODE_KEY + " = %@", new NSArray("FACT"));
		adresse = EOQualifier.filteredArrayWithQualifier(listeAdresses, qualifier);
		if (adresse.count() > 0) {
			return ((EORepartPersonneAdresse) adresse.objectAtIndex(0)).adresse();
		}
		// - Recherche de l'adresse pro
		qualifier = EOQualifier.qualifierWithQualifierFormat(
				EORepartPersonneAdresse.TADR_CODE_KEY + " = %@", new NSArray("PRO"));
		adresse = EOQualifier.filteredArrayWithQualifier(listeAdresses, qualifier);
		if (adresse.count() > 0) {
			return ((EORepartPersonneAdresse) adresse.objectAtIndex(0)).adresse();
		}
		// Recherche d'autres adresses que PRO ou FACT
		if (listeAdresses != null && listeAdresses.count() > 0) {
			return ((EORepartPersonneAdresse) listeAdresses.objectAtIndex(0)).adresse();
		}
		return null;
	}

	private String getTelForFournis(EOPersonne personne) {
		if (personne == null) {
			return null;
		}
		NSArray listTel = personne.personneTelephones();
		NSMutableArray args = new NSMutableArray();
		args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPersonneTelephone.TYPE_NO_TEL_KEY + "." + EOTypeNoTel.C_TYPE_NO_TEL_KEY
				+ " = %@", new NSArray("TEL")));
		args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPersonneTelephone.TYPE_TEL_RELATIONSHIP_KEY + "." + EOTypeTel.C_TYPE_TEL_KEY
				+ " = %@", new NSArray("PRF")));
		listTel = EOQualifier.filteredArrayWithQualifier(listTel, new EOAndQualifier(args));
		if (listTel == null || listTel.count() == 0) {
			return null;
		}
		return (String) ((EOPersonneTelephone) listTel.lastObject()).noTelephone();
	}

	private String getFaxForFournis(EOPersonne personne) {
		if (personne == null) {
			return null;
		}
		NSArray listFax = personne.personneTelephones();
		NSMutableArray args = new NSMutableArray();
		args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPersonneTelephone.TYPE_NO_TEL_KEY + "." + EOTypeNoTel.C_TYPE_NO_TEL_KEY
				+ " = %@", new NSArray("FAX")));
		args.addObject(EOQualifier.qualifierWithQualifierFormat(EOPersonneTelephone.TYPE_TEL_RELATIONSHIP_KEY + "." + EOTypeTel.C_TYPE_TEL_KEY
				+ " = %@", new NSArray("PRF")));
		listFax = EOQualifier.filteredArrayWithQualifier(listFax, new EOAndQualifier(args));
		if (listFax == null || listFax.count() == 0) {
			return null;
		}
		return (String) ((EOPersonneTelephone) listFax.lastObject()).noTelephone();
	}

	/**
	 * Generate address as an XML stream.
	 *
	 * @param tmCktlXMLWriter xml writer.
	 * @param ec editing context.
	 * @param adr address.
	 * @throws IOException if an IO exception occurs.
	 */
	private void addAdresseInXml(CktlXMLWriter tmCktlXMLWriter, EOEditingContext ec, EOAdresse adr) throws IOException {
		if (adr != null) {
			tmCktlXMLWriter.writeElement("adrbp", StringTool.replace(StringTool.ifNull(adr.adrBp(), ""), "&", CktlXMLWriter.SpecChars.amp));
			tmCktlXMLWriter.writeElement("adradresse1", StringTool.replace(StringTool.ifNull(adr.adrAdresse1(), ""), "&",
					CktlXMLWriter.SpecChars.amp));
			tmCktlXMLWriter.writeElement("adradresse2", StringTool.replace(StringTool.ifNull(adr.adrAdresse2(), ""), "&",
					CktlXMLWriter.SpecChars.amp));
			tmCktlXMLWriter.writeElement("adrcp", StringTool.ifNull(StringTool.ifNull(adr.cpEtranger(), adr.codePostal()), ""));
			tmCktlXMLWriter.writeElement("adrville", StringTool.replace(StringTool.ifNull(adr.ville(), ""), "&", CktlXMLWriter.SpecChars.amp));
			if (adr.pays() != null && !adr.pays().equals(FinderPays.findDefault(ec))) {
				tmCktlXMLWriter.writeElement("lcpays", StringTool.replace(StringTool.ifNull(adr.pays().lcPays(), ""), "&",
						CktlXMLWriter.SpecChars.amp));
			}
			tmCktlXMLWriter.writeElement("adrcpetranger", StringTool.ifNull(adr.cpEtranger(), ""));
		}
	}

	private void addPrestationLigneInXml(CktlXMLWriter tmCktlXMLWriter, EOPrestationLigne ligne, boolean isFormationContinue) throws IOException {
		if (ligne != null) {
			System.out.println("ReportFactorySix.addPrestationLigneInXml()");
			tmCktlXMLWriter.startElement(ligne.typeArticle().tyarLibelle().toLowerCase());
			tmCktlXMLWriter.writeElement("prixlignettc", StringTool.ifNull(ligne.prligTotalTtc(), ""));
			tmCktlXMLWriter.writeElement("cartptype", StringTool.ifNull(ligne.typeArticle().tyarLibelle(), ""));
			tmCktlXMLWriter.writeElement("dlignbresteafacturer", StringTool.ifNull(ligne.prligQuantiteReste(), ""));
			tmCktlXMLWriter.writeElement("dlignbarticles", StringTool.ifNull(ligne.prligQuantite(), ""));
			tmCktlXMLWriter.writeElement("dligartht", StringTool.ifNull(ligne.prligArtHt(), ""));
			tmCktlXMLWriter.writeElement("dligdescription",
			    StringTool.replace(
    			    StringTool.replace(
    			        StringTool.ifNull(ligne.prligDescription(), ""), 
    			        "&", CktlXMLWriter.SpecChars.amp),
			        "\n", CktlXMLWriter.SpecChars.br));

			tmCktlXMLWriter.writeElement("dligreference", StringTool.replace(StringTool.ifNull(ligne.prligReference(), ""), "&",
					CktlXMLWriter.SpecChars.amp));
			tmCktlXMLWriter.writeElement("prixligneht", StringTool.ifNull(ligne.prligTotalHt(), ""));
			tmCktlXMLWriter.startElement("taux");
			if (ligne.tva() != null) {
				tmCktlXMLWriter.writeElement("taucode", StringTool.ifNull(ligne.tva().tvaTaux(), ""));
				tmCktlXMLWriter.writeElement("tautaux", StringTool.ifNull(ligne.tva().tvaTaux(), ""));
			}
			tmCktlXMLWriter.endElement();// taux
			tmCktlXMLWriter.writeElement("isformationcontinue", isFormationContinue ? "true" : "false");
			if (ligne.catalogueArticle() != null && isFormationContinue) {
				EOArticlePrestation artp = ligne.catalogueArticle().article().articlePrestation();
				tmCktlXMLWriter.writeElement("responsable", StringTool.ifNull(artp.artpCfcResponsable(), ""));
				if (artp.artpCfcDateDebut() != null) {
					tmCktlXMLWriter.writeElement("datedebut", new NSTimestampFormatter("%Y-%m-%d").format(artp.artpCfcDateDebut()));
				}
				if (artp.artpCfcDateFin() != null) {
					tmCktlXMLWriter.writeElement("datefin", new NSTimestampFormatter("%Y-%m-%d").format(artp.artpCfcDateFin()));
				}
				tmCktlXMLWriter.writeElement("duree", StringTool.ifNull(artp.artpCfcDuree(), ""));
				tmCktlXMLWriter.writeElement("nodeclaration", StringTool.ifNull(artp.artpCfcNoDeclaration(), ""));
			}
			tmCktlXMLWriter.endElement();// typeLigne
		}
	}

	private void addFacturePapierLigneInXml(CktlXMLWriter tmCktlXMLWriter, EOFacturePapierLigne ligne) throws IOException {
		if (ligne != null) {
			tmCktlXMLWriter.startElement(ligne.typeArticle().tyarLibelle().toLowerCase());
			tmCktlXMLWriter.writeElement("fapnumero", ligne.facturePapier().fapNumero().toString());
			tmCktlXMLWriter.writeElement("cartptype", StringTool.ifNull(ligne.typeArticle().tyarLibelle(), ""));
			tmCktlXMLWriter.writeElement("fligquantite", StringTool.ifNull(ligne.fligQuantite(), ""));
			tmCktlXMLWriter.writeElement("fligartht", StringTool.ifNull(ligne.fligArtHt(), ""));
			tmCktlXMLWriter.writeElement("fligdescription",
			    StringCtrl.replace(
			        StringCtrl.replace(
			            StringTool.ifNull(ligne.fligDescription(), ""), 
			            "&", CktlXMLWriter.SpecChars.amp),
		            "\n", CktlXMLWriter.SpecChars.br));
			tmCktlXMLWriter.writeElement("fligreference", StringCtrl.replace(StringTool.ifNull(ligne.fligReference(), ""), "&",
					CktlXMLWriter.SpecChars.amp));
			tmCktlXMLWriter.writeElement("fligtotalht", StringTool.ifNull(ligne.fligTotalHt(), ""));
			tmCktlXMLWriter.writeElement("fligtotalttc", StringTool.ifNull(ligne.fligTotalTtc(), ""));
			tmCktlXMLWriter.writeElement("fligtotaltva", StringTool.ifNull(ligne.fligTotalTva(), ""));
			tmCktlXMLWriter.startElement("taux");
			if (ligne.tva() != null) {
				tmCktlXMLWriter.writeElement("taucode", StringTool.ifNull(ligne.tva().tvaTaux(), ""));
				tmCktlXMLWriter.writeElement("tautaux", StringTool.ifNull(ligne.tva().tvaTaux(), ""));
			}
			tmCktlXMLWriter.endElement();// taux

			if (ligne.prestationLigne() != null && ligne.prestationLigne().catalogueArticle() != null) {
				EOArticlePrestation artp = ligne.prestationLigne().catalogueArticle().article().articlePrestation();
				tmCktlXMLWriter.writeElement("responsable", StringTool.ifNull(artp.artpCfcResponsable(), ""));
				if (artp.artpCfcDateDebut() != null) {
					tmCktlXMLWriter.writeElement("datedebut", new NSTimestampFormatter("%Y-%m-%d").format(artp.artpCfcDateDebut()));
				}
				if (artp.artpCfcDateFin() != null) {
					tmCktlXMLWriter.writeElement("datefin", new NSTimestampFormatter("%Y-%m-%d").format(artp.artpCfcDateFin()));
				}
				tmCktlXMLWriter.writeElement("duree", StringTool.ifNull(artp.artpCfcDuree(), ""));
				tmCktlXMLWriter.writeElement("nodeclaration", StringTool.ifNull(artp.artpCfcNoDeclaration(), ""));
			}

			tmCktlXMLWriter.endElement();// typeLigne
		}
	}

	private int genererPDF(InputStream xmlstream, int streamSize, String maquetteID) throws Exception {
		if (!printer.checkTemplate(maquetteID)) {
			throw new Exception("Le modele " + maquetteID + " n'est pas present sur le service d'impression");
		}
		printer.setJobAliveTimeout(300);
		int jobId = printer.printFileDiffered(maquetteID, xmlstream, streamSize);
		// On continue s'il n'y a pas d'erreurs.
		if (printer.hasSuccess()) {
			// OK. On attend la fin de creation du document...
			// On pose les verrous pour pouvoir executer "wait"
			synchronized (this) {
				do {
					// On attend 1 seconde avant de redemander
					try {
						wait(1000);
					} catch (Exception e) {
					}
					// On verifie l'etat d'impression et on s'arrete si c'est fini
					printer.getPrintStatus(jobId);
				} while (printer.isPrintInProgress());
			}
		}
		if (printer.hasError())
			throw new Exception("Erreur lors de l'impression PDF : " + printer.getMessage());
		return jobId;
	}

	private InputStream getPdfStream(int jobId) throws Exception {
		InputStream loc = printer.getPrintResult(jobId);
		if (printer.getContentSize() == 0) {
			throw new Exception("La longueur du flux PDF genere est nulle.");
		}
		return loc;
	}

	private class MyPRLIGComp extends NSComparator {
		public int compare(Object o1, Object o2) {
			NSTimestamp d1 = ((NSTimestamp) ((EOPrestationLigne) o1).prligDate());
			NSTimestamp d2 = ((NSTimestamp) ((EOPrestationLigne) o2).prligDate());
			if (d1 == null || d2 == null) {
				return NSComparator.OrderedAscending;
			}
			return d1.compare(d2);
		}
	}

	private class MyFLIGComp extends NSComparator {
		public int compare(Object o1, Object o2) {
			NSTimestamp d1 = ((NSTimestamp) ((EOFacturePapierLigne) o1).fligDate());
			NSTimestamp d2 = ((NSTimestamp) ((EOFacturePapierLigne) o2).fligDate());
			if (d1 == null || d2 == null) {
				return NSComparator.OrderedAscending;
			}
			return d1.compare(d2);
		}
	}

}