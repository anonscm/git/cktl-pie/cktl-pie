/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.server.tools;

import java.math.BigDecimal;

import org.cocktail.kava.server.metier.EOAdresse;
import org.cocktail.kava.server.metier.EOFacturePapier;
import org.cocktail.kava.server.metier.EOParametres;
import org.cocktail.kava.server.metier.EOPrestation;
import org.cocktail.kava.server.metier.EOPrestationLigne;
import org.cocktail.kava.server.metier.EORepartPersonneAdresse;

import app.server.Application;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestampFormatter;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

public class MailPrestation {
	static Application	app	= (Application) WOApplication.application();

	public static String mailDevisFr(EOPrestation prestation) {
		StringBuffer body = new StringBuffer("<HTML>");
		body.append(enteteFournisseur(prestation, false));
		body.append("<P>Bonjour, ");
		if (prestation.personne() != null) {
			body.append(prestation.personne().persNomPrenom()).append("</p>\n");
		}
		else {
			body.append(prestation.fournisUlr().personne().persNomPrenom()).append("</p>\n");
		}
		body.append("<P>Nous vous remercions de faire appel &#224 notre service.</P>").append("\n");
		NSTimestampFormatter formatter = new NSTimestampFormatter("%d/%m/%Y");
		body.append("<P>Vous trouverez ci-joint un devis correspondant &#224 votre demande du ")
				.append(formatter.format(prestation.prestDate())).append("</P>").append("\n");
		body.append("<P>N&#176 du devis : <B>").append(prestation.prestNumero().toString()).append("</B></p>\n");
		body.append(detailCommande(prestation, false));
		body.append("<TABLE>").append("\n");
		body.append("<TR><TD>D&#232s que vous aurez valid&#233 ce devis, nous &#233tablirons la facture correspondante que vous").append(
				"</td></tr>");
		body.append("<TR><TD>pourrez r&#233gler par carte bancaire (paiement s&#233curis&#233 PayBox)").append("</td></tr>");
		body.append("</TABLE>").append("\n");

		String url = app.config().stringForKey(EOParametres.PARAM_CONFIG_URL_APP_WEB) + "wa/validationDevisClient?noDevis=" + prestation.prestNumero().toString();
		body.append("<p>Pour valider le devis, cliquez ici : <a href=\"").append(url).append("\">").append(url).append("</a></p>");
		// body.append("<P>Votre commande vous sera alors livr&#233e dans les meilleurs d&#233lais.</p>");
		body.append("<P>Merci.</p>");
        body.append("<p></p>");
        url= app.config().stringForKey(EOParametres.PARAM_CONFIG_URL_APP_WEB)+"wa/mdpPerdu";
        body.append("<p><i>Si vous avez perdu votre mot de passe, <a href=\"").append(url).append("\">cliquez ici</a> pour en obtenir un nouveau.</i></p>");
		body.append("</HTML>");
		// System.out.println(body.toString());
		return body.toString();
	}

	public static String mailDemandeValidationDevisFr(EOPrestation prestation) {
		StringBuffer body = new StringBuffer("<HTML>");
		body.append(enteteFournisseur(prestation, false));
		body.append("<P>Bonjour, ");
		// if (prestation.personne() != null) {
		// body.append(prestation.personne().persNomPrenom()).append("</p>\n");
		// } else {
		// body.append(prestation.fournisUlr().personne().persNomPrenom()).append("</p>\n");
		// }
		body.append("<P>Vous avez recu une demande de validation de devis de <a href=\"mailto:");
		if (prestation.personne() != null) {
			body.append(prestation.personne().repartPersonneMail().email()).append("\">").append(prestation.personne().persNomPrenom()).append(
					"</a></p>\n");
		}
		NSTimestampFormatter formatter = new NSTimestampFormatter("%d/%m/%Y");
		body.append("<P>Vous trouverez ci-dessous le devis correspondant &#224 la demande du ").append(formatter.format(prestation.prestDate()))
				.append("</P>").append("\n");
		body.append("<P>N&#176 du devis : <B>").append(prestation.prestNumero().toString()).append("</B></p>\n");
		body.append(detailCommande(prestation, false));
		String url = app.config().stringForKey(EOParametres.PARAM_CONFIG_URL_APP_WEB) + "/wa/validationDevisFournisseur?noDevis=" + prestation.prestNumero().toString();
		body.append("<P>Pour valider le devis et envoy&#233 un email au client, cliquez ici : <a href=\"").append(url).append("\">").append(url)
				.append("</a></p>");
		body.append("<P>Merci.</p>");
        body.append("<p></p>");
        url= app.config().stringForKey(EOParametres.PARAM_CONFIG_URL_APP_WEB)+"wa/mdpPerdu";
        body.append("<p><i>Si vous avez perdu votre mot de passe, <a href=\"").append(url).append("\">cliquez ici</a> pour en obtenir un nouveau.</i></p>");
		body.append("</HTML>");
		return body.toString();
	}

	private static String enteteFournisseur(EOPrestation prestation, boolean useEn) {
		StringBuffer body = new StringBuffer();
		if (logo(prestation) != null && !"".equals(logo(prestation)))
			body.append("<img src='").append(logo(prestation)).append("'/>").append("\n");
		body.append("<TABLE>").append("\n");
		// body.append("<TR><TH align=left>Service Fourniture de Documents</TH></TR>").append("\n");
		body.append("<TR><TH align=left>").append(prestation.fournisUlrPrest().personne_persNomPrenom()).append("</TH></TR>").append("\n");
		// body.append("<TR><TH align=left>12 rue de l'Ecole de M&#223decine</TH></TR>").append("\n");
		// body.append("<TR><TH align=left>75270 PARIS CEDEX 06</TH></TR>").append("\n");
		NSArray list = (NSArray) prestation.fournisUlrPrest().personne().repartPersonneAdresses();
		EOQualifier.filteredArrayWithQualifier(list, EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.RPA_PRINCIPAL_KEY
				+ " = %@", new NSArray("O")));
		int max = (list.count() > 1) ? 1 : list.count();
		for (int i = 0; i < max; i++) {
			EOAdresse adr = (EOAdresse) ((EOGenericRecord) list.objectAtIndex(i)).valueForKey("adresse");
			if (adr.adrAdresse1() != null && !"".equals(adr.adrAdresse1()))
				body.append("<TR><TH align=left>").append(adr.adrAdresse1()).append("</TH></TR>").append("\n");
			if (adr.adrAdresse2() != null && !"".equals(adr.adrAdresse2()))
				body.append("<TR><TH align=left>").append(adr.adrAdresse2()).append("</TH></TR>").append("\n");

			body.append("<TR><TH align=left>").append(adr.codePostal()).append(" ").append(adr.ville());
			if (adr.adrBp() != null)
				body.append(" ").append(adr.adrBp());
			body.append("</TH></TR>").append("\n");
			if (adr.adrUrlPere() != null && !"".equals(adr.adrUrlPere()))
				body.append("<TR><TH align=left><A HREF=\"").append(adr.adrUrlPere()).append("\">").append(adr.adrUrlPere()).append(
						"</A></TH></TR>").append("\n");
		}
		body.append("</TABLE>").append("\n");
		String contact = "Vo(s) contact(s)";
		if (useEn)
			contact = "Your contact(s)";
		body.append("<P>").append(contact).append(" : <A HREF='mailto:'").append(responsable(prestation)).append("'>").append(
				responsable(prestation)).append("</A></P>");
		return body.toString();
	}

	private static String detailCommande(EOPrestation prestation, boolean useEn) {
		StringBuffer body = new StringBuffer();
		body.append("<TABLE bgcolor=99ffff border=1>").append("\n");
		if (useEn)
			body
					.append(
							"<TR ><TH align=left>Article</TH><TH>Quantity</TH><TH>Unitary price</TH><TH>Total HT</TH><TH>VAT</TH><TH>Total TTC in Euros</TH></TR>")
					.append("\n");
		else
			body
					.append(
							"<TR ><TH align=left>Article</TH><TH>Quantit&#233</TH><TH>Prix unitaire</TH><TH>Total HT</TH><TH>TVA</TH><TH>Total TTC en Euros</TH></TR>")
					.append("\n");
		NSArray lignes = prestation.prestationLignes();
		NSMutableArray sort = new NSMutableArray();
		sort.addObject(EOSortOrdering.sortOrderingWithKey(EOPrestationLigne.PRLIG_REFERENCE_KEY, EOSortOrdering.CompareAscending));
		lignes = EOSortOrdering.sortedArrayUsingKeyOrderArray(lignes, sort);

		for (int i = 0; i < lignes.count(); i++) {
			body.append("<TR><TD align=left width=400>").append(
					((EOPrestationLigne) lignes.objectAtIndex(i)).prligDescription()).append("</td>");
			body.append("<TD align=right>").append(
					((EOPrestationLigne) lignes.objectAtIndex(i)).prligQuantite().setScale(0, BigDecimal.ROUND_HALF_UP)).append("</td>");
			body.append("<TD align=right>").append(
					((EOPrestationLigne) lignes.objectAtIndex(i)).prligArtHt().setScale(2, BigDecimal.ROUND_HALF_UP)).append("</td>");
			BigDecimal totalArtHt = ((EOPrestationLigne) lignes.objectAtIndex(i)).prligTotalHt().setScale(2, BigDecimal.ROUND_HALF_UP);
			body.append("<TD align=right>").append(totalArtHt).append("</td>");
			body.append("<TD align=right width=40>").append(
					((EOPrestationLigne) lignes.objectAtIndex(i)).prligTotalTva().setScale(2, BigDecimal.ROUND_HALF_UP)).append("</td>");
			body.append("<TD align=right>").append(
					((EOPrestationLigne) lignes.objectAtIndex(i)).prligTotalTtc().setScale(2, BigDecimal.ROUND_HALF_UP)).append("</td>");
		}
		body.append("<TR><TD align=right>TOTAL").append("</td>");
		body.append("<TD align=right>").append("</td>");
		body.append("<TD align=right>").append("</td>");
		body.append("<TD align=right>").append(prestation.prestTotalHt()).append("</td>");
		body.append("<TD align=right>").append(prestation.prestTotalTva()).append("</td>");
		body.append("<TD align=right>").append(prestation.prestTotalTtc()).append("</td>");
		body.append("</tr></TABLE>").append("\n");
		return body.toString();
	}

	public static String mailFactureFr(EOFacturePapier facture) {
		EOPrestation prestation = facture.prestation();
		StringBuffer body = new StringBuffer("<HTML>");
		body.append(enteteFournisseur(prestation, false));
		body.append("<P>Bonjour, ");
		if (prestation.personne() != null) {
			body.append(prestation.personne().persNomPrenom()).append("</p>\n");
		}
		else {
			body.append(prestation.fournisUlr().personne().persNomPrenom()).append("</p>\n");
		}
		body.append("<P>Nous vous remercions pour votre commande.</P>").append("\n");
		body.append("<P>Vous trouverez ci-joint la facture n&#176; ").append(facture.fapNumero()).append("</P>").append("\n");
		if (facture.fapDateReglement() == null) {
			body.append("<P>Rappel de votre devis N&#176;<B>").append(prestation.prestNumero().toString()).append("</B></p>");
			body.append(detailCommande(prestation, false));
			body.append("<TABLE>").append("\n");
			body.append("<TR><TD>Pour effectuer un r&#232glement par carte bancaire (paiement s&#233curis&#233 Paybox)").append("</td></tr>");
			body.append("<TR><TD>Aller &#224; l'adresse suivante : ").append("</td></tr>");
			String url = app.config().stringForKey(EOParametres.PARAM_CONFIG_URL_APP_WEB) + "wa/paiementFacture?noDevis=" + prestation.prestNumero().toString();
			body.append("<TR><TD><A HREF=\"").append(url).append("\">").append(url).append("</A>").append("</td></tr>");
			body.append("</TABLE>").append("\n");
			body
					.append("<P>D&#232s r&#233ception de l'avis de paiement, les documents vous seront envoy&#233s dans les meilleurs d&#233lais.</p>");
		}
		else
			body.append("<P>Vos documents vous seront envoy&#233s dans les meilleurs d&#233lais.</p>");
		body.append("<P>Merci</p>");
		body.append("<P>A bient&#244t.</p>");
        body.append("<p></p>");
        String url= app.config().stringForKey(EOParametres.PARAM_CONFIG_URL_APP_WEB)+"wa/mdpPerdu";
        body.append("<p><i>Si vous avez perdu votre mot de passe, <a href=\"").append(url).append("\">cliquez ici</a> pour en obtenir un nouveau.</i></p>");
		body.append("</HTML>");
		return body.toString();
	}

	public static String mailDevisEn(EOPrestation prestation) {
		StringBuffer body = new StringBuffer("<HTML>");
		body.append(enteteFournisseur(prestation, true));
		body.append("<P>Dear ");
		if (prestation.personne() != null) {
			body.append(prestation.personne().persNomPrenom()).append("\n");
		}
		else {
			body.append(prestation.fournisUlr().personne().persNomPrenom()).append("\n");
		}
		body.append("<P>We thank you for trusting our service.</P>").append("\n");
		NSTimestampFormatter formatter = new NSTimestampFormatter("%b %d, /%Y");
		body.append("<P>You'll find the estimate of your order date ").append(formatter.format(prestation.prestDate())).append("</P>").append(
				"\n");
		body.append("<P>N&#176; of your order : <B>").append(prestation.prestNumero().toString()).append("</B>");
		body.append(detailCommande(prestation, true));
		body.append("<TABLE>").append("\n");
		body.append("<TR><TD>As soon as you accept the estimate, we'll send you an invoice,");
		body.append("<TR><TD>and you'll pay by credit card (secured payment by PayBox)");
		body.append("</TABLE>").append("\n");

		String url = app.config().stringForKey(EOParametres.PARAM_CONFIG_URL_APP_WEB) + "wa/validationDevisClient?noDevis=" + prestation.prestNumero().toString();
		body.append("<p>To accept the estimate, click : <a href=\"").append(url).append("\">").append(url).append("</a></p>");
		body.append("<P>Many thanks.");
        body.append("<p></p>");
        url= app.config().stringForKey(EOParametres.PARAM_CONFIG_URL_APP_WEB)+"wa/mdpPerdu";
        body.append("<p><i> If you lost your password, <a href=\"").append(url).append("\">click here</a> to obtain new.</i></p>");
		body.append("</HTML>");
		return body.toString();
	}

	public static String mailFactureEn(EOFacturePapier facture) {
		EOPrestation prestation = facture.prestation();
		StringBuffer body = new StringBuffer("<HTML>");
		body.append(enteteFournisseur(prestation, true));
		body.append("<P>Your contact : <A HREF='mailto:'").append(responsable(prestation)).append("'>").append(responsable(prestation)).append(
				"</A></P>");
		body.append("<P>Dear ");
		if (prestation.personne() != null) {
			body.append(prestation.personne().persNomPrenom()).append("\n");
		}
		else {
			body.append(prestation.fournisUlr().personne().persNomPrenom()).append("\n");
		}
		body.append("<P>We thank you for your order.</P>").append("\n");
		body.append("<P>You will find enclosed the invoice n&#176; ").append(facture.fapNumero()).append("</P>").append("\n");
		if (facture.fapDateReglement() == null) {
			body.append("<P>N&#176; of estimate : <B>").append(prestation.prestNumero().toString()).append("</B>");
			body.append(detailCommande(prestation, true));
			body.append("<TABLE>").append("\n");
			body.append("<TR><TD>To process the on line payment, please visit");
			String url = app.config().stringForKey(EOParametres.PARAM_CONFIG_URL_APP_WEB) + "wa/paiementFacture?noDevis=" + prestation.prestNumero().toString();
			body.append("<TR><TD><A HREF=\"").append(url).append("\">").append(url).append("</A>").append("</td></tr>");
			body.append("</TABLE>").append("\n");
			body.append("<P>As soon as your payment is processed, we'll send you the documents</p>");
		}
		else
			body.append("<P>We have received your payment, so we'll send you the documents</p>");
		body.append("<P>Best regards.</p>");
        body.append("<p></p>");
        String url= app.config().stringForKey(EOParametres.PARAM_CONFIG_URL_APP_WEB)+"wa/mdpPerdu";
        body.append("<p><i> If you lost your password, <a href=\"").append(url).append("\">click here</a> to obtain new.</i></p>");
		body.append("</HTML>");
		return body.toString();
	}

	public static String responsable(EOPrestation prestation) {
		try {
			NSArray a = null;
			if (prestation.catalogue() != null) {
				a = (NSArray) prestation.catalogue().catalogueResponsables().valueForKeyPath("utilisateur.personne.repartPersonneMail.email");
			}
			if (a == null || a.count() == 0)
				return "";
			return a.componentsJoinedByString(",");
		}
		catch (Exception e) {
			return "";
		}
	}

	public static String logo(EOPrestation prestation) {
		String url = null;
		try {
			if (prestation.catalogue() != null) {
				url = (String) prestation.catalogue().valueForKey("URL_LOGO_CATALOGUE");
			}
		}
		catch (Throwable e) {
			// e.printStackTrace();
			return null;
		}
		return url;
	}

}