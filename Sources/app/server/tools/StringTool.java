/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.server.tools;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;

public class StringTool {

	/**
	 * Formatte une chaine selon une longueur donnee, avec un caractere et un sens donne
	 * 
	 * Exemple : "TOTO",10,"Z","D" ==> return "TOTOZZZZZZ"
	 * 
	 * @param chaine
	 *            Chaine a completer
	 * @param nbCars
	 *            Nombre de caracteres en retour
	 * @param carac
	 *            Caractere de completion
	 * @param sens
	 *            Completion vers la droite (D) ou la gauche (G)
	 * @return
	 */
	public static String stringCompletion(String chaine, int nbCars) {
		return stringCompletion(chaine, nbCars, " ", "D");
	}

	public static String stringCompletion(String chaine, int nbCars, String carac) {
		return stringCompletion(chaine, nbCars, carac, "D");
	}

	public static String stringCompletion(String chaine, int nbCars, String carac, String sens) {
		String retour = chaine;

		if (retour.length() > nbCars)
			retour = retour.substring(0, nbCars);

		for (int i = 0; i < nbCars - chaine.length(); i++) {
			if ("D".equals(sens))
				retour = retour + carac;
			else
				retour = carac + retour;
		}

		return retour;
	}

	/**
	 * Test si le caractere <i>c</i> est une lettre "de base" (a-z, A-Z). Il ne doit pas etre une lettre accentue, une chiffre ou un autre
	 * caractere special.
	 */
	public static boolean isBasicLetter(char c) {
		int numVal = Character.getNumericValue(c);
		return (((Character.getNumericValue('a') <= numVal) && (numVal <= Character.getNumericValue('z'))) || ((Character.getNumericValue('A') <= numVal) && (numVal <= Character
				.getNumericValue('Z'))));
	}

	/**
	 * 
	 */
	public static String chaineSansAccents(String chaine) {
		String retour = chaine.toLowerCase();

		replace(retour, "é", "e");
		replace(retour, "è", "e");
		replace(retour, "ê", "e");

		replace(retour, "à", "a");
		replace(retour, "â", "a");

		replace(retour, "ï", "i");
		replace(retour, "î", "i");

		replace(retour, "ö", "o");
		replace(retour, "ô", "o");

		replace(retour, "ü", "u");
		replace(retour, "ù", "u");

		return retour.toUpperCase();
	}

	/**
	 * 
	 */
	public static boolean chaineVide(String chaine) {
		if (!"".equals(recupererChaine(chaine)))
			return false;

		return true;
	}

	public static String ifNull(Object pstr, String defaultStr) {
		String s = null;
		if (pstr == NSKeyValueCoding.NullValue || pstr == null) {
			s = null;
		}
		else {
			s = pstr.toString();
		}
		if (s == null || s.trim().equals("")) {
			return defaultStr;
		}
		return s;
	}

	/**
	 * Teste si le caractere <i>c</i> est un chiffre entre 0 et 9.
	 */
	public static boolean isBasicDigit(char c) {
		int numVal = Character.getNumericValue(c);
		return ((Character.getNumericValue('0') <= numVal) && (numVal <= Character.getNumericValue('9')));
	}

	/**
	 * Teste si le caractere <i>c</i> est un caractere "acceptable". Il l'est si c'est une lettre de base (<i>isBasicLetter</i>), un chiffre (<i>isBasicDigit</i>)
	 * ou un des caracteres <i>acceptChars</i>.
	 * 
	 * @see #isBasicLetter(char)
	 * @see #isBasicDigit(char)
	 * @see #isAcceptChar(char)
	 */
	public static boolean isAcceptChar(char c, String acceptChars) {
		boolean rep = isBasicLetter(c);
		if (!rep)
			rep = isBasicDigit(c);
		if ((!rep) && (acceptChars != null)) {
			for (int i = 0; i < acceptChars.length(); i++)
				if (c == acceptChars.charAt(i))
					return true;
		}
		return rep;
	}

	/**
	 * Retourne la liste des caracteres acceptes par defaut comme caracteres legales.
	 * 
	 * <p>
	 * Cette implementation renvoie la chaine "._-".
	 * 
	 * @see #isAcceptChar(char)
	 */
	public static String defaultAcceptChars() {
		return "._-";
	}

	/**
	 * Test si la chaine de caracteres est "acceptable". Elle l'est si tous les caracteres de la chaine sont acceptables (<i>isAcceptChar</i>) :
	 * les lettres "de base", les chiffres et les caracteres acceptes par defaut.
	 * 
	 * @see #isAcceptChar(char)
	 */
	public static boolean isAcceptBasicString(String aString) {
		for (int i = 0; i < aString.length(); i++) {
			if (!isAcceptChar(aString.charAt(i)))
				return false;
		}
		return true;
	}

	/**
	 * Teste si le caractere <i>c</i> est un caractere "acceptable". Il l'est si c'est une lettre de base (<i>isBasicLetter</i>), un chiffre (<i>isBasicDigit</i>)
	 * ou un des caracteres supplementaires acceptes par defaut (<i>defaultAcceptChars</i>).
	 * 
	 * @see #isBasicLetter(char)
	 * @see #isBasicDigit(char)
	 * @see #isAcceptChar(char, String)
	 * @see #defaultAcceptChars()
	 */
	public static boolean isAcceptChar(char c) {
		return isAcceptChar(c, defaultAcceptChars());
	}

	/**
	 * Convertie la chaine de caracteres <i>s</i> en sa representation "legale". Tous les caracteres de la chaine doivent etre une lettre
	 * latine, un chiffre ou un des caracteres <i>acceptChars</i>. Chaque caractere "illegal" est remplace par <i>charToReplace</i>.
	 * 
	 * @see #isAcceptChar(char, String)
	 * @see #toBasicString(String)
	 */
	public static String toBasicString(String s, String acceptChars, char charToReplace) {
		StringBuffer newStr;

		if ((s == null) || (s.length() == 0))
			return s;
		newStr = new StringBuffer();
		for (int i = 0; i < s.length(); i++) {
			if (isAcceptChar(s.charAt(i), acceptChars))
				newStr.append(s.charAt(i));
			else
				newStr.append(charToReplace);
		}
		return newStr.toString();
	}

	/**
	 * Convertie la chaine de caracteres <i>s</i> en sa representation "legale". Tous les caracteres de la chaine doivent etre une lettre
	 * latine, un chiffre ou un des caracteres acceptes par defaut. Chaque caractere "illegal" est remplace par <i>charToReplace</i>.
	 * 
	 * <p>
	 * Cette methode peut etre utilisee pour changer le nom des fichiers en leur representation "portable" (pas d'espaces, pas des lettres
	 * accentuees, etc.)
	 * </p>
	 * 
	 * @see #isAcceptChar(char, String)
	 * @see #defaultAcceptChars()
	 * @see #toBasicString(String, String, char)
	 */
	public static String toBasicString(String aString) {
		return toBasicString(aString, defaultAcceptChars(), '_');
	}

	public static String replace(String chaine, String chaine1, String chaine2) {
		return (NSArray.componentsSeparatedByString(chaine, chaine1)).componentsJoinedByString(chaine2);
	}

	public static String formatter2Chiffres(int nombre) {
		if (nombre < 10)
			return "0" + nombre;

		return "" + nombre;
	}

	public static String capitalizedString(String aString) {
		if ("".equals(aString))
			return "";

		String debut = (aString.substring(0, 1)).toUpperCase();
		String fin = (aString.substring(1, aString.length())).toLowerCase();

		return debut.concat(fin);
	}

	public static String recupererChaine(String laChaine) {
		if ((laChaine == null) || (laChaine == NSKeyValueCoding.NullValue.toString()) || (laChaine.equals("")) || (laChaine.equals("*nil*")))
			return "";

		return laChaine;
	}

	public static int stringToInt(String num, int defaultValue) {
		try {
			num = num.trim();
			return Integer.valueOf(num).intValue();
		}
		catch (NumberFormatException ex) {
			return defaultValue;
		}
	}

	public static Integer stringToInteger(String num, int defaultValue) {
		try {
			num = num.trim();
			return Integer.valueOf(num);
		}
		catch (NumberFormatException ex) {
			return new Integer(defaultValue);
		}
	}

	public static boolean characterIsPresentInString(String chaine, String car) {
		if ((NSArray.componentsSeparatedByString(chaine, car)).count() > 1)
			return true;

		return false;
	}

	public static boolean containsIgnoreCase(String s, String substring) {
		s = s.toUpperCase();
		substring = substring.toUpperCase();
		return (s.indexOf(substring) >= 0);
	}

	public static boolean startsWithIgnoreCase(String s, String substring) {
		s = s.toUpperCase();
		substring = substring.toUpperCase();
		return s.startsWith(substring);
	}

	public static String getSuffix(String s, String prefix) {
		s = s.toUpperCase();
		prefix = prefix.toUpperCase();
		if (s.startsWith(prefix))
			return s.substring(prefix.length());
		else
			return "";
	}

	public static String get0Int(int number, int digits) {
		String s = String.valueOf(number);
		for (; s.length() < digits; s = "0" + s)
			;
		return s;
	}

	public static String toHttp(String aString) {
		int idx, startIdx;
		startIdx = 0;
		do {
			idx = aString.indexOf(" ", startIdx);
			if (idx != -1) {
				aString = aString.substring(0, idx) + "%20" + aString.substring(idx + 1);
				startIdx = idx + 3;
			}
		}
		while (idx != -1);
		return aString;
	}

	public static NSArray componentsSeparatedByString(String aString, String separateur) {
		NSMutableArray localMutableArray = new NSMutableArray();
		int index;

		while (aString.indexOf(separateur) > -1) {
			index = aString.indexOf(separateur);
			localMutableArray.addObject(aString.substring(0, index));
			aString = aString.substring(index + (separateur.length()), aString.length());
		}

		localMutableArray.addObject(aString);
		return localMutableArray;
	}

}
