/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.TimeZone;
import java.util.Vector;

import org.cocktail.application.serveur.CocktailSession;
import org.cocktail.application.serveur.eof.EOExercice;
import org.cocktail.fwkcktlcompta.server.remotes.CktlComptaRemoteDelegatePrint;
import org.cocktail.fwkcktlcompta.server.remotes.RemoteDelegateCompta;
import org.cocktail.fwkcktlcompta.server.remotes.RemoteDelegateSepaSDD;
import org.cocktail.fwkcktlwebapp.common.util.CRIpto;
import org.cocktail.fwkcktlwebapp.common.util.FileCtrl;
import org.cocktail.javaclientutilities.reporting.server.Reporter;
import org.cocktail.kava.server.SessionPieFwk;
import org.cocktail.kava.server.metier.EOFacturePapier;
import org.cocktail.kava.server.metier.EOGestion;
import org.cocktail.kava.server.metier.EOPrestation;
import org.cocktail.kava.server.metier.remotes.RemoteDelegateRecette;
import org.cocktail.kava.server.procedures.GetNumerotation;
import org.cocktail.kava.server.ws.WSPrestationFactory;
import org.cocktail.kava.server.ws.consumer.Example;
import org.cocktail.reporting.server.jrxml.JrxmlReporter;

import Structure.server.DBContextDelegate;
import app.server.reports.ReportFactorySix;
import app.server.tools.MailPrestation;

import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eodistribution.EODistributionContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSBundle;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimeZone;

import er.extensions.appserver.ERXApplication;

public class Session extends CocktailSession {

	DBContextDelegate dbDelegate; // utilise par le framework Structure

	public Application app;
	public EOEditingContext ec;
	public String userLogin;
	public RemoteDelegateCompta remoteDelegateCompta;
	public RemoteDelegateRecette remoteDelegateRecette;
	//	public RemoteDelegateReport remoteDelegateReport;
	public CktlComptaRemoteDelegatePrint remoteDelegatePrint;
	public RemoteDelegateSepaSDD remoteDelegateSepaSDD;

	protected SessionPieFwk sessionPieFwk;

	private String versionClient;
	private WSPrestationFactory currentWSPrestationFactory;


	public Session() {
		super();

		// utilise par le framework Structure
		dbDelegate = new DBContextDelegate(defaultEditingContext(), true);

		app = (Application) ERXApplication.application();
		ec = this.defaultEditingContext();

		sessionPieFwk = new SessionPieFwk(app.dataBus(), ec);
		remoteDelegateCompta = new RemoteDelegateCompta(this);
		remoteDelegateRecette = new RemoteDelegateRecette(this);
		remoteDelegatePrint = new CktlComptaRemoteDelegatePrint(this);
		remoteDelegateSepaSDD = new RemoteDelegateSepaSDD(this);

	}

	public SessionPieFwk sessionPieFwk() {
		return sessionPieFwk;
	}

	public boolean distributionContextShouldFollowKeyPath(EODistributionContext distributionContext, String path) {
		return true;
	}

	/**
	 * Utilise par le framework Structure
	 *
	 * @param message
	 */
	public void clientSideRequestFilter(String message) {
		System.out.println("\n\n\n\nclientSideRequestFilter : " + message + "\n\n\n");
		dbDelegate.setFilter(message);
	}

	public void clientSideRequestTest() {
		new Example();
	}

	public void terminate() {
		System.out.println("[" + new Date().toString() + "] " + userLogin + " fin de session !");
		super.terminate();
	}

	public Boolean clientSideRequestIsDevelopmentMode() {
		return ERXApplication.isDevelopmentModeSafe();
	}

	public String clientSideRequestGetParam(String paramKey) {
		return app.getParam(paramKey);
	}

	public void clientSideRequestPushVersionClient(String versionClient) throws Exception {
		this.versionClient = versionClient;
		if (!this.versionClient.equals(VersionMe.rawVersion())) {
			Exception e = new Exception("La version de PieApp renvoyée par le client " + versionClient + " n'est pas compatible avec la version coté serveur (" + VersionMe.rawVersion() + ")");
			e.printStackTrace();
			throw e;
		}
	}

	public NSArray clientSideRequestGetParams(String paramKey) {
		return app.getParams(paramKey);
	}

	public Boolean clientSideRequestGetParamBoolean(String paramKey) {
		return Boolean.valueOf(app.getParamBoolean(paramKey));
	}

	public Boolean clientSideRequestGetParamBoolean(String paramKey, Boolean defaultValueWhenNotFound) {
		return Boolean.valueOf(app.getParamBoolean(paramKey, defaultValueWhenNotFound.booleanValue()));
	}

	public void clientSideRequestServerReset() {
		app.resetAppParametres();
		app.dataBus().editingContext().invalidateAllObjects();
	}

	public void clientSideRequestResetAppParametres() {
		app.resetAppParametres();
	}

	public Number clientSideRequestGetNumerotation(EOEnterpriseObject exercice, EOEnterpriseObject gestion, String tnuEntite) throws Exception {
		return GetNumerotation.get(app.dataBus(), ec, (EOExercice) exercice, (EOGestion) gestion, tnuEntite);
	}

	public void clientSideRequestSendMail(String mailFrom, String mailTo, String mailCC, String mailSubject, String mailBody, String filename,
			NSData filedata) throws Exception {
		if (!app.sendMail(mailFrom, mailTo, mailCC, mailSubject, mailBody, filename, filedata)) {
			throw new Exception("Erreur lors de l'envoi du mail.");
		}
	}

	public void clientSideRequestSendMailHtml(String mailFrom, String mailTo, String mailCC, String mailSubject, String mailBody,
			String filename, NSData filedata) throws Exception {
		if (!app.sendMailHtml(mailFrom, mailTo, mailCC, mailSubject, mailBody, filename, filedata)) {
			throw new Exception("Erreur lors de l'envoi du mail.");
		}
	}

	public String clientSideRequestOutLog() {
		return app.outLog();
	}

	public String clientSideRequestErrLog() {
		return app.errLog();
	}

	public String clientSideRequestGetMailDevisFr(EOEnterpriseObject prestation) throws Exception {
		return MailPrestation.mailDevisFr((EOPrestation) prestation);
	}

	public String clientSideRequestGetMailDemandeValidationDevisFr(EOEnterpriseObject prestation) throws Exception {
		return MailPrestation.mailDemandeValidationDevisFr((EOPrestation) prestation);
	}

	public String clientSideRequestGetMailDevisEn(EOEnterpriseObject prestation) throws Exception {
		return MailPrestation.mailDevisEn((EOPrestation) prestation);
	}

	public String clientSideRequestGetMailFactureFr(EOEnterpriseObject facture) throws Exception {
		return MailPrestation.mailFactureFr((EOFacturePapier) facture);
	}

	public String clientSideRequestGetMailFactureEn(EOEnterpriseObject facture) throws Exception {
		return MailPrestation.mailFactureEn((EOFacturePapier) facture);
	}

	public Boolean clientSideRequestRequestsAuthentication() {
		return Boolean.valueOf(app.getParamBoolean("REQUESTS_AUTHENTICATION", true));
	}

	public void clientSideRequestSetFwkCktlWebApp(String userLogin) {
		this.userLogin = userLogin;
		System.out.println("[" + new Date().toString() + "] " + userLogin + " connecte !");
	}

	public String clientSideRequestAppliId() {
		return app.appliId();
	}

	public String clientSideRequestAppliVersion() {
		return app.txtAppliVersion();
	}

	public String clientSideRequestCopyright() {
		return app.copyright();
	}

	public String clientSideRequestAppliBdVersion() {
		return app.getAppliBdVersion();
	}

	public String clientSideRequestMinAppliBdVersion() {
		return Version.minAppliBdVersion();
	}

	public String clientSideRequestBdConnexionName() {
		return app.bdConnexionName();
	}

	public String clientSideRequestConfigFileContent() {
		return app.fileContent(app.configFilePath());
	}

	public String clientSideRequestInitLog() {
		return app.initLog().toString();
	}

	public NSTimeZone clientSideRequestDefaultNSTimeZone() {
		return NSTimeZone.defaultTimeZone();
	}

	public TimeZone clientSideRequestDefaultTimeZone() {
		return TimeZone.getDefault();
	}

	// impressions devis
	public NSData clientSideRequestPrintDevis(EOEnterpriseObject prestation, NSDictionary metadata) throws Exception {
		if (prestation == null) {
			throw new Exception("Devis a imprimer null");
		}
		return ReportFactorySix.printDevis(app, ec, (EOPrestation) prestation, metadata);
	}

	// facture papier
	public NSData clientSideRequestPrintFacturePapier(EOEnterpriseObject facturePapier, NSDictionary metadata) throws Exception {
		if (facturePapier == null) {
			throw new Exception("Facture papier a imprimer null");
		}
		return ReportFactorySix.printFacturePapier(app, ec, (EOFacturePapier) facturePapier, metadata);
	}

	// facture papier anglais
	public NSData clientSideRequestPrintFacturePapierAnglais(EOEnterpriseObject facturePapier, NSDictionary metadata) throws Exception {
		if (facturePapier == null) {
			throw new Exception("Facture papier a imprimer null");
		}
		return ReportFactorySix.printFacturePapierAnglais(app, ec, (EOFacturePapier) facturePapier, metadata);
	}

	public Boolean clientSideRequestCheckMaquetteExists(String maquetteId) {
		if (maquetteId == null) {
			return Boolean.FALSE;
		}
		return Boolean.valueOf(ReportFactorySix.checkMaquetteExists(app, maquetteId));
	}

	/**
	 * Recherche le dictionnaire de connexion d'un utilisateur grace au serveur d'autentification si dispo
	 */
	public NSDictionary clientSideRequestGetUserAuthentication(String usrLogin, String usrPass, Boolean isCrypted) {
		if (isCrypted.booleanValue()) {
			usrLogin = CRIpto.decrypt(usrLogin);
			usrPass = CRIpto.decrypt(usrPass);
		}

		System.out.println("[" + new Date().toString() + "] " + "Tentative de connexion pour : " + usrLogin);

		Hashtable h = app.getUserAuthentication(usrLogin, usrPass);

		if (h.get("err") != null) {
			System.out.println("Erreur lors de la tentative d'identification de : " + usrLogin + " : " + h.get("err"));
			return new NSDictionary(h.get("err"), "err");
		}

		Integer noIndividu = null;
		try {
			noIndividu = new Integer(h.get("noIndividu").toString());
		} catch (Exception e) {
		}
		Integer persId = null;
		try {
			persId = new Integer(h.get("persId").toString());
		} catch (Exception e) {
		}
		Integer noCompte = null;
		try {
			noCompte = new Integer(h.get("noCompte").toString());
		} catch (Exception e) {
		}
		NSMutableDictionary d = new NSMutableDictionary(8);
		dicoTakeValueForKey(d, (String) h.get("login"), "login");
		dicoTakeValueForKey(d, noIndividu, "noIndividu");
		dicoTakeValueForKey(d, persId, "persId");
		dicoTakeValueForKey(d, (String) h.get("nom"), "nom");
		dicoTakeValueForKey(d, (String) h.get("prenom"), "prenom");
		dicoTakeValueForKey(d, (String) h.get("email"), "email");
		dicoTakeValueForKey(d, noCompte, "noCompte");
		dicoTakeValueForKey(d, (String) h.get("vLan"), "vLan");

		System.out.println("[" + new Date().toString() + "] " + "Identification reussie de : " + usrLogin);

		return d.immutableClone();
	}

	public NSData clientSideRequestReadResourceFile(String resourceFileName) {
		Vector v = FileCtrl.findFiles(bundlePath(NSBundle.mainBundle()), resourceFileName, true);
		String filePath = null;
		NSData data = null;
		if (v.size() > 0) {
			filePath = (String) v.elementAt(0);
		}
		if (filePath != null) {
			try {
				data = new NSData(new FileInputStream(filePath), 2048);
			} catch (IOException ioe) {
				System.err.println(ioe);
			}
		}
		return data;
	}

	private String bundlePath(NSBundle bundle) {
		// On utilise la reflection de Java
		// pour ne pas avoir le message "deprecated" a la compilation
		try {
			Method method = NSBundle.class.getMethod("bundlePath", null);
			return (String) method.invoke(bundle, null);
		} catch (Throwable ex) {
		}
		return null;
	}

	// methodes pour les frameworks Echeancier et Cocowork...

	public NSData clientSideRequestCreateReport(String jasperFileName, NSDictionary parameters, Integer format, String modelNameForJdbcConnection) {
		System.out.println("\n\nSession.clientSideRequestCreateReport()");
		System.out.println("jasperFileName = " + jasperFileName);
		System.out.println("parameters = " + parameters);
		System.out.println("format = " + format);
		System.out.println("modelNameForJdbcConnection = " + modelNameForJdbcConnection);
		System.out.println("\n\n");
		NSData data = null;
		try {
			data = clientSideRequestPrintJasperReport(jasperFileName, null, parameters, null, true);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return data;
	}

	public Connection getNewJDBCConnectionForModelName(String name) {
		NSDictionary dicoConnection = null;
		try {
			dicoConnection = EOModelGroup.defaultGroup().modelNamed(name).connectionDictionary();
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (Throwable e) {
			app.addLogMessage("INFORMATION", "le modelName n'est pas redefini -> utilisation de la connexion par defaut");
			return app.getJDBCConnection();
		}

		try {
			return DriverManager.getConnection(dicoConnection.valueForKey("URL").toString(), dicoConnection.valueForKey("username").toString(), dicoConnection.valueForKey("password").toString());
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return app.getJDBCConnection();
	}

	/**
	 * Retourne la derniere exception rencontree lors de la creation du report.
	 *
	 * @return L'exception rencontree.
	 */
	public String clientSideRequestGetCreateReportErrorMessage() {
		System.out.println("\n\nSession.clientSideRequestGetLastReporterExceptionThrown()");
		System.out.println("\n\n");
		if (Reporter.getLastReporterExceptionThrown() != null) {
			StringBuffer buf = new StringBuffer(Reporter.getLastReporterExceptionThrown().getMessage());
			if (Reporter.getLastReporterExceptionThrown().getCause() != null) {
				buf.append("\n");
				buf.append("Cause: ");
				buf.append(Reporter.getLastReporterExceptionThrown().getCause().getMessage());
			}
			return buf.toString();
		}
		return "Aucun d\u00E9tail disponible.";
	}

	// Reports Jasper

	public final NSData clientSideRequestPrintJasperReport(String idreport, String sqlQuery, NSDictionary parametres, String customJasperId,
			Boolean printIfEmpty) throws Exception {
		
		try {
			System.out.println("Session.clientSideRequestPrintJasper() DEBUT");
			HashMap params = null;
			if (parametres != null) {
				params = new HashMap(parametres.hashtable());
				System.out.println("parametres : " + params);
			} else {
				params = new HashMap();
			}

			String jasperId = idreport;
			System.out.println("Session.clientSideRequestPrintByThread() JASPER ID : " + jasperId);
			if (customJasperId != null) {
				jasperId = customJasperId;
				System.out.println("Utilisation d'un report personnalise :" + customJasperId);
			}

			final String realJasperFilName = getRealJasperFileName(jasperId);
			// recuperer le chemin du repertoire du report
			final File f = new File(realJasperFilName);
			String rep = f.getParent();
			if (rep != null) {
				if (!rep.endsWith(File.separator)) {
					rep = rep + File.separator;
				}
			}
			params.put("REP_BASE_PATH", rep);
			

			// utilisation des methodes du FrameWorkReporting pour générer les pdf
			JrxmlReporter report = new JrxmlReporter();
			report.printNow(app.getJDBCConnection(), sqlQuery, realJasperFilName, params, 1, printIfEmpty, true, null);
			NSData pdf = new NSData(report.getCurrentReportingTaskThread().getDataResult());
			return pdf;


		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * On verifie si le report est defini dans la table, sinon renvoie le chemin d'acces au report fourni par defaut par l'application.
	 *
	 * @param fn
	 * @return
	 */
	private final String getRealJasperFileName(String rname) {
		String loc = null;
		if (loc == null) {
			System.out.println("Session.getRealJasperFileName() Report " + rname
					+ " NON defini dans la table REPORT, utilisation du report par defaut");
			loc = app.getReportsLocation() + rname;
		}
		return loc;
	}

	// privates...
	private void dicoTakeValueForKey(NSMutableDictionary dico, Object value, String key) {
		if (dico != null && value != null && key != null) {
			dico.takeValueForKey(value, key);
		}
	}

	public WSPrestationFactory getCurrentWSPrestationFactory() {
		return currentWSPrestationFactory;
	}

	public void setCurrentWSPrestationFactory(WSPrestationFactory currentWSPrestationFactory) {
		this.currentWSPrestationFactory = currentWSPrestationFactory;
	}

	public String clientSideRequestVersionRaw() {
		return VersionMe.rawVersion();
	}
}
