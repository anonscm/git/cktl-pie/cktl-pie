/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.server;

import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlcompta.common.FwkCktlComptaMoteurCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlERXStaticResourceRequestHandler;
import org.cocktail.fwkcktlwebapp.server.init.NSLegacyBundleMonkeyPatch;
import org.cocktail.kava.server.finder.FinderParametreJefyAdmin;
import org.cocktail.kava.server.metier.EOParametreJefyAdmin;
import org.cocktail.kava.server.ws.WSPrestationFactoryProvider;
import org.cocktail.pieFwk.common.ApplicationConfig;
import org.cocktail.pieFwk.common.service.AbstractFacturationService;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation._NSUtilities;
import com.woinject.WOInject;

public class Application extends ZApplication implements ApplicationConfig {

	private static final String		MAIN_MODEL_NAME			= "PieFwk";
	private static final String		PARAMETRES_TABLE_NAME	= "Parametres";
	private static final String		CONFIG_FILE_NAME		= "Pie.config";
	private static final String		CONFIG_TABLE_NAME		= "FwkCktlWebApp_GrhumParametres";
	private static final String[]	REQUIRED_PARAMS			= {"APP_ID", "APP_USE_CAS", "REQUESTS_AUTHENTICATION"};
	private static final String[]	MAQUETTES_SIX			= {"SIXID_PIE_DEVIS", "SIXID_PIE_FACTURE"};

	private Map<Number, Integer> PARAM_MONETAIRE_PAR_ANNEE = new HashMap<Number, Integer>();

	public static void main(String argv[]) {
		NSLegacyBundleMonkeyPatch.apply();
        WOInject.init("app.server.Application", argv);

	}

	public Application() {
		super();
		try {
			super.initApplicationSuite();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		setDefaultRequestHandler(requestHandlerForKey(directActionRequestHandlerKey()));

		// fix pour que les WebServerResources marchent en javaclient
		if (isDirectConnectEnabled()) {
			registerRequestHandler(new CktlERXStaticResourceRequestHandler(), "wr");
		}
		fixClassLoading();
	}

	private void fixClassLoading() {
		_NSUtilities.setClassForName(Main.class, Main.class.getSimpleName());
	}
	
	public boolean forceUseNewConnexion() {
		return false;
	}

	/**
	 * @return booléen bloquant: si false, le démarrage de l'application s'interrompt !
	 */
	protected boolean initApplicationSpecial() {
		try {
			initLog().appendTitle("Vérification des frameworks...");

			int[] fwkVersion, minVersion;

			// PieFwk Serveur
			//TODO supprimer ce qui est en commentaire ci-dessous une fois testé
			//			try {
			//				fwkVersion = org.cocktail.kava.server.Version.version();
			//				minVersion = VersionMe.MIN_FWK_PIE_VERSION;
			//			} catch (Throwable e) {
			//				throw new Exception("Framework PieFwk  manquant ou erroné !");
			//			}
			//			compareFwkVersion("PieFwk", fwkVersion, minVersion);
			//
			//			// CocktailApplication
			//			try {
			//				fwkVersion = versionCocktailApplication().version();
			//				minVersion = VersionCommon.MIN_FWK_COCKTAIL_APPLICATION_VERSION;
			//			} catch (Throwable e) {
			//				throw new Exception("Framework CocktailApplication2  manquant ou erroné !\n"+e);
			//			}
			//compareFwkVersion("CocktailApplication2", fwkVersion, minVersion);

			//			// Echeancier
			//			try {
			//				fwkVersion = org.cocktail.echeancier.server.Version.version();
			//				minVersion = VersionCommon.MIN_FWK_ECHEANCIER_VERSION;
			//			} catch (Throwable e) {
			//				throw new Exception("Framework Echeancier manquant ou erroné !");
			//			}
			//			compareFwkVersion("Echeancier", fwkVersion, minVersion);
			//
			//			// CocoWork
			//			try {
			//				fwkVersion = org.cocktail.cocowork.server.Version.version();
			//				minVersion = VersionCommon.MIN_FWK_COCOWORK_VERSION;
			//			} catch (Throwable e) {
			//				throw new Exception("Framework CocoWork manquant ou erroné !");
			//			}
			//			compareFwkVersion("CocoWork", fwkVersion, minVersion);
			//
			//			// TheJavaClientUtilities
			//			try {
			//				fwkVersion = org.cocktail.javaclientutilities.Version.version();
			//				minVersion = VersionCommon.MIN_FWK_THEJAVACLIENTUTILITIES_VERSION;
			//			} catch (Throwable e) {
			//				throw new Exception("Framework TheJavaClientUtilities manquant ou erroné !");
			//			}
			//			compareFwkVersion("TheJavaClientUtilities", fwkVersion, minVersion);
			//
			//			// Structure
			//			try {
			//				fwkVersion = Structure.common.Version.version();
			//				minVersion = VersionCommon.MIN_FWK_STRUCTURE_VERSION;
			//			} catch (Throwable e) {
			//				e.printStackTrace();
			//				throw new Exception("Framework Structure manquant ou erroné !");
			//			}
			//			compareFwkVersion("Structure", fwkVersion, minVersion);

		} catch (Exception e) {
			initLog().append("");
			initLog().appendWarning(e.getMessage());
			return false;
		}

		// WebServices
		initLog().appendTitle("Initialisation des WebServices");

		// WSPrestation
		initWebServiceProvider(WSPrestationFactoryProvider.class, WSPrestationFactoryProvider.WS_EXTERNAL_NAME,
				WSPrestationFactoryProvider.WS_PARAM, WSPrestationFactoryProvider.WS_PASSWORD_PARAM);

		// moteur SEPA
		initMoteurCompta();

		return true;
	}

	protected void initMoteurCompta() {
		FwkCktlComptaMoteurCtrl.getSharedInstance().init(FwkCktlComptaMoteurCtrl.Contexte.SERVEUR);
		FwkCktlComptaMoteurCtrl.getSharedInstance().setReportsBaseLocation(getReportsLocation());
		FwkCktlComptaMoteurCtrl.getSharedInstance().setRefApplicationCreation(VersionMe.APPLICATION_TYAP_STRID);
	}

	protected String appliId() {
		return Version.appliId();
	}

	public String htmlAppliVersion() {
		return Version.htmlAppliVersion();
	}

	protected String txtAppliVersion() {
		return Version.txtAppliVersion();
	}

	public String copyright() {
		return Version.copyright();
	}

	public String windowTitle() {
		return appliId() + " - " + txtAppliVersion() + " - " + bdConnexionName();
	}

	public String mainModelName() {
		return MAIN_MODEL_NAME;
	}

	public String configFileName() {
		return CONFIG_FILE_NAME;
	}

	public String configTableName() {
		return CONFIG_TABLE_NAME;
	}

	protected String[] requiredParams() {
		return REQUIRED_PARAMS;
	}

	protected String[] maquettesSix() {
		return MAQUETTES_SIX;
	}

	public boolean _isSupportedDevelopmentPlatform() {
		return (super._isSupportedDevelopmentPlatform() || System.getProperty("os.name").startsWith("Windows"));
	}

	protected String parametresTableName() {
		return PARAMETRES_TABLE_NAME;
	}



	public int nbDecimalesImposees(EOEditingContext edc, Number exercice) {
		if (!PARAM_MONETAIRE_PAR_ANNEE.containsKey(exercice)) {
			PARAM_MONETAIRE_PAR_ANNEE.put(exercice, loadParamMonetaire(edc, exercice));
		}
		return PARAM_MONETAIRE_PAR_ANNEE.get(exercice);
	}

	private Integer loadParamMonetaire(EOEditingContext edc, Number exercice) {
		boolean useDecimal = getParamJefyAdminBoolean(edc, EOParametreJefyAdmin.PARAM_USE_DECIMAL, exercice, true);
		if (useDecimal) {
			return AbstractFacturationService.DEUX_DECIMALES;
		} else {
			return AbstractFacturationService.ZERO_DECIMALE;
		}
	}

	private boolean getParamJefyAdminBoolean(EOEditingContext edc, String parKey, Number exercice, boolean defaultValueWhenNotFound) {
		String parValue = getParamJefyAdmin(edc, parKey, exercice);
		return getGenericParamBoolean(parValue, defaultValueWhenNotFound);
	}

	private String getParamJefyAdmin(EOEditingContext edc, String parKey, Number exercice) {
		try {
			return FinderParametreJefyAdmin.find(edc, parKey, exercice);
		} catch (Exception e) {
			return null;
		}
	}

	private boolean getGenericParamBoolean(String parValue, boolean defaultValueWhenNotFound) {
		if (parValue == null) {
			return defaultValueWhenNotFound;
		}
		if (parValue.equals("1") || parValue.equalsIgnoreCase("O") || parValue.equalsIgnoreCase("OUI") || parValue.equalsIgnoreCase("Y")
				|| parValue.equalsIgnoreCase("YES") || parValue.equalsIgnoreCase("TRUE")) {
			return true;
		}
		return false;
	}
}
