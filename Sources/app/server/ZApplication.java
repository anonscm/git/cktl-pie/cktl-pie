/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.TimeZone;

import org.cocktail.application.serveur.CocktailApplication;
import org.cocktail.fwkcktlwebapp.common.database.CktlUserInfoDB;
import org.cocktail.fwkcktlwebapp.common.print.CktlPrintConst;
import org.cocktail.fwkcktlwebapp.common.print.CktlPrinter;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.kava.server.metier.EOParametres;

import app.server.avirer.CktlMailMessage;
import app.server.tools.BufferedLog;
import app.server.tools.Log;

import com.webobjects.appserver.WOWebServiceRegistrar;
import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestampFormatter;

import er.extensions.appserver.ERXApplication;

public abstract class ZApplication extends CocktailApplication {

	/**
	 * @return booléen bloquant: si false, le démarrage de l'application
	 *         s'interrompt !
	 */
	protected abstract boolean initApplicationSpecial();
	protected abstract String[] requiredParams();
	protected abstract String[] maquettesSix();
	protected abstract String txtAppliVersion();
	protected abstract String parametresTableName();
	protected abstract String appliId();

	private NSMutableDictionary appParametres;
	private BufferedLog redirectedOutStream, redirectedErrStream;
	private Log initLog;

	public ZApplication() {
		super();
	}

	public void initApplicationSuite() {

		initLog = new Log();
		initLog.setShowOutput("1".equals(getParam("SHOWINITLOG")));
		initLog.setShowTrace("1".equals(getParam("SHOWTRACE")));

		initLog.appendBigTitle("INITIALISATION");

		boolean ok1 = init();
		boolean ok2 = checkUp();

		boolean ok3 = initApplicationSpecial();
		if (!ok3) {
			initLog.append("");
			initLog.appendWarning("Il y a eu des ERREURS CRITIQUES D'INITIALISATION, impossible de démarrer l'application (voir ci-dessus pour les détails) !!!");
			initLog.append("");
			System.exit(-1);
		}

		if (!ok1 || !ok2) {
			initLog.append("");
			initLog.appendWarning("Il y a eu des ERREURS D'INITIALISATION !!!");
			initLog.append("");
		}

		initLog.appendBigTitle("INITIALISATION TERMINEE");
	}

	private boolean init() {
		boolean ok = true;

		// full logs?
		if (getParamBoolean("SHOWSQLLOGS")) {
			NSLog.setAllowedDebugLevel(NSLog.DebugLevelDetailed);
			NSLog.allowDebugLoggingForGroups(NSLog.DebugGroupSQLGeneration);
			NSLog.allowDebugLoggingForGroups(NSLog.DebugGroupDatabaseAccess);
		}

		// on ajuste le timezone (passer autre chose que null si on veut forcer)
		initTimeZones(null);

		return ok;
	}

	/**
	 * @return true si tout est ok, false si il y a des warnings
	 */
	private boolean checkUp() {
		// Versions...
		initLog.appendTitle("Versions");
		initLog.appendKeyValue("Version de WebObjects", ERXApplication.isWO54() ? "5.4" : "Non 5.4");
		initLog.appendKeyValue("Version Java Runtime", System.getProperty("java.version"));
		initLog.appendKeyValue("Version de " + name(), txtAppliVersion());

		// Propriétés système...
		if ("1".equals(getParam("DISPLAY_SYSTEM_PROPERTIES"))) {
			initLog.appendTitle("Proprietes systeme");
			initLog.appendMap(System.getProperties());
		}

		// Base de données...
		boolean okBdAccess = checkBdConnection();

		// Vérification des paramétres obligatoires...
		boolean okRequiredParams = checkRequiredParams();

		// Verification de la version de la base...
		boolean okBdVersion = checkDbVersion();

		// Verification du serveur SIX et de la presence des maquettes SIX s'il y en a...
		boolean okSIX = checkSix();

		return okBdAccess && okRequiredParams && okBdVersion && okSIX;
	}

	/**
	 * Compare 2 numéros de version donnés sous la forme de tableaux de int.
	 * Chaque numéro de version peut contenir autant d'éléments que l'on
	 * veut.<br>
	 * 2 numéros null ou vides sont considérés égaux.<br>
	 * 1 numéro null est considéré inférieur é l'autre non null.<br>
	 * 1 numéro vide est considéré inférieur é l'autre non vide.<br>
	 * <b>ATTENTION:<b> 1.2.0 est considéré égal é 1.2 !!!
	 *
	 * @param version
	 * @param anotherVersion
	 * @return Un entier négatif, zéro, ou un entier positif si le premier
	 *         argument est inférieur, égal é, ou supérieur au second.
	 */
	public static int compare(final int[] version, final int[] anotherVersion) {
		if (version == null && anotherVersion == null) {
			return 0;
		}
		if (version == null) {
			return -1;
		}
		if (anotherVersion == null) {
			return 1;
		}
		for (int i = 0; i < version.length && i < anotherVersion.length; i++) {
			if (version[i] < anotherVersion[i]) {
				return -1;
			}
			if (version[i] > anotherVersion[i]) {
				return 1;
			}
		}
		if (version.length < anotherVersion.length) {
			for (int i = version.length; i < anotherVersion.length; i++) {
				if (anotherVersion[i] > 0) {
					return -1;
				}
			}
		}
		if (version.length > anotherVersion.length) {
			for (int i = anotherVersion.length; i < version.length; i++) {
				if (version[i] > 0) {
					return 1;
				}
			}
		}
		return 0;
	}

	public void compareFwkVersion(String fwkName, int[] fwkVersion, int[] minVersion) throws Exception {
		initLog.appendKeyValue(fwkName, toString(fwkVersion) + " (minimum requis: " + toString(minVersion) + ")");
	}

	private static String toString(int[] array) {
		if (array == null) {
			return "";
		}
		StringBuffer sb = new StringBuffer("");
		for (int i = 0; i < array.length; i++) {
			sb = sb.append(array[i]);
			sb = sb.append('.');
		}
		if (sb.length() > 0) {
			sb = sb.deleteCharAt(sb.length() - 1);
		}
		return sb.toString();
	}

	/**
	 * Récupére une valeur dans la table parametresTableName(). Si elle
	 * n'existe pas dans la table, va la chercher dans le configFileName(), et
	 * si n'existe pas va dans la table configTableName()
	 *
	 * @param paramKey
	 *            La clé é rechercher
	 * @return La premiére valeur associée é la clé paramkey.
	 * @see ZApplication#appParametres
	 */
	public String getParam(String paramKey) {
		System.out.println("paramKey = " + paramKey);
		NSArray a = (NSArray) appParametres().valueForKey(paramKey);
		String res = null;
		if (a == null || a.count() == 0) {
			// recherche dans le configFileName()/configTableName()
			res = config().stringForKey(paramKey);
		} else {
			res = (String) a.objectAtIndex(0);
		}
		System.out.println("res = " + res);
		return res;
	}

	/**
	 * Récupére x valeur(s) dans la table parametresTableName(). Si elle
	 * n'existe pas dans la table, va la chercher dans le configFileName(), et
	 * si n'existe pas va dans la table configTableName()
	 *
	 * @param paramKey
	 *            La clé é rechercher
	 * @return La(les) valeur(s) associée(s) é la clé paramkey.
	 * @see ZApplication#appParametres
	 */
	public NSArray getParams(String paramKey) {
		NSArray a = (NSArray) appParametres().valueForKey(paramKey);
		if (a == null || a.count() == 0) {
			// recherche dans le configFileName()/configTableName()
			a = config().valuesForKey(paramKey);
		}
		if (a == null) {
			a = new NSArray();
		}
		return a;
	}

	/**
	 * Récupére une valeur dans la table parametresTableName(). A n'utiliser
	 * que pour récupérer des paramétres spécifiques é l'application, et si
	 * on ne veut pas aller chercher le paramétre ailleurs que dans la table de
	 * paramétrage de l'application.
	 *
	 * @param paramKey
	 *            La clé é rechercher
	 * @return La premiére valeur associée é la clé paramkey.
	 * @see ZApplication#appParametres
	 */
	public String getAppParam(String paramKey) {
		NSArray a = (NSArray) appParametres().valueForKey(paramKey);
		String res = null;
		if (a != null && a.count() > 0) {
			res = (String) a.objectAtIndex(0);
		}
		return res;
	}

	/**
	 * Récupére x valeur(s) dans la table parametresTableName(). A n'utiliser
	 * que pour récupérer des paramétres spécifiques é l'application, et si
	 * on ne veut pas aller chercher les paramétres ailleurs que dans la table
	 * de paramétrage de l'application.
	 *
	 * @param paramKey
	 *            La clé é rechercher
	 * @return La(les) valeur(s) associée(s) é la clé paramkey.
	 * @see ZApplication#appParametres
	 */
	public NSArray getAppParams(String paramKey) {
		NSArray a = (NSArray) appParametres().valueForKey(paramKey);
		if (a == null) {
			a = new NSArray();
		}
		return a;
	}

	public boolean getParamBoolean(String paramKey) {
		return getParamBoolean(paramKey, false);
	}

	public boolean getParamBoolean(String paramKey, boolean defaultValueWhenNotFound) {
		String s = getParam(paramKey);
		if (s == null) {
			return defaultValueWhenNotFound;
		}
		if (s.equals("1") || s.equalsIgnoreCase("O") || s.equalsIgnoreCase("OUI") || s.equalsIgnoreCase("Y") || s.equalsIgnoreCase("YES") || s.equalsIgnoreCase("TRUE")) {
			return true;
		}
		return false;
	}

	public String hrefContactMail() {
		return "mailto:" + contactMail();
	}

	public String appHtmlCssStyles() {
		return getParam("APP_HTML_CSS_STYLES");
	}

	public boolean appUseCas() {
		return getParamBoolean("APP_USE_CAS");
	}

	public boolean allowWebAccess() {
		return getParamBoolean("ALLOW_WEB_ACCESS", true);
	}

	public Log initLog() {
		return initLog;
	}

	public String outLog() {
		return redirectedOutStream.toString();
	}

	public String errLog() {
		return redirectedErrStream.toString();
	}

	public void resetAppParametres() {
		appParametres = null;
	}

	protected Hashtable getUserAuthentication(String usrLogin, String usrPass) {
		CktlUserInfoDB info = new CktlUserInfoDB(dataBus());
		if (usrPass == null) {
			System.out.println("!!! Connexion de " + usrLogin + " sans mot de passe !!!");
			info.setAcceptEmptyPass(true);
		}
		info.compteForLogin(usrLogin, usrPass, true);
		Hashtable h = new Hashtable();
		switch (info.errorCode()) {
		case CktlUserInfoDB.ERROR_COMPTE:
			h.put("err", "Login inconnu !");
			return h;
		case CktlUserInfoDB.ERROR_PASSWORD:
			h.put("err", "Mot de passe incorrect !");
			return h;
		case CktlUserInfoDB.ERROR_INDIVIDU:
			h.put("err", "Pas d'individu associé a ce login ! " + info.errorMessage());
			return h;
		case CktlUserInfoDB.ERROR_SOURCE:
			h.put("err", "Erreur lors de la tentative d'authentification, peut-étre un pb de connection é la base de données ! " + info.errorMessage());
			return h;
		case CktlUserInfoDB.ERROR_NONE:
			return info.toHashtable();
		default:
			h.put("err", "Erreur lors de l'authentification (erreur non récupérée) ! " + info.errorMessage());
			return h;
		}
		// }
	}

	public boolean sendMail(String mailFrom, String mailTo, String mailCC, String mailSubject, String mailBody, String filename, NSData filedata) {
		if (getParamBoolean("TEST_MODE") && getParam("TEST_EMAIL") != null && getParam("TEST_EMAIL").length() > 0) {
			String preamble = "Vous recevez ce message car l'adresse \"" + getParam("TEST_EMAIL") + "\" est l'adresse de test spécifiée dans le fichier de configuration de l'application "
					+ this.appliId() + ".\n\n";
			preamble = preamble + "Normalement le message devrait étre adressé é :\n";
			preamble = preamble + "To : " + mailTo + "\n";
			preamble = preamble + "Cc : " + mailCC + "\n\n";
			preamble = preamble + "------- Message original -------\n";
			mailBody = preamble + mailBody;

			mailTo = getParam("TEST_EMAIL");
			mailCC = null;
		}
		mailBody = mailBody + "\n\n" + "---------------------------------------------------\n" + "Ce message a été généré automatiquement par l'application " + this.appliId();
		return mailBus().sendMail(mailFrom, mailTo, mailCC, mailSubject, mailBody, filename, filedata);
	}

	public boolean sendMailHtml(String mailFrom, String mailTo, String mailCC, String mailSubject, String mailBody, String filename, NSData filedata) {
		if (getParamBoolean("TEST_MODE") && getParam("TEST_EMAIL") != null && getParam("TEST_EMAIL").length() > 0) {
			String preamble = "Vous recevez ce message car l'adresse \"" + getParam("TEST_EMAIL") + "\" est l'adresse de test spécifiée dans le fichier de configuration de l'application "
					+ this.appliId() + ".<br><br>";
			preamble = preamble + "Normalement le message devrait étre adressé é :<br>";
			preamble = preamble + "To : " + mailTo + "<br>";
			preamble = preamble + "Cc : " + mailCC + "<br><br>";
			preamble = preamble + "------- Message original -------<br>";
			mailBody = preamble + mailBody;

			mailTo = getParam("TEST_EMAIL");
			mailCC = null;
		}
		mailBody = mailBody + "<br><br>" + "---------------------------------------------------<br>" + "Ce message a été généré automatiquement par l'application " + this.appliId();

		try {
			CktlMailMessage mailMessage = new CktlMailMessage(getParam("GRHUM_HOST_MAIL"));
			if (filename != null && StringCtrl.normalize(filename).length() > 0 && filedata != null) {
				byte[][] fData = new byte[1][];
				fData[0] = filedata.bytes(0, filedata.length());
				String[] fNames = new String[1];
				fNames[0] = StringCtrl.normalize(filename);
				mailMessage.initMessage(mailFrom, mailTo, mailSubject, mailBody, fNames, fData, true);
			} else {
				mailMessage.initMessage(mailFrom, mailTo, mailSubject, mailBody, null, null, true);
			}
			mailMessage.addCCs(CktlMailMessage.toArray(mailCC));
			mailMessage.setReplyTo(mailFrom);
			// Vector pbAdrs = mailMessage.safeSend();
			mailMessage.safeSend();
			return true;
		} catch (Throwable ex) {
			ex.printStackTrace();
			return false;
		}
	}

	private boolean checkBdConnection() {
		initLog.appendTitle("Base de donnees");
		if (CktlDataBus.isDatabaseConnected()) {
			initLog.appendSuccess("La connexion a la base de donnees est active");
			return true;
		} else {
			initLog.appendWarning("La connexion a la base de donnees n'est pas active!!");
			return false;
		}
	}

	private String[] bdSecondPartUrl(EOModel model) {
		String url = (String) model.connectionDictionary().valueForKey("URL");
		if (url == null || url.length() == 0) {
			return new String[0];
		}
		String[] res;
		// L'url est du type jdbc:oracle:thin:@<machine>:<port>:<instance>
		// On sépare la partie jdbc de la partie server
		res = url.split("@");
		if (res.length > 1) {
			String serverUrl = res[1];
			res = serverUrl.split(":");
			if (res.length > 0) {
				return res;
			}
		}
		return new String[0];
	}

	/**
	 * Initialise le TimeZone é utiliser pour l'application.
	 */
	private void initTimeZones(String timezone) {
		initLog.appendTitle("Initialisation du NSTimeZone");
		// timezone par défaut
		NSTimeZone theTimezone = NSTimeZone.defaultTimeZone();
		initLog.appendKeyValue("NSTimeZone par defaut recupere sur le systeme (avant initialisation) ", theTimezone, 70);
		if (timezone != null && !timezone.trim().equals("")) {
			NSTimeZone ntz = NSTimeZone.timeZoneWithName(timezone, false);
			if (ntz == null) {
				initLog.appendWarning("Le Timezone defini n'est pas valide (" + timezone + ")");
			} else {
				theTimezone = ntz;
			}
		}
		TimeZone.setDefault(theTimezone);
		NSTimeZone.setDefaultTimeZone(theTimezone);

		initLog.appendKeyValue("NSTimeZone par defaut utilise dans l'application", NSTimeZone.defaultTimeZone(), 70);
		NSTimestampFormatter ntf = new NSTimestampFormatter();
		initLog.appendKeyValue("Les NSTimestampFormatter analyseront les dates avec le NSTimeZone", ntf.defaultParseTimeZone(), 70);
		initLog.appendKeyValue("Les NSTimestampFormatter afficheront les dates avec le NSTimeZone", ntf.defaultFormatTimeZone(), 70);
		initLog.append("");
	}

	/**
	 * Verifie la version de la base de donnee par rapport a la version minimum
	 * requise pour cette version de l'application
	 */
	private boolean checkDbVersion() {
		if (Version.minAppliBdVersion() == null) {
			return true;
		}
		initLog.appendTitle("Verification de la version de la base...");
		initLog.appendKeyValue("Version minimum requise", Version.minAppliBdVersion());
		String ver = getAppliBdVersion();

		if (ver == null) {
			initLog.appendKeyValue("Version utilisee", "N/A");
			initLog.appendWarning("Impossible de déterminer la version de la base utilisée (table DB_VERSION vide). Cela risque de poser des problémes! Mettre la base é jour.");
			initLog.appendWarning("On ne lance pas l'application tant que ce probléme n'est pas résolu !!! Désolé !");
			terminate();
			return false;
		} else {
			initLog.appendKeyValue("Version utilisee", ver);
			if (ver.compareTo(Version.minAppliBdVersion()) >= 0) {
				initLog.appendSuccess("Test de version OK.");
				return true;
			} else {
				initLog.appendWarning("Version de base incompatible. Cela risque de poser des problemes! Mettre la base a jour (version minimum: " + Version.minAppliBdVersion()
						+ ") pour utiliser cette version de l'application.");
				return false;
			}
		}
	}

	/**
	 * Récupére la version actuelle de la base de donnees avec un
	 * rawRowsForSQL
	 */
	public String getAppliBdVersion() {
		NSArray a = null;
		try {
			a = EOUtilities.rawRowsForSQL(dataBus().editingContext(), mainModelName(),
					"select db_version from jefy_recette.db_version where db_date = (select max(db_date) from jefy_recette.db_version)");
			return (String) ((NSDictionary) a.lastObject()).valueForKey("DB_VERSION");
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Enregistrement d'un WebService
	 *
	 * @param wsClass
	 * @param wsName
	 */
	protected void initWebServiceProvider(Class wsClass, String wsName, String wsParam, String wsPasswordParam) {
		if (getParamBoolean(wsParam)) {
			String wsPassword = getParam(wsPasswordParam);
			if (wsPassword != null && wsPassword.length() > 0) {
				try {
					WOWebServiceRegistrar.registerWebService(wsName, wsClass, true);
					initLog.appendSuccess("Le WebService " + wsName + " est actif.");
				} catch (Exception e) {
					initLog.appendWarning("Impossible d'activer le webservice " + wsName + " !");
				}
			} else {
				initLog().appendWarning("Le webservice " + wsName + " n'a pas de mot de passe défini, il n'est pas activé !");
			}
		} else {
			initLog().append("Le WebService " + wsName + " n'est pas activé.");
		}
	}

	/**
	 * Vérifie si tous les paramétres nécessaires é l'application sont bien
	 * présents et initialisés.
	 */
	private boolean checkRequiredParams() {
		boolean ok = true;
		if (requiredParams() == null || requiredParams().length == 0) {
			return ok;
		}
		initLog.appendTitle("Verification des parametres");
		for (int i = 0; i < requiredParams().length; i++) {
			String p = getParam(requiredParams()[i]);
			if (p == null || p.trim().length() == 0) {
				initLog.appendWarning("Le parametre " + requiredParams()[i] + " est absent ou vide. Initialisez-le dans la table " + parametresTableName() == null ? "" : parametresTableName()
						+ ", ou " + configTableName() + " ou bien dans le fichier " + configFileName());
				ok = false;
			} else {
				// On affiche la paire clé/valeur dans le log
				initLog.appendKeyValue(requiredParams()[i], p);
			}
		}
		initLog.append("");
		return ok;
	}

	/**
	 * @return Les parametres de l'application stockés dans la table
	 *         parametresTableName()
	 */
	private NSMutableDictionary appParametres() {
		if (appParametres == null) {
			if (dataBus().editingContext() == null || parametresTableName() == null) {
				return new NSMutableDictionary();
			}
			appParametres = new NSMutableDictionary();
			try {
				NSArray vParam = dataBus().fetchArray(dataBus().editingContext(), parametresTableName(), null, null);
				System.out.println("vParam = " + vParam);
				String previousParamKey = null;
				NSMutableArray a = null;
				java.util.Enumeration enumerator = vParam.objectEnumerator();
				while (enumerator.hasMoreElements()) {
					EOGenericRecord vTmpRec = (EOGenericRecord) enumerator.nextElement();
					if (vTmpRec.valueForKey(EOParametres.PAR_KEY_KEY) == null || ((String) vTmpRec.valueForKey(EOParametres.PAR_KEY_KEY)).equals("")
							|| vTmpRec.valueForKey(EOParametres.PAR_VALUE_KEY) == null) {
						continue;
					}
					if (!((String) vTmpRec.valueForKey(EOParametres.PAR_KEY_KEY)).equalsIgnoreCase(previousParamKey)) {
						if (a != null && a.count() > 0) {
							appParametres.setObjectForKey(a, previousParamKey);
						}
						previousParamKey = (String) vTmpRec.valueForKey(EOParametres.PAR_KEY_KEY);
						a = new NSMutableArray();
					}
					if (vTmpRec.valueForKey(EOParametres.PAR_VALUE_KEY) != null) {
						a.addObject((String) vTmpRec.valueForKey(EOParametres.PAR_VALUE_KEY));
					}
				}
				if (a != null && a.count() > 0) {
					appParametres.setObjectForKey(a, previousParamKey);
				}
				dataBus().editingContext().invalidateAllObjects();
			} catch (Exception e) {
				appParametres = new NSMutableDictionary();
			}
		}
		return appParametres;
	}

	private void redirectLogs() {
		// 307200 = limite le log en mémoire a 100Ko
		redirectedOutStream = new BufferedLog(System.out, 307200);
		redirectedErrStream = new BufferedLog(System.err, 307200);
		System.setOut(new PrintStream(redirectedOutStream));
		System.setErr(new PrintStream(redirectedErrStream));
		// on force la sortie du NSLog (qui était initialisé par défaut sur
		// System.xxx AVANT le changement)
		((NSLog.PrintStreamLogger) NSLog.out).setPrintStream(System.out);
		((NSLog.PrintStreamLogger) NSLog.debug).setPrintStream(System.out);
		((NSLog.PrintStreamLogger) NSLog.err).setPrintStream(System.err);
	}

	private boolean checkSix() {
		boolean ok = true;

		if (maquettesSix() != null && maquettesSix().length > 0) {
			try {
				initLog.appendTitle("Verification du dialogue avec le service d'impression");
				CktlPrinter printer = checkIfPrintServiceAvailable(config());
				String sixServiceHost = getParam("SIX_SERVICE_HOST");
				String sixServicePort = getParam("SIX_SERVICE_PORT");
				for (int i = 0; i < maquettesSix().length; i++) {
					try {
						String maquetteSix = getParam(maquettesSix()[i]);
						checkIfReportsExists(sixServiceHost, sixServicePort, printer, maquettesSix()[i], maquetteSix);
						initLog.appendSuccess("Le modele d'impression reference par le parametre " + maquettesSix()[i] + " (=" + maquetteSix + ") est accessible");
					} catch (Exception e) {
						ok = false;
						initLog.appendWarning(e.getMessage());
					}
				}
			} catch (Exception e) {
				ok = false;
				initLog.appendWarning(e.getMessage());
			}
		}

		return ok;
	}

	/**
	 * Vérifie la disponibilité du service d'impression.
	 */
	private CktlPrinter checkIfPrintServiceAvailable(CktlConfig config) throws Exception {
		CktlPrinter printer = null;
		try {
			printer = CktlPrinter.newDefaultInstance(config);
			initLog.appendSuccess("Le moteur d'impression est accessible");
		} catch (ClassNotFoundException e) {
			throw new Exception("La classe necessaire a l'appel au service d'impression n'est pas accessible (parametre XML_PRINTER_DRIVER). Les frameworks ne doivent pas etre a jour.");
		} catch (Exception e1) {
			throw new Exception("Impossible d'initialiser le client du service d'impression. Certains parametres peuvent contenir des valeurs erronees (parametres commencant par SIX_).");
		}

		if (printer != null) {
			Dictionary dicoprint = printer.checkService();
			if (dicoprint == null) {
				throw new Exception("Le service d'impression est indisponible.");
			}
			initLog.appendSuccess("Le service d'impression est disponible.");
			initLog.appendKeyValue("SERVICE_NAME", dicoprint.get(CktlPrintConst.SERVICE_NAME_KEY));
			initLog.appendKeyValue("SERVICE_VERSION_KEY", dicoprint.get(CktlPrintConst.SERVICE_VERSION_KEY));
			initLog.appendKeyValue("SERVICE_DESCRIPTION_KEY", dicoprint.get(CktlPrintConst.SERVICE_DESCRIPTION_KEY));
			initLog.append("");
			return printer;
		}
		return null;
	}

	/**
	 * Vérifie si le modéle d'impression est bien disponible.
	 */
	private void checkIfReportsExists(String sixServiceHost, String sixServicePort, CktlPrinter printer, String maquetteParam, String maquetteID) throws Exception {
		if (!printer.checkTemplate(maquetteID)) {
			switch (printer.getErrorCode()) {
			case CktlPrintConst.ERR_CONNECT:
				throw new Exception("Le service SIX " + sixServiceHost + " : " + sixServicePort + " n'est pas disponible.");

			case CktlPrintConst.ERR_NO_TEMPLATE:
				throw new Exception("Le modele d'impression " + maquetteID + " (reference par le parametre " + maquetteParam + ") n'est pas reference dans la base de donnees du service d'impression.");

			case CktlPrintConst.ERR_OK:
				break;

			default:
				throw new Exception("Une erreur est survenue lors d'une operation SIX. Code erreur : " + printer.getErrorCode());
			}
		}
	}

	/**
	 * Récupére le contenu du fichier filePath
	 */
	public String fileContent(String filePath) {
		try {
			File f = new File(filePath);
			int size = (int) f.length();
			FileInputStream in = new FileInputStream(f);
			byte[] data = new byte[size];
			in.read(data);
			in.close();
			return new String(data);
		} catch (FileNotFoundException fnfe) {
			System.err.println("Fichier " + filePath + " introuvable !");
			return "";
		} catch (IOException ioe) {
			System.err.println("Erreur lors de la lecture du fchier " + filePath + " : " + ioe);
			return "";
		}
	}

	public String getReportsLocation() {
		String param = getParam("REPORTS_LOCATION");
		if (param != null) {
			return param;
		}
		final String aPath = path().concat("/Contents/Resources/reports/");
		return aPath;
	}
}