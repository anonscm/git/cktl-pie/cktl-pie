/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.server.avirer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.Date;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;

public class CktlMailMessage {
	private Session		session;
	private String		encoding;
	private MimeMessage	message;
	private Vector		addrsTO;
	private Vector		addrsCC;
	private Vector		addrsBCC;
	private String		contentType;

	public CktlMailMessage(String mailServer) {
		Properties props = System.getProperties();
		props.put("mail.smtp.host", mailServer);
		session = Session.getDefaultInstance(props, null);
		encoding = "iso-8859-1";
		contentType = null;
		addrsTO = new Vector();
		addrsCC = new Vector();
		addrsBCC = new Vector();
	}

	public void initMessage(String from, String to, String subject, String body, String attachements[]) throws MessagingException,
			AddressException {
		initMessage(from, to, subject, body, attachements, false);
	}

	public void initMessage(String from, String to, String subject, String body, String attachements[], boolean useHTML)
			throws MessagingException, AddressException {
		createNewMimeMessage(from, to, subject);
		final MimeMultipart related = new MimeMultipart("related");
		final MimeMultipart attachment = new MimeMultipart("mixed");
		final MimeMultipart alternative = new MimeMultipart("alternative");
		final boolean html = useHTML;
		if (attachements != null)
			setAttachmentPart(attachements, related, attachment, body, html);
		setHtmlText(related, alternative, body, html);
		setContent(message, alternative, attachment, body);
	}

	public void initMessage(String from, String to, String subject, String body, String attachNames[], byte attachData[][])
			throws MessagingException, AddressException {
		initMessage(from, to, subject, body, attachNames, attachData, false);
	}

	public void initMessage(String from, String to, String subject, String body, String attachNames[], byte attachData[][], boolean useHTML)
			throws MessagingException, AddressException {
		createNewMimeMessage(from, to, subject);
		final MimeMultipart related = new MimeMultipart("related");
		final MimeMultipart attachment = new MimeMultipart("mixed");
		final MimeMultipart alternative = new MimeMultipart("alternative");
		final boolean html = useHTML;
		if (attachNames != null)
			setAttachmentPart(attachData, attachNames, related, attachment, body, html);
		setHtmlText(related, alternative, body, html);
		setContent(message, alternative, attachment, body);
	}

	private void setAttachmentPart(final byte attachData[][], final String attachNames[], final MimeMultipart related,
			final MimeMultipart attachment, final String body, final boolean htmlText) throws MessagingException {
		for (int i = 0; i < attachData.length; ++i) {
			// creation du fichier a inclure
			final MimeBodyPart messageFilePart = new MimeBodyPart();
			final String fileName = attachNames[i];
			final DataSource source = new ByteArrayDataSource(attachData[i], fileName);
			messageFilePart.setDataHandler(new DataHandler(source));
			messageFilePart.setFileName(fileName);
			// Image a inclure dans un texte au format HTML ou piece jointe
			if (htmlText && null != body && body.matches(".*<img[^>]*src=[\"|']?cid:([\"|']?" + fileName + "[\"|']?)[^>]*>.*")) {
				// " <-- pour eviter une coloration synthaxique desastreuse...
				messageFilePart.setDisposition("inline");
				messageFilePart.setHeader("Content-ID", '<' + fileName + '>');
				related.addBodyPart(messageFilePart);
			}
			else {
				messageFilePart.setDisposition("attachment");
				attachment.addBodyPart(messageFilePart);
			}
		}
	}

	private void setHtmlText(final MimeMultipart related, final MimeMultipart alternative, final String body, final boolean useHtml)
			throws MessagingException {
		// Plain text
		final BodyPart plainText = new MimeBodyPart();
		plainText.setContent(HtmlToText(body), "text/plain; charset=" + encoding);
		alternative.addBodyPart(plainText);
		// Html text ou Html text + images incluses
		if (useHtml) {
			final BodyPart htmlText = new MimeBodyPart();
			htmlText.setContent(body, "text/html; charset=" + encoding);
			if (0 != related.getCount()) {
				related.addBodyPart(htmlText, 0);
				final MimeBodyPart tmp = new MimeBodyPart();
				tmp.setContent(related);
				alternative.addBodyPart(tmp);
			}
			else {
				alternative.addBodyPart(htmlText);
			}
		}
	}

	// definition du corps de l'e-mail
	private void setContent(final MimeMessage message, final MimeMultipart alternative, final MimeMultipart attachment, final String body)
			throws MessagingException {
		if (0 != attachment.getCount()) {
			// Contenu mixte: Pieces jointes + texte
			if (0 != alternative.getCount() || null != body) {
				// Texte alternatif = texte + texte html
				final MimeBodyPart tmp = new MimeBodyPart();
				tmp.setContent(alternative);
				attachment.addBodyPart(tmp);
			}
			else {
				// Juste du texte
				final BodyPart plainText = new MimeBodyPart();
				plainText.setContent(body, "text/plain; " + encoding);
				attachment.addBodyPart(plainText, 0);
			}
			message.setContent(attachment);
		}
		else {
			// Juste un message texte
			if (0 != alternative.getCount()) {
				// Texte alternatif = texte + texte html
				message.setContent(alternative);
			}
			else {
				// Texte
				message.setText(body);
			}
		}
	}

	// defini les fichiers a joindre
	private void setAttachmentPart(final String[] attachmentPaths, final MimeMultipart related, final MimeMultipart attachment,
			final String body, final boolean htmlText) throws MessagingException {
		for (int i = 0; i < attachmentPaths.length; ++i) {
			// creation du fichier a inclure
			final MimeBodyPart messageFilePart = new MimeBodyPart();
			final DataSource source = new FileDataSource(attachmentPaths[i]);
			final String fileName = source.getName();
			messageFilePart.setDataHandler(new DataHandler(source));
			messageFilePart.setFileName(fileName);
			// Image a inclure dans un texte au format HTML ou piece jointe
			if (htmlText && null != body && body.matches(".*<img[^>]*src=[\"|']?cid:([\"|']?" + fileName + "[\"|']?)[^>]*>.*")) {
				// " <-- pour eviter une coloration synthaxique desastreuse...
				messageFilePart.setDisposition("inline");
				messageFilePart.setHeader("Content-ID", '<' + fileName + '>');
				related.addBodyPart(messageFilePart);
			}
			else {
				messageFilePart.setDisposition("attachment");
				attachment.addBodyPart(messageFilePart);
			}
		}
	}

	public void initMessage(String from, String to, String subject, String body, boolean useHTML) throws MessagingException, AddressException {
		initMessage(from, to, subject, body, null, null, useHTML);
	}

	public void initMessage(String from, String to, String subject, String body) throws MessagingException, AddressException {
		initMessage(from, to, subject, body, false);
	}

	private void createNewMimeMessage(String from, String to, String subject) throws MessagingException, AddressException {
		message = new MimeMessage(session);
		message.setFrom(new InternetAddress(from));
		String atos[] = toArray(to);
		if (atos.length == 0) {
			throw new AddressException("Addresse TO not specified");
		}
		for (int i = 0; i < atos.length; i++) {
			addTO(atos[i]);
		}

		message.setSubject(subject, encoding);
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public void addCCs(String cc[]) throws AddressException {
		if (cc != null) {
			for (int i = 0; i < cc.length; i++) {
				addCC(cc[i]);
			}

		}
	}

	public void addCC(String aCC) throws AddressException {
		addrsCC.addElement(new InternetAddress(aCC));
	}

	public void addBCCs(String bcc[]) throws AddressException {
		if (bcc != null) {
			for (int i = 0; i < bcc.length; i++) {
				addBCC(bcc[i]);
			}

		}
	}

	public void addBCC(String aBCC) throws AddressException {
		addrsBCC.addElement(new InternetAddress(aBCC));
	}

	public void addTO(String aTO) throws AddressException {
		addrsTO.addElement(new InternetAddress(aTO));
	}

	public void removeAddress(String address) {
		try {
			InternetAddress addr = new InternetAddress(address);
			addrsBCC.removeElement(addr);
			addrsCC.removeElement(addr);
			addrsTO.removeElement(addr);
		}
		catch (AddressException addressexception) {
		}
	}

	public void setReplyTo(String address) throws MessagingException {
		InternetAddress addrList[] = (InternetAddress[]) null;
		if (address != null) {
			addrList = new InternetAddress[1];
			addrList[0] = new InternetAddress(address);
			message.setReplyTo(null);
		}
		message.setReplyTo(addrList);
	}

	public void setHeader(String name, String value) throws MessagingException {
		message.setHeader(name, value);
	}

	private void resetAddresses(Vector addresses, javax.mail.Message.RecipientType type) throws MessagingException {
		message.setRecipients(type, (Address[]) null);
		for (int i = 0; i < addresses.size(); i++) {
			message.addRecipient(type, (InternetAddress) addresses.elementAt(i));
		}
	}

	public void send() throws MessagingException, SendFailedException {
		resetAddresses(addrsTO, javax.mail.Message.RecipientType.TO);
		resetAddresses(addrsCC, javax.mail.Message.RecipientType.CC);
		resetAddresses(addrsBCC, javax.mail.Message.RecipientType.BCC);
		message.setSentDate(new Date());
		Transport.send(message);
	}

	public Vector safeSend() throws MessagingException, SendFailedException {
		Vector failedAddrs = new Vector();
		try {
			send();
		}
		catch (SendFailedException ex) {
			Address addrs[] = ex.getInvalidAddresses();
			if (addrs != null) {
				for (int i = 0; i < addrs.length; i++) {
					failedAddrs.addElement(addrs[i].toString());
					removeAddress(addrs[i].toString());
				}

				send();
			}
			else {
				throw ex;
			}
		}
		return failedAddrs;
	}

	private static Vector toVector(String strings, String separator) {
		return toVector(strings, separator, true);
	}

	private static Vector toVector(String strings, String separator, boolean ignoreEmptyStrings) {
		Vector v = new Vector();
		if (strings != null) {
			for (StringTokenizer st = new StringTokenizer(strings, separator); st.hasMoreTokens();) {
				String s = st.nextToken().trim();
				if (!ignoreEmptyStrings || s != null && !s.equals("")) {
					v.addElement(s);
				}
			}

		}
		return v;
	}

	private static String[] toArray(Vector strings) {
		if (strings == null) {
			return null;
		}
		else {
			String sv[] = new String[strings.size()];
			strings.copyInto(sv);
			return sv;
		}
	}

	public static String[] toArray(String aString) {
		if (aString == null) {
			return null;
		}
		else {
			return toArray(toVector(aString, ","));
		}
	}

	public static String getNameFromPath(String filePath) {
		for (; filePath.endsWith("/") || filePath.endsWith("\\"); filePath = filePath.substring(0, filePath.length() - 1)) {
		}
		int n = filePath.lastIndexOf("/");
		int m = filePath.lastIndexOf("\\");
		if ((n != -1 || m != -1) && m > n) {
			n = m;
		}
		return filePath.substring(n + 1);
	}

	/*
	 * private void traceMessage() throws Exception { System.out.println("--------Header Message------"); Enumeration header =
	 * message.getAllHeaderLines(); while (header.hasMoreElements()) { String element = (String) header.nextElement();
	 * System.out.println(element); } System.out.println("--------Content Message------"); System.out.println("getContentType=" +
	 * message.getContentType()); System.out.println("getContentID=" + message.getContentID()); System.out.println("getContentLanguage=" +
	 * message.getContentLanguage()); System.out.println("getContentMD5=" + message.getContentMD5()); System.out.println("getDataHandler=" +
	 * message.getDataHandler()); System.out.println("getDescription=" + message.getDescription()); System.out.println("getDisposition=" +
	 * message.getDisposition()); System.out.println("getEncoding=" + message.getEncoding()); System.out.println("getFileName=" +
	 * message.getFileName()); System.out.println("getMessageID=" + message.getMessageID()); System.out.println("getMessageNumber=" +
	 * message.getMessageNumber()); System.out.println("getSize=" + message.getSize()); System.out.println("getSubject=" + message.getSubject());
	 * System.out.println("getFrom=" + message.getFrom()); System.out.println("getInputStream=" + message.getInputStream());
	 * System.out.println("getReceivedDate=" + message.getReceivedDate()); System.out.println("getLineCount=" + message.getLineCount());
	 * System.out.println("getFlags=" + message.getFlags()); System.out.println("getFolder=" + message.getFolder());
	 * System.out.println("getReplyTo=" + message.getReplyTo()); System.out.println("getSentDate=" + message.getSentDate());
	 * System.out.println("getAllRecipients=" + message.getAllRecipients()); }
	 */

	/**
	 * Convertit un texte au format html en texte brut
	 */
	public static final String HtmlToText(final String s) {
		final HTMLEditorKit kit = new HTMLEditorKit();
		final Document doc = kit.createDefaultDocument();
		try {
			kit.read(new StringReader(s), doc, 0);
			return doc.getText(0, doc.getLength()).trim();
		}
		catch (final IOException ioe) {
			return s;
		}
		catch (final BadLocationException ble) {
			return s;
		}
	}

	public class ByteArrayDataSource implements DataSource {

		private byte	data[];

		private String	name;

		private String	contentType;

		public String getContentType() {
			return contentType;
		}

		public InputStream getInputStream() throws IOException {
			if (data == null) {
				throw new IOException("Data source is null");
			}
			else {
				return new ByteArrayInputStream(data);
			}
		}

		public String getName() {
			return name;
		}

		public OutputStream getOutputStream() throws IOException {
			if (data == null) {
				throw new IOException("Data source is null");
			}
			else {
				return new ByteArrayOutputStream(data.length);
			}
		}

		public ByteArrayDataSource(byte data[], String name, String contentType) {
			this.data = data;
			this.name = name;
			this.contentType = contentType;
		}

		public ByteArrayDataSource(byte data[], String name) {
			this(data, name, "application/octet-stream");
		}
	}

}
