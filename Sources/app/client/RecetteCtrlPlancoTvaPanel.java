/*
CRI - Universite de La Rochelle, 2006

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.awt.Dimension;
import java.math.BigDecimal;

import javax.swing.JTextField;

import org.cocktail.kava.client.factory.FactoryRecetteCtrlPlancoTva;
import org.cocktail.kava.client.finder.FinderPlancoVisa;
import org.cocktail.kava.client.metier.EOGestion;
import org.cocktail.kava.client.metier.EOPlanComptable;
import org.cocktail.kava.client.metier.EOPlancoVisa;
import org.cocktail.kava.client.metier.EORecetteCtrlPlanco;
import org.cocktail.kava.client.metier.EORecetteCtrlPlancoTva;

import app.client.select.PlanComptableTvaSelect;
import app.client.ui.table.ZEOTableModelColumn;
import app.client.ui.table.ZExtendedTablePanel;

import com.webobjects.foundation.NSArray;

public class RecetteCtrlPlancoTvaPanel extends CtrlPanel {

	private static final boolean	DEFAULT_MULTIPLE_INIT			= true;	// demarre par defaut en multiple ou single ?
	private static final boolean	DEFAULT_MULTIPLE_INIT_LOCKED	= false;	// interdit ou non le changement

	private ZEOTableModelColumn		colTva;

	private PlanComptableTvaSelect	planComptableTvaSelect;

	private EORecetteCtrlPlanco		currentObject;

	public RecetteCtrlPlancoTvaPanel() {
		super("Compte d'imputation TVA", 25);
		planComptableTvaSelect = new PlanComptableTvaSelect();
	}

	public void setEditable(boolean editable) {
		colTva.setEditable(editable);
		super.setEditable(editable);
	}

	public void setColumnsEditable(boolean editable) {
		colTva.setEditable(editable);
	}

	public void setCurrentObject(EORecetteCtrlPlanco object) {
		this.currentObject = object;
		updateData();
	}

	public void add(EOPlanComptable eo) {
		if (currentObject == null || eo == null) {
			return;
		}
		// si on est en mode multiple ou qu'il n'y a pas encore de ctrl, on ajoute
		if (multipleEnable || currentObject.recetteCtrlPlancoTvas() == null || currentObject.recetteCtrlPlancoTvas().count() == 0) {
			EORecetteCtrlPlancoTva object = FactoryRecetteCtrlPlancoTva.newObject(ec);
			object.setPlanComptableRelationship(eo);
			object.setExerciceRelationship(currentObject.exercice());
			object.setGestionRelationship(getGestion(currentObject));
			setMontant(object);
			if (object.rpcotvaTvaSaisie() == null || object.rpcotvaTvaSaisie().compareTo(new BigDecimal(0.0)) == 0) {
				ec.deleteObject(object);
			}
			else {
				currentObject.addToRecetteCtrlPlancoTvasRelationship(object);
			}
		}
		else {
			// on est en mode single et y'a deja un ctrl, on le modifie
			((EORecetteCtrlPlancoTva) currentObject.recetteCtrlPlancoTvas().objectAtIndex(0)).setPlanComptableRelationship(eo);
			setMontant((EORecetteCtrlPlancoTva) currentObject.recetteCtrlPlancoTvas().objectAtIndex(0));
			EORecetteCtrlPlancoTva object = (EORecetteCtrlPlancoTva) currentObject.recetteCtrlPlancoTvas().objectAtIndex(0);
			if (object.rpcotvaTvaSaisie() == null || object.rpcotvaTvaSaisie().compareTo(new BigDecimal(0.0)) == 0) {
				currentObject.removeFromRecetteCtrlPlancoTvasRelationship(object);
				ec.deleteObject(object);
			}
		}
		updateData();
	}

	protected void onAdd() {
		if (planComptableTvaSelect.open(currentObject.recette().utilisateur(), currentObject.exercice(), null)) {
			try {
				add(planComptableTvaSelect.getSelected());
			}
			catch (Exception e) {
				System.err.println("Erreur lors de la tentative d'insertion d'un nouvel objet RecetteCtrlPlancoTva: " + e);
			}
		}
	}

	protected void onSelect() {
		EOPlanComptable current = null;
		if (currentObject.recetteCtrlPlancoTvas() != null && currentObject.recetteCtrlPlancoTvas().count() > 0) {
			current = ((EORecetteCtrlPlancoTva) currentObject.recetteCtrlPlancoTvas().objectAtIndex(0)).planComptable();
		}
		if (planComptableTvaSelect.open(currentObject.recette().utilisateur(), currentObject.exercice(), current)) {
			try {
				add(planComptableTvaSelect.getSelected());
			}
			catch (Exception e) {
				System.err.println("Erreur lors de la tentative d'insertion/modification d'un nouvel objet RecetteCtrlPlancoTva: " + e);
			}
		}
	}

	protected void onUpdate() {
		if (table.selectedObject() != null) {
			if (planComptableTvaSelect.open(currentObject.recette().utilisateur(), currentObject.exercice(), ((EORecetteCtrlPlancoTva) table
					.selectedObject()).planComptable())) {
				((EORecetteCtrlPlancoTva) table.selectedObject()).setPlanComptableRelationship(planComptableTvaSelect.getSelected());
				table.updateUI();
			}
		}
	}

	protected void onDelete() {
		if (table.selectedObject() != null && app.showConfirmationDialog("ATTENTION", "Supprimer cette ligne??", "OUI", "NON")) {
			currentObject.removeFromRecetteCtrlPlancoTvasRelationship((EORecetteCtrlPlancoTva) table.selectedObject());
			ec.deleteObject(table.selectedObject());
			updateData();
		}
	}

	private void setMontant(EORecetteCtrlPlancoTva ctrl) {
		// on fixe la tva par rapport soit au restant du montant tva de la recette planco, soit au montant tva de la recette planco
		BigDecimal newTva = null;
		if (currentObject.rpcoTtcSaisie() != null && currentObject.rpcoHtSaisie() != null) {
			newTva = currentObject.rpcoTtcSaisie().subtract(currentObject.rpcoHtSaisie());
		}
		if (multipleEnable && newTva != null) {
			newTva = newTva.subtract((BigDecimal) currentObject.recetteCtrlPlancoTvas().valueForKey(
					"@sum." + EORecetteCtrlPlancoTva.RPCOTVA_TVA_SAISIE_KEY));
		}
		if (newTva == null || newTva.doubleValue() < 0.0) {
			newTva = new BigDecimal(0.0);
		}
		ctrl.setRpcotvaTvaSaisie(newTva);
		table.updateData();
	}

	private EOGestion getGestion(EORecetteCtrlPlanco ctrlPlanco) {
		EOGestion gestion = null;
		try {
			gestion = ctrlPlanco.recette().facture().organ().gestion();
			if (!gestion.isSacd(ctrlPlanco.exercice())) {
				NSArray array=FinderPlancoVisa.find(ctrlPlanco.editingContext(), ctrlPlanco.planComptable(), ctrlPlanco.exercice());
				if (array!=null && array.count()==1 && !"COMPOSANTE".equalsIgnoreCase(((EOPlancoVisa)array.objectAtIndex(0)).pviContrepartieGestion()))
					gestion = gestion.comptabilite().gestion();
			}
		}
		catch (Exception e) {
			gestion = null;
		}
		return gestion;
	}

	public void updateMontant() {
		if (currentObject == null || currentObject.recetteCtrlPlancoTvas() == null || currentObject.recetteCtrlPlancoTvas().count() != 1) {
			return;
		}
		BigDecimal newTva = null;
		if (currentObject.rpcoTtcSaisie() != null && currentObject.rpcoHtSaisie() != null) {
			newTva = currentObject.rpcoTtcSaisie().subtract(currentObject.rpcoHtSaisie());
		}
		((EORecetteCtrlPlancoTva) currentObject.recetteCtrlPlancoTvas().objectAtIndex(0)).setRpcotvaTvaSaisie(newTva);
		table.updateData();
	}

	protected void updateData() {
		if (currentObject == null) {
			table.setObjectArray(null);
			tfCtrl.clean();
			setEditable(false);
		}
		else {
			setEditable(true);
			if (multipleEnable) {
				table.setObjectArray(currentObject.recetteCtrlPlancoTvas());
			}
			else {
				if (currentObject.recetteCtrlPlancoTvas() == null || currentObject.recetteCtrlPlancoTvas().count() == 0) {
					tfCtrl.clean();
				}
				else {
					// on vire ceux qui sont en trop ! et on en garde un... le dernier...
					while (currentObject.recetteCtrlPlancoTvas().count() > 1) {
						EORecetteCtrlPlancoTva ctrl = (EORecetteCtrlPlancoTva) currentObject.recetteCtrlPlancoTvas().objectAtIndex(0);
						currentObject.removeFromRecetteCtrlPlancoTvasRelationship(ctrl);
						ec.deleteObject(ctrl);
					}
					EORecetteCtrlPlancoTva ctrl = (EORecetteCtrlPlancoTva) currentObject.recetteCtrlPlancoTvas().objectAtIndex(0);
					setMontant(ctrl);
					tfCtrl.clean();
					if (ctrl.planComptable() != null) {
						tfCtrl.setText(ctrl.planComptable().pcoNum() + " - " + ctrl.planComptable().pcoLibelle());
					}
				}
			}
		}
	}

	private final class TablePanel extends ZExtendedTablePanel {

		public TablePanel() {
			super("Comptes d'imputation TVA");

			final ZEOTableModelColumn.ZEONumFieldTableCellEditor _editor = new ZEOTableModelColumn.ZEONumFieldTableCellEditor(new JTextField(),
					app.getCurrencyFormatEdit());

			addCol(new ZEOTableModelColumn(getDG(), EORecetteCtrlPlancoTva.PLAN_COMPTABLE_KEY + "." + EOPlanComptable.PCO_NUM_KEY, "Code", 40));
			addCol(new ZEOTableModelColumn(getDG(), EORecetteCtrlPlancoTva.PLAN_COMPTABLE_KEY + "." + EOPlanComptable.PCO_LIBELLE_KEY, "Lib",
					120));
			colTva = new ZEOTableModelColumn(getDG(), EORecetteCtrlPlancoTva.RPCOTVA_TVA_SAISIE_KEY, "Tva", 70, app.getCurrencyFormatDisplay());
			colTva.setEditable(true);
			colTva.setTableCellEditor(_editor);
			colTva.setFormatEdit(app.getCurrencyFormatEdit());
			colTva.setColumnClass(BigDecimal.class);
			addCol(colTva);

			initGUI();

			setPreferredSize(new Dimension(0, 70));
		}

		protected void onAdd() {
			RecetteCtrlPlancoTvaPanel.this.onAdd();
		}

		protected void onUpdate() {
			RecetteCtrlPlancoTvaPanel.this.onUpdate();
		}

		protected void onDelete() {
			RecetteCtrlPlancoTvaPanel.this.onDelete();
		}

		protected void onSelectionChanged() {
		}

	}

	protected boolean defaultMultipleInit() {
		return DEFAULT_MULTIPLE_INIT;
	}

	protected boolean defaultMultipleInitLocked() {
		return DEFAULT_MULTIPLE_INIT_LOCKED;
	}

	protected ZExtendedTablePanel getTablePanel() {
		return new TablePanel();
	}

}
