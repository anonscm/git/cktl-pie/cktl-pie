/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.kava.client.factory.FactoryFacture;
import org.cocktail.kava.client.factory.FactoryRecette;
import org.cocktail.kava.client.factory.FactoryRecettePapier;
import org.cocktail.kava.client.factory.FactoryRecettePapierAdrClient;
import org.cocktail.kava.client.finder.FinderModeRecouvrement;
import org.cocktail.kava.client.finder.FinderPlancoVisa;
import org.cocktail.kava.client.finder.FinderRecette;
import org.cocktail.kava.client.finder.FinderTva;
import org.cocktail.kava.client.finder.FinderTypeApplication;
import org.cocktail.kava.client.finder.FinderTypeCreditRec;
import org.cocktail.kava.client.metier.EOAdresse;
import org.cocktail.kava.client.metier.EOFacture;
import org.cocktail.kava.client.metier.EOFacturePapier;
import org.cocktail.kava.client.metier.EOFonction;
import org.cocktail.kava.client.metier.EOModeRecouvrement;
import org.cocktail.kava.client.metier.EOPlanComptable;
import org.cocktail.kava.client.metier.EORecette;
import org.cocktail.kava.client.metier.EORecetteCtrlPlanco;
import org.cocktail.kava.client.metier.EORecettePapier;
import org.cocktail.kava.client.metier.EORecettePapierAdrClient;
import org.cocktail.kava.client.metier.EOTva;
import org.cocktail.kava.client.metier.EOUtilisateur;
import org.cocktail.kava.client.procedures.Api;
import org.cocktail.kava.client.service.RecettePapierService;
import org.cocktail.pieFwk.common.exception.EntityInitializationException;

import app.client.select.ClientSelect;
import app.client.select.OrganSelect;
import app.client.select.RibfourUlrSelect;
import app.client.select.validator.FournisseurEtatValideValidator;
import app.client.select.validator.SelectValidatorSeverity;
import app.client.tools.Computation;
import app.client.tools.Constants;
import app.client.ui.DisabledGlassPane;
import app.client.ui.UIUtilities;
import app.client.ui.ZEOComboBox;
import app.client.ui.ZNumberField;
import app.client.ui.ZTextField;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

public class FactureRecetteAddUpdate extends JDialog implements IRecetteCtrlPlanco {

	private static final String WINDOW_TITLE = "Recette";

	protected ApplicationClient app;
	protected EOEditingContext ec;

	private static FactureRecetteAddUpdate sharedInstance;

	private EORecette eoRecette;

	private Action actionClose, actionValid, actionCancel;
	private Action actionShowRequired, actionUnshowRequired;
	private Action actionPersonneSelect, actionOrganSelect;
	private Action actionCheckModeOuhlala;
	private Action actionRibfourUlrSelect, actionRibfourUlrDelete;
	private Action actionCbCalculAutoHt;
	private Action actionCbCalculAutoTtc;

	private ZNumberField tfRecNumero;
	private ZTextField tfExercice, tfRecLib;
	private ZNumberField tfRecHtSaisie, tfRecTtcSaisie;
	private ZNumberField tfNbPiece;
	private ZEOComboBox cbTva;
	private JCheckBox cbCalculAutoHt, cbCalculAutoTtc;

	private ZTextField tfPersonne, tfRibClient, tfOrgan;
	private ZEOComboBox cbTypeCreditRec, cbModeRecouvrement;
	private CbActionListener cbActionListener;

	private JCheckBox checkBoxModeOuhlala;

	private RecetteCtrlActionPanel recetteCtrlAction;
	private RecetteCtrlAnalytiquePanel recetteCtrlAnalytique;
	private RecetteCtrlConventionPanel recetteCtrlConvention;
	private RecetteCtrlPlancoPanel recetteCtrlPlanco;
	private RecetteCtrlPlancoTvaPanel recetteCtrlPlancoTva;
	private RecetteCtrlPlancoCtpPanel recetteCtrlPlancoCtp;

	private JButton btValid, btCancel;

	private ClientSelect personneSelect;
	private OrganSelect organSelect;
	private RibfourUlrSelect ribfourUlrSelect;

	private Number recIdUpdating;
	private boolean updating;
	private boolean result;

	private EOAdresse adresseClientSelected;

	private RecettePapierService recettePapierService = RecettePapierService.instance();

	public FactureRecetteAddUpdate() {
		super((JFrame) null, WINDOW_TITLE, true);

		app = (ApplicationClient) EOApplication.sharedApplication();
		ec = app.editingContext();

		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new LocalWindowListener());

		actionClose = new ActionClose();
		actionValid = new ActionValid();
		actionCancel = new ActionCancel();
		actionShowRequired = new ActionShowRequired();
		actionUnshowRequired = new ActionUnshowRequired();
		actionPersonneSelect = new ActionPersonneSelect();
		actionRibfourUlrSelect = new ActionRibfourUlrSelect();
		actionRibfourUlrDelete = new ActionRibfourUlrDelete();
		actionOrganSelect = new ActionOrganSelect();
		actionCheckModeOuhlala = new ActionCheckModeOuhlala();
		actionCbCalculAutoHt = new ActionCheckCalculAutoHT();
		actionCbCalculAutoTtc = new ActionCheckCalculAutoTTC();

		tfRecNumero = new ZNumberField(4);
		tfExercice = new ZTextField(4);
		tfRecLib = new ZTextField(50);

		tfRecHtSaisie = new ZNumberField(10, app.getCurrencyFormatEdit());
		tfRecTtcSaisie = new ZNumberField(10, app.getCurrencyFormatEdit());
		tfNbPiece = new ZNumberField(8, Constants.FORMAT_INTEGER);

		cbTva = new ZEOComboBox(FinderTva.find(ec), EOTva.TVA_TAUX_KEY, "", Constants.FORMAT_DECIMAL_DISPLAY, null, 80);
		cbTva.addActionListener(cbActionListener);

		cbCalculAutoHt = new JCheckBox(actionCbCalculAutoHt);
		cbCalculAutoTtc = new JCheckBox(actionCbCalculAutoTtc);

		tfPersonne = new ZTextField(40);
		tfPersonne.setViewOnly();
		tfRibClient = new ZTextField(30);
		tfRibClient.setViewOnly();
		tfOrgan = new ZTextField(20);
		tfOrgan.setViewOnly();

		cbActionListener = new CbActionListener();
		cbTypeCreditRec = new ZEOComboBox(new NSArray(), EOTypeCredit.LIB_KEY, null, null, null, 150);
		cbTypeCreditRec.addActionListener(cbActionListener);
		cbModeRecouvrement = new ZEOComboBox(new NSArray(), EOModeRecouvrement.LIB_KEY, null, null, null, 220);
		cbModeRecouvrement.addActionListener(cbActionListener);

		checkBoxModeOuhlala = new JCheckBox(actionCheckModeOuhlala);

		recetteCtrlAction = new RecetteCtrlActionPanel();
		recetteCtrlAnalytique = new RecetteCtrlAnalytiquePanel();
		recetteCtrlConvention = new RecetteCtrlConventionPanel();
		recetteCtrlPlanco = new RecetteCtrlPlancoPanel(this);
		recetteCtrlPlancoTva = new RecetteCtrlPlancoTvaPanel();
		recetteCtrlPlancoCtp = new RecetteCtrlPlancoCtpPanel();

		btCancel = new JButton(actionCancel);
		btValid = new JButton(actionValid);

		initGUI();

		cbCalculAutoTtc.setSelected(true);
		checkBoxModeOuhlala.getAction().actionPerformed(null);
	}

	public static FactureRecetteAddUpdate sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new FactureRecetteAddUpdate();
		}
		return sharedInstance;
	}

	public boolean openNew(EOExercice exercice) throws EntityInitializationException {
		if (exercice == null) {
			System.err.println("[FactureRecetteAddUpdate:open(EOExercice)] Aucun exercice pass\u00E9 pour ouvrir en cr\u00E9ation !");
			throw new EntityInitializationException("Aucun exercice passé pour ouvrir en création");
		}

		updating = false;
		recIdUpdating = null;

		setTitle(WINDOW_TITLE + " - Cr\u00E9ation");

		EOFacture eoFacture = FactoryFacture.newObject(ec);
		eoFacture.setExerciceRelationship(exercice);
		eoFacture.setUtilisateurRelationship(app.appUserInfo().utilisateur());
		eoFacture.setTypeApplicationRelationship(FinderTypeApplication.typeApplicationRecette(ec));

		eoRecette = FactoryRecette.newObject(ec, eoFacture);
		eoRecette.setUtilisateurRelationship(app.appUserInfo().utilisateur());

		EORecettePapier rpp = FactoryRecettePapier.newObject(ec, eoRecette);
		rpp.setUtilisateurRelationship(app.appUserInfo().utilisateur());
		eoRecette.setRecettePapierRelationship(rpp);

		updateData();
		return open();
	}

	public boolean open(EORecette recette) {
		if (recette == null) {
			System.err.println("[FactureRecetteAddUpdate:open(EORecette)] Aucune recette pass\u00E9e pour ouvrir en modif !");
			return false;
		}

		// v\u00E9rifie si on peut afficher cette recette sans perte de donn\u00E9es...
		if (recette.hasMultipleCtrls()) {
			if ((recette.hasMultipleCtrlActions() && !recetteCtrlAction.acceptMultiple())
					|| (recette.hasMultipleCtrlAnalytiques() && !recetteCtrlAnalytique.acceptMultiple())
					|| (recette.hasMultipleCtrlConventions() && !recetteCtrlConvention.acceptMultiple())
					|| (recette.hasMultipleCtrlPlancos() && !recetteCtrlPlanco.acceptMultiple())
					|| (recette.hasMultipleCtrlPlancoTvas() && !recetteCtrlPlancoTva.acceptMultiple())
					|| (recette.hasMultipleCtrlPlancoCtps() && !recetteCtrlPlancoCtp.acceptMultiple())) {
				app.showErrorDialog("Oups ! Impossible d'ouvrir cette recette sans perte de donn\u00E9es, donc on ne touche pas !");
				System.err.println("Plusieurs enregistrements existent pour un recetteCtrl qui est bloqu\u00E9 en mode single...");
				System.err.println("Ca n'arrive que si la configuration de l'application (ou du moteur sql) a chang\u00E9 depuis la cr\u00E9ation.");
				return false;
			}
		}

		updating = true;

		// on r\u00E9cup\u00E9re la cl\u00E9 tout de suite, passke apr\u00E9s s'il y a des relations nouvelles, donc qui n'existent pas
		// c\u00E9t\u00E9 serveur (les RecetteCtrl...), on ne peut plus passer l'objet recette au serveur pour r\u00E9cup\u00E9rer sa cl\u00E9
		NSDictionary dicoForPrimaryKeys = org.cocktail.kava.client.ServerProxy.primaryKeyForObject(ec, recette);
		recIdUpdating = (Number) dicoForPrimaryKeys.objectForKey(EORecette.PRIMARY_KEY_KEY);

		setTitle(WINDOW_TITLE + " - Modification");

		this.eoRecette = recette;
		this.eoRecette.setUtilisateurRelationship(app.appUserInfo().utilisateur());

		// si au moins un des ctrl a plus d'une ligne, on passe en mode ouhlala
		if (!checkBoxModeOuhlala.isSelected() && recette.hasMultipleCtrls()) {
			checkBoxModeOuhlala.setSelected(true);
			checkBoxModeOuhlala.getAction().actionPerformed(null);
		}
		updateData();
		return open();
	}

	public boolean canQuit() {
		if (isShowing()) {
			actionCancel.actionPerformed(null);
		}
		return !isShowing();
	}

	public EORecette getLastOpened() {
		return eoRecette;
	}

	// met \u00E9 jour les planco tva et ctp par d\u00E9faut pour le nouveau planco s\u00E9lectionn\u00E9
	public void didAddNewPlanco(EOPlanComptable planComptable, EOExercice exercice) {
		recetteCtrlPlancoTva.add(FinderPlancoVisa.findPlancoTva(ec, planComptable, exercice));
		recetteCtrlPlancoCtp.add(FinderPlancoVisa.findPlancoCtp(ec, eoRecette.recettePapier().modeRecouvrement(), planComptable));
		gereEtatRecetteCtrlPlancoCtp(recetteCtrlPlancoCtp.isEditable());
	}

	public void onSelectionChanged(EORecetteCtrlPlanco ctrlPlanco) {
		recetteCtrlPlancoTva.setCurrentObject(ctrlPlanco);
		recetteCtrlPlancoCtp.setCurrentObject(ctrlPlanco);
		gereEtatRecetteCtrlPlancoCtp(recetteCtrlPlancoCtp.isEditable());
	}

	private void gereEtatRecetteCtrlPlancoCtp(boolean editable) {
		recetteCtrlPlancoCtp.setEditable(editable && eoRecette != null
				&& !app.canUseFonction(EOFonction.DROIT_INTERDIT_SAISIE_CONTREPARTIE, eoRecette.exercice()));
	}

	private boolean open() {
		app.superviseur().setWaitCursor(this, true);
		result = false;
		adresseClientSelected = null;
		int screenWidth = (int) this.getGraphicsConfiguration().getBounds().getWidth();
		int screenHeight = (int) this.getGraphicsConfiguration().getBounds().getHeight();
		this.setLocation((screenWidth / 2) - ((int) this.getSize().getWidth() / 2),
				((screenHeight / 2) - ((int) this.getSize().getHeight() / 2)));

		app.superviseur().setWaitCursor(this, false);
		setVisible(true);
		return result;
	}

	private boolean echeancierExistePourFacture(EOFacture facture) {
		if (facture == null) {
			return false;
		}

		boolean echeancierExiste = false;

		// test lien avec echeancier national
		// puis test lien avec echeancier sepa 
		if (facture.echeId() != null) {
			echeancierExiste = true;
		}
		else if (facture.facturePapier() != null) {
			NSArray facturesPapiers = facture.facturePapier();
			if (facturesPapiers != null && facturesPapiers.count() == 1) {
				echeancierExiste = ((EOFacturePapier) facturesPapiers.objectAtIndex(0)).hasEcheancierSepa(ec);
			}
		}

		return echeancierExiste;
	}

	private void onValid() {
		try {
			app.superviseur().setWaitCursor(this, true);
			boolean echeancierExistePourFacture = echeancierExistePourFacture(eoRecette.facture());
			boolean recouvrementEcheancierPourCetteRecette = eoRecette.recettePapier() != null
					&& eoRecette.recettePapier().modeRecouvrement() != null
					&& eoRecette.recettePapier().modeRecouvrement().isEcheancier();
			boolean recouvrementNonEcheancierPourCetteRecette = eoRecette.recettePapier() != null
					&& eoRecette.recettePapier().modeRecouvrement() != null
					&& !eoRecette.recettePapier().modeRecouvrement().isEcheancier();
			if (recouvrementEcheancierPourCetteRecette && !echeancierExistePourFacture) {
				throw new Exception(
						"Pas de mode de recouvrement \u00E9ch\u00E9ancier sans \u00E9ch\u00E9ancier ! Passer par la facture pour cr\u00E9er un \u00E9ch\u00E9ancier !");
			}
			if (recouvrementNonEcheancierPourCetteRecette && echeancierExistePourFacture) {
				throw new Exception("Un \u00E9ch\u00E9ancier est associ\u00E9 \u00E0 la recette, il n'est donc pas possible de modifier le mode de recouvrement.");
			}

			if (recouvrementEcheancierPourCetteRecette && echeancierExistePourFacture) {
				app.showInfoDialog("Attention, un \u00E9ch\u00E9ancier est d\u00E9fini sur la facture associ\u00E9e, veillez \u00E0 conserver la coh\u00E9rence des montants.");
			}

			controlerCodeGestionContreparties();
			controlerTauxProrata();

			// avertissement en cas de presence d'un echeancier

			eoRecette.setRecLib(tfRecLib.getText());
			eoRecette.recettePapier().setRppNbPiece(Integer.valueOf(tfNbPiece.getNumber().intValue()));

			EORecettePapier eoRecettePapier = eoRecette.recettePapier();
			EORecettePapierAdrClient currentRppAdrCli = eoRecettePapier.currentRecPapierAdresseClient();
			EOAdresse currentAdresse = currentRppAdrCli == null ? null : currentRppAdrCli.toAdresse();
			if (adresseClientSelected != null && !adresseClientSelected.equals(currentAdresse)) {
				EORecettePapierAdrClient eoNewAdrClient = FactoryRecettePapierAdrClient.newObject(
						ec, app.getCurrentUtilisateur().personne(), eoRecettePapier, adresseClientSelected);
				eoRecettePapier.addToToRecettePapierAdrClientsRelationship(eoNewAdrClient);
				if (currentRppAdrCli != null) {
					// Tips : pour l'instant il est difficile de gerer un historique pour les recettes
					// par souci d'uniformite on applique la meme logique sur les prestations.
					// on a une table historique contenant 1 seule ligne.
					eoRecettePapier.removeFromToRecettePapierAdrClientsRelationship(currentRppAdrCli);
					ec.deleteObject(currentRppAdrCli);
				}
			}

			NSDictionary returnedDico = null;
			if (updating) {
				// on est en modification...
				Api.updFactureRecetteAdresse(ec, eoRecette, recIdUpdating);
			}
			else {
				// on est en cr\u00E9ation...
				returnedDico = Api.insFactureRecetteAdresse(ec, eoRecette);
			}
			ec.revert();
			result = true;
			if (returnedDico != null) {
				eoRecette = FinderRecette.findByPrimaryKey(ec, returnedDico.valueForKey("020aRecId"));
			}
			app.superviseur().setWaitCursor(this, false);
			setVisible(false);
		} catch (Exception e) {
			app.superviseur().setWaitCursor(this, false);
			app.showErrorDialog(e);
		} finally {
			ec.revert();
			app.superviseur().setWaitCursor(this, false);
		}
	}

	private void onCancel() {
		if (app.showConfirmationDialog("ATTENTION", "Annuler???", "OUI!", "NON")) {
			ec.revert();
			result = false;
			app.superviseur().setWaitCursor(this, false);
			setVisible(false);
		}
	}

	private void onClose() {
		actionCancel.actionPerformed(null);
	}

	private void onCheckModeOuhlala() {
		boolean modeOuhlala = checkBoxModeOuhlala.isSelected();

		// si on cherche \u00E9 passer en mode simple alors qu'il y a des multiples...
		if (!modeOuhlala && eoRecette != null && eoRecette.hasMultipleCtrls()) {
			// on v\u00E9rifie s'il y a un multiple sur un ctrl qui pourrait passer en single... si oui, NAN ! :-)
			if ((eoRecette.hasMultipleCtrlActions() && recetteCtrlAction.acceptSingle())
					|| (eoRecette.hasMultipleCtrlAnalytiques() && recetteCtrlAnalytique.acceptSingle())
					|| (eoRecette.hasMultipleCtrlConventions() && recetteCtrlConvention.acceptSingle())
					|| (eoRecette.hasMultipleCtrlPlancos() && recetteCtrlPlanco.acceptSingle())
					|| (eoRecette.hasMultipleCtrlPlancoTvas() && recetteCtrlPlancoTva.acceptSingle())
					|| (eoRecette.hasMultipleCtrlPlancoCtps() && recetteCtrlPlancoCtp.acceptSingle())) {
				app.showInfoDialog("Vous ne pouvez pas passer en mode simple; des informations seraient perdues !");
				checkBoxModeOuhlala.setSelected(true);
				return;
			}
		}

		recetteCtrlAction.setMultiple(modeOuhlala);
		recetteCtrlAnalytique.setMultiple(modeOuhlala);
		recetteCtrlConvention.setMultiple(modeOuhlala);
		recetteCtrlPlanco.setMultiple(modeOuhlala);
		recetteCtrlPlancoTva.setMultiple(modeOuhlala);
		recetteCtrlPlancoCtp.setMultiple(modeOuhlala);
	}

	private void controlerCodeGestionContreparties() {
		if (recetteCtrlPlancoCtp == null) {
			return;
		}
		this.recetteCtrlPlancoCtp.controlerCodeGestionContreparties();
	}

	private void controlerTauxProrata() {
		if (this.eoRecette == null) {
			return;
		}
		this.eoRecette.updateTauxProrata();
	}

	private void updateData() {
		if (eoRecette == null) {
			return;
		}
		tfRecNumero.setNumber(eoRecette.recNumero());
		tfExercice.setText(eoRecette.exercice().exeExercice().toString());
		tfRecLib.setText(eoRecette.recLib());
		tfRecHtSaisie.setNumber(eoRecette.recHtSaisie());
		tfRecTtcSaisie.setNumber(eoRecette.recTtcSaisie());

		cbTva.removeActionListener(cbActionListener);
		cbTva.clean();
		cbTva.addActionListener(cbActionListener);

		if (eoRecette.recettePapier() != null) {
			tfNbPiece.setNumber(eoRecette.recettePapier().rppNbPiece());
		}

		if (eoRecette.facture().personne() != null) {
			StringBuilder txtBuilder = new StringBuilder(eoRecette.facture().personne().persNomPrenom());
			if (eoRecette.recettePapier() != null) {
				EOAdresse currentAdr = recettePapierService.currentAdresseClient(ec, eoRecette.recettePapier());
				if (currentAdr != null) {
					txtBuilder.append(" ").append(currentAdr.toInlineString());
				}
			}
			tfPersonne.setText(txtBuilder.toString());
		}
		else {
			tfPersonne.setText(null);
		}

		if (eoRecette.recettePapier() != null && eoRecette.recettePapier().ribfourUlr() != null) {
			tfRibClient.setText(eoRecette.recettePapier().ribfourUlr().cBanque() + " " + eoRecette.recettePapier().ribfourUlr().cGuichet() + " "
					+ eoRecette.recettePapier().ribfourUlr().noCompte() + " - " + eoRecette.recettePapier().ribfourUlr().ribTitco());
		}
		else {
			tfRibClient.setText(null);
		}

		if (eoRecette.facture().organ() != null) {
			tfOrgan.setText(eoRecette.facture().organ().orgLib());
		}
		else {
			tfOrgan.setText(null);
		}

		if (eoRecette.facture().typeCreditRec() != null) {
			cbTypeCreditRec.removeActionListener(cbActionListener);
			cbTypeCreditRec.setObjects(FinderTypeCreditRec.find(ec, eoRecette.exercice()));
			cbTypeCreditRec.setSelectedEOObject(eoRecette.facture().typeCreditRec());
			cbTypeCreditRec.addActionListener(cbActionListener);
		}
		else {
			cbTypeCreditRec.setObjects(FinderTypeCreditRec.find(ec, eoRecette.exercice()));
			cbTypeCreditRec.clean();
		}

		if (eoRecette.recettePapier() != null && eoRecette.recettePapier().modeRecouvrement() != null) {
			cbModeRecouvrement.removeActionListener(cbActionListener);
			if (eoRecette.facture() != null && eoRecette.facture().typeApplication() != null
					&& eoRecette.facture().typeApplication().equals(
							FinderTypeApplication.typeApplicationPrestationInterne(ec))) {
				cbModeRecouvrement.setObjects(FinderModeRecouvrement.find(ec, eoRecette.exercice()));
			}
			else {
				cbModeRecouvrement.setObjects(
						FinderModeRecouvrement.findSansPrestationInterne(ec, eoRecette.exercice()));
			}
			cbModeRecouvrement.setSelectedEOObject(eoRecette.recettePapier().modeRecouvrement());
			cbModeRecouvrement.addActionListener(cbActionListener);
		}
		else {
			if (eoRecette.facture() != null && eoRecette.facture().typeApplication() != null
					&& eoRecette.facture().typeApplication().equals(
							FinderTypeApplication.typeApplicationPrestationInterne(ec))) {
				cbModeRecouvrement.setObjects(FinderModeRecouvrement.find(ec, eoRecette.exercice()));
			}
			else {
				cbModeRecouvrement.setObjects(
						FinderModeRecouvrement.findSansPrestationInterne(ec, eoRecette.exercice()));
			}
		}

		recetteCtrlAction.setCurrentObject(eoRecette);
		recetteCtrlAnalytique.setCurrentObject(eoRecette);
		recetteCtrlConvention.setCurrentObject(eoRecette);
		recetteCtrlPlanco.setCurrentObject(eoRecette);

		updateInterfaceEnabling();
	}

	private void updateMontants() {

		if (cbCalculAutoTtc.isSelected()) {
			if (tfRecHtSaisie.getNumber() == null) {
				tfRecTtcSaisie.setNumber(null);
			}
			else {
				if (cbTva.getSelectedEOObject() == null) {
					tfRecTtcSaisie.setNumber(tfRecHtSaisie.getNumber());
				}
				else {
					tfRecTtcSaisie.setNumber(
							Computation.getTtc(tfRecHtSaisie.getBigDecimal(), (EOTva) cbTva.getSelectedEOObject()));
				}
			}
		}
		else {
			if (cbCalculAutoHt.isSelected()) {
				if (tfRecTtcSaisie.getNumber() == null) {
					tfRecHtSaisie.setNumber(null);
				}
				else {
					if (cbTva.getSelectedEOObject() == null) {
						tfRecHtSaisie.setNumber(tfRecTtcSaisie.getNumber());
					}
					else {
						tfRecHtSaisie.setNumber(
								Computation.getHt(tfRecTtcSaisie.getBigDecimal(), (EOTva) cbTva.getSelectedEOObject()));
					}
				}
			}
		}

		eoRecette.setRecHtSaisie(tfRecHtSaisie.getBigDecimal());
		eoRecette.facture().setFacHtSaisie(tfRecHtSaisie.getBigDecimal());
		eoRecette.setRecTtcSaisie(tfRecTtcSaisie.getBigDecimal());
		eoRecette.facture().setFacTtcSaisie(tfRecTtcSaisie.getBigDecimal());

		recetteCtrlAction.updateMontants();
		recetteCtrlAnalytique.updateMontants();
		recetteCtrlConvention.updateMontants();
		recetteCtrlPlanco.updateMontants();
		recetteCtrlPlancoTva.updateMontant();
		recetteCtrlPlancoCtp.updateMontant();
	}

	private void updateInterfaceEnabling() {
		if (eoRecette == null) {
			return;
		}

		boolean recetteTypeRecette = eoRecette.facture() == null
				|| FinderTypeApplication.typeApplicationRecette(ec).equals(eoRecette.facture().typeApplication());
		boolean recetteIndependante = eoRecette.recettePapier() == null
				|| "N".equalsIgnoreCase(eoRecette.recettePapier().rppVisible());
		boolean recetteInterne = eoRecette.facture() != null
				&& FinderTypeApplication.typeApplicationPrestationInterne(ec).equals(
						eoRecette.facture().typeApplication());
		boolean hasFullAccess = hasPermissionOrgan(eoRecette, app.appUserInfo().utilisateur());

		tfRecLib.setEnabled(hasFullAccess);
		tfRecHtSaisie.setEnabled(hasFullAccess && !recetteInterne);
		tfRecTtcSaisie.setEnabled(hasFullAccess && !recetteInterne);
		cbCalculAutoHt.setEnabled(hasFullAccess && !recetteInterne);
		cbCalculAutoTtc.setEnabled(hasFullAccess && !recetteInterne);
		cbTva.setEnabled(hasFullAccess && !recetteInterne);
		tfNbPiece.setEnabled(hasFullAccess && recetteIndependante);

		actionPersonneSelect.setEnabled(hasFullAccess && recetteIndependante && recetteTypeRecette);
		actionRibfourUlrSelect.setEnabled(hasFullAccess
				&& recetteIndependante
				&& eoRecette.facture() != null
				&& eoRecette.facture().personne() != null
				&& eoRecette.facture().fournisUlr() != null
				&& !(eoRecette.facture().typeApplication().equals(
						FinderTypeApplication.typeApplicationPrestationInterne(ec))));
		actionRibfourUlrDelete.setEnabled(hasFullAccess
				&& actionRibfourUlrSelect.isEnabled()
				&& eoRecette.recettePapier() != null
				&& eoRecette.recettePapier().ribfourUlr() != null);

		actionOrganSelect.setEnabled(hasFullAccess && recetteTypeRecette);
		cbTypeCreditRec.setEnabled(hasFullAccess && recetteTypeRecette);

		if (eoRecette.facture() != null
				&& eoRecette.facture().typeApplication() != null
				&& (eoRecette.facture().typeApplication().equals(
						FinderTypeApplication.typeApplicationPrestationInterne(ec))
				|| eoRecette.facture().echeId() != null)) {
			cbModeRecouvrement.setEnabled(false);
		}
		else {
			cbModeRecouvrement.setEnabled(hasFullAccess && cbModeRecouvrement.isEnabled() && recetteIndependante);
		}

		actionCheckModeOuhlala.setEnabled(hasFullAccess);

		recetteCtrlAction.setEditable(hasFullAccess);
		recetteCtrlAnalytique.setEditable(hasFullAccess);
		recetteCtrlConvention.setEditable(hasFullAccess);
		recetteCtrlPlanco.setEditable(hasFullAccess);
		recetteCtrlPlancoTva.setEditable(hasFullAccess);

		gereEtatRecetteCtrlPlancoCtp(hasFullAccess);

		btValid.setEnabled(hasFullAccess);
	}

	private boolean hasPermissionOrgan(EORecette recette, EOUtilisateur loggedUser) {
		boolean hasPermissionOrgan = true;
		if (recette != null && recette.facture() != null && recette.facture().organ() != null) {
			hasPermissionOrgan = recette.facture().organ().hasPermission(loggedUser);
		}
		return hasPermissionOrgan;
	}

	private void showRequired(boolean show) {
		tfRecHtSaisie.showRequired(show);
		tfRecTtcSaisie.showRequired(show);
		tfNbPiece.showRequired(show);
		tfPersonne.showRequired(show);
		tfOrgan.showRequired(show);
		cbTypeCreditRec.showRequired(show);
		cbModeRecouvrement.showRequired(show);
		recetteCtrlAction.showRequired(show);
		recetteCtrlPlanco.showRequired(show);
		recetteCtrlPlancoTva.showRequired(show);
		recetteCtrlPlancoCtp.showRequired(show);
	}

	private void onPersonneSelect() {
		if (personneSelect == null) {
			personneSelect = new FactureRecetteAddUpdateClientSelect();
		}
		adresseClientSelected = null;
		if (personneSelect.open(app.appUserInfo().utilisateur(), eoRecette.exercice(), null, true, true, true)) {
			adresseClientSelected = personneSelect.getAdresseSelected();
			eoRecette.facture().setPersonneRelationship(personneSelect.getSelected());
			eoRecette.facture().setFournisUlrRelationship(personneSelect.getSelectedFournisUlr());
			if (eoRecette.recettePapier() != null) {
				eoRecette.recettePapier().setPersonneRelationship(eoRecette.facture().personne());
				eoRecette.recettePapier().setFournisUlrRelationship(eoRecette.facture().fournisUlr());
			}
			if (eoRecette.facture().personne() != null) {
				StringBuilder txtBuilder = new StringBuilder(eoRecette.facture().personne().persNomPrenom());
				if (adresseClientSelected != null) {
					txtBuilder.append(" ").append(adresseClientSelected.toInlineString());
				}
				tfPersonne.setText(txtBuilder.toString());
			}
			updateInterfaceEnabling();
		}
	}

	private void onRibfourUlrSelect() {
		if (ribfourUlrSelect == null) {
			ribfourUlrSelect = new RibfourUlrSelect();
		}
		if (ribfourUlrSelect.open(app.appUserInfo().utilisateur(), eoRecette.exercice(), eoRecette.facture().fournisUlr(), eoRecette
				.recettePapier().ribfourUlr())) {
			eoRecette.recettePapier().setRibfourUlrRelationship(ribfourUlrSelect.getSelected());
			if (eoRecette.recettePapier().ribfourUlr() != null) {
				tfRibClient.setText(eoRecette.recettePapier().ribfourUlr().cBanque() + " " + eoRecette.recettePapier().ribfourUlr().cGuichet()
						+ " " + eoRecette.recettePapier().ribfourUlr().noCompte() + " - " + eoRecette.recettePapier().ribfourUlr().ribTitco());
			}
			updateInterfaceEnabling();
		}
	}

	private void onRibfourUlrDelete() {
		eoRecette.recettePapier().setRibfourUlrRelationship(null);
		tfRibClient.setText(null);
		updateInterfaceEnabling();
	}

	private void onOrganSelect() {
		if (organSelect == null) {
			organSelect = new OrganSelect();
		}
		if (organSelect.open(app.appUserInfo().utilisateur(), eoRecette.exercice(), eoRecette.facture().organ(), null)) {
			eoRecette.facture().setOrganRelationship(organSelect.getSelected());
			if (eoRecette.facture().organ() != null) {
				tfOrgan.setText(eoRecette.facture().organ().orgLib());
				majRecetteCtrlPlancoCtpCodeGestion();
			}
			updateInterfaceEnabling();
		}
	}

	private void majRecetteCtrlPlancoCtpCodeGestion() {
		if (this.recetteCtrlPlancoCtp == null) {
			return;
		}
		this.recetteCtrlPlancoCtp.updateCodeGestionContreparties();
	}

	private void initGUI() {
		JPanel contentPanel = new JPanel(new BorderLayout());
		contentPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanel);

		// Border commonEmptyThickBorder = BorderFactory.createEmptyBorder(12, 3, 12, 3);
		Border commonEmptyThinBorder = BorderFactory.createEmptyBorder(0, 3, 0, 3);

		// num\u00E9ro + exercice
		JPanel panelRecNumero = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel jlRecNumero = new JLabel("Recette N\u00B0 ");
		jlRecNumero.setFont(jlRecNumero.getFont().deriveFont(Font.BOLD, 20));
		jlRecNumero.setForeground(Constants.COLOR_LABEL_FGD_NORMAL);
		panelRecNumero.add(jlRecNumero);
		tfRecNumero.setFont(tfRecNumero.getFont().deriveFont(Font.BOLD, 24));
		tfRecNumero.setHorizontalAlignment(SwingConstants.RIGHT);
		tfRecNumero.setViewOnly();
		panelRecNumero.add(tfRecNumero);

		JPanel tempPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JLabel jlExercice = new JLabel("Exercice ");
		jlExercice.setFont(jlExercice.getFont().deriveFont(Font.BOLD, 16));
		jlExercice.setForeground(Constants.COLOR_LABEL_FGD_NORMAL);
		tempPanel.add(jlExercice);
		tfExercice.setFont(tfExercice.getFont().deriveFont(Font.BOLD, 18));
		tfExercice.setHorizontalAlignment(SwingConstants.CENTER);
		tfExercice.setViewOnly();
		tempPanel.add(tfExercice);
		JPanel panelExercice = new JPanel(new GridBagLayout());
		panelExercice.add(tempPanel, new GridBagConstraints());

		JPanel panelRecNumeroExercice = new JPanel(new BorderLayout());
		panelRecNumeroExercice.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		// panelRecNumeroExercice.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.gray));
		panelRecNumeroExercice.add(panelRecNumero, BorderLayout.LINE_START);
		panelRecNumeroExercice.add(new JPanel(), BorderLayout.CENTER);
		panelRecNumeroExercice.add(panelExercice, BorderLayout.LINE_END);

		// libell\u00E9
		JPanel panelRecLib = UIUtilities.labeledComponent("Libell\u00E9", tfRecLib, null);
		// panelRecLib.setBorder(commonEmptyThinBorder);

		// montants
		MontantsFocusListener mfl = new MontantsFocusListener();
		JPanel panelMontants = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panelMontants.setBorder(BorderFactory.createEmptyBorder());

		cbCalculAutoHt.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);
		cbCalculAutoHt.setVerticalTextPosition(SwingConstants.TOP);
		cbCalculAutoHt.setHorizontalTextPosition(SwingConstants.CENTER);
		cbCalculAutoHt.setIconTextGap(0);
		cbCalculAutoHt.setFocusPainted(false);
		cbCalculAutoHt.setBorderPaintedFlat(true);

		cbCalculAutoTtc.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);
		cbCalculAutoTtc.setVerticalTextPosition(SwingConstants.TOP);
		cbCalculAutoTtc.setHorizontalTextPosition(SwingConstants.CENTER);
		cbCalculAutoTtc.setIconTextGap(0);
		cbCalculAutoTtc.setFocusPainted(false);
		cbCalculAutoTtc.setBorderPaintedFlat(true);

		tfRecHtSaisie.addFocusListener(mfl);
		tfRecTtcSaisie.addFocusListener(mfl);

		panelMontants.add(UIUtilities.labeledComponent("Montant HT", tfRecHtSaisie, null));
		panelMontants.add(UIUtilities.labeledComponent(" ", cbCalculAutoHt, null));
		panelMontants.add(Box.createHorizontalStrut(10));
		panelMontants.add(UIUtilities.labeledComponent("Tva", cbTva, null));
		panelMontants.add(Box.createHorizontalStrut(10));
		panelMontants.add(UIUtilities.labeledComponent("Montant TTC", tfRecTtcSaisie, null));
		panelMontants.add(UIUtilities.labeledComponent(" ", cbCalculAutoTtc, null));
		panelMontants.add(Box.createHorizontalStrut(50));
		panelMontants.add(UIUtilities.labeledComponent("Nb de pi\u00E8ces", tfNbPiece, null));

		Box panelLibMontants = Box.createVerticalBox();
		panelLibMontants.setBorder(BorderFactory.createEmptyBorder());
		panelLibMontants.add(panelRecLib);
		panelLibMontants.add(panelMontants);

		// client
		Box panelClient = Box.createVerticalBox();
		panelClient.setBorder(BorderFactory.createTitledBorder(null, "Client", TitledBorder.DEFAULT_JUSTIFICATION,
				TitledBorder.DEFAULT_POSITION, null, Constants.COLOR_LABEL_FGD_LIGHT));
		panelClient.add(UIUtilities.labeledComponent(null, tfPersonne, actionPersonneSelect));
		panelClient.add(UIUtilities.labeledComponent("Rib client", tfRibClient, new Action[] {
				actionRibfourUlrSelect, actionRibfourUlrDelete
		},
				UIUtilities.DEFAULT_BOXED, UIUtilities.DEFAULT_ALIGNMENT));

		// ligne budg + type cred
		JPanel panelOrganTypCred = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panelOrganTypCred.setBorder(BorderFactory.createEmptyBorder());
		panelOrganTypCred.add(UIUtilities.labeledComponent("Ligne budg\u00E9taire", tfOrgan, actionOrganSelect));
		panelOrganTypCred.add(Box.createHorizontalStrut(10));
		panelOrganTypCred.add(UIUtilities.labeledComponent("Type de cr\u00E9dit", cbTypeCreditRec, null));

		Box panelBudgetMor = Box.createVerticalBox();
		panelBudgetMor.setBorder(BorderFactory.createTitledBorder(
				null, null, TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION,
				null, Constants.COLOR_LABEL_FGD_LIGHT));
		panelBudgetMor.add(panelOrganTypCred);
		panelBudgetMor.add(UIUtilities.labeledComponent("Mode de recouvrement", cbModeRecouvrement, null));

		checkBoxModeOuhlala.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);
		checkBoxModeOuhlala.setVerticalTextPosition(SwingConstants.TOP);
		checkBoxModeOuhlala.setHorizontalTextPosition(SwingConstants.CENTER);
		checkBoxModeOuhlala.setIconTextGap(0);
		checkBoxModeOuhlala.setFocusPainted(false);
		checkBoxModeOuhlala.setBorderPaintedFlat(true);
		Box panelCheckBox = Box.createVerticalBox();
		panelCheckBox.setBorder(BorderFactory.createEmptyBorder());
		panelCheckBox.add(Box.createVerticalGlue());
		panelCheckBox.add(checkBoxModeOuhlala);

		JPanel panelBudgetCheckBox = new JPanel(new BorderLayout(0, 0));
		panelBudgetCheckBox.setBorder(BorderFactory.createEmptyBorder());
		panelBudgetCheckBox.add(panelBudgetMor, BorderLayout.LINE_START);
		panelBudgetCheckBox.add(new JPanel(), BorderLayout.CENTER);
		panelBudgetCheckBox.add(panelCheckBox, BorderLayout.LINE_END);

		// actions + canal
		JPanel panelCtrls = new JPanel(new GridLayout(1, 2, 10, 0));
		panelCtrls.setBorder(commonEmptyThinBorder);
		panelCtrls.add(recetteCtrlAction);
		panelCtrls.add(recetteCtrlAnalytique);

		// planco
		JPanel panelPlanco = new JPanel(new BorderLayout(0, 0));
		panelPlanco.setBorder(commonEmptyThinBorder);
		panelPlanco.add(recetteCtrlPlanco, BorderLayout.LINE_START);
		panelPlanco.add(new JPanel(), BorderLayout.CENTER);

		// planco tva + planco ctp
		JPanel panelPlancos = new JPanel(new GridLayout(1, 2, 10, 0));
		panelPlancos.setBorder(commonEmptyThinBorder);
		panelPlancos.add(recetteCtrlPlancoTva);
		panelPlancos.add(recetteCtrlPlancoCtp);

		// convention
		JPanel panelConvention = new JPanel(new BorderLayout(0, 0));
		panelConvention.setBorder(commonEmptyThinBorder);
		panelConvention.add(recetteCtrlConvention, BorderLayout.LINE_START);
		panelConvention.add(new JPanel(), BorderLayout.CENTER);

		Box avant = Box.createVerticalBox();
		avant.setBorder(BorderFactory.createEmptyBorder());
		avant.add(panelRecNumeroExercice);
		avant.add(Box.createVerticalStrut(10));
		avant.add(panelLibMontants);
		avant.add(Box.createVerticalStrut(10));
		avant.add(panelClient);
		avant.add(Box.createVerticalStrut(10));
		avant.add(panelBudgetCheckBox);
		avant.add(Box.createVerticalStrut(10));

		Box panelResizable = Box.createVerticalBox();
		panelResizable.setBorder(BorderFactory.createEmptyBorder());
		panelResizable.add(panelCtrls);
		panelResizable.add(panelPlanco);
		panelResizable.add(panelPlancos);
		panelResizable.add(panelConvention);

		JPanel center = new JPanel(new BorderLayout());
		center.setBorder(BorderFactory.createEmptyBorder());
		center.add(avant, BorderLayout.NORTH);
		center.add(panelResizable, BorderLayout.CENTER);

		getContentPane().add(center, BorderLayout.CENTER);
		getContentPane().add(buildBottomPanel(), BorderLayout.SOUTH);

		setGlassPane(new DisabledGlassPane(getContentPane(), btCancel));
		initInputMap();

		this.validate();
		this.pack();
	}

	private final JPanel buildBottomPanel() {
		JPanel p = new JPanel();
		p.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.add(Box.createHorizontalGlue());
		p.add(btCancel);
		p.add(Box.createRigidArea(new Dimension(5, 0)));
		p.add(btValid);
		return p;
	}

	private void initInputMap() {
		((JComponent) getContentPane()).getActionMap().put("CTRL_Q", app.superviseur().mainMenu.actionQuit);
		((JComponent) getContentPane()).getActionMap().put("CTRL_S", actionValid);
		((JComponent) getContentPane()).getActionMap().put("F10", actionValid);
		((JComponent) getContentPane()).getActionMap().put("ESCAPE", actionCancel);
		((JComponent) getContentPane()).getActionMap().put("F8", actionShowRequired);
		((JComponent) getContentPane()).getActionMap().put("ALT_F8", actionUnshowRequired);
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
				KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK), "CTRL_Q");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
				KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK), "CTRL_S");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F10, 0), "F10");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"ESCAPE");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("F8"), "F8");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("released F8"), "ALT_F8");
	}

	private class CbActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JComboBox cb = (JComboBox) e.getSource();
			if (cb == cbTypeCreditRec && eoRecette != null && eoRecette.facture() != null) {
				eoRecette.facture().setTypeCreditRecRelationship((EOTypeCredit) cbTypeCreditRec.getSelectedEOObject());
			}
			if (cb == cbModeRecouvrement && eoRecette != null && eoRecette.recettePapier() != null) {
				eoRecette.recettePapier().setModeRecouvrementRelationship(
						(EOModeRecouvrement) cbModeRecouvrement.getSelectedEOObject());
				if (eoRecette.facture() != null) {
					eoRecette.facture().setModeRecouvrementRelationship(eoRecette.recettePapier().modeRecouvrement());
				}
			}
			if (cb == cbTva) {
				updateMontants();
			}
			updateInterfaceEnabling();
		}
	}

	private class MontantsFocusListener implements FocusListener {
		public void focusGained(FocusEvent e) {
		}

		public void focusLost(FocusEvent e) {
			if (e.getComponent() == tfRecHtSaisie) {
				updateMontants();
			}
			if (e.getComponent() == tfRecTtcSaisie) {
				updateMontants();
			}
		}
	}

	private class ActionCheckCalculAutoHT extends AbstractAction {
		public ActionCheckCalculAutoHT() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CALCULER_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Calculer automatiquement le HT depuis le TTC");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				if (cbCalculAutoHt.isSelected()) {
					tfRecHtSaisie.setEditable(false);
					if (cbCalculAutoTtc.isSelected()) {
						cbCalculAutoTtc.setSelected(false);
						tfRecTtcSaisie.setEditable(true);
					}
					updateMontants();
				}
				else {
					tfRecHtSaisie.setEditable(true);
				}
			}
		}
	}

	private class ActionCheckCalculAutoTTC extends AbstractAction {
		public ActionCheckCalculAutoTTC() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CALCULER_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Calculer automatiquement le TTC depuis le HT");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				if (cbCalculAutoTtc.isSelected()) {
					tfRecTtcSaisie.setEditable(false);
					if (cbCalculAutoHt.isSelected()) {
						cbCalculAutoHt.setSelected(false);
						tfRecHtSaisie.setEditable(false);
					}
					updateMontants();
				}
				else {
					tfRecTtcSaisie.setEditable(true);
				}
			}
		}
	}

	private class ActionValid extends AbstractAction {
		public ActionValid() {
			super("Valider");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onValid();
			}
		}
	}

	private class ActionCancel extends AbstractAction {
		public ActionCancel() {
			super("Annuler");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CANCEL_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onCancel();
			}
		}
	}

	private class ActionClose extends AbstractAction {
		public ActionClose() {
			super("Fermer");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CLOSE_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onClose();
			}
		}
	}

	private class ActionShowRequired extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				showRequired(true);
			}
		}
	}

	private class ActionUnshowRequired extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				showRequired(false);
			}
		}
	}

	private class ActionCheckModeOuhlala extends AbstractAction {
		public ActionCheckModeOuhlala() {
			super("Mode expert");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Permet de g\u00E9rer de multiples actions, imputations, codes analytiques, ...");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onCheckModeOuhlala();
			}
		}
	}

	private class ActionPersonneSelect extends AbstractAction {
		public ActionPersonneSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onPersonneSelect();
			}
		}
	}

	private class ActionRibfourUlrSelect extends AbstractAction {
		public ActionRibfourUlrSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onRibfourUlrSelect();
			}
		}
	}

	private class ActionRibfourUlrDelete extends AbstractAction {
		public ActionRibfourUlrDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onRibfourUlrDelete();
			}
		}
	}

	private class ActionOrganSelect extends AbstractAction {
		public ActionOrganSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onOrganSelect();
			}
		}
	}

	private class LocalWindowListener implements WindowListener {
		public void windowActivated(WindowEvent arg0) {
		}

		public void windowClosed(WindowEvent arg0) {
		}

		public void windowClosing(WindowEvent arg0) {
			actionClose.actionPerformed(null);
		}

		public void windowDeactivated(WindowEvent arg0) {
		}

		public void windowDeiconified(WindowEvent arg0) {
		}

		public void windowIconified(WindowEvent arg0) {
		}

		public void windowOpened(WindowEvent arg0) {
		}
	}

	public void setWaitCursor(boolean bool) {
		if (bool) {
			this.getGlassPane().setVisible(true);
		}
		else {
			this.getGlassPane().setVisible(false);
		}
	}

	/**
	 * Client select implementation.
	 * 
	 * @author flagouey
	 */
	private final class FactureRecetteAddUpdateClientSelect extends ClientSelect {
		/** Serial version ID. */
		private static final long serialVersionUID = 1L;

		@Override
		protected void registerValidators() {
			super.registerValidators();
			addPersonneValidators(new FournisseurEtatValideValidator(
					SelectValidatorSeverity.ERROR,
					FournisseurEtatValideValidator.ERR_FOURNISSEUR_ETAT_VALIDATION_EN_COURS));
		}
	}

}
