/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.kava.client.finder.FinderExercice;

import app.client.tools.Constants;
import app.client.ui.CRICursor;
import app.client.ui.UIUtilities;
import app.client.ui.ZEOComboBox;
import app.client.ui.ZTextField;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;


public class Superviseur extends JFrame {

	/** Onglet catalogue. */
	public static final String		ONGLET_CATALOGUE		= "Catalogues";
	/** Onglet Prestation. */
	public static final String		ONGLET_PRESTATION		= "Prestations";
	/** Onglet Facture papier. */
	public static final String		ONGLET_FACTURE_PAPIER	= "Factures";
	/** Onglet Recette. */
	public static final String		ONGLET_RECETTE			= "Recettes";

	public final Dimension WINDOW_DIMENSION = new Dimension(930, 600);

	private ZEOComboBox				cbExercice;
	private EOExercice				currentExercice;
	private ZTextField				tfExercice;
	private JLabel					labelState;

	public JTabbedPane				jtp;

	public Menu					mainMenu;
	protected ApplicationClient		app;
	protected EOEditingContext		ec;
	long start = System.currentTimeMillis();

	public Superviseur(String windowTitle) {
		super(windowTitle);
		//setGlassPane(new BusyGlassPanel());

		app = (ApplicationClient) EOApplication.sharedApplication();
		ec = app.editingContext();


		mainMenu = new Menu();

		cbExercice = new ZEOComboBox(FinderExercice.findAll(ec), EOExercice.PRIMARY_KEY_KEY, null, null, null, 90);
		tfExercice = new ZTextField(12);
		tfExercice.setViewOnly();

		currentExercice = FinderExercice.findExerciceEnCours(ec);

		initGUI();
	}

	public void setVisible(boolean flag) {
		super.setVisible(false);
	}

	public void setOngletSelected(String aOnglet)
	{
		jtp.setSelectedIndex(jtp.indexOfTab(aOnglet));
		update();
	}

	public void show() {
		hide();
	}

	public void init() {
		cbExercice.setSelectedEOObject(currentExercice);

		CbActionListener cbListener = new CbActionListener();
		cbExercice.addActionListener(cbListener);

		// init interfaces
		System.out.println("Debut : " + new NSTimestamp());
		Catalogue.sharedInstance().init(app.getCurrentUtilisateur());
		System.out.println("Catalogue : " + new NSTimestamp());
		Prestation.sharedInstance().init(app.getCurrentUtilisateur());
		System.out.println("Prestation : " + new NSTimestamp());
		FacturePapier.sharedInstance().init(app.getCurrentUtilisateur());
		System.out.println("FacturePapier : " + new NSTimestamp());
		FactureRecette.sharedInstance().init(app.getCurrentUtilisateur());
		System.out.println("FactureRecette : " + new NSTimestamp());
	}

	public void open() {
		this.update();
		this.show();
		System.out.println("Chargement interface : " + (System.currentTimeMillis() - start) + "ms");
	}

	public boolean canQuit() {
		return Catalogue.sharedInstance().canQuit()
				&& Prestation.sharedInstance().canQuit()
				&& FacturePapier.sharedInstance().canQuit()
				&& FactureRecette.sharedInstance().canQuit();
	}

	public void setEnableUI(boolean enable) {
		cbExercice.setEnabled(enable);
	}

	public void update() {
		if (currentExercice().exeStatFac().equalsIgnoreCase(EOExercice.EXERCICE_OUVERT)) {
			tfExercice.setText("Ouvert");
		}
		else {
			if (currentExercice().exeStatFac().equalsIgnoreCase(EOExercice.EXERCICE_RESTREINT)) {
				tfExercice.setText("Restreint");
			}
			else {
				if (currentExercice().exeStatFac().equalsIgnoreCase(EOExercice.EXERCICE_PREPARATION)) {
					tfExercice.setText("En pr\u00E9paration");
				}
				else {
					tfExercice.setText("Clos");
				}
			}
		}

		mainMenu.update();

		((IPieTab) jtp.getSelectedComponent()).update();
	}

	public void setStateLabel(String s) {
		labelState.setText(s);
	}

	private void initGUI() {
		//setGlassPane(new BusyGlassPanel());
		JPanel contentPanel = new JPanel(new BorderLayout());
		contentPanel.setBorder(BorderFactory.createEmptyBorder());
		contentPanel.setPreferredSize(WINDOW_DIMENSION);
		setContentPane(contentPanel);

		jtp = new JTabbedPane();
		if (StringCtrl.toBool(app.getApplicationParametre("APP_SHOW_ONGLET_CATALOGUE"))) {
			jtp.add(ONGLET_CATALOGUE, Catalogue.sharedInstance());
		}
		jtp.add(ONGLET_PRESTATION, Prestation.sharedInstance());
		jtp.add(ONGLET_FACTURE_PAPIER, FacturePapier.sharedInstance());
		jtp.add(ONGLET_RECETTE, FactureRecette.sharedInstance());
		jtp.addChangeListener(new OngletChangeListener());

		JPanel top = new JPanel(new BorderLayout());
		top.add(mainMenu, BorderLayout.NORTH);
		top.add(buildToolbar(), BorderLayout.CENTER);

		getContentPane().add(top, BorderLayout.NORTH);
		getContentPane().add(jtp, BorderLayout.CENTER);
		getContentPane().add(buildBottomPanel(), BorderLayout.SOUTH);

		this.validate();
		this.pack();
	}

	private JPanel buildToolbar() {
		JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		p.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		p.add(UIUtilities.labeledComponent("Exercice", cbExercice, null, SwingConstants.HORIZONTAL));
		p.add(Box.createRigidArea(new Dimension(5, 0)));
		tfExercice.setForeground(Constants.COLOR_LABEL_FGD_INFO);
		tfExercice.setHorizontalAlignment(JTextField.CENTER);
		p.add(UIUtilities.labeledComponent(null, tfExercice, null, SwingConstants.HORIZONTAL));

		return p;
	}

	private final JPanel buildBottomPanel() {
		JPanel p = new JPanel(new BorderLayout());
		p.setBorder(BorderFactory.createEmptyBorder());
		JLabel jl = new JLabel(ServerProxy.serverAppliVersion(ec) + " - " + ServerProxy.serverBdConnexionName(ec));
		jl.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);

		labelState = new JLabel();
		labelState.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);

		p.add(jl, BorderLayout.LINE_START);
		p.add(new JPanel(), BorderLayout.CENTER);
		p.add(labelState, BorderLayout.LINE_END);
		return p;
	}

	private class CbActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JComboBox cb = (JComboBox) e.getSource();
			if (cb == cbExercice) {
				currentExercice = (EOExercice) cbExercice.getSelectedEOObject();
				app.setCurrentExercice(currentExercice);
			}
			update();
		}
	}

	private class OngletChangeListener implements ChangeListener {
		public void stateChanged(ChangeEvent e) {
			JTabbedPane tabbedPane = (JTabbedPane) e.getSource();

			if (tabbedPane.getSelectedComponent().equals(Catalogue.sharedInstance())) {
				Catalogue.sharedInstance().update();
			}

			if (tabbedPane.getSelectedComponent().equals(Prestation.sharedInstance())) {
				Prestation.sharedInstance().afficheRechercheAvanceeIfOpen();
				Prestation.sharedInstance().update();
			}
			else {
				Prestation.sharedInstance().masquerRechercheAvancee();
			}
			if (tabbedPane.getSelectedComponent().equals(FacturePapier.sharedInstance())) {
				FacturePapier.sharedInstance().afficheRechercheAvanceeIfOpen();
				FacturePapier.sharedInstance().update();
			}
			else {
				FacturePapier.sharedInstance().masquerRechercheAvancee();
			}
			if (tabbedPane.getSelectedComponent().equals(FactureRecette.sharedInstance())) {
				FactureRecette.sharedInstance().afficheRechercheAvanceeIfOpen();
				FactureRecette.sharedInstance().update();
			}
			else {
				FactureRecette.sharedInstance().masquerRechercheAvancee();
			}
		}
	}

	public void setWaitCursor(boolean bool) {
		CRICursor.setWaitCursor(this, bool);
	}

	public void setWaitCursor(Component component, boolean bool) {
		CRICursor.setWaitCursor(component, bool);
	}


	public EOExercice currentExercice() {
		return currentExercice;
	}
}
