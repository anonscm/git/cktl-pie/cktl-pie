/*
CRI - Universite de La Rochelle, 2006

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JPanel;

import app.client.tools.Constants;
import app.client.ui.UIUtilities;
import app.client.ui.ZTextField;
import app.client.ui.table.ZExtendedTablePanel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;

public abstract class CtrlPanel extends JPanel {

	protected abstract boolean defaultMultipleInit();

	protected abstract boolean defaultMultipleInitLocked();

	protected abstract void onSelect();

	protected abstract ZExtendedTablePanel getTablePanel();

	protected abstract void updateData();

	protected ZExtendedTablePanel	table;
	protected ZTextField			tfCtrl;

	protected boolean				multipleEnable;

	private Action					actionSelect;

	protected ApplicationClient		app;
	protected EOEditingContext		ec;

	public CtrlPanel(String labelSingle, int textFieldSize) {
		app = (ApplicationClient) EOApplication.sharedApplication();
		ec = app.editingContext();

		onCreate(labelSingle, textFieldSize);
		initGUI(labelSingle);

		multipleEnable = defaultMultipleInit();
		if (multipleEnable) {
			((CardLayout) getLayout()).show(this, "multiple");
		}
		else {
			((CardLayout) getLayout()).show(this, "single");
		}
		if (defaultMultipleInitLocked()) {
			System.out.println("INFO: " + labelSingle + " bloque en mode " + (defaultMultipleInit() ? "multiple" : "single"));
		}
	}

	public void setEditable(boolean editable) {
		table.setEditable(editable);
		actionSelect.setEnabled(editable);
	}

	public boolean isEditable() {
		return table.isEditable();
	}

	public final void setMultiple(boolean yn) {
		if (defaultMultipleInitLocked()) {
			return;
		}
		if (multipleEnable == yn) {
			return;
		}
		multipleEnable = yn;
		if (multipleEnable) {
			((CardLayout) getLayout()).show(this, "multiple");
		}
		else {
			((CardLayout) getLayout()).show(this, "single");
		}
		updateData();
	}

	public final void showRequired(boolean show) {
		if (multipleEnable) {
			table.showRequired(show);
		}
		else {
			tfCtrl.showRequired(show);
		}
	}

	public boolean acceptSingle() {
		return !defaultMultipleInitLocked() || !defaultMultipleInit();
	}

	public boolean acceptMultiple() {
		return !defaultMultipleInitLocked() || defaultMultipleInit();
	}

	private void initGUI(String labelSingle) {
		setLayout(new CardLayout());

		if (acceptMultiple()) {
			add(createGUIWhenAcceptMultiple(labelSingle), "multiple");
		}

		if (acceptSingle()) {
			add(createGUIWhenAcceptSingle(labelSingle), "single");
		}

		validate();
	}

	protected void onCreate(String labelSingle, int textFieldSize) {
		table = getTablePanel();
		actionSelect = new ActionSelect();
		tfCtrl = new ZTextField(textFieldSize);
		tfCtrl.setViewOnly();
	}

	protected Component createGUIWhenAcceptMultiple(String labelSingle) {
		return this.table;
	}

	protected Component createGUIWhenAcceptSingle(String labelSingle) {
		return UIUtilities.labeledComponent(labelSingle, tfCtrl, actionSelect);
	}

	private class ActionSelect extends AbstractAction {
		public ActionSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onSelect();
			}
		}
	}

}
