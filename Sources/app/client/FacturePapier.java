/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;

import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.cocowork.client.metier.grhum.AdresseBanqueBox;
import org.cocktail.echeancier.client.exception.EcheancierException;
import org.cocktail.echeancier.client.metier.Echeancier;
import org.cocktail.echeancier.client.metier.EcheancierDetail;
import org.cocktail.echeancier.client.ui.eocontroller.common.ControllerDelegate;
import org.cocktail.echeancier.common.util.debug.Debug;
import org.cocktail.fwkcktlcompta.client.remotecalls.ServerCallCompta;
import org.cocktail.fwkcktlcompta.common.IFwkCktlComptaParam;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineType;
import org.cocktail.fwkcktlcompta.common.sepasdd.origines.OrigineComplements;
import org.cocktail.fwkcktlcompta.common.util.CktlConfigUtil;
import org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ctrl.SepaSddMandatCtrl;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZDropDownButton;
import org.cocktail.kava.client.factory.FactoryFacturePapier;
import org.cocktail.kava.client.finder.FinderFacturePapier;
import org.cocktail.kava.client.finder.FinderFournisUlr;
import org.cocktail.kava.client.finder.FinderTypeApplication;
import org.cocktail.kava.client.finder.FinderTypeEtat;
import org.cocktail.kava.client.finder.FinderTypePublic;
import org.cocktail.kava.client.metier.EOAdresse;
import org.cocktail.kava.client.metier.EOFacturePapier;
import org.cocktail.kava.client.metier.EOFonction;
import org.cocktail.kava.client.metier.EOFournisUlr;
import org.cocktail.kava.client.metier.EOGrhumParametres;
import org.cocktail.kava.client.metier.EOOrgan;
import org.cocktail.kava.client.metier.EOPersonne;
import org.cocktail.kava.client.metier.EOPrestation;
import org.cocktail.kava.client.metier.EORibfourUlr;
import org.cocktail.kava.client.metier.EOUtilisateur;
import org.cocktail.kava.client.procedures.ApiPrestation;
import org.cocktail.kava.client.qualifier.Qualifiers;
import org.cocktail.kava.client.qualifier.Qualifiers.QualifierKey;
import org.cocktail.kava.client.service.ComptaEntityHelper;
import org.cocktail.kava.client.service.FacturePapierService;
import org.cocktail.pie.client.interfaces.FinderFactureRechercheAvance;
import org.cocktail.pieFwk.common.exception.EntityInitializationException;

import Structure.client.STRepartBanqueAdresse;
import app.client.configuration.ConfigurationCouleur;
import app.client.reports.ReportFactoryClient;
import app.client.select.ClientSelect;
import app.client.select.validator.FournisseurEtatValideValidator;
import app.client.select.validator.FournisseurRequisValidator;
import app.client.select.validator.SelectValidator;
import app.client.select.validator.SelectValidatorMessage;
import app.client.select.validator.SelectValidatorMessages;
import app.client.select.validator.SelectValidatorSeverity;
import app.client.tools.Constants;
import app.client.ui.UIUtilities;
import app.client.ui.ZEOComboBox;
import app.client.ui.ZNumberField;
import app.client.ui.ZTextField;
import app.client.ui.table.ZEOTableCellRenderer;
import app.client.ui.table.ZEOTableModelColumn;
import app.client.ui.table.ZExtendedTablePanel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class FacturePapier extends JPanel implements IPieTab {

	private static final String SIXID_PIE_FACTURE_ANGLAIS_PARAM = "SIXID_PIE_FACTURE_ANGLAIS";

	protected ApplicationClient app;
	protected EOEditingContext ec;

	private static FacturePapier sharedInstance;

	private ConfigurationCouleur configurationCouleur;

	private ZNumberField tfFapNumero;
	private ZTextField tfFapLib, tfExercice;
	private ZEOComboBox cbFournisUlr;
	private CbActionListener cbActionListener;
	private JCheckBox checkBoxFacturesRecettees;
	private Action actionCheckFacturesRecettees;
	private JButton btnEcheancier;

	private FacturePapierTablePanel facturePapierTablePanel;

	private Action actionValideClient, actionValidePrest;
	private Action actionPrint, actionMail;
	private Action actionEcheancier;
	private Action actionEcheancierSepa;
	private Action actionLegend;
	private Action actionClose;
	private Action actionRechercheAvance;

	private List<SelectValidator> fournisValidators;
	private EOAdresse adresseClientSelected;

	private FinderFactureRechercheAvance monFactureRechercheAvance;

	private JComboBox listMaxResult;
	private Action actionSrch;
	public Boolean atLeastOnSearchHasBeenMade = Boolean.FALSE;


	public static FacturePapier sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new FacturePapier();
		}
		return sharedInstance;
	}

	private FacturePapier() {
		super();

		app = (ApplicationClient) EOApplication.sharedApplication();
		ec = app.editingContext();

		this.configurationCouleur = ConfigurationCouleur.instance();

		actionValideClient = new ActionValideClient();
		actionValidePrest = new ActionValidePrestataire();
		actionPrint = new ActionPrint();
		actionMail = new ActionMail();
		initActionsEcheancier();

		actionLegend = new ActionLegend();
		actionClose = new ActionClose();
		actionRechercheAvance = new ActionRechercheAvance();
		actionSrch = new ActionSrch();

		tfFapNumero = new ZNumberField(5, app.getNumeroFormatDisplay());
		//tfFapNumero.getDocument().addDocumentListener(new SearchFapNumeroDocumentListener());
		tfFapLib = new ZTextField(15);
		//tfFapLib.getDocument().addDocumentListener(new SearchFapLibDocumentListener());
		tfExercice = new ZTextField(4);
		cbActionListener = new CbActionListener();
		cbFournisUlr = new ZEOComboBox(FinderFournisUlr.find(ec, app.getParam(EOGrhumParametres.PARAM_ANNUAIRE_FOU_VALIDE_INTERNE)),
				EOFournisUlr.PERSONNE_PERS_NOM_PRENOM_KEY, "Tous", null, null, 150);
		//cbFournisUlr.addActionListener(cbActionListener);
		actionCheckFacturesRecettees = new ActionCheckFacturesRecettees();
		checkBoxFacturesRecettees = new JCheckBox(actionCheckFacturesRecettees);

		MyActionListener defaultActionListener = new MyActionListener(actionSrch);
		tfFapNumero.addActionListener(defaultActionListener);
		tfFapLib.addActionListener(defaultActionListener);

		facturePapierTablePanel = new FacturePapierTablePanel();
		facturePapierTablePanel.setEditable(false);

		fournisValidators = new ArrayList<SelectValidator>();

		registerValidators();

		listMaxResult = new JComboBox(new String[] {
				"25", "50", "100", "Tous"
		});

		initGUI();
	}

	private class MyActionListener implements ActionListener {
		private final Action action;

		/**
	         *
	         */
		public MyActionListener(Action a) {
			super();
			action = a;
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(final ActionEvent e) {
			action.actionPerformed(e);
		}

	}

	private void initActionsEcheancier() {
		actionEcheancier = new ActionEcheancier();
		actionEcheancierSepa = new ActionEcheancierSepa();
	}

	public void setQualifierForFacturePapierTablePanel(EOQualifier qual) {
		facturePapierTablePanel.setFilteringQualifier(qual);
		facturePapierTablePanel.updateData();
	}

	private void registerValidators() {
		fournisValidators.add(new FournisseurRequisValidator());
		fournisValidators.add(new FournisseurEtatValideValidator(
				SelectValidatorSeverity.ERROR,
				FournisseurEtatValideValidator.ERR_ONPRINT_FOURNISSEUR_ETAT_VALIDATION_EN_COURS));
	}

	/**
	 * Permet eventuellement de configurer au lancement l'interface selon des prefs utilisateur.
	 *
	 * @param utilisateur
	 */
	public void init(org.cocktail.application.client.eof.EOUtilisateur utilisateur) {
	}

	public void duplicateFacturePapier() {
		EOFacturePapier fap = getSelected();
		if (fap == null) {
			app.showErrorDialog("Pas de facture s\u00E9lectionn\u00E9e !");
			return;
		}
		if (fap.typePublic().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(ec))) {
			app.showErrorDialog("La facture s\u00E9lectionn\u00E9e est de type prestation interne, on ne duplique pas de facture interne (sinon quid de l'engagement?) !");
			return;
		}
		ClientSelect clientSelect = new DuplicataFactureClientSelect();
		boolean extraBall = true;
		while (extraBall) {
			extraBall = false;
			adresseClientSelected = null;
			if (clientSelect
					.open(app.appUserInfo().utilisateur(), fap.exercice(), null, true, true, true, "<b>Duplication de la facture No "
							+ fap.fapNumero()
							+ "</b>\nS\u00E9lectionnez un client et validez pour cr\u00E9er une nouvelle facture (copie de la facture s\u00E9lectionn\u00E9e).")) {
				adresseClientSelected = clientSelect.getAdresseSelected();
				Integer fapNumero = duplicate(fap, clientSelect.getSelectedFournisUlr(), adresseClientSelected);
				if (fapNumero != null) {
					extraBall = app.showConfirmationDialog("OK", "Facture No " + fapNumero + " g\u00E9n\u00E9r\u00E9e !", "Dupliquer encore (autre client)",
							"Terminer");
				}
			}
		}
	}

	private Integer duplicate(EOFacturePapier facturePapier, EOFournisUlr fournisUlr, EOAdresse fournisUlrAdresse) {
		try {
			if (fournisUlr == null) {
				throw new Exception("Il faut indiquer un client pour dupliquer une facture !");
			}
			NSDictionary returnedDico = null;
			if (facturePapier.updateTauxProrata()) {
				ec.saveChanges();
			}
			returnedDico = ApiPrestation.duplicateFacturePapierAdresse(
					ec, facturePapier, app.appUserInfo().utilisateur(), fournisUlr, fournisUlrAdresse);
			if (returnedDico != null) {
				updateData();
				return (Integer) returnedDico.valueForKey("050aFapNumeroOut");
			}
			throw new Exception("Probl\u00E8me pour dupliquer la facture ! D\u00E9sol\u00E9 !!!");
		} catch (Exception e) {
			app.showErrorDialog(e);
			return null;
		}
	}



	public void update() {
		EOExercice exercice = app.superviseur().currentExercice();
		tfExercice.setText(exercice.exeExercice().toString());

		// TODO autoriser tout \u00E9a plus tard m\u00E9me sur un exercice clos (pour consultation / traitement des ann\u00E9es pr\u00E9c\u00E9dentes)
		facturePapierTablePanel.setEditable(false);
		if (exercice.exeStatFac().equalsIgnoreCase(EOExercice.EXERCICE_OUVERT)) {
			if (app.canUseFonction(EOFonction.DROIT_FACTURER, exercice)) {
				facturePapierTablePanel.setEditable(true);
			}
		} else {
			if (exercice.exeStatFac().equalsIgnoreCase(EOExercice.EXERCICE_RESTREINT)) {
				if (app.canUseFonction(EOFonction.DROIT_FACTURER_PERIODE_BLOCAGE, exercice)) {
					facturePapierTablePanel.setEditable(true);
				}
			}
		}
		updateData();
	}

	public void updateData() {
		if (atLeastOnSearchHasBeenMade) {
			long start = System.currentTimeMillis();
			app.superviseur().setWaitCursor(this, true);
			Integer fetchLimit = null;
			try {
				fetchLimit = Integer.valueOf((String) listMaxResult.getSelectedItem());
			} catch (NumberFormatException e) {
				//on ne fait rien, on laisse fetchlimit à null
			}
			facturePapierTablePanel.reloadData(updateFilteringQualifier(), fetchLimit);

			updateInterfaceEnabling();
		setWaitCursor(false);
		System.out.println("Chargement factures : " + (System.currentTimeMillis() - start) + "ms");
		}
	}

	private void updateInterfaceEnabling() {
		EOFacturePapier facturePapierSelected = (EOFacturePapier) facturePapierTablePanel.selectedObject();
		if (facturePapierSelected != null) {
			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(facturePapierSelected)));
		}
		EOUtilisateur loggedUser = app.appUserInfo().utilisateur();
		boolean canManage = facturePapierTablePanel.isEditable();
		boolean hasPermissionOrganPrest = true;
		if (facturePapierSelected != null && facturePapierSelected.organ() != null) {
			hasPermissionOrganPrest = facturePapierSelected.organ().hasPermission(loggedUser);
			canManage &= hasPermissionOrganPrest;
		}
		boolean hasPermissionOrganClient = hasPermissionOrganPrest;
		if (facturePapierSelected != null
				&& isPrestationInterne(facturePapierSelected)
				&& facturePapierSelected.prestation() != null
				&& facturePapierSelected.prestation().prestationBudgetClient() != null
				&& facturePapierSelected.prestation().prestationBudgetClient().organ() != null) {
			hasPermissionOrganClient = facturePapierSelected.prestation().prestationBudgetClient().organ().hasPermission(loggedUser);
		}

		actionValideClient.setEnabled((canManage || hasPermissionOrganClient)
				&& facturePapierSelected != null
				&& facturePapierSelected.typeEtat().equals(FinderTypeEtat.typeEtatValide(ec)));
		if (facturePapierSelected != null && facturePapierSelected.fapDateValidationClient() != null) {
			actionValideClient.putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
			actionValideClient.putValue(AbstractAction.SHORT_DESCRIPTION,
					"D\u00E9valider la facture c\u00F4t\u00E9 client (facture valid\u00E9e par "
							+ (facturePapierSelected.utilisateurValidationClient() != null
									&& facturePapierSelected.utilisateurValidationClient().personne_persNomPrenom() != null ? facturePapierSelected
									.utilisateurValidationClient().personne_persNomPrenom() : "???") + " le "
							+ facturePapierSelected.fapDateValidationClient().toLocaleString() + ")");
		} else {
			if (facturePapierSelected != null && facturePapierSelected.isValidableClient()) {
				actionValideClient.putValue(AbstractAction.SMALL_ICON, Constants.ICON_NOTVALID_16);
			} else {
				actionValideClient.putValue(AbstractAction.SMALL_ICON, Constants.ICON_NOTVALID_WARNING_16);
			}
			actionValideClient.putValue(AbstractAction.SHORT_DESCRIPTION, "Valider la facture c\u00F4t\u00E9 client");
		}

		// TODO: voir ici pour les droits de validation de la facture
		actionValidePrest.setEnabled(canManage
				&& facturePapierSelected != null
				&& facturePapierSelected.typeEtat().equals(FinderTypeEtat.typeEtatValide(ec)));

		if (facturePapierSelected != null && facturePapierSelected.fapDateValidationPrest() != null) {
			actionValidePrest.putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
			actionValidePrest.putValue(AbstractAction.SHORT_DESCRIPTION,
					"D\u00E9valider la facture c\u00F4t\u00E9 prestataire (= supprimer la recette) (facture valid\u00E9e par "
							+ (facturePapierSelected.utilisateurValidationPrest() != null
									&& facturePapierSelected.utilisateurValidationPrest().personne_persNomPrenom() != null ? facturePapierSelected.utilisateurValidationPrest()
									.personne_persNomPrenom() : "???") + " le " + facturePapierSelected.fapDateValidationPrest().toLocaleString() + ")");
		} else {
			if (facturePapierSelected != null && facturePapierSelected.isValidablePrest()) {
				actionValidePrest.putValue(AbstractAction.SMALL_ICON, Constants.ICON_NOTVALID_16);
			} else {
				actionValidePrest.putValue(AbstractAction.SMALL_ICON, Constants.ICON_NOTVALID_WARNING_16);
			}
			actionValidePrest.putValue(AbstractAction.SHORT_DESCRIPTION, "Valider la facture c\u00F4t\u00E9 prestataire (= recetter)");
		}

		actionPrint.setEnabled(hasPermissionOrganPrest
				&& facturePapierSelected != null
				&& (facturePapierSelected.typeEtat().equals(FinderTypeEtat.typeEtatValide(ec))
						|| facturePapierSelected.typeEtat().equals(FinderTypeEtat.typeEtatNon(ec))));

		actionMail.setEnabled(hasPermissionOrganPrest
				&& facturePapierSelected != null
				&& (facturePapierSelected.typeEtat().equals(FinderTypeEtat.typeEtatValide(ec))
						|| facturePapierSelected.typeEtat().equals(FinderTypeEtat.typeEtatNon(ec))));

		// Gestion echeancier
		boolean hasEcheancierNational = false;
		if (facturePapierSelected != null) {
			hasEcheancierNational = facturePapierSelected.hasEcheancierNational();
		}

		boolean isEcheancierEnabled = canManage && facturePapierSelected != null
				&& (facturePapierSelected.typeEtat().equals(FinderTypeEtat.typeEtatValide(ec))
						|| facturePapierSelected.typeEtat().equals(FinderTypeEtat.typeEtatNon(ec)))
				&& facturePapierSelected.modeRecouvrement() != null
				&& facturePapierSelected.modeRecouvrement().isEcheancier();

		btnEcheancier.setEnabled(isEcheancierEnabled);
		actionEcheancier.setEnabled(isEcheancierEnabled);
		actionEcheancierSepa.setEnabled(isEcheancierEnabled);

		boolean hasEcheancierSepa = false;
		if (isEcheancierEnabled && facturePapierSelected != null) {
            hasEcheancierSepa = facturePapierSelected.hasEcheancierSepa(ec);
		}
		
		if (facturePapierSelected != null && (hasEcheancierNational || hasEcheancierSepa)) {
			btnEcheancier.setIcon(Constants.ICON_EDIT2_16);
			btnEcheancier.setToolTipText("Editer l'\u00E9ch\u00E9ancier existant...");
		} else {
			btnEcheancier.setIcon(Constants.ICON_EDIT_16);
			btnEcheancier.setToolTipText("Calculer un \u00E9ch\u00E9ancier sur cette facture...");
		}

		// handle organ permission
		facturePapierTablePanel.actionDelete.setEnabled(hasPermissionOrganPrest);
		app.updateMenuDuplicataFacturePapier(hasPermissionOrganPrest);
	}

	public boolean canQuit() {
		return FacturePapierAddUpdate.sharedInstance().canQuit();
	}

	public EOFacturePapier getSelected() {
		return (EOFacturePapier) facturePapierTablePanel.selectedObject();
	}

	public void setSelected(EOFacturePapier facturePapier) {
		facturePapierTablePanel.setSelectedObject(facturePapier);
	}

	public void setSelected(NSArray facturePapiers) {
		facturePapierTablePanel.setSelectedObjects(facturePapiers);
	}

	private SelectValidatorMessages validateFournisseur(EOFacturePapier facturePapier) {
		// reteste les fournisseurs associé a la personne.
		SelectValidatorMessages messages = new SelectValidatorMessages();
		for (SelectValidator fournisValidator : fournisValidators) {
			fournisValidator.validate(messages, facturePapier.personne());
		}

		if (!messages.hasErrorMessages()) {
			// test le fournisseur associe a la facture
			EOFournisUlr fournisseur = facturePapier.fournisUlr();
			if (fournisseur == null) {
				messages.addMessage(new SelectValidatorMessage(
						SelectValidatorSeverity.ERROR, FournisseurRequisValidator.ERR_NON_FOURNISSEUR));
			} else if (fournisseur.isEtatInvalide()) {
				messages.addMessage(new SelectValidatorMessage(
						SelectValidatorSeverity.ERROR, FournisseurEtatValideValidator.ERR_FOURNISSEUR_ETAT_INVALIDE));
			} else if (!fournisseur.isEtatValide()) {
				messages.addMessage(new SelectValidatorMessage(
						SelectValidatorSeverity.ERROR, FournisseurEtatValideValidator.ERR_ONPRINT_FOURNISSEUR_ETAT_VALIDATION_EN_COURS));
			}
		}
		return messages;
	}

	private boolean isPrestationInterne(EOFacturePapier facturePapier) {
		return facturePapier != null
				&& facturePapier.typePublic() != null
				&& facturePapier.typePublic().typeApplication().equals(
						FinderTypeApplication.typeApplicationPrestationInterne(ec));
	}



	private NSArray updateFilteringQualifier() {
		NSMutableArray andQuals = new NSMutableArray();
		if (tfFapNumero.getNumber() != null) {
			andQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOFacturePapier.FAP_NUMERO_KEY + " = %@",
					new NSArray(tfFapNumero.getNumber())));
		}
		if (tfFapLib.getText() != null) {
			String searchLibelleString = "*" + tfFapLib.getText() + "*";
			NSMutableArray quals = new NSMutableArray();

			// libellé
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOFacturePapier.FAP_LIB_KEY + " caseInsensitiveLike %@",
					new NSArray(searchLibelleString)));

			// client
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOFacturePapier.PERSONNE_PERS_NOM_PRENOM_KEY + " caseInsensitiveLike %@",
					new NSArray(searchLibelleString)));

			// prestataire
			quals.addObject(Qualifiers.getQualifierFactory(QualifierKey.FAP_FOUR_PREST_PRENOM_PERSONNE).build(
					EOQualifier.QualifierOperatorCaseInsensitiveLike, searchLibelleString));
			quals.addObject(Qualifiers.getQualifierFactory(QualifierKey.FAP_FOUR_PREST_NOM_PERSONNE).build(
					EOQualifier.QualifierOperatorCaseInsensitiveLike, searchLibelleString));
			andQuals.addObject(new EOOrQualifier(quals));
		}

		if (cbFournisUlr.getSelectedEOObject() != null) {
			andQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOFacturePapier.FOURNIS_ULR_PREST_KEY + " = %@",
					new NSArray((EOFournisUlr) cbFournisUlr.getSelectedEOObject())));
		}
		if (!checkBoxFacturesRecettees.isSelected()) {
			andQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOFacturePapier.FACTURE_KEY + " = nil", null));
		}
		if (getMonFactureRechercheAvance().getResultat() != null
				&& getMonFactureRechercheAvance().getResultat().count() > 0) {
			andQuals.addObjectsFromArray(getMonFactureRechercheAvance().getResultat());
		}
		return andQuals;
	}

	private void onClose() {

		this.setVisible(false);
	}

	private void onSearchFapNumero() {
		tfFapLib.setText(null);
		cbFournisUlr.setSelectedEOObject(null);
		updateData();
		//updateFiltering();

	}

	private void onPrint() {
		if (facturePapierTablePanel.selectedObject() == null) {
			return;
		}
		EOFacturePapier facturePapier = (EOFacturePapier) facturePapierTablePanel.selectedObject();

		// si facture anglais pr\u00E9sente, on demande en quoi on veut imprimer...
		String sixId = app.getParam(SIXID_PIE_FACTURE_ANGLAIS_PARAM);
		if (sixId != null && ServerProxy.checkMaquetteExists(ec, sixId)
				&& !app.showConfirmationDialog("Dialecte?", "Imprimer la facture en Fran\u00E7ais ou en Anglais ?", "Fran\u00E7ais", "Anglais")) {
			ReportFactoryClient.printFacturePapierAnglais(facturePapier, null);
		} else {
			ReportFactoryClient.printFacturePapier(facturePapier, null);
		}
	}

	private void onValidePrestataire() {
		EOFacturePapier facturePapier = (EOFacturePapier) facturePapierTablePanel.selectedObject();
		try {
			if (facturePapier.updateTauxProrata()) {
				ec.saveChanges();
			}
			if (facturePapier.fapDateValidationPrest() == null) {
				FactoryFacturePapier.validePrest(ec, facturePapier, app.appUserInfo().utilisateur());
			} else {
				FactoryFacturePapier.devalidePrest(ec, facturePapier, app.appUserInfo().utilisateur());
			}
		} catch (Exception ex) {
			app.showErrorDialog(ex);
		}
		FactureRecette.sharedInstance().updateData();
		facturePapierTablePanel.updateData();
	}

	private void onMail() {
		if (facturePapierTablePanel.selectedObject() == null) {
			return;
		}
		EOFacturePapier facturePapier = (EOFacturePapier) facturePapierTablePanel.selectedObject();
		Mail.sharedInstance().open(facturePapier);

	}

	private void onEcheancier() {
		if (facturePapierTablePanel.selectedObject() == null) {
			return;
		}
		final EOFacturePapier fap = (EOFacturePapier) facturePapierTablePanel.selectedObject();

		if (!assertCreationEcheancierPossible(fap)) {
		    return;
		}

		try {
			NSTimestamp contrainteDate1ereEcheance = null, contrainteDateDerniereEcheance = null;

			// si c'est de la Formation Continue, traitement sp\u00E9cial...
			if (fap.typePublic().equals(FinderTypePublic.typePublicFormationContinue(ec)) && fap.facturePapierLignes() != null) {
				NSTimestamp[] contrainteDates = fap.companion().getDateDebutEtFinFormationContinue();
				contrainteDate1ereEcheance = contrainteDates[0];
				contrainteDateDerniereEcheance = contrainteDates[1];
			}

			final org.cocktail.echeancier.client.ui.eocontroller.EcheancierController controller;
			String title;
			if (fap.echeId() == null) { // nouvel \u00E9ch\u00E9ancier
				BigDecimal montantPremiereEcheance = null;
				// si facture FC, montant premi\u00E9re \u00E9ch\u00E9ance par d\u00E9faut \u00E9 1 tiers du total...
				if (fap.typePublic().equals(FinderTypePublic.typePublicFormationContinue(ec))) {
					montantPremiereEcheance = fap.fapTotalTtc().divide(new BigDecimal(3.0), BigDecimal.ROUND_HALF_UP);
				}
				controller = new org.cocktail.echeancier.client.ui.eocontroller.EcheancierControllerUtil().creationEcheancierPrelevement(
						ec,
						fap.fapLib(), fap.fapTotalTtc(), contrainteDate1ereEcheance != null ? contrainteDate1ereEcheance : new NSTimestamp(),
						montantPremiereEcheance, fap.ribfourUlr().ribOrdre(), true);
				title = "Nouvel \u00E9ch\u00E9ancier";
			} else {
				if (fap.fapDateValidationPrest() == null) {
					// modif
					// demande edition ou suppression
					if (!app.showConfirmationDialog("Que doit-on faire ?",
							"Un \u00E9ch\u00E9ancier existe d\u00E9j\u00E0 sur cette facture... voulez-vous l'\u00E9diter ou le supprimer ?", "Editer l'\u00E9ch\u00E9ancier",
							"Supprimer l'\u00E9ch\u00E9ancier")) {
						if (app.showConfirmationDialog("huh?", "Certain de vouloir supprimer cet \u00E9ch\u00E9ancier ?", "Oui... SUPPRIMER !",
								"Non, je me suis plant\u00E9 !")) {
							new org.cocktail.echeancier.client.process.EcheancierProcess(ec, Boolean.TRUE).supprimerEcheancier(fap.echeId());
							fap.setEcheId(null);
							ec.saveChanges();
							facturePapierTablePanel.updateData();
						}
						return;
					}
					controller = new org.cocktail.echeancier.client.ui.eocontroller.EcheancierControllerUtil().ouvertureEcheancier(ec, fap.echeId(),
							false, true);
					title = "Edition d'un \u00E9ch\u00E9ancier";
				} else {
					// consultation
					controller = new org.cocktail.echeancier.client.ui.eocontroller.EcheancierControllerUtil().ouvertureEcheancier(ec, fap.echeId(),
							true, true);
					title = "Consultation d'un \u00E9ch\u00E9ancier";
				}
			}

			Number persId = (Number) org.cocktail.kava.client.ServerProxy.primaryKeyForObject(ec, fap.personne()).valueForKey(EOPersonne.PRIMARY_KEY_KEY);
			Number persIdFournis = (Number) org.cocktail.kava.client.ServerProxy.primaryKeyForObject(ec, fap.fournisUlrPrest().personne()).valueForKey(
					EOPersonne.PRIMARY_KEY_KEY);
			Number orgId = (Number) org.cocktail.kava.client.ServerProxy.primaryKeyForObject(ec, fap.organ()).valueForKey(EOOrgan.PRIMARY_KEY_KEY);
			Number ribOrdre = (Number) org.cocktail.kava.client.ServerProxy.primaryKeyForObject(ec, fap.ribfourUlr()).valueForKey(EORibfourUlr.PRIMARY_KEY_KEY);

			controller.setDatePremiereEcheanceContrainte(contrainteDate1ereEcheance);
			controller.setDateDerniereEcheanceContrainte(contrainteDateDerniereEcheance);

			// recherche de l'adresse de la banque du client, creation eventuelle si necessaire
			AdresseBanqueBox adresseBanqueBox = new AdresseBanqueBox(ec);
			STRepartBanqueAdresse repart = adresseBanqueBox.chercherAdresseBanque(ribOrdre, true);
			Debug.printRecord("repart banque adresse from AdresseBanqueUtil", repart);

			controller.setEcheancierReporterParam(new org.cocktail.echeancier.client.ui.report.EcheancierReporterParam(ec, persId, persIdFournis,
					orgId));

			controller.setAutorisationPrelevementReporterParam(new org.cocktail.echeancier.client.ui.report.AutorisationPrelevementReporterParam(ec,
					persId, persIdFournis, orgId, ribOrdre));

			controller.addControllerDelegate(new DateDebutEcheancierController());
			controller.addControllerDelegate(new org.cocktail.echeancier.client.ui.eocontroller.common.ControllerDelegate() {
				public void controllerDidCancel() {
					super.controllerDidCancel();
				}

				public void controllerDidValidate(Object object) {
					super.controllerDidValidate(object);
					if (fap.echeId() == null) {
						fap.setEcheId((Integer) object);
						ec.saveChanges();
						facturePapierTablePanel.updateData();
					}
				}
			});
			controller.runController(title);
		} catch (NoSuchMethodError nsme) {
			app.showErrorDialog("L'appel au framework Echeancier ne fonctionne pas, il doit y avoir une "
					+ "incompatibilit\u00E9 de version entre l'application et le framework !");
			return;
		} catch (Exception e) {
			app.showErrorDialog(e);
			return;
		}
	}

	private void onEcheancierSepa() {
		if (!app.canUseFonction(EOFonction.DROIT_GESTION_MANDATS_SEPA, null) && !app.canUseFonction(EOFonction.DROIT_GESTION_MANDATS_SEPA_MARACUJA, null)) {
			app.showErrorDialog("Vous n'avez pas les droits pour la gestion des mandats SEPA SDD");
			return;
		}

		final EOFacturePapier currentFacturePapier = (EOFacturePapier) facturePapierTablePanel.selectedObject();

		if (!assertCreationEcheancierSepaPossible(currentFacturePapier)) {
			return;
		}

		try {
			openMandatSepaWindow();
			updateInterfaceEnabling();
		} catch (Exception e) {
			app.showErrorDialog(e);
			return;
		}
	}

	private boolean assertCreationEcheancierPossible(EOFacturePapier facturePapier) {
	    if (!facturePapierExiste(facturePapier)) {
	        app.showErrorDialog("Il faut sélectionner une facture pour cette action.");
            return false;
	    }

	    if (!ribExiste(facturePapier)) {
	        app.showErrorDialog("Il faut un rib client pour cr\u00E9er un \u00E9ch\u00E9ancier !");
            return false;
	    }
	    
	    if (!organigrammeBudgetaireExiste(facturePapier)) {
	        app.showErrorDialog("Il faut une ligne budg\u00E9taire recette pour cr\u00E9er un \u00E9ch\u00E9ancier !");
            return false;
	    }

		return true;
	}
	
	private boolean assertCreationEcheancierSepaPossible(EOFacturePapier facturePapier) {
	    if (!facturePapierExiste(facturePapier)) {
            app.showErrorDialog("Il faut sélectionner une facture pour cette action.");
            return false;
        }
        
        if (!organigrammeBudgetaireExiste(facturePapier)) {
            app.showErrorDialog("Il faut une ligne budg\u00E9taire recette pour cr\u00E9er un \u00E9ch\u00E9ancier !");
            return false;
        }

        return true;
    }
	
	protected boolean facturePapierExiste(EOFacturePapier facturePapier) {
	    return facturePapier != null;
	}
	
	protected boolean ribExiste(EOFacturePapier facturePapier) {
	    if (!facturePapierExiste(facturePapier)) {
	        return false;
	    }
	    
	    return facturePapier.ribfourUlr() != null;
	}
	
	protected boolean organigrammeBudgetaireExiste(EOFacturePapier facturePapier) {
	    if (!facturePapierExiste(facturePapier)) {
            return false;
        }
        
        return facturePapier.organ() != null;
	}
	
	

	private void openMandatSepaWindow() {
		final EOFacturePapier currentFacturePapier = (EOFacturePapier) facturePapierTablePanel.selectedObject();
		final SepaSddMandatCtrl mandatCtrl = new SepaSddMandatCtrl(ec);
		Map<OrigineComplements, Object> origineFactureComplements =
				FacturePapierService.instance().origineFactureComplements(ec, currentFacturePapier);
		mandatCtrl.openDialog(
				app.getMainWindow(), true,
				ComptaEntityHelper.toFacturePapier(ec, currentFacturePapier),
				ISepaSddOrigineType.FACTURE, origineFactureComplements);
	}

	private void onLegend() {
		JFrame frame = new JFrame("L\u00E9gende des couleurs");
		Box contentPanel = Box.createVerticalBox();

		JPanel panel1 = new JPanel(new FlowLayout(FlowLayout.LEFT, 20, 0));
		panel1.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		JTextField label1 = new JTextField("Texte", 6);
		label1.setBorder(null);
		label1.setEditable(false);
		label1.setBackground(null);
		label1.setForeground(configurationCouleur.factureNonValideClientForeground(app));
		panel1.add(label1);
		panel1.add(new JLabel("Facture non valid\u00E9e."));
		contentPanel.add(panel1);

		JPanel panel2 = new JPanel(new FlowLayout(FlowLayout.LEFT, 20, 0));
		panel2.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		JTextField label2 = new JTextField("Texte", 6);
		label2.setBorder(null);
		label2.setEditable(false);
		label2.setBackground(null);
		label2.setForeground(configurationCouleur.factureValideClientForeground(app));
		panel2.add(label2);
		panel2.add(new JLabel("Facture valid\u00E9e par le client."));
		contentPanel.add(panel2);

		JPanel panel3 = new JPanel(new FlowLayout(FlowLayout.LEFT, 20, 0));
		panel3.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		JTextField label3 = new JTextField("", 6);
		label3.setBorder(null);
		label3.setEditable(false);
		label3.setBackground(configurationCouleur.factureValidePrestataireBackground(app));
		panel3.add(label3);
		panel3.add(new JLabel("Facture valid\u00E9e par le prestataire (= recett\u00E9e)."));
		contentPanel.add(panel3);

		frame.setContentPane(contentPanel);
		frame.setLocationRelativeTo(app.superviseur());
		frame.setResizable(false);
		frame.validate();
		frame.pack();
		frame.show();
	}

	private class FacturePapierTablePanel extends ZExtendedTablePanel {

		public FacturePapierTablePanel() {
			super(null);

			ZEOTableModelColumn factureNumeroColonne = new ZEOTableModelColumn(
					getDG(), EOFacturePapier.FAP_NUMERO_KEY, "No", 50, app.getNumeroFormatDisplay());
			factureNumeroColonne.setAlignment(SwingConstants.LEFT);
			addCol(factureNumeroColonne);
			ZEOTableModelColumn prestationNumeroColonne = new ZEOTableModelColumn(
					getDG(), EOFacturePapier.PRESTATION_KEY + "." + EOPrestation.PREST_NUMERO_KEY, "Prestation", 50, app.getNumeroFormatDisplay());
			prestationNumeroColonne.setAlignment(SwingConstants.LEFT);
			addCol(prestationNumeroColonne);

			addCol(new ZEOTableModelColumn(getDG(), EOFacturePapier.FAP_REF_KEY, "Ref", 90));
			addCol(new ZEOTableModelColumn(getDG(), EOFacturePapier.FAP_LIB_KEY, "Libell\u00E9", 200));
			addCol(new ZEOTableModelColumn(getDG(), EOFacturePapier.PERSONNE_PERS_NOM_PRENOM_KEY, "Client", 120));
			ZEOTableModelColumn col3 = new ZEOTableModelColumn(getDG(), EOFacturePapier.FAP_DATE_KEY, "Date", 50, Constants.FORMAT_DATESHORT);
			col3.setColumnClass(NSTimestamp.class);
			addCol(col3);

			ZEOTableModelColumn col4 = new ZEOTableModelColumn(getDG(), EOFacturePapier.FAP_TOTAL_HT_KEY, "HT", 70, app.getCurrencyFormatDisplay());
			col4.setColumnClass(BigDecimal.class);
			col4.setAlignment(SwingConstants.RIGHT);
			addCol(col4);

			ZEOTableModelColumn col6 = new ZEOTableModelColumn(getDG(), EOFacturePapier.FAP_TOTAL_TTC_KEY, "TTC", 70, app.getCurrencyFormatDisplay());
			col6.setColumnClass(BigDecimal.class);
			col6.setAlignment(SwingConstants.RIGHT);
			addCol(col6);

			initGUI();

			getTablePanel().getTable().setMyRenderer(new DataTableCellRenderer());
		}

		//		public void reloadData() {
		//			EOUtilisateur utilisateur = app.appUserInfo().utilisateur();
		//			if (app.canUseFonction(EOFonction.DROIT_VOIR_TOUTES_LES_FACTURES_PAPIER, null)) {
		//				utilisateur = null;
		//			}
		//			setObjectArray(FinderFacturePapier.find(ec, app.superviseur().currentExercice(), utilisateur, null));
		//		}

		public void reloadData(NSArray quals, Integer fetchLimit) {
			NSMutableArray args = new NSMutableArray();
			args.addObject(EOQualifier.qualifierWithQualifierFormat("exercice = %@", new NSArray(app.superviseur().currentExercice())));
			EOUtilisateur utilisateur = app.appUserInfo().utilisateur();
			if (app.canUseFonction(EOFonction.DROIT_VOIR_TOUTES_LES_FACTURES_PAPIER, null)) {
				utilisateur = null;
			}

			if (utilisateur != null) {
				args.addObject(EOQualifier.qualifierWithQualifierFormat("utilisateur = %@", new NSArray(utilisateur)));
			}

			if (quals != null && quals.count() > 0) {
				args.addObjectsFromArray(quals);
			}

			setObjectArray(FinderFacturePapier.findWithLimit(ec, new EOAndQualifier(args), fetchLimit));
		}

		protected void onAdd() {
			try {
				app.superviseur().setWaitCursor(this, true);
				if (FacturePapierAddUpdate.sharedInstance().openNew(app.superviseur().currentExercice())) {
					EOFacturePapier fp = FacturePapierAddUpdate.sharedInstance().getLastOpened();
					ajouterInDg(fp, 0);
				}
			} catch (EntityInitializationException eie) {
				app.showErrorDialog(eie);
			}
 finally {
				app.superviseur().setWaitCursor(this, false);
			}
		}

		protected void onUpdate() {
			if (((EOFacturePapier) selectedObject()).typeEtat().equals(FinderTypeEtat.typeEtatAnnule(ec))) {
				app.showInfoDialog("Cette facture papier n'est pas valide, vous ne pouvez pas la modifier !");
				return;
			}
			app.superviseur().setWaitCursor(this, true);
			if (FacturePapierAddUpdate.sharedInstance().open((EOFacturePapier) selectedObject())) {
				fireSelectedTableRowUpdated();
			}
			app.superviseur().setWaitCursor(this, false);
		}

		protected void onDelete() {
			if (selectedObject() != null) {
				EOFacturePapier facture = (EOFacturePapier) selectedObject();
				if (!app.canUseFonction(EOFonction.DROIT_SUPPRIMER_FACTURES, facture.exercice())) {
					app.showInfoDialog("Vous n'\u00EAtes pas autoris\u00E9 \u00E0 supprimer des factures !");
					return;
				}
				if (facture.fapDateValidationPrest() != null) {
					app.showInfoDialog("Cette facture est valid\u00E9e par le prestataire, impossible de la supprimer !");
					return;
				}
				if (!app.showConfirmationDialog("ATTENTION", "Supprimer cette facture??", "OUI", "NON")) {
					return;
				}
				if (facture.echeId() != null) {
					if (!app.showConfirmationDialog("Be careful !",
							"Un \u00E9ch\u00E9ancier est rattach\u00E9 \u00E0 cette facture, il sera supprim\u00E9 en m\u00EAme temps... Tout supprimer ?", "OUI", "NON")) {
						return;
					}
				}
				try {
					if (facture.echeId() != null) {
						new org.cocktail.echeancier.client.process.EcheancierProcess(ec, Boolean.TRUE).supprimerEcheancier(facture.echeId());
					}
					FactoryFacturePapier.delFacturePapier(ec, facture, app.appUserInfo().utilisateur());
					//FacturePapier.this.updateData();
					deleteSelectionInDg();
				} catch (Exception e) {
					app.showErrorDialog(e);
					return;
				}
			}
		}

		protected void onSelectionChanged() {
			EOFacturePapier selected = (EOFacturePapier) facturePapierTablePanel.selectedObject();
			if (selected != null && selected.fapDateValidationPrest() != null && selected.facture() != null) {
				FactureRecette.sharedInstance().setSelected(selected.facture().recettes());
			}
			updateInterfaceEnabling();
		}

		private class DataTableCellRenderer extends ZEOTableCellRenderer {
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

				EOFacturePapier facturePapier = (EOFacturePapier) getTablePanel().getObjectAtRow(row);
				if (facturePapier.fapDateValidationClient() == null) {
					label.setForeground(configurationCouleur.factureNonValideClientForeground(app));
				} else {
					label.setForeground(configurationCouleur.factureValideClientForeground(app));
				}

				if (isSelected) {
					label.setBackground(table.getSelectionBackground());
				} else {
					if (facturePapier.fapDateValidationPrest() == null) {
						label.setBackground(table.getBackground());
					} else {
						label.setBackground(configurationCouleur.factureValidePrestataireBackground(app));
					}
				}
				return label;
			}
		}

	}

	private void initGUI() {
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));

		JPanel centerTablePanel = new JPanel(new BorderLayout());
		centerTablePanel.setBorder(BorderFactory.createEmptyBorder());
		centerTablePanel.add(facturePapierTablePanel, BorderLayout.CENTER);
		centerTablePanel.add(buildToolBar(), BorderLayout.LINE_END);

		add(buildSearchBar(), BorderLayout.NORTH);
		add(centerTablePanel, BorderLayout.CENTER);

		initInputMap();
	}

	private final JPanel buildSearchBar() {
		JPanel p = new JPanel(new BorderLayout());
		p.setBorder(BorderFactory.createLineBorder(Color.gray, 1));
		JPanel p1 = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
		p1.add(UIUtilities.labeledComponent("Num\u00E9ro", tfFapNumero, null));
		p1.add(Box.createHorizontalStrut(5));
		p1.add(UIUtilities.labeledComponent("Lib / Client / Prestataire", tfFapLib, null));
		p1.add(Box.createHorizontalStrut(5));
		p1.add(UIUtilities.labeledComponent("Prestataire", cbFournisUlr, null));
		checkBoxFacturesRecettees.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);
		checkBoxFacturesRecettees.setVerticalTextPosition(SwingConstants.TOP);
		checkBoxFacturesRecettees.setHorizontalTextPosition(SwingConstants.CENTER);
		checkBoxFacturesRecettees.setIconTextGap(0);
		checkBoxFacturesRecettees.setFocusPainted(false);
		checkBoxFacturesRecettees.setBorderPaintedFlat(true);
		p1.add(UIUtilities.labeledComponent(null, checkBoxFacturesRecettees, null));
		p1.add(UIUtilities.labeledComponent("Nombre de résultats", listMaxResult, null));
		p1.add(UIUtilities.labeledComponent(" ", makeButtonLib(actionSrch), null));
		p.add(p1, BorderLayout.LINE_START);

		p.add(new JPanel(), BorderLayout.CENTER);

		JPanel p2 = new JPanel(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		tfExercice.setViewOnly();
		tfExercice.setHorizontalAlignment(SwingConstants.CENTER);
		p2.add(UIUtilities.labeledComponent("Exercice", tfExercice, null));
		p.add(p2, BorderLayout.LINE_END);

		JPanel p3 = new JPanel(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		p3.add(UIUtilities.labeledComponent("Recherche Avancée", makeButton(actionRechercheAvance), null));
		p.add(p3, BorderLayout.LINE_END);

		JPanel finalPanel = new JPanel(new BorderLayout());
		finalPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 5, 20));
		finalPanel.add(p, BorderLayout.CENTER);
		return finalPanel;
	}

	private final JComponent buildToolBar() {
		btnEcheancier = makeEcheancierButton();
		JToolBar toolBarPanel = new JToolBar("Facture Toolbar", JToolBar.VERTICAL);
		toolBarPanel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		toolBarPanel.add(makeButton(actionValideClient));
		toolBarPanel.add(makeButton(actionValidePrest));
		toolBarPanel.addSeparator(new Dimension(20, 20));
		toolBarPanel.add(makeButton(actionMail));
		toolBarPanel.add(makeButton(actionPrint));
		toolBarPanel.addSeparator(new Dimension(20, 20));
		toolBarPanel.add(btnEcheancier);
		toolBarPanel.addSeparator(new Dimension(20, 20));
		toolBarPanel.addSeparator(new Dimension(20, 20));
		toolBarPanel.add(makeButton(actionLegend));

		return toolBarPanel;
	}

	private final JButton makeEcheancierButton() {
		Boolean prelevementNationalEnabled = CktlConfigUtil.getBooleanValueForConfig((String) ServerCallCompta.clientSideRequestGetConfig(ec, IFwkCktlComptaParam.PRELEVEMENTNATIONAL_ENABLED));
		Boolean prelevementSepaEnabled = CktlConfigUtil.getBooleanValueForConfig((String) ServerCallCompta.clientSideRequestGetConfig(ec, IFwkCktlComptaParam.SEPASDDMANDAT_ENABLED));
		JButton btnEcheancier;
		if (prelevementNationalEnabled && prelevementSepaEnabled) {
			btnEcheancier = new ZDropDownButton("", Constants.ICON_EDIT_16);
			((ZDropDownButton) btnEcheancier).addActionItem(actionEcheancier);
			((ZDropDownButton) btnEcheancier).addActionItem(actionEcheancierSepa);
		}
		else if (prelevementNationalEnabled) {
			btnEcheancier = new JButton(actionEcheancier);
		}
		else {
			btnEcheancier = new JButton(actionEcheancierSepa);
		}
		return decorateButton(btnEcheancier);
	}

	private JButton makeButton(Action a) {
		return UIUtilities.makeButton(a);
	}

	private JButton makeButtonLib(Action a) {
		return UIUtilities.makeButtonLib(a);
	}

	private JButton decorateButton(JButton button) {
		Dimension d = new Dimension(24, 24);
		button.setPreferredSize(d);
		button.setMaximumSize(d);
		button.setMinimumSize(d);
		button.setHorizontalAlignment(SwingConstants.CENTER);
		button.setBorderPainted(true);
		button.setFocusPainted(false);

		return button;
	}

	private void initInputMap() {
		getActionMap().put("ESCAPE", actionClose);
		getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "ESCAPE");
		getActionMap().put("F5", new ActionUpdate());
		getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0), "F5");
	}

	private class ActionValideClient extends AbstractAction {
		public ActionValideClient() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Valider / d\u00E9valider la facture c\u00F4t\u00E9 client");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				EOFacturePapier facturePapier = (EOFacturePapier) facturePapierTablePanel.selectedObject();
				if (facturePapier == null) {
					return;
				}
				try {
					if (facturePapier.updateTauxProrata()) {
						ec.saveChanges();
					}
					if (facturePapier.fapDateValidationClient() == null) {
						FactoryFacturePapier.valideClient(ec, facturePapier, app.appUserInfo().utilisateur());
					} else {
						FactoryFacturePapier.devalideClient(ec, facturePapier, app.appUserInfo().utilisateur());
					}
				} catch (Exception ex) {
					app.showErrorDialog(ex);
				}
				facturePapierTablePanel.updateData();
			}
		}
	}

	private class ActionValidePrestataire extends AbstractAction {
		public ActionValidePrestataire() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Valider la facture c\u00F4t\u00E9 prestataire");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				EOFacturePapier facturePapier = (EOFacturePapier) facturePapierTablePanel.selectedObject();
				if (facturePapier == null) {
					return;
				}

				SelectValidatorMessages messages = validateFournisseur((EOFacturePapier) facturePapierTablePanel.selectedObject());
				if (!messages.hasMessages()) {
					onValidePrestataire();
				} else {
					app.showErrorDialog(messages.buildErrorText());
				}
			}
		}
	}

	private class ActionPrint extends AbstractAction {
		public ActionPrint() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_PRINT_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer la facture papier");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				SelectValidatorMessages messages =
						validateFournisseur((EOFacturePapier) facturePapierTablePanel.selectedObject());
				if (!messages.hasMessages()) {
					onPrint();
				}
				else {
					app.showErrorDialog(messages.buildErrorText());
				}
			}
		}
	}

	private class ActionMail extends AbstractAction {
		public ActionMail() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_EMAIL_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Envoyer la facture par mail...");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onMail();
			}
		}
	}

	private class ActionEcheancier extends AbstractAction {
		public ActionEcheancier() {
			super("Echeancier National");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_EDIT_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Calculer un \u00E9ch\u00E9ancier sur cette facture");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onEcheancier();
			}
		}
	}

	private class ActionEcheancierSepa extends AbstractAction {
		public ActionEcheancierSepa() {
			super("Echeancier SEPA");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_EDIT_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Calculer un \u00E9ch\u00E9ancier SEPA sur cette facture");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onEcheancierSepa();
			}
		}
	}

	private class ActionLegend extends AbstractAction {
		public ActionLegend() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_COLORS_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "L\u00E9gende des couleurs");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onLegend();
			}
		}
	}

	private class ActionClose extends AbstractAction {
		public ActionClose() {
			super("Fermer");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CLOSE_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onClose();
			}
		}
	}

	private class ActionRechercheAvance extends AbstractAction {

		public ActionRechercheAvance() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.getIconForName(IconeCocktail.RECHERCHER));
		}

		public void actionPerformed(ActionEvent e) {
			getMonFactureRechercheAvance().setOpen(true);
			getMonFactureRechercheAvance().afficherFenetre();
		}
	}

	private class ActionSearchFapNumero extends AbstractAction {
		public ActionSearchFapNumero() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_SEARCH_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onSearchFapNumero();
			}
		}
	}

	private class ActionUpdate extends AbstractAction {
		public ActionUpdate() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_REFRESH_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Refresh");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				update();
			}
		}
	}

	private class ActionCheckFacturesRecettees extends AbstractAction {
		public ActionCheckFacturesRecettees() {
			super("Factures recett\u00E9es");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Voir les factures d\u00E9j\u00E0 recett\u00E9es");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				//				updateFiltering();
				getMonFactureRechercheAvance().setEnableSearchRecette(checkBoxFacturesRecettees.isSelected());
				//updateData();
			}
		}
	}


	private class CbActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			updateData();
		}
	}


	public void setWaitCursor(boolean bool) {
		app.superviseur().setWaitCursor(this, bool);
	}



	public FinderFactureRechercheAvance getMonFactureRechercheAvance() {
		if (monFactureRechercheAvance == null)
		{
			monFactureRechercheAvance = new FinderFactureRechercheAvance(app, 0, 0, 300, 200);
			monFactureRechercheAvance.setParent(this);
		}
		return monFactureRechercheAvance;
	}

	/**
	 * Client select implementation.
	 *
	 * @author flagouey
	 */
	private final class DuplicataFactureClientSelect extends ClientSelect {
		@Override
		protected void registerValidators() {
			super.registerValidators();
			addPersonneValidators(new FournisseurEtatValideValidator(
					SelectValidatorSeverity.WARNING, FournisseurEtatValideValidator.WARN_FOURNISSEUR_ETAT_INVALIDE));
		}
	}

	private final class DateDebutEcheancierController extends ControllerDelegate {
		private static final String ERR_ECHEANCE_DATE_DEB_ANTERIEURE =
				"Les dates d'échéances ne peuvent pas être antérieures à la date du jour.";

		@Override
		public void controllerDidCancel() {
			super.controllerDidCancel();
		}

		@Override
		public void controllerDidValidate(Object echeancierId) {
			super.controllerDidValidate(echeancierId);
			if (echeancierId instanceof Number) {
				try {
					Echeancier echeancier = chargerEcheancier((Number) echeancierId);
					verifierDateDebutEcheances(echeancier);
				} catch (EcheancierException ee) {
					// aucune validation car aucun echeancier (un log info serait un plus)
				}
			}
		}

		private Echeancier chargerEcheancier(Number echeancierId) throws EcheancierException {
			return Echeancier.objectWithPrimaryKey(ec, (Number) echeancierId);
		}

		private void verifierDateDebutEcheances(Echeancier echeancier) {
			NSArray details = echeancier.details();
			if (details != null && details.count() > 0) {
				NSTimestamp dateDuJourNormalise = getDateJourNormalise();
				for (int idxEcheances = 0; idxEcheances < details.count(); idxEcheances++) {
					EcheancierDetail detail = (EcheancierDetail) details.objectAtIndex(idxEcheances);
					verifierDateDebutEcheance(detail, dateDuJourNormalise);
				}
			}
		}

		private void verifierDateDebutEcheance(EcheancierDetail detail, NSTimestamp dateDuJourNormalise) {
			if (dateDuJourNormalise.compare(detail.echdDatePrevue()) > 0) {
				throw new NSValidation.ValidationException(ERR_ECHEANCE_DATE_DEB_ANTERIEURE);
			}
		}

		private NSTimestamp getDateJourNormalise() {
			final GregorianCalendar now = new GregorianCalendar();
			now.set(Calendar.HOUR_OF_DAY, 0);
			now.set(Calendar.MINUTE, 0);
			now.set(Calendar.SECOND, 0);
			now.set(Calendar.MILLISECOND, 0);
			return new NSTimestamp(now.getTime());
		}
	}

	public void afficheRechercheAvanceeIfOpen() {
		if (isRechercheAvanceeOpen()) {
			getMonFactureRechercheAvance().afficherFenetre();
		}
	}

	public boolean isRechercheAvanceeOpen() {
		if (monFactureRechercheAvance == null) {
			return false;
		}
		return getMonFactureRechercheAvance().isOpen();
	}

	public void masquerRechercheAvancee() {
		if (monFactureRechercheAvance == null) {
			return;
		}
		getMonFactureRechercheAvance().masquerFenetre();
	}



	private class ActionSrch extends AbstractAction {
		public ActionSrch() {
			super("Rechercher");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_SEARCH_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Lancer la recherche");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				atLeastOnSearchHasBeenMade = true;
				updateData();
			}
		}
	}

}
