/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client.select;

import java.math.BigDecimal;
import java.util.Set;

import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.kava.client.finder.FinderCatalogueArticle;
import org.cocktail.kava.client.finder.FinderTypeEtat;
import org.cocktail.kava.client.metier.EOArticle;
import org.cocktail.kava.client.metier.EOCatalogue;
import org.cocktail.kava.client.metier.EOCatalogueArticle;
import org.cocktail.kava.client.metier.EOTypeArticle;
import org.cocktail.kava.client.metier.EOTypePublic;
import org.cocktail.kava.client.metier.EOUtilisateur;

import app.client.tools.Constants;
import app.client.ui.ZDialogSelect;
import app.client.ui.table.ZEOTableModelColumn;
import app.client.ui.table.ZTablePanel;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

/**
 * Handle Article selection.
 */
public class CatalogueArticleSelect extends ZDialogSelect {

	/** Serial version Id. */
	private static final long serialVersionUID = 1L;
	/** Window title. */
	private static final String	WINDOW_TITLE	= "Article";
	/** Window Width. */
	private static final int	WINDOW_WIDTH	= 640;
	/** Window Height. */
	private static final int	WINDOW_HEIGHT	= 480;

	/**
	 * Public constructor.
	 */
	public CatalogueArticleSelect() {
		super(WINDOW_TITLE, true, WINDOW_WIDTH, WINDOW_HEIGHT);

		// Selection multiple autorisee...
		table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	}

	/**
	 * Open dialog..
	 * @param utilisateur current user.
	 * @param catalogue   catalogue instance.
	 * @param typePublic  typePublic.
	 * @param object      catalogueArticle instance.
	 * @return true if user valid ; false when cancel.
	 */
	public boolean open(EOUtilisateur utilisateur, EOCatalogue catalogue, EOTypePublic typePublic,
			EOCatalogueArticle object) {
		if (typePublic != null) {
			setInfo("<b>Sélection d'un article</b>\nVous ne voyez ici que les articles valides autorisé pour le <u>type de public "
					+ typePublic.typuLibelle() + "</u>.");
		} else {
			setInfo(null);
		}

		NSArray articlesValides = FinderCatalogueArticle.find(
				ec, catalogue, typePublic, FinderTypeEtat.typeEtatValide(ec));

		table.setObjectArray(articlesValides);

		update();
		if (object != null) {
			tfSearch.setText(null);
			table.setSelectedObject(object);
			table.getTable().scrollToSelection();
		}
		return open();
	}

	public boolean open(EOUtilisateur utilisateur, EOCatalogueArticle catalogueArticle, EOTypePublic typePublic,
			Set<EOTypeArticle> typesArticle, EOCatalogueArticle object) {

		if (typePublic != null) {
			setInfo("<b>S&egrave;lection d'une option/remise</b>\nVous ne voyez ici que les options et remises valides autoris&egrave;es pour le type de public <u>"
					+ typePublic.typuLibelle() + "</u>.");
		} else {
			setInfo(null);
		}

		table.setObjectArray(FinderCatalogueArticle.find(
				ec, catalogueArticle, typePublic, FinderTypeEtat.typeEtatValide(ec), typesArticle, null));

		update();

		if (object != null) {
			tfSearch.setText(null);
			table.setSelectedObject(object);
			table.getTable().scrollToSelection();
		}
		return open();
	}

	public NSArray getSelecteds() {
		return table.selectedObjects();
	}

	private final class TablePanel extends ZTablePanel {

		public TablePanel(ZTablePanel.IZTablePanelMdl listener) {
			super(listener);
			cols.clear();
			cols.add(new ZEOTableModelColumn(getDG(), EOCatalogueArticle.ARTICLE_KEY + "." + EOArticle.TYPE_ARTICLE_KEY + "."
					+ EOTypeArticle.TYAR_LIBELLE_KEY, "Type", 50));
			cols.add(new ZEOTableModelColumn(getDG(), EOCatalogueArticle.CAAR_REFERENCE_KEY, "Réference", 100));
			cols.add(new ZEOTableModelColumn(getDG(), EOCatalogueArticle.ARTICLE_KEY + "." + EOArticle.ART_LIBELLE_KEY, "Description", 200));
			ZEOTableModelColumn col4 = new ZEOTableModelColumn(getDG(), EOCatalogueArticle.CAAR_PRIX_HT_KEY, "HT", 50, app
					.getCurrencyPrecisionFormatDisplay());
			col4.setColumnClass(BigDecimal.class);
			col4.setAlignment(SwingConstants.RIGHT);
			cols.add(col4);
			ZEOTableModelColumn col5 = new ZEOTableModelColumn(getDG(), EOCatalogueArticle.CAAR_PRIX_TTC_KEY, "TTC", 50, app
					.getCurrencyPrecisionFormatDisplay());
			col5.setColumnClass(BigDecimal.class);
			col5.setAlignment(SwingConstants.RIGHT);
			cols.add(col5);
		}
	}

	protected boolean checkInputValues() {
		boolean isValid = true;
		try {
			if (getSelecteds() == null || getSelecteds().count() == 0) {
				throw new Exception("Veuillez sélectionner un article !");
			}
		} catch (Exception e) {
			app.showErrorDialog(e);
			return false;
		}
		return isValid;
	}

	protected void initTable() {
		table = new TablePanel(new TableListener());
		table.initGUI();

		table.getTable().setBackground(Constants.COLOR_COL_BGD_NORMAL);
		table.getTable().setSelectionBackground(Constants.COLOR_COL_BGD_SELECTED);
		table.getTable().setForeground(Constants.COLOR_COL_FGD_NORMAL);
		table.getTable().setSelectionForeground(Constants.COLOR_COL_FGD_SELECTED);

		// tri...
		table.getDisplayGroup().setSortOrderings(
				new NSArray(EOSortOrdering.sortOrderingWithKey(EOCatalogueArticle.CAAR_REFERENCE_KEY,
						EOSortOrdering.CompareCaseInsensitiveAscending)));
	}

	protected EOQualifier filteringQualifier(String searchString) {
		if (searchString == null) {
			return null;
		}
		return EOQualifier.qualifierWithQualifierFormat(EOCatalogueArticle.ARTICLE_KEY + "." + EOArticle.ART_LIBELLE_KEY
				+ " caseInsensitiveLike %@ or " + EOCatalogueArticle.CAAR_REFERENCE_KEY + " caseInsensitiveLike %@", new NSArray(new Object[] {
				"*" + searchString + "*", "*" + searchString + "*" }));
	}

}
