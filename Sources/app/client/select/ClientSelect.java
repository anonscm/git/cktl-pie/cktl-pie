/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */
package app.client.select;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;

import org.cocktail.kava.client.metier.EOAdresse;
import org.cocktail.kava.client.metier.EOFournisUlr;
import org.cocktail.kava.client.metier.EOIndividuUlr;
import org.cocktail.kava.client.metier.EOPersonne;
import org.cocktail.kava.client.metier.EORepartPersonneAdresse;
import org.cocktail.kava.client.metier.EOStructureUlr;
import org.cocktail.kava.client.service.PersonneService;

import app.client.select.validator.FournisseurRequisValidator;
import app.client.select.validator.FournisseurTypeRequisValidator;
import app.client.select.validator.PersonneRequisValidator;
import app.client.tools.Constants;
import app.client.ui.UIUtilities;
import app.client.ui.ZEOComboBox;
import app.client.ui.ZTextArea;
import app.client.ui.table.ZEOTableCellRenderer;
import app.client.ui.table.ZEOTableModelColumn;
import app.client.ui.table.ZExtendedTablePanel;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;

public class ClientSelect extends ContactSelect {

	private static final String	   WINDOW_TITLE	              = "Clients";

	private static final String    TABLE_PERSONNE_COLNAME     = "Personne";
	private static final String    TABLE_FOURNISVALID_COLNAME = "Validité";
	private static final String    TABLE_FOURNISTYPE_COLNAME  = "Type";

	private static final String    COLFOURNISTYPE_LABEL_TIERS            = "Tiers";
	private static final String    COLFOURNISTYPE_LABEL_FOURNISSEUR     = "Fournisseur";
	private static final String    COLFOURNISTYPE_LABEL_CLIENT          = "Client";
	private static final String    COLFOURNISTYPE_LABEL_NON_FOURNISSEUR = "Non Fournisseur";

	private static final String    COLFOURNISETAT_LABEL_VALIDE  = "Validé";
	private static final String    COLFOURNISETAT_LABEL_ATTENTE = "En attente";
	private static final String    COLFOURNISETAT_LABEL_INVALIDE = "Non valide";

	private static final String    SEPARATOR = ", ";
	private static final String    ERR_ADRESSE_OBLIGATOIRE = "L'adresse est obligatoire. "
			+ "Si aucune adresse n'existe, une mise à jour de ce fournisseur est nécessaire.";

	private ZTextArea			    labelAdresse;
	private ZEOComboBox		        cbAdresses;
	private JPanel                  panelAdresse;
	private CbAdresseActionListener	cbAdrActionListener;
	private EOAdresse               adresseSelected;

	private PersonneService personneService = PersonneService.instance();

	public ClientSelect() {
		super(WINDOW_TITLE);
	}

	@Override
	protected void registerValidators() {
		addPersonneValidators(
				new PersonneRequisValidator(),
				new FournisseurRequisValidator(),
				new FournisseurTypeRequisValidator());
	}

	@Override
	protected void init(int windowWidth, int windowHeight) {
		cbAdrActionListener = new CbAdresseActionListener();
		cbAdresses = new ZEOComboBox(new NSArray(), EORepartPersonneAdresse.ADRESSE_SHORT_KEY, null, null, null, 350);
		cbAdresses.addActionListener(cbAdrActionListener);
		labelAdresse = new ZTextArea(7, 60);
		super.init(windowWidth, windowHeight);
	}

	@Override
	protected ZExtendedTablePanel initTable() {
		ZExtendedTablePanel innerTable = new ClientTablePanel();
		// tri...
		innerTable.getTablePanel().getDisplayGroup().setSortOrderings(
				new NSArray(EOSortOrdering.sortOrderingWithKey(EOIndividuUlr.PERSONNE_PERS_NOM_PRENOM_KEY,
						EOSortOrdering.CompareCaseInsensitiveAscending)));
		return innerTable;
	}

	@Override
	protected void addCenterComponentsOnInitGUI(Box centralBox) {
		super.addCenterComponentsOnInitGUI(centralBox);

		Box tempPanel = Box.createVerticalBox();
		tempPanel.add(cbAdresses);
		tempPanel.add(Box.createVerticalStrut(5));
		labelAdresse.setViewOnly();
		tempPanel.add(UIUtilities.labeledComponent(null, labelAdresse, null));

		panelAdresse = UIUtilities.labeledComponent("Adresse a utiliser...", tempPanel, null);
		centralBox.add(panelAdresse);
	}

	@Override
	protected void resetAdditionalEntities() {
		super.resetAdditionalEntities();
		adresseSelected = null;
	}

	@Override
	protected boolean onOkAdditionalActions() {
		super.onOkAdditionalActions();
		EORepartPersonneAdresse personneAdresses = (EORepartPersonneAdresse) cbAdresses.getSelectedEOObject();
		if (personneAdresses == null || personneAdresses.adresse() == null) {
			app.showErrorDialog(ERR_ADRESSE_OBLIGATOIRE);
			return false;
		}
		adresseSelected = personneAdresses.adresse();
		return true;
	}

	/**
	 * @return adresse selected.
	 */
	public EOAdresse getAdresseSelected() {
		return adresseSelected;
	}


	private void updateAdresses(EOPersonne personne) {
		if (personne == null) {
			cbAdresses.setObjects(null);
			cbAdresses.clean();
			updateAdresse();
			return;
		}
		// on affiche les adresses de la personne + preselection
		cbAdresses.setObjects(personne.repartPersonneAdresses());
		EORepartPersonneAdresse defaultAdresse = personneService.defaultAdress(ec, personne);
		NSKeyValueCoding dico = defaultAdresse == null ? null
				: new NSDictionary(defaultAdresse.adresseShort(), EORepartPersonneAdresse.ADRESSE_SHORT_KEY);
		cbAdresses.setSelectedEOObject(dico);
		cbAdresses.setVisible(true);
	}

	private void updateAdresse() {
		String adresse = null;
		if (cbAdresses.getSelectedEOObject() != null) {
			adresse = getAdresse(((EORepartPersonneAdresse) cbAdresses.getSelectedEOObject()).adresse());
		}
		labelAdresse.setText(adresse);
	}

	private String getAdresse(EOAdresse adresse) {
		String s = "";
		if (adresse != null) {
			String adr1 = adresse.adrAdresse1();
			String adr2 = adresse.adrAdresse2();
			s = s + ((adr1 == null ? "" : adr1) + (adr2 == null ? "" : "\n" + adr2));
			// suite...
			s = s + "\n";
			String bp = adresse.adrBp();
			if (bp != null && !bp.equals("")) {
				s = s + bp + "\n";
			}
			String ville = adresse.ville();
			String cp = adresse.cpEtranger();
			if (cp != null && !cp.equals("")) {
				// adresse etrangere ==> cpEtranger ville \n pays
				s = s + cp + " " + (ville == null ? "" : ville);
				if (adresse.pays() != null) {
					String pays = adresse.pays().llPays();
					if (pays != null && !pays.equals("")) {
						s = s + "\n" + pays;
					}
				}
			} else {
				cp = adresse.codePostal();
				s = s + ((cp == null ? "" : cp) + " " + (ville == null ? "" : ville));
			}
		}
		return s;
	}

	private class CbAdresseActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			updateAdresse();
		}
	}

	private final class ClientTablePanel extends ZExtendedTablePanel {
		public ClientTablePanel() {
			super(null, false, false, false);
			ZEOTableModelColumn personneModelCol = new ZEOTableModelColumn(
					getDG(), EOIndividuUlr.PERSONNE_KEY, TABLE_PERSONNE_COLNAME);
			ZEOTableModelColumn fouValideModelCol = new ZEOTableModelColumn(
					getDG(), EOIndividuUlr.PERSONNE_KEY, TABLE_FOURNISVALID_COLNAME);
			ZEOTableModelColumn fouTypeModelCol = new ZEOTableModelColumn(
					getDG(), EOIndividuUlr.PERSONNE_KEY, TABLE_FOURNISTYPE_COLNAME);

			personneModelCol.setTableCellRenderer(new PersonneTableCellRenderer());
			fouValideModelCol.setTableCellRenderer(new FournisseurEtatTableCellRenderer());
			fouTypeModelCol.setTableCellRenderer(new FournisseurTypeTableCellRenderer());

			addCol(personneModelCol);
			addCol(fouValideModelCol);
			addCol(fouTypeModelCol);
			initGUI();
		}

		protected void onAdd() {
		}

		protected void onUpdate() {
		}

		protected void onDelete() {
		}

		protected void onSelectionChanged() {
			if (table.selectedObject() != null) {
				EOPersonne currentPersonne = null;
				if (rbStructures.isSelected()) {
					currentPersonne = ((EOStructureUlr) table.selectedObject()).personne();
				} else {
					currentPersonne = ((EOIndividuUlr) table.selectedObject()).personne();
				}
				updateAdresses(currentPersonne);
			} else {
				updateAdresses(null);
			}
		}

		public void onDbClick(final MouseEvent e) {
			actionOk.actionPerformed(null);
		}

		private class PersonneTableCellRenderer extends ZEOTableCellRenderer {
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				if (value instanceof EOPersonne) {
					EOPersonne personne = (EOPersonne) value;
					label.setText(personne.persNomPrenom() + " (id: " + personne.persId() + ")");
				}
				return label;
			}
		}

		/**
		 * Gere l'affichage de l'etat du fournisseur.
		 * @author flagouey
		 */
		private class FournisseurEtatTableCellRenderer extends ZEOTableCellRenderer {
			/** Serial version ID. */
			private static final long serialVersionUID = 1L;

			private Map<String, String> fournisEtatsMap;

			public FournisseurEtatTableCellRenderer() {
				super();
				initFournisEtatsLabelMap();
			}

			private void initFournisEtatsLabelMap() {
				this.fournisEtatsMap = new HashMap<String, String>();
				this.fournisEtatsMap.put(EOFournisUlr.FOU_ETAT_VALIDE,  COLFOURNISETAT_LABEL_VALIDE);
				this.fournisEtatsMap.put(EOFournisUlr.FOU_ETAT_ATTENTE, COLFOURNISETAT_LABEL_ATTENTE);
				this.fournisEtatsMap.put(EOFournisUlr.FOU_ETAT_INVALIDE, COLFOURNISETAT_LABEL_INVALIDE);
			}

			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				if (value instanceof EOPersonne) {
					EOPersonne personne = (EOPersonne) value;
					ImageIcon icon = null;
					String text = null;

					// recupere les fournisseurs
					NSArray fournisseurs = personne.fournisUlrs();

					// si    un seul fournisseur est rattache a cette personne, on determine l'affichage en fonction de son etat.
					// sinon si apres filtre (valide, en attent) il n'en reste qu'un on peur également déterminer l'affichage
					if (fournisseurs != null && fournisseurs.count() == 1) {
						EOFournisUlr uniqueFournisseur = (EOFournisUlr) fournisseurs.objectAtIndex(0);
						icon = preparerAffichageIcone(uniqueFournisseur);
						text = fournisEtatsMap.get(uniqueFournisseur.fouValide());
					} else {
						List<EOFournisUlr> fournisseursValidesEnAttente = personne.fournisseurs(
								EOPersonne.FILTRE_FOURNISSEUR_VALIDE, EOPersonne.FILTRE_FOURNISSEUR_EN_ATTENTE);
						if (fournisseursValidesEnAttente != null && fournisseursValidesEnAttente.size() == 1) {
							EOFournisUlr uniqueFournisseur = (EOFournisUlr) fournisseursValidesEnAttente.get(0);
							icon = preparerAffichageIcone(uniqueFournisseur);
							text = fournisEtatsMap.get(uniqueFournisseur.fouValide());
						}
					}
					label.setText(text);
					label.setIcon(icon);
				}
				return label;
			}

			private ImageIcon preparerAffichageIcone(EOFournisUlr fournisseur) {
				ImageIcon icon = null;
				if (EOFournisUlr.FOU_ETAT_VALIDE.equals(fournisseur.fouValide())) {
					icon = Constants.ICON_ITEM_VALID;
				} else if (EOFournisUlr.FOU_ETAT_ATTENTE.equals(fournisseur.fouValide())) {
					icon = Constants.ICON_ITEM_WARNING;
				} else if (EOFournisUlr.FOU_ETAT_INVALIDE.equals(fournisseur.fouValide())) {
					icon = Constants.ICON_ITEM_INVALID;
				}
				return icon;
			}
		}

		/**
		 * Gere l'affichage du type de fournisseur.
		 * @author flagouey
		 */
		private class FournisseurTypeTableCellRenderer extends ZEOTableCellRenderer {
			private Map<String, String> fournisTypeMap;

			public FournisseurTypeTableCellRenderer() {
				super();
				initFournisTypeLabelMap();
			}

			private void initFournisTypeLabelMap() {
				this.fournisTypeMap = new HashMap<String, String>();
				this.fournisTypeMap.put(EOFournisUlr.FOU_TYPE_TIERS, COLFOURNISTYPE_LABEL_TIERS);
				this.fournisTypeMap.put(EOFournisUlr.FOU_TYPE_FOURNISSEUR, COLFOURNISTYPE_LABEL_FOURNISSEUR);
				this.fournisTypeMap.put(EOFournisUlr.FOU_TYPE_CLIENT, COLFOURNISTYPE_LABEL_CLIENT);
			}

			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				if (value instanceof EOPersonne) {
					EOPersonne personne = (EOPersonne) value;
					ImageIcon icon = Constants.ICON_ITEM_INVALID;
					String text = COLFOURNISTYPE_LABEL_NON_FOURNISSEUR;

					// recupere les fournisseurs
					NSArray fournisseurs = personne.fournisUlrs();

					// si    un seul fournisseur est rattache a cette personne, on determine l'affichage en fonction de son type.
					// sinon on signale l'anomalie.
					if (fournisseurs != null && fournisseurs.count() == 1) {
						EOFournisUlr uniqueFournisseur = (EOFournisUlr) fournisseurs.objectAtIndex(0);
						icon = prepareAffichageIcone(uniqueFournisseur);
						text = prepareAffichageTexte(fournisTypeMap, uniqueFournisseur);
					} else if (fournisseurs != null && fournisseurs.count() > 1) {
						List<EOFournisUlr> fournisseursValidesEnAttente = personne.fournisseurs(
								EOPersonne.FILTRE_FOURNISSEUR_VALIDE, EOPersonne.FILTRE_FOURNISSEUR_EN_ATTENTE);
						if (fournisseursValidesEnAttente != null && fournisseursValidesEnAttente.size() == 1) {
							EOFournisUlr uniqueFournisseur = (EOFournisUlr) fournisseursValidesEnAttente.get(0);
							icon = prepareAffichageIcone(uniqueFournisseur);
							text = prepareAffichageTexte(fournisTypeMap, uniqueFournisseur);
						} else if (fournisseursValidesEnAttente != null && fournisseursValidesEnAttente.size() > 1) {
							StringBuilder valueAsString = new StringBuilder();
							for (int idx = 0; idx < fournisseursValidesEnAttente.size(); idx++) {
								EOFournisUlr currentFournisUlr = (EOFournisUlr) fournisseursValidesEnAttente.get(idx);
								valueAsString.append(fournisTypeMap.get(currentFournisUlr.fouType()))
								             .append(" (code: ").append(currentFournisUlr.fouCode()).append(")");
								if (idx < fournisseurs.count() - 1) {
									valueAsString.append(SEPARATOR);
								}
							}
							icon = Constants.ICON_WARNING;
							text = valueAsString.toString();
						}
					}
					label.setText(text);
					label.setIcon(icon);
				}
				return label;
			}

			private ImageIcon prepareAffichageIcone(EOFournisUlr fournisseur) {
				ImageIcon icon = null;
				if (EOFournisUlr.FOU_TYPE_CLIENT.equals(fournisseur.fouType())
						|| EOFournisUlr.FOU_TYPE_TIERS.equals(fournisseur.fouType())) {
					icon = Constants.ICON_ITEM_VALID;
				} else if (EOFournisUlr.FOU_TYPE_FOURNISSEUR.equals(fournisseur.fouType())) {
					icon = Constants.ICON_ITEM_WARNING;
				}
				return icon;
			}

			private String prepareAffichageTexte(Map<String, String> fournisseurTypeMap, EOFournisUlr fournisseur) {
				return fournisseurTypeMap.get(fournisseur.fouType()) + " (code: " + fournisseur.fouCode() + ")";
			}
		}
	}
}
