/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */
package app.client.select;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kava.client.EFournisseurType;
import org.cocktail.kava.client.finder.FinderIndividuUlr;
import org.cocktail.kava.client.finder.FinderStructureUlr;
import org.cocktail.kava.client.metier.EOFournisUlr;
import org.cocktail.kava.client.metier.EOIndividuUlr;
import org.cocktail.kava.client.metier.EOPersonne;
import org.cocktail.kava.client.metier.EOStructureUlr;
import org.cocktail.kava.client.metier.EOUtilisateur;

import app.client.ApplicationClient;
import app.client.select.validator.IndividuRequisValidator;
import app.client.select.validator.PersonneRequisValidator;
import app.client.select.validator.SelectValidator;
import app.client.select.validator.SelectValidatorMessages;
import app.client.tools.Constants;
import app.client.ui.UIUtilities;
import app.client.ui.ZLabel;
import app.client.ui.ZTextField;
import app.client.ui.table.ZEOTableModelColumn;
import app.client.ui.table.ZExtendedTablePanel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class ContactSelect extends JDialog {

	public static final int		   MROK			= 1;
	public static final int		   MRCANCEL		= 0;

	public static final String DEFAULT_INFO_MSG =
			"<b>Selection d'un client</b>\nVous pouvez le trouver parmi les structures ou individus enregistres dans l'annuaire et definir l'adresse à utiliser si plusieurs. Seuls les clients ou tiers sont sélectionnables.";
	public static final String ERR_FOURNISSEUR = "Une erreur est survenue lors de la sélection du fournisseur. Aucun fournisseur valide ou en attente de validation trouvé.";

	/** Min search text field length. */
	private static final int       MIN_SEARCH_FIELD_LENGTH = 2;

	/** Min search text field message. */
	private static final String MIN_SEARCH_FIELD_MSG = "La recherche s''effectue sur {0, number} caractères au moins.";

	private static final String	   WINDOW_TITLE	            = "Contacts";
	private static final int	   WINDOW_WIDTH	            = 720;
	private static final int	   WINDOW_HEIGHT	        = 450;
	private static final String    TABLE_PERSONNE_COLNAME   = "Personne";

	protected ApplicationClient	    app;
	protected EOEditingContext	    ec;
	protected int				    modalResult;
	protected JRadioButton		    rbStructures, rbIndividus;
	protected Action		    	actionOk, actionCancel;
	protected List<SelectValidator> personneValidators;
	protected List<SelectValidator> individuValidators;

	private int					    windowWidth, windowHeight;

	protected ZExtendedTablePanel	table;

	protected ZTextField		    tfSearch;

	private JButton	    			btOk;

	private ZLabel				    labelInfo;

	private ButtonGroup			    matrixTypeRecherche;

	private Action				    actionSearchStructure, actionSearchIndividu, actionSearchFournis;

	private JComboBox               cbFournisTypeFilter;

	private CbFournisListener       cbFournisListener;

	private EOStructureUlr		    structureUlr;

	private EOPersonne			    personneSelected;
	private EOIndividuUlr		    individuUlrSelected;
	private EOStructureUlr		    structureUlrSelected;
	private EOFournisUlr		    fournisUlrSelected;

	private EOExercice			    exercice;

	public ContactSelect() {
		this(WINDOW_TITLE);
	}

	public ContactSelect(String windowTitle) {
		super((JFrame) null, windowTitle, true);
		this.personneValidators = new ArrayList<SelectValidator>();
		this.individuValidators = new ArrayList<SelectValidator>();
		registerValidators();
		init(WINDOW_WIDTH, WINDOW_HEIGHT);
	}

	public boolean open(EOUtilisateur utilisateur, EOExercice exercice, EOStructureUlr structureUlr) {
		return open(utilisateur, exercice, structureUlr, false, true, false);
	}

	public boolean open(EOUtilisateur utilisateur, EOExercice exercice, EOStructureUlr structureUlr, boolean searchStructures,
			boolean searchIndividus, boolean searchFournis) {
		return open(utilisateur, exercice, structureUlr, searchStructures, searchIndividus, searchFournis, null);
	}

	public boolean open(EOUtilisateur utilisateur, EOExercice exercice, EOStructureUlr structureUlr, boolean searchStructures,
				boolean searchIndividus, boolean searchFournis, String textInfo) {
		if (!searchStructures && !searchIndividus) {
			return false;
		}

		this.exercice = exercice;

		actionSearchStructure.setEnabled(searchStructures);
		actionSearchIndividu.setEnabled(searchIndividus);
		actionSearchFournis.setEnabled(searchFournis);
		cbFournisTypeFilter.setVisible(searchFournis);

		this.structureUlr = structureUlr;

		if (!rbStructures.isSelected() && !rbIndividus.isSelected()) {
			// premier passage
			if (searchStructures) {
				rbStructures.setSelected(true);
			} else {
				rbIndividus.setSelected(true);
			}
		} else {
			rbStructures.setSelected(!searchIndividus || rbStructures.isSelected());
			rbIndividus.setSelected(!searchStructures || rbIndividus.isSelected());
		}

		onSearch();
		tfSearch.selectAndFocus();

		resetSelectedEntities();

		if (structureUlr == null) {
			labelInfo.setTextHtml(DEFAULT_INFO_MSG);
			labelInfo.setVisible(true);
		} else {
			labelInfo.setVisible(false);
		}

		if (textInfo != null) {
			labelInfo.setTextHtml(textInfo);
			labelInfo.setVisible(true);
		}

		modalResult = MRCANCEL;
		setSize(windowWidth, windowHeight);
		centerWindow();
		show();
		// en modal la fenetre reste active jusqu'au closeWindow...
		tfSearch.selectAndFocus();
		return modalResult == MROK;
	}

	public void updateLabelInfo(String labelInfo) {
		this.labelInfo.setTextHtml(labelInfo);
		this.labelInfo.setVisible(true);
	}

	public EOPersonne getSelected() {
		return personneSelected;
	}

	public EOIndividuUlr getSelectedIndividuUlr() {
		return individuUlrSelected;
	}

	public EOStructureUlr getSelectedStructureUlr() {
		return structureUlrSelected;
	}

	public EOFournisUlr getSelectedFournisUlr() {
		return fournisUlrSelected;
	}

	public EOPersonne getSelectedPersonne() {
		return personneSelected;
	}

	public void addPersonneValidators(SelectValidator... validators) {
		Collections.addAll(this.personneValidators, validators);
	}

	protected void registerValidators() {
		this.personneValidators.add(new PersonneRequisValidator());
		this.individuValidators.add(new IndividuRequisValidator());
	}

	protected void init(int windowWidth, int windowHeight) {
		this.windowWidth = windowWidth;
		this.windowHeight = windowHeight;

		app = (ApplicationClient) EOApplication.sharedApplication();
		ec = app.editingContext();

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

		actionOk = new ActionOk();
		actionCancel = new ActionCancel();
		actionSearchStructure = new ActionSearchStructure();
		actionSearchIndividu = new ActionSearchIndividu();
		actionSearchFournis = new ActionSearchFournis();

		matrixTypeRecherche = new ButtonGroup();

        cbFournisListener = new CbFournisListener();
		cbFournisTypeFilter = new JComboBox(EFournisseurType.labels().toArray());
		cbFournisTypeFilter.setSelectedItem(EFournisseurType.TOUS.getLabel());
		cbFournisTypeFilter.addActionListener(cbFournisListener);

		table = initTable();
		initGUI();
		initInputMap();

		addWindowListener(new LocalWindowListener());
	}

	/**
	 * Entry for specific reset actions.
	 */
	protected void resetAdditionalEntities() {
	}

	/**
	 * Entry for additional actions when onOk fired.
	 */
	protected boolean onOkAdditionalActions() {
		return true;
	}

	/**
	 *  Reset UI entities to null.
	 */
	private void resetSelectedEntities() {
		personneSelected = null;
		individuUlrSelected = null;
		structureUlrSelected = null;
		fournisUlrSelected = null;
		resetAdditionalEntities();
	}

	private final void centerWindow() {
		int screenWidth = (int) this.getGraphicsConfiguration().getBounds().getWidth();
		int screenHeight = (int) this.getGraphicsConfiguration().getBounds().getHeight();
		this.setLocation((screenWidth / 2) - ((int) this.getSize().getWidth() / 2),
				((screenHeight / 2) - ((int) this.getSize().getHeight() / 2)));
	}

	private void onSearch() {
		if (tfSearch.getText() != null	|| searchAutoEnabled()) {
			// check text length
			if (!validSearchField(tfSearch.getText())) {
				app.showErrorDialog(MessageFormat.format(MIN_SEARCH_FIELD_MSG, MIN_SEARCH_FIELD_LENGTH));
				return;
			}

			// si la selectBox est affiche, on filtre les resultats sinon cela ne rentre pas en compte.
			EFournisseurType fournisseurType = cbFournisTypeFilter.isVisible()
					? EFournisseurType.findByLabel((String) cbFournisTypeFilter.getSelectedItem())
							: EFournisseurType.TOUS;
			if (rbStructures.isSelected()) {
				table.setObjectArray(FinderStructureUlr.find(ec, tfSearch.getText(), null, fournisseurType));
			} else {
				table.setObjectArray(FinderIndividuUlr.find(ec, tfSearch.getText(), structureUlr, fournisseurType));
			}
		} else {
			table.setObjectArray(null);
		}
	}

	private boolean validSearchField(String text) {
		boolean isValid = true;
		if (text != null && text.length() < MIN_SEARCH_FIELD_LENGTH) {
			isValid = false;
		}
		return isValid;
	}

	private boolean searchAutoEnabled() {
		return cbFournisTypeFilter.isVisible()
				&& !EFournisseurType.FOURNISSEUR.getLabel().equals(cbFournisTypeFilter.getSelectedItem())
				&& rbIndividus.isSelected()
				&& structureUlr != null;
	}

	private void onOk() {
		if (table.selectedObject() == null) {
			app.showErrorDialog("Selectionnez un client!");
			return;
		}

		// initialisation.
		resetSelectedEntities();
		if (rbStructures.isSelected()) {
			personneSelected = ((EOStructureUlr) table.selectedObject()).personne();
			structureUlrSelected = (EOStructureUlr) table.selectedObject();
		} else {
			personneSelected = ((EOIndividuUlr) table.selectedObject()).personne();
			individuUlrSelected = (EOIndividuUlr) table.selectedObject();
		}

		SelectValidatorMessages prsMessages = runPersonneValidations(personneSelected);
		if (prsMessages.hasErrorMessages()) {
			app.showErrorDialog(prsMessages.errorMessages().get(0).getMessage());
			return;
		}
		SelectValidatorMessages indMessages = runIndividuValidations(individuUlrSelected);
		if (indMessages.hasErrorMessages()) {
			app.showErrorDialog(indMessages.errorMessages().get(0).getMessage());
			return;
		}

		if (prsMessages.hasWarnMessages()) {
			if (!app.showConfirmationDialog("Informations", prsMessages.buildWarnText(), "Continuer")) {
				return;
			}
		}

		// le fournisseur peut etre null.
		List<EOFournisUlr> fournisseurs = personneSelected.fournisseurs(
				EOPersonne.FILTRE_FOURNISSEUR_VALIDE, EOPersonne.FILTRE_FOURNISSEUR_EN_ATTENTE);
		if (fournisseurs != null) {
			fournisUlrSelected = (EOFournisUlr) fournisseurs.get(0);
		}

		if (!onOkAdditionalActions()) {
			return;
		}

		modalResult = MROK;
		tfSearch.selectAndFocus();
		hide();
	}

	private void onCancel() {
		modalResult = MRCANCEL;
		tfSearch.selectAndFocus();
		hide();
	}

	private SelectValidatorMessages runPersonneValidations(EOPersonne personne) {
		SelectValidatorMessages messages = new SelectValidatorMessages();
		for (SelectValidator validator : this.personneValidators) {
			validator.validate(messages, personne);
			if (messages.hasErrorMessages()) {
				break;
			}
		}
		return messages;
	}

	private SelectValidatorMessages runIndividuValidations(EOIndividuUlr individu) {
		SelectValidatorMessages messages = new SelectValidatorMessages();
		for (SelectValidator validator : this.individuValidators) {
			validator.validate(messages, individu);
			if (messages.hasErrorMessages()) {
				break;
			}
		}
		return messages;
	}

	protected ZExtendedTablePanel initTable() {
		ZExtendedTablePanel innerTable = new ContactTablePanel();
		// tri...
		innerTable.getTablePanel().getDisplayGroup().setSortOrderings(
				new NSArray(EOSortOrdering.sortOrderingWithKey(EOIndividuUlr.PERSONNE_PERS_NOM_PRENOM_KEY,
						EOSortOrdering.CompareCaseInsensitiveAscending)));
		return innerTable;
	}

	private void initGUI() {
		labelInfo = new ZLabel("", Constants.ICON_INFO_32, SwingConstants.LEADING);
		labelInfo.setBackground(Color.white);
		labelInfo.setOpaque(true);

		JPanel panelMatrix = new JPanel(new FlowLayout(FlowLayout.LEFT));
		rbStructures = new JRadioButton(actionSearchStructure);
		panelMatrix.add(rbStructures);
		matrixTypeRecherche.add(rbStructures);
		rbIndividus = new JRadioButton(actionSearchIndividu);
		panelMatrix.add(rbIndividus);
		matrixTypeRecherche.add(rbIndividus);

		Box recherchePanel = Box.createVerticalBox();
		recherchePanel.add(UIUtilities.labeledComponent("Rechercher parmi ...", panelMatrix, null, true));
		JPanel tempPanel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		tfSearch = new ZTextField(18);
		tempPanel2.add(UIUtilities.labeledComponent("Recherche...", tfSearch, new ActionSearch()));
		tempPanel2.add(UIUtilities.labeledComponent("Filtrer par :", cbFournisTypeFilter, null));
		recherchePanel.add(tempPanel2);

		JPanel topPanel = new JPanel(new BorderLayout());
		topPanel.add(labelInfo, BorderLayout.NORTH);
		topPanel.add(recherchePanel, BorderLayout.CENTER);

		Box centerPanel = Box.createVerticalBox();
		centerPanel.add(table);
		centerPanel.add(Box.createVerticalStrut(8));

		addCenterComponentsOnInitGUI(centerPanel);

		JPanel bottomPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		bottomPanel.add(new JButton(actionCancel));
		btOk = new JButton(actionOk);
		btOk.setRequestFocusEnabled(true);
		bottomPanel.add(btOk);

		JPanel p = new JPanel(new BorderLayout());
		p.add(topPanel, BorderLayout.NORTH);
		p.add(centerPanel, BorderLayout.CENTER);
		p.add(bottomPanel, BorderLayout.SOUTH);

		this.setContentPane(p);
		p.getRootPane().setDefaultButton(btOk);
		this.pack();
	}

	protected void addCenterComponentsOnInitGUI(Box centralBox) {
	}


	// private EOAdresse getEOAdresse(EOPersonne personne) {
	// NSArray listeAdresses = personne.repartPersonneAdresses();
	// // - Recherche de l'adresse principale
	// EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.RPA_PRINCIPAL_KEY + " = %@", new NSArray("O"));
	// NSArray adresse = EOQualifier.filteredArrayWithQualifier(listeAdresses, qualifier);
	// if (adresse.count() > 0) {
	// return ((EORepartPersonneAdresse) adresse.objectAtIndex(0)).adresse();
	// }
	// // - Recherche de l'adresse de fact
	// qualifier = EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TADR_CODE_KEY + " = %@", new NSArray("FACT"));
	// adresse = EOQualifier.filteredArrayWithQualifier(listeAdresses, qualifier);
	// if (adresse.count() > 0) {
	// return ((EORepartPersonneAdresse) adresse.objectAtIndex(0)).adresse();
	// }
	// // - Recherche de l'adresse pro
	// qualifier = EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TADR_CODE_KEY + " = %@", new NSArray("PRO"));
	// adresse = EOQualifier.filteredArrayWithQualifier(listeAdresses, qualifier);
	// if (adresse.count() > 0) {
	// return ((EORepartPersonneAdresse) adresse.objectAtIndex(0)).adresse();
	// }
	// // Recherche d'autres adresses que PRO ou FACT
	// if (listeAdresses != null && listeAdresses.count() > 0) {
	// return ((EORepartPersonneAdresse) listeAdresses.objectAtIndex(0)).adresse();
	// }
	// return null;
	// }

	private void initInputMap() {
		((JComponent) getContentPane()).getActionMap().put("ESCAPE", actionCancel);
		((JComponent) getContentPane()).getActionMap().put("CTRL_S", actionOk);
		((JComponent) getContentPane()).getActionMap().put("F10", actionOk);

		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("F8"), "F8");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("released F8"), "ALT_F8");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"ESCAPE");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
				KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK), "CTRL_S");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F10, 0), "F10");
	}

	private class LocalWindowListener implements WindowListener {
		public void windowOpened(WindowEvent arg0) {
		}

		public void windowClosed(WindowEvent arg0) {
		}

		public void windowIconified(WindowEvent arg0) {
		}

		public void windowDeiconified(WindowEvent arg0) {
		}

		public void windowActivated(WindowEvent arg0) {
		}

		public void windowDeactivated(WindowEvent arg0) {
		}

		public void windowClosing(WindowEvent arg0) {
			actionCancel.actionPerformed(null);
		}
	}

	private class ActionSearchStructure extends AbstractAction {
		public ActionSearchStructure() {
			super("Personnes morale");
			putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher parmi les personnes morales de l'annuaire");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onSearch();
			}
		}
	}

	private class ActionSearchIndividu extends AbstractAction {
		public ActionSearchIndividu() {
			super("Personnes physique/contacts");
			putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher parmi les personnes physique/contacts de l'annuaire");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onSearch();
			}
		}
	}

	private class ActionSearchFournis extends AbstractAction {
		public ActionSearchFournis() {
			super("Fournisseurs");
			putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher uniquement parmi ceux qui sont fournisseurs");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onSearch();
			}
		}
	}

	private class ActionSearch extends AbstractAction {
		public ActionSearch() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_SEARCH_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher...");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onSearch();
			}
		}
	}

	private class ActionOk extends AbstractAction {
		public ActionOk() {
			super("Valider");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onOk();
			}
		}
	}

	private class ActionCancel extends AbstractAction {
		public ActionCancel() {
			super("Annuler");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CANCEL_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onCancel();
			}
		}
	}

	/**
	 * Filtre par type de fournisseurs les resultats d'une recherche Personne.
	 * @author flagouey
	 */
	private class CbFournisListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			onSearch();
		}
	}

	private final class ContactTablePanel extends ZExtendedTablePanel {
		public ContactTablePanel() {
			super(null, false, false, false);
			ZEOTableModelColumn personneModelCol = new ZEOTableModelColumn(
					getDG(), EOIndividuUlr.PERSONNE_PERS_NOM_PRENOM_KEY, TABLE_PERSONNE_COLNAME);
			addCol(personneModelCol);
			initGUI();
		}
		protected void onAdd() {
		}
		protected void onUpdate() {
		}
		protected void onDelete() {
		}
		protected void onSelectionChanged() {
		}
		public void onDbClick(final MouseEvent e) {
			actionOk.actionPerformed(null);
		}

	}
}
