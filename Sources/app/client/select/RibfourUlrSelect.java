/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client.select;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kava.client.metier.EOFournisUlr;
import org.cocktail.kava.client.metier.EOParametres;
import org.cocktail.kava.client.metier.EORibfourUlr;
import org.cocktail.kava.client.metier.EOUtilisateur;

import app.client.ApplicationClient;
import app.client.RibfourUlrAdd;
import app.client.tools.Constants;
import app.client.ui.UIUtilities;
import app.client.ui.ZTextField;
import app.client.ui.table.ZEOTableModelColumn;
import app.client.ui.table.ZExtendedTablePanel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class RibfourUlrSelect extends JDialog {

	private static final String	WINDOW_TITLE	= "Ribs";
	private static final int	WINDOW_WIDTH	= 450;
	private static final int	WINDOW_HEIGHT	= 350;

	private int					windowWidth, windowHeight;

	protected ZTextField	tfSearch;

	protected TablePanel		table;

	private JButton				btOk;
	private Action				actionOk, actionCancel;

	public static final int		MROK			= 1;
	public static final int		MRCANCEL		= 0;
	protected int				modalResult;

	protected ApplicationClient	app;
	protected EOEditingContext	ec;

	private EOFournisUlr		fournisUlr;

	public RibfourUlrSelect() {
		super((JFrame) null, WINDOW_TITLE, true);
		init(WINDOW_WIDTH, WINDOW_HEIGHT);
	}

	private void init(int windowWidth, int windowHeight) {
		this.windowWidth = windowWidth;
		this.windowHeight = windowHeight;

		app = (ApplicationClient) EOApplication.sharedApplication();
		ec = app.editingContext();

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

		actionOk = new ActionOk();
		actionCancel = new ActionCancel();

		table = new TablePanel();
		// tri...
		table.getTablePanel().getDisplayGroup().setSortOrderings(
				new NSArray(EOSortOrdering.sortOrderingWithKey(EORibfourUlr.RIB_TITCO_KEY, EOSortOrdering.CompareCaseInsensitiveAscending)));

		initGUI();
		initInputMap();

		addWindowListener(new LocalWindowListener());
	}

	public boolean open(EOUtilisateur utilisateur, EOExercice exercice, EOFournisUlr fournisUlr, EORibfourUlr object) {
		if (app.getParamBoolean(EOParametres.PARAM_AUTORISE_CREATION_RIB, exercice)) {
			table.setEditable(true);
			table.actionAdd.putValue(AbstractAction.SHORT_DESCRIPTION, "Creer un nouveau rib...");
		} else {
			table.setEditable(false);
			table.actionAdd.putValue(AbstractAction.SHORT_DESCRIPTION, "Vous n'avez pas les droits necessaires pour creer un rib !");
		}
		this.fournisUlr = fournisUlr;
		table.setObjectArray(fournisUlr.ribfourUlrs());
		update();
		return open();
	}

	public boolean open() {
		modalResult = MRCANCEL;
		setSize(windowWidth, windowHeight);
		centerWindow();
		show();
		// en modal la fenetre reste active jusqu'au closeWindow...
		return modalResult == MROK;
	}

	private final void centerWindow() {
		int screenWidth = (int) this.getGraphicsConfiguration().getBounds().getWidth();
		int screenHeight = (int) this.getGraphicsConfiguration().getBounds().getHeight();
		this.setLocation((screenWidth / 2) - ((int) this.getSize().getWidth() / 2),
				((screenHeight / 2) - ((int) this.getSize().getHeight() / 2)));
	}

	private void onOk() {
		if (!checkInputValues()) {
			return;
		}
		modalResult = MROK;
		tfSearch.selectAndFocus();
		hide();
	}

	private void onCancel() {
		modalResult = MRCANCEL;
		tfSearch.selectAndFocus();
		hide();
	}

	public EORibfourUlr getSelected() {
		return (EORibfourUlr) table.selectedObject();
	}

	private final class TablePanel extends ZExtendedTablePanel {

		public TablePanel() {
			super(null, true, false, false);
			addCol(new ZEOTableModelColumn(getDG(), EORibfourUlr.C_BANQUE_KEY, "Code banque", 60));
			addCol(new ZEOTableModelColumn(getDG(), EORibfourUlr.C_GUICHET_KEY, "Code guichet", 60));
			addCol(new ZEOTableModelColumn(getDG(), EORibfourUlr.NO_COMPTE_KEY, "No compte", 100));
			addCol(new ZEOTableModelColumn(getDG(), EORibfourUlr.RIB_TITCO_KEY, "Titulaire", 150));
			addCol(new ZEOTableModelColumn(getDG(), EORibfourUlr.RIB_VALIDE_KEY, "Valide", 40));
			actionAdd.putValue(AbstractAction.SHORT_DESCRIPTION, "Creer un nouveau rib client...");
			initGUI();
		}

		protected void onAdd() {
			if (RibfourUlrAdd.sharedInstance().openNew(fournisUlr)) {
				ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(fournisUlr)));
				setObjectArray(fournisUlr.ribfourUlrs());
			}
		}

		protected void onUpdate() {
		}

		protected void onDelete() {
		}

		protected void onSelectionChanged() {
		}

		public void onDbClick(final MouseEvent e) {
			actionOk.actionPerformed(null);
		}
	}

	private void initGUI() {
		JPanel topPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		tfSearch = new ZTextField(18);
		tfSearch.getDocument().addDocumentListener(new SearchDocumentListener());
		topPanel.add(UIUtilities.labeledComponent("Recherche...", tfSearch, null));

		JPanel bottomPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		bottomPanel.add(new JButton(actionCancel));
		btOk = new JButton(actionOk);
		btOk.setRequestFocusEnabled(true);
		bottomPanel.add(btOk);

		JPanel p = new JPanel(new BorderLayout());
		p.add(topPanel, BorderLayout.NORTH);
		p.add(table, BorderLayout.CENTER);
		p.add(bottomPanel, BorderLayout.SOUTH);

		this.setContentPane(p);
		p.getRootPane().setDefaultButton(btOk);
		this.pack();
	}

	protected boolean checkInputValues() {
		try {
			if (getSelected() == null) {
				throw new Exception("Selectionnez un rib!");
			}
			if (getSelected().ribValide() == null || !getSelected().ribValide().equalsIgnoreCase("O")) {
				throw new Exception("Selectionnez un rib VALIDE!");
			}
		}
		catch (Exception e) {
			app.showErrorDialog(e);
			return false;
		}
		return true;
	}

	private class SearchDocumentListener implements DocumentListener {

		public void changedUpdate(DocumentEvent arg0) {
			update();
		}

		public void insertUpdate(DocumentEvent arg0) {
			update();
		}

		public void removeUpdate(DocumentEvent arg0) {
			update();
		}

	}

	protected void update() {
		table.setFilteringQualifier(filteringQualifier(tfSearch.getText()));
	}

	protected EOQualifier filteringQualifier(String searchString) {
		if (searchString == null) {
			return null;
		}
		return EOQualifier.qualifierWithQualifierFormat(EORibfourUlr.RIB_TITCO_KEY + " caseInsensitiveLike %@ or " + EORibfourUlr.C_BANQUE_KEY
				+ " caseInsensitiveLike %@ or " + EORibfourUlr.NO_COMPTE_KEY + " caseInsensitiveLike %@ or " + EORibfourUlr.C_GUICHET_KEY
				+ " caseInsensitiveLike %@", new NSArray(new Object[] { "*" + searchString + "*", "*" + searchString + "*",
				"*" + searchString + "*", "*" + searchString + "*" }));
	}

	private void initInputMap() {
		((JComponent) getContentPane()).getActionMap().put("F8", new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				showRequired(true);
			}
		});
		((JComponent) getContentPane()).getActionMap().put("ALT_F8", new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				showRequired(false);
			}
		});
		((JComponent) getContentPane()).getActionMap().put("ESCAPE", actionCancel);
		((JComponent) getContentPane()).getActionMap().put("CTRL_S", actionOk);
		((JComponent) getContentPane()).getActionMap().put("F10", actionOk);

		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("F8"), "F8");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("released F8"), "ALT_F8");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"ESCAPE");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
				KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK), "CTRL_S");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F10, 0), "F10");
	}

	private void showRequired(boolean show) {
	}

	private class LocalWindowListener implements WindowListener {
		public void windowOpened(WindowEvent arg0) {
		}

		public void windowClosed(WindowEvent arg0) {
		}

		public void windowIconified(WindowEvent arg0) {
		}

		public void windowDeiconified(WindowEvent arg0) {
		}

		public void windowActivated(WindowEvent arg0) {
		}

		public void windowDeactivated(WindowEvent arg0) {
		}

		public void windowClosing(WindowEvent arg0) {
			actionCancel.actionPerformed(null);
		}
	}

	private class ActionOk extends AbstractAction {
		public ActionOk() {
			super("Valider");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onOk();
			}
		}
	}

	private class ActionCancel extends AbstractAction {
		public ActionCancel() {
			super("Annuler");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CANCEL_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onCancel();
			}
		}
	}

}
