/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client.select;

import java.awt.event.ActionEvent;

import org.cocktail.application.client.eof.EOTypeEtat;
import org.cocktail.kava.client.finder.FinderCatalogue;
import org.cocktail.kava.client.finder.FinderTypeEtat;
import org.cocktail.kava.client.metier.EOCatalogue;
import org.cocktail.kava.client.metier.EOCataloguePrestation;
import org.cocktail.kava.client.metier.EOFonction;
import org.cocktail.kava.client.metier.EOFournisUlr;
import org.cocktail.kava.client.metier.EOTypePublic;
import org.cocktail.kava.client.metier.EOUtilisateur;
import org.cocktail.kava.client.qualifier.Qualifiers;
import org.cocktail.kava.client.qualifier.Qualifiers.QualifierKey;

import app.client.tools.Constants;
import app.client.ui.ZExtendedDialogSelect;
import app.client.ui.table.ZEOTableModelColumn;
import app.client.ui.table.ZTablePanel;

import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

// TODO : pemettre la selection de catalogues archivés ou dont la date est passée.

public class CatalogueSelect extends ZExtendedDialogSelect {

	private static final String	WINDOW_TITLE	= "Catalogue";
	private static final int	WINDOW_WIDTH	= 640;
	private static final int	WINDOW_HEIGHT	= 480;

	private EOUtilisateur utilisateur;
	private EOTypePublic typePublic;
	private EOCatalogue object;

	public CatalogueSelect() {
		super(WINDOW_TITLE, true, WINDOW_WIDTH, WINDOW_HEIGHT);
	}

	public boolean open(EOUtilisateur aUtilisateur, EOTypePublic aTypePublic, EOCatalogue aObject) {
		utilisateur = aUtilisateur;
		typePublic = aTypePublic;
		object = aObject;



		if (typePublic != null) {
			setInfo("<b>Selection d'un catalogue</b>\nVous ne voyez ici que les catalogues valides autorises pour le type de public <u>"
					+ typePublic.typuLibelle() + "</u>.");
		} else {
			setInfo(null);
		}

		if (cbNonValide != null && cbNonValide.isSelected()) {
			loadData(null);
		} else {
			loadData(FinderTypeEtat.typeEtatValide(ec));
		}

		if (object != null) {
			tfSearch.setText(null);
			table.setSelectedObject(object);
			table.getTable().scrollToSelection();
		}
		return open();
	}



	private void loadData(EOTypeEtat unTypeEtat) {
		if (app.canUseFonction(EOFonction.DROIT_VOIR_TOUS_LES_CATALOGUES, null)) {
			table.setObjectArray(FinderCatalogue.find(ec, null, null, unTypeEtat, typePublic));
		} else {
			table.setObjectArray(FinderCatalogue.find(ec, utilisateur, null, unTypeEtat, typePublic));
		}
		update();

	}


	public EOCatalogue getSelected() {
		return (EOCatalogue) table.selectedObject();
	}

	private final class TablePanel extends ZTablePanel {

		public TablePanel(ZTablePanel.IZTablePanelMdl listener) {
			super(listener);
			cols.clear();
			cols.add(new ZEOTableModelColumn(getDG(), EOCatalogue.CATALOGUE_PRESTATION_KEY + "." + EOCataloguePrestation.CAT_NUMERO_KEY, "No",
					50));
			//cols.add(new ZEOTableModelColumn(getDG(), EOCatalogue.CAT_LIBELLE_KEY, "Libelle", 200));
			cols.add(new ZEOTableModelColumn(getDG(), "compCatLibelle", "Libelle", 200));

			cols.add(new ZEOTableModelColumn(getDG(), EOCatalogue.FOURNIS_ULR_KEY + "." + EOFournisUlr.PERSONNE_PERS_NOM_PRENOM_KEY,
					"Fournisseur", 200));

			cols.add(new ZEOTableModelColumn(getDG(), "periodeValidite", "Période", 100));

		}
	}

	protected boolean checkInputValues() {
		try {
			if (getSelected() == null) {
				throw new Exception("Selectionnez un catalogue!");
			}
		}
		catch (Exception e) {
			app.showErrorDialog(e);
			return false;
		}
		return true;
	}

	protected void initTable() {
		table = new TablePanel(new TableListener());
		table.initGUI();

		table.getTable().setBackground(Constants.COLOR_COL_BGD_NORMAL);
		table.getTable().setSelectionBackground(Constants.COLOR_COL_BGD_SELECTED);
		table.getTable().setForeground(Constants.COLOR_COL_FGD_NORMAL);
		table.getTable().setSelectionForeground(Constants.COLOR_COL_FGD_SELECTED);

		// tri...
		table.getDisplayGroup().setSortOrderings(
				new NSArray(EOSortOrdering.sortOrderingWithKey(EOCatalogue.CAT_LIBELLE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending)));
	}

	protected EOQualifier filteringQualifier(String searchString) {
		if (searchString == null) {
			return null;
		}
		NSMutableArray orQualsArray = new NSMutableArray();
		String patternSearch =  "*" + searchString + "*";
		NSArray patternSearchAsArray = new NSArray(new Object[] {patternSearch});
		orQualsArray.addObject(EOQualifier.qualifierWithQualifierFormat(
				EOCatalogue.CATALOGUE_PRESTATION_KEY + "." + EOCataloguePrestation.CAT_NUMERO_KEY
				+ " caseInsensitiveLike %@ ", patternSearchAsArray));
		orQualsArray.addObject(EOQualifier.qualifierWithQualifierFormat(
				EOCatalogue.CAT_LIBELLE_KEY + " caseInsensitiveLike %@", patternSearchAsArray));
		orQualsArray.addObject(Qualifiers.getQualifierFactory(QualifierKey.CATALOGUE_FOUR_PRENOM_PERSONNE).build(
				EOQualifier.QualifierOperatorCaseInsensitiveLike, patternSearch));
		orQualsArray.addObject(Qualifiers.getQualifierFactory(QualifierKey.CATALOGUE_FOUR_NOM_PERSONNE).build(
				EOQualifier.QualifierOperatorCaseInsensitiveLike, patternSearch));

		return new EOOrQualifier(orQualsArray);
	}

	protected void checkValidationChanged() {
		// TODO Auto-generated method stub

	}

	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == cbNonValide) {
			if (isNonValideChecked()) {
				loadData(null);
			} else {
				loadData(FinderTypeEtat.typeEtatValide(ec));
			}

		}

	}

}
