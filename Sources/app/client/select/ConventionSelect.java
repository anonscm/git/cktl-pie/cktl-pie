/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client.select;

import java.util.Date;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.cocowork.client.exception.ExceptionConventionCreation;
import org.cocktail.cocowork.client.facade.convention.FacadeEditionConventionFormation;
import org.cocktail.cocowork.client.metier.convention.Contrat;
import org.cocktail.cocowork.client.proxy.ProxyPrestationConventionFormation;
import org.cocktail.kava.client.finder.FinderConvention;
import org.cocktail.kava.client.finder.FinderTypePublic;
import org.cocktail.kava.client.metier.EOConvention;
import org.cocktail.kava.client.metier.EOOrgan;
import org.cocktail.kava.client.metier.EOTypePublic;
import org.cocktail.kava.client.metier.EOUtilisateur;

import app.client.tools.Constants;
import app.client.ui.ZDialogSelect2;
import app.client.ui.table.ZEOTableModelColumn;
import app.client.ui.table.ZExtendedTablePanel;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class ConventionSelect extends ZDialogSelect2 {

	private static final String WINDOW_TITLE = "Conventions";

	private static final int WINDOW_WIDTH = 500;

	private static final int WINDOW_HEIGHT = 350;

	protected EOTypePublic _typeFC = FinderTypePublic.typePublicFormationContinue(ec);

	protected EOTypePublic _typePM = FinderTypePublic.typePublicPretMateriel(ec);

	private EOTypePublic _typePublic;

	protected FacadeEditionConventionFormation facadeEditionConventionFormation;

	public ConventionSelect() {
		super(WINDOW_TITLE, true, WINDOW_WIDTH, WINDOW_HEIGHT);
	}

	public boolean openRec(EOUtilisateur utilisateur, EOExercice exercice, EOOrgan organ, EOTypeCredit typeCreditRec, EOConvention object, EOTypePublic aTypePublic) {
		_typePublic = aTypePublic;
		table.setObjectArray(FinderConvention.findRec(ec, exercice, organ, typeCreditRec));
		update();
		if (object != null) {
			tfSearch.setText(null);
			table.setSelectedObject(object);
			table.getTablePanel().getTable().scrollToSelection();
		}
		majAjout();
		return open();
	}

	public boolean openDep(EOUtilisateur utilisateur, EOExercice exercice, EOOrgan organ, EOTypeCredit typeCreditDep, EOConvention object, EOTypePublic aTypePublic) {
		_typePublic = aTypePublic;
		table.setObjectArray(FinderConvention.findDep(ec, exercice, organ, typeCreditDep));
		update();
		if (object != null) {
			tfSearch.setText(null);
			table.setSelectedObject(object);
			table.getTablePanel().getTable().scrollToSelection();
		}
		majAjout();
		return open();
	}

	public EOConvention getSelected() {
		return (EOConvention) table.selectedObject();
	}

	private final class TablePanel extends ZExtendedTablePanel {

		public TablePanel() {// ZTablePanel.IZTablePanelMdl listener) {
			super(null, true, true, false);
			this.getTablePanel().setTableListener(new TableListener2());
			// cols.clear();
			addCol(new ZEOTableModelColumn(getDG(), EOConvention.CON_REFERENCE_EXTERNE_KEY, "Ref", 150));
			addCol(new ZEOTableModelColumn(getDG(), EOConvention.CON_OBJET_COURT_KEY, "Objet", 150));
			actionAdd.putValue(AbstractAction.SHORT_DESCRIPTION, "Creer une nouvelle convention...");
			actionUpdate.putValue(AbstractAction.SHORT_DESCRIPTION, "Voir la convention");
			actionUpdate.putValue(AbstractAction.SMALL_ICON, Constants.ICON_LOUPE_16);
			actionUpdate.setEnabled(false);
		}

		protected void onAdd() {

			if (!_typeFC.equals(_typePublic)) {
				JOptionPane.showMessageDialog(
						this,
						"Cette operation n'est possible que pour les prestations de type " + _typeFC.typuLibelle(),
						"Interdit",
						JOptionPane.INFORMATION_MESSAGE);
				return;
			}

			conventionCreerOuChercher();
		}

		protected void onDelete() {
		}

		protected void onSelectionChanged() {
		}

		// ouvrir la convention selectionnee
		protected void onUpdate() {

			conventionAfficher();
		}

	}

	protected boolean checkInputValues() {
		try {
			if (getSelected() == null) {
				throw new Exception("Selectionnez une convention!");
			}
		}
		catch (Exception e) {
			app.showErrorDialog(e);
			return false;
		}
		return true;
	}

	protected void initTable() {
		table = new TablePanel();// new TableListener());
		table.initGUI();

		table.getTablePanel().getTable().setBackground(Constants.COLOR_COL_BGD_NORMAL);
		table.getTablePanel().getTable().setSelectionBackground(Constants.COLOR_COL_BGD_SELECTED);
		table.getTablePanel().getTable().setForeground(Constants.COLOR_COL_FGD_NORMAL);
		table.getTablePanel().getTable().setSelectionForeground(Constants.COLOR_COL_FGD_SELECTED);

		// tri...
		table.getDG().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey(EOConvention.CON_REFERENCE_EXTERNE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending)));
	}

	protected EOQualifier filteringQualifier(String searchString) {
		if (searchString == null) {
			return null;
		}
		return EOQualifier.qualifierWithQualifierFormat(EOConvention.CON_REFERENCE_EXTERNE_KEY + " caseInsensitiveLike %@ or " + EOConvention.CON_OBJET_COURT_KEY + " caseInsensitiveLike %@", new NSArray(new Object[] { "*" + searchString + "*", "*" + searchString + "*" }));
	}

	protected void delegateSelectionChanged() {
		if (table != null)
			table.actionUpdate.setEnabled(table.selectedObject() != null);
		super.delegateSelectionChanged();
	}

	/**
	 * Mise a jour (dis/enabled) du bouton d'ajout de creation d'un convention
	 */
	private void majAjout() {
		if (_typePublic != null && _typePublic.equals(FinderTypePublic.typePublicFormationContinue(ec)))
			// // bouton dispo uniquement au sein d'une facture avec prestation de formation continue
			// if (_facturePapier!=null && _facturePapier.prestation()!=null && _typeFC.equals(_facturePapier.prestation().typePublic()))
			table.actionAdd.setEnabled(true);
		else
			table.actionAdd.setEnabled(false);
	}

	public final FacadeEditionConventionFormation getFacadeEditionConventionFormation() {
		return this.facadeEditionConventionFormation;
	}

	public final void setFacadeEditionConventionFormation(FacadeEditionConventionFormation facadeEditionConventionFormation) {
		this.facadeEditionConventionFormation = facadeEditionConventionFormation;
	}

	/**
	 * Demande de creation a minima d'une convention de formation.
	 * @throws CreationConventionException
	 * @throws ArgumentException
	 */
	protected void conventionCreerOuChercher() {

		ProxyPrestationConventionFormation conventionProxy =
			new ProxyPrestationConventionFormation(getFacadeEditionConventionFormation(), this, Boolean.TRUE);

		try {
			conventionProxy.creerOuChercherConvention(this, "setContrat");
			// le fwk redonnera la main a l'application via this.setContrat()...
		}
		catch (ExceptionConventionCreation e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, e.getMessage(), "Probleme", JOptionPane.WARNING_MESSAGE);
		}
		catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "Impossible de poursuivre la creation de la convention :\n\n" + e.getMessage(), "Erreur imprevue", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Utilise aussi pour recevoir le contrat selectionne dans le finder de conventions.
	 * @param selection Objet Contrat selectionne.
	 * @param aEteValide Indique si la selection a ete validee ou annulee.
	 */
	public void setContrat(Object selection, Boolean aEteValide) {

		if (aEteValide.booleanValue()) {

			Contrat contrat = (Contrat) selection;
			if (contrat != null) {
				try {
					// fetch de la convention dans la classe metier de PIE :
					Number conOrdre = (Number) contrat.getPrimaryKey();
					System.out.println("[" + new Date() + "] ConventionSelect.setContrat() conOrdre = " + conOrdre);
					EOConvention convention = (EOConvention) FinderConvention.find(ec, conOrdre);

					// selection de la convention et fermeture de cette fenetre
					table.setObjectArray(new NSArray(convention));
					table.setSelectedObject(convention);
				}
				catch (Exception e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(this, "Impossible de recuperer la convention creee : \n\n" + e.getMessage(), "Erreur grave", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	/**
	 * Demande d'affichage de la fenetre d'edition de la convention selectionne.
	 */
	protected void conventionAfficher() {

		ProxyPrestationConventionFormation conventionProxy = new ProxyPrestationConventionFormation(getFacadeEditionConventionFormation(), this, Boolean.TRUE);

		try {
			conventionProxy.afficherConvention(getSelected(), app.appUserInfo().utilisateur(), true);
		}
		catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "Impossible d'ouvrir la convention : \n\n" + e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
		}
	}

	public boolean isConventionEnabled(){
		return (_typeFC.equals(_typePublic) || (_typePM != null && _typePM.equals(_typePublic)));
	}
}
