/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client.select;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.kava.client.finder.FinderCatalogue;
import org.cocktail.kava.client.finder.FinderCatalogueArticle;
import org.cocktail.kava.client.finder.FinderTypeArticle;
import org.cocktail.kava.client.finder.FinderTypeEtat;
import org.cocktail.kava.client.metier.EOArticle;
import org.cocktail.kava.client.metier.EOCatalogue;
import org.cocktail.kava.client.metier.EOCatalogueArticle;
import org.cocktail.kava.client.metier.EOFonction;
import org.cocktail.kava.client.metier.EOFournisUlr;
import org.cocktail.kava.client.metier.EOTypeArticle;
import org.cocktail.kava.client.metier.EOTypePublic;
import org.cocktail.kava.client.metier.EOUtilisateur;

import app.client.ApplicationClient;
import app.client.tools.Constants;
import app.client.ui.UIUtilities;
import app.client.ui.ZEOComboBox;
import app.client.ui.ZLabel;
import app.client.ui.ZTextField;
import app.client.ui.table.ZEOTableModelColumn;
import app.client.ui.table.ZTablePanel;
import app.client.ui.table.ZTablePanel.IZTablePanelMdl;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class CatalogueCatalogueArticleSelect extends JDialog {

	private static final String	WINDOW_TITLE	= "Catalogues & articles";
	private static final int	WINDOW_WIDTH	= 640;
	private static final int	WINDOW_HEIGHT	= 480;

	private int					windowWidth, windowHeight;

	private ZTablePanel			table;

	private ZTextField			tfSearch;

	private JButton				btOk;
	private Action				actionOk, actionCancel;

	private ZLabel				labelInfo;

	public static final int		MROK			= 1;
	public static final int		MRCANCEL		= 0;
	private int					modalResult;

	private ApplicationClient	app;
	private EOEditingContext	ec;

	private ZEOComboBox			cbCatalogue;
	private EOTypePublic		typePublic;

	public CatalogueCatalogueArticleSelect() {
		super((JFrame) null, WINDOW_TITLE, true);
		init(WINDOW_WIDTH, WINDOW_HEIGHT);
	}

	public boolean open(EOUtilisateur utilisateur, EOFournisUlr fournisUlr, EOTypePublic typePublic) {
		this.typePublic = typePublic;
		cbCatalogue.setVisible(true);
		if (app.canUseFonction(EOFonction.DROIT_VOIR_TOUS_LES_CATALOGUES, null)) {
			cbCatalogue.setObjects(FinderCatalogue.find(ec, null, fournisUlr, typePublic));
		} else {
			cbCatalogue.setObjects(FinderCatalogue.find(ec, utilisateur, fournisUlr, typePublic));
		}
		onCatalogueChanged();

		modalResult = MRCANCEL;
		setSize(windowWidth, windowHeight);
		centerWindow();
		show();
		// en modal la fenetre reste active jusqu'au closeWindow...
		tfSearch.selectAndFocus();
		return modalResult == MROK;
	}

	public NSArray getSelecteds() {
		return table.selectedObjects();
	}

	private void onCatalogueChanged() {
		EOCatalogue catalogue = (EOCatalogue) cbCatalogue.getSelectedEOObject();
		if (catalogue == null) {
			table.setObjectArray(null);
		} else {
			table.setObjectArray(
					FinderCatalogueArticle.find(ec, catalogue, typePublic, FinderTypeEtat.typeEtatValide(ec)));
		}
	}

	private void init(int windowWidth, int windowHeight) {
		this.windowWidth = windowWidth;
		this.windowHeight = windowHeight;

		app = (ApplicationClient) EOApplication.sharedApplication();
		ec = app.editingContext();

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

		actionOk = new ActionOk();
		actionCancel = new ActionCancel();

		cbCatalogue = new ZEOComboBox(new NSArray(), EOCatalogue.CAT_LIBELLE_KEY, null, null, null, 250);
		cbCatalogue.addActionListener(new CbActionListener());

		initTable();
		table.initGUI();

		table.getTable().setBackground(Constants.COLOR_COL_BGD_NORMAL);
		table.getTable().setSelectionBackground(Constants.COLOR_COL_BGD_SELECTED);
		table.getTable().setForeground(Constants.COLOR_COL_FGD_NORMAL);
		table.getTable().setSelectionForeground(Constants.COLOR_COL_FGD_SELECTED);

		// Selection multiple autorisee...
		table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		initGUI();
		initInputMap();

		addWindowListener(new LocalWindowListener());
	}

	private final void centerWindow() {
		int screenWidth = (int) this.getGraphicsConfiguration().getBounds().getWidth();
		int screenHeight = (int) this.getGraphicsConfiguration().getBounds().getHeight();
		this.setLocation((screenWidth / 2) - ((int) this.getSize().getWidth() / 2),
				((screenHeight / 2) - ((int) this.getSize().getHeight() / 2)));
	}

	private void onOk() {
		if (!checkInputValues()) {
			return;
		}
		modalResult = MROK;
		tfSearch.selectAndFocus();
		hide();
	}

	private void onCancel() {
		modalResult = MRCANCEL;
		tfSearch.selectAndFocus();
		hide();
	}

	private void initGUI() {
		labelInfo = new ZLabel("", Constants.ICON_INFO_32, SwingConstants.LEADING);
		labelInfo
				.setTextHtml("<b>Selection d'un article de catalogue</b>\nSelectionnez d'abord le catalogue ou vous souhaitez trouver l'article, puis choisissez l'article.\nLes catalogues et articles presentes ici sont filtres en fonction du type de public");
		labelInfo.setBackground(Color.white);
		labelInfo.setOpaque(true);

		tfSearch = new ZTextField(18);
		tfSearch.getDocument().addDocumentListener(new SearchDocumentListener());
		Box recherchePanel = Box.createVerticalBox();
		recherchePanel.add(UIUtilities.labeledComponent("Catalogue", cbCatalogue, null, true));
		recherchePanel.add(Box.createVerticalStrut(10));
		recherchePanel.add(UIUtilities.labeledComponent("Recherche...", tfSearch, null));

		JPanel topPanel = new JPanel(new BorderLayout());
		topPanel.add(labelInfo, BorderLayout.NORTH);
		topPanel.add(recherchePanel, BorderLayout.CENTER);

		JPanel bottomPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		bottomPanel.add(new JButton(actionCancel));
		btOk = new JButton(actionOk);
		btOk.setRequestFocusEnabled(true);
		bottomPanel.add(btOk);

		JPanel p = new JPanel(new BorderLayout());
		p.add(topPanel, BorderLayout.NORTH);
		p.add(table, BorderLayout.CENTER);
		p.add(bottomPanel, BorderLayout.SOUTH);

		this.setContentPane(p);
		p.getRootPane().setDefaultButton(btOk);
		this.pack();
	}

	private void update() {
		if (tfSearch.getText() == null) {
			table.setFilteringQualifier(null);
		}
		else {
			table.setFilteringQualifier(EOQualifier.qualifierWithQualifierFormat(EOCatalogueArticle.CAAR_REFERENCE_KEY
					+ " caseInsensitiveLike %@ or " + EOCatalogueArticle.ARTICLE_KEY + "." + EOArticle.ART_LIBELLE_KEY
					+ " caseInsensitiveLike %@", new NSArray(new Object[] { "*" + tfSearch.getText() + "*", "*" + tfSearch.getText() + "*" })));
		}
	}

	private final class TablePanel extends ZTablePanel {

		public TablePanel(ZTablePanel.IZTablePanelMdl listener) {
			super(listener);
			cols.clear();
			cols.add(new ZEOTableModelColumn(getDG(), EOCatalogueArticle.CAAR_REFERENCE_KEY, "Ref", 50));
			cols.add(new ZEOTableModelColumn(getDG(), EOCatalogueArticle.ARTICLE_KEY + "." + EOArticle.ART_LIBELLE_KEY, "Lib", 150));
			cols.add(new ZEOTableModelColumn(getDG(), EOCatalogueArticle.CAAR_PRIX_HT_KEY, "HT", 40));
			cols.add(new ZEOTableModelColumn(getDG(), EOCatalogueArticle.CAAR_PRIX_TTC_KEY, "TTC", 40));
		}
	}

	private boolean checkInputValues() {
		try {
			if (getSelecteds() == null || getSelecteds().count() == 0) {
				throw new Exception("Selectionnez un article !");
			}
		}
		catch (Exception e) {
			app.showErrorDialog(e);
			return false;
		}
		return true;
	}

	private void initTable() {
		table = new TablePanel(new TableListener());
		table.initGUI();

		table.getTable().setBackground(Constants.COLOR_COL_BGD_NORMAL);
		table.getTable().setSelectionBackground(Constants.COLOR_COL_BGD_SELECTED);
		table.getTable().setForeground(Constants.COLOR_COL_FGD_NORMAL);
		table.getTable().setSelectionForeground(Constants.COLOR_COL_FGD_SELECTED);

		// tri...
		table.getDisplayGroup().setSortOrderings(
				new NSArray(EOSortOrdering.sortOrderingWithKey(EOCatalogueArticle.CAAR_REFERENCE_KEY,
						EOSortOrdering.CompareCaseInsensitiveAscending)));
	}

	private class SearchDocumentListener implements DocumentListener {

		public void changedUpdate(DocumentEvent arg0) {
			update();
		}

		public void insertUpdate(DocumentEvent arg0) {
			update();
		}

		public void removeUpdate(DocumentEvent arg0) {
			update();
		}

	}

	private final class TableListener implements IZTablePanelMdl {
		public void selectionChanged() {
		}

		public void onDbClick(final MouseEvent e) {
			actionOk.actionPerformed(null);
		}
	}

	private void initInputMap() {
		((JComponent) getContentPane()).getActionMap().put("ESCAPE", actionCancel);
		((JComponent) getContentPane()).getActionMap().put("CTRL_S", actionOk);
		((JComponent) getContentPane()).getActionMap().put("F10", actionOk);

		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("F8"), "F8");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("released F8"), "ALT_F8");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"ESCAPE");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
				KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK), "CTRL_S");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F10, 0), "F10");
	}

	private class LocalWindowListener implements WindowListener {
		public void windowOpened(WindowEvent arg0) {
		}

		public void windowClosed(WindowEvent arg0) {
		}

		public void windowIconified(WindowEvent arg0) {
		}

		public void windowDeiconified(WindowEvent arg0) {
		}

		public void windowActivated(WindowEvent arg0) {
		}

		public void windowDeactivated(WindowEvent arg0) {
		}

		public void windowClosing(WindowEvent arg0) {
			actionCancel.actionPerformed(null);
		}
	}

	private class ActionOk extends AbstractAction {
		public ActionOk() {
			super("Valider");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onOk();
			}
		}
	}

	private class ActionCancel extends AbstractAction {
		public ActionCancel() {
			super("Annuler");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CANCEL_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onCancel();
			}
		}
	}

	private class CbActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JComboBox cb = (JComboBox) e.getSource();
			if (cb == cbCatalogue) {
				onCatalogueChanged();
			}
		}
	}

}
