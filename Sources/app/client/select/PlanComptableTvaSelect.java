/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client.select;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kava.client.finder.FinderPlanComptableTva;
import org.cocktail.kava.client.finder.FinderPlanComptableTvaReaitva;
import org.cocktail.kava.client.metier.EOFonction;
import org.cocktail.kava.client.metier.EOPlanComptable;
import org.cocktail.kava.client.metier.EOPlanComptableTva;
import org.cocktail.kava.client.metier.EOPlanComptableTvaReaitva;
import org.cocktail.kava.client.metier.EOUtilisateur;

import app.client.tools.Constants;
import app.client.ui.ZDialogSelect;
import app.client.ui.table.ZEOTableModelColumn;
import app.client.ui.table.ZTablePanel;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class PlanComptableTvaSelect extends ZDialogSelect {

	private static final String	WINDOW_TITLE	= "Imputations comptables TVA";
	private static final int	WINDOW_WIDTH	= 350;
	private static final int	WINDOW_HEIGHT	= 350;

	public PlanComptableTvaSelect() {
		super(WINDOW_TITLE, true, WINDOW_WIDTH, WINDOW_HEIGHT);
	}
	
	//UP 02/09/2009 : tenir compte du plan comptable par exercice
	public boolean open(EOUtilisateur utilisateur, EOExercice exercice, EOPlanComptable object) {
		if (app.canUseFonction(EOFonction.DROIT_TVA_NON_STANDARD, exercice)) {
			table.setObjectArray((NSArray) FinderPlanComptableTvaReaitva.find(ec,exercice).valueForKey(EOPlanComptableTvaReaitva.PLAN_COMPTABLE_KEY));
		}
		else {
			table.setObjectArray((NSArray) FinderPlanComptableTva.find(ec,exercice).valueForKey(EOPlanComptableTva.PLAN_COMPTABLE_KEY));
		}
		update();
		if (object != null) {
			tfSearch.setText(null);
			table.setSelectedObject(object);
			table.getTable().scrollToSelection();
		}
		return open();
	}

	public EOPlanComptable getSelected() {
		return (EOPlanComptable) table.selectedObject();
	}

	private final class TablePanel extends ZTablePanel {

		public TablePanel(ZTablePanel.IZTablePanelMdl listener) {
			super(listener);
			cols.clear();
			cols.add(new ZEOTableModelColumn(getDG(), EOPlanComptable.PCO_NUM_KEY, "Code", 50));
			cols.add(new ZEOTableModelColumn(getDG(), EOPlanComptable.PCO_LIBELLE_KEY, "Libelle", 130));
		}
	}

	protected boolean checkInputValues() {
		try {
			if (getSelected() == null) {
				throw new Exception("Selectionnez un compte d'imputation TVA!");
			}
		}
		catch (Exception e) {
			app.showErrorDialog(e);
			return false;
		}
		return true;
	}

	protected void initTable() {
		table = new TablePanel(new TableListener());
		table.initGUI();

		table.getTable().setBackground(Constants.COLOR_COL_BGD_NORMAL);
		table.getTable().setSelectionBackground(Constants.COLOR_COL_BGD_SELECTED);
		table.getTable().setForeground(Constants.COLOR_COL_FGD_NORMAL);
		table.getTable().setSelectionForeground(Constants.COLOR_COL_FGD_SELECTED);

		// tri...
		table.getDisplayGroup().setSortOrderings(
				new NSArray(EOSortOrdering.sortOrderingWithKey(EOPlanComptable.PCO_NUM_KEY, EOSortOrdering.CompareCaseInsensitiveAscending)));
	}

	protected EOQualifier filteringQualifier(String searchString) {
		if (searchString == null) {
			return null;
		}
		return EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_NUM_KEY + " caseInsensitiveLike %@ or "
				+ EOPlanComptable.PCO_LIBELLE_KEY + " caseInsensitiveLike %@", new NSArray(new Object[] { "*" + searchString + "*",
				"*" + searchString + "*" }));
	}

}
