/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne;
import org.cocktail.fwkcktlcomptaguiswing.client.all.IConnectedUserInfos;
import org.cocktail.kava.client.finder.FinderUtilisateur;
import org.cocktail.kava.client.metier.EOUtilisateur;
import org.cocktail.kava.client.metier.EOUtilisateurFonction;
import org.cocktail.kava.client.metier.EOUtilisateurFonctionExercice;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

public class AppUserInfo implements IConnectedUserInfos {

    private static final String EMPTY = "";
    private static final String SPACE = " ";

	private ApplicationClient	app;
	private EOEditingContext	ec;

	private String				nom, prenom, login, email, vlan;
	private Integer				noCompte;
	private Integer             persId;
	private Integer             noIndividu;
	private EOUtilisateur		utilisateur;
	private Map<String, Object>	utilisateurFonctions;
	private EOGrhumPersonne     grhumPersonne;

	public AppUserInfo(String login) {
		super();
		init();
		NSDictionary userId = ServerProxy.getUserAuthentication(ec, login, null, Boolean.valueOf(false));

		if (userId.valueForKey("err") == null) {
			initValues(userId);
			initGrhumPersonne();
		} else {
			app.showErrorDialog(userId.valueForKey("err").toString());
		}
	}

	public AppUserInfo(NSDictionary userId) {
		super();
		init();
		initValues(userId);
		initGrhumPersonne();
	}

	private void init() {
		app = (ApplicationClient) EOApplication.sharedApplication();
		ec = app.editingContext();
	}

	private void initValues(NSDictionary userId) {
		nom = (String) userId.valueForKey("nom");
		prenom = (String) userId.valueForKey("prenom");
		login = (String) userId.valueForKey("login");
		email = (String) userId.valueForKey("email");
		vlan = (String) userId.valueForKey("vLan");
		noCompte = (Integer) userId.valueForKey("noCompte");
		persId = (Integer) userId.valueForKey("persId");
		noIndividu = (Integer) userId.valueForKey("noIndividu");
		updateUtilisateur(persId);
	}

	private void initGrhumPersonne() {
		this.grhumPersonne = EOGrhumPersonne.fetchByKeyValue(ec, EOGrhumPersonne.PERS_ID_KEY, getPersId());
	}

	/**
	 * Construit un HashMap des fonctions de l'utilisateur - Si le fonIdinterne est dans le HashMap avec pour valeur null, c'est qu'il a droit a
	 * cette fonction. - Si le fonIdInterne est dans le HashMap avec pour valeur non null, c'est que la valeur est un HashSet qui stocke tous les
	 * exercices pour lesquels il a droit a cette fonction. j'suis clair la ? :-)
	 *
	 * @param persId
	 */
	public void updateUtilisateur(Integer persId) {
		if (utilisateurFonctions == null) {
			utilisateurFonctions = new HashMap();
		} else {
			utilisateurFonctions.clear();
		}

		utilisateur = fetchUtilisateur(persId);
		if (utilisateur != null) {
			NSArray ufArray = utilisateur.utilisateurFonctions();
			if (ufArray != null) {
				for (int i = 0; i < ufArray.count(); i++) {
					EOUtilisateurFonction uf = (EOUtilisateurFonction) ufArray.objectAtIndex(i);
					if (uf.fonction_fonSpecExercice().equalsIgnoreCase("N")) {
						utilisateurFonctions.put(uf.fonction_fonIdInterne(), null);
					} else {
						NSArray ufeArray = uf.utilisateurFonctionExercices();
						HashSet ufeHashSet = null;
						if (ufeArray != null && ufeArray.count() > 0) {
							ufeHashSet = new HashSet(ufeArray.count());
							for (int j = 0; j < ufeArray.count(); j++) {
								EOUtilisateurFonctionExercice ufe = (EOUtilisateurFonctionExercice) ufeArray.objectAtIndex(j);
								ufeHashSet.add(ufe.exercice_exeExercice());
							}
						}
						if (ufeHashSet != null) {
							utilisateurFonctions.put(uf.fonction_fonIdInterne(), ufeHashSet);
						}
					}
				}
			}
		}
	}

	private EOUtilisateur fetchUtilisateur(Integer persId) {
		try {
			return FinderUtilisateur.findUtilisateur(ec, persId);
		} catch (Exception e) {
			return null;
		}
	}

	public boolean isConnected() {
		return (utilisateur != null);
	}

	public Integer getPersId() {
		return persId;
	}

	public String getName() {
		return nom() + (prenom() != null ? SPACE + prenom() : EMPTY);
	}

	public boolean canUseFonction(String fonIdInterne, EOExercice exercice) {
		if (utilisateurFonctions() == null) {
			return false;
		}
		if (utilisateurFonctions().containsKey(fonIdInterne)) {
			if (utilisateurFonctions().get(fonIdInterne) == null || exercice == null) {
				return true;
			} else {
				Set ufeHashSet = (Set) utilisateurFonctions().get(fonIdInterne);
				if (ufeHashSet.contains(exercice.exeExercice())) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean isFonctionAutoriseeByActionID(String fonctionId) {
		return canUseFonction(fonctionId, null);
	}

	public Integer getCurrentExerciceAsInt() {
		EOExercice exerciceEnCours = app.getCurrentExercice();
		if (exerciceEnCours == null) {
			return null;
		}
		return exerciceEnCours.exeExercice();
	}

	public EOGrhumPersonne getGrhumPersonne() {
		return grhumPersonne;
	}

	public String login() {
		return login;
	}

	public String email() {
		return email;
	}

	public Integer noCompte() {
		return noCompte;
	}

	public Integer noIndividu() {
		return noIndividu;
	}

	public String nom() {
		return nom;
	}

	public String prenom() {
		return prenom;
	}

	public String vlan() {
		return vlan;
	}

	public EOUtilisateur utilisateur() {
		return utilisateur;
	}

	public Map<String, Object> utilisateurFonctions() {
		return utilisateurFonctions;
	}

	public String toString() {
		String s = "login: <" + login + ">, ";
		s = s + "nom: <" + nom + ">, ";
		s = s + "prenom: <" + prenom + ">, ";
		s = s + "persId: <" + persId + ">, ";
		s = s + "noIndividu: <" + noIndividu + ">, ";
		s = s + "email: <" + email + ">, ";
		s = s + "vlan: <" + vlan + ">, ";
		s = s + "noCompte: <" + noCompte + ">, ";
		s = s + "utilisateur: <" + utilisateur + ">\n";
		return s;
	}
}
