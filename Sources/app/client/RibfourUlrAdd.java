/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;

import org.cocktail.kava.client.metier.EOFournisUlr;
import org.cocktail.kava.client.procedures.InsClientRib;

import app.client.tools.Constants;
import app.client.ui.UIUtilities;
import app.client.ui.ZTextField;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;

public class RibfourUlrAdd extends JDialog {

	private static final String		WINDOW_TITLE	= "Cr\u00E9ation d'un nouveau RIB client";

	protected ApplicationClient		app;
	protected EOEditingContext		ec;

	private static RibfourUlrAdd	sharedInstance;

	private Action					actionClose, actionValid, actionCancel;
	private Action					actionShowRequired, actionUnshowRequired;

	private ZTextField		tfRibPays, tfRibBanque, tfRibGuichet, tfRibCompte, tfRibCle;

	private JButton					btValid, btCancel;

	private boolean					result;

	private EOFournisUlr			fournisUlr;

	public RibfourUlrAdd() {
		super((JFrame) null, WINDOW_TITLE, true);

		app = (ApplicationClient) EOApplication.sharedApplication();
		ec = app.editingContext();

		setGlassPane(new BusyGlassPanel());
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new LocalWindowListener());

		actionClose = new ActionClose();
		actionValid = new ActionValid();
		actionCancel = new ActionCancel();
		actionShowRequired = new ActionShowRequired();
		actionUnshowRequired = new ActionUnshowRequired();

		tfRibPays = new ZTextField(6);
		tfRibBanque = new ZTextField(8);
		tfRibGuichet = new ZTextField(8);
		tfRibCompte = new ZTextField(20);
		tfRibCle = new ZTextField(5);

		btCancel = new JButton(actionCancel);
		btValid = new JButton(actionValid);

		initGUI();

		setEditable(true, false);
	}

	public static RibfourUlrAdd sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new RibfourUlrAdd();
		}
		return sharedInstance;
	}

	public boolean openNew(EOFournisUlr fournisUlr) {
		this.fournisUlr = fournisUlr;
		setEditable(true, false);
		cleanData();
		return open();
	}

	public boolean canQuit() {
		if (isShowing()) {
			actionCancel.actionPerformed(null);
		}
		return !isShowing();
	}

	private boolean open() {
		result = false;
		int screenWidth = (int) this.getGraphicsConfiguration().getBounds().getWidth();
		int screenHeight = (int) this.getGraphicsConfiguration().getBounds().getHeight();
		this.setLocation((screenWidth / 2) - ((int) this.getSize().getWidth() / 2),
				((screenHeight / 2) - ((int) this.getSize().getHeight() / 2)));
		show();

		return result;
	}

	private void onValid() {
		try {
			InsClientRib.save(ec, fournisUlr, tfRibPays.getText(), tfRibBanque.getText(), tfRibGuichet.getText(), tfRibCompte.getText(),
					tfRibCle.getText());
			result = true;
			hide();
		}
		catch (Exception e) {
			app.showErrorDialog(e);
		}
	}

	private void onCancel() {
		if (app.showConfirmationDialog("ATTENTION", "Annuler???", "OUI!", "NON")) {
			result = false;
			hide();
		}
	}

	private void onClose() {
		actionCancel.actionPerformed(null);
	}

	private void setEditable(boolean editable, boolean forUpdate) {
		tfRibPays.setEditable(editable);
		tfRibBanque.setEditable(editable);
		tfRibGuichet.setEditable(editable);
		tfRibCompte.setEditable(editable);
		tfRibCle.setEditable(editable);

		actionCancel.setEnabled(editable);
		actionValid.setEnabled(editable);
		actionClose.setEnabled(!editable);
	}

	private void cleanData() {
		tfRibPays.clean();
		tfRibBanque.clean();
		tfRibGuichet.clean();
		tfRibCompte.clean();
		tfRibCle.clean();
	}

	private void showRequired(boolean show) {
		tfRibBanque.showRequired(show);
		tfRibGuichet.showRequired(show);
		tfRibCompte.showRequired(show);
		tfRibCle.showRequired(show);
	}

	private void initGUI() {
		JPanel contentPanel = new JPanel(new BorderLayout());
		contentPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		// contentPanel.setPreferredSize(WINDOW_DIMENSION);
		setContentPane(contentPanel);
		setResizable(false);

		// type
		JPanel panelRib = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panelRib.setBorder(BorderFactory.createEmptyBorder());
		panelRib.add(UIUtilities.labeledComponent("[Pays]", tfRibPays, null));
		panelRib.add(Box.createHorizontalStrut(5));
		panelRib.add(UIUtilities.labeledComponent("Banque", tfRibBanque, null));
		panelRib.add(UIUtilities.labeledComponent("Guichet", tfRibGuichet, null));
		panelRib.add(UIUtilities.labeledComponent("Compte", tfRibCompte, null));
		panelRib.add(UIUtilities.labeledComponent("Cl\u00E9", tfRibCle, null));

		Box center = Box.createVerticalBox();
		center.setBorder(BorderFactory.createEmptyBorder());

		center.add(UIUtilities.labeledComponent("Rib (IBAN)", panelRib, null, true));

		getContentPane().add(center, BorderLayout.CENTER);
		getContentPane().add(buildBottomPanel(), BorderLayout.PAGE_END);

		initInputMap();

		this.validate();
		this.pack();
	}

	private final JPanel buildBottomPanel() {
		JPanel p = new JPanel();
		p.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.add(Box.createHorizontalGlue());
		p.add(btCancel);
		p.add(Box.createRigidArea(new Dimension(5, 0)));
		p.add(btValid);
		return p;
	}

	private void initInputMap() {
		((JComponent) getContentPane()).getActionMap().put("CTRL_Q", app.superviseur().mainMenu.actionQuit);
		((JComponent) getContentPane()).getActionMap().put("CTRL_S", actionValid);
		((JComponent) getContentPane()).getActionMap().put("F10", actionValid);
		((JComponent) getContentPane()).getActionMap().put("ESCAPE", actionCancel);
		((JComponent) getContentPane()).getActionMap().put("F8", actionShowRequired);
		((JComponent) getContentPane()).getActionMap().put("ALT_F8", actionUnshowRequired);
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
				KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK), "CTRL_Q");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
				KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK), "CTRL_S");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F10, 0), "F10");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"ESCAPE");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("F8"), "F8");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("released F8"), "ALT_F8");
	}

	private class ActionValid extends AbstractAction {
		public ActionValid() {
			super("Valider");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onValid();
			}
		}
	}

	private class ActionCancel extends AbstractAction {
		public ActionCancel() {
			super("Annuler");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CANCEL_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onCancel();
			}
		}
	}

	private class ActionClose extends AbstractAction {
		public ActionClose() {
			super("Fermer");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CLOSE_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onClose();
			}
		}
	}

	private class ActionShowRequired extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				showRequired(true);
			}
		}
	}

	private class ActionUnshowRequired extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				showRequired(false);
			}
		}
	}

	private class LocalWindowListener implements WindowListener {
		public void windowActivated(WindowEvent arg0) {
		}

		public void windowClosed(WindowEvent arg0) {
		}

		public void windowClosing(WindowEvent arg0) {
			actionClose.actionPerformed(null);
		}

		public void windowDeactivated(WindowEvent arg0) {
		}

		public void windowDeiconified(WindowEvent arg0) {
		}

		public void windowIconified(WindowEvent arg0) {
		}

		public void windowOpened(WindowEvent arg0) {
		}
	}

	public void setWaitCursor(boolean bool) {
		if (bool) {
			this.getGlassPane().setVisible(true);
		}
		else {
			this.getGlassPane().setVisible(false);
		}
	}

	/**
	 * Permet de d\u00E9finir un panel qui sert a afficher un waitcursor et a interdire toute modif sur le panel.
	 * 
	 * @author rprin
	 */
	public final class BusyGlassPanel extends JPanel {
		public final Color	COLOR_WASH	= new Color(64, 64, 64, 32);

		public BusyGlassPanel() {
			super.setOpaque(false);
			super.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

			// Suck up them events!!!
			super.addKeyListener((new KeyAdapter() {
			}));
			super.addMouseListener((new MouseAdapter() {
			}));
			super.addMouseMotionListener((new MouseMotionAdapter() {
			}));
		}

		public final void paintComponent(Graphics p_graphics) {
			Dimension l_size = super.getSize();

			// Wash the pane with translucent gray.
			p_graphics.setColor(COLOR_WASH);
			p_graphics.fillRect(0, 0, l_size.width, l_size.height);

			// Paint a grid of white/black dots.
			p_graphics.setColor(Color.white);
			for (int j = 3; j < l_size.height; j += 8) {
				for (int i = 3; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
			p_graphics.setColor(Color.black);
			for (int j = 4; j < l_size.height; j += 8) {
				for (int i = 4; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
		}
	}

}
