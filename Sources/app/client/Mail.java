/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.FileOutputStream;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;
import javax.swing.border.Border;

import org.cocktail.kava.client.finder.FinderCatalogue;
import org.cocktail.kava.client.metier.EOFacturePapier;
import org.cocktail.kava.client.metier.EOPrestation;

import app.client.reports.ReportFactoryClient;
import app.client.tools.Constants;
import app.client.ui.UIUtilities;
import app.client.ui.ZLabel;
import app.client.ui.ZTextArea;
import app.client.ui.ZTextField;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;


public class Mail extends JDialog {

	private static final Dimension	WINDOW_DIMENSION	= new Dimension(650, 500);
	private static final String		WINDOW_TITLE		= "Envoi de mail";

	protected ApplicationClient		app;
	protected EOEditingContext		ec;

	private static Mail				sharedInstance;

	private Action					actionClose, actionValid, actionCancel;
	private Action					actionShowRequired, actionUnshowRequired;
	private Action					actionFileSelect, actionFileDelete, actionFileView;
	private Action					actionUseSystemMailer;

	// private ZEOMatrix matrixTypeMail;

	private ZTextField				tfSender, tfTo, tfCC, tfSubject;
	// private ZorglubLabel tfBody;
	private ZTextArea				tfPlus;

	private ZTextField				tfFileChooser;
	private JFileChooser			fileChooser;
	private NSData					datafile;
	private String					filename;

	private ZLabel					labelInfo;

	private JButton					btValid, btCancel, btFileView;

	private boolean					result;

	private JEditorPane				tfBody;

	public Mail() {
		super((JFrame) null, WINDOW_TITLE, true);

		app = (ApplicationClient) EOApplication.sharedApplication();
		ec = app.editingContext();

		setGlassPane(new BusyGlassPanel());
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new LocalWindowListener());

		actionClose = new ActionClose();
		actionValid = new ActionValid();
		actionCancel = new ActionCancel();
		actionShowRequired = new ActionShowRequired();
		actionUnshowRequired = new ActionUnshowRequired();
		actionFileSelect = new ActionFileSelect();
		actionFileDelete = new ActionFileDelete();
		actionFileView = new ActionFileView();

		actionUseSystemMailer = new ActionUseSystemMailer();

		// matrixTypeMail = new ZEOMatrix(FinderTypeArticle.findAll(ec), EOTypeArticle.TYAR_LIBELLE_KEY, null, new MatrixItemListener(),
		// SwingConstants.HORIZONTAL);

		tfSender = new ZTextField(40);
		tfTo = new ZTextField(40);
		tfCC = new ZTextField(60);
		tfSubject = new ZTextField(60);

		tfBody = new JEditorPane("text/html", "");
		tfBody.setFocusable(true);
		tfBody.setEditable(true);

		tfPlus = new ZTextArea(6, 90);

		tfFileChooser = new ZTextField(50);

		labelInfo = new ZLabel();

		btCancel = new JButton(actionCancel);
		btValid = new JButton(actionValid);

		initGUI();

		setEditable(true);
	}

	public static Mail sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new Mail();
		}
		return sharedInstance;
	}

	/**
	 * Envoie le devis par mail au client du devis
	 *
	 * @param prestation
	 * @throws Exception
	 */
	public void sendDevis(EOPrestation prestation) throws Exception {
		if (prestation == null) {
			return;
		}
		updateData(prestation, false);
		filename = "Devis-" + prestation.prestNumero() + ".pdf";
		datafile = ReportFactoryClient.getDevis(prestation, null);
		send();
	}

	/**
	 * Envoie la facture par mail au client de la facture
	 *
	 * @param facturePapier
	 * @throws Exception
	 */
	public void sendFacture(EOFacturePapier facturePapier) throws Exception {
		if (facturePapier == null) {
			return;
		}
		updateData(facturePapier, false);
		filename = "Facture-" + facturePapier.fapNumero() + ".pdf";
		datafile = ReportFactoryClient.getFacturePapier(facturePapier, null);
		send();
	}

	private void send() throws Exception {
		if (filename == null) {
			datafile = null;
		}
		String body = tfBody.getText();
		if (tfPlus.getText() != null && tfPlus.getText().length() > 0) {
			body = body + "\n" + tfPlus.getText();
		}
		ServerProxy.sendMailHtml(ec, tfSender.getText(), tfTo.getText(), tfCC.getText(), tfSubject.getText(), body, filename, datafile);
	}

	public boolean open(EOPrestation prestation) {
		if (prestation == null) {
			System.err.println("[Mail:open(EOPrestation)] Aucune prestation pass\u00E9e pour ouvrir l'envoi de mail !");
			return false;
		}

		// matrixTypeMail.selectRadioButtonByEo(prestation);

		setTitle(WINDOW_TITLE + " - Devis");

		boolean anglais = false;
		String langue = FinderCatalogue.findKeyWeb(ec, prestation.catalogue(), "LANGUE");
		if (langue != null && langue.equalsIgnoreCase("ANGLAIS")) {
			anglais = true;
		}

		try {
			updateData(prestation, anglais);
			if (anglais) {
				// TODO en anglais !!!!!!!
				filename = "Estimate-" + prestation.prestNumero() + ".pdf";
				datafile = ReportFactoryClient.getDevis(prestation, null);
			} else {
				filename = "Devis-" + prestation.prestNumero() + ".pdf";
				datafile = ReportFactoryClient.getDevis(prestation, null);
			}
			btFileView.setVisible(true);
			labelInfo.setText("Le devis au format pdf sera joint au mail.");

			setEditable(true);

			return open();
		} catch (Exception e) {
			System.err.println("Probl\u00E8me pour ouvrir l'envoi de mail : " + e);
			return false;
		}
	}

	public boolean open(EOFacturePapier facturePapier) {
		if (facturePapier == null) {
			System.err.println("[Mail:open(EOFacturePapier)] Aucune facture papier pass\u00E9e pour ouvrir l'envoi de mail !");
			return false;
		}

		// matrixTypeMail.selectRadioButtonByEo(facturePapier);
		System.out.println("Lancement du mail...");

		setTitle(WINDOW_TITLE + " - Facture");

		boolean anglais = false;
		if (facturePapier.prestation() != null) {
			String langue = FinderCatalogue.findKeyWeb(ec, facturePapier.prestation().catalogue(), "LANGUE");
			if (langue != null && langue.equalsIgnoreCase("ANGLAIS")) {
				anglais = true;
			}
		}

		try {
			updateData(facturePapier, anglais);
			if (anglais) {
				filename = "Invoice-" + facturePapier.fapNumero() + ".pdf";
				datafile = ReportFactoryClient.getFacturePapierAnglais(facturePapier, null);
			} else {
				filename = "Facture-" + facturePapier.fapNumero() + ".pdf";
				datafile = ReportFactoryClient.getFacturePapier(facturePapier, null);
			}
			btFileView.setVisible(true);
			labelInfo.setText("La facture au format pdf sera jointe au mail.");

			setEditable(true);

			return open();
		} catch (Exception e) {
			System.err.println("Probl\u00E8me pour ouvrir l'envoi de mail : " + e);
			return false;
		}
	}

	/**
	 * Envoie les informations de connexion par mail au client
	 *
	 * @param email
	 * @param login
	 * @param password
	 *            en clair
	 * @throws Exception
	 */
	public boolean openLoginPassword(String email, String login, String password) throws Exception {
		if (email == null || login == null || password == null) {
			return false;
		}

		setTitle(WINDOW_TITLE + " - Informations de connexion");

		try {
			tfSender.setText(app.appUserInfo().email());
			tfTo.setText(email);
			tfCC.setText(null);
			tfSubject.setText("Informations de connexion");
			tfBody
					.setText("Bonjour,<br><br>Vous avez un nouveau compte pour acc\u00E9der aux services en ligne de l'\u00E9tablissement.<br><br>Vos informations de connexion:<br>Nom d'utilisateur: "
							+ login + "<br>Mot de passe: " + password + "<br>");
			btFileView.setVisible(false);
			labelInfo.setText(null);

			setEditable(true);

			return open();
		}
		catch (Exception e) {
			System.err.println("Probl\u00E8me pour ouvrir l'envoi de mail : " + e);
			return false;
		}
	}

	public boolean canQuit() {
		if (isShowing()) {
			actionCancel.actionPerformed(null);
		}
		return !isShowing();
	}

	private boolean open() {
		result = false;
		int screenWidth = (int) this.getGraphicsConfiguration().getBounds().getWidth();
		int screenHeight = (int) this.getGraphicsConfiguration().getBounds().getHeight();
		this.setLocation((screenWidth / 2) - ((int) this.getSize().getWidth() / 2),
				((screenHeight / 2) - ((int) this.getSize().getHeight() / 2)));

		app.superviseur().setWaitCursor(this, false);
		setVisible(true);
		return result;
	}

	private void onValid() {
		try {
			checkUp();
			send();
			app.showInfoDialog("Mail envoy\u00E9 !");
			result = true;
			//hide();
			setVisible(false);
		}
		catch (Exception e) {
			app.showErrorDialog(e);
		}
	}

	private void onCancel() {
		if (app.showConfirmationDialog("Attention", "Annuler l'envoi ?", "Oui", "Non")) {
			ec.revert();
			result = false;
			//hide();
			setVisible(false);
		}
	}

	private void onClose() {
		actionCancel.actionPerformed(null);
	}

	// FIXME chiader un peu plus les controles ici (mail valide, sujet par defaut, ...)
	private void checkUp() throws Exception {
		if (tfSender.getText() == null) {
			throw new Exception("Il faut une adresse mail d'exp\u00E9diteur !");
		}
		if (tfTo.getText() == null) {
			throw new Exception("Il faut une adresse mail destinataire !");
		}
		if (tfSubject.getText() == null) {
			throw new Exception("Il faut un sujet !");
		}
		if (tfBody.getText() == null) {
			throw new Exception("Il faut un message !");
		}
	}

	// private void onUpdateTypeMail() {
	// // TODO
	// boolean b = matrixTypeMail.getSelectedEOObject().equals(FinderTypeArticle.typeArticleRemise());
	// }

	private void updateData(EOPrestation prestation, boolean anglais) throws Exception {
		cleanData();
		if (prestation == null) {
			return;
		}

		tfSender.setText(app.appUserInfo().email());
		if (prestation.individuUlr() != null && prestation.individuUlr().personne() != null
				&& prestation.individuUlr().personne().repartPersonneMail() != null) {
			tfTo.setText(prestation.individuUlr().personne().repartPersonneMail().email());
		}
		else {
			if (prestation.personne() != null && prestation.personne().repartPersonneMail() != null) {
				tfTo.setText(prestation.personne().repartPersonneMail().email());
			}
			else {
				tfTo.setText(null);
			}
		}
		tfCC.setText(null);

		if (anglais) {
			tfSubject.setText("Estimate No " + prestation.prestNumero());
		}
		else {
			tfSubject.setText("Devis No " + prestation.prestNumero());
		}

		// on fait selon qu'on veut en bium ou pas...
		String formatMail = FinderCatalogue.findKeyWeb(ec, prestation.catalogue(), "FORMAT_MAIL");
		if (formatMail != null && formatMail.equalsIgnoreCase("BIUM")) {
			if (anglais) {
				tfBody.setText(ServerProxy.getMailDevisEn(ec, prestation));
			}
			else {
				tfBody.setText(ServerProxy.getMailDevisFr(ec, prestation));
			}
		}
		else {
			if (anglais) {
				String body = "Please find enclosed your estimate No " + prestation.prestNumero() + " : '" + prestation.prestLibelle() + "'.\n";
				tfBody.setText(body);
			}
			else {
				String body = "Veuillez trouver en fichier joint votre devis No " + prestation.prestNumero() + " : '"
						+ prestation.prestLibelle() + "'.\n";
				tfBody.setText(body);
			}
		}

		// onUpdateTypeMail();
	}

	private void updateData(EOFacturePapier facturePapier, boolean anglais) throws Exception {
		cleanData();
		if (facturePapier == null) {
			return;
		}

		tfSender.setText(app.appUserInfo().email());
		if (facturePapier.individuUlr() != null && facturePapier.individuUlr().personne() != null
				&& facturePapier.individuUlr().personne().repartPersonneMail() != null) {
			tfTo.setText(facturePapier.individuUlr().personne().repartPersonneMail().email());
		}
		else {
			// TODO: UP, elargir l'envoi de la facture aux responsables CR pour validation coté client
			if (facturePapier.personne() != null && facturePapier.personne().repartPersonneMail() != null) {
				tfTo.setText(facturePapier.personne().repartPersonneMail().email());
			} else {
				tfTo.setText(null);
			}
		}
		tfCC.setText(null);

		if (anglais) {
			tfSubject.setText("Invoice No " + facturePapier.fapNumero());
		}
		else {
			tfSubject.setText("Facture No " + facturePapier.fapNumero());
		}

		// on fait selon qu'on veut en format bium ou pas...
		boolean formatBium = false;
		if (facturePapier.prestation() != null) {
			String formatMail = FinderCatalogue.findKeyWeb(ec, facturePapier.prestation().catalogue(), "FORMAT_MAIL");
			if (formatMail != null && formatMail.equalsIgnoreCase("BIUM")) {
				formatBium = true;
			}
		}
		if (formatBium) {
			if (anglais) {
				tfBody.setText(ServerProxy.getMailFactureEn(ec, facturePapier));
			}
			else {
				tfBody.setText(ServerProxy.getMailFactureFr(ec, facturePapier));
			}
		}
		else {
			StringBuffer body = new StringBuffer();

			boolean isValideClient = facturePapier.fapDateValidationClient() != null;
			String urlPageValFact = app.getParam("PAGE_VALIDATION_FACTURE");
			System.out.println("urlPageValFact="+urlPageValFact);
			boolean isCanValideClient = (urlPageValFact!=null) && !urlPageValFact.trim().equals("") && !isValideClient;

			Number numeroFacture = null;

			if(isCanValideClient) {
				NSDictionary pKeysFP = org.cocktail.kava.client.ServerProxy.primaryKeyForObject(ec, facturePapier);
				if(pKeysFP!=null) {
					numeroFacture = (Number)pKeysFP.valueForKey("fapId");
					urlPageValFact = urlPageValFact.replaceAll("%fapId",String.valueOf(numeroFacture.intValue()));
				}
				else {
					isCanValideClient = false;
				}
			}




			System.out.println("isValideClient="+isValideClient);
			System.out.println("isCanValideClient="+isCanValideClient);


			if (anglais) {
				body.append("Please find enclosed your invoice number ");
				body.append( facturePapier.fapNumero() );
				body.append(" : '");
				body.append(facturePapier.fapLib() );
				body.append("'.\n");

				// inviter le client à faire la validation par mail.
				if(isCanValideClient) {

						body.append("<p>");
						body.append("<b>We invite you now to confirm your purchase by clicking on the link below</b>");
						body.append("<br>");
						body.append("(authentication is required)");
						body.append("<br>");
						body.append("<br>");

						body.append("<a href=\"");
						body.append(urlPageValFact);
						body.append("\">");
						body.append(urlPageValFact);
						body.append("</a>");
						body.append("</p>");
				}
			}
			else {
				body.append("Veuillez trouver en fichier joint votre facture No ");
				body.append( facturePapier.fapNumero() );
				body.append(" : '");
				body.append(facturePapier.fapLib() );
				body.append("'.\n");

				// inviter le client à faire la validation par mail.
				if(isCanValideClient) {

						body.append("<p>");
						body.append("<b>Nous vous invitons dés maintenant à valider votre achat en cliquant sur lien ci-dessous</b>");
						body.append("<br>");
						body.append("(une authentification sera nécessaire)");
						body.append("<br>");
						body.append("<br>");

						body.append("<a href=\"");
						body.append(urlPageValFact);
						body.append("\">");
						body.append(urlPageValFact);
						body.append("</a>");
						body.append("</p>");
				}
			}
			tfBody.setText( body.toString() );
		}

		// onUpdateTypeMail();
	}

	private void setEditable(boolean editable) {
		// matrixTypeMail.setEnabled(editable);

		tfSender.setEditable(editable);
		tfTo.setEditable(editable);
		tfCC.setEditable(editable);
		tfSubject.setEditable(editable);
		tfPlus.setEditable(editable);

		actionFileSelect.setEnabled(editable);
		actionFileDelete.setEnabled(editable);
		actionFileView.setEnabled(editable);

		actionCancel.setEnabled(editable);
		actionValid.setEnabled(editable);
		actionClose.setEnabled(!editable);
	}

	private void cleanData() {
		// matrixTypeMail.clean();
		tfSender.clean();
		tfTo.clean();
		tfCC.clean();
		tfSubject.clean();
		tfBody.setText(null);
		tfPlus.clean();
		tfFileChooser.clean();
	}

	private void showRequired(boolean show) {
		// matrixTypeMail.showRequired(show);
		tfSender.showRequired(show);
		tfTo.showRequired(show);
	}

	private void onFileSelect() {
		if (fileChooser == null) {
			fileChooser = new JFileChooser();
		}
		if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			if (fileChooser.getSelectedFile() != null) {
				tfFileChooser.setText(fileChooser.getSelectedFile().getAbsolutePath());
				actionFileDelete.setEnabled(true);
			}
			else {
				tfFileChooser.setText(null);
				actionFileDelete.setEnabled(false);
			}
		}
	}

	private void onFileDelete() {
		tfFileChooser.setText(null);
		actionFileDelete.setEnabled(false);
	}

	private void onFileView() {
		if (datafile != null) {
			try {
				app.openFile(datafile, filename);
			}
			catch (Exception e) {
				app.showErrorDialog("Impossible de visualiser le fichier : " + e);
			}
		}
	}


	// TODO : implémenter envoi de mail
	private void onUserSystemMailer() {


		String tmpDir = System.getProperty("java.io.tmpdir");
		String separator = System.getProperty("file.separator");
		String fullPathName = tmpDir+separator+filename;
		FileOutputStream fos;
		boolean noFileOnDisk = false;
		try {
			fos = new FileOutputStream(fullPathName);
			fos.write(datafile.bytes());
			fos.close();

		} catch (Exception e) {
			noFileOnDisk = true;
			e.printStackTrace();
		}

		String mailtoLink = "mailto:%to%&subject=%subject%&body=%body%&attachment=%attachment%";
		String tmp = tfTo.getText();
		mailtoLink = mailtoLink.replaceAll("%to%", tmp);
		tmp = tfSubject.getText();
		mailtoLink = mailtoLink.replaceAll("%subject%", tmp);
		tmp = tfBody.getText() + "\n" + tfPlus.getText();
		mailtoLink = mailtoLink.replaceAll("%body%", tmp);
		if(!noFileOnDisk) {
			mailtoLink = mailtoLink.replaceAll("%attachment%", fullPathName);
		}

	}

	private void initGUI() {
		JPanel contentPanel = new JPanel(new BorderLayout());
		contentPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		contentPanel.setPreferredSize(WINDOW_DIMENSION);
		setContentPane(contentPanel);

		Border commonEmptyThinBorder = BorderFactory.createEmptyBorder(0, 3, 0, 3);

		// type
		// JPanel panelTypeMail = UIUtilities.labeledComponent("Type de mail", matrixTypeMail, null, true);

		// sender
		JPanel panelSender = UIUtilities.labeledComponent("Exp\u00E9diteur", tfSender, null);
		panelSender.setBorder(commonEmptyThinBorder);

		// to
		JPanel panelTo = UIUtilities.labeledComponent("A", tfTo, null);
		panelTo.setBorder(commonEmptyThinBorder);

		// cc
		JPanel panelCc = UIUtilities.labeledComponent("Cc", tfCC, null);
		panelCc.setBorder(commonEmptyThinBorder);

		// subject
		JPanel panelSubject = UIUtilities.labeledComponent("Sujet", tfSubject, null);
		panelSubject.setBorder(commonEmptyThinBorder);

		// body
		JScrollPane panelBody = new JScrollPane(tfBody, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		// plus
		JPanel panelPlus = UIUtilities.labeledComponent("Commentaires \u00E0 ajouter au mail", tfPlus, null);
		panelPlus.setBorder(commonEmptyThinBorder);

		// fichier li\u00E9
		// JPanel panelFileChooser = UIUtilities.labeledComponent("Fichier li\u00E9", new ZDropBox(tfFileChooser), new Action[] { actionFileSelect,
		// actionFileDelete }, true, UIUtilities.DEFAULT_ALIGNMENT);

		// label et visu
		JPanel labelAndVisu = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		labelAndVisu.setBorder(BorderFactory.createEmptyBorder());
		labelAndVisu.add(labelInfo);
		labelAndVisu.add(Box.createHorizontalStrut(10));
		btFileView = new JButton(actionFileView);
		labelAndVisu.add(btFileView);

		JPanel center = new JPanel(new BorderLayout());
		center.setBorder(BorderFactory.createEmptyBorder());

		// center.add(panelTypeMail);
		Box top = Box.createVerticalBox();
		top.setBorder(BorderFactory.createEmptyBorder());
		top.add(panelSender);
		top.add(panelTo);
		top.add(panelCc);
		top.add(panelSubject);
		top.add(Box.createVerticalStrut(10));

		center.add(top, BorderLayout.NORTH);
		center.add(panelBody);
		center.add(labelAndVisu, BorderLayout.SOUTH);

		// center.add(panelPlus);
		// center.add(panelFileChooser);

		getContentPane().add(center, BorderLayout.CENTER);
		getContentPane().add(buildBottomPanel(), BorderLayout.PAGE_END);

		initInputMap();

		this.validate();
		this.pack();
	}

	private final JPanel buildBottomPanel() {
		JPanel p = new JPanel();
		p.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.add(Box.createHorizontalGlue());
		p.add(btCancel);
		p.add(Box.createRigidArea(new Dimension(5, 0)));
		p.add(btValid);
		return p;
	}

	private void initInputMap() {
		((JComponent) getContentPane()).getActionMap().put("CTRL_Q", app.superviseur().mainMenu.actionQuit);
		((JComponent) getContentPane()).getActionMap().put("CTRL_S", actionValid);
		((JComponent) getContentPane()).getActionMap().put("F10", actionValid);
		((JComponent) getContentPane()).getActionMap().put("ESCAPE", actionCancel);
		((JComponent) getContentPane()).getActionMap().put("F8", actionShowRequired);
		((JComponent) getContentPane()).getActionMap().put("ALT_F8", actionUnshowRequired);
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
				KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK), "CTRL_Q");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
				KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK), "CTRL_S");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F10, 0), "F10");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"ESCAPE");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("F8"), "F8");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("released F8"), "ALT_F8");
	}

	// private class MatrixItemListener implements ItemListener {
	// public void itemStateChanged(ItemEvent arg0) {
	// onUpdateTypeMail();
	// }
	// }

	private class ActionValid extends AbstractAction {
		public ActionValid() {
			super("Valider");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onValid();
			}
		}
	}

	private class ActionCancel extends AbstractAction {
		public ActionCancel() {
			super("Annuler");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CANCEL_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onCancel();
			}
		}
	}

	private class ActionClose extends AbstractAction {
		public ActionClose() {
			super("Fermer");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CLOSE_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onClose();
			}
		}
	}

	private class ActionShowRequired extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				showRequired(true);
			}
		}
	}

	private class ActionUnshowRequired extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				showRequired(false);
			}
		}
	}

	private class ActionFileSelect extends AbstractAction {
		public ActionFileSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onFileSelect();
			}
		}
	}

	private class ActionFileDelete extends AbstractAction {
		public ActionFileDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onFileDelete();
			}
		}
	}

	private class ActionFileView extends AbstractAction {
		public ActionFileView() {
			super("Visualiser");
			putValue(AbstractAction.SHORT_DESCRIPTION, "Visualiser le fichier qui sera joint au mail");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_LOUPE_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onFileView();
			}
		}
	}



	private class ActionUseSystemMailer extends AbstractAction {
		public ActionUseSystemMailer() {
			super("Utiliser mon logiciel de mail");
			putValue(AbstractAction.SHORT_DESCRIPTION, "Utiliser le logiciel d'envoi de mail de mon ordinateur");
			//putValue(AbstractAction.SMALL_ICON, Constants.ICON_LOUPE_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onUserSystemMailer();
			}
		}
	}


	private class LocalWindowListener implements WindowListener {
		public void windowActivated(WindowEvent arg0) {
		}

		public void windowClosed(WindowEvent arg0) {
		}

		public void windowClosing(WindowEvent arg0) {
			actionClose.actionPerformed(null);
		}

		public void windowDeactivated(WindowEvent arg0) {
		}

		public void windowDeiconified(WindowEvent arg0) {
		}

		public void windowIconified(WindowEvent arg0) {
		}

		public void windowOpened(WindowEvent arg0) {
		}
	}

	public void setWaitCursor(boolean bool) {
		if (bool) {
			this.getGlassPane().setVisible(true);
		}
		else {
			this.getGlassPane().setVisible(false);
		}
	}

	/**
	 * Permet de d\u00E9finir un panel qui sert a afficher un waitcursor et a interdire toute modif sur le panel.
	 *
	 * @author rprin
	 */
	public final class BusyGlassPanel extends JPanel {
		public final Color	COLOR_WASH	= new Color(64, 64, 64, 32);

		public BusyGlassPanel() {
			super.setOpaque(false);
			super.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

			// Suck up them events!!!
			super.addKeyListener((new KeyAdapter() {
			}));
			super.addMouseListener((new MouseAdapter() {
			}));
			super.addMouseMotionListener((new MouseMotionAdapter() {
			}));
		}

		public final void paintComponent(Graphics p_graphics) {
			Dimension l_size = super.getSize();

			// Wash the pane with translucent gray.
			p_graphics.setColor(COLOR_WASH);
			p_graphics.fillRect(0, 0, l_size.width, l_size.height);

			// Paint a grid of white/black dots.
			p_graphics.setColor(Color.white);
			for (int j = 3; j < l_size.height; j += 8) {
				for (int i = 3; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
			p_graphics.setColor(Color.black);
			for (int j = 4; j < l_size.height; j += 8) {
				for (int i = 4; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
		}
	}

}
