/*
CRI - Universite de La Rochelle, 2006

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.kava.client.factory.FactoryRecetteCtrlPlancoCtp;
import org.cocktail.kava.client.finder.FinderPlancoVisa;
import org.cocktail.kava.client.metier.EOGestion;
import org.cocktail.kava.client.metier.EOPlanComptable;
import org.cocktail.kava.client.metier.EOPlancoVisa;
import org.cocktail.kava.client.metier.EORecetteCtrlPlanco;
import org.cocktail.kava.client.metier.EORecetteCtrlPlancoCtp;

import app.client.select.PlanComptableCtpSelect;
import app.client.ui.UIUtilities;
import app.client.ui.ZEOComboBox;
import app.client.ui.ZTextField;
import app.client.ui.table.ZEOTableModelColumn;
import app.client.ui.table.ZExtendedTablePanel;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class RecetteCtrlPlancoCtpPanel extends CtrlPanel {

	private static final boolean	DEFAULT_MULTIPLE_INIT			= true;	    // demarre par defaut en multiple ou single ?
	private static final boolean	DEFAULT_MULTIPLE_INIT_LOCKED	= false;	// interdit ou non le changement
	private static final String     ERR_CODE_GESTION_SACD = "Le code gestion de la contrepartie devrait être {0}.";

	private ZEOTableModelColumn   	colTTC, colGesCode;
	private ZEOComboBox		     	cbGestion;
	private ZTextField      		tfCodeGestion;

	private PlanComptableCtpSelect	planComptableCtpSelect;

	private EORecetteCtrlPlanco		currentObject;

	public RecetteCtrlPlancoCtpPanel() {
		super("Compte d'imputation de contrepartie", 25);
	}

	@Override
	protected void onCreate(String labelSingle, int textFieldSize) {
		super.onCreate(labelSingle, textFieldSize);
		tfCodeGestion = new ZTextField(5);
		tfCodeGestion.setViewOnly();
		planComptableCtpSelect = new PlanComptableCtpSelect();
	}

	@Override
	protected Component createGUIWhenAcceptSingle(String labelSingle) {
		Component parentUI = super.createGUIWhenAcceptSingle(labelSingle);
		JPanel panelWhenSingle = new JPanel(new BorderLayout());
		panelWhenSingle.add(parentUI, BorderLayout.PAGE_START);
		panelWhenSingle.add(UIUtilities.labeledComponent("Code Gestion", tfCodeGestion, null), BorderLayout.PAGE_END);
		return panelWhenSingle;
	}

	public void setEditable(boolean editable) {
		colTTC.setEditable(editable);
		colGesCode.setEditable(editable);
		super.setEditable(editable);
	}

	public void setColumnsEditable(boolean editable) {
		colTTC.setEditable(editable);
	}

	public void setCurrentObject(EORecetteCtrlPlanco object) {
		this.currentObject = object;
		updateComboBoxCodeGestion(getGestion(currentObject));
		updateData();
	}

	public void add(EOPlanComptable eo) {
		if (currentObject == null || eo == null) {
			return;
		}
		// si on est en mode multiple ou qu'il n'y a pas encore de ctrl, on ajoute
		if (multipleEnable || currentObject.recetteCtrlPlancoCtps() == null || currentObject.recetteCtrlPlancoCtps().count() == 0) {
			EORecetteCtrlPlancoCtp object = FactoryRecetteCtrlPlancoCtp.newObject(ec);
			object.setPlanComptableRelationship(eo);
			object.setExerciceRelationship(currentObject.exercice());
			object.setGestionRelationship(getGestion(currentObject));
			setMontant(object);
			if (object.rpcoctpTtcSaisie() == null || object.rpcoctpTtcSaisie().compareTo(new BigDecimal(0.0)) == 0) {
				ec.deleteObject(object);
			} else {
				currentObject.addToRecetteCtrlPlancoCtpsRelationship(object);
			}
		} else {
			// on est en mode single et y'a deja un ctrl, on le modifie
			EORecetteCtrlPlancoCtp recCtrlPlancoCtp =
					(EORecetteCtrlPlancoCtp) currentObject.recetteCtrlPlancoCtps().objectAtIndex(0);
			recCtrlPlancoCtp.setPlanComptableRelationship(eo);
			recCtrlPlancoCtp.setGestionRelationship(getGestion(currentObject));
			setMontant(recCtrlPlancoCtp);
			if (recCtrlPlancoCtp.rpcoctpTtcSaisie() == null
					|| recCtrlPlancoCtp.rpcoctpTtcSaisie().compareTo(new BigDecimal(0.0)) == 0) {
				currentObject.removeFromRecetteCtrlPlancoCtpsRelationship(recCtrlPlancoCtp);
				ec.deleteObject(recCtrlPlancoCtp);
			}
		}
		updateData();
	}

	protected void onAdd() {
		if (planComptableCtpSelect.open(currentObject.recette().utilisateur(), currentObject.exercice(), null)) {
			try {
				add(planComptableCtpSelect.getSelected());
			} catch (Exception e) {
				System.err.println("Erreur lors de la tentative d'insertion d'un nouvel objet RecetteCtrlPlancoCtp: " + e);
			}
		}
	}

	protected void onSelect() {
		EOPlanComptable current = null;
		if (currentObject == null) {
			return;
		}
		if (currentObject.recetteCtrlPlancoCtps() != null
				&& currentObject.recetteCtrlPlancoCtps().count() > 0) {
			current = ((EORecetteCtrlPlancoCtp) currentObject.recetteCtrlPlancoCtps().objectAtIndex(0)).planComptable();
		}
		if (planComptableCtpSelect.open(currentObject.recette().utilisateur(), currentObject.exercice(), current)) {
			try {
				add(planComptableCtpSelect.getSelected());
			} catch (Exception e) {
				System.err.println("Erreur lors de la tentative d'insertion/modification d'un nouvel objet RecetteCtrlPlancoCtp: " + e);
			}
		}
	}

	protected void onUpdate() {
		if (table.selectedObject() != null) {
			if (planComptableCtpSelect.open(currentObject.recette().utilisateur(), currentObject.exercice(), ((EORecetteCtrlPlancoCtp) table
					.selectedObject()).planComptable())) {
				((EORecetteCtrlPlancoCtp) table.selectedObject()).setPlanComptableRelationship(planComptableCtpSelect.getSelected());
				table.updateUI();
			}
		}
	}

	protected void onDelete() {
		if (table.selectedObject() != null && app.showConfirmationDialog("ATTENTION", "Supprimer cette ligne??", "OUI", "NON")) {
			currentObject.removeFromRecetteCtrlPlancoCtpsRelationship((EORecetteCtrlPlancoCtp) table.selectedObject());
			ec.deleteObject(table.selectedObject());
			updateData();
		}
	}

	private void setMontant(EORecetteCtrlPlancoCtp ctrl) {
		// on fixe le ttc par rapport soit au restant du montant ttc de la recette, soit au montant ttc de la recette
		BigDecimal newTtc = currentObject.rpcoTtcSaisie();
		if (multipleEnable && newTtc != null) {
			newTtc = newTtc.subtract((BigDecimal) currentObject.recetteCtrlPlancoCtps().valueForKey(
					"@sum." + EORecetteCtrlPlancoCtp.RPCOCTP_TTC_SAISIE_KEY));
		}
		if (newTtc == null || newTtc.doubleValue() < 0.0) {
			newTtc = new BigDecimal(0.0);
		}
		ctrl.setRpcoctpTtcSaisie(newTtc);
		table.updateData();
	}

	/**
	 * Retrieve EOGestion entity bound to the EORecetteCtrlPlanco specified.
	 * @param ctrlPlanco EORecetteCtrlPlanco.
	 * @return EOGestion bound to the EORecetteCtrlPlanco.
	 */
	private EOGestion getGestion(EORecetteCtrlPlanco ctrlPlanco) {
		EOGestion gestion = null;
		if (!hasOrgan(ctrlPlanco)) {
			return gestion;
		}
		try {
			gestion = ctrlPlanco.recette().facture().organ().gestion();
			if (!gestion.isSacd(ctrlPlanco.exercice())) {
				NSArray array = FinderPlancoVisa.find(
						ctrlPlanco.editingContext(), ctrlPlanco.planComptable(), ctrlPlanco.exercice());
				if (array != null && array.count() == 1 && !"COMPOSANTE".equalsIgnoreCase(
						((EOPlancoVisa) array.objectAtIndex(0)).pviContrepartieGestion())) {
					gestion = gestion.comptabilite().gestion();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			gestion = null;
		}
		return gestion;
	}
	
	private boolean hasOrgan(EORecetteCtrlPlanco ctrlPlanco) {
		return ctrlPlanco != null
				&& ctrlPlanco.recette() != null
				&& ctrlPlanco.recette().facture() != null
				&& ctrlPlanco.recette().facture().organ() != null;
	}

	private void updateComboBoxCodeGestion(EOGestion gestion) {
		cbGestion.removeAllItems();
		if (gestion == null) {
			return;
		}
		Set<EOGestion> codesGestioAutorises = new HashSet<EOGestion>();
		codesGestioAutorises.add(gestion);
		if (!gestion.isSacd(currentObject.exercice())) {
			codesGestioAutorises.add(gestion.comptabilite().gestion());
		}
		this.cbGestion.setObjects(new NSArray(codesGestioAutorises.toArray()));
	}

	public void updateMontant() {
		if (currentObject == null || currentObject.recetteCtrlPlancoCtps() == null || currentObject.recetteCtrlPlancoCtps().count() != 1) {
			return;
		}
		((EORecetteCtrlPlancoCtp) currentObject.recetteCtrlPlancoCtps().objectAtIndex(0)).setRpcoctpTtcSaisie(currentObject.rpcoTtcSaisie());
		table.updateData();
	}

	public void updateCodeGestionContreparties() {
		if (!hasCtrlPlancoCtps()) {
			return;
		}

		EOGestion gestion = getGestion(currentObject);
		if (gestion == null) {
			return;
		}
		NSArray ctrlPlancoCtps = currentObject.recetteCtrlPlancoCtps();
		for (int idxCtrlPlancoCtp = 0; idxCtrlPlancoCtp < ctrlPlancoCtps.count(); idxCtrlPlancoCtp++) {
			EORecetteCtrlPlancoCtp recCtrlPlancoCtp = (EORecetteCtrlPlancoCtp)
					ctrlPlancoCtps.objectAtIndex(idxCtrlPlancoCtp);
			recCtrlPlancoCtp.setGestionRelationship(gestion);
		}
		updateComboBoxCodeGestion(gestion);
		updateData();
	}

	public void controlerCodeGestionContreparties() {
		if (!hasCtrlPlancoCtps()) {
			return;
		}
		EOGestion gestion = getGestion(currentObject);
		if (gestion != null && gestion.isSacd(currentObject.exercice())) {
			NSArray ctrlPlancoCtps = currentObject.recetteCtrlPlancoCtps();
			for (int idxCtrlPlancoCtp = 0; idxCtrlPlancoCtp < ctrlPlancoCtps.count(); idxCtrlPlancoCtp++) {
				EORecetteCtrlPlancoCtp recCtrlPlancoCtp = (EORecetteCtrlPlancoCtp)
						ctrlPlancoCtps.objectAtIndex(idxCtrlPlancoCtp);
				if (recCtrlPlancoCtp.gestion() != null
						&& !gestion.gesCode().equals(recCtrlPlancoCtp.gestion().gesCode())) {
					throw new NSValidation.ValidationException(
							MessageFormat.format(ERR_CODE_GESTION_SACD, gestion.gesCode()));
				}
			}
		}
	}

	private boolean hasCtrlPlancoCtps() {
		return currentObject != null
				&& currentObject.recetteCtrlPlancoCtps() != null && currentObject.recetteCtrlPlancoCtps().count() > 0;
	}

	protected void updateData() {
		if (currentObject == null) {
			table.setObjectArray(null);
			tfCtrl.clean();
			tfCodeGestion.clean();
			setEditable(false);
		} else {
			setEditable(true);
			if (multipleEnable) {
				table.setObjectArray(currentObject.recetteCtrlPlancoCtps());
			} else {
				if (currentObject.recetteCtrlPlancoCtps() == null
						|| currentObject.recetteCtrlPlancoCtps().count() == 0) {
					tfCtrl.clean();
					tfCodeGestion.clean();
				} else {
					// on vire ceux qui sont en trop ! et on en garde un... le dernier...
					while (currentObject.recetteCtrlPlancoCtps().count() > 1) {
						EORecetteCtrlPlancoCtp ctrl =
								(EORecetteCtrlPlancoCtp) currentObject.recetteCtrlPlancoCtps().objectAtIndex(0);
						currentObject.removeFromRecetteCtrlPlancoCtpsRelationship(ctrl);
						ec.deleteObject(ctrl);
					}
					EORecetteCtrlPlancoCtp ctrl =
							(EORecetteCtrlPlancoCtp) currentObject.recetteCtrlPlancoCtps().objectAtIndex(0);
					setMontant(ctrl);
					tfCtrl.clean();
					tfCodeGestion.clean();
					if (ctrl.planComptable() != null) {
						tfCtrl.setText(ctrl.planComptable().pcoNum() + " - " + ctrl.planComptable().pcoLibelle());
					}
					if (ctrl != null && ctrl.gestion() != null) {
						tfCodeGestion.setText(ctrl.gestion().gesCode());
					}
				}
			}
		}
	}

	private final class TablePanel extends ZExtendedTablePanel {

		public TablePanel() {
			super("Comptes d'imputation de contrepartie");

			final ZEOTableModelColumn.ZEONumFieldTableCellEditor _editor = new ZEOTableModelColumn.ZEONumFieldTableCellEditor(new JTextField(),
					app.getCurrencyFormatEdit());

			cbGestion = new ZEOComboBox(null, EOGestion.GES_CODE_KEY, null, null, null, 60);
			final ZEOTableModelColumn.ZEOComboBoxTableCellEditor _editor2 = new ZEOTableModelColumn.ZEOComboBoxTableCellEditor(cbGestion);

			addCol(new ZEOTableModelColumn(getDG(), EORecetteCtrlPlancoCtp.PLAN_COMPTABLE_KEY + "." + EOPlanComptable.PCO_NUM_KEY, "Code", 40));
			addCol(new ZEOTableModelColumn(getDG(), EORecetteCtrlPlancoCtp.PLAN_COMPTABLE_KEY + "." + EOPlanComptable.PCO_LIBELLE_KEY, "Lib",
					120));
			colTTC = new ZEOTableModelColumn(getDG(), EORecetteCtrlPlancoCtp.RPCOCTP_TTC_SAISIE_KEY, "TTC", 70, app.getCurrencyFormatDisplay());
			colTTC.setEditable(true);
			colTTC.setTableCellEditor(_editor);
			colTTC.setFormatEdit(app.getCurrencyFormatEdit());
			colTTC.setColumnClass(BigDecimal.class);
			addCol(colTTC);
			colGesCode = new ZEOTableModelColumn(getDG(), EORecetteCtrlPlancoCtp.GESTION_KEY + "." + EOGestion.GES_CODE_KEY, "Code gestion", 60);
			colGesCode.setEditable(true);
			colGesCode.setTableCellEditor(_editor2);
			colGesCode.setMyModifier(new GesCodeModifier());
			addCol(colGesCode);

			initGUI();

			setPreferredSize(new Dimension(0, 70));
		}

		private class GesCodeModifier implements ZEOTableModelColumn.Modifier {
			public void setValueAtRow(Object value, int row) {
				((EORecetteCtrlPlancoCtp) (displayedObjects().objectAtIndex(row))).setGestionRelationship((EOGestion) value);
			}
		}

		protected void onAdd() {
			RecetteCtrlPlancoCtpPanel.this.onAdd();
		}

		protected void onUpdate() {
			RecetteCtrlPlancoCtpPanel.this.onUpdate();
		}

		protected void onDelete() {
			RecetteCtrlPlancoCtpPanel.this.onDelete();
		}

		protected void onSelectionChanged() {
		}

	}

	protected boolean defaultMultipleInit() {
		return DEFAULT_MULTIPLE_INIT;
	}

	protected boolean defaultMultipleInitLocked() {
		return DEFAULT_MULTIPLE_INIT_LOCKED;
	}

	protected ZExtendedTablePanel getTablePanel() {
		return new TablePanel();
	}

}
