/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.math.BigDecimal;
import java.util.Date;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.cocowork.client.exception.ExceptionConventionModification;
import org.cocktail.cocowork.client.facade.convention.FacadeEditionConventionFormation;
import org.cocktail.cocowork.client.facade.convention.FacadeEditionGeneralitesConventionFormation;
import org.cocktail.cocowork.client.metier.convention.ModeGestion;
import org.cocktail.cocowork.client.metier.convention.TypeContrat;
import org.cocktail.cocowork.client.metier.convention.TypeReconduction;
import org.cocktail.cocowork.client.proxy.ProxyPrestationConventionFormation;
import org.cocktail.kava.client.factory.FactoryPrestation;
import org.cocktail.kava.client.factory.FactoryPrestationAdrClient;
import org.cocktail.kava.client.factory.FactoryPrestationLigne;
import org.cocktail.kava.client.finder.FinderCataloguePublic;
import org.cocktail.kava.client.finder.FinderLolfNomenclatureRecette;
import org.cocktail.kava.client.finder.FinderModeRecouvrement;
import org.cocktail.kava.client.finder.FinderTypeApplication;
import org.cocktail.kava.client.finder.FinderTypeArticle;
import org.cocktail.kava.client.finder.FinderTypeCreditRec;
import org.cocktail.kava.client.finder.FinderTypePublic;
import org.cocktail.kava.client.metier.EOAdresse;
import org.cocktail.kava.client.metier.EOCatalogueArticle;
import org.cocktail.kava.client.metier.EOCataloguePublic;
import org.cocktail.kava.client.metier.EOConvention;
import org.cocktail.kava.client.metier.EOFonction;
import org.cocktail.kava.client.metier.EOLolfNomenclatureRecette;
import org.cocktail.kava.client.metier.EOModeRecouvrement;
import org.cocktail.kava.client.metier.EOOrgan;
import org.cocktail.kava.client.metier.EOParametres;
import org.cocktail.kava.client.metier.EOPrestation;
import org.cocktail.kava.client.metier.EOPrestationAdrClient;
import org.cocktail.kava.client.metier.EOPrestationLigne;
import org.cocktail.kava.client.metier.EOTva;
import org.cocktail.kava.client.metier.EOTypeArticle;
import org.cocktail.kava.client.metier.EOTypePublic;
import org.cocktail.kava.client.metier.EOUtilisateur;
import org.cocktail.kava.client.service.PrestationService;
import org.cocktail.pieFwk.common.exception.EntityInitializationException;

import app.client.select.CatalogueArticleSelect;
import app.client.select.CatalogueSelect;
import app.client.select.ClientSelect;
import app.client.select.ContactSelect;
import app.client.select.ConventionSelect;
import app.client.select.FournisUlrInterneSelect;
import app.client.select.LolfNomenclatureRecetteSelect;
import app.client.select.OrganSelect;
import app.client.select.validator.FournisseurEtatValideValidator;
import app.client.select.validator.SelectValidatorSeverity;
import app.client.tools.Constants;
import app.client.ui.DisabledGlassPane;
import app.client.ui.UIUtilities;
import app.client.ui.ZEOComboBox;
import app.client.ui.ZLabel;
import app.client.ui.ZNumberField;
import app.client.ui.ZTextArea;
import app.client.ui.ZTextField;
import app.client.ui.table.ZEOTableModelColumn;
import app.client.ui.table.ZExtendedTablePanel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSTimestamp;

public class PrestationAddUpdate extends JDialog {

	private static final String WINDOW_TITLE = "Prestation";

	protected ApplicationClient app;

	protected EOEditingContext ec;

	private static PrestationAddUpdate sharedInstance;

	private EOPrestation eoPrestation;

	private PrestationBudgetClientAddUpdate prestationBudgetClient;

	private Action actionClose, actionValid, actionCancel;

	private Action actionShowRequired, actionUnshowRequired;

	private Action actionCatalogueSelect, actionClientSelect, actionContactClientSelect, actionContactClientDelete;

	private Action actionOrganSelect, actionOrganDelete;

	private Action actionLolfNomenclatureRecetteSelect, actionLolfNomenclatureRecetteDelete;

	private Action actionConventionSelect, actionConventionDelete, actionConventionOpen;

	private Action actionCommentaires;

	private ZLabel labelInfo;

	private ZNumberField tfPrestNumero;

	private ZTextField tfExercice;

	private ZEOComboBox cbTypePublic;

	private CbActionListener cbActionListener;

	private ZTextField tfClient, tfContactClient;

	private ClientSelect clientSelect;
	private ContactSelect contactClientSelect;

	private FournisUlrInterneSelect fournisUlrInterneSelect;

	private ZTextField tfCatalogue;

	private CatalogueSelect catalogueSelect;

	private ZTextField tfPrestLibelle;

	private PrestationLigneTablePanel prestationLigneTablePanel;

	private PrestationLigneORTablePanel prestationLigneORTablePanel;

	private CatalogueArticleSelect catalogueArticleSelect;

	private ZTextArea tfCommentairePrest, tfCommentaireClient;

	private JTabbedPane jtpBudget;

	private ZTextField tfOrgan;

	private OrganSelect organSelect;

	private ZEOComboBox cbTypeCreditRec;

	private ZEOComboBox cbModeRecouvrement;

	private ZTextField tfLolfNomenclatureRecette;

	private LolfNomenclatureRecetteSelect lolfNomenclatureRecetteSelect;

	private ZTextField tfConvention;

	private ConventionSelect conventionSelect;

	protected EOTypePublic typeFC;

	private JCheckBox checkBoxLasm;

	private Action actionCheckLasm;

	private ZNumberField tfPrestRemiseGlobale, tfTotalHt, tfTotalTva, tfTotalTtc;

	private JButton btValid, btCancel;

	private JDialog dialogCommentaires;

	private boolean result;

	private EOAdresse adresseClientSelected;

	private PrestationService prestationService = PrestationService.instance();

	/**
	 * Pour fwk Cocowork
	 */
	private EOEditingContext ecCocowork;
	private FacadeEditionConventionFormation facadeEditionConventionFormation;
	private EOTypePublic _typeFC;

	public PrestationAddUpdate() {
		super((JFrame) null, WINDOW_TITLE, true);

		app = (ApplicationClient) EOApplication.sharedApplication();
		ec = app.editingContext();

		typeFC = FinderTypePublic.typePublicFormationContinue(ec);
		_typeFC = FinderTypePublic.typePublicFormationContinue(ec);

		setGlassPane(new BusyGlassPanel());
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new LocalWindowListener());

		prestationBudgetClient = new PrestationBudgetClientAddUpdate();

		actionClose = new ActionClose();
		actionValid = new ActionValid();
		actionCancel = new ActionCancel();
		actionShowRequired = new ActionShowRequired();
		actionUnshowRequired = new ActionUnshowRequired();
		actionCatalogueSelect = new ActionCatalogueSelect();
		actionClientSelect = new ActionClientSelect();
		actionContactClientSelect = new ActionContactClientSelect();
		actionContactClientDelete = new ActionContactClientDelete();
		actionOrganSelect = new ActionOrganSelect();
		actionOrganDelete = new ActionOrganDelete();
		actionLolfNomenclatureRecetteSelect = new ActionLolfNomenclatureRecetteSelect();
		actionLolfNomenclatureRecetteDelete = new ActionLolfNomenclatureRecetteDelete();
		actionConventionSelect = new ActionConventionSelect();
		actionConventionOpen = new ActionConventionOpen();
		actionConventionDelete = new ActionConventionDelete();
		actionCommentaires = new ActionCommentaires();

		actionCheckLasm = new ActionCheckLasm();
		checkBoxLasm = new JCheckBox(actionCheckLasm);

		tfPrestNumero = new ZNumberField(4);
		tfExercice = new ZTextField(4);
		labelInfo = new ZLabel("", Constants.ICON_INFO_32, SwingConstants.LEADING);

		cbTypePublic = new ZEOComboBox(FinderTypePublic.findAll(ec), EOTypePublic.TYPU_LIBELLE_KEY, null, null, null, 200);
		cbActionListener = new CbActionListener();
		cbTypePublic.addActionListener(cbActionListener);

		tfClient = new ZTextField(45);
		tfClient.setViewOnly();
		tfContactClient = new ZTextField(30);
		tfContactClient.setViewOnly();

		tfCatalogue = new ZTextField(50);
		tfCatalogue.setViewOnly();

		tfPrestLibelle = new ZTextField(60);

		tfCommentairePrest = new ZTextArea(6, 60);
		tfCommentaireClient = new ZTextArea(6, 60);
		dialogCommentaires = new JDialog(this, "Commentaires", false);

		tfOrgan = new ZTextField(20);
		tfOrgan.setViewOnly();
		cbTypeCreditRec = new ZEOComboBox(new NSArray(), EOTypeCredit.LIB_KEY, null, null, null, 150);
		cbTypeCreditRec.addActionListener(cbActionListener);
		tfLolfNomenclatureRecette = new ZTextField(30);
		tfLolfNomenclatureRecette.setViewOnly();
		cbModeRecouvrement = new ZEOComboBox(new NSArray(), EOModeRecouvrement.LIB_KEY, "", null, null, 200);
		cbModeRecouvrement.addActionListener(cbActionListener);
		tfConvention = new ZTextField(25);
		tfConvention.setViewOnly();

		tfPrestRemiseGlobale = new ZNumberField(10, Constants.FORMAT_DECIMAL_EDIT);
		tfPrestRemiseGlobale.getDocument().addDocumentListener(new RemiseGlobaleDocumentListener());
		tfTotalHt = new ZNumberField(10, app.getCurrencyFormatDisplay());
		tfTotalHt.setViewOnly();
		tfTotalTva = new ZNumberField(10, app.getCurrencyFormatDisplay());
		tfTotalTva.setViewOnly();
		tfTotalTtc = new ZNumberField(10, app.getCurrencyFormatDisplay());
		tfTotalTtc.setViewOnly();

		prestationLigneTablePanel = new PrestationLigneTablePanel();
		prestationLigneORTablePanel = new PrestationLigneORTablePanel();

		btCancel = new JButton(actionCancel);
		btValid = new JButton(actionValid);

		ecCocowork = new EOEditingContext();
		facadeEditionConventionFormation = null;

		initGUI();
	}

	public static PrestationAddUpdate sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new PrestationAddUpdate();
		}
		return sharedInstance;
	}

	public boolean openNew(EOExercice exercice) throws EntityInitializationException {
		if (exercice == null) {
			System.err.println("[PrestationAddUpdate:open(EOExercice, EOCatalogue)] Aucun exercice pass\u00E9 pour ouvrir en cr\u00E9ation !");
			throw new EntityInitializationException("Aucun exercice passé pour ouvrir en création.");
		}
		app.superviseur().setWaitCursor(this, true);
		eoPrestation = FactoryPrestation.newObject(ec);
		eoPrestation.setExerciceRelationship(exercice);
		eoPrestation.setUtilisateurRelationship(app.appUserInfo().utilisateur());

		// init du type de public
		onTypePublicUpdate((EOTypePublic) cbTypePublic.getSelectedEOObject());

		setTitle(WINDOW_TITLE + " - Cr\u00E9ation");

		updateData();

		return open();
	}

	public boolean open(EOPrestation prestation) {
		if (prestation == null) {
			System.err.println("[PrestationAddUpdate:open(EOPrestation)] Aucune prestation pass\u00E9e pour ouvrir en modif !");
			return false;
		}
		app.superviseur().setWaitCursor(this, true);

		this.eoPrestation = prestation;

		setTitle(WINDOW_TITLE + " - Modification");
		updateData();

		// charge la facade d'edition de convention a partir de la convention
		// liee a la presta
		try {
			conventionChargerFacadesConvention(eoPrestation.convention());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return open();
	}

	public boolean canQuit() {
		if (isShowing()) {
			actionCancel.actionPerformed(null);
		}
		return !isShowing();
	}

	public EOPrestation getLastOpened() {
		return eoPrestation;
	}

	private boolean open() {
		result = false;
		adresseClientSelected = null;
		int screenWidth = (int) this.getGraphicsConfiguration().getBounds().getWidth();
		int screenHeight = (int) this.getGraphicsConfiguration().getBounds().getHeight();
		this.setLocation((screenWidth / 2) - ((int) this.getSize().getWidth() / 2), ((screenHeight / 2) - ((int) this.getSize().getHeight() / 2)));
		app.superviseur().setWaitCursor(this, false);
		setVisible(true);
		return result;
	}

	private void onValid() {
		try {
			app.superviseur().setWaitCursor(this, true);
			eoPrestation.setPrestLibelle(tfPrestLibelle.getText());
			eoPrestation.setPrestCommentairePrest(tfCommentairePrest.getText());
			eoPrestation.setPrestCommentaireClient(tfCommentaireClient.getText());

			setTypeCreditRec();

			// si pas encore d'utilisateur, on affecte celui qui est en train de
			// tripatouiller la prestation...
			if (eoPrestation.utilisateur() == null) {
				eoPrestation.setUtilisateurRelationship(app.appUserInfo().utilisateur());
			}

			EOPrestationAdrClient currentAdrPrestClient = eoPrestation.currentPrestationAdresseClient();
			EOAdresse currentAdresse = currentAdrPrestClient == null ? null : currentAdrPrestClient.toAdresse();
			if (adresseClientSelected != null && !adresseClientSelected.equals(currentAdresse)) {
				EOPrestationAdrClient eoPrestationAdrClient = FactoryPrestationAdrClient.newObject(
						ec, app.getCurrentUtilisateur().personne(), eoPrestation, adresseClientSelected);
				eoPrestation.addToToPrestationAdrClientsRelationship(eoPrestationAdrClient);
				if (currentAdrPrestClient != null) {
					// Tips : pour l'instant il est difficile de gerer un historique pour les recettes
					// par souci d'uniformite on applique la meme logique sur les prestations.
					// on a une table historique contenant 1 seule ligne.
					eoPrestation.removeFromToPrestationAdrClientsRelationship(currentAdrPrestClient);
					ec.deleteObject(currentAdrPrestClient);
				}
			}

			// si pas de taux de prorata, on affecte le 100%
			eoPrestation.updateTauxProrata();
			ec.saveChanges();

			// maj convention liee
			conventionMettreAJour();

			result = true;
			setVisible(false);
			//hide();
		} catch (Exception e) {
			app.superviseur().setWaitCursor(this, false);
			app.showErrorDialog(e);
		}
 finally {
			app.superviseur().setWaitCursor(this, false);
		}
	}

	private void onCancel() {
		if (app.showConfirmationDialog("ATTENTION", "Annuler ?", "OUI!", "NON")) {

			try {
				getFacadeEditionConventionFormation().annulerModifications();
			} catch (Exception e) {
				e.printStackTrace();
			}

			ec.revert();
			ec.saveChanges();
			result = false;
			app.superviseur().setWaitCursor(this, false);
			setVisible(false);
			//hide();

		}
	}

	private void onClose() {
		if (ecCocowork != null && ecCocowork.hasChanges()) {
			ecCocowork.revert();
		}
		actionCancel.actionPerformed(null);
	}

	private void onRemiseGlobaleUpdate() {
		if (eoPrestation == null) {
			return;
		}
		eoPrestation.setPrestRemiseGlobale(tfPrestRemiseGlobale.getBigDecimal());
		prestationLigneTablePanel.updateData();
		prestationLigneORTablePanel.updateData();
		updateTotaux();
	}

	private void onQteUpdate() {
		prestationLigneTablePanel.updateData();
		prestationLigneORTablePanel.reloadData();
		updateTotaux();
	}

	private void updateData() {
		if (eoPrestation == null) {
			return;
		}

		tfPrestNumero.setNumber(eoPrestation.prestNumero());
		tfExercice.setText(eoPrestation.exercice().exeExercice().toString());

		cbTypePublic.removeActionListener(cbActionListener);
		cbTypePublic.setSelectedEOObject(eoPrestation.typePublic());
		cbTypePublic.addActionListener(cbActionListener);

		checkBoxLasm.setSelected("O".equals(eoPrestation.prestApplyTva()));
		if (eoPrestation.typePublic() != null && eoPrestation.typePublic().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(ec))) {
			// TODO activer quand ce sera ok dans Abricot...
			checkBoxLasm.setVisible(false);
		} else {
			checkBoxLasm.setVisible(false);
		}

		if (eoPrestation.personne() != null) {
			StringBuilder txtBuilder = new StringBuilder(eoPrestation.personne().persNomPrenom());
			EOAdresse currentAdr = prestationService.currentAdresseClient(ec, eoPrestation);
			if (currentAdr != null) {
				txtBuilder.append(" ").append(currentAdr.toInlineString());
			}
			tfClient.setText(txtBuilder.toString());
		} else {
			tfClient.setText(null);
		}

		if (eoPrestation.individuUlr() != null) {
			tfContactClient.setText(eoPrestation.individuUlr().personne().persNomPrenom());
		} else {
			tfContactClient.setText(null);
		}

		if (eoPrestation.catalogue() != null) {
			tfCatalogue.setText(eoPrestation.catalogue().cataloguePrestation().catNumero() + " - " + eoPrestation.catalogue().catLibelle());
		} else {
			tfCatalogue.setText(null);
		}

		tfPrestLibelle.setText(eoPrestation.prestLibelle());

		prestationLigneTablePanel.reloadData();
		prestationLigneORTablePanel.reloadData();

		tfCommentairePrest.setText(eoPrestation.prestCommentairePrest());
		tfCommentaireClient.setText(eoPrestation.prestCommentaireClient());
		if (eoPrestation.prestCommentairePrest() != null || eoPrestation.prestCommentaireClient() != null) {
			actionCommentaires.putValue(AbstractAction.SMALL_ICON, Constants.ICON_NOTE2_16);
		} else {
			actionCommentaires.putValue(AbstractAction.SMALL_ICON, Constants.ICON_NOTE_16);
		}

		updateInfosBudgetairesPrestataire();
		prestationBudgetClient.updateData(eoPrestation);

		tfPrestRemiseGlobale.setNumber(eoPrestation.prestRemiseGlobale());
		updateTotaux();

		updateInterfaceEnabling();
	}

	private void updateInfosBudgetairesPrestataire() {
		if (eoPrestation.organ() != null) {
			tfOrgan.setText(eoPrestation.organ().orgLib());
		} else {
			tfOrgan.setText(null);
		}

		if (eoPrestation.typeCreditRec() != null) {
			cbTypeCreditRec.removeActionListener(cbActionListener);
			cbTypeCreditRec.setObjects(FinderTypeCreditRec.find(ec, eoPrestation.exercice()));
			cbTypeCreditRec.setSelectedEOObject(eoPrestation.typeCreditRec());
			cbTypeCreditRec.addActionListener(cbActionListener);
		} else {
			cbTypeCreditRec.setObjects(FinderTypeCreditRec.find(ec, eoPrestation.exercice()));
			cbTypeCreditRec.clean();
		}

		if (eoPrestation.lolfNomenclatureRecette() != null) {
			tfLolfNomenclatureRecette.setText(eoPrestation.lolfNomenclatureRecette().lolfCode() + " - " + eoPrestation.lolfNomenclatureRecette().lolfLibelle());
		} else {
			tfLolfNomenclatureRecette.setText(null);
		}

		if (eoPrestation.convention() != null) {
			tfConvention.setText(eoPrestation.convention().conReferenceExterne());
		} else {
			tfConvention.setText(null);
		}

		cbModeRecouvrement.removeActionListener(cbActionListener);
		if (eoPrestation.modeRecouvrement() != null) {
			if (eoPrestation.typePublic() != null && eoPrestation.typePublic().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(ec))) {
				cbModeRecouvrement.setObjects(FinderModeRecouvrement.find(ec, eoPrestation.exercice()));
			} else {
				cbModeRecouvrement.setObjects(FinderModeRecouvrement.findSansPrestationInterne(ec, eoPrestation.exercice()));
			}
			cbModeRecouvrement.setSelectedEOObject(eoPrestation.modeRecouvrement());
		} else {
			if (eoPrestation.typePublic() != null && eoPrestation.typePublic().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(ec))) {
				cbModeRecouvrement.setObjects(FinderModeRecouvrement.find(ec, eoPrestation.exercice()));
			} else {
				cbModeRecouvrement.setObjects(FinderModeRecouvrement.findSansPrestationInterne(ec, eoPrestation.exercice()));
			}
			cbModeRecouvrement.clean();
		}
		cbModeRecouvrement.addActionListener(cbActionListener);
	}

	private void updateTotaux() {
		tfTotalHt.setNumber(eoPrestation.companion().totalHTLive());
		tfTotalTva.setNumber(eoPrestation.companion().totalTVALive());
		tfTotalTtc.setNumber(eoPrestation.companion().totalTTCLive());

		prestationBudgetClient.updateDataDisponible();
	}

	private void updateInterfaceEnabling() {
		if (eoPrestation == null) {
			return;
		}

		EOUtilisateur loggedUser = app.appUserInfo().utilisateur();
		boolean hasPermissionPrestationsRepro = app.canUseFonction(EOFonction.DROIT_GERER_PRESTATIONS_REPRO, null);
		boolean hasPermissionOrganPrestataire = hasPermissionOrganPrestataire(eoPrestation, loggedUser);
		boolean hasPermissionOrganClient = prestationBudgetClient.hasPermissionOrganClient();
		updateInterfacePrestataire(hasPermissionOrganPrestataire, hasPermissionPrestationsRepro);
		updateInterfaceClient();
		updateDefaultBudgetTab();
		updateValidAction(hasPermissionOrganPrestataire || hasPermissionOrganClient || hasPermissionPrestationsRepro);
		updateCommentaireClient(hasPermissionOrganPrestataire
				|| hasPermissionOrganClient || hasPermissionPrestationsRepro);
		updateCommentairePrestataire(hasPermissionOrganPrestataire);
	}

	private boolean hasPermissionOrganPrestataire(EOPrestation prestation, EOUtilisateur loggedUser) {
		boolean hasPermissionOrganPrestataire = true;
		if (prestation != null && prestation.organ() != null) {
			hasPermissionOrganPrestataire = prestation.organ().hasPermission(loggedUser);
		}
		return hasPermissionOrganPrestataire;
	}

	private void updateCommentaireClient(boolean writeAccess) {
		tfCommentaireClient.setEditable(writeAccess);
	}

	private void updateCommentairePrestataire(boolean writeAccess) {
		tfCommentairePrest.setEditable(writeAccess);
	}

	private void updateInterfacePrestataire(boolean hasFullAccess, boolean hasPermissionReprographie) {
		boolean permissionReproDebloquee = hasFullAccess || hasPermissionReprographie;

		cbTypePublic.setEnabled(hasFullAccess && eoPrestation.personne() == null && eoPrestation.catalogue() == null);

		actionCheckLasm.setEnabled(hasFullAccess && eoPrestation.prestDateValideClient() == null);

		actionClientSelect.setEnabled(
				permissionReproDebloquee
				&& eoPrestation.typePublic() != null
				&& eoPrestation.prestDateValideClient() == null);

		// si personne morale, on autorise un contact client
		actionContactClientSelect.setEnabled(
				permissionReproDebloquee
				&& eoPrestation.personne() != null
				&& eoPrestation.personne().isPersonneMorale());

		actionContactClientDelete.setEnabled(permissionReproDebloquee && eoPrestation.individuUlr() != null);

		actionCatalogueSelect.setEnabled(hasFullAccess
				&& eoPrestation.typePublic() != null
				&& eoPrestation.prestDateValideClient() == null
				&& (eoPrestation.prestationLignes() == null || eoPrestation.prestationLignes().count() == 0));

		tfPrestLibelle.setEditable(permissionReproDebloquee && eoPrestation.prestDateFacturation() == null);
		tfCommentairePrest.setEditable(hasFullAccess);

		prestationLigneTablePanel.setEditable(hasFullAccess
				&& eoPrestation.catalogue() != null
				&& eoPrestation.prestDateValideClient() == null);

		prestationLigneORTablePanel.setEditable(hasFullAccess
				&& eoPrestation.catalogue() != null
				&& eoPrestation.prestDateValideClient() == null);

		actionOrganSelect.setEnabled(hasFullAccess && eoPrestation.prestDateValidePrest() == null);
		actionOrganDelete.setEnabled(hasFullAccess
				&& eoPrestation.organ() != null
				&& eoPrestation.prestDateValidePrest() == null);

		cbTypeCreditRec.setEnabled(hasFullAccess && eoPrestation.prestDateValidePrest() == null);
		cbModeRecouvrement.setEnabled(hasFullAccess
				&& eoPrestation.prestDateValidePrest() == null
				&& !eoPrestation.typePublic().typeApplication().equals(
						FinderTypeApplication.typeApplicationPrestationInterne(ec)));

		actionLolfNomenclatureRecetteSelect.setEnabled(hasFullAccess && eoPrestation.prestDateValidePrest() == null);
		actionLolfNomenclatureRecetteDelete.setEnabled(hasFullAccess
				&& eoPrestation.lolfNomenclatureRecette() != null
				&& eoPrestation.prestDateValidePrest() == null);

		actionConventionSelect.setEnabled(hasFullAccess && eoPrestation.prestDateValidePrest() == null);
		actionConventionOpen.setEnabled(hasFullAccess && eoPrestation.convention() != null);
		actionConventionDelete.setEnabled(hasFullAccess
				&& eoPrestation.convention() != null
				&& eoPrestation.prestDateValidePrest() == null);

		tfPrestRemiseGlobale.setEditable(hasFullAccess
				&& eoPrestation.catalogue() != null
				&& eoPrestation.prestDateValideClient() == null);
	}

	private void updateInterfaceClient() {
		prestationBudgetClient.updateInterfaceEnabling();
	}

	private void updateDefaultBudgetTab() {
		if (isPrestationInterne(eoPrestation)) {
			jtpBudget.setEnabledAt(1, true);
		} else {
			jtpBudget.setEnabledAt(1, false);
			jtpBudget.setSelectedIndex(0);
		}
	}

	private void updateValidAction(boolean permission) {
		this.btValid.setEnabled(permission);
	}

	private boolean isPrestationInterne(EOPrestation prestation) {
		return prestation != null
				&& prestation.typePublic() != null
				&& prestation.typePublic().typeApplication().equals(
						FinderTypeApplication.typeApplicationPrestationInterne(ec));
	}

	private void showRequired(boolean show) {
		cbTypePublic.showRequired(show);
		tfClient.showRequired(show);
		tfCatalogue.showRequired(show);
		tfPrestLibelle.showRequired(show);
		prestationLigneTablePanel.showRequired(show);

		prestationBudgetClient.showRequired(show);
	}

	private void onClientSelect() {
		if (eoPrestation.typePublic() == null) {
			app.showInfoDialog("Choisissez le type de public d'abord !");
			return;
		}
		adresseClientSelected = null;
		if (eoPrestation.typePublic().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(ec))) {
			// on recherche parmi les fournisseurs valides internes uniquement
			if (fournisUlrInterneSelect == null) {
				fournisUlrInterneSelect = new FournisUlrInterneSelect();
			}
			if (fournisUlrInterneSelect.open(app.appUserInfo().utilisateur(), null, null)) {
				eoPrestation.setPersonneRelationship(fournisUlrInterneSelect.getSelected().personne());
				eoPrestation.setFournisUlrRelationship(fournisUlrInterneSelect.getSelected());
			}
		} else {
			if (clientSelect == null) {
				clientSelect = new PrestationAddUpdateClientSelect();
			}
			if (clientSelect.open(app.appUserInfo().utilisateur(), eoPrestation.exercice(), null, true, true, true)) {
				eoPrestation.setPersonneRelationship(clientSelect.getSelected());
				// plus un truc pourri, merci...
				eoPrestation.setFournisUlrRelationship(clientSelect.getSelectedFournisUlr());
				adresseClientSelected = clientSelect.getAdresseSelected();
			}
		}
		if (eoPrestation.personne() != null) {
			StringBuilder clientTextBuilder = new StringBuilder(eoPrestation.personne().persNomPrenom());
			if (adresseClientSelected != null) {
				clientTextBuilder.append(" ").append(adresseClientSelected.toInlineString());
			}
			tfClient.setText(clientTextBuilder.toString());
		}

		// si personne physique, on vire le contact
		if (eoPrestation.personne() != null && !eoPrestation.personne().isPersonneMorale()) {
			eoPrestation.setIndividuUlrRelationship(null);
			tfContactClient.setText(null);
		}

		updateInterfaceEnabling();
	}

	private void onContactClientSelect() {
		if (contactClientSelect == null) {
			contactClientSelect = new ContactSelect();
		}
		if (contactClientSelect.open(app.appUserInfo().utilisateur(), eoPrestation.exercice(), eoPrestation.personne().structureUlr())) {
			eoPrestation.setIndividuUlrRelationship(contactClientSelect.getSelectedIndividuUlr());
			if (eoPrestation.individuUlr() != null) {
				tfContactClient.setText(eoPrestation.individuUlr().personne().persNomPrenom());
			}
			updateInterfaceEnabling();
		}
	}

	private void onContactClientDelete() {
		eoPrestation.setIndividuUlrRelationship(null);
		tfContactClient.setText(null);
		updateInterfaceEnabling();
	}

	private void onCatalogueSelect() {
		if (eoPrestation.prestationLignes() != null && eoPrestation.prestationLignes().count() > 0) {
			app.showInfoDialog("Le panier n'est pas vide, vous ne pouvez pas changer de catalogue !");
			return;
		}
		if (eoPrestation.typePublic() == null) {
			app.showInfoDialog("Choisissez le type de public d'abord !");
			return;
		}
		if (catalogueSelect == null) {
			catalogueSelect = new CatalogueSelect();
		}
		if (catalogueSelect.open(app.appUserInfo().utilisateur(), eoPrestation.typePublic(), null)) {
			eoPrestation.setCatalogueRelationship(catalogueSelect.getSelected());
			if (eoPrestation.catalogue() != null) {
				eoPrestation.setFournisUlrPrestRelationship(eoPrestation.catalogue().fournisUlr());

				tfCatalogue.setText(eoPrestation.catalogue().catLibelle());

				// met a jour la remise globale
				EOCataloguePublic cp = FinderCataloguePublic.find(
						ec, eoPrestation.catalogue(), (EOTypePublic) cbTypePublic.getSelectedEOObject());
				if (cp != null) {
					eoPrestation.setPrestRemiseGlobale(cp.capPourcentage());
				} else {
					eoPrestation.setPrestRemiseGlobale(null);
				}
				tfPrestRemiseGlobale.setNumber(eoPrestation.prestRemiseGlobale());

				// recupere les informations budgetaires par defaut du catalogue
				// si la ligne budgetaire (et le code lolf) n'est pas valide pour l'exercice, on ne l'ajoute pas
				// meme si elle est definie par defaut
				EOQualifier qual = app.getToolsCocktailEOF().getQualifierForPeriodeAndExercice(
						EOOrgan.ORG_DATE_OUVERTURE_KEY, EOOrgan.ORG_DATE_CLOTURE_KEY, eoPrestation.exercice());
				NSArray list = null;
				if (eoPrestation.catalogue().cataloguePrestation().organRecette() != null) {
					list = EOQualifier.filteredArrayWithQualifier(
							new NSArray(eoPrestation.catalogue().cataloguePrestation().organRecette()), qual);
				}
				if (list != null && list.count() != 0) {
					eoPrestation.setOrganRelationship((EOOrgan) list.lastObject());
				} else {
					eoPrestation.setOrganRelationship(null);
				}
				qual = app.getToolsCocktailEOF().getQualifierForPeriodeAndExercice(
						EOLolfNomenclatureRecette.LOLF_OUVERTURE_KEY, EOLolfNomenclatureRecette.LOLF_FERMETURE_KEY,
						eoPrestation.exercice());
				list = null;
				if (eoPrestation.catalogue().cataloguePrestation().organRecette() != null
						&& eoPrestation.catalogue().cataloguePrestation().lolfNomenclatureRecette() != null) {
					list = EOQualifier.filteredArrayWithQualifier(new NSArray(
							eoPrestation.catalogue().cataloguePrestation().lolfNomenclatureRecette()), qual);
				}
				if (list != null && list.count() != 0) {
					eoPrestation.setLolfNomenclatureRecetteRelationship(
							eoPrestation.catalogue().cataloguePrestation().lolfNomenclatureRecette());
				} else {
					eoPrestation.setLolfNomenclatureRecetteRelationship(null);
				}
				updateInfosBudgetairesPrestataire();
			} else {
				eoPrestation.setFournisUlrPrestRelationship(null);
			}
			updateInterfaceEnabling();
		}
	}

	private void onOrganSelect() {
		if (organSelect == null) {
			organSelect = new OrganSelect();
		}
		if (organSelect.open(app.appUserInfo().utilisateur(), eoPrestation.exercice(), null, null)) {
			eoPrestation.setOrganRelationship(organSelect.getSelected());
			if (eoPrestation.organ() != null) {
				tfOrgan.setText(eoPrestation.organ().orgLib());
				// si on controle la destination par rapport a l'organ et au
				// type cred, on verifie que s'il y en a un de selectionnee deja,
				// il est
				// toujours autorise, sinon on le vire
				if (app.getParamBoolean(EOParametres.PARAM_CTRL_ORGAN_DEST, eoPrestation.exercice())) {
					if (eoPrestation.typeCreditRec() != null && eoPrestation.lolfNomenclatureRecette() != null) {
						NSArray a = FinderLolfNomenclatureRecette.find(ec, eoPrestation.exercice(), eoPrestation.organ(), eoPrestation.typeCreditRec());
						if (!a.containsObject(eoPrestation.lolfNomenclatureRecette())) {
							eoPrestation.setLolfNomenclatureRecetteRelationship(null);
							tfLolfNomenclatureRecette.setText(null);
						}
					}
				}
			}
			updateInterfaceEnabling();
		}
	}

	private void onOrganDelete() {
		eoPrestation.setOrganRelationship(null);
		tfOrgan.setText(null);
		updateInterfaceEnabling();
	}

	private void onLolfNomenclatureRecetteSelect() {
		if (lolfNomenclatureRecetteSelect == null) {
			lolfNomenclatureRecetteSelect = new LolfNomenclatureRecetteSelect();
		}
		if (lolfNomenclatureRecetteSelect.open(app.appUserInfo().utilisateur(), eoPrestation.exercice(), eoPrestation.organ(), eoPrestation.typeCreditRec(), eoPrestation.lolfNomenclatureRecette())) {
			eoPrestation.setLolfNomenclatureRecetteRelationship(lolfNomenclatureRecetteSelect.getSelected());
			if (eoPrestation.lolfNomenclatureRecette() != null) {
				tfLolfNomenclatureRecette.setText(eoPrestation.lolfNomenclatureRecette().lolfCode() + "." + eoPrestation.lolfNomenclatureRecette().lolfLibelle());
			}
			updateInterfaceEnabling();
		}
	}

	private void onLolfNomenclatureRecetteDelete() {
		eoPrestation.setLolfNomenclatureRecetteRelationship(null);
		tfLolfNomenclatureRecette.setText(null);
		updateInterfaceEnabling();
	}

	private void onConventionSelect() {
		if (eoPrestation.convention() != null) {
			JOptionPane.showMessageDialog(this, "Vous devez d'abord \"d\u00E9tacher\" la convention existante \n" + "avant de pouvoir en s\u00E9lectionner une autre.");
			return;
		}

		if (conventionSelect == null) {
			conventionSelect = new ConventionSelect();
		}

		// si formation continue : possibilite de creer une convention de formation a la volee
		if (_typeFC.equals(eoPrestation.typePublic())) {

			if (eoPrestation.catalogue() == null) {
				JOptionPane.showMessageDialog(this, "Vous devez choisir un catalogue.");
				return;
			}
			if (eoPrestation.prestationLignes() == null || eoPrestation.prestationLignes().count() < 1) {
				JOptionPane.showMessageDialog(this, "Vous devez choisir un article.");
				return;
			}
			if (eoPrestation.personne() == null) {
				JOptionPane.showMessageDialog(this, "Vous devez choisir un client.");
				return;
			}

			try {
				// determination des parametres de creation d'une convention
				conventionChargerFacadeConvention();
				// passe ces proprietes au controller ou l'on peut demander la creation de la convention
				conventionSelect.setFacadeEditionConventionFormation(getFacadeEditionConventionFormation());
			} catch (Exception e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(
						this,
						"Une erreur s'est produite, qui vous emp\u00E9chera de cr\u00E9er une convention \n" +
								"de formation depuis PIE.",
						"Avertissement",
						JOptionPane.ERROR_MESSAGE);
			}
		}
		else {
			conventionSelect.setFacadeEditionConventionFormation(null);
		}

		if (conventionSelect.openRec(app.appUserInfo().utilisateur(), eoPrestation.exercice(), eoPrestation.organ(), eoPrestation.typeCreditRec(), eoPrestation.convention(), eoPrestation.typePublic())) {

			eoPrestation.setConventionRelationship(conventionSelect.getSelected());
			if (eoPrestation.convention() != null) {
				tfConvention.setText(eoPrestation.convention().conReferenceExterne());
			}
			updateInterfaceEnabling();
		}
	}

	private void onConventionOpen() {

		// affichage des generalites de la convention associee
		if (eoPrestation.convention() != null) {

			conventionAfficher();
		}
		updateInterfaceEnabling();
	}

	/**
	 * Demande d'affichage de la fenetre d'edition de la convention selectionnee.
	 */
	protected void conventionAfficher() {

		ProxyPrestationConventionFormation conventionProxy = new ProxyPrestationConventionFormation(
				getFacadeEditionConventionFormation(), this, Boolean.TRUE);
		try {
			conventionProxy.afficherConvention(eoPrestation.convention(), app.appUserInfo().utilisateur(), eoPrestation.personne(), eoPrestation, eoPrestation.organ(), true);
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "Impossible d'ouvrir la convention : \n\n" + e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
		}
	}

	public FacadeEditionConventionFormation getFacadeEditionConventionFormation() {
		if (facadeEditionConventionFormation == null)
			facadeEditionConventionFormation = new FacadeEditionConventionFormation(ecCocowork, Boolean.TRUE);

		return this.facadeEditionConventionFormation;
	}

	public void setFacadeEditionConventionFormation(FacadeEditionConventionFormation facadeEditionConventionFormation) {
		this.facadeEditionConventionFormation = facadeEditionConventionFormation;
	}

	private void onConventionDelete() {
		try {
			// permet d'annuler la creation eventuelle de la convention a la volee
			conventionAnnulerCreation();
		} catch (Exception e) {
			e.printStackTrace();
		}

		eoPrestation.setConventionRelationship(null);
		tfConvention.setText(null);
		updateInterfaceEnabling();
	}

	private void onCommentaires() {
		dialogCommentaires.setLocationRelativeTo(this);
		dialogCommentaires.show();
	}

	private void onTypePublicUpdate(EOTypePublic typePublic) {
		if (eoPrestation != null) {
			eoPrestation.setTypePublicRelationship(typePublic);
			if (typePublic != null && typePublic.typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(ec))) {
				cbModeRecouvrement.setObjects(FinderModeRecouvrement.find(ec, eoPrestation.exercice()));
				EOModeRecouvrement modeRecouvrement = FinderModeRecouvrement.modeRecouvrementPrestationInterne(eoPrestation.exercice());
				cbModeRecouvrement.setSelectedEOObject(modeRecouvrement);
				eoPrestation.setModeRecouvrementRelationship(modeRecouvrement);
				// autorise la lasm...
				// TODO activer quand ce sera ok dans Abricot...
				checkBoxLasm.setVisible(false);
				// on initialise a non par defaut
				checkBoxLasm.setSelected(false);
				eoPrestation.setPrestApplyTva("N");
			}
			else {
				cbModeRecouvrement.setObjects(FinderModeRecouvrement.findSansPrestationInterne(ec, eoPrestation.exercice()));
				cbModeRecouvrement.setSelectedEOObject(eoPrestation.modeRecouvrement());
				// interdit la lasm...
				checkBoxLasm.setVisible(false);
				// on initialise a oui toujours en non interne
				checkBoxLasm.setSelected(true);
				eoPrestation.setPrestApplyTva("O");
			}
			prestationLigneTablePanel.reloadData();
			prestationLigneORTablePanel.reloadData();
			updateTotaux();
		}
		updateInterfaceEnabling();
	}

	private class PrestationLigneTablePanel extends ZExtendedTablePanel {

		private ZEOTableModelColumn colQte;

		public PrestationLigneTablePanel() {
			super("Articles", true, false, true);

			final ZEOTableModelColumn.ZEONumFieldTableCellEditor _editor = new ZEOTableModelColumn.ZEONumFieldTableCellEditor(new JTextField(), Constants.FORMAT_DECIMAL_EDIT);

			addCol(new ZEOTableModelColumn(getDG(), EOPrestationLigne.PRLIG_REFERENCE_KEY, "Ref", 60));

			addCol(new ZEOTableModelColumn(getDG(), EOPrestationLigne.PRLIG_DESCRIPTION_KEY, "Description", 150));

			ZEOTableModelColumn col3 = new ZEOTableModelColumn(getDG(), EOPrestationLigne.PRLIG_ART_HT_KEY, "HT", 50, app.getCurrencyPrecisionFormatDisplay());
			col3.setColumnClass(BigDecimal.class);
			col3.setAlignment(SwingConstants.RIGHT);
			addCol(col3);

			ZEOTableModelColumn col4 = new ZEOTableModelColumn(getDG(), EOPrestationLigne.TVA_KEY + "." + EOTva.TVA_TAUX_KEY, "TVA", 50, Constants.FORMAT_DECIMAL_DISPLAY);
			col4.setColumnClass(BigDecimal.class);
			col4.setAlignment(SwingConstants.RIGHT);
			addCol(col4);

			ZEOTableModelColumn col5 = new ZEOTableModelColumn(getDG(), EOPrestationLigne.PRLIG_ART_TTC_KEY, "TTC", 50, app.getCurrencyPrecisionFormatDisplay());
			col5.setColumnClass(BigDecimal.class);
			col5.setAlignment(SwingConstants.RIGHT);
			addCol(col5);

			colQte = new ZEOTableModelColumn(getDG(), EOPrestationLigne.PRLIG_QUANTITE_KEY, "Qt\u00E9", 60, Constants.FORMAT_DECIMAL_DISPLAY);
			colQte.setEditable(true);
			colQte.setTableCellEditor(_editor);
			colQte.setFormatEdit(Constants.FORMAT_DECIMAL_EDIT);
			colQte.setColumnClass(BigDecimal.class);
			colQte.setMyModifier(new QteModifier());
			colQte.setAlignment(SwingConstants.RIGHT);
			addCol(colQte);

			ZEOTableModelColumn col7 = new ZEOTableModelColumn(getDG(), EOPrestationLigne.PRLIG_TOTAL_TTC_KEY, "Total TTC", 70, app.getCurrencyPrecisionFormatDisplay());
			col7.setColumnClass(BigDecimal.class);
			col7.setAlignment(SwingConstants.RIGHT);
			addCol(col7);

			ZEOTableModelColumn col8 = new ZEOTableModelColumn(getDG(), EOPrestationLigne.PRLIG_QUANTITE_RESTE_KEY, "Qt\u00E9 reste \u00E0 facturer", 80, Constants.FORMAT_DECIMAL_DISPLAY);
			col8.setColumnClass(BigDecimal.class);
			col8.setAlignment(SwingConstants.RIGHT);
			addCol(col8);

			ZEOTableModelColumn col9 = new ZEOTableModelColumn(getDG(), EOPrestationLigne.PRLIG_TOTAL_RESTE_TTC_KEY, "Total reste TTC", 70, app.getCurrencyPrecisionFormatDisplay());
			col9.setColumnClass(BigDecimal.class);
			col9.setAlignment(SwingConstants.RIGHT);
			addCol(col9);

			initGUI();

			setPreferredSize(new Dimension(0, 120));

			// pour n'afficher que les articles ici
			setFilteringQualifier(EOQualifier.qualifierWithQualifierFormat(EOPrestationLigne.TYPE_ARTICLE_KEY + "=%@", new NSArray(FinderTypeArticle.typeArticleArticle(ec))));
		}

		public void reloadData() {
			if (eoPrestation != null) {
				setObjectArray(eoPrestation.prestationLignes());
			}
			else {
				setObjectArray(null);
			}
			updateInterfaceEnabling();
		}

		protected void onAdd() {
			if (eoPrestation == null) {
				return;
			}
			if (eoPrestation.catalogue() == null) {
				app.showInfoDialog("Choisissez un catalogue d'abord !");
				return;
			}
			if (catalogueArticleSelect == null) {
				catalogueArticleSelect = new CatalogueArticleSelect();
			}

			if (afficheLaListeDesArticlesSelectionnable()) {
				NSArray articles = catalogueArticleSelect.getSelecteds();
				if (isArticlesSelectionnes(articles)) {
					ajouteLignesPrestation(articles);

					reloadData();
					updateTotaux();
				}
			}
		}

		private boolean afficheLaListeDesArticlesSelectionnable() {
			return catalogueArticleSelect.open(
					eoPrestation.utilisateur(), eoPrestation.catalogue(), eoPrestation.typePublic(), null);
		}

		private boolean isArticlesSelectionnes(NSArray articles) {
			return catalogueArticleSelect.getSelecteds() != null && catalogueArticleSelect.getSelecteds().count() > 0;
		}

		private void ajouteLignesPrestation(NSArray articles) {
			for (int idxArticle = 0; idxArticle < articles.count(); idxArticle++) {
				EOCatalogueArticle article = (EOCatalogueArticle) articles.objectAtIndex(idxArticle);
				if (!isArticlePresentDansPrestation(article)) {
					EOPrestationLigne ligne = ajouterUneLignePrestation(article);
					changeLibellePrestation(ligne);
				}
			}
		}

		private void changeLibellePrestation(EOPrestationLigne ligne) {
			if (isVideOuNull(tfPrestLibelle.getText())) {
				tfPrestLibelle.setText(ligne.prligDescription());
			}
		}

		public boolean isVideOuNull(String chaine) {
			return chaine == null || "".equals(chaine);
		}

		private EOPrestationLigne ajouterUneLignePrestation(EOCatalogueArticle article) {
			EOPrestationLigne ligne = FactoryPrestationLigne.newObject(ec, eoPrestation, article);
			ligne.setPrligQuantite(new BigDecimal(1.0));
			ligne.setPrligQuantiteReste(new BigDecimal(1.0));
			return ligne;
		}

		private boolean isArticlePresentDansPrestation(EOCatalogueArticle article) {
			return ((NSArray) eoPrestation.prestationLignes().valueForKey(EOPrestationLigne.CATALOGUE_ARTICLE_KEY))
					.containsObject(article);
		}

		protected void onUpdate() {
		}

		protected void onDelete() {
			if (selectedObject() != null) {
				EOPrestationLigne ligneArticle = (EOPrestationLigne) selectedObject();
				if (ligneArticle.prestationLignes() != null) {
					while (ligneArticle.prestationLignes().count() > 0) {
						EOPrestationLigne optionRemiseAssociee =
								(EOPrestationLigne) ligneArticle.prestationLignes().objectAtIndex(0);
						FactoryPrestationLigne.removeObject(ec, optionRemiseAssociee, ligneArticle);
					}
				}
				eoPrestation.removeFromPrestationLignesRelationship(ligneArticle);
				ec.deleteObject(ligneArticle);
				reloadData();
				updateTotaux();
			}
		}

		private boolean supprimerLigne(EOPrestation prestation, EOPrestationLigne lignePrestation) {
			boolean isDeleted = false;
			if (prestation == null
					|| prestation.prestationLignes() == null
					|| prestation.prestationLignes().count() == 0) {
				return isDeleted;
			}

			// verifie si la ligne prestation appartient a cette prestation
			if (prestation.prestationLignes().containsObject(lignePrestation)) {
				prestation.removeFromPrestationLignesRelationship(lignePrestation);
				isDeleted = true;
			}
			return isDeleted;
		}

		protected void onSelectionChanged() {
			prestationLigneORTablePanel.reloadData();
		}

		public void setEditable(boolean editable) {
			colQte.setEditable(editable);
			super.setEditable(editable);
		}

		private class QteModifier implements ZEOTableModelColumn.Modifier {
			public void setValueAtRow(Object value, int row) {
				((NSKeyValueCoding) (displayedObjects().objectAtIndex(row))).takeValueForKey(value, colQte.getAttributeName());
				onQteUpdate();
			}
		}
	}

	private class PrestationLigneORTablePanel extends ZExtendedTablePanel {

		final ZEOTableModelColumn.ZEONumFieldTableCellEditor _editor = new ZEOTableModelColumn.ZEONumFieldTableCellEditor(new JTextField(), Constants.FORMAT_DECIMAL_EDIT);

		ZEOTableModelColumn colPrligQuantite;

		public PrestationLigneORTablePanel() {

			super("Options / Remises", true, false, true);

			addCol(new ZEOTableModelColumn(getDG(), EOPrestationLigne.TYPE_ARTICLE_KEY + "." + EOTypeArticle.TYAR_LIBELLE_KEY, "Type", 80));

			addCol(new ZEOTableModelColumn(getDG(), EOPrestationLigne.PRLIG_REFERENCE_KEY, "Ref", 80));

			addCol(new ZEOTableModelColumn(getDG(), EOPrestationLigne.PRLIG_DESCRIPTION_KEY, "Description", 200));

			ZEOTableModelColumn col4 = new ZEOTableModelColumn(getDG(), EOPrestationLigne.PRLIG_ART_HT_KEY, "HT", 50, app.getCurrencyPrecisionFormatDisplay());
			col4.setColumnClass(BigDecimal.class);
			col4.setAlignment(SwingConstants.RIGHT);
			addCol(col4);

			ZEOTableModelColumn col5 = new ZEOTableModelColumn(getDG(), EOPrestationLigne.TVA_KEY + "." + EOTva.TVA_TAUX_KEY, "TVA", 50, Constants.FORMAT_DECIMAL_DISPLAY);
			col5.setColumnClass(BigDecimal.class);
			col5.setAlignment(SwingConstants.RIGHT);
			addCol(col5);

			ZEOTableModelColumn col6 = new ZEOTableModelColumn(getDG(), EOPrestationLigne.PRLIG_ART_TTC_KEY, "TTC", 50, app.getCurrencyPrecisionFormatDisplay());
			col6.setColumnClass(BigDecimal.class);
			col6.setAlignment(SwingConstants.RIGHT);
			addCol(col6);

			colPrligQuantite = new ZEOTableModelColumn(getDG(), EOPrestationLigne.PRLIG_QUANTITE_KEY, "Qt\u00E9", 60, Constants.FORMAT_DECIMAL_DISPLAY);
			colPrligQuantite.setEditable(true);
			colPrligQuantite.setTableCellEditor(_editor);
			colPrligQuantite.setFormatEdit(Constants.FORMAT_DECIMAL_EDIT);
			colPrligQuantite.setColumnClass(BigDecimal.class);
			colPrligQuantite.setMyModifier(new QteModifier());
			colPrligQuantite.setAlignment(SwingConstants.RIGHT);
			addCol(colPrligQuantite);

			ZEOTableModelColumn col8 = new ZEOTableModelColumn(getDG(), EOPrestationLigne.PRLIG_TOTAL_TTC_KEY, "Total TTC", 60, app.getCurrencyPrecisionFormatDisplay());
			col8.setColumnClass(BigDecimal.class);
			col8.setAlignment(SwingConstants.RIGHT);
			addCol(col8);

			initGUI();

			setPreferredSize(new Dimension(0, 70));
		}

		public void setEditable(boolean editable) {
			super.setEditable(editable);
			colPrligQuantite.setEditable(editable);
		}

		public void reloadData() {
			if (getPrestationLigneSelected() != null) {
				setEditable(true);
				setObjectArray(getPrestationLigneSelected().prestationLignes());
			} else {
				setEditable(false);
				setObjectArray(null);
			}
			updateInterfaceEnabling();
		}

		protected void onAdd() {
			if (prestationLigneTablePanel.selectedObject() == null) {
				return;
			}
			if (catalogueArticleSelect == null) {
				catalogueArticleSelect = new CatalogueArticleSelect();
			}
			if (afficheLaListeDesOptionsRemisesSelectionnables()) {
				if (isOptionsRemisesSelectionnees()) {
					NSArray optionsRemises = catalogueArticleSelect.getSelecteds();
					for (int i = 0; i < optionsRemises.count(); i++) {
						EOCatalogueArticle caar = (EOCatalogueArticle) optionsRemises.objectAtIndex(i);
						if (!isOptionRemisePresenteDansPrestation(caar)) {
							FactoryPrestationLigne.newObject(ec, getPrestationLigneSelected(), caar);
						}
					}
					reloadData();
					updateTotaux();
				}
			}
		}

		private boolean isOptionRemisePresenteDansPrestation(EOCatalogueArticle caar) {
			return ((NSArray) getPrestationLigneSelected().prestationLignes().valueForKey(
					EOPrestationLigne.CATALOGUE_ARTICLE_KEY)).containsObject(caar);
		}

		private boolean isOptionsRemisesSelectionnees() {
			return catalogueArticleSelect.getSelecteds() != null && catalogueArticleSelect.getSelecteds().count() > 0;
		}

		private boolean afficheLaListeDesOptionsRemisesSelectionnables() {
			return catalogueArticleSelect.open(
					eoPrestation.utilisateur(),
					((EOPrestationLigne) prestationLigneTablePanel.selectedObject()).catalogueArticle(),
					eoPrestation.typePublic(),
					FinderTypeArticle.typeOptionEtRemise(ec),
					null);
		}

		protected void onUpdate() {
		}

		protected void onDelete() {
			if (selectedObject() != null) {
				EOPrestationLigne object = (EOPrestationLigne) selectedObject();
				//on supprime correctement la prestation lignes de la prestation ligne pere ET de la prestation !!!
				FactoryPrestationLigne.removeObject(ec, object, getPrestationLigneSelected());
				reloadData();
				updateTotaux();
			}
			updateInterfaceEnabling();
		}

		private EOPrestationLigne getPrestationLigneSelected() {
			return (EOPrestationLigne) prestationLigneTablePanel.selectedObject();
		}

		protected void onSelectionChanged() {
		}

		private class QteModifier implements ZEOTableModelColumn.Modifier {
			public void setValueAtRow(Object value, int row) {
				((NSKeyValueCoding) (displayedObjects().objectAtIndex(row))).takeValueForKey(value, colPrligQuantite.getAttributeName());
				onQteUpdate();
			}
		}
	}

	private void initGUI() {
		JPanel contentPanel = new JPanel(new BorderLayout());
		contentPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanel);

		// info
		labelInfo.setBackground(Color.white);
		labelInfo.setOpaque(true);
		labelInfo.setVisible(false);

		// numero
		JPanel panelNumero = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel jlNumero = new JLabel("Prestation N\u00B0 ");
		jlNumero.setFont(jlNumero.getFont().deriveFont(Font.BOLD, 20));
		jlNumero.setForeground(Constants.COLOR_LABEL_FGD_NORMAL);
		panelNumero.add(jlNumero);
		tfPrestNumero.setFont(tfPrestNumero.getFont().deriveFont(Font.BOLD, 24));
		tfPrestNumero.setHorizontalAlignment(SwingConstants.RIGHT);
		tfPrestNumero.setViewOnly();
		panelNumero.add(tfPrestNumero);
		panelNumero.add(labelInfo);

		JPanel tmpPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JLabel jlExercice = new JLabel("Exercice ");
		jlExercice.setFont(jlExercice.getFont().deriveFont(Font.BOLD, 16));
		jlExercice.setForeground(Constants.COLOR_LABEL_FGD_NORMAL);
		tmpPanel.add(jlExercice);
		tfExercice.setFont(tfExercice.getFont().deriveFont(Font.BOLD, 18));
		tfExercice.setHorizontalAlignment(SwingConstants.CENTER);
		tfExercice.setViewOnly();
		tmpPanel.add(tfExercice);
		JPanel panelExercice = new JPanel(new GridBagLayout());
		panelExercice.add(tmpPanel, new GridBagConstraints());

		JPanel panelNumeroExercice = new JPanel(new BorderLayout());
		panelNumeroExercice.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		// panelRecNumeroExercice.setBorder(BorderFactory.createMatteBorder(0,
		// 0, 1, 0, Color.gray));
		panelNumeroExercice.add(panelNumero, BorderLayout.LINE_START);
		panelNumeroExercice.add(new JPanel(), BorderLayout.CENTER);
		panelNumeroExercice.add(panelExercice, BorderLayout.LINE_END);

		// type client
		JPanel panelTypePublic = new JPanel(new FlowLayout(FlowLayout.LEFT));
		panelTypePublic.setBorder(BorderFactory.createTitledBorder(null, "Type de client", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, Constants.COLOR_LABEL_FGD_LIGHT));
		panelTypePublic.add(UIUtilities.labeledComponent(null, cbTypePublic, null));

		// client
		Box panelClient = Box.createVerticalBox();
		panelClient.setBorder(BorderFactory.createTitledBorder(null, "Client", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, Constants.COLOR_LABEL_FGD_LIGHT));
		panelClient.add(UIUtilities.labeledComponent(null, tfClient, actionClientSelect));
		panelClient.add(UIUtilities.labeledComponent("Contact client (facultatif)", tfContactClient, new Action[] {
				actionContactClientSelect, actionContactClientDelete
		}, UIUtilities.DEFAULT_BOXED, UIUtilities.DEFAULT_ALIGNMENT));

		Box panelTypePublicClient = Box.createHorizontalBox();
		panelTypePublicClient.setBorder(BorderFactory.createEmptyBorder());
		panelTypePublicClient.add(panelTypePublic);
		panelTypePublicClient.add(panelClient);

		// catalogue
		JPanel panelCatalogueLasm = new JPanel(new FlowLayout(FlowLayout.LEFT));
		panelCatalogueLasm.setBorder(BorderFactory.createEmptyBorder());

		JPanel panelCatalogue = UIUtilities.labeledComponent("Catalogue", tfCatalogue, actionCatalogueSelect);
		panelCatalogue.setBorder(BorderFactory.createEmptyBorder());
		panelCatalogueLasm.add(panelCatalogue);
		panelCatalogueLasm.add(Box.createHorizontalStrut(50));
		checkBoxLasm.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);
		checkBoxLasm.setVerticalTextPosition(SwingConstants.TOP);
		checkBoxLasm.setHorizontalTextPosition(SwingConstants.CENTER);
		checkBoxLasm.setIconTextGap(0);
		checkBoxLasm.setFocusPainted(false);
		checkBoxLasm.setBorderPaintedFlat(true);
		panelCatalogueLasm.add(UIUtilities.labeledComponent(null, checkBoxLasm, null));

		// libell\u00E9
		JPanel panelLibelle = UIUtilities.labeledComponent("Libell\u00E9", tfPrestLibelle, null);
		panelLibelle.setBorder(BorderFactory.createEmptyBorder());

		// panier
		Box panelPanier = Box.createVerticalBox();
		panelPanier.setBorder(BorderFactory.createTitledBorder(null, "Panier", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, Constants.COLOR_LABEL_FGD_LIGHT));
		panelPanier.add(prestationLigneTablePanel);
		panelPanier.add(prestationLigneORTablePanel);

		// budget prestataire
		JPanel panelOrganTypCredTap = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panelOrganTypCredTap.add(UIUtilities.labeledComponent("Ligne budg\u00E9taire", tfOrgan, new Action[] {
				actionOrganSelect, actionOrganDelete
		}, UIUtilities.DEFAULT_BOXED, UIUtilities.DEFAULT_ALIGNMENT));
		panelOrganTypCredTap.add(Box.createHorizontalStrut(10));
		panelOrganTypCredTap.add(UIUtilities.labeledComponent("Type de cr\u00E9dit", cbTypeCreditRec, null));

		JPanel panelModeRecouvrementConvention = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panelModeRecouvrementConvention.add(UIUtilities.labeledComponent("Mode de recouvrement", cbModeRecouvrement, null));
		panelModeRecouvrementConvention.add(Box.createHorizontalStrut(10));
		panelModeRecouvrementConvention.add(UIUtilities.labeledComponent("Convention", tfConvention, new Action[] {
				actionConventionSelect, actionConventionOpen, actionConventionDelete
		}, UIUtilities.DEFAULT_BOXED, UIUtilities.DEFAULT_ALIGNMENT));

		Box panelBudgetairePrest = Box.createVerticalBox();
		panelBudgetairePrest.setBorder(BorderFactory.createEmptyBorder());
		panelBudgetairePrest.add(panelOrganTypCredTap);
		panelBudgetairePrest.add(UIUtilities.labeledComponent("Action Lolf", tfLolfNomenclatureRecette, new Action[] {
				actionLolfNomenclatureRecetteSelect, actionLolfNomenclatureRecetteDelete
		}, UIUtilities.DEFAULT_BOXED, UIUtilities.DEFAULT_ALIGNMENT));
		panelBudgetairePrest.add(panelModeRecouvrementConvention);

		jtpBudget = new JTabbedPane();
		jtpBudget.setBorder(BorderFactory.createTitledBorder(null, "Informations budg\u00E9taires", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, Constants.COLOR_LABEL_FGD_LIGHT));
		jtpBudget.add("Prestataire", panelBudgetairePrest);
		jtpBudget.add("Client interne", prestationBudgetClient.getCenterPanel());

		// totaux
		JPanel panelRemise = new JPanel(new FlowLayout(FlowLayout.LEFT));
		panelRemise.setBorder(BorderFactory.createTitledBorder(null, "Remise globale", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, Constants.COLOR_LABEL_FGD_LIGHT));
		panelRemise.add(UIUtilities.labeledComponent("%", tfPrestRemiseGlobale, null));

		JPanel tempPanel = new JPanel(new GridLayout(3, 2, 5, 5));
		tempPanel.setBorder(BorderFactory.createEmptyBorder());
		JLabel label2 = new JLabel("Total HT");
		label2.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);
		tempPanel.add(label2);
		tempPanel.add(UIUtilities.labeledComponent(null, tfTotalHt, null));
		JLabel label3 = new JLabel("Total TVA");
		label3.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);
		tempPanel.add(label3);
		tempPanel.add(UIUtilities.labeledComponent(null, tfTotalTva, null));
		JLabel label4 = new JLabel("Total TTC");
		label4.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);
		tempPanel.add(label4);
		tempPanel.add(UIUtilities.labeledComponent(null, tfTotalTtc, null));

		JPanel panelTotaux = new JPanel(new BorderLayout());
		panelTotaux.setBorder(BorderFactory.createTitledBorder(null, "Totaux", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, Constants.COLOR_LABEL_FGD_LIGHT));
		panelTotaux.add(new JPanel(), BorderLayout.CENTER);
		panelTotaux.add(tempPanel, BorderLayout.EAST);

		Box panelRemiseTotaux = Box.createVerticalBox();
		panelRemiseTotaux.setBorder(BorderFactory.createEmptyBorder());
		panelRemiseTotaux.add(panelRemise);
		panelRemiseTotaux.add(panelTotaux);

		Box panelBudgetTotaux = Box.createHorizontalBox();
		panelBudgetTotaux.setBorder(BorderFactory.createEmptyBorder());
		panelBudgetTotaux.add(jtpBudget);
		panelBudgetTotaux.add(panelRemiseTotaux);

		Box top = Box.createVerticalBox();
		top.setBorder(BorderFactory.createEmptyBorder());
		// top.add(panelNumeroExercice);
		top.add(panelTypePublicClient);
		top.add(panelCatalogueLasm);
		top.add(panelLibelle);

		Box bottom = Box.createVerticalBox();
		bottom.setBorder(BorderFactory.createEmptyBorder());
		bottom.add(panelBudgetTotaux);

		JPanel tempCenter = new JPanel(new BorderLayout());
		tempCenter.setBorder(BorderFactory.createEmptyBorder());
		tempCenter.add(top, BorderLayout.NORTH);
		tempCenter.add(panelPanier, BorderLayout.CENTER);
		tempCenter.add(bottom, BorderLayout.SOUTH);

		JPanel center = new JPanel(new BorderLayout());
		center.setBorder(BorderFactory.createEmptyBorder());
		center.add(tempCenter, BorderLayout.CENTER);
		center.add(buildToolBar(), BorderLayout.LINE_END);

		getContentPane().add(panelNumeroExercice, BorderLayout.NORTH);
		getContentPane().add(center, BorderLayout.CENTER);
		getContentPane().add(buildBottomPanel(), BorderLayout.SOUTH);

		// panneau de commentaires
		Box panelCommentaires = Box.createVerticalBox();
		panelCommentaires.add(UIUtilities.labeledComponent("Commentaire client", new JScrollPane(tfCommentaireClient), null, true));
		panelCommentaires.add(UIUtilities.labeledComponent("Commentaire prestataire (imprim\u00E9 en bas du devis)", new JScrollPane(tfCommentairePrest), null, true));
		dialogCommentaires.setContentPane(panelCommentaires);
		dialogCommentaires.setResizable(false);
		dialogCommentaires.validate();
		dialogCommentaires.pack();

		setGlassPane(new DisabledGlassPane(getContentPane(), btCancel));
		initInputMap();

		this.validate();
		this.pack();
	}

	private final JPanel buildBottomPanel() {
		JPanel p = new JPanel();
		p.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.add(Box.createHorizontalGlue());
		p.add(btCancel);
		p.add(Box.createRigidArea(new Dimension(5, 0)));
		p.add(btValid);
		return p;
	}

	private final JComponent buildToolBar() {
		JToolBar toolBarPanel = new JToolBar("Prestation Toolbar", JToolBar.VERTICAL);
		toolBarPanel.setBorder(BorderFactory.createEmptyBorder(15, 4, 4, 4));
		toolBarPanel.add(makeButton(actionCommentaires));
		// toolBarPanel.addSeparator(new Dimension(20, 20));
		return toolBarPanel;
	}

	private JButton makeButton(Action a) {
		Dimension d = new Dimension(24, 24);
		JButton bt = new JButton(a);
		bt.setPreferredSize(d);
		bt.setMaximumSize(d);
		bt.setMinimumSize(d);
		bt.setHorizontalAlignment(SwingConstants.CENTER);
		bt.setBorderPainted(true);
		bt.setFocusPainted(false);
		return bt;
	}

	private void initInputMap() {
		((JComponent) getContentPane()).getActionMap().put("CTRL_Q", app.superviseur().mainMenu.actionQuit);
		((JComponent) getContentPane()).getActionMap().put("CTRL_S", actionValid);
		((JComponent) getContentPane()).getActionMap().put("F10", actionValid);
		((JComponent) getContentPane()).getActionMap().put("ESCAPE", actionCancel);
		((JComponent) getContentPane()).getActionMap().put("F8", actionShowRequired);
		((JComponent) getContentPane()).getActionMap().put("ALT_F8", actionUnshowRequired);
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK), "CTRL_Q");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK), "CTRL_S");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F10, 0), "F10");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "ESCAPE");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("F8"), "F8");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("released F8"), "ALT_F8");
	}

	private class ActionValid extends AbstractAction {
		public ActionValid() {
			super("Enregistrer");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onValid();
			}
		}
	}

	private class ActionCancel extends AbstractAction {
		public ActionCancel() {
			super("Annuler");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CANCEL_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onCancel();
			}
		}
	}

	private class ActionClose extends AbstractAction {
		public ActionClose() {
			super("Fermer");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CLOSE_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onClose();
			}
		}
	}

	private class ActionShowRequired extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				showRequired(true);
			}
		}
	}

	private class ActionUnshowRequired extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				showRequired(false);
			}
		}
	}

	private class ActionCatalogueSelect extends AbstractAction {
		public ActionCatalogueSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onCatalogueSelect();
			}
		}
	}

	private class ActionClientSelect extends AbstractAction {
		public ActionClientSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onClientSelect();
			}
		}
	}

	private class ActionContactClientSelect extends AbstractAction {
		public ActionContactClientSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onContactClientSelect();
			}
		}
	}

	private class ActionContactClientDelete extends AbstractAction {
		public ActionContactClientDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onContactClientDelete();
			}
		}
	}

	private class ActionOrganSelect extends AbstractAction {
		public ActionOrganSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onOrganSelect();
			}
		}
	}

	private class ActionOrganDelete extends AbstractAction {
		public ActionOrganDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onOrganDelete();
			}
		}
	}

	private class ActionLolfNomenclatureRecetteSelect extends AbstractAction {
		public ActionLolfNomenclatureRecetteSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onLolfNomenclatureRecetteSelect();
			}
		}
	}

	private class ActionLolfNomenclatureRecetteDelete extends AbstractAction {
		public ActionLolfNomenclatureRecetteDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onLolfNomenclatureRecetteDelete();
			}
		}
	}

	private class ActionConventionSelect extends AbstractAction {
		public ActionConventionSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onConventionSelect();
			}
		}
	}

	private class ActionConventionOpen extends AbstractAction {
		public ActionConventionOpen() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_LOUPE_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onConventionOpen();
			}
		}
	}

	private class ActionConventionDelete extends AbstractAction {
		public ActionConventionDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onConventionDelete();
			}
		}
	}

	private class ActionCommentaires extends AbstractAction {
		public ActionCommentaires() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_NOTE_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Commentaires client et prestataire...");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onCommentaires();
			}
		}
	}

	private class ActionCheckLasm extends AbstractAction {
		public ActionCheckLasm() {
			super("Lasm");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Prestation interne dans le cadre de la LASM (Livraison A Soi-M\u00EAme) = inclure la TVA");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				if (checkBoxLasm.isSelected()) {
					eoPrestation.setPrestApplyTva("O");
				}
				else {
					eoPrestation.setPrestApplyTva("N");
				}
				prestationLigneTablePanel.reloadData();
				prestationLigneORTablePanel.reloadData();
				updateTotaux();
			}
		}
	}

	private void setTypeCreditRec() {

		System.out.println("setTypeCreditRec");

		if (eoPrestation != null) {
			eoPrestation.setTypeCreditRecRelationship((EOTypeCredit) cbTypeCreditRec.getSelectedEOObject());
			// si on controle la destination par rapport a l'organ et au
			// type cred, on verifie que s'il y en a un de selectionne deja,
			// il est
			// toujours autorise, sinon on le vire
			if (app.getParamBoolean(EOParametres.PARAM_CTRL_ORGAN_DEST, eoPrestation.exercice())) {
				if (eoPrestation.organ() != null && eoPrestation.typeCreditRec() != null && eoPrestation.lolfNomenclatureRecette() != null) {
					NSArray a = FinderLolfNomenclatureRecette.find(ec, eoPrestation.exercice(), eoPrestation.organ(), eoPrestation.typeCreditRec());
					if (!a.containsObject(eoPrestation.lolfNomenclatureRecette())) {
						eoPrestation.setLolfNomenclatureRecetteRelationship(null);
						tfLolfNomenclatureRecette.setText(null);
					}
				}
			}
		}

	}

	private void setModeRecouvrement() {
		if (eoPrestation != null) {
			eoPrestation.setModeRecouvrementRelationship((EOModeRecouvrement) cbModeRecouvrement.getSelectedEOObject());
		}
	}

	private class CbActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JComboBox cb = (JComboBox) e.getSource();
			if (cb == cbTypePublic) {
				onTypePublicUpdate((EOTypePublic) cbTypePublic.getSelectedEOObject());
			}
			if (cb == cbTypeCreditRec && eoPrestation != null) {
				setTypeCreditRec();
			}
			if (cb == cbModeRecouvrement) {
				setModeRecouvrement();
			}
		}
	}

	private class RemiseGlobaleDocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent arg0) {
			onRemiseGlobaleUpdate();
		}

		public void insertUpdate(DocumentEvent arg0) {
			onRemiseGlobaleUpdate();
		}

		public void removeUpdate(DocumentEvent arg0) {
			onRemiseGlobaleUpdate();
		}
	}

	private class LocalWindowListener implements WindowListener {
		public void windowActivated(WindowEvent arg0) {
		}

		public void windowClosed(WindowEvent arg0) {
		}

		public void windowClosing(WindowEvent arg0) {
			actionClose.actionPerformed(null);
		}

		public void windowDeactivated(WindowEvent arg0) {
		}

		public void windowDeiconified(WindowEvent arg0) {
		}

		public void windowIconified(WindowEvent arg0) {
		}

		public void windowOpened(WindowEvent arg0) {
		}
	}

	public void setWaitCursor(boolean bool) {
		if (bool) {
			this.getGlassPane().setVisible(true);
		} else {
			this.getGlassPane().setVisible(false);
		}
	}

	/**
	 * Permet de definir un panel qui sert a afficher un waitcursor et a interdire toute modif sur le panel.
	 *
	 * @author rprin
	 */
	public final class BusyGlassPanel extends JPanel {
		public final Color COLOR_WASH = new Color(64, 64, 64, 32);

		public BusyGlassPanel() {
			super.setOpaque(false);
			super.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

			// Suck up them events!!!
			super.addKeyListener((new KeyAdapter() {
			}));
			super.addMouseListener((new MouseAdapter() {
			}));
			super.addMouseMotionListener((new MouseMotionAdapter() {
			}));
		}

		public final void paintComponent(Graphics p_graphics) {
			Dimension l_size = super.getSize();

			// Wash the pane with translucent gray.
			p_graphics.setColor(COLOR_WASH);
			p_graphics.fillRect(0, 0, l_size.width, l_size.height);

			// Paint a grid of white/black dots.
			p_graphics.setColor(Color.white);
			for (int j = 3; j < l_size.height; j += 8) {
				for (int i = 3; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
			p_graphics.setColor(Color.black);
			for (int j = 4; j < l_size.height; j += 8) {
				for (int i = 4; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
		}
	}

	/**
	 * Charge la facade d'edition a partir de la convention specifiee.
	 *
	 * @throws Exception Impossible de charger la facade
	 */
	protected void conventionChargerFacadesConvention(EOConvention convention) throws Exception {
		if (convention != null) {
			getFacadeEditionConventionFormation().ouvrirConvention(convention);
			getFacadeEditionConventionFormation().setUtilisateur(app.appUserInfo().utilisateur());
		}
	}

	/**
	 * Rassemble et charge la facade d'edition a partir de la prestation en cours les proprietes permettant de creer une convention de formation si
	 * besoin.
	 *
	 * @throws Exception Impossible de charger la facade
	 */
	protected void conventionChargerFacadeConvention() throws Exception {
		try {
			if (getFacadeEditionConventionFormation().getEtablissementGestionnaire() == null)
				getFacadeEditionConventionFormation().setEtablissementsGestionnairesPossibles();
			getFacadeEditionConventionFormation().setCentreResponsabiliteGestionnaire(eoPrestation.catalogue().fournisUlr().personne().structureUlr());
			getFacadeEditionConventionFormation().setTypeContrat(TypeContrat.TYCON_ID_INTERNE_CONVENTION_FORMATION);
			getFacadeEditionConventionFormation().setTypeReconduction(TypeReconduction.TR_ID_INTERNE_NON_PREVUE);
			getFacadeEditionConventionFormation().setModeGestion(ModeGestion.LIBELLE_COURT_CS);

			getFacadeEditionConventionFormation().setUtilisateur(app.appUserInfo().utilisateur());

			// partenaire = client prestation
			getFacadeEditionConventionFormation().supprimerPartenaires();// au
																			// cas
																			// ou
																			// changement
																			// de
																			// client
			getFacadeEditionConventionFormation().modifierPartenaire(eoPrestation.personne(), Boolean.TRUE, null, eoPrestation.companion().totalTTCLive(), true);

			// dates debut et fin
			NSTimestamp dateDebut = (NSTimestamp) eoPrestation.prestationLignes().valueForKeyPath("@min.catalogueArticle.article.articlePrestation.artpCfcDateDebut");
			NSTimestamp dateFin = (NSTimestamp) eoPrestation.prestationLignes().valueForKeyPath("@max.catalogueArticle.article.articlePrestation.artpCfcDateFin");
			getFacadeEditionConventionFormation().setDateDebut(dateDebut != null ? dateDebut : new NSTimestamp());
			getFacadeEditionConventionFormation().setDateFin(dateFin != null ? dateFin : new NSTimestamp());
			getFacadeEditionConventionFormation().setDateDebutExec(dateDebut);
			getFacadeEditionConventionFormation().setDateFinExec(dateFin);

			// NB: eoPrestation.prestLibelle() est null si on est en creation
			// donc on utilise le text field
			String objet;
			if (tfPrestLibelle.getText() != null && !"".equals(tfPrestLibelle.getText()))
				objet = tfPrestLibelle.getText();
			else
				objet = "Formation " + eoPrestation.personne().persNomPrenom();

			getFacadeEditionConventionFormation().setTauxTva(null);
			getFacadeEditionConventionFormation().setMontantGlobalHT(eoPrestation.companion().totalTTCLive(), true);
			getFacadeEditionConventionFormation().setObjet(objet);
			getFacadeEditionConventionFormation().setReferenceExterne(FacadeEditionGeneralitesConventionFormation.GetDefaultReferenceExterneAvecPrestation(eoPrestation.personne().persNomPrenom()));
			getFacadeEditionConventionFormation().setObservations(FacadeEditionGeneralitesConventionFormation.GetDefaultObservationsAvecPrestation(eoPrestation.prestNumero()));
			getFacadeEditionConventionFormation().setCreditsLimitatifs(Boolean.FALSE);
			getFacadeEditionConventionFormation().setLucratif(Boolean.FALSE);
			getFacadeEditionConventionFormation().setTvaRecuperable(Boolean.FALSE);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Impossible de pr\u00E9parer les infos permettant de cr\u00E9er une convention " + "\u00E0 partir de cette prestation :\n\n" + e.getMessage(), e);
		}

		System.out.println("[" + new Date() + "] PrestationAddUpdate.takeValuesForConvention() facade edition convention : ");
		System.out.println(getFacadeEditionConventionFormation().toFullString());
	}

	/**
	 * Met a jour la convention associee a cette prestation (libelle, montant, etc).
	 *
	 * @throws Exception Impossible de mettre a jour.
	 */
	protected void conventionMettreAJour() throws Exception {

		if (eoPrestation != null && _typeFC.equals(eoPrestation.typePublic()) && eoPrestation.convention() != null) {

			try {
				conventionChargerFacadeConvention();
				ProxyPrestationConventionFormation conventionProxy = new ProxyPrestationConventionFormation(getFacadeEditionConventionFormation(), this, Boolean.TRUE);
				conventionProxy.mettreAJourConvention(eoPrestation.convention());
			} catch (ExceptionConventionModification e) {
				e.printStackTrace();
				// throw new Exception("Impossible de mettre a jour la
				// convention associee a cette prestation :\n\n" +
				// e.getMessage(), e);
			}
		}
	}

	/**
	 * @throws Exception
	 */
	protected void conventionAnnulerCreation() throws Exception {

		if (eoPrestation != null && _typeFC.equals(eoPrestation.typePublic()) && eoPrestation.convention() != null) {

			ProxyPrestationConventionFormation conventionProxy =
					new ProxyPrestationConventionFormation(getFacadeEditionConventionFormation(), this, Boolean.TRUE);

			try {
				conventionProxy.annulerCreationConvention(eoPrestation.convention());
			} catch (ExceptionConventionModification e) {
				e.printStackTrace();
				throw new Exception("Impossible d'annuler la cr\u00E9ation de la convention associ\u00E9e \u00E0 cette prestation :\n\n" + e.getMessage(), e);
			}

		}
	}

	/**
	 * Client select implementation.
	 *
	 * @author flagouey
	 */
	private final class PrestationAddUpdateClientSelect extends ClientSelect {
		/** Serial Version ID. */
		private static final long serialVersionUID = 1L;

		@Override
		protected void registerValidators() {
			super.registerValidators();
			addPersonneValidators(new FournisseurEtatValideValidator(
					SelectValidatorSeverity.WARNING, FournisseurEtatValideValidator.WARN_FOURNISSEUR_ETAT_INVALIDE));
		}
	}

}
