/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.net.URL;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.kava.client.factory.FactoryCatalogueArticle;
import org.cocktail.kava.client.finder.FinderTva;
import org.cocktail.kava.client.finder.FinderTypeArticle;
import org.cocktail.kava.client.finder.FinderTypePublic;
import org.cocktail.kava.client.metier.EOCatalogue;
import org.cocktail.kava.client.metier.EOCatalogueArticle;
import org.cocktail.kava.client.metier.EOTva;
import org.cocktail.kava.client.metier.EOTypeArticle;
import org.cocktail.kava.client.metier.EOTypePublic;

import app.client.select.PlancoCreditRecSelect;
import app.client.tools.Computation;
import app.client.tools.Constants;
import app.client.ui.UIUtilities;
import app.client.ui.ZDateField;
import app.client.ui.ZDropBox;
import app.client.ui.ZEOComboBox;
import app.client.ui.ZEOMatrix;
import app.client.ui.ZNumberField;
import app.client.ui.ZTextArea;
import app.client.ui.ZTextField;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;

public class CatalogueArticleAddUpdate extends JDialog {

	// private static final Dimension WINDOW_DIMENSION = new Dimension(530, 500);
	private static final String WINDOW_TITLE = "Article";

	protected ApplicationClient app;
	protected EOEditingContext ec;

	private static CatalogueArticleAddUpdate sharedInstance;

	private EOCatalogueArticle eoCatalogueArticle;

	private Action actionClose, actionValid, actionCancel;
	private Action actionShowRequired, actionUnshowRequired;
	private Action actionPlancoCreditRecSelect;
	private Action actionFileSelect, actionFileDelete;
	private Action actionCheckCalculAutoHT, actionCheckCalculAutoTTC;

	private ZEOMatrix matrixTypeArticle;
	private JCheckBox checkBoxArtpInvisibleWeb;
	private ZEOComboBox cbTypePublic;

	private CbActionListener cbActionListener;

	private ZTextArea tfArtLibelle;
	private ZTextField tfCaarReference;
	private ZNumberField tfCaarPrixHt, tfCaarPrixTtc;
	private ZEOComboBox cbTva;
	private JCheckBox cbCalculAutoHt, cbCalculAutoTtc;
	private ZNumberField tfArtpQteMin, tfArtpQteMax;
	private ZDateField tfArtpCfcDateDebut, tfArtpCfcDateFin;
	private ZNumberField tfArtpCfcDuree;

	private JPanel panelFc;

	private ZTextField tfFileChooser;
	private JFileChooser fileChooser;
	private GEDProxy gedProxy;

	private ZTextField tfPcoNumRecette;

	private JButton btValid, btCancel;

	private PlancoCreditRecSelect plancoCreditRecSelect;

	private DocumentListener htDocumentListener, ttcDocumentListener;

	private boolean result;

	public CatalogueArticleAddUpdate() {
		super((JFrame) null, WINDOW_TITLE, true);

		app = (ApplicationClient) EOApplication.sharedApplication();
		ec = app.editingContext();

		setGlassPane(new BusyGlassPanel());
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new LocalWindowListener());

		actionClose = new ActionClose();
		actionValid = new ActionValid();
		actionCancel = new ActionCancel();
		actionShowRequired = new ActionShowRequired();
		actionUnshowRequired = new ActionUnshowRequired();
		actionPlancoCreditRecSelect = new ActionPlancoCreditRecSelect();
		actionFileSelect = new ActionFileSelect();
		actionFileDelete = new ActionFileDelete();

		cbActionListener = new CbActionListener();

		matrixTypeArticle = new ZEOMatrix(FinderTypeArticle.findAll(ec), EOTypeArticle.TYAR_LIBELLE_KEY, null, new MatrixItemListener(),
				SwingConstants.HORIZONTAL);
		checkBoxArtpInvisibleWeb = new JCheckBox(new ActionCheckArtpInvisible());
		cbTypePublic = new ZEOComboBox(FinderTypePublic.findAll(ec), EOTypePublic.TYPU_LIBELLE_KEY, "Tous", null, null, 200);
		cbTypePublic.addActionListener(cbActionListener);

		tfArtLibelle = new ZTextArea(4, 60);
		tfCaarReference = new ZTextField(40);

		htDocumentListener = new PrixDocumentListener();
		ttcDocumentListener = new PrixDocumentListener();
		tfCaarPrixHt = new ZNumberField(10, app.getCurrencyPrecisionFormatEdit());
		tfCaarPrixHt.getDocument().addDocumentListener(htDocumentListener);
		tfCaarPrixTtc = new ZNumberField(10, app.getCurrencyPrecisionFormatEdit());

		actionCheckCalculAutoHT = new ActionCheckCalculAutoHT();
		actionCheckCalculAutoTTC = new ActionCheckCalculAutoTTC();
		cbCalculAutoHt = new JCheckBox(actionCheckCalculAutoHT);
		cbCalculAutoTtc = new JCheckBox(actionCheckCalculAutoTTC);

		cbTva = new ZEOComboBox(FinderTva.find(ec), EOTva.TVA_TAUX_KEY, null, Constants.FORMAT_DECIMAL_DISPLAY, null, 80);
		cbTva.addActionListener(cbActionListener);

		tfArtpQteMin = new ZNumberField(10, Constants.FORMAT_DECIMAL_EDIT);
		tfArtpQteMax = new ZNumberField(10, Constants.FORMAT_DECIMAL_EDIT);

		tfArtpCfcDateDebut = new ZDateField(10);
		tfArtpCfcDateFin = new ZDateField(10);
		tfArtpCfcDuree = new ZNumberField(6, Constants.FORMAT_INTEGER);

		tfPcoNumRecette = new ZTextField(40);
		tfPcoNumRecette.setViewOnly();

		tfFileChooser = new ZTextField(40);
		// tfFileChooser.setDragEnabled(true);
		// tfFileChooser.setTransferHandler(new TransferHandler("file"));
		gedProxy = new GEDProxy();

		btCancel = new JButton(actionCancel);
		btValid = new JButton(actionValid);

		initGUI();

		cbCalculAutoTtc.setSelected(true);

		setEditable(true, false);
	}

	// private class FileTransferHandler extends TransferHandler {
	//
	// }

	public static CatalogueArticleAddUpdate sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new CatalogueArticleAddUpdate();
		}
		return sharedInstance;
	}

	/**
	 * Cr\u00E9ation d'un nouvel article de type article
	 *
	 * @param catalogue
	 * @return
	 */
	public boolean openNew(EOCatalogue catalogue) {
		if (catalogue == null) {
			System.err.println("[CatalogueArticleAddUpdate:open(EOCatalogue)] Aucun catalogue pass\u00E9 pour ouvrir en cr\u00E9ation !");
			return false;
		}
		app.superviseur().setWaitCursor(this, true);

		setTitle(WINDOW_TITLE + " - Cr\u00E9ation");
		setEditable(true, false);

		eoCatalogueArticle = FactoryCatalogueArticle.newObject(ec, catalogue);
		eoCatalogueArticle.article().articlePrestation().setPcoNumRecette(
				catalogue.cataloguePrestation().pcoNumRecette());
		eoCatalogueArticle.article().setTypeArticleRelationship(FinderTypeArticle.typeArticleArticle(ec));
		updateData();

		return open();
	}

	/**
	 * Cr\u00E9ation d'un nouvel article de type option ou remise
	 *
	 * @param catalogueArticle L'article p\u00E9re...
	 * @return
	 */
	public boolean openNew(EOCatalogueArticle catalogueArticle) {
		if (catalogueArticle == null) {
			System.err.println("[CatalogueArticleAddUpdate:open(EOCatalogueArticle)] Aucun article pass\u00E9 pour ouvrir en cr\u00E9ation !");
			return false;
		}
		app.superviseur().setWaitCursor(this, true);
		setTitle(WINDOW_TITLE + " - Cr\u00E9ation");
		setEditable(true, false);

		eoCatalogueArticle = FactoryCatalogueArticle.newObject(ec, catalogueArticle);
		eoCatalogueArticle.article().articlePrestation().setPcoNumRecette(
				catalogueArticle.article().articlePrestation().pcoNumRecette());
		eoCatalogueArticle.article().setTypeArticleRelationship(FinderTypeArticle.typeArticleOption(ec));
		updateData();

		return open();
	}

	public boolean open(EOCatalogueArticle article) {
		if (article == null) {
			System.err.println("[CatalogueArticleAddUpdate:open(EOCatalogueArticle)] Aucun article pass\u00E9 pour ouvrir en modif !");
			return false;
		}

		setTitle(WINDOW_TITLE + " - Modification");
		setEditable(true, true);

		// en modif., on ne calcule pas par d\u00E9faut les prix automatiquement, sinon le simple fait d'ouvrir en modif un article
		// pourrait modifier un tarif a cause des arrondis...
		if (cbCalculAutoHt.isSelected()) {
			cbCalculAutoHt.setSelected(false);
			actionCheckCalculAutoHT.actionPerformed(null);
		}
		if (cbCalculAutoTtc.isSelected()) {
			cbCalculAutoTtc.setSelected(false);
			actionCheckCalculAutoTTC.actionPerformed(null);
		}

		this.eoCatalogueArticle = article;
		updateData();

		return open();
	}

	public boolean canQuit() {
		if (isShowing()) {
			actionCancel.actionPerformed(null);
		}
		return !isShowing();
	}

	public EOCatalogueArticle getLastOpened() {
		return eoCatalogueArticle;
	}

	private boolean open() {
		result = false;
		int screenWidth = (int) this.getGraphicsConfiguration().getBounds().getWidth();
		int screenHeight = (int) this.getGraphicsConfiguration().getBounds().getHeight();
		this.setLocation((screenWidth / 2) - ((int) this.getSize().getWidth() / 2),
				((screenHeight / 2) - ((int) this.getSize().getHeight() / 2)));

		app.superviseur().setWaitCursor(this, false);
		setVisible(true);

		return result;
	}

	private void onValid() {
		try {
			eoCatalogueArticle.article().setTypeArticleRelationship((EOTypeArticle) matrixTypeArticle.getSelectedEOObject());
			eoCatalogueArticle.article().articlePrestation().setArtpInvisibleWeb(checkBoxArtpInvisibleWeb.isSelected() ? "O" : "N");
			eoCatalogueArticle.article().articlePrestation().setTypePublicRelationship((EOTypePublic) cbTypePublic.getSelectedEOObject());

			eoCatalogueArticle.article().setArtLibelle(tfArtLibelle.getText());
			eoCatalogueArticle.setCaarReference(tfCaarReference.getText());
			eoCatalogueArticle.setCaarPrixHt(tfCaarPrixHt.getBigDecimal());
			eoCatalogueArticle.setCaarPrixTtc(tfCaarPrixTtc.getBigDecimal());

			eoCatalogueArticle.setTvaRelationship((EOTva) cbTva.getSelectedEOObject());

			if (eoCatalogueArticle.article().typeArticle().equals(FinderTypeArticle.typeArticleRemise(ec))) {
				eoCatalogueArticle.article().articlePrestation().setArtpQteMin(tfArtpQteMin.getBigDecimal());
				eoCatalogueArticle.article().articlePrestation().setArtpQteMax(tfArtpQteMax.getBigDecimal());
				// v\u00E9rif que les montants soient n\u00E9gatifs
				if (eoCatalogueArticle.caarPrixHt() != null && eoCatalogueArticle.caarPrixTtc() != null) {
					if (eoCatalogueArticle.caarPrixHt().signum() == 1 || eoCatalogueArticle.caarPrixTtc().signum() == 1) {
						if (!app.showConfirmationDialog("ATTENTION",
								"Pour une remise, les montants doivent \u00EAtre n\u00E9gatifs... convertir en n\u00E9gatif?", "Oui", "Non")) {
							return;
						}
						if (eoCatalogueArticle.caarPrixHt().signum() == 1) {
							eoCatalogueArticle.setCaarPrixHt(eoCatalogueArticle.caarPrixHt().negate());
						}
						if (eoCatalogueArticle.caarPrixTtc().signum() == 1) {
							eoCatalogueArticle.setCaarPrixTtc(eoCatalogueArticle.caarPrixTtc().negate());
						}
					}
				}
			}
			else {
				eoCatalogueArticle.article().articlePrestation().setArtpQteMin(null);
				eoCatalogueArticle.article().articlePrestation().setArtpQteMax(null);
				// v\u00E9rif que les montants soient positifs
				if (eoCatalogueArticle.caarPrixHt() != null && eoCatalogueArticle.caarPrixTtc() != null) {
					if (eoCatalogueArticle.caarPrixHt().signum() == -1 || eoCatalogueArticle.caarPrixTtc().signum() == -1) {
						if (!app
								.showConfirmationDialog("ATTENTION", "Les montants doivent \u00EAtre positifs... convertir en positif?", "Oui", "Non")) {
							return;
						}
						if (eoCatalogueArticle.caarPrixHt().signum() == -1) {
							eoCatalogueArticle.setCaarPrixHt(eoCatalogueArticle.caarPrixHt().negate());
						}
						if (eoCatalogueArticle.caarPrixTtc().signum() == -1) {
							eoCatalogueArticle.setCaarPrixTtc(eoCatalogueArticle.caarPrixTtc().negate());
						}
					}
				}
			}

			if (FinderTypePublic.typePublicFormationContinue(ec).equals(eoCatalogueArticle.article().articlePrestation().typePublic())) {
				eoCatalogueArticle.article().articlePrestation().setArtpCfcDateDebut(tfArtpCfcDateDebut.getNSTimestamp());
				eoCatalogueArticle.article().articlePrestation().setArtpCfcDateFin(tfArtpCfcDateFin.getNSTimestamp());
				eoCatalogueArticle.article().articlePrestation().setArtpCfcDuree(tfArtpCfcDuree.getInteger());
			}
			else {
				eoCatalogueArticle.article().articlePrestation().setArtpCfcDateDebut(null);
				eoCatalogueArticle.article().articlePrestation().setArtpCfcDateFin(null);
				eoCatalogueArticle.article().articlePrestation().setArtpCfcDuree(null);
			}

			// FIXME
			eoCatalogueArticle.article().articlePrestation().setArtpQteDispo(new Integer(0));

			ec.saveChanges();
			result = true;

			// sauvegarde du fichier lie
			try {
				saveAttachedFile();
			} catch (Exception e) {
				app.showErrorDialog("Erreur lors de la sauvegarde du fichier li\u00E9 : " + e);
			}

			hide();
		} catch (Exception e) {
			app.showErrorDialog(e);
		}
	}

	private void onCancel() {
		if (app.showConfirmationDialog("Attention", "Voulez vous vraiment annuler ?", "Oui", "Non")) {
			ec.revert();
			result = false;
			hide();
		}
	}

	private void onClose() {
		actionCancel.actionPerformed(null);
	}

	private void saveAttachedFile() throws Exception {
		String oldLien = null, newLien = null;
		Integer oldPid = null, newPid = null;

		oldLien = eoCatalogueArticle.caarDocReference();
		if (eoCatalogueArticle.caarDocId() != null) {
			oldPid = new Integer(eoCatalogueArticle.caarDocId().intValue());
		}

		String refFile = tfFileChooser.getText();
		// v\u00E9rif du fichier...
		if (refFile != null) {
			File rootFile = new File(refFile);
			if (!rootFile.exists()) {
				try {
					URL url = new URL(refFile);
					newLien = url.toString();
				} catch (java.net.MalformedURLException e) {
					throw new Exception("Erreur! Pas de fichier a enregistrer.");
				}
			}
			else {
				// on enregistre le fichier par GEDFS...
				newPid = saveGedFile(oldPid, refFile, "ACTES/PRESTATION", "COUR");
				newLien = gedProxy.referenceDocument(newPid);
			}
		}
		else {
			if (oldPid != null) {
				gedProxy.supprimerDocument(oldPid);
			}
		}

		// si le fichier a chang\u00E9...
		if (oldLien != newLien && (oldLien == null || !oldLien.equals(newLien))) {
			if (newLien != null) {
				eoCatalogueArticle.setCaarDocId(newPid);
				eoCatalogueArticle.setCaarDocReference(newLien);
				ec.saveChanges();
			}
			else {
				if (oldLien != null) {
					// virer l'ancien
					eoCatalogueArticle.setCaarDocId(null);
					eoCatalogueArticle.setCaarDocReference(null);
					ec.saveChanges();
				}
			}
		}
	}

	/**
	 * @param pidOldFile
	 * @param newFile
	 * @param titre
	 * @param categorie
	 * @return L'id du fichier enregistr\u00E9
	 * @throws Exception
	 */
	private Integer saveGedFile(Integer pidOldFile, String newFile, String titre, String categorie) throws Exception {
		if (pidOldFile != null && newFile == null) {
			gedProxy.supprimerDocument(pidOldFile);
			return null;
		}
		if (newFile == null) {
			return null;
		}
		File rootFile = new File(newFile);
		if (!rootFile.exists()) {
			throw new Exception("Erreur! Pas de fichier a enregistrer.");
		}
		if (rootFile.isDirectory()) {
			throw new Exception("Erreur! Le fichier attach\u00E9 ne doit pas \u00EAtre un r\u00E9pertoire.");
		}
		if (pidOldFile != null) {
			// Si c'est le meme fichier
			String oldFilename = gedProxy.referenceDocument(pidOldFile);
			if (oldFilename.endsWith(rootFile.getName())) {
				// Remplacer le document
				gedProxy.remplacerDocument(rootFile.getPath(), titre, pidOldFile);
				return pidOldFile;
			}
			else {
				gedProxy.supprimerDocument(pidOldFile);
			}
		}
		Integer documentID = gedProxy.enregistrerDocument(newFile, titre, categorie);
		if (documentID.intValue() == -1) {
			throw new Exception("Erreur lors de l'enregistrement du fichier " + newFile);
		}
		return documentID;
	}

	private void onUpdateTypeArticle() {
		if (eoCatalogueArticle == null) {
			return;
		}
		boolean b = matrixTypeArticle.getSelectedEOObject().equals(FinderTypeArticle.typeArticleRemise(ec));
		tfArtpQteMin.setEditable(b);
		tfArtpQteMax.setEditable(b);
	}

	private void updateMontants() {
		if (eoCatalogueArticle == null) {
			return;
		}
		if (cbCalculAutoTtc.isSelected()) {
			if (tfCaarPrixHt.getNumber() == null) {
				tfCaarPrixTtc.setNumber(null);
				return;
			}
			if (cbTva.getSelectedEOObject() == null) {
				tfCaarPrixTtc.setNumber(tfCaarPrixHt.getNumber());
				return;
			}
			tfCaarPrixTtc.setNumber(Computation.getTtc(tfCaarPrixHt.getBigDecimal(), (EOTva) cbTva.getSelectedEOObject()));
		}
		else {
			if (cbCalculAutoHt.isSelected()) {
				if (tfCaarPrixTtc.getNumber() == null) {
					tfCaarPrixHt.setNumber(null);
					return;
				}
				if (cbTva.getSelectedEOObject() == null) {
					tfCaarPrixHt.setNumber(tfCaarPrixTtc.getNumber());
					return;
				}
				tfCaarPrixHt.setNumber(Computation.getHt(tfCaarPrixTtc.getBigDecimal(), (EOTva) cbTva.getSelectedEOObject()));
			}
		}
	}

	private void onUpdateTypePublic() {
		if (eoCatalogueArticle == null) {
			return;
		}
		if (cbTypePublic.getSelectedEOObject() != null
				&& cbTypePublic.getSelectedEOObject().equals(FinderTypePublic.typePublicFormationContinue(ec))) {
			tfArtpCfcDateDebut.setEditable(true);
			tfArtpCfcDateFin.setEditable(true);
			tfArtpCfcDuree.setEditable(true);
			panelFc.setVisible(true);
		}
		else {
			tfArtpCfcDateDebut.setEditable(false);
			tfArtpCfcDateFin.setEditable(false);
			tfArtpCfcDuree.setEditable(false);
			panelFc.setVisible(false);
		}
	}

	private void updateData() {
		cleanData();
		if (eoCatalogueArticle == null) {
			return;
		}

		matrixTypeArticle.selectRadioButtonByEo(eoCatalogueArticle.article().typeArticle());
		checkBoxArtpInvisibleWeb.setSelected("O".equals(eoCatalogueArticle.article().articlePrestation().artpInvisibleWeb()));
		cbTypePublic.setSelectedEOObject(eoCatalogueArticle.article().articlePrestation().typePublic());

		tfArtLibelle.setText(eoCatalogueArticle.article().artLibelle());
		tfCaarReference.setText(eoCatalogueArticle.caarReference());
		tfCaarPrixHt.setNumber(eoCatalogueArticle.caarPrixHt());
		tfCaarPrixTtc.setNumber(eoCatalogueArticle.caarPrixTtc());

		cbTva.removeActionListener(cbActionListener);
		if (eoCatalogueArticle.tva() != null) {
			cbTva.setSelectedEOObject(eoCatalogueArticle.tva());
		}
		else {
			cbTva.setSelectedIndex(0);
		}
		cbTva.addActionListener(cbActionListener);

		tfArtpQteMin.setNumber(eoCatalogueArticle.article().articlePrestation().artpQteMin());
		tfArtpQteMax.setNumber(eoCatalogueArticle.article().articlePrestation().artpQteMax());

		tfArtpCfcDateDebut.setDate(eoCatalogueArticle.article().articlePrestation().artpCfcDateDebut());
		tfArtpCfcDateFin.setDate(eoCatalogueArticle.article().articlePrestation().artpCfcDateFin());
		tfArtpCfcDuree.setNumber(eoCatalogueArticle.article().articlePrestation().artpCfcDuree());

		if (eoCatalogueArticle.article().articlePrestation().pcoNumRecette() != null) {
			tfPcoNumRecette.setText(eoCatalogueArticle.article().articlePrestation().pcoNumRecette());
		} else {
			tfPcoNumRecette.setText(null);
		}

		tfFileChooser.setText(eoCatalogueArticle.caarDocReference());
		if (!gedProxy.isAvailable()) {
			actionFileSelect.setEnabled(false);
			actionFileDelete.setEnabled(false);
		}
		else {
			actionFileSelect.setEnabled(true);
			if (eoCatalogueArticle.caarDocReference() != null) {
				actionFileDelete.setEnabled(true);
			}
			else {
				actionFileDelete.setEnabled(false);
			}
		}

		// active/desactive ce qui va bien en fct du type d'article...
		EOEnterpriseObject eo = FinderTypeArticle.typeArticleArticle(ec);
		if (eoCatalogueArticle.article().typeArticle().equals(eo)) {
			matrixTypeArticle.setEnabled(false);
			actionPlancoCreditRecSelect.setEnabled(true);
		}
		else {
			matrixTypeArticle.setEnabled(true);
			matrixTypeArticle.getRadioButtonByEo(eo).setEnabled(false);
			actionPlancoCreditRecSelect.setEnabled(false);
		}
		onUpdateTypeArticle();
	}

	private void setEditable(boolean editable, boolean forUpdate) {

		matrixTypeArticle.setEnabled(editable);
		checkBoxArtpInvisibleWeb.setEnabled(editable);
		cbTypePublic.setEnabled(editable);

		tfArtLibelle.setEditable(editable);
		tfCaarReference.setEditable(editable);
		tfCaarPrixHt.setEditable(editable && !cbCalculAutoHt.isSelected());
		tfCaarPrixTtc.setEditable(editable && !cbCalculAutoTtc.isSelected());

		cbTva.setEnabled(editable);

		tfArtpQteMin.setEditable(editable);
		tfArtpQteMax.setEditable(editable);

		tfArtpCfcDateDebut.setEditable(editable);
		tfArtpCfcDateFin.setEditable(editable);
		tfArtpCfcDuree.setEditable(editable);

		actionFileSelect.setEnabled(editable);
		actionFileDelete.setEnabled(editable);

		actionPlancoCreditRecSelect.setEnabled(editable);

		actionCancel.setEnabled(editable);
		actionValid.setEnabled(editable);
		actionClose.setEnabled(!editable);
	}

	private void cleanData() {
		matrixTypeArticle.clean();
		checkBoxArtpInvisibleWeb.setSelected(false);
		cbTypePublic.clean();
		tfArtLibelle.clean();
		tfCaarReference.clean();
		tfCaarPrixHt.clean();
		tfCaarPrixTtc.setText(null);
		cbTva.clean();
		tfArtpQteMin.clean();
		tfArtpQteMax.clean();
		tfArtpCfcDateDebut.clean();
		tfArtpCfcDateFin.clean();
		tfArtpCfcDuree.clean();
		tfFileChooser.clean();
		tfPcoNumRecette.clean();
	}

	private void showRequired(boolean show) {
		matrixTypeArticle.showRequired(show);
		tfArtLibelle.showRequired(show);
		tfCaarReference.showRequired(show);
		tfCaarPrixHt.showRequired(show);
		tfCaarPrixTtc.showRequired(show);
		cbTva.showRequired(show);
		tfPcoNumRecette.showRequired(show);
	}

	private void onPlancoCreditRecSelect() {
		if (plancoCreditRecSelect == null) {
			plancoCreditRecSelect = new PlancoCreditRecSelect();
		}
		if (plancoCreditRecSelect.open(app.appUserInfo().utilisateur(), app.superviseur().currentExercice(),
				eoCatalogueArticle.article().articlePrestation().pcoNumRecette())) {
			eoCatalogueArticle.article().articlePrestation().setPcoNumRecette(
					plancoCreditRecSelect.getSelected().pcoNum());
			if (eoCatalogueArticle.article().articlePrestation().pcoNumRecette() != null) {
				tfPcoNumRecette.setText(eoCatalogueArticle.article().articlePrestation().pcoNumRecette());
			}
		}
	}

	private void onFileSelect() {
		if (fileChooser == null) {
			fileChooser = new JFileChooser();
		}
		if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			if (fileChooser.getSelectedFile() != null) {
				tfFileChooser.setText(fileChooser.getSelectedFile().getAbsolutePath());
				actionFileDelete.setEnabled(true);
			}
			else {
				tfFileChooser.setText(null);
				actionFileDelete.setEnabled(false);
			}
		}
	}

	private void onFileDelete() {
		tfFileChooser.setText(null);
		actionFileDelete.setEnabled(false);
	}

	private void initGUI() {
		JPanel contentPanel = new JPanel(new BorderLayout());
		contentPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		// contentPanel.setPreferredSize(WINDOW_DIMENSION);
		setContentPane(contentPanel);
		setResizable(false);

		Border commonEmptyThickBorder = BorderFactory.createEmptyBorder(12, 3, 12, 3);
		Border commonEmptyThinBorder = BorderFactory.createEmptyBorder(0, 3, 0, 3);

		// type
		JPanel panelTypeArticle = UIUtilities.labeledComponent("Type d'article", matrixTypeArticle, null, true);

		//
		JPanel panel2 = new JPanel(new BorderLayout(0, 0));
		panel2.setBorder(commonEmptyThinBorder);
		panel2.add(UIUtilities.labeledComponent(" ", checkBoxArtpInvisibleWeb, null), BorderLayout.LINE_START);
		panel2.add(new JPanel(), BorderLayout.CENTER);
		panel2.add(UIUtilities.labeledComponent("Type de public", cbTypePublic, null), BorderLayout.LINE_END);

		// prix
		JPanel panelTemp2 = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panelTemp2.setBorder(commonEmptyThinBorder);

		cbCalculAutoHt.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);
		cbCalculAutoHt.setVerticalTextPosition(SwingConstants.TOP);
		cbCalculAutoHt.setHorizontalTextPosition(SwingConstants.CENTER);
		cbCalculAutoHt.setIconTextGap(0);
		cbCalculAutoHt.setFocusPainted(false);
		cbCalculAutoHt.setBorderPaintedFlat(true);

		cbCalculAutoTtc.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);
		cbCalculAutoTtc.setVerticalTextPosition(SwingConstants.TOP);
		cbCalculAutoTtc.setHorizontalTextPosition(SwingConstants.CENTER);
		cbCalculAutoTtc.setIconTextGap(0);
		cbCalculAutoTtc.setFocusPainted(false);
		cbCalculAutoTtc.setBorderPaintedFlat(true);

		panelTemp2.add(UIUtilities.labeledComponent("HT", tfCaarPrixHt, null));
		panelTemp2.add(UIUtilities.labeledComponent(" ", cbCalculAutoHt, null));
		panelTemp2.add(Box.createHorizontalStrut(20));
		panelTemp2.add(UIUtilities.labeledComponent("Tva", cbTva, null));
		panelTemp2.add(Box.createHorizontalStrut(20));
		panelTemp2.add(UIUtilities.labeledComponent("TTC", tfCaarPrixTtc, null));
		panelTemp2.add(UIUtilities.labeledComponent(" ", cbCalculAutoTtc, null));

		JPanel panelPrix = UIUtilities.labeledComponent("Tarif", panelTemp2, null, true);

		// r\u00E9f\u00E9rence
		JPanel panelReference = UIUtilities.labeledComponent("Libell\u00E9 / R\u00E9f\u00E9rence", tfCaarReference, null);
		panelReference.setBorder(commonEmptyThinBorder);

		// description
		JPanel panelDescription = UIUtilities.labeledComponent("Description", tfArtLibelle, null);
		panelDescription.setBorder(commonEmptyThinBorder);

		// quantites min et max
		JPanel panelTemp = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panelTemp.setBorder(BorderFactory.createTitledBorder(null, "Param\u00EAtres de remise quantitative", TitledBorder.DEFAULT_JUSTIFICATION,
				TitledBorder.DEFAULT_POSITION, null, Constants.COLOR_LABEL_FGD_LIGHT));
		panelTemp.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);
		panelTemp.add(UIUtilities.labeledComponent("Qt\u00E9 min", tfArtpQteMin, null));
		panelTemp.add(Box.createHorizontalStrut(20));
		panelTemp.add(UIUtilities.labeledComponent("Qt\u00E9 max", tfArtpQteMax, null));
		JPanel panelQtes = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panelQtes.setBorder(commonEmptyThickBorder);
		panelQtes.add(panelTemp);

		// sp\u00E9cifs fc
		JPanel panelTemp3 = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panelTemp3.setBorder(BorderFactory.createTitledBorder(null, "Sp\u00E9cificit\u00E9s Formation Continue", TitledBorder.DEFAULT_JUSTIFICATION,
				TitledBorder.DEFAULT_POSITION, null, Constants.COLOR_LABEL_FGD_LIGHT));
		panelTemp3.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);
		panelTemp3.add(UIUtilities.labeledComponent("Date de d\u00E9but", tfArtpCfcDateDebut, null));
		panelTemp3.add(Box.createHorizontalStrut(10));
		panelTemp3.add(UIUtilities.labeledComponent("Date de fin", tfArtpCfcDateFin, null));
		panelTemp3.add(Box.createHorizontalStrut(30));
		panelTemp3.add(UIUtilities.labeledComponent("Dur\u00E9e (heures)", tfArtpCfcDuree, null));
		panelFc = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panelFc.setBorder(commonEmptyThickBorder);
		panelFc.add(panelTemp3);

		// qtes + fc
		// JPanel panelQtesFc = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		Box panelQtesFc = Box.createHorizontalBox();
		panelQtesFc.setBorder(commonEmptyThickBorder);
		panelQtesFc.add(panelQtes);
		panelQtesFc.add(Box.createHorizontalStrut(10));
		panelQtesFc.add(panelFc);

		// planco
		JPanel panelPlanco = UIUtilities.labeledComponent("Imputation recette", tfPcoNumRecette, actionPlancoCreditRecSelect, true);
		panelPlanco.setBorder(commonEmptyThinBorder);

		// fichier li\u00E9
		JPanel panelFileChooser = UIUtilities.labeledComponent("Fichier li\u00E9 (Ged)", new ZDropBox(tfFileChooser), new Action[] {
				actionFileSelect, actionFileDelete
		}, true, UIUtilities.DEFAULT_ALIGNMENT);
		panelFileChooser.setBorder(commonEmptyThinBorder);

		Box center = Box.createVerticalBox();
		center.setBorder(BorderFactory.createEmptyBorder());

		center.add(panelTypeArticle);
		center.add(panel2);
		center.add(panelPrix);
		center.add(panelReference);
		center.add(panelDescription);
		center.add(panelQtesFc);
		center.add(panelPlanco);
		center.add(panelFileChooser);

		getContentPane().add(center, BorderLayout.CENTER);
		getContentPane().add(buildBottomPanel(), BorderLayout.PAGE_END);

		initInputMap();

		this.validate();
		this.pack();
	}

	private final JPanel buildBottomPanel() {
		JPanel p = new JPanel();
		p.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.add(Box.createHorizontalGlue());
		p.add(btCancel);
		p.add(Box.createRigidArea(new Dimension(5, 0)));
		p.add(btValid);
		return p;
	}

	private void initInputMap() {
		((JComponent) getContentPane()).getActionMap().put("CTRL_Q", app.superviseur().mainMenu.actionQuit);
		((JComponent) getContentPane()).getActionMap().put("CTRL_S", actionValid);
		((JComponent) getContentPane()).getActionMap().put("F10", actionValid);
		((JComponent) getContentPane()).getActionMap().put("ESCAPE", actionCancel);
		((JComponent) getContentPane()).getActionMap().put("F8", actionShowRequired);
		((JComponent) getContentPane()).getActionMap().put("ALT_F8", actionUnshowRequired);
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
				KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK), "CTRL_Q");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
				KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK), "CTRL_S");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F10, 0), "F10");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"ESCAPE");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("F8"), "F8");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("released F8"), "ALT_F8");
	}

	private class MatrixItemListener implements ItemListener {
		public void itemStateChanged(ItemEvent arg0) {
			onUpdateTypeArticle();
		}
	}

	private class CbActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JComboBox cb = (JComboBox) e.getSource();
			if (cb == cbTva && eoCatalogueArticle != null) {
				updateMontants();
			}
			if (cb == cbTypePublic && eoCatalogueArticle != null) {
				onUpdateTypePublic();
			}
		}
	}

	private class PrixDocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent arg0) {
			updateMontants();
		}

		public void insertUpdate(DocumentEvent arg0) {
			updateMontants();
		}

		public void removeUpdate(DocumentEvent arg0) {
			updateMontants();
		}
	}

	private class ActionCheckCalculAutoHT extends AbstractAction {
		public ActionCheckCalculAutoHT() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CALCULER_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Calculer automatiquement le HT depuis le TTC");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				if (cbCalculAutoHt.isSelected()) {
					tfCaarPrixHt.setEditable(false);
					tfCaarPrixTtc.getDocument().addDocumentListener(ttcDocumentListener);
					if (cbCalculAutoTtc.isSelected()) {
						cbCalculAutoTtc.setSelected(false);
						tfCaarPrixTtc.setEditable(true);
						tfCaarPrixHt.getDocument().removeDocumentListener(htDocumentListener);
					}
					updateMontants();
				}
				else {
					tfCaarPrixHt.setEditable(true);
					tfCaarPrixTtc.getDocument().removeDocumentListener(ttcDocumentListener);
				}
			}
		}
	}

	private class ActionCheckCalculAutoTTC extends AbstractAction {
		public ActionCheckCalculAutoTTC() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CALCULER_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Calculer automatiquement le TTC depuis le HT");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				if (cbCalculAutoTtc.isSelected()) {
					tfCaarPrixTtc.setEditable(false);
					tfCaarPrixHt.getDocument().addDocumentListener(htDocumentListener);
					if (cbCalculAutoHt.isSelected()) {
						cbCalculAutoHt.setSelected(false);
						tfCaarPrixHt.setEditable(true);
						tfCaarPrixTtc.getDocument().removeDocumentListener(ttcDocumentListener);
					}
					updateMontants();
				}
				else {
					tfCaarPrixTtc.setEditable(true);
					tfCaarPrixHt.getDocument().removeDocumentListener(htDocumentListener);
				}
			}
		}
	}

	private class ActionValid extends AbstractAction {
		public ActionValid() {
			super("Valider");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onValid();
			}
		}
	}

	private class ActionCancel extends AbstractAction {
		public ActionCancel() {
			super("Annuler");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CANCEL_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onCancel();
			}
		}
	}

	private class ActionClose extends AbstractAction {
		public ActionClose() {
			super("Fermer");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CLOSE_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onClose();
			}
		}
	}

	private class ActionShowRequired extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				showRequired(true);
			}
		}
	}

	private class ActionUnshowRequired extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				showRequired(false);
			}
		}
	}

	private class ActionPlancoCreditRecSelect extends AbstractAction {
		public ActionPlancoCreditRecSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onPlancoCreditRecSelect();
			}
		}
	}

	private class ActionFileSelect extends AbstractAction {
		public ActionFileSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onFileSelect();
			}
		}
	}

	private class ActionFileDelete extends AbstractAction {
		public ActionFileDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onFileDelete();
			}
		}
	}

	private class ActionCheckArtpInvisible extends AbstractAction {
		public ActionCheckArtpInvisible() {
			super("Masquer l'article sur le web");
			putValue(AbstractAction.SHORT_DESCRIPTION, "Permet de cacher cet article pour les clients sur le web");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
			}
		}
	}

	private class LocalWindowListener implements WindowListener {
		public void windowActivated(WindowEvent arg0) {
		}

		public void windowClosed(WindowEvent arg0) {
		}

		public void windowClosing(WindowEvent arg0) {
			actionClose.actionPerformed(null);
		}

		public void windowDeactivated(WindowEvent arg0) {
		}

		public void windowDeiconified(WindowEvent arg0) {
		}

		public void windowIconified(WindowEvent arg0) {
		}

		public void windowOpened(WindowEvent arg0) {
		}
	}

	public void setWaitCursor(boolean bool) {
		if (bool) {
			this.getGlassPane().setVisible(true);
		}
		else {
			this.getGlassPane().setVisible(false);
		}
	}

	/**
	 * Permet de d\u00E9finir un panel qui sert à afficher un waitcursor et à interdire toute modif sur le panel.
	 *
	 * @author rprin
	 */
	public final class BusyGlassPanel extends JPanel {
		public final Color COLOR_WASH = new Color(64, 64, 64, 32);

		public BusyGlassPanel() {
			super.setOpaque(false);
			super.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

			// Suck up them events!!!
			super.addKeyListener((new KeyAdapter() {
			}));
			super.addMouseListener((new MouseAdapter() {
			}));
			super.addMouseMotionListener((new MouseMotionAdapter() {
			}));
		}

		public final void paintComponent(Graphics p_graphics) {
			Dimension l_size = super.getSize();

			// Wash the pane with translucent gray.
			p_graphics.setColor(COLOR_WASH);
			p_graphics.fillRect(0, 0, l_size.width, l_size.height);

			// Paint a grid of white/black dots.
			p_graphics.setColor(Color.white);
			for (int j = 3; j < l_size.height; j += 8) {
				for (int i = 3; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
			p_graphics.setColor(Color.black);
			for (int j = 4; j < l_size.height; j += 8) {
				for (int i = 4; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
		}
	}

}
