/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.math.BigDecimal;
import java.text.NumberFormat;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.eof.EOTypeEtat;
import org.cocktail.kava.client.finder.FinderCatalogue;
import org.cocktail.kava.client.finder.FinderCatalogueArticle;
import org.cocktail.kava.client.finder.FinderFournisUlr;
import org.cocktail.kava.client.finder.FinderTypeEtat;
import org.cocktail.kava.client.metier.EOArticle;
import org.cocktail.kava.client.metier.EOArticlePrestation;
import org.cocktail.kava.client.metier.EOCatalogue;
import org.cocktail.kava.client.metier.EOCatalogueArticle;
import org.cocktail.kava.client.metier.EOCataloguePrestation;
import org.cocktail.kava.client.metier.EOFonction;
import org.cocktail.kava.client.metier.EOFournisUlr;
import org.cocktail.kava.client.metier.EOGrhumParametres;
import org.cocktail.kava.client.metier.EOTva;
import org.cocktail.kava.client.metier.EOTypeArticle;
import org.cocktail.kava.client.metier.EOUtilisateur;
import org.cocktail.kava.client.qualifier.Qualifiers;
import org.cocktail.kava.client.qualifier.Qualifiers.QualifierKey;

import app.client.tools.Constants;
import app.client.ui.UIUtilities;
import app.client.ui.ZEOComboBox;
import app.client.ui.ZNumberField;
import app.client.ui.ZTextField;
import app.client.ui.table.ZEOTableCellRenderer;
import app.client.ui.table.ZEOTableModelColumn;
import app.client.ui.table.ZExtendedTablePanel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class Catalogue extends JPanel implements IPieTab {

	private static final Dimension			WINDOW_DIMENSION	= new Dimension(500, 400);
	private static final String				WINDOW_TITLE		= "Catalogues";

	protected ApplicationClient				app;
	protected EOEditingContext				ec;

	private static Catalogue				sharedInstance;

	private ZNumberField				tfCatNumero;
	private ZTextField				tfCatLibelle, tfExercice;
	private ZEOComboBox				cbFournisUlr;
	private CbActionListener				cbActionListener;

	private CatalogueTablePanel				catalogueTablePanel;
	private CatalogueArticleTablePanel		catalogueArticleTablePanel;
	private CatalogueArticleORTablePanel	catalogueArticleORTablePanel;

	private Action							actionClose;

	private JSplitPane						tablesPanel, bottomTablesPanel;

	public Catalogue(String windowTitle) {
		super();

		app = (ApplicationClient) EOApplication.sharedApplication();
		ec = app.editingContext();

		actionClose = new ActionClose();

		NumberFormat integerFormat = NumberFormat.getIntegerInstance();
		integerFormat.setGroupingUsed(false);
		tfCatNumero = new ZNumberField(5, integerFormat);
		tfCatNumero.getDocument().addDocumentListener(new SearchCatNumeroDocumentListener());
		tfCatLibelle = new ZTextField(20);
		tfCatLibelle.getDocument().addDocumentListener(new SearchCatLibelleDocumentListener());
		tfExercice = new ZTextField(4);
		cbActionListener = new CbActionListener();
		cbFournisUlr = new ZEOComboBox(FinderFournisUlr.find(ec, app.getParam(EOGrhumParametres.PARAM_ANNUAIRE_FOU_VALIDE_INTERNE)),
				EOFournisUlr.PERSONNE_PERS_NOM_PRENOM_KEY, "Tous", null, null, 200);
		cbFournisUlr.addActionListener(cbActionListener);

		catalogueTablePanel = new CatalogueTablePanel();
		catalogueTablePanel.setEditable(true);
		catalogueArticleTablePanel = new CatalogueArticleTablePanel();
		catalogueArticleTablePanel.setEditable(false);
		catalogueArticleORTablePanel = new CatalogueArticleORTablePanel();
		catalogueArticleORTablePanel.setEditable(false);

		initGUI();
	}

	public static Catalogue sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new Catalogue(WINDOW_TITLE);
		}
		return sharedInstance;
	}

	/**
	 * Permet eventuellement de configurer au lancement l'interface selon des prefs utilisateur
	 *
	 * @param utilisateur
	 */
	public void init(org.cocktail.application.client.eof.EOUtilisateur utilisateur) {
		tablesPanel.setDividerLocation(0.4);
		bottomTablesPanel.setDividerLocation(0.4);
	}



	public void update() {
		tfExercice.setText(app.superviseur().currentExercice().exeExercice().toString());
		if (app.canUseFonction(EOFonction.DROIT_GERER_CATALOGUES, null)) {
			catalogueTablePanel.setEditable(true);
			catalogueArticleTablePanel.setEditable(true);
			catalogueArticleORTablePanel.setEditable(true);
		}
		else {
			catalogueTablePanel.setEditable(false);
			catalogueArticleTablePanel.setEditable(false);
			catalogueArticleORTablePanel.setEditable(false);
		}
		updateData();
	}

	public void updateData() {
		long start = System.currentTimeMillis();
		catalogueTablePanel.reloadData();
		updateFiltering();
		System.out.println("Chargement catalogues : " + (System.currentTimeMillis() - start) + "ms");

	}

	public boolean canQuit() {
		return CatalogueAddUpdate.sharedInstance().canQuit() && CatalogueArticleAddUpdate.sharedInstance().canQuit();
	}

	public EOCatalogue getSelected() {
		return (EOCatalogue) catalogueTablePanel.selectedObject();
	}

	private void updateFiltering() {
		NSMutableArray andQuals = new NSMutableArray();
		if (tfCatNumero.getNumber() != null) {
			andQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOCatalogue.CATALOGUE_PRESTATION_KEY + "."
					+ EOCataloguePrestation.CAT_NUMERO_KEY + " = %@", new NSArray(tfCatNumero.getNumber())));
		}
		if (tfCatLibelle.getText() != null) {
			String searchLibelleString = "*" + tfCatLibelle.getText() + "*";
			NSMutableArray quals = new NSMutableArray();
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOCatalogue.CAT_LIBELLE_KEY + " caseInsensitiveLike %@",
					new NSArray(searchLibelleString)));
			quals.addObject(Qualifiers.getQualifierFactory(QualifierKey.CATALOGUE_FOUR_PRENOM_PERSONNE).build(
					EOQualifier.QualifierOperatorCaseInsensitiveLike, searchLibelleString));
			quals.addObject(Qualifiers.getQualifierFactory(QualifierKey.CATALOGUE_FOUR_NOM_PERSONNE).build(
					EOQualifier.QualifierOperatorCaseInsensitiveLike, searchLibelleString));
			andQuals.addObject(new EOOrQualifier(quals));
		}
		if (cbFournisUlr.getSelectedEOObject() != null) {
			andQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOCatalogue.FOURNIS_ULR_KEY + " = %@",
					new NSArray((EOFournisUlr) cbFournisUlr.getSelectedEOObject())));
		}
		catalogueTablePanel.setFilteringQualifier(new EOAndQualifier(andQuals));
	}

	private void onClose() {
		//this.hide();
		System.out.println("Catalogue:onClose");
		setVisible(false);
	}

	private void onSearchCatNumero() {
		tfCatLibelle.setText(null);
		cbFournisUlr.setSelectedEOObject(null);
		updateFiltering();
	}

	private class CatalogueTablePanel extends ZExtendedTablePanel {

		private boolean	displayArchives;

		public CatalogueTablePanel() {
			super(null);

			ZEOTableModelColumn col1 = new ZEOTableModelColumn(getDG(), EOCatalogue.CATALOGUE_PRESTATION_KEY + "."
					+ EOCataloguePrestation.CAT_NUMERO_KEY, "No", 50);
			col1.setColumnClass(Integer.class);
			col1.setAlignment(SwingConstants.LEFT);
			addCol(col1);
			addCol(new ZEOTableModelColumn(getDG(), EOCatalogue.CAT_LIBELLE_KEY, "Lib", 150));
			addCol(new ZEOTableModelColumn(getDG(), EOCatalogue.FOURNIS_ULR_KEY + "." + EOFournisUlr.PERSONNE_PERS_NOM_PRENOM_KEY,
					"Fournisseur", 100));
			ZEOTableModelColumn col4 = new ZEOTableModelColumn(getDG(), EOCatalogue.CATALOGUE_PRESTATION_KEY + "."
					+ EOCataloguePrestation.CAT_DATE_VOTE_KEY, "Date vote", 50, Constants.FORMAT_DATESHORT);
			col4.setColumnClass(NSTimestamp.class);
			col4.setAlignment(SwingConstants.LEFT);
			addCol(col4);
			addCol(new ZEOTableModelColumn(getDG(), EOCatalogue.CATALOGUE_PRESTATION_KEY + "." + EOCataloguePrestation.CAT_PUBLIE_WEB_KEY,
					"Web", 10));

			initGUI();

			getTablePanel().getTable().setMyRenderer(new DataTableCellRenderer());

			displayArchives = false;
			buildPopup();
		}

		public void reloadData() {
			EOUtilisateur utilisateur = app.appUserInfo().utilisateur();
			if (app.canUseFonction(EOFonction.DROIT_VOIR_TOUS_LES_CATALOGUES, null)) {
				utilisateur = null;
			}
			EOTypeEtat typeEtat = null;
			if (!displayArchives) {
				typeEtat = FinderTypeEtat.typeEtatValide(ec);
			}
			setObjectArray(FinderCatalogue.find(ec, utilisateur, null, typeEtat, null));
		}

		protected void onAdd() {
			System.out.println("Catalogue.onAdd");
			if (CatalogueAddUpdate.sharedInstance().openNew(
					app.appUserInfo().utilisateur(), (EOFournisUlr) cbFournisUlr.getSelectedEOObject())) {
				System.out.println("lastSelected:" + CatalogueAddUpdate.sharedInstance().getLastOpened());
				reloadData();
				setSelectedObject(CatalogueAddUpdate.sharedInstance().getLastOpened());
			}
			System.out.println("fin Catalogue.onAdd");
			ec.saveChanges();
		}

		protected void onUpdate() {
			System.out.println("Catalogue.onUpdate");
			if (selectedObject() == null) {
				return;
			}
			if (((EOCatalogue) selectedObject()).typeEtat().equals(FinderTypeEtat.typeEtatAnnule(ec))) {
				app.showInfoDialog("Ce catalogue est archiv\u00E9, vous ne pouvez pas le modifier !");
				return;
			}
			if (CatalogueAddUpdate.sharedInstance().open((EOCatalogue) selectedObject())) {
				updateData();
			}
		}

		protected void onDelete() {
			System.out.println("Catalogue.onDelete");
			if (selectedObject() != null) {
				EOCatalogue cat = (EOCatalogue) selectedObject();
				if (cat.typeEtat().equals(FinderTypeEtat.typeEtatAnnule(ec))) {
					if (app.showConfirmationDialog("Attention", "Ce catalogue est archiv\u00E9. Souhaitez-vous le r\u00E9activer?", "Oui", "Non")) {
						try {
							((EOCatalogue) selectedObject()).setTypeEtatRelationship(FinderTypeEtat.typeEtatValide(ec));
							ec.saveChanges();
							reloadData();
						}
						catch (Exception e) {
							app.showErrorDialog(e);
						}
					}
				}
				else {
					if (app.showConfirmationDialog("Attention", "Archiver ce catalogue ?", "Oui", "Non")) {
						try {
							((EOCatalogue) selectedObject()).setTypeEtatRelationship(FinderTypeEtat.typeEtatAnnule(ec));
							ec.saveChanges();
							reloadData();
						}
						catch (Exception e) {
							app.showErrorDialog(e);
						}
					}
				}
			}
		}

		protected void onSelectionChanged() {
			catalogueArticleTablePanel.reloadData();
		}

		private void buildPopup() {
			final JCheckBoxMenuItem menuItem = new JCheckBoxMenuItem("Afficher les catalogues archiv\u00E9s");
			menuItem.setSelected(displayArchives);
			menuItem.addItemListener(new MyPopupItemListener());
			JPopupMenu popup = new JPopupMenu();
			popup.add(menuItem);
			getTablePanel().getTable().setPopup(popup);
		}

		private final class MyPopupItemListener implements ItemListener {
			public void itemStateChanged(ItemEvent e) {
				displayArchives = ((JCheckBoxMenuItem) e.getItem()).isSelected();
				reloadData();
			}
		}

		private class DataTableCellRenderer extends ZEOTableCellRenderer {
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

				if (displayArchives) {
					EOCatalogue catalogue = (EOCatalogue) getTablePanel().getObjectAtRow(row);
					if (catalogue.typeEtat().equals(FinderTypeEtat.typeEtatAnnule(ec))) {
						label.setForeground(Color.gray);
					}
					else {
						if (isSelected) {
							label.setForeground(table.getSelectionForeground());
						}
						else {
							label.setForeground(table.getForeground());
						}
					}
				}
				return label;
			}
		}

	}

	private class CatalogueArticleTablePanel extends ZExtendedTablePanel {

		private boolean	displayArchives;

		public CatalogueArticleTablePanel() {
			super("Articles");

			addCol(new ZEOTableModelColumn(getDG(), EOCatalogueArticle.CAAR_REFERENCE_KEY, "Ref", 80));
			addCol(new ZEOTableModelColumn(getDG(), EOCatalogueArticle.ARTICLE_KEY + "." + EOArticle.ART_LIBELLE_KEY, "Description", 150));
			ZEOTableModelColumn col3 = new ZEOTableModelColumn(getDG(), EOCatalogueArticle.CAAR_PRIX_HT_KEY, "HT", 50, app
					.getCurrencyPrecisionFormatDisplay());
			col3.setColumnClass(BigDecimal.class);
			col3.setAlignment(SwingConstants.RIGHT);
			addCol(col3);
			ZEOTableModelColumn col4 = new ZEOTableModelColumn(getDG(), EOCatalogueArticle.TVA_KEY + "." + EOTva.TVA_TAUX_KEY, "Tva", 50,
					Constants.FORMAT_DECIMAL_DISPLAY);
			col4.setColumnClass(BigDecimal.class);
			col4.setAlignment(SwingConstants.RIGHT);
			addCol(col4);
			ZEOTableModelColumn col5 = new ZEOTableModelColumn(getDG(), EOCatalogueArticle.CAAR_PRIX_TTC_KEY, "TTC", 50, app
					.getCurrencyPrecisionFormatDisplay());
			col5.setColumnClass(BigDecimal.class);
			col5.setAlignment(SwingConstants.RIGHT);
			addCol(col5);

			initGUI();

			getTablePanel().getTable().setMyRenderer(new DataTableCellRenderer());

			displayArchives = false;
			buildPopup();
		}

		public void reloadData() {
			if (catalogueTablePanel.selectedObject() == null) {
				setEditable(false);
				setObjectArray(null);
			} else {
				setEditable(true);
					EOTypeEtat typeEtat = null;
					if (!displayArchives) {
						typeEtat = FinderTypeEtat.typeEtatValide(ec);
					}
					setObjectArray(FinderCatalogueArticle.find(
							ec, (EOCatalogue) catalogueTablePanel.selectedObject(), null, typeEtat));
			}
		}

		protected void onAdd() {
			if (CatalogueArticleAddUpdate.sharedInstance().openNew((EOCatalogue) catalogueTablePanel.selectedObject())) {
				reloadData();
				setSelectedObject(CatalogueArticleAddUpdate.sharedInstance().getLastOpened());
			}
		}

		protected void onUpdate() {
			if (selectedObject() == null) {
				return;
			}
			if (((EOCatalogueArticle) selectedObject()).typeEtat().equals(FinderTypeEtat.typeEtatAnnule(ec))) {
				app.showInfoDialog("Cet article est archiv\u00E9, vous ne pouvez pas le modifier !");
				return;
			}
			if (CatalogueArticleAddUpdate.sharedInstance().open((EOCatalogueArticle) selectedObject())) {
				updateData();
			}
		}

		protected void onDelete() {
			if (selectedObject() != null) {
				EOCatalogueArticle cat = (EOCatalogueArticle) selectedObject();
				if (cat.typeEtat().equals(FinderTypeEtat.typeEtatAnnule(ec))) {
					if (app.showConfirmationDialog("Attention", "Cet article est archiv\u00E9. Souhaitez-vous le r\u00E9activer?", "Oui", "Non")) {
						try {
							((EOCatalogueArticle) selectedObject()).setTypeEtatRelationship(FinderTypeEtat.typeEtatValide(ec));
							ec.saveChanges();
							reloadData();
						}
						catch (Exception e) {
							app.showErrorDialog(e);
						}
					}
				}
				else {
					if (app.showConfirmationDialog("Attention", "Archiver cet article ?", "Oui", "Non")) {
						try {
							((EOCatalogueArticle) selectedObject()).setTypeEtatRelationship(FinderTypeEtat.typeEtatAnnule(ec));
							ec.saveChanges();
							reloadData();
						}
						catch (Exception e) {
							app.showErrorDialog(e);
						}
					}
				}
			}
		}

		protected void onSelectionChanged() {
			catalogueArticleORTablePanel.reloadData();
		}

		private void buildPopup() {
			final JCheckBoxMenuItem menuItem = new JCheckBoxMenuItem("Afficher les articles archiv\u00E9s");
			menuItem.setSelected(displayArchives);
			menuItem.addItemListener(new MyPopupItemListener());
			JPopupMenu popup = new JPopupMenu();
			popup.add(menuItem);
			getTablePanel().getTable().setPopup(popup);
		}

		private final class MyPopupItemListener implements ItemListener {
			public void itemStateChanged(ItemEvent e) {
				displayArchives = ((JCheckBoxMenuItem) e.getItem()).isSelected();
				reloadData();
			}
		}

		private class DataTableCellRenderer extends ZEOTableCellRenderer {
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

				if (displayArchives) {
					EOCatalogueArticle catalogueArticle = (EOCatalogueArticle) getTablePanel().getObjectAtRow(row);
					if (catalogueArticle.typeEtat().equals(FinderTypeEtat.typeEtatAnnule(ec))) {
						label.setForeground(Color.gray);
					}
					else {
						if (isSelected) {
							label.setForeground(table.getSelectionForeground());
						}
						else {
							label.setForeground(table.getForeground());
						}
					}
				}
				return label;
			}
		}

	}

	private class CatalogueArticleORTablePanel extends ZExtendedTablePanel {

		private boolean	displayArchives;

		public CatalogueArticleORTablePanel() {
			super("Options / Remises");

			addCol(new ZEOTableModelColumn(getDG(), EOCatalogueArticle.ARTICLE_KEY + "." + EOArticle.TYPE_ARTICLE_KEY + "."
					+ EOTypeArticle.TYAR_LIBELLE_KEY, "Type", 50));
			addCol(new ZEOTableModelColumn(getDG(), EOCatalogueArticle.CAAR_REFERENCE_KEY, "Ref", 80));
			addCol(new ZEOTableModelColumn(getDG(), EOCatalogueArticle.ARTICLE_KEY + "." + EOArticle.ART_LIBELLE_KEY, "Description", 150));
			ZEOTableModelColumn col4 = new ZEOTableModelColumn(getDG(), EOCatalogueArticle.CAAR_PRIX_HT_KEY, "HT", 50, app
					.getCurrencyPrecisionFormatDisplay());
			col4.setColumnClass(BigDecimal.class);
			col4.setAlignment(SwingConstants.RIGHT);
			addCol(col4);
			ZEOTableModelColumn col5 = new ZEOTableModelColumn(getDG(), EOCatalogueArticle.TVA_KEY + "." + EOTva.TVA_TAUX_KEY, "Tva", 50,
					Constants.FORMAT_DECIMAL_DISPLAY);
			col5.setColumnClass(BigDecimal.class);
			col5.setAlignment(SwingConstants.RIGHT);
			addCol(col5);
			ZEOTableModelColumn col6 = new ZEOTableModelColumn(getDG(), EOCatalogueArticle.CAAR_PRIX_TTC_KEY, "TTC", 50, app
					.getCurrencyPrecisionFormatDisplay());
			col6.setColumnClass(BigDecimal.class);
			col6.setAlignment(SwingConstants.RIGHT);
			addCol(col6);
			ZEOTableModelColumn col7 = new ZEOTableModelColumn(getDG(), EOCatalogueArticle.ARTICLE_KEY + "." + EOArticle.ARTICLE_PRESTATION_KEY
					+ "." + EOArticlePrestation.ARTP_QTE_MIN_KEY, "Qt\u00E9 min", 50, Constants.FORMAT_DECIMAL_DISPLAY);
			col7.setColumnClass(BigDecimal.class);
			col7.setAlignment(SwingConstants.RIGHT);
			addCol(col7);
			ZEOTableModelColumn col8 = new ZEOTableModelColumn(getDG(), EOCatalogueArticle.ARTICLE_KEY + "." + EOArticle.ARTICLE_PRESTATION_KEY
					+ "." + EOArticlePrestation.ARTP_QTE_MAX_KEY, "Qt\u00E9 max", 50, Constants.FORMAT_DECIMAL_DISPLAY);
			col8.setColumnClass(BigDecimal.class);
			col8.setAlignment(SwingConstants.RIGHT);
			addCol(col8);

			initGUI();

			getTablePanel().getTable().setMyRenderer(new DataTableCellRenderer());

			displayArchives = false;
			buildPopup();
		}

		public void reloadData() {
			if (catalogueArticleTablePanel.selectedObject() == null) {
				setEditable(false);
				setObjectArray(null);
			} else {
				setEditable(true);
				EOTypeEtat typeEtat = null;
				if (!displayArchives) {
					typeEtat = FinderTypeEtat.typeEtatValide(ec);
				}
				NSArray optionsRemises = FinderCatalogueArticle.find(ec,
						(EOCatalogueArticle) catalogueArticleTablePanel.selectedObject(), null, typeEtat, null, null);
				setObjectArray(optionsRemises);
			}
		}

		protected void onAdd() {
			if (CatalogueArticleAddUpdate.sharedInstance().openNew((EOCatalogueArticle) catalogueArticleTablePanel.selectedObject())) {
				reloadData();
				setSelectedObject(CatalogueArticleAddUpdate.sharedInstance().getLastOpened());
			}
		}

		protected void onUpdate() {
			if (selectedObject() == null) {
				return;
			}
			if (((EOCatalogueArticle) selectedObject()).typeEtat().equals(FinderTypeEtat.typeEtatAnnule(ec))) {
				app.showInfoDialog("Cet article est archiv\u00E9, vous ne pouvez pas le modifier !");
				return;
			}
			if (CatalogueArticleAddUpdate.sharedInstance().open((EOCatalogueArticle) selectedObject())) {
				updateData();
			}
		}

		protected void onDelete() {
			if (selectedObject() != null) {
				EOCatalogueArticle cat = (EOCatalogueArticle) selectedObject();
				if (cat.typeEtat().equals(FinderTypeEtat.typeEtatAnnule(ec))) {
					if (app.showConfirmationDialog("Attention", "Cet article est archiv\u00E9. Souhaitez-vous le r\u00E9activer?", "Oui", "Non")) {
						try {
							((EOCatalogueArticle) selectedObject()).setTypeEtatRelationship(FinderTypeEtat.typeEtatValide(ec));
							ec.saveChanges();
							reloadData();
						}
						catch (Exception e) {
							app.showErrorDialog(e);
						}
					}
				}
				else {
					if (app.showConfirmationDialog("Attention", "Archiver cet article ?", "Oui", "Non")) {
						try {
							((EOCatalogueArticle) selectedObject()).setTypeEtatRelationship(FinderTypeEtat.typeEtatAnnule(ec));
							ec.saveChanges();
							reloadData();
						}
						catch (Exception e) {
							app.showErrorDialog(e);
						}
					}
				}
			}
		}

		protected void onSelectionChanged() {
		}

		private void buildPopup() {
			final JCheckBoxMenuItem menuItem = new JCheckBoxMenuItem("Afficher les articles archiv\u00E9s");
			menuItem.setSelected(displayArchives);
			menuItem.addItemListener(new MyPopupItemListener());
			JPopupMenu popup = new JPopupMenu();
			popup.add(menuItem);
			getTablePanel().getTable().setPopup(popup);
		}

		private final class MyPopupItemListener implements ItemListener {
			public void itemStateChanged(ItemEvent e) {
				displayArchives = ((JCheckBoxMenuItem) e.getItem()).isSelected();
				reloadData();
			}
		}

		private class DataTableCellRenderer extends ZEOTableCellRenderer {
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

				if (displayArchives) {
					EOCatalogueArticle catalogueArticle = (EOCatalogueArticle) getTablePanel().getObjectAtRow(row);
					if (catalogueArticle.typeEtat().equals(FinderTypeEtat.typeEtatAnnule(ec))) {
						label.setForeground(Color.gray);
					}
					else {
						if (isSelected) {
							label.setForeground(table.getSelectionForeground());
						}
						else {
							label.setForeground(table.getForeground());
						}
					}
				}
				return label;
			}
		}

	}

	private void initGUI() {
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		setPreferredSize(WINDOW_DIMENSION);

		bottomTablesPanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true, catalogueArticleTablePanel, catalogueArticleORTablePanel);
		bottomTablesPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		bottomTablesPanel.setDividerSize(5);
		tablesPanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true, catalogueTablePanel, bottomTablesPanel);
		tablesPanel.setBorder(BorderFactory.createEmptyBorder());
		tablesPanel.setDividerSize(5);

		add(buildSearchBar(), BorderLayout.NORTH);
		add(tablesPanel, BorderLayout.CENTER);
		// getContentPane().add(buildBottomPanel(), BorderLayout.PAGE_END);

		initInputMap();

	}

	private final JPanel buildSearchBar() {
		JPanel p = new JPanel(new BorderLayout());
		p.setBorder(BorderFactory.createLineBorder(Color.gray, 1));
		JPanel p1 = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
		p1.add(UIUtilities.labeledComponent("Num\u00E9ro", tfCatNumero, new ActionSearchCatNumero()));
		p1.add(Box.createHorizontalStrut(5));
		p1.add(UIUtilities.labeledComponent("Lib / Fournisseur", tfCatLibelle, null));
		p1.add(Box.createHorizontalStrut(5));
		p1.add(UIUtilities.labeledComponent("Fournisseur", cbFournisUlr, null));
		p.add(p1, BorderLayout.LINE_START);

		p.add(new JPanel(), BorderLayout.CENTER);

		// les catalogues ne d\u00E9pendent pas de l'exercice
		// JPanel p2 = new JPanel(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		// tfExercice.setViewOnly();
		// tfExercice.setHorizontalAlignment(SwingConstants.CENTER);
		// p2.add(UIUtilities.labeledComponent("Exercice", tfExercice, null));
		// p.add(p2, BorderLayout.LINE_END);

		JPanel finalPanel = new JPanel(new BorderLayout());
		finalPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 5, 20));
		finalPanel.add(p, BorderLayout.CENTER);
		return finalPanel;
	}

	// private final JPanel buildBottomPanel() {
	// JPanel p = new JPanel();
	// p.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
	// p.setLayout(new BoxLayout(p, BoxLayout.LINE_AXIS));
	// p.add(new JButton(actionClose));
	// p.add(Box.createHorizontalGlue());
	// return p;
	// }

	private void initInputMap() {
		getActionMap().put("ESCAPE", actionClose);
		getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"ESCAPE");
		getActionMap().put("F5", new ActionUpdate());
		getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0), "F5");
	}

	private class ActionClose extends AbstractAction {
		public ActionClose() {
			super("Fermer");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CLOSE_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onClose();
			}
		}
	}

	private class ActionSearchCatNumero extends AbstractAction {
		public ActionSearchCatNumero() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_SEARCH_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onSearchCatNumero();
			}
		}
	}

	private class ActionUpdate extends AbstractAction {
		public ActionUpdate() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_REFRESH_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Refresh");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				update();
			}
		}
	}

	private class SearchCatNumeroDocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent arg0) {
		}

		public void insertUpdate(DocumentEvent arg0) {
		}

		public void removeUpdate(DocumentEvent arg0) {
			if (tfCatNumero.getNumber() == null) {
				updateFiltering();
			}
		}
	}

	private class SearchCatLibelleDocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent arg0) {
			tfCatNumero.setText(null);
			updateFiltering();
		}

		public void insertUpdate(DocumentEvent arg0) {
			tfCatNumero.setText(null);
			updateFiltering();
		}

		public void removeUpdate(DocumentEvent arg0) {
			tfCatNumero.setText(null);
			updateFiltering();
		}
	}

	private class CbActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			updateFiltering();
		}
	}

	private class LocalWindowListener implements WindowListener {
		public void windowActivated(WindowEvent arg0) {
		}

		public void windowClosed(WindowEvent arg0) {
		}

		public void windowClosing(WindowEvent arg0) {
			actionClose.actionPerformed(null);
		}

		public void windowDeactivated(WindowEvent arg0) {
		}

		public void windowDeiconified(WindowEvent arg0) {
		}

		public void windowIconified(WindowEvent arg0) {
		}

		public void windowOpened(WindowEvent arg0) {
		}
	}

	public void setWaitCursor(boolean bool) {
		app.superviseur().setWaitCursor(this, bool);
	}



}
