/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.util.TimeZone;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.tools.ToolsCocktailLogs;
import org.cocktail.kava.client.metier.EOFacturePapier;
import org.cocktail.kava.client.metier.EOGestion;
import org.cocktail.kava.client.metier.EOPrestation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSTimeZone;

public class ServerProxy {

	private static final String CLIENT_SIDE_REQUEST_VERSION_RAW = "clientSideRequestVersionRaw";
	private static final String CLIENT_SIDE_REQUEST_PUSH_VERSION_CLIENT = "clientSideRequestPushVersionClient";
	private static final String SESSION_STR = "session";

	public static Boolean isDevelopmentMode(EOEditingContext ec) {
		return (Boolean) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
					"clientSideRequestIsDevelopmentMode", null, null, false);
	}

	public static String getMailDevisFr(EOEditingContext ec, EOPrestation prestation) throws Exception {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestGetMailDevisFr", new Class[] {
					EOEnterpriseObject.class
				}, new Object[] {
					prestation
				}, false);
	}

	public static String getMailDemandeValidationDevisFr(EOEditingContext ec, EOPrestation prestation) throws Exception {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore())
				.invokeRemoteMethodWithKeyPath(ec, SESSION_STR, "clientSideRequestGetMailDemandeValidationDevisFr",
						new Class[] {
							EOEnterpriseObject.class
						}, new Object[] {
							prestation
						}, false);
	}

	public static String getMailDevisEn(EOEditingContext ec, EOPrestation prestation) throws Exception {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestGetMailDevisEn", new Class[] {
					EOEnterpriseObject.class
				}, new Object[] {
					prestation
				}, false);
	}

	public static String getMailFactureFr(EOEditingContext ec, EOFacturePapier facture) throws Exception {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestGetMailFactureFr", new Class[] {
					EOEnterpriseObject.class
				}, new Object[] {
					facture
				}, false);
	}

	public static String getMailFactureEn(EOEditingContext ec, EOFacturePapier facture) throws Exception {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestGetMailFactureEn", new Class[] {
					EOEnterpriseObject.class
				}, new Object[] {
					facture
				}, false);
	}

	// impressions
	// Devis
	public static NSData printDevis(EOEditingContext ec, EOPrestation prestation, NSDictionary metadata) throws Exception {
		return (NSData) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestPrintDevis", new Class[] {
						EOEnterpriseObject.class, NSDictionary.class
				},
				new Object[] {
						prestation, metadata
				}, false);
	}

	// FacturePapier
	public static NSData printFacturePapier(EOEditingContext ec, EOFacturePapier facturePapier, NSDictionary metadata) throws Exception {
		return (NSData) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestPrintFacturePapier", new Class[] {
						EOEnterpriseObject.class, NSDictionary.class
				}, new Object[] {
						facturePapier, metadata
				}, false);
	}

	public static NSData printFacturePapierAnglais(EOEditingContext ec, EOFacturePapier facturePapier, NSDictionary metadata) throws Exception {
		return (NSData) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestPrintFacturePapierAnglais", new Class[] {
						EOEnterpriseObject.class, NSDictionary.class
				}, new Object[] {
						facturePapier, metadata
				}, false);
	}

	public static boolean checkMaquetteExists(EOEditingContext ec, String maquetteId) {
		return ((Boolean) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestCheckMaquetteExists", new Class[] {
					String.class
				}, new Object[] {
					maquetteId
				}, false)).booleanValue();
	}

	public static String serverAppliId(EOEditingContext ec) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestAppliId", null, null, false);
	}

	public static String serverAppliVersion(EOEditingContext ec) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestAppliVersion", new Class[] {}, new Object[] {}, false);
	}

	public static String serverCopyright(EOEditingContext ec) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestCopyright", new Class[] {}, new Object[] {}, false);
	}

	public static String serverAppliBdVersion(EOEditingContext ec) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestAppliBdVersion", new Class[] {}, new Object[] {}, false);
	}

	public static String serverMinAppliBdVersion(EOEditingContext ec) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestMinAppliBdVersion", new Class[] {}, new Object[] {}, false);
	}

	public static String serverBdConnexionName(EOEditingContext ec) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestBdConnexionName", new Class[] {}, new Object[] {}, false);
	}

	public static String serverConfigFileContent(EOEditingContext ec) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestConfigFileContent", new Class[] {}, new Object[] {}, false);
	}

	public static String serverInitLog(EOEditingContext ec) {
		return ToolsCocktailLogs.logsServeur().toString();
	}

	public static String serverOutLog(EOEditingContext ec) {
		return ToolsCocktailLogs.logsServeur().toString();
	}

	public static String serverErrLog(EOEditingContext ec) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestErrLog", new Class[] {}, new Object[] {}, false);
	}

	public static NSTimeZone serverDefaultNSTimeZone(EOEditingContext ec) {
		return (NSTimeZone) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestDefaultNSTimeZone", new Class[] {}, new Object[] {}, false);
	}

	public static TimeZone serverDefaultTimeZone(EOEditingContext ec) {
		return (TimeZone) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestDefaultTimeZone", new Class[] {}, new Object[] {}, false);
	}

	public static boolean serverRequestsAuthentication(EOEditingContext ec) {
		return ((Boolean) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestRequestsAuthentication", null, null, false)).booleanValue();
	}

	public static void serverSetFwkCktlWebApp(EOEditingContext ec, String userLogin) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, "clientSideRequestSetFwkCktlWebApp",
				new Class[] {
					String.class
				}, new Object[] {
					userLogin
				}, false);
	}

	/**
	 * Recupere le parametre paramKey, en cherchant dans l'ordre: la table Parametres, le .config, la table GrhumParametres
	 *
	 * @param ec
	 * @param paramKey
	 * @return La valeur du parametre trouve (String)
	 */
	public static String getParam(EOEditingContext ec, String paramKey) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestGetParam", new Class[] {
					String.class
				}, new Object[] {
					paramKey
				}, false);
	}

	/**
	 * Recupere le(s) parametre(s) paramKey, en cherchant dans l'ordre: la table Parametres, le .config, la table GrhumParametres
	 *
	 * @param ec
	 * @param paramKey
	 * @return La (ou les) valeur(s) du parametre trouve (NSArray, vide si rien trouve mais jamais null)
	 */
	public static NSArray getParams(EOEditingContext ec, String paramKey) {
		return (NSArray) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestGetParams", new Class[] {
					String.class
				}, new Object[] {
					paramKey
				}, false);
	}

	public static boolean getParamBoolean(EOEditingContext ec, String paramKey) {
		return ((Boolean) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestGetParamBoolean", new Class[] {
					String.class
				}, new Object[] {
					paramKey
				}, false)).booleanValue();
	}

	public static boolean getParamBoolean(EOEditingContext ec, String paramKey, boolean defaultValueWhenNotFound) {
		return ((Boolean) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestGetParamBoolean", new Class[] {
						String.class, Boolean.class
				}, new Object[] {
						paramKey,
						Boolean.valueOf(defaultValueWhenNotFound)
				}, false)).booleanValue();
	}

	public static void serverReset(EOEditingContext ec) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, "clientSideRequestServerReset",
				new Class[] {}, new Object[] {}, false);
	}

	public static void resetAppParametres(EOEditingContext ec) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, "clientSideRequestResetAppParametres",
				new Class[] {}, new Object[] {}, false);
	}

	public static NSDictionary getUserAuthentication(EOEditingContext ec, String userName, String password, Boolean isCrypted) {
		return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestGetUserAuthentication", new Class[] {
						String.class, String.class, Boolean.class
				}, new Object[] {
						userName,
						password, isCrypted
				}, false);
	}

	public static NSData readResourceFile(EOEditingContext ec, String resourceFileName) {
		return (NSData) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestReadResourceFile", new Class[] {
					String.class
				}, new Object[] {
					resourceFileName
				}, false);
	}

	public static void sendMail(EOEditingContext ec, String mailFrom, String mailTo, String mailCC, String mailSubject, String mailBody,
			String filename, NSData filedata) throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, "clientSideRequestSendMail",
				new Class[] {
						String.class, String.class, String.class, String.class, String.class, String.class, NSData.class
				}, new Object[] {
						mailFrom, mailTo, mailCC, mailSubject, mailBody, filename, filedata
				}, false);
	}

	public static void sendMailHtml(EOEditingContext ec, String mailFrom, String mailTo, String mailCC, String mailSubject, String mailBody,
			String filename, NSData filedata) throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, "clientSideRequestSendMailHtml",
				new Class[] {
						String.class, String.class, String.class, String.class, String.class, String.class, NSData.class
				}, new Object[] {
						mailFrom, mailTo, mailCC, mailSubject, mailBody, filename, filedata
				}, false);
	}

	// reports jasper
	public static NSData printJasperReport(EOEditingContext ec, String idReport, String sqlQuery, NSDictionary parametres,
			String customJasperId, Boolean printIfEmpty) throws Exception {
		return (NSData) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestPrintJasperReport",
				new Class[] {
						String.class, String.class, NSDictionary.class, String.class, Boolean.class
				}, new Object[] {
						idReport, sqlQuery,
						parametres, customJasperId, printIfEmpty
				}, false);
	}

	public static Number getNumerotation(EOEditingContext ec, EOExercice exercice, EOGestion gestion, String tnuEntite) throws Exception {
		return (Number) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR,
				"clientSideRequestGetNumerotation", new Class[] {
						EOEnterpriseObject.class, EOEnterpriseObject.class, String.class
				},
				new Object[] {
						exercice, gestion, tnuEntite
				}, false);
	}

	public static String serverVersionRaw(EOEditingContext ec) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, SESSION_STR, CLIENT_SIDE_REQUEST_VERSION_RAW, new Class[] {}, new Object[] {}, false);
	}

	/**
	 * Envoie un n° de version vers le serveur. Celui-ci renvoit une exception si la version n'est pas la même que la sienne.
	 *
	 * @param ec
	 * @param version (en raw)
	 * @throws Exception
	 */
	public static void serverPushVersionRaw(EOEditingContext ec, String version) throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, SESSION_STR, CLIENT_SIDE_REQUEST_PUSH_VERSION_CLIENT, new Class[] {
					String.class
				}, new Object[] {
					version
				}, false);
	}

}