/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.text.NumberFormat;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.text.DefaultHighlighter;

import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kava.client.finder.FinderFacturePapier;
import org.cocktail.kava.client.finder.FinderRecette;
import org.cocktail.kava.client.finder.FinderTypeApplication;
import org.cocktail.kava.client.finder.FinderTypeEtat;
import org.cocktail.kava.client.metier.EOFacture;
import org.cocktail.kava.client.metier.EOFacturePapier;
import org.cocktail.kava.client.metier.EOFonction;
import org.cocktail.kava.client.metier.EORecette;
import org.cocktail.kava.client.metier.EOTypeApplication;
import org.cocktail.kava.client.metier.EOUtilisateur;
import org.cocktail.kava.client.procedures.Api;
import org.cocktail.pie.client.interfaces.FinderRecetteRechercheAvance;
import org.cocktail.pieFwk.common.exception.EntityInitializationException;

import app.client.configuration.ConfigurationCouleur;
import app.client.reports.ReportFactoryClient;
import app.client.tools.Constants;
import app.client.ui.UIUtilities;
import app.client.ui.ZEOComboBox;
import app.client.ui.ZNumberField;
import app.client.ui.ZTextField;
import app.client.ui.table.ZEOTableCellRenderer;
import app.client.ui.table.ZEOTableModelColumn;
import app.client.ui.table.ZExtendedTablePanel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class FactureRecette extends JPanel implements IPieTab {


	protected ApplicationClient app;
	protected EOEditingContext ec;

	private static FactureRecette sharedInstance;

	private ConfigurationCouleur configurationCouleur;

	private ZNumberField tfRecNumero;
	private ZTextField tfRecLib, tfExercice;
	private ZNumberField tfMontant;
	private ZEOComboBox cbTypeApplication;
	private JCheckBox checkBoxRecettesTitrees;
	private Action actionCheckRecettesTitrees;

	private FactureRecetteTablePanel factureRecetteTablePanel;

	private Action actionClose;
	private Action actionPrint, actionLegend;
	private Action actionRechercheAvance;
	private FinderRecetteRechercheAvance monFinderRecetteRechercheAvance;


	private JComboBox listMaxResult;
	private Action actionSrch;
	public Boolean atLeastOnSearchHasBeenMade = Boolean.FALSE;

	public FactureRecette() {
		super();

		app = (ApplicationClient) EOApplication.sharedApplication();
		ec = app.editingContext();

		this.configurationCouleur = ConfigurationCouleur.instance();

		actionClose = new ActionClose();
		actionPrint = new ActionPrint();
		actionLegend = new ActionLegend();
		actionRechercheAvance = new ActionRechercheAvance();
		actionSrch = new ActionSrch();

		NumberFormat integerFormat = NumberFormat.getIntegerInstance();
		integerFormat.setGroupingUsed(false);
		tfRecNumero = new ZNumberField(5, integerFormat);
		//tfRecNumero.getDocument().addDocumentListener(new SearchRecNumeroDocumentListener());
		tfRecLib = new ZTextField(10);
		//tfRecLib.getDocument().addDocumentListener(new SearchRecLibOrMontantDocumentListener());
		tfMontant = new ZNumberField(5, Constants.FORMAT_DECIMAL_DISPLAY);
		//tfMontant.getDocument().addDocumentListener(new SearchRecLibOrMontantDocumentListener());
		tfExercice = new ZTextField(4);
		cbTypeApplication = new ZEOComboBox(new NSArray(new Object[] { FinderTypeApplication.typeApplicationRecette(ec),
				FinderTypeApplication.typeApplicationPrestationExterne(ec), FinderTypeApplication.typeApplicationPrestationInterne(ec) }),
				EOTypeApplication.TYAP_LIBELLE_KEY, "Tous", null, null, 120);
		actionCheckRecettesTitrees = new ActionCheckRecettesTitrees();
		checkBoxRecettesTitrees = new JCheckBox(actionCheckRecettesTitrees);

		MyActionListener defaultActionListener = new MyActionListener(actionSrch);
		tfRecNumero.addActionListener(defaultActionListener);
		tfRecLib.addActionListener(defaultActionListener);
		tfMontant.addActionListener(defaultActionListener);

		factureRecetteTablePanel = new FactureRecetteTablePanel();
		factureRecetteTablePanel.setEditable(true);
		listMaxResult = new JComboBox(new String[] {
				"25", "50", "100", "Tous"
		});

		initGUI();
	}

	public static FactureRecette sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new FactureRecette();
		}
		return sharedInstance;
	}

	/**
	 * Permet eventuellement de configurer au lancement l'interface selon des
	 * prefs utilisateur.
	 * @param utilisateur
	 */
	public void init(org.cocktail.application.client.eof.EOUtilisateur utilisateur) {
	}

	private class MyActionListener implements ActionListener {
		private final Action action;

		/**
	         *
	         */
		public MyActionListener(Action a) {
			super();
			action = a;
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(final ActionEvent e) {
			action.actionPerformed(e);
		}

	}

	public void update() {
		// TODO autoriser tout ca plus tard meme sur un exercice clos (pour
		// consultation / traitement des annees precedentes)
		factureRecetteTablePanel.setEditable(true);
		EOExercice exercice = app.superviseur().currentExercice();
		tfExercice.setText(exercice.exeExercice().toString());
		if (exercice.exeStatFac().equalsIgnoreCase(EOExercice.EXERCICE_OUVERT)) {
			if (!app.canUseFonction(EOFonction.DROIT_FACTURER, exercice)) {
				factureRecetteTablePanel.setEditable(false);
			}
		}
		if (exercice.exeStatRec().equalsIgnoreCase(EOExercice.EXERCICE_OUVERT)) {
			if (!app.canUseFonction(EOFonction.DROIT_RECETTER, exercice)) {
				factureRecetteTablePanel.setEditable(false);
			}
		}
		if (exercice.exeStatFac().equalsIgnoreCase(EOExercice.EXERCICE_RESTREINT)) {
			if (!app.canUseFonction(EOFonction.DROIT_FACTURER_PERIODE_BLOCAGE, exercice)) {
				factureRecetteTablePanel.setEditable(false);
			}
		}
		if (exercice.exeStatRec().equalsIgnoreCase(EOExercice.EXERCICE_RESTREINT)) {
			if (!app.canUseFonction(EOFonction.DROIT_RECETTER_PERIODE_BLOCAGE, exercice)) {
				factureRecetteTablePanel.setEditable(false);
			}
		}
		if (!exercice.exeStatFac().equalsIgnoreCase(EOExercice.EXERCICE_OUVERT) && !exercice.exeStatFac().equalsIgnoreCase(EOExercice.EXERCICE_RESTREINT))
			factureRecetteTablePanel.setEditable(false);
		if (!exercice.exeStatRec().equalsIgnoreCase(EOExercice.EXERCICE_OUVERT) && !exercice.exeStatRec().equalsIgnoreCase(EOExercice.EXERCICE_RESTREINT))
			factureRecetteTablePanel.setEditable(false);

		updateData();
	}

	public void updateData() {
		if (atLeastOnSearchHasBeenMade) {
			long start = System.currentTimeMillis();
			app.superviseur().setWaitCursor(this, true);
			Integer fetchLimit = null;
			try {
				fetchLimit = Integer.valueOf((String) listMaxResult.getSelectedItem());
			} catch (NumberFormatException e) {
				//on ne fai rien, on laisse fetchlimit à null
			}
			factureRecetteTablePanel.reloadData(updateFilteringQualifier(), fetchLimit);
			updateInterfaceEnabling();
		setWaitCursor(false);
		System.out.println("Chargement recettes : " + (System.currentTimeMillis() - start) + "ms");
		}

		//		
		//		
		//		long start = System.currentTimeMillis();
		//		setWaitCursor(true);
		//		factureRecetteTablePanel.reloadData(updateFilteringQualifier());
		//		setWaitCursor(false);
		//		updateInterfaceEnabling();
		//		System.out.println("Chargement recettes : " + (System.currentTimeMillis() - start) + "ms");

	}

	private void updateInterfaceEnabling() {
		EORecette selected = (EORecette) factureRecetteTablePanel.selectedObject();

		if (selected == null) {
			actionPrint.setEnabled(false);
			actionPrint.putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer le titre provisoire");
			factureRecetteTablePanel.actionUpdate.setEnabled(false);
			factureRecetteTablePanel.actionDelete.setEnabled(false);
			return;
		}

		if (selected.isTitred()) {
			actionPrint.setEnabled(false);
			if (selected.recetteReduction() != null) {
				actionPrint.putValue(AbstractAction.SHORT_DESCRIPTION, "R\u00E9duction d\u00E9j\u00E0 titr\u00E9e, pas d'impression de r\u00E9duction de titre provisoire !");
			} else {
				actionPrint.putValue(AbstractAction.SHORT_DESCRIPTION, "Recette d\u00E9j\u00E9 titr\u00E9e, pas d'impression de titre provisoire !");
			}
			factureRecetteTablePanel.actionDelete.setEnabled(false);
		} else {
			actionPrint.setEnabled(true);
			if (selected.recetteReduction() != null) {
				actionPrint.putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer la r\u00E9duction de titre provisoire");
			} else {
				actionPrint.putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer le titre provisoire");
			}
		}

		// cas d'une r\u00E9duction
		if (selected.recetteReduction() != null) {
			// factureRecetteTablePanel.actionUpdate.setEnabled(false);
			factureRecetteTablePanel.actionDelete.putValue(AbstractAction.SHORT_DESCRIPTION, null);
			factureRecetteTablePanel.actionDelete.putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_16_LIGHTENED);
		} else {
			// cas de la recette...
			// si vis\u00E9e, bouton suppression devient r\u00E9duction, sinon reste
			// suppression
			if (selected.isVised()) {
				factureRecetteTablePanel.actionDelete.putValue(AbstractAction.SHORT_DESCRIPTION, "R\u00E9duire cette recette (r\u00E9duction de titre)");
				factureRecetteTablePanel.actionDelete.putValue(AbstractAction.SMALL_ICON, Constants.ICON_REDUIRE_16);
				factureRecetteTablePanel.actionDelete.setEnabled(true);
			} else {
				factureRecetteTablePanel.actionDelete.putValue(AbstractAction.SHORT_DESCRIPTION, null);
				factureRecetteTablePanel.actionDelete.putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_16_LIGHTENED);
			}
		}

		// handle organ permission
		if (selected != null && selected.facture() != null && selected.facture().organ() != null
				&& !selected.facture().organ().hasPermission(app.appUserInfo().utilisateur())) {
			actionPrint.setEnabled(false);
			factureRecetteTablePanel.actionDelete.setEnabled(false);
		}

	}

	public boolean canQuit() {
		return FactureRecetteAddUpdate.sharedInstance().canQuit();
	}

	public EORecette getSelected() {
		return (EORecette) factureRecetteTablePanel.selectedObject();
	}

	public void setSelected(EORecette recette) {
		factureRecetteTablePanel.setSelectedObject(recette);
	}

	public void setSelected(NSArray recettes) {
		factureRecetteTablePanel.setSelectedObjects(recettes);
	}

	private NSArray updateFilteringQualifier() {
		NSMutableArray andQuals = new NSMutableArray();

		if (tfRecNumero.getNumber() != null) {
			andQuals.addObject(EOQualifier.qualifierWithQualifierFormat(
					EORecette.REC_NUMERO_KEY + " = %@", new NSArray(tfRecNumero.getNumber())));
		}
		if (tfRecLib.getText() != null) {
			NSMutableArray quals = new NSMutableArray();
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(
					EORecette.REC_LIB_KEY + " caseInsensitiveLike %@", new NSArray("*" + tfRecLib.getText() + "*")));
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(
					EORecette.FACTURE_PERSONNE_PERS_NOM_PRENOM_KEY + " caseInsensitiveLike %@",
					new NSArray("*" + tfRecLib.getText() + "*")));
			andQuals.addObject(new EOOrQualifier(quals));
		}
		if (tfMontant.getNumber() != null) {
			andQuals.addObject(EOQualifier.qualifierWithQualifierFormat(
					EORecette.REC_TTC_SAISIE_KEY + " = %@",	new NSArray(tfMontant.getBigDecimal())));
		}
		if (cbTypeApplication.getSelectedEOObject() != null) {
			andQuals.addObject(EOQualifier.qualifierWithQualifierFormat(
					EORecette.FACTURE_TYPE_APPLICATION_TYAP_LIBELLE_KEY + " = %@",
					new NSArray(((EOTypeApplication) cbTypeApplication.getSelectedEOObject()).tyapLibelle())));
		}
		if (!checkBoxRecettesTitrees.isSelected()) {
			andQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EORecette.TIT_ID_KEY + " = nil", null));
		}
		NSArray advSearchQualifiers = getMonFinderRecetteRechercheAvance().getResultat();
		if (advSearchQualifiers != null && advSearchQualifiers.count() > 0) {
			andQuals.addObjectsFromArray(advSearchQualifiers);
		}
		return andQuals;
	}

	private void onClose() {
		this.hide();
	}

	public FinderRecetteRechercheAvance getMonFinderRecetteRechercheAvance() {
		if(monFinderRecetteRechercheAvance==null)
		{
			monFinderRecetteRechercheAvance = new FinderRecetteRechercheAvance(app,0,0,300,200);
			monFinderRecetteRechercheAvance.setParent(this);
		}
		return monFinderRecetteRechercheAvance;
	}

	private void onSearchRecNumero() {
		tfRecLib.setText(null);
		tfMontant.setText(null);
		cbTypeApplication.setSelectedEOObject(null);
		updateData();
	}

	private void onPrint() {
		if (factureRecetteTablePanel.selectedObject() == null) {
			return;
		}
		EORecette recette = (EORecette) factureRecetteTablePanel.selectedObject();
		// si d\u00E9j\u00E9 un titre de recette, NAN!
		if (recette.isTitred()) {
			app.showErrorDialog("Cette recette a d\u00E9j\u00E0 \u00E9t\u00E9 titr\u00E9e, on n'imprime plus de titre provisoire !");
			return;
		}
		if (recette.recetteReduction() != null) {
			ReportFactoryClient.printReduction(recette, null);
		} else {
			ReportFactoryClient.printRecette(recette, null);
		}
	}

	private void onReduction() {
		if (factureRecetteTablePanel.selectedObject() == null) {
			return;
		}
		EORecette recette = (EORecette) factureRecetteTablePanel.selectedObject();
		if (recette.facture() != null
				&& recette.facture().typeApplication().equals(
						FinderTypeApplication.typeApplicationPrestationInterne(ec))) {
			app.showErrorDialog("Cette recette est une prestation interne, pas de r\u00E9duction de prestation interne pour l'instant !");
			return;
		}
		// si pas encore titr\u00E9e, NAN!
		if (!recette.isTitred()) {
			app.showErrorDialog("Cette recette n'a pas encore \u00E9t\u00E9 titr\u00E9e, pas de r\u00E9duction possible (la supprimer directement si vraiment on y tient )!");
			return;
		}
		// si pas encore vis\u00E9e, NAN!
		if (!recette.isVised()) {
			app.showErrorDialog(
					"Cette recette n'a pas encore \u00E9t\u00E9 vis\u00E9e, pas de r\u00E9duction possible !");
			return;
		}
		try {
			if (FactureRecetteReductionAddUpdate.sharedInstance().openNew(recette)) {
				//				factureRecetteTablePanel.reloadData();
				factureRecetteTablePanel.updateData();
				factureRecetteTablePanel.setSelectedObject(
						FactureRecetteReductionAddUpdate.sharedInstance().getLastOpened());
			}
		} catch (EntityInitializationException eie) {
			app.showErrorDialog(eie);
		}
	}

	private void onLegend() {
		JFrame frame = new JFrame("L\u00E9gende des couleurs");
		Box contentPanel = Box.createVerticalBox();

		JPanel panel1 = new JPanel(new FlowLayout(FlowLayout.LEFT, 20, 0));
		panel1.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		JTextField label1 = new JTextField("Texte", 6);
		label1.setBorder(null);
		label1.setEditable(false);
		label1.setBackground(null);
		label1.setForeground(configurationCouleur.recetteNonTitreeForeground(app));
		panel1.add(label1);
		panel1.add(new JLabel("Recette NON titr\u00E9e."));
		contentPanel.add(panel1);

		JPanel panel2 = new JPanel(new FlowLayout(FlowLayout.LEFT, 20, 0));
		panel2.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		JTextField label2 = new JTextField("Texte", 6);
		label2.setBorder(null);
		label2.setEditable(false);
		label2.setBackground(null);
		label2.setForeground(configurationCouleur.recetteTitreeForeground(app));
		panel2.add(label2);
		panel2.add(new JLabel("Recette titr\u00E9e."));
		contentPanel.add(panel2);

		JPanel panel3 = new JPanel(new FlowLayout(FlowLayout.LEFT, 20, 0));
		panel3.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		JTextField label3 = new JTextField("Texte", 6);
		label3.setBorder(null);
		label3.setEditable(false);
		label3.setBackground(null);
		label3.setForeground(configurationCouleur.reductionNonTitreeForeground(app));
		panel3.add(label3);
		panel3.add(new JLabel("R\u00E9duction NON titr\u00E9e."));
		contentPanel.add(panel3);

		JPanel panel4 = new JPanel(new FlowLayout(FlowLayout.LEFT, 20, 0));
		panel4.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		JTextField label4 = new JTextField("Texte", 6);
		label4.setBorder(null);
		label4.setEditable(false);
		label4.setBackground(null);
		label4.setForeground(configurationCouleur.reductionTitreeForeground(app));
		panel4.add(label4);
		panel4.add(new JLabel("R\u00E9duction titr\u00E9e."));
		contentPanel.add(panel4);

		frame.setContentPane(contentPanel);
		frame.setLocationRelativeTo(app.superviseur());
		frame.setResizable(false);
		frame.validate();
		frame.pack();
		frame.show();
	}

	private class FactureRecetteTablePanel extends ZExtendedTablePanel {

		public FactureRecetteTablePanel() {
			super(null);

			ZEOTableModelColumn col1 = new ZEOTableModelColumn(getDG(), EORecette.REC_NUMERO_KEY, "No", 40);
			col1.setColumnClass(Integer.class);
			col1.setAlignment(SwingConstants.LEFT);
			addCol(col1);

			ZEOTableModelColumn colFact = new ZEOTableModelColumn(getDG(), EORecette.FACTURE_KEY + "." + EOFacture.FACTURE_PAPIER_KEY + "." + EOFacturePapier.FAP_NUMERO_AS_INT_KEY, "Facture", 40);
			colFact.setColumnClass(Integer.class);
			colFact.setAlignment(SwingConstants.LEFT);
			addCol(colFact);

			ZEOTableModelColumn col1bis = new ZEOTableModelColumn(getDG(), EORecette.TIT_NUMERO_KEY, "Titre", 40);
			col1bis.setColumnClass(Integer.class);
			col1bis.setAlignment(SwingConstants.LEFT);
			addCol(col1bis);

			ZEOTableModelColumn col2 = new ZEOTableModelColumn(getDG(), EORecette.REC_DATE_SAISIE_KEY, "Date", 50, Constants.FORMAT_DATESHORT);
			col2.setColumnClass(NSTimestamp.class);
			addCol(col2);

			addCol(new ZEOTableModelColumn(getDG(), EORecette.REC_LIB_KEY, "Lib", 150));
			addCol(new ZEOTableModelColumn(getDG(), EORecette.FACTURE_PERSONNE_PERS_NOM_PRENOM_KEY, "Client", 150));
			addCol(new ZEOTableModelColumn(getDG(), EORecette.FACTURE_TYPE_APPLICATION_TYAP_LIBELLE_KEY, "Type recette", 100));
			ZEOTableModelColumn col4 = new ZEOTableModelColumn(getDG(), EORecette.REC_HT_SAISIE_KEY, "HT", 50, app.getCurrencyFormatDisplay());
			col4.setColumnClass(BigDecimal.class);
			col4.setAlignment(SwingConstants.RIGHT);
			addCol(col4);

			ZEOTableModelColumn col5 = new ZEOTableModelColumn(getDG(), EORecette.REC_TTC_SAISIE_KEY, "TTC", 50, app.getCurrencyFormatDisplay());
			col5.setColumnClass(BigDecimal.class);
			col5.setAlignment(SwingConstants.RIGHT);
			addCol(col5);

			initGUI();

			getTablePanel().getTable().setMyRenderer(new DataTableCellRenderer());
		}


		public void reloadData(NSArray quals, Integer fetchLimit) {
			EOUtilisateur utilisateur = app.appUserInfo().utilisateur();
			if (app.canUseFonction(EOFonction.DROIT_VOIR_TOUTES_LES_RECETTES, null)) {
				utilisateur = null;
			}
			NSMutableArray args = new NSMutableArray();

			args.addObject(EOQualifier.qualifierWithQualifierFormat(
					EORecette.TYPE_ETAT_KEY + " = %@", new NSArray(FinderTypeEtat.typeEtatValide(ec))));
			args.addObject(EOQualifier.qualifierWithQualifierFormat(
					EORecette.EXERCICE_KEY + " = %@", new NSArray(app.superviseur().currentExercice())));
			if (utilisateur != null) {
				args.addObject(EOQualifier.qualifierWithQualifierFormat(
						EORecette.UTILISATEUR_KEY + " = %@", new NSArray(utilisateur)));
			}
			if (quals != null && quals.count() > 0) {
				args.addObjectsFromArray(quals);
			}
			setObjectArray(FinderRecette.findWithLimit(ec, new EOAndQualifier(args), fetchLimit));
		}

		protected void onAdd() {
			try {
				app.superviseur().setWaitCursor(this, true);
				if (FactureRecetteAddUpdate.sharedInstance().openNew(app.superviseur().currentExercice())) {
					EORecette fp = FactureRecetteAddUpdate.sharedInstance().getLastOpened();
					ajouterInDg(fp, 0);
				}
			} catch (EntityInitializationException eie) {
				app.superviseur().setWaitCursor(this, false);
				app.showErrorDialog(eie);
			}
 finally {
				app.superviseur().setWaitCursor(this, false);
			}
		}

		protected void onUpdate() {
			if (selectedObject() == null) {
				return;
			}
			try {
				app.superviseur().setWaitCursor(this, true);
				if (((EORecette) selectedObject()).recetteReduction() != null) {
					if (FactureRecetteReductionAddUpdate.sharedInstance().open((EORecette) selectedObject())) {
						fireSelectedTableRowUpdated();
					}
				}
				else {
					if (FactureRecetteAddUpdate.sharedInstance().open((EORecette) selectedObject())) {
						fireSelectedTableRowUpdated();
					}
				}
			} catch (Exception e) {
				app.superviseur().setWaitCursor(this, false);
				app.showErrorDialog(e);
			}
			app.superviseur().setWaitCursor(this, false);
		}

		protected void onDelete() {
			if (selectedObject() == null) {
				return;
			}
			// cas d'une recette a r\u00E9duire...
			if (((EORecette) selectedObject()).recetteReduction() == null && ((EORecette) selectedObject()).isVised()) {
				onReduction();
			}
			// cas de la suppression (d'une recette ou d'une r\u00E9duction)...
			else {
				String lib = "recette";
				if (((EORecette) selectedObject()).recetteReduction() != null) {
					lib = "r\u00E9duction";
				}
				if (((EORecette) selectedObject()).isTitred()) {
					app.showErrorDialog("Cette " + lib + " est d\u00E9j\u00E0 titr\u00E9e, interdit de la supprimer !");
					return;
				}
				EORecette recette = (EORecette) selectedObject();
				if (recette.recetteReduction() == null) {
					if (!recette.facture().typeApplication().equals(FinderTypeApplication.typeApplicationRecette(ec))) {
						if (recette.facture().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(ec))
								|| recette.facture().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationExterne(ec))) {
							app
									.showErrorDialog("Cette "
											+ lib
											+ " est issue d'une facture, interdit de la supprimer ici !\nSi vous tenez \u00E0 la supprimer, utilisez l'onglet Facture pour d\u00E9valider la facture c\u00F4t\u00E9 prestataire, cela annulera la recette correspondante.");
							return;
						}
						app.showErrorDialog("Cette " + lib
								+ " est issue d'une autre application (n'est pas de type application Recette), interdit de la supprimer ici !");
						return;
					}
				}
				if (app.showConfirmationDialog("ATTENTION", "Supprimer DEFINITIVEMENT cette " + lib + "??", "OUI", "NON")) {
					try {
						if (recette.recetteReduction() == null) {
							NSArray facturesPapier = FinderFacturePapier.find(ec, recette.facture());
							Api.delFactureRecette(ec, recette, app.appUserInfo().utilisateur());
							if (facturesPapier != null && facturesPapier.count() > 0) {
								ec.invalidateObjectsWithGlobalIDs(ec._globalIDsForObjects(facturesPapier));
								FacturePapier.sharedInstance().updateData();
							}
						} else {
							Api.delReduction(ec, recette, app.appUserInfo().utilisateur());
						}
						//FactureRecette.this.updateData();
						deleteSelectionInDg();
					} catch (Exception e) {
						app.showErrorDialog(e);
					}
				}
			}
		}

		protected void onSelectionChanged() {
			updateInterfaceEnabling();
		}

		private class DataTableCellRenderer extends ZEOTableCellRenderer {
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

				EORecette recette = (EORecette) getTablePanel().getObjectAtRow(row);
				if (recette.recetteReduction() == null) {
					if (recette.isTitred()) {
						label.setForeground(configurationCouleur.recetteTitreeForeground(app));
					} else {
						label.setForeground(configurationCouleur.recetteNonTitreeForeground(app));
					}
				} else {
					if (recette.isTitred()) {
						label.setForeground(configurationCouleur.reductionTitreeForeground(app));
					} else {
						label.setForeground(configurationCouleur.reductionNonTitreeForeground(app));
					}
				}

				if (isSelected) {
					label.setBackground(table.getSelectionBackground());
				} else {
					label.setBackground(table.getBackground());
				}
				return label;
			}
		}

	}

	private void initGUI() {
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));

		JPanel centerTablePanel = new JPanel(new BorderLayout());
		centerTablePanel.setBorder(BorderFactory.createEmptyBorder());
		centerTablePanel.add(factureRecetteTablePanel, BorderLayout.CENTER);
		centerTablePanel.add(buildToolBar(), BorderLayout.LINE_END);

		add(buildSearchBar(), BorderLayout.NORTH);
		add(centerTablePanel, BorderLayout.CENTER);

		initInputMap();
	}

	private final JPanel buildSearchBar() {
		JPanel p = new JPanel(new BorderLayout());
		p.setBorder(BorderFactory.createLineBorder(Color.gray, 1));

		JPanel p1 = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
		p1.add(UIUtilities.labeledComponent("Num\u00E9ro", tfRecNumero, null));
		p1.add(Box.createHorizontalStrut(5));
		p1.add(UIUtilities.labeledComponent("Lib/Client", tfRecLib, null));
		p1.add(Box.createHorizontalStrut(5));
		p1.add(UIUtilities.labeledComponent("Montant (ttc)", tfMontant, null));
		p1.add(Box.createHorizontalStrut(5));
		p1.add(UIUtilities.labeledComponent("Type de recette", cbTypeApplication, null));
		p1.add(Box.createHorizontalStrut(5));
		checkBoxRecettesTitrees.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);
		checkBoxRecettesTitrees.setVerticalTextPosition(SwingConstants.TOP);
		checkBoxRecettesTitrees.setHorizontalTextPosition(SwingConstants.CENTER);
		checkBoxRecettesTitrees.setIconTextGap(0);
		checkBoxRecettesTitrees.setFocusPainted(false);
		checkBoxRecettesTitrees.setBorderPaintedFlat(true);
		p1.add(UIUtilities.labeledComponent(null, checkBoxRecettesTitrees, null));
		p1.add(UIUtilities.labeledComponent("Nombre de résultats", listMaxResult, null));
		p1.add(UIUtilities.labeledComponent(" ", makeButtonLib(actionSrch), null));
		p.add(p1, BorderLayout.LINE_START);

		JPanel p2 = new JPanel(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		p2.add(UIUtilities.labeledComponent("Recherche Avancée",makeButton(actionRechercheAvance), null));
		p2.add(Box.createHorizontalStrut(5));
		tfExercice.setViewOnly();
		tfExercice.setHorizontalAlignment(SwingConstants.CENTER);
		tfExercice.setHighlighter(new DefaultHighlighter());
		p2.add(UIUtilities.labeledComponent("Exercice", tfExercice, null));
		p.add(p2, BorderLayout.LINE_END);

		JPanel finalPanel = new JPanel(new BorderLayout());
		finalPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 2, 2));
		finalPanel.add(p, BorderLayout.CENTER);
		return finalPanel;
	}

	private final JComponent buildToolBar() {
		JToolBar toolBarPanel = new JToolBar("Recettes Toolbar", JToolBar.VERTICAL);
		toolBarPanel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		toolBarPanel.add(makeButton(actionPrint));
		toolBarPanel.addSeparator(new Dimension(20, 20));
		toolBarPanel.addSeparator(new Dimension(20, 20));
		toolBarPanel.add(makeButton(actionLegend));

		return toolBarPanel;
	}



	// private final JPanel buildBottomPanel() {
	// JPanel p = new JPanel();
	// p.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
	// p.setLayout(new BoxLayout(p, BoxLayout.LINE_AXIS));
	// p.add(new JButton(actionClose));
	// p.add(Box.createHorizontalGlue());
	// return p;
	// }

	private void initInputMap() {
		getActionMap().put("ESCAPE", actionClose);
		getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "ESCAPE");
		getActionMap().put("F5", new ActionUpdate());
		getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0), "F5");
	}

	private class ActionClose extends AbstractAction {
		public ActionClose() {
			super("Fermer");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CLOSE_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onClose();
			}
		}
	}

	private class ActionRechercheAvance extends AbstractAction {

		public ActionRechercheAvance() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.getIconForName(IconeCocktail.RECHERCHER));
		}

		public void actionPerformed(ActionEvent e) {
			getMonFinderRecetteRechercheAvance().setOpen(true);
			getMonFinderRecetteRechercheAvance().afficherFenetre();
		}
	}

	private class ActionSearchRecNumero extends AbstractAction {
		public ActionSearchRecNumero() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_SEARCH_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onSearchRecNumero();
			}
		}
	}

	private class ActionUpdate extends AbstractAction {
		public ActionUpdate() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_REFRESH_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Refresh");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				update();
			}
		}
	}

	private class ActionPrint extends AbstractAction {
		public ActionPrint() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_PRINT_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer le titre provisoire");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onPrint();
			}
		}
	}

	private class ActionLegend extends AbstractAction {
		public ActionLegend() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_COLORS_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "L\u00E9gende des couleurs");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onLegend();
			}
		}
	}

	private class ActionCheckRecettesTitrees extends AbstractAction {
		public ActionCheckRecettesTitrees() {
			super("Recettes titr\u00E9es");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Voir les recettes d\u00E9j\u00E0 titr\u00E9es");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
//				updateFiltering();
				getMonFinderRecetteRechercheAvance().setEnableSearchRecette(checkBoxRecettesTitrees.isSelected());
				//updateData();
			}
		}
	}



	public void setWaitCursor(boolean bool) {
		app.superviseur().setWaitCursor(this, bool);
	}


	public void afficheRechercheAvanceeIfOpen() {
		if (isRechercheAvanceeOpen()) {
			getMonFinderRecetteRechercheAvance().afficherFenetre();
		}
	}

	public boolean isRechercheAvanceeOpen() {
		if (monFinderRecetteRechercheAvance == null) {
			return false;
		}
		return getMonFinderRecetteRechercheAvance().isOpen();
	}

	public void masquerRechercheAvancee() {
		if (monFinderRecetteRechercheAvance == null) {
			return;
		}
		getMonFinderRecetteRechercheAvance().masquerFenetre();
	}

	private JButton makeButton(Action a) {
		return UIUtilities.makeButton(a);
	}

	private JButton makeButtonLib(Action a) {
		return UIUtilities.makeButtonLib(a);
	}

	private class ActionSrch extends AbstractAction {
		public ActionSrch() {
			super("Rechercher");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_SEARCH_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Lancer la recherche");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				atLeastOnSearchHasBeenMade = true;
				updateData();
			}
		}
	}

	public void clearFilters() {
		tfRecNumero.setText(null);
		tfRecLib.setText(null);
		tfMontant.setText(null);
		checkBoxRecettesTitrees.setSelected(false);
		cbTypeApplication.setSelectedEOObject(null);
	}

}
