/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.math.BigDecimal;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.kava.client.factory.FactoryFacture;
import org.cocktail.kava.client.factory.FactoryRecette;
import org.cocktail.kava.client.factory.FactoryRecettePapier;
import org.cocktail.kava.client.finder.FinderModeRecouvrement;
import org.cocktail.kava.client.finder.FinderRecette;
import org.cocktail.kava.client.finder.FinderTypeApplication;
import org.cocktail.kava.client.finder.FinderTypeCreditRec;
import org.cocktail.kava.client.metier.EOAdresse;
import org.cocktail.kava.client.metier.EOFacture;
import org.cocktail.kava.client.metier.EOModeRecouvrement;
import org.cocktail.kava.client.metier.EOPlanComptable;
import org.cocktail.kava.client.metier.EORecette;
import org.cocktail.kava.client.metier.EORecetteCtrlPlanco;
import org.cocktail.kava.client.metier.EORecettePapier;
import org.cocktail.kava.client.metier.EORecettePapierAdrClient;
import org.cocktail.kava.client.procedures.Api;
import org.cocktail.kava.client.service.RecettePapierService;
import org.cocktail.pieFwk.common.exception.EntityInitializationException;

import app.client.select.ClientSelect;
import app.client.select.OrganSelect;
import app.client.select.RibfourUlrSelect;
import app.client.select.validator.FournisseurEtatValideValidator;
import app.client.select.validator.SelectValidatorSeverity;
import app.client.tools.Constants;
import app.client.ui.UIUtilities;
import app.client.ui.ZEOComboBox;
import app.client.ui.ZNumberField;
import app.client.ui.ZTextField;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

public class FactureRecetteReductionAddUpdate extends JDialog implements IRecetteCtrlPlanco {

	private static final String WINDOW_TITLE = "R\u00E9duction";

	protected ApplicationClient app;
	protected EOEditingContext ec;

	private static FactureRecetteReductionAddUpdate sharedInstance;

	private EORecette eoRecette;

	private Action actionValid, actionCancel;
	private Action actionShowRequired, actionUnshowRequired;
	private Action actionPersonneSelect, actionOrganSelect;
	private Action actionCheckModeOuhlala;
	private Action actionRibfourUlrSelect, actionRibfourUlrDelete;

	private ZNumberField tfRecNumero;
	private ZNumberField tfRecNumeroInitial;
	private ZTextField tfRecLibInitial, tfExercice, tfRecLib;
	private ZNumberField tfRecHtSaisie, tfRecTtcSaisie;
	private ZNumberField tfNbPiece;

	private ZTextField tfPersonne, tfRibClient, tfOrgan;
	private ZEOComboBox cbTypeCreditRec, cbModeRecouvrement;
	private CbActionListener cbActionListener;

	private JCheckBox checkBoxModeOuhlala;

	private RecetteCtrlActionPanel recetteCtrlAction;
	private RecetteCtrlAnalytiquePanel recetteCtrlAnalytique;
	private RecetteCtrlConventionPanel recetteCtrlConvention;
	private RecetteCtrlPlancoPanel recetteCtrlPlanco;
	private RecetteCtrlPlancoTvaPanel recetteCtrlPlancoTva;
	private RecetteCtrlPlancoCtpPanel recetteCtrlPlancoCtp;

	private JButton btValid, btCancel;

	private ClientSelect personneSelect;
	private OrganSelect organSelect;
	private RibfourUlrSelect ribfourUlrSelect;

	private Number recIdUpdating;
	private boolean updating;
	private boolean result;

	private RecettePapierService recettePapierService = RecettePapierService.instance();

	public FactureRecetteReductionAddUpdate() {
		super((JFrame) null, WINDOW_TITLE, true);

		app = (ApplicationClient) EOApplication.sharedApplication();
		ec = app.editingContext();

		setGlassPane(new BusyGlassPanel());
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new LocalWindowListener());

		actionValid = new ActionValid();
		actionCancel = new ActionCancel();
		actionShowRequired = new ActionShowRequired();
		actionUnshowRequired = new ActionUnshowRequired();
		actionPersonneSelect = new ActionPersonneSelect();
		actionRibfourUlrSelect = new ActionRibfourUlrSelect();
		actionRibfourUlrDelete = new ActionRibfourUlrDelete();
		actionOrganSelect = new ActionOrganSelect();
		actionCheckModeOuhlala = new ActionCheckModeOuhlala();

		tfRecNumero = new ZNumberField(4);
		tfRecNumero.setViewOnly();
		tfRecNumeroInitial = new ZNumberField(6);
		tfRecNumeroInitial.setViewOnly();
		tfRecLibInitial = new ZTextField(40);
		tfRecLibInitial.setViewOnly();
		tfExercice = new ZTextField(4);
		tfRecLib = new ZTextField(50);
		tfRecHtSaisie = new ZNumberField(10, app.getCurrencyFormatEdit());
		tfRecTtcSaisie = new ZNumberField(10, app.getCurrencyFormatEdit());
		tfNbPiece = new ZNumberField(8, Constants.FORMAT_INTEGER);

		tfPersonne = new ZTextField(40);
		tfPersonne.setViewOnly();
		tfRibClient = new ZTextField(30);
		tfRibClient.setViewOnly();
		tfOrgan = new ZTextField(20);
		tfOrgan.setViewOnly();

		cbActionListener = new CbActionListener();
		cbTypeCreditRec = new ZEOComboBox(new NSArray(), EOTypeCredit.LIB_KEY, null, null, null, 150);
		cbTypeCreditRec.addActionListener(cbActionListener);
		cbModeRecouvrement = new ZEOComboBox(new NSArray(), EOModeRecouvrement.LIB_KEY, null, null, null, 220);
		cbModeRecouvrement.addActionListener(cbActionListener);

		checkBoxModeOuhlala = new JCheckBox(actionCheckModeOuhlala);

		recetteCtrlAction = new RecetteCtrlActionPanel();
		recetteCtrlAnalytique = new RecetteCtrlAnalytiquePanel();
		recetteCtrlConvention = new RecetteCtrlConventionPanel();
		recetteCtrlPlanco = new RecetteCtrlPlancoPanel(this);
		recetteCtrlPlancoTva = new RecetteCtrlPlancoTvaPanel();
		recetteCtrlPlancoCtp = new RecetteCtrlPlancoCtpPanel();

		btCancel = new JButton(actionCancel);
		btValid = new JButton(actionValid);

		initGUI();

		tfRecLib.setEditable(true);

		tfRecHtSaisie.setEditable(true);
		tfRecTtcSaisie.setEditable(true);
		tfNbPiece.setEditable(true);

		actionCheckModeOuhlala.setEnabled(true);

		actionPersonneSelect.setEnabled(false);
		actionRibfourUlrSelect.setEnabled(false);
		actionRibfourUlrDelete.setEnabled(false);
		actionOrganSelect.setEnabled(false);
		cbTypeCreditRec.setEnabled(false);
		cbModeRecouvrement.setEnabled(false);

		recetteCtrlAction.setEditable(false);
		recetteCtrlAnalytique.setEditable(false);
		recetteCtrlConvention.setEditable(false);
		recetteCtrlPlanco.setEditable(false);
		recetteCtrlPlancoTva.setEditable(false);
		recetteCtrlPlancoCtp.setEditable(false);

		recetteCtrlAction.setColumnsEditable(true);
		recetteCtrlAnalytique.setColumnsEditable(true);
		recetteCtrlConvention.setColumnsEditable(true);
		recetteCtrlPlanco.setColumnsEditable(true);
		recetteCtrlPlancoTva.setColumnsEditable(true);
		recetteCtrlPlancoCtp.setColumnsEditable(true);

		checkBoxModeOuhlala.getAction().actionPerformed(null);
	}

	public static FactureRecetteReductionAddUpdate sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new FactureRecetteReductionAddUpdate();
		}
		return sharedInstance;
	}

	public boolean openNew(EORecette recette) throws EntityInitializationException {
		if (recette == null) {
			System.err.println("[FactureRecetteReductionAddUpdate:open(EORecette)] Aucune recette pass\u00E9e pour ouvrir en cr\u00E9ation !");
			throw new EntityInitializationException("Aucune recette passée pour ouvrir en création.");
		}

		updating = false;
		recIdUpdating = null;

		setTitle(WINDOW_TITLE + " - Cr\u00E9ation");

		EOFacture eoFacture = FactoryFacture.newObject(ec, recette);
		eoFacture.setUtilisateurRelationship(app.appUserInfo().utilisateur());

		eoRecette = FactoryRecette.newObject(ec, eoFacture);
		eoRecette.setRecetteReductionRelationship(recette);
		eoRecette.setUtilisateurRelationship(app.appUserInfo().utilisateur());
		eoRecette.setRecLib(recette.recLib());

		EORecettePapier rpp = FactoryRecettePapier.newObject(ec, eoRecette);
		rpp.setUtilisateurRelationship(app.appUserInfo().utilisateur());
		rpp.setRppNbPiece(recette.recettePapier().rppNbPiece());
		eoRecette.setRecettePapierRelationship(rpp);

		eoRecette.setRecHtSaisie(recette.recHtSaisie());
		eoRecette.setRecTtcSaisie(recette.recTtcSaisie());
		eoRecette.setRecTvaSaisie(recette.recTvaSaisie());

		eoRecette.setRecetteCtrlActions(new NSMutableArray(recette.recetteCtrlActions()));
		eoRecette.setRecetteCtrlAnalytiques(new NSMutableArray(recette.recetteCtrlAnalytiques()));
		eoRecette.setRecetteCtrlConventions(new NSMutableArray(recette.recetteCtrlConventions()));
		eoRecette.setRecetteCtrlPlancos(new NSMutableArray(recette.recetteCtrlPlancos()));

		// si au moins un des ctrl a plus d'une ligne, on passe en mode ouhlala
		if (!checkBoxModeOuhlala.isSelected() && recette.hasMultipleCtrls()) {
			checkBoxModeOuhlala.setSelected(true);
			checkBoxModeOuhlala.getAction().actionPerformed(null);
		}

		updateData();

		return open();
	}

	public boolean open(EORecette recette) {
		if (recette == null) {
			System.err.println("[FactureRecetteReductionAddUpdate:open(EORecette)] Aucune recette pass\u00E9e pour ouvrir en modif !");
			return false;
		}

		// v\u00E9rifie si on peut afficher cette recette sans perte de donn\u00E9es...
		if (recette.hasMultipleCtrls()) {
			if ((recette.hasMultipleCtrlActions() && !recetteCtrlAction.acceptMultiple())
					|| (recette.hasMultipleCtrlAnalytiques() && !recetteCtrlAnalytique.acceptMultiple())
					|| (recette.hasMultipleCtrlConventions() && !recetteCtrlConvention.acceptMultiple())
					|| (recette.hasMultipleCtrlPlancos() && !recetteCtrlPlanco.acceptMultiple())
					|| (recette.hasMultipleCtrlPlancoTvas() && !recetteCtrlPlancoTva.acceptMultiple())
					|| (recette.hasMultipleCtrlPlancoCtps() && !recetteCtrlPlancoCtp.acceptMultiple())) {
				app.showErrorDialog("Oups ! Impossible d'ouvrir cette recette sans perte de donn\u00E9es, donc on ne touche pas !");
				System.err.println("Plusieurs enregistrements existent pour un recetteCtrl qui est bloqu\u00E9 en mode single...");
				System.err.println("Ca n'arrive que si la configuration de l'application (ou du moteur sql) a chang\u00E9 depuis la cr\u00E9ation.");
				return false;
			}
		}
		app.superviseur().setWaitCursor(this, true);
		updating = true;

		// on r\u00E9cup\u00E9re la cl\u00E9 tout de suite, passke apr\u00E9s s'il y a des relations nouvelles, donc qui n'existent pas
		// c\u00E9t\u00E9 serveur (les RecetteCtrl...), on ne peut plus passer l'objet recette au serveur pour r\u00E9cup\u00E9rer sa cl\u00E9
		NSDictionary dicoForPrimaryKeys = org.cocktail.kava.client.ServerProxy.primaryKeyForObject(ec, recette);
		recIdUpdating = (Number) dicoForPrimaryKeys.objectForKey(EORecette.PRIMARY_KEY_KEY);

		setTitle(WINDOW_TITLE + " - Modification");

		this.eoRecette = recette;
		this.eoRecette.setUtilisateurRelationship(app.appUserInfo().utilisateur());

		// si au moins un des ctrl a plus d'une ligne, on passe en mode ouhlala
		if (!checkBoxModeOuhlala.isSelected() && recette.hasMultipleCtrls()) {
			checkBoxModeOuhlala.setSelected(true);
			checkBoxModeOuhlala.getAction().actionPerformed(null);
		}

		updateData();

		return open();
	}

	public boolean canQuit() {
		if (isShowing()) {
			actionCancel.actionPerformed(null);
		}
		return !isShowing();
	}

	public EORecette getLastOpened() {
		return eoRecette;
	}

	// met \u00E9 jour les planco tva et ctp par d\u00E9faut pour le nouveau planco s\u00E9lectionn\u00E9
	public void didAddNewPlanco(EOPlanComptable planComptable, EOExercice exercice) {
	}

	public void onSelectionChanged(EORecetteCtrlPlanco ctrlPlanco) {
		recetteCtrlPlancoTva.setCurrentObject(ctrlPlanco);
		recetteCtrlPlancoCtp.setCurrentObject(ctrlPlanco);
		recetteCtrlPlancoTva.setEditable(false);
		recetteCtrlPlancoTva.setColumnsEditable(true);
		recetteCtrlPlancoCtp.setEditable(false);
		recetteCtrlPlancoCtp.setColumnsEditable(true);
	}

	private boolean open() {
		app.superviseur().setWaitCursor(this, true);
		result = false;
		int screenWidth = (int) this.getGraphicsConfiguration().getBounds().getWidth();
		int screenHeight = (int) this.getGraphicsConfiguration().getBounds().getHeight();
		this.setLocation((screenWidth / 2) - ((int) this.getSize().getWidth() / 2),
				((screenHeight / 2) - ((int) this.getSize().getHeight() / 2)));

		app.superviseur().setWaitCursor(this, false);
		setVisible(true);
		return result;
	}

	private void onValid() {
		try {
			app.superviseur().setWaitCursor(this, true);
			eoRecette.setRecLib(tfRecLib.getText());
			eoRecette.recettePapier().setRppNbPiece(Integer.valueOf(tfNbPiece.getNumber().intValue()));
			NSDictionary returnedDico = null;
			if (eoRecette.updateTauxProrata()) {
				ec.saveChanges();
			}
			if (updating) {
				// on est en modification...
				// TODO
				// Api.updReduction(ec, eoRecette, recIdUpdating);
			} else {
				// on est en cr\u00E9ation...
				returnedDico = Api.insReductionAdresse(ec, eoRecette);
			}
			ec.revert();
			result = true;
			if (returnedDico != null) {
				eoRecette = FinderRecette.findByPrimaryKey(ec, returnedDico.valueForKey("010aRecId"));
			}
			app.superviseur().setWaitCursor(this, false);
			setVisible(false);
		} catch (Exception e) {
			app.superviseur().setWaitCursor(this, false);
			app.showErrorDialog(e);
		}
 finally {
			app.superviseur().setWaitCursor(this, false);
		}
	}

	private void onCancel() {
		if (app.showConfirmationDialog("ATTENTION", "Annuler???", "OUI!", "NON")) {
			ec.revert();
			result = false;
			hide();
		}
	}

	private void onCheckModeOuhlala() {
		boolean modeOuhlala = checkBoxModeOuhlala.isSelected();

		// si on cherche \u00E9 passer en mode simple alors qu'il y a des multiples...
		if (!modeOuhlala && eoRecette != null && eoRecette.hasMultipleCtrls()) {
			// on v\u00E9rifie s'il y a un multiple sur un ctrl qui pourrait passer en single... si oui, NAN ! :-)
			if ((eoRecette.hasMultipleCtrlActions() && recetteCtrlAction.acceptSingle())
					|| (eoRecette.hasMultipleCtrlAnalytiques() && recetteCtrlAnalytique.acceptSingle())
					|| (eoRecette.hasMultipleCtrlConventions() && recetteCtrlConvention.acceptSingle())
					|| (eoRecette.hasMultipleCtrlPlancos() && recetteCtrlPlanco.acceptSingle())
					|| (eoRecette.hasMultipleCtrlPlancoTvas() && recetteCtrlPlancoTva.acceptSingle())
					|| (eoRecette.hasMultipleCtrlPlancoCtps() && recetteCtrlPlancoCtp.acceptSingle())) {
				app.showInfoDialog("Nan! Vous ne pouvez pas passer en mode simple, des informations seraient perdues !");
				checkBoxModeOuhlala.setSelected(true);
				return;
			}
		}

		recetteCtrlAction.setMultiple(modeOuhlala);
		recetteCtrlAnalytique.setMultiple(modeOuhlala);
		recetteCtrlConvention.setMultiple(modeOuhlala);
		recetteCtrlPlanco.setMultiple(modeOuhlala);
		recetteCtrlPlancoTva.setMultiple(modeOuhlala);
		recetteCtrlPlancoCtp.setMultiple(modeOuhlala);

		recetteCtrlPlancoTva.setEditable(false);
		recetteCtrlPlancoTva.setColumnsEditable(true);
		recetteCtrlPlancoCtp.setEditable(false);
		recetteCtrlPlancoCtp.setColumnsEditable(true);
	}

	private void updateData() {
		if (eoRecette == null) {
			return;
		}
		tfRecNumero.setNumber(eoRecette.recNumero());
		tfExercice.setText(eoRecette.exercice().exeExercice().toString());
		if (eoRecette.recetteReduction() != null) {
			tfRecNumeroInitial.setNumber(eoRecette.recetteReduction().recNumero());
			tfRecLibInitial.setText(eoRecette.recetteReduction().recLib());
		} else {
			tfRecNumeroInitial.setNumber(null);
			tfRecLibInitial.setText(null);
		}
		tfRecLib.setText(eoRecette.recLib());
		tfRecHtSaisie.setNumber(eoRecette.recHtSaisie());
		tfRecTtcSaisie.setNumber(eoRecette.recTtcSaisie());
		if (eoRecette.recettePapier() != null) {
			tfNbPiece.setNumber(eoRecette.recettePapier().rppNbPiece());
		}

		if (eoRecette.facture().personne() != null) {
			StringBuilder txtBuilder = new StringBuilder(eoRecette.facture().personne().persNomPrenom());
			EOAdresse currentAdrReduction = null;
			if (eoRecette.recettePapier() != null) {
				EORecettePapierAdrClient curRecPapierAdrClient =
						eoRecette.recettePapier().currentRecPapierAdresseClient();
				if (curRecPapierAdrClient != null && curRecPapierAdrClient.toAdresse() != null) {
					currentAdrReduction = curRecPapierAdrClient.toAdresse();
				}
			}
			if (currentAdrReduction == null
					&& eoRecette.recetteReduction() != null
					&& eoRecette.recetteReduction().recettePapier() != null) {
				currentAdrReduction =
						recettePapierService.currentAdresseClient(ec, eoRecette.recetteReduction().recettePapier());
			}
			if (currentAdrReduction != null) {
				txtBuilder.append(" ").append(currentAdrReduction.toInlineString());
			}
			tfPersonne.setText(txtBuilder.toString());
		} else {
			tfPersonne.setText(null);
		}

		if (eoRecette.recettePapier() != null && eoRecette.recettePapier().ribfourUlr() != null) {
			tfRibClient.setText(eoRecette.recettePapier().ribfourUlr().cBanque() + " " + eoRecette.recettePapier().ribfourUlr().cGuichet() + " "
					+ eoRecette.recettePapier().ribfourUlr().noCompte() + " - " + eoRecette.recettePapier().ribfourUlr().ribTitco());
		} else {
			tfRibClient.setText(null);
		}

		if (eoRecette.facture().organ() != null) {
			tfOrgan.setText(eoRecette.facture().organ().orgLib());
		} else {
			tfOrgan.setText(null);
		}

		if (eoRecette.facture().typeCreditRec() != null) {
			cbTypeCreditRec.removeActionListener(cbActionListener);
			cbTypeCreditRec.setObjects(FinderTypeCreditRec.find(ec, eoRecette.exercice()));
			cbTypeCreditRec.setSelectedEOObject(eoRecette.facture().typeCreditRec());
			cbTypeCreditRec.addActionListener(cbActionListener);
		} else {
			cbTypeCreditRec.setObjects(FinderTypeCreditRec.find(ec, eoRecette.exercice()));
		}

		if (eoRecette.recettePapier() != null && eoRecette.recettePapier().modeRecouvrement() != null) {
			cbModeRecouvrement.removeActionListener(cbActionListener);
			if (eoRecette.facture() != null && eoRecette.facture().typeApplication() != null
					&& eoRecette.facture().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(ec))) {
				cbModeRecouvrement.setObjects(FinderModeRecouvrement.find(ec, eoRecette.exercice()));
			} else {
				cbModeRecouvrement.setObjects(FinderModeRecouvrement.findSansPrestationInterne(ec, eoRecette.exercice()));
			}
			cbModeRecouvrement.setSelectedEOObject(eoRecette.recettePapier().modeRecouvrement());
			cbModeRecouvrement.addActionListener(cbActionListener);
		}
		else {
			if (eoRecette.facture() != null && eoRecette.facture().typeApplication() != null
					&& eoRecette.facture().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(ec))) {
				cbModeRecouvrement.setObjects(FinderModeRecouvrement.find(ec, eoRecette.exercice()));
			}
			else {
				cbModeRecouvrement.setObjects(FinderModeRecouvrement.findSansPrestationInterne(ec, eoRecette.exercice()));
			}
		}

		recetteCtrlAction.setCurrentObject(eoRecette);
		recetteCtrlAnalytique.setCurrentObject(eoRecette);
		recetteCtrlConvention.setCurrentObject(eoRecette);
		recetteCtrlPlanco.setCurrentObject(eoRecette);

		recetteCtrlPlancoTva.setEditable(false);
		recetteCtrlPlancoTva.setColumnsEditable(true);
		recetteCtrlPlancoCtp.setEditable(false);
		recetteCtrlPlancoCtp.setColumnsEditable(true);

		updateInterfaceEnabling();
	}

	private void updateInterfaceEnabling() {
		if (eoRecette == null) {
			return;
		}

		boolean enabled = !updating;

		tfRecLib.setEditable(enabled);

		tfRecHtSaisie.setEnabled(enabled);
		tfRecTtcSaisie.setEnabled(enabled);
		tfNbPiece.setEnabled(enabled);

		// TODO virer ce bloc quand on aura fait l'update...
		if (updating) {
			// pas d'update pour l'instant, donc on enl\u00E9ve le bouton Valider (remplace par fermer)
			btCancel.setVisible(false);
			actionValid.putValue(AbstractAction.NAME, "Fermer");
			actionValid.putValue(AbstractAction.SMALL_ICON, Constants.ICON_CLOSE_16);
		}
		else {
			btCancel.setVisible(true);
			actionValid.putValue(AbstractAction.NAME, "Valider");
			actionValid.putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
		}
	}

	private void showRequired(boolean show) {
		tfRecHtSaisie.showRequired(show);
		tfRecTtcSaisie.showRequired(show);
		tfNbPiece.showRequired(show);
		tfPersonne.showRequired(show);
		tfOrgan.showRequired(show);
		cbTypeCreditRec.showRequired(show);
		cbModeRecouvrement.showRequired(show);
		recetteCtrlAction.showRequired(show);
		recetteCtrlPlanco.showRequired(show);
		recetteCtrlPlancoTva.showRequired(show);
		recetteCtrlPlancoCtp.showRequired(show);
	}

	private void onPersonneSelect() {
		if (personneSelect == null) {
			personneSelect = new RecetteAddUpdateClientSelect();
		}
		if (personneSelect.open(app.appUserInfo().utilisateur(), eoRecette.exercice(), null, true, true, true)) {
			eoRecette.facture().setPersonneRelationship(personneSelect.getSelected());
			eoRecette.facture().setFournisUlrRelationship(personneSelect.getSelectedFournisUlr());
			if (eoRecette.recettePapier() != null) {
				eoRecette.recettePapier().setPersonneRelationship(eoRecette.facture().personne());
				eoRecette.recettePapier().setFournisUlrRelationship(eoRecette.facture().fournisUlr());
			}
			if (eoRecette.facture().personne() != null) {
				StringBuilder txtBuilder = new StringBuilder(eoRecette.facture().personne().persNomPrenom());
				if (eoRecette.recettePapier() != null) {
					EORecettePapierAdrClient currentAdr = eoRecette.recettePapier().currentRecPapierAdresseClient();
					if (currentAdr != null) {
						txtBuilder.append(" ").append(currentAdr.toAdresse().toInlineString());
					}
				}
				tfPersonne.setText(txtBuilder.toString());
			}
			updateInterfaceEnabling();
		}
	}

	private void onRibfourUlrSelect() {
		if (ribfourUlrSelect == null) {
			ribfourUlrSelect = new RibfourUlrSelect();
		}
		if (ribfourUlrSelect.open(app.appUserInfo().utilisateur(), eoRecette.exercice(), eoRecette.facture().fournisUlr(), eoRecette
				.recettePapier().ribfourUlr())) {
			eoRecette.recettePapier().setRibfourUlrRelationship(ribfourUlrSelect.getSelected());
			if (eoRecette.recettePapier().ribfourUlr() != null) {
				tfRibClient.setText(eoRecette.recettePapier().ribfourUlr().cBanque() + " " + eoRecette.recettePapier().ribfourUlr().cGuichet()
						+ " " + eoRecette.recettePapier().ribfourUlr().noCompte() + " - " + eoRecette.recettePapier().ribfourUlr().ribTitco());
			}
			updateInterfaceEnabling();
		}
	}

	private void onRibfourUlrDelete() {
		eoRecette.recettePapier().setRibfourUlrRelationship(null);
		tfRibClient.setText(null);
		updateInterfaceEnabling();
	}

	private void onOrganSelect() {
		if (organSelect == null) {
			organSelect = new OrganSelect();
		}
		if (organSelect.open(app.appUserInfo().utilisateur(), eoRecette.exercice(), eoRecette.facture().organ(), null)) {
			eoRecette.facture().setOrganRelationship(organSelect.getSelected());
			if (eoRecette.facture().organ() != null) {
				tfOrgan.setText(eoRecette.facture().organ().orgLib());
			}
			updateInterfaceEnabling();
		}
	}

	private void initGUI() {
		JPanel contentPanel = new JPanel(new BorderLayout());
		contentPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanel);

		Border commonEmptyThinBorder = BorderFactory.createEmptyBorder(0, 3, 0, 3);

		// num\u00E9ro + exercice
		JPanel panelRecNumero = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel jlRecNumero = new JLabel("R\u00E9duction N\u00B0 ");
		jlRecNumero.setFont(jlRecNumero.getFont().deriveFont(Font.BOLD, 20));
		jlRecNumero.setForeground(Constants.COLOR_LABEL_FGD_NORMAL);
		panelRecNumero.add(jlRecNumero);
		tfRecNumero.setFont(tfRecNumero.getFont().deriveFont(Font.BOLD, 24));
		tfRecNumero.setHorizontalAlignment(SwingConstants.RIGHT);
		panelRecNumero.add(tfRecNumero);

		JPanel tempPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JLabel jlExercice = new JLabel("Exercice ");
		jlExercice.setFont(jlExercice.getFont().deriveFont(Font.BOLD, 16));
		jlExercice.setForeground(Constants.COLOR_LABEL_FGD_NORMAL);
		tempPanel.add(jlExercice);
		tfExercice.setFont(tfExercice.getFont().deriveFont(Font.BOLD, 18));
		tfExercice.setHorizontalAlignment(SwingConstants.CENTER);
		tfExercice.setViewOnly();
		tempPanel.add(tfExercice);
		JPanel panelExercice = new JPanel(new GridBagLayout());
		panelExercice.add(tempPanel, new GridBagConstraints());

		JPanel panelRecNumeroExercice = new JPanel(new BorderLayout());
		panelRecNumeroExercice.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		panelRecNumeroExercice.add(panelRecNumero, BorderLayout.LINE_START);
		panelRecNumeroExercice.add(new JPanel(), BorderLayout.CENTER);
		panelRecNumeroExercice.add(panelExercice, BorderLayout.LINE_END);

		// Recette initiale
		JPanel panelRecetteInitialeTemp = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panelRecetteInitialeTemp.setBorder(BorderFactory.createEmptyBorder());
		panelRecetteInitialeTemp.add(tfRecNumeroInitial);
		panelRecetteInitialeTemp.add(Box.createHorizontalStrut(10));
		panelRecetteInitialeTemp.add(tfRecLibInitial);
		JPanel panelRecetteInitiale = UIUtilities.labeledComponent("Recette initiale", panelRecetteInitialeTemp, null);

		// libell\u00E9
		JPanel panelRecLib = UIUtilities.labeledComponent("Libell\u00E9", tfRecLib, null);

		// montants
		MontantsFocusListener mfl = new MontantsFocusListener();
		JPanel panelMontants = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panelMontants.setBorder(BorderFactory.createEmptyBorder());
		tfRecHtSaisie.addFocusListener(mfl);
		tfRecTtcSaisie.addFocusListener(mfl);

		panelMontants.add(UIUtilities.labeledComponent("Montant HT", tfRecHtSaisie, null));
		panelMontants.add(Box.createHorizontalStrut(10));
		panelMontants.add(UIUtilities.labeledComponent("Montant TTC", tfRecTtcSaisie, null));
		panelMontants.add(Box.createHorizontalStrut(60));
		panelMontants.add(UIUtilities.labeledComponent("Nb de pi\u00E8ces", tfNbPiece, null));

		Box panelLibMontants = Box.createVerticalBox();
		panelLibMontants.setBorder(BorderFactory.createEmptyBorder());
		panelLibMontants.add(panelRecLib);
		panelLibMontants.add(panelMontants);

		// client
		Box panelClient = Box.createVerticalBox();
		panelClient.setBorder(BorderFactory.createTitledBorder(null, "Client", TitledBorder.DEFAULT_JUSTIFICATION,
				TitledBorder.DEFAULT_POSITION, null, Constants.COLOR_LABEL_FGD_LIGHT));
		panelClient.add(UIUtilities.labeledComponent(null, tfPersonne, null));
		panelClient.add(UIUtilities.labeledComponent("Rib client", tfRibClient, null, UIUtilities.DEFAULT_BOXED, UIUtilities.DEFAULT_ALIGNMENT));

		// ligne budg + type cred
		JPanel panelOrganTypCred = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panelOrganTypCred.setBorder(BorderFactory.createEmptyBorder());
		panelOrganTypCred.add(UIUtilities.labeledComponent("Ligne budg\u00E9taire", tfOrgan, null));
		panelOrganTypCred.add(Box.createHorizontalStrut(10));
		panelOrganTypCred.add(UIUtilities.labeledComponent("Type de cr\u00E9dit", cbTypeCreditRec, null));

		Box panelBudgetMor = Box.createVerticalBox();
		panelBudgetMor.setBorder(BorderFactory.createTitledBorder(null, null, TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION,
				null, Constants.COLOR_LABEL_FGD_LIGHT));
		panelBudgetMor.add(panelOrganTypCred);
		panelBudgetMor.add(UIUtilities.labeledComponent("Mode de recouvrement", cbModeRecouvrement, null));

		checkBoxModeOuhlala.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);
		checkBoxModeOuhlala.setVerticalTextPosition(SwingConstants.TOP);
		checkBoxModeOuhlala.setHorizontalTextPosition(SwingConstants.CENTER);
		checkBoxModeOuhlala.setIconTextGap(0);
		checkBoxModeOuhlala.setFocusPainted(false);
		checkBoxModeOuhlala.setBorderPaintedFlat(true);
		Box panelCheckBox = Box.createVerticalBox();
		panelCheckBox.setBorder(BorderFactory.createEmptyBorder());
		panelCheckBox.add(Box.createVerticalGlue());
		panelCheckBox.add(checkBoxModeOuhlala);

		JPanel panelBudgetCheckBox = new JPanel(new BorderLayout(0, 0));
		panelBudgetCheckBox.setBorder(BorderFactory.createEmptyBorder());
		panelBudgetCheckBox.add(panelBudgetMor, BorderLayout.LINE_START);
		panelBudgetCheckBox.add(new JPanel(), BorderLayout.CENTER);
		panelBudgetCheckBox.add(panelCheckBox, BorderLayout.LINE_END);

		// actions + canal
		JPanel panelCtrls = new JPanel(new GridLayout(1, 2, 10, 0));
		panelCtrls.setBorder(commonEmptyThinBorder);
		panelCtrls.add(recetteCtrlAction);
		panelCtrls.add(recetteCtrlAnalytique);

		// planco
		JPanel panelPlanco = new JPanel(new BorderLayout(0, 0));
		panelPlanco.setBorder(commonEmptyThinBorder);
		panelPlanco.add(recetteCtrlPlanco, BorderLayout.LINE_START);
		panelPlanco.add(new JPanel(), BorderLayout.CENTER);

		// planco tva + planco ctp
		JPanel panelPlancos = new JPanel(new GridLayout(1, 2, 10, 0));
		panelPlancos.setBorder(commonEmptyThinBorder);
		panelPlancos.add(recetteCtrlPlancoTva);
		panelPlancos.add(recetteCtrlPlancoCtp);

		// convention
		JPanel panelConvention = new JPanel(new BorderLayout(0, 0));
		panelConvention.setBorder(commonEmptyThinBorder);
		panelConvention.add(recetteCtrlConvention, BorderLayout.LINE_START);
		panelConvention.add(new JPanel(), BorderLayout.CENTER);

		Box avant = Box.createVerticalBox();
		avant.setBorder(BorderFactory.createEmptyBorder());
		avant.add(panelRecNumeroExercice);
		avant.add(Box.createVerticalStrut(10));
		avant.add(panelRecetteInitiale);
		avant.add(Box.createVerticalStrut(10));
		avant.add(panelLibMontants);
		avant.add(Box.createVerticalStrut(10));
		avant.add(panelClient);
		avant.add(Box.createVerticalStrut(10));
		avant.add(panelBudgetCheckBox);
		avant.add(Box.createVerticalStrut(10));

		Box panelResizable = Box.createVerticalBox();
		panelResizable.setBorder(BorderFactory.createEmptyBorder());
		panelResizable.add(panelCtrls);
		panelResizable.add(panelPlanco);
		panelResizable.add(panelPlancos);
		panelResizable.add(panelConvention);

		JPanel center = new JPanel(new BorderLayout());
		center.setBorder(BorderFactory.createEmptyBorder());
		center.add(avant, BorderLayout.NORTH);
		center.add(panelResizable, BorderLayout.CENTER);

		getContentPane().add(center, BorderLayout.CENTER);
		getContentPane().add(buildBottomPanel(), BorderLayout.SOUTH);

		initInputMap();

		this.validate();
		this.pack();
	}

	private final JPanel buildBottomPanel() {
		JPanel p = new JPanel();
		p.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.add(Box.createHorizontalGlue());
		p.add(btCancel);
		p.add(Box.createRigidArea(new Dimension(5, 0)));
		p.add(btValid);
		return p;
	}

	private void initInputMap() {
		((JComponent) getContentPane()).getActionMap().put("CTRL_Q", app.superviseur().mainMenu.actionQuit);
		((JComponent) getContentPane()).getActionMap().put("CTRL_S", actionValid);
		((JComponent) getContentPane()).getActionMap().put("F10", actionValid);
		((JComponent) getContentPane()).getActionMap().put("ESCAPE", actionCancel);
		((JComponent) getContentPane()).getActionMap().put("F8", actionShowRequired);
		((JComponent) getContentPane()).getActionMap().put("ALT_F8", actionUnshowRequired);
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
				KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK), "CTRL_Q");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
				KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK), "CTRL_S");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F10, 0), "F10");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"ESCAPE");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("F8"), "F8");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("released F8"), "ALT_F8");
	}

	private class CbActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JComboBox cb = (JComboBox) e.getSource();
			if (cb == cbTypeCreditRec && eoRecette != null && eoRecette.facture() != null) {
				eoRecette.facture().setTypeCreditRecRelationship((EOTypeCredit) cbTypeCreditRec.getSelectedEOObject());
			}
			if (cb == cbModeRecouvrement && eoRecette != null && eoRecette.recettePapier() != null) {
				eoRecette.recettePapier().setModeRecouvrementRelationship(
						(EOModeRecouvrement) cbModeRecouvrement.getSelectedEOObject());
				if (eoRecette.facture() != null) {
					eoRecette.facture().setModeRecouvrementRelationship(eoRecette.recettePapier().modeRecouvrement());
				}
			}
			updateInterfaceEnabling();
		}
	}

	private class MontantsFocusListener implements FocusListener {
		public void focusGained(FocusEvent e) {
		}

		public void focusLost(FocusEvent e) {
			if (e.getComponent() == tfRecHtSaisie) {
				BigDecimal bd = tfRecHtSaisie.getBigDecimal();
				eoRecette.setRecHtSaisie(bd);
				eoRecette.facture().setFacHtSaisie(bd);
				// mise a jour des montants s'il n'y a qu'une ligne
				updateMontants();
			}
			if (e.getComponent() == tfRecTtcSaisie) {
				BigDecimal bd = tfRecTtcSaisie.getBigDecimal();
				eoRecette.setRecTtcSaisie(bd);
				eoRecette.facture().setFacTtcSaisie(bd);
				// mise a jour des montants s'il n'y a qu'une ligne
				updateMontants();
			}
		}

		private void updateMontants() {
			recetteCtrlAction.updateMontants();
			recetteCtrlAnalytique.updateMontants();
			recetteCtrlConvention.updateMontants();
			recetteCtrlPlanco.updateMontants();
			recetteCtrlPlancoTva.updateMontant();
			recetteCtrlPlancoCtp.updateMontant();
		}
	}

	private class ActionValid extends AbstractAction {
		public ActionValid() {
			super("Valider");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onValid();
			}
		}
	}

	private class ActionCancel extends AbstractAction {
		public ActionCancel() {
			super("Annuler");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CANCEL_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onCancel();
			}
		}
	}

	private class ActionShowRequired extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				showRequired(true);
			}
		}
	}

	private class ActionUnshowRequired extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				showRequired(false);
			}
		}
	}

	private class ActionCheckModeOuhlala extends AbstractAction {
		public ActionCheckModeOuhlala() {
			super("Mode expert");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Permet de g\u00E9rer de multiples actions, imputations, codes analytiques, ...");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onCheckModeOuhlala();
			}
		}
	}

	private class ActionPersonneSelect extends AbstractAction {
		public ActionPersonneSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onPersonneSelect();
			}
		}
	}

	private class ActionRibfourUlrSelect extends AbstractAction {
		public ActionRibfourUlrSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onRibfourUlrSelect();
			}
		}
	}

	private class ActionRibfourUlrDelete extends AbstractAction {
		public ActionRibfourUlrDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onRibfourUlrDelete();
			}
		}
	}

	private class ActionOrganSelect extends AbstractAction {
		public ActionOrganSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onOrganSelect();
			}
		}
	}

	private class LocalWindowListener implements WindowListener {
		public void windowActivated(WindowEvent arg0) {
		}

		public void windowClosed(WindowEvent arg0) {
		}

		public void windowClosing(WindowEvent arg0) {
			actionCancel.actionPerformed(null);
		}

		public void windowDeactivated(WindowEvent arg0) {
		}

		public void windowDeiconified(WindowEvent arg0) {
		}

		public void windowIconified(WindowEvent arg0) {
		}

		public void windowOpened(WindowEvent arg0) {
		}
	}

	public void setWaitCursor(boolean bool) {
		if (bool) {
			this.getGlassPane().setVisible(true);
		}
		else {
			this.getGlassPane().setVisible(false);
		}
	}

	/**
	 * Permet de d\u00E9finir un panel qui sert a afficher un waitcursor et a interdire toute modif sur le panel.
	 *
	 * @author rprin
	 */
	public final class BusyGlassPanel extends JPanel {
		public final Color COLOR_WASH = new Color(64, 64, 64, 32);

		public BusyGlassPanel() {
			super.setOpaque(false);
			super.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

			// Suck up them events!!!
			super.addKeyListener((new KeyAdapter() {
			}));
			super.addMouseListener((new MouseAdapter() {
			}));
			super.addMouseMotionListener((new MouseMotionAdapter() {
			}));
		}

		public final void paintComponent(Graphics p_graphics) {
			Dimension l_size = super.getSize();

			// Wash the pane with translucent gray.
			p_graphics.setColor(COLOR_WASH);
			p_graphics.fillRect(0, 0, l_size.width, l_size.height);

			// Paint a grid of white/black dots.
			p_graphics.setColor(Color.white);
			for (int j = 3; j < l_size.height; j += 8) {
				for (int i = 3; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
			p_graphics.setColor(Color.black);
			for (int j = 4; j < l_size.height; j += 8) {
				for (int i = 4; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
		}
	}

	/**
	 * Client select implementation.
	 * @author flagouey
	 */
	private final class RecetteAddUpdateClientSelect extends ClientSelect {
		/** Serial Version ID. */
		private static final long serialVersionUID = 1L;

		@Override
		protected void registerValidators() {
			super.registerValidators();
			addPersonneValidators(new FournisseurEtatValideValidator(
					SelectValidatorSeverity.ERROR,
					FournisseurEtatValideValidator.ERR_FOURNISSEUR_ETAT_VALIDATION_EN_COURS));
		}
	}

}
