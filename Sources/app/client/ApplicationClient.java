/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import static org.cocktail.fwkcktlcomptaguiswing.client.all.OperatingSystemHelper.OS.MAC_OS_X;
import static org.cocktail.fwkcktlcomptaguiswing.client.all.OperatingSystemHelper.OS.WINDOWS;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JFrame;
import javax.swing.JMenuBar;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.MainNibControlerInterfaceController;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.fwkcktlcompta.common.FwkCktlComptaMoteurCtrl;
import org.cocktail.fwkcktlcompta.common.FwkCktlComptaMoteurCtrl.Contexte;
import org.cocktail.fwkcktlcomptaguiswing.client.all.IComptaGuiSwingApplication;
import org.cocktail.fwkcktlwebapp.common.CktlDockClient;
import org.cocktail.kava.client.finder.FinderParametreBudget;
import org.cocktail.kava.client.finder.FinderParametreDepense;
import org.cocktail.kava.client.finder.FinderParametreJefyAdmin;
import org.cocktail.kava.client.finder.FinderParametreMaracuja;
import org.cocktail.kava.client.finder.FinderParametres;
import org.cocktail.kava.client.metier.EOParametreJefyAdmin;
import org.cocktail.pie.client.interfaces.ControleurFactureCumulative;
import org.cocktail.pie.client.interfaces.FinderConsultationPaiementWeb;
import org.cocktail.pie.client.interfaces.MigrationExercicePie;
import org.cocktail.pieFwk.common.ApplicationConfig;
import org.cocktail.pieFwk.common.service.AbstractFacturationService;

import app.client.tools.Log;
import app.client.ui.WaitingFrame;
import app.server.VersionMe;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eoapplication.EOFrameController;
import com.webobjects.eoapplication.EOWindowObserver;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

/**
 * Pour y faire reference, utiliser EOApplication.sharedApplication();.
 */
public class ApplicationClient extends ApplicationCocktail implements ApplicationConfig, IComptaGuiSwingApplication {
	/** Minimal JRE Version. */
	private static final String MINJREVERSION = "1.5";
	/** Show trace. */
	private static final boolean SHOWTRACE = true;
	/** Show Logs. */
	private static final boolean SHOWLOGS = true;
	/** Window Title. */
	private static final String WINDOWTITLE = "Pie";

	/** Application Libelle. */
	public static final String APPLICATION_TYAP_LIBELLE = "RECETTE";

	// Chaines correspondant aux commandes permettant de lancer une commande sur les differents systemes
	private static final String MAC_OS_X_EXEC = "open ";
	private static final String WINDOWS_EXEC = "launch.bat ";

	private Superviseur superviseur;

	private WaitingFrame waitingFrame;
	private MainNibControlerInterfaceController main;

	//	private String initialFwkCktlWebApp, platform, execCmd, temporaryDir;
	private String platform, execCmd, temporaryDir;
	private String applicationName;
	private String applicationVersion;
	private String applicationBdConnexionName;
	private Log appLog;

	private AppUserInfo appUserInfo;

	private NumberFormat currencyFormatDisplay, currencyFormatEdit;
	private NumberFormat currencyPrecisionFormatDisplay, currencyPrecisionFormatEdit;
	private NumberFormat numeroFormatDisplay;

	private MigrationExercicePie migration;
	private FinderConsultationPaiementWeb consult;
	private ControleurFactureCumulative factureCumulative;

	// Nouvelles sorties
	private MyByteArrayOutputStream redirectedOutStream, redirectedErrStream;

	private Map<Number, Integer> PARAM_MONETAIRE_PAR_ANNEE = new HashMap<Number, Integer>();

	public ApplicationClient() {
		super();
		try {

			setWithLogs(false);
			setTYAPSTRID(VersionMe.APPLICATION_TYAP_STRID);
			setNAME_APP(APPLICATION_TYAP_LIBELLE);

			// redirection des logs
			redirectLogs();

			appLog = new Log();
			appLog.setShowOutput(showLogs());
			appLog.setShowTrace(showTrace());
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void finishInitialization() {
		try {

			super.finishInitialization();
			setQuitsOnLastWindowClose(true);

			initTimeZones();
			initCurrency();
			initMoteurCompta();
			this.numeroFormatDisplay = createNumberFormatter();

			appLog.appendTitle("Verification des versions");
			appLog.appendKeyValue("Version minimale JRE", MINJREVERSION);
			appLog.appendKeyValue("Version utilisee JRE", System.getProperty("java.version"));
			if (checkCurrentJREVersionVsMin(System.getProperty("java.version"), MINJREVERSION)) {
				appLog.appendSuccess("Test de version Ok");
			} else {
				appLog.appendWarning("La version JRE presente n'est pas compatible avec la version minimale");
			}

			try {
				compareJarVersionsClientAndServer();
			} catch (Exception e) {
				e.printStackTrace();
				showErrorDialog(e.getMessage());
				quit();
			}

			setStateOK();

		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	protected void initMoteurCompta() {
		FwkCktlComptaMoteurCtrl.getSharedInstance().init(Contexte.CLIENT);
		FwkCktlComptaMoteurCtrl.getSharedInstance().setRefApplicationCreation(VersionMe.APPLICATION_TYAP_STRID);
	}

	public void initMonApplication() {
		try {

			super.initMonApplication();
			initForPlatform();
			ServerProxy.serverSetFwkCktlWebApp(editingContext(), getUserInfos().login());
			appUserInfo = new AppUserInfo(getUserInfos().login());
			initGUI();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void setState(String s) {
		if (superviseur != null) {
			superviseur.setStateLabel(s);
		}
	}

	public void setStateOK() {
		setState("OK");
	}

	public void setStateWorking() {
		setState("Cogitation...");
	}

	private void initGUI() {
		if (superviseur == null) {

			afficherUnLogDansTransactionLog("Initialisation des composants...", 85);
			superviseur = new Superviseur(mainWindowTitle());

			afficherUnLogDansTransactionLog("Initialisation interface...", 90);
			superviseur.init();
		}
		getMainWindow().addWindowListener(new MainWindowListener());
		getMainWindow().setLocation(new Point(100, 100));
		afficherUnLogDansTransactionLog("Initialisation des données...", 95);

		// Ajout du menu
		JMenuBar menuBar = superviseur.mainMenu;
		main = new MainNibControlerInterfaceController(getAppEditingContext());
		EOFrameController.runControllerInNewFrame(main, mainWindowTitle());
		JMenuBar fwkMenuBar = main.component().getRootPane().getJMenuBar();
		main.view.setLayout(new BorderLayout());
		main.view.add(superviseur.getContentPane());
		main.component().getTopLevelAncestor().setBounds(0, 0, superviseur.WINDOW_DIMENSION.width + 150, superviseur.WINDOW_DIMENSION.height + 100);
		addLesPanelsModal(main);
		for (int i = 0; i < fwkMenuBar.getMenuCount(); i++) {
			menuBar.add(fwkMenuBar.getMenu(i));
		}

		superviseur.open();
	}

	/**
	 * Initialisation des variables dependantes de la plateforme d'exécution.
	 */
	private void initForPlatform() {
		appLog.appendTitle("Detection de la plateforme d'execution");
		temporaryDir = null;
		platform = System.getProperties().getProperty("os.name");
		appLog.appendKeyValue("Plateforme detectee", platform);
		try {
			temporaryDir = System.getProperty("java.io.tmpdir");

			if (!temporaryDir.endsWith(File.separator)) {
				temporaryDir = temporaryDir + File.separator;
			}
		} catch (Exception e) {
			appLog.appendWarning("Impossible de recuperer le repertoire temporaire");
		}

		if (platform.equals(MAC_OS_X.getName())) {
			execCmd = MAC_OS_X_EXEC;
			if (temporaryDir == null) {
				temporaryDir = "/tmp/";
			}
		} else if (platform.startsWith(WINDOWS.getName())) {
			execCmd = WINDOWS_EXEC;
			if (temporaryDir == null) {
				temporaryDir = "c:\\temp\\";
			}
		} else {
			execCmd = "";
		}

		File tmpDir = new File(temporaryDir);
		if (!tmpDir.exists()) {
			appLog.append("Tentative de creation du repertoire temporaire " + tmpDir);
			try {
				tmpDir.mkdirs();
				appLog.appendWarning("Repertoire " + tmpDir + " cree.");
			} catch (Exception e) {
				appLog.appendWarning("Impossible de creer le repertoire " + tmpDir);
			}
		} else {
			appLog.appendKeyValue("Repertoire temporaire", tmpDir);
		}
		appLog.appendKeyValue("Commande de lancement", execCmd);
	}

	/**
	 * Initialise le TimeZone é utiliser dans l'application é partir de ceux du serveur d'application.
	 */
	private void initTimeZones() {
		appLog.appendTitle("Initialisation du NSTimeZone");
		TimeZone.setDefault(ServerProxy.serverDefaultTimeZone(editingContext()));
		NSTimeZone.setDefaultTimeZone(ServerProxy.serverDefaultNSTimeZone(editingContext()));
		appLog.appendKeyValue("NSTimeZone par defaut utilise dans l'application", NSTimeZone.defaultTimeZone(), 75);
		NSTimestampFormatter ntf = new NSTimestampFormatter();
		appLog.appendKeyValue("Les NSTimestampFormatter analyseront les dates avec le NSTimeZone", ntf.defaultParseTimeZone(), 75);
		appLog.appendKeyValue("Les NSTimestampFormatter afficheront les dates avec le NSTimeZone", ntf.defaultFormatTimeZone(), 75);
	}

	private void initCurrency() {
		appLog.appendTitle("Mise à jour de la monnaie");
		EOExercice exercice = null;
		if (superviseur != null) {
			exercice = superviseur.currentExercice();
		}
		boolean useDecimal = getParamJefyAdminBoolean(EOParametreJefyAdmin.PARAM_USE_DECIMAL, exercice, true);
		if (useDecimal) {
			setCurrencyFormatDisplay(new DecimalFormat("#,##0.00"));
			setCurrencyFormatEdit(new DecimalFormat("#,##0.00"));
			setCurrencyPrecisionFormatDisplay(new DecimalFormat("#,##0.000"));
			setCurrencyPrecisionFormatEdit(new DecimalFormat("#,##0.000"));
		} else {
			setCurrencyFormatDisplay(new DecimalFormat("#,##0"));
			setCurrencyFormatEdit(new DecimalFormat("#,##0"));
			setCurrencyPrecisionFormatDisplay(new DecimalFormat("#,##0"));
			setCurrencyPrecisionFormatEdit(new DecimalFormat("#,##0"));
		}
		appLog.appendKeyValue("Format pour l'affichage", getCurrencyFormatDisplay(), 75);
		appLog.appendKeyValue("Format pour la saisie", getCurrencyFormatEdit(), 75);
	}

	private DecimalFormat createNumberFormatter() {
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.getDefault());
		otherSymbols.setDecimalSeparator('.');
		DecimalFormat numeroFormatDisplayLocal = new DecimalFormat("0.##", otherSymbols);
		return numeroFormatDisplayLocal;
	}

	// le titre de la fenétre principale de l'appli...
	private String mainWindowTitle() {
		// le titre de l'appli
		StringBuffer mainWindowTitle = new StringBuffer(WINDOWTITLE);
		return mainWindowTitle.toString();
	}

	/**
	 * Renvoie la fen?tre principale (ou null si celle-ci n'est pas encore créée).
	 */
	public Window getMainWindow() {
		if (superviseur != null) {
			return superviseur;
		}
		return null;
	}

	/**
	 * Renvoie la fen?tre principale (sous forme de JFrame) si celle-ci est créée.
	 */
	public JFrame getMainFrame() {
		if (superviseur != null) {
			return (JFrame) getMainWindow();
		}
		return null;
	}

	public void closeAllWindowsExceptMain() {
		for (int i = windowObserver().visibleWindows().count() - 1; i >= 0; i--) {
			if ((Window) windowObserver().visibleWindows().objectAtIndex(i) != getMainWindow()) {
				((Window) windowObserver().visibleWindows().objectAtIndex(i)).dispose();
			}
		}
	}

	// récup de paramétres depuis la table Parametres
	public String getParam(String parKey, EOExercice exercice) {
		try {
			return FinderParametres.find(editingContext(), parKey, exercice).parValue();
		} catch (Exception e) {
			return null;
		}
	}

	public boolean getParamBoolean(String parKey, EOExercice exercice) {
		return getParamBoolean(parKey, exercice, false);
	}

	public boolean getParamBoolean(String parKey, EOExercice exercice, boolean defaultValueWhenNotFound) {
		String parValue = getParam(parKey, exercice);
		return getGenericParamBoolean(parValue, defaultValueWhenNotFound);
	}

	// récup de paramétres depuis la table ParametreJefyAdmin
	public String getParamJefyAdmin(String parKey, EOExercice exercice) {
		try {
			return FinderParametreJefyAdmin.find(editingContext(), parKey, exercice).parValue();
		} catch (Exception e) {
			return null;
		}
	}

	public String getParamJefyAdmin(String parKey, Number exercice) {
		try {
			return FinderParametreJefyAdmin.find(editingContext(), parKey, exercice).parValue();
		} catch (Exception e) {
			return null;
		}
	}

	public boolean getParamJefyAdminBoolean(String parKey, EOExercice exercice) {
		return getParamJefyAdminBoolean(parKey, exercice, false);
	}

	public boolean getParamJefyAdminBoolean(String parKey, EOExercice exercice, boolean defaultValueWhenNotFound) {
		String parValue = getParamJefyAdmin(parKey, exercice);
		return getGenericParamBoolean(parValue, defaultValueWhenNotFound);
	}

	public boolean getParamJefyAdminBoolean(String parKey, Number exercice, boolean defaultValueWhenNotFound) {
		String parValue = getParamJefyAdmin(parKey, exercice);
		return getGenericParamBoolean(parValue, defaultValueWhenNotFound);
	}

	// récup de paramétres depuis la table ParametreMaracuja
	public String getParamMaracuja(String parKey, EOExercice exercice) {
		try {
			return FinderParametreMaracuja.find(editingContext(), parKey, exercice).parValue();
		} catch (Exception e) {
			return null;
		}
	}

	public boolean getParamMaracujaBoolean(String parKey, EOExercice exercice) {
		return getParamMaracujaBoolean(parKey, exercice, false);
	}

	public boolean getParamMaracujaBoolean(String parKey, EOExercice exercice, boolean defaultValueWhenNotFound) {
		String parValue = getParamMaracuja(parKey, exercice);
		return getGenericParamBoolean(parValue, defaultValueWhenNotFound);
	}

	// récup de paramétres depuis la table ParametreDepense
	public String getParamDepense(String parKey, EOExercice exercice) {
		try {
			return FinderParametreDepense.find(editingContext(), parKey, exercice).parValue();
		} catch (Exception e) {
			return null;
		}
	}

	public boolean getParamDepenseBoolean(String parKey, EOExercice exercice) {
		return getParamDepenseBoolean(parKey, exercice, false);
	}

	public boolean getParamDepenseBoolean(String parKey, EOExercice exercice, boolean defaultValueWhenNotFound) {
		String parValue = getParamDepense(parKey, exercice);
		return getGenericParamBoolean(parValue, defaultValueWhenNotFound);
	}

	// récup de paramétres depuis la table ParametreBudget
	public String getParamBudget(String parKey, EOExercice exercice) {
		try {
			return FinderParametreBudget.find(editingContext(), parKey, exercice).bparValue();
		} catch (Exception e) {
			return null;
		}
	}

	public boolean getParamBudgetBoolean(String parKey, EOExercice exercice) {
		return getParamBudgetBoolean(parKey, exercice, false);
	}

	public boolean getParamBudgetBoolean(String parKey, EOExercice exercice, boolean defaultValueWhenNotFound) {
		String parValue = getParamBudget(parKey, exercice);
		return getGenericParamBoolean(parValue, defaultValueWhenNotFound);
	}

	protected boolean getGenericParamBoolean(String parValue, boolean defaultValueWhenNotFound) {
		if (parValue == null) {
			return defaultValueWhenNotFound;
		}

		if (parValue.equals("1") || parValue.equalsIgnoreCase("O") || parValue.equalsIgnoreCase("OUI") || parValue.equalsIgnoreCase("Y")
				|| parValue.equalsIgnoreCase("YES") || parValue.equalsIgnoreCase("TRUE")) {
			return true;
		}

		return false;
	}

	// récup de paramétres généraux depuis le serveur
	public String getParam(String parKey) {
		return ServerProxy.getParam(editingContext(), parKey);
	}

	public NSArray getParams(String parKey) {
		return ServerProxy.getParams(editingContext(), parKey);
	}

	public boolean getParamBoolean(String parKey) {
		return ServerProxy.getParamBoolean(editingContext(), parKey);
	}

	public boolean getParamBoolean(String parKey, boolean defaultValueWhenNotFound) {
		return ServerProxy.getParamBoolean(editingContext(), parKey, defaultValueWhenNotFound);
	}

	/**
	 * Retourne le nom d'utilisateur donne par la barre de lancement des applications (le Dock). Le Dock lui-m?me recupere le login a partir du
	 * serveur CAS.
	 * <p>
	 * Retourne <i>null</i> si le Dock n'est pas disponible ou le login CAS ne peut pas etre recupere pour d'autres raisons. On suppose que dans ce
	 * cas l'utilisateur devra saisir le login explicitement.
	 * </p>
	 * <p>
	 * Cette methode utilise les parametres <code>LRAppDockPort</code> et <code>LRAppDockTicket</code> passes en ligne de commandes lors de lancement
	 * de l'applications par Dock. Ils correspondent respectivement au numero du port d'ecoute du Dock et le ticket (unique) a presenter au Dock.
	 * </p>
	 */
	private String getCASUserName() {
		String dockPort = (String) arguments().valueForKey("LRAppDockPort");
		String dockTicket = (String) arguments().valueForKey("LRAppDockTicket");
		String userName = null;
		// Les deux parametres doivent etre donnees pour communiquer avec Dock
		// On suppose que le Dock est disponible sur le poste local
		// getNetID(null, ...)
		if ((dockTicket != null) && (dockPort != null)) {
			userName = CktlDockClient.getNetID(null, dockPort, dockTicket);
		}
		return userName;
	}

	private boolean canUseApp() {
		if (appUserInfo() != null && appUserInfo().isConnected()) {
			return true;
		} else {
			showInfoDialog("Droits insuffisants pour accéder é cette application !");
			return false;
		}
	}

	public boolean canUseFonction(String fonIdInterne, EOExercice exercice) {
		return appUserInfo().canUseFonction(fonIdInterne, exercice);
	}

	/**
	 * @return l'objet représentant les infos utilisateurs.
	 * @see AppUserInfo
	 */
	public AppUserInfo appUserInfo() {
		return appUserInfo;
	}

	public Superviseur superviseur() {
		return superviseur;
	}

	public String aboutMsg() {
		StringBuffer msg = new StringBuffer();
		msg.append(ServerProxy.serverAppliId(editingContext()));
		msg.append("\n");
		msg.append("\nVersion : " + ServerProxy.serverAppliVersion(editingContext()));
		msg.append("\nBase : " + ServerProxy.serverAppliBdVersion(editingContext()) + " (mini requis:"
				+ ServerProxy.serverMinAppliBdVersion(editingContext()) + ")");
		msg.append("\n\nUtilisateur : " + appUserInfo().login());
		msg.append("\nBD : " + ServerProxy.serverBdConnexionName(editingContext()));
		msg.append("\nJRE : " + System.getProperty("java.version"));
		msg.append("\n\n" + ServerProxy.serverCopyright(editingContext()));
		return msg.toString();
	}

	public final void showErrorDialog(Exception e, Window parentWindow) {
		showErrorDialog(e);
	}

	public void showErrorDialog(Exception e) {
		String text = e.getMessage();

		if ((text == null) || (text.trim().length() == 0)) {
			this.appLog.trace("ERREUR... Impossible de recuperer le message...");
			e.printStackTrace();
			text = "Une erreur est survenue. Impossible de récupérer le message, il doit etre accessible dans la console...";
		} else {
			text = extractErrorMessage(text, "ORA-20001:");
			text = extractErrorMessage(text, "ORA-02290:");
			text = improveErrorMessage(text);
		}
		showErrorDialog(text);
	}

	public void showErrorDialog(String text) {
		System.err.println(text);
		EODialogs.runInformationDialog("Erreur", text);
	}

	private String extractErrorMessage(String errorMessage, String sqlCode) {
		String text = errorMessage;
		String[] msgs = errorMessage.split(sqlCode);
		if (msgs.length > 1) {
			text = msgs[1].split("\n")[0];
		}
		return text;
	}

	private String improveErrorMessage(String errorMessage) {
		String improvedErrorMessage = errorMessage;
		if (errorMessage.contains("CH_PRESTATION_LIGNE_3")) {
			improvedErrorMessage = "La quantité d'une ligne de prestation ne peut être inférieure au reste."
					+ " (violation de contrainte CH_PRESTATION_LIGNE_3)";
		}
		return improvedErrorMessage;
	}

	public void showInfoDialog(String text, Window parentWindow) {
		showInfoDialog(text);
	}

	public void showInfoDialog(String text) {
		EODialogs.runInformationDialog("Information", text);
	}

	public boolean showConfirmationDialog(String titre, String message, String defaultRep, Window parentWindow) {
		return showConfirmationDialog(titre, message, defaultRep);
	}

	public boolean showConfirmationDialog(String title, String message, String defaultResponse, String alternateResponse) {
		return EODialogs.runConfirmOperationDialog(title, message, defaultResponse, alternateResponse);
	}

	public boolean showConfirmationDialog(String title, String message, String defaultResponse) {
		return showConfirmationDialog(title, message, defaultResponse, "Annuler");
	}

	public void showinfoDialogFonctionNonImplementee(Window parentWindow) {
		showInfoDialogFonctionNonImplementee();
	}

	public void showInfoDialogFonctionNonImplementee() {
		EODialogs.runInformationDialog("Information", "Cette fonctionnalité n'est pas encore disponible.");
	}

	protected boolean showLogs() {
		return SHOWLOGS;
	}

	protected boolean showTrace() {
		return SHOWTRACE;
	}

	public final Window activeWindow() {
		return windowObserver().activeWindow();
	}

	public final String getApplicationFullName() {
		return getApplicationName() + " - " + getApplicationVersion() + " - " + getApplicationBdConnexionName();
	}

	public final String getApplicationName() {
		if (applicationName == null) {
			applicationName = ServerProxy.serverAppliId(editingContext());
		}
		return applicationName;
	}

	public final String getApplicationVersion() {
		if (applicationVersion == null) {
			applicationVersion = ServerProxy.serverAppliVersion(editingContext());
		}
		return applicationVersion;
	}

	public final String getApplicationBdConnexionName() {
		if (applicationBdConnexionName == null) {
			applicationBdConnexionName = ServerProxy.serverBdConnexionName(editingContext());
		}
		return applicationBdConnexionName;
	}

	public String getJREVersion() {
		return System.getProperty("java.version");
	}

	/** Ouvre un fichier (ou lance un programme externe) */
	public void openFile(String filePath) throws Exception {
		if (filePath == null) {
			return;
		}
		superviseur.setWaitCursor(true);
		File aFile = new File(filePath);
		if (platform.startsWith(WINDOWS.getName())) {
			Runtime.getRuntime().exec(new String[] {
					"rundll32", "url.dll,FileProtocolHandler", "\"" + aFile + "\""
			});
		}
		else {
			Runtime.getRuntime().exec("open " + aFile);
		}
		superviseur.setWaitCursor(false);
	}

	/** Ouvre un fichier non enregistré qui est sous forme de NSData (ou lance un programme externe) */
	public void openFile(NSData filedata, String filename) throws Exception {
		if (filedata == null || filename == null) {
			return;
		}
		superviseur.setWaitCursor(true);
		String filePath = getTemporaryDir() + filename;
		try {
			FileOutputStream fileOutputStream = new FileOutputStream(filePath);
			filedata.writeToStream(fileOutputStream);
			fileOutputStream.close();
		} catch (Exception exception) {
			throw new Exception("Impossible d'ecrire le fichier sur le disque. Vérifiez qu'un autre fichier n'est pas déjé ouvert.\n"
					+ exception.getMessage());
		}
		// Vérifier que le fichier a bien ete créé
		try {
			File tmpFile = new File(filePath);
			if (!tmpFile.exists()) {
				throw new Exception("Le fichier " + filePath + " n'existe pas.");
			}
		} catch (Exception e) {
			throw new Exception(e);
		}

		// ouverture...
		try {
			openFile(filePath);
		} catch (Exception e) {
			throw new Exception("Impossible d'ouvrir le fichier " + filePath + ". Erreur: " + e);
		}

		superviseur.setWaitCursor(false);
	}

	// Ouvre une url
	public void openURL(String urlString) throws Exception {
		superviseur.setWaitCursor(true);
		if (urlString.startsWith("http://")) {
			URL url = new URL(urlString);
			if (platform.startsWith(WINDOWS.getName())) {
				System.out.println("url = " + url);
				Runtime.getRuntime().exec(new String[] {
						"rundll32", "url.dll,FileProtocolHandler", "\"" + url + "\""
				});
			} else {
				Runtime.getRuntime().exec(MAC_OS_X_EXEC + url);
			}
		}
		superviseur.setWaitCursor(false);
	}

	public boolean canQuit() {
		return (superviseur() == null || superviseur().canQuit());
	}

	public void updateLoadingMsg(String msg) {
		if ((waitingFrame != null) && (waitingFrame.isVisible())) {
			waitingFrame.setMessages(msg, null);
		}
	}

	public void updateMenuDuplicataFacturePapier(boolean hasPermissionOrgan) {
		superviseur.mainMenu.updateDuplicataFacturePapier(hasPermissionOrgan);
	}

	private boolean checkCurrentJREVersionVsMin(String currentVersion, String minVersion) {
		return (currentVersion.compareTo(minVersion) >= 0);
	}

	public EOEditingContext editingContext() {
		return getAppEditingContext();
	}

	public class MyEOWindowObserver extends EOWindowObserver {
		public void windowActivated(WindowEvent event) {
			super.windowActivated(event);
			// Si l'événement se produit sur la fenetre principale
			if (event.getWindow() == getMainWindow()) {
				// Récupérer toutes les fen^etres ouvertes et les mettre en premier-plan
				for (int i = visibleWindows().count() - 1; i >= 0; i--) {
					if ((Window) visibleWindows().objectAtIndex(i) != getMainWindow()) {
						((Window) visibleWindows().objectAtIndex(i)).toFront();
					}
				}
			}
		}
	}

	public class MainWindowListener implements WindowListener {
		public void windowOpened(WindowEvent arg0) {
		}

		public void windowClosed(WindowEvent arg0) {
			quit();
		}

		public void windowIconified(WindowEvent arg0) {
		}

		public void windowDeiconified(WindowEvent arg0) {
		}

		public void windowActivated(WindowEvent arg0) {
			if (arg0.getWindow() == getMainWindow()) {
				final LinkedList visibleWindows = visibleWindows();
				for (Iterator iter = visibleWindows.iterator(); iter.hasNext();) {
					final Window element = (Window) iter.next();
					if (!element.equals(getMainWindow())) {
						element.toFront();
					}
				}
			}
		}

		/**
		 * @return la liste des fenetres ouvertes (dans l'ordre d'ouverture)
		 */
		private LinkedList visibleWindows() {
			final LinkedList list = new LinkedList();
			final Window[] array = Frame.getFrames();
			for (int i = 0; i < array.length; i++) {
				list.addAll(visibleWindows(array[i]));
			}
			return list;
		}

		/**
		 * Renvoie la liste des fenetres et sous-fenetres visibles à partir de win.
		 *
		 * @param win
		 * @param list
		 * @return
		 */
		private LinkedList visibleWindows(final Window win) {
			final LinkedList list = new LinkedList();
			list.add(win);
			final Window[] array = win.getOwnedWindows();
			for (int i = 0; i < array.length; i++) {
				Window window = array[i];
				if (window.isVisible()) {
					list.addAll(visibleWindows(window));
				}
			}
			return list;
		}

		public void windowDeactivated(WindowEvent arg0) {
		}

		public void windowClosing(WindowEvent arg0) {
			quit();
		}
	}

	public static void setWaitCursorForWindow(Window window, boolean isWaiting) {
		if (isWaiting)
			window.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		else
			window.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}

	/**
	 * Méthode qui <b>tente</b> de contourner le bug EOF qui se produit lors d'un saveChanges avec l'erreur "reentered responseToMessage()".<br>
	 * <b>Il faut appeler cette méthode avant de créer un descendant d'EOCustomObject, donc bien avant le saveChanges()</b><br>
	 * Le principe est d'appeler la méthode EOClassDescription.classDescriptionForEntityName("A") pour chaque relation de l'objet qu'on va créer. Il
	 * faut appeler cette méthode avant de créer un objet, au lancement de l'application par exemple, sur toutes les entités du mod?le. Par exemple
	 * dans le cas d'un objet Facture qui a des objets Ligne, appeler EOClassDescription.classDescriptionForEntityName("Facture") avant de créer un
	 * objet Ligne. Répéter l'opération pour toutes les relations de l'objet.
	 *
	 * @param list Liste de String identifiant une entité du mod?le.
	 * @see http://www.omnigroup.com/mailman/archive/webobjects-dev/2002-May/023698.html
	 */
	public static final void fixWoBug_responseToMessage(final String[] list) {
		for (int i = 0; i < list.length; i++) {
			EOClassDescription.classDescriptionForEntityName(list[i]);
			// System.out.println(list[i] +" OK");
		}
	}
	
    public static boolean isNewObject(EOEditingContext edc, EOEnterpriseObject record) {
        return edc.globalIDForObject(record).isTemporary();
    }

	public static String formatDateToString(NSTimestamp date, String format) {
		NSTimestampFormatter formatter = new NSTimestampFormatter(format);
		formatter.setDefaultParseTimeZone(((NSTimestamp) date).timeZone());
		formatter.setDefaultFormatTimeZone(ServerProxy.serverDefaultNSTimeZone(((ApplicationClient) EOApplication.sharedApplication()).getAppEditingContext()));
		return formatter.format(date);
	}

	public static NSTimestamp formatStringToDate(String str, String format) {
		NSTimestampFormatter formatter = new NSTimestampFormatter(format);
		formatter.setDefaultParseTimeZone(ServerProxy.serverDefaultNSTimeZone(((ApplicationClient) EOApplication.sharedApplication()).getAppEditingContext()));
		formatter.setDefaultFormatTimeZone(ServerProxy.serverDefaultNSTimeZone(((ApplicationClient) EOApplication.sharedApplication()).getAppEditingContext()));
		try {
			return (NSTimestamp) formatter.parseObject(str);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Menu APPLICATION --> .......
	 */
	public void appPreferences() {
		// TODO
		showInfoDialogFonctionNonImplementee();
	}

	public void appReset() {
		setState("Réinitialisation en cours...");
		editingContext().invalidateAllObjects();
		appUserInfo.updateUtilisateur(appUserInfo().getPersId());
		superviseur.init();
		superviseur.open();
		ServerProxy.serverReset(editingContext());
		setStateOK();
	}

	public void quit() {
		//vire le pb sur l'affichage auto du savechanges
		if (main != null) {
			main.dispose();
		}

		super.quit();
	}

	/**
	 * Menu OUTILS --> .......
	 */
	public void outilsPIFromCommande() {
		if (PrestationFromCommandeAdd.sharedInstance().openNew()) {
			Prestation.sharedInstance().updateData();
			FacturePapier.sharedInstance().updateData();
		}
	}

	public void outilsDuplicateFacturePapier() {
		FacturePapier.sharedInstance().duplicateFacturePapier();
	}

	public void outilsStatistiques() {
		showInfoDialogFonctionNonImplementee();
	}

	public void importCatalogues() {
		showInfoDialog("Fonction non disponible pour l'instant, lancez séparément l'application d'import des catalogues.");
	}

	/**
	 * Pour l'interface de migration d'exercice
	 */
	public void interfaceMigration() {
		//ToolsCocktailLogs.addStatistique("OPEN", "Interface de migration d'exercice");
		if (migration == null) {
			migration = new MigrationExercicePie();
		}
		migration.open();
	}

	/**
	 * Pour l'interface de consultation des paiement web
	 */
	public void paiementWeb() {
		//ToolsCocktailLogs.addStatistique("OPEN", "Consultation paiement par le web");
		if (consult == null) {
			consult = new FinderConsultationPaiementWeb(this, 0, 0, 800, 480);
		}
		consult.afficherFenetre();
	}

	/**
	 * Pour l'interface de facture cumulative
	 */
	public void factureCumulative() {
		//ToolsCocktailLogs.addStatistique("OPEN", "Prestation cumulative");
		if (factureCumulative == null) {
			factureCumulative = new ControleurFactureCumulative();
		}
		factureCumulative.afficherFenetre();
	}

	/**
	 * Menu AIDE --> .......
	 */
	public void showLog() {
		super.showLog();
	}

	public void showAboutDialog() {
		EODialogs.runInformationDialog("A propos", aboutMsg());
	}

	public String clientOutLog() {
		return redirectedOutStream.toString();
	}

	public String clientErrLog() {
		return redirectedErrStream.toString();
	}

	public String serverOutLog() {
		return ServerProxy.serverInitLog(editingContext()) + "\n...\n\n\n" + ServerProxy.serverOutLog(editingContext());
	}

	public String serverErrLog() {
		return ServerProxy.serverErrLog(editingContext());
	}

	public NSArray fetch(String tableName, EOQualifier qualifier) {
		try {
			return editingContext().objectsWithFetchSpecification(new EOFetchSpecification(tableName, qualifier, null));
		} catch (Exception e) {
			return new NSArray();
		}
	}

	public EOGenericRecord fetchOne(String tableName, EOQualifier qualifier) {
		try {
			return (EOGenericRecord) editingContext().objectsWithFetchSpecification(new EOFetchSpecification(tableName, qualifier, null))
					.lastObject();
		} catch (Exception e) {
			return null;
		}
	}

	private void redirectLogs() {
		redirectedOutStream = new MyByteArrayOutputStream(System.out);
		redirectedErrStream = new MyByteArrayOutputStream(System.err);
		System.setOut(new PrintStream(redirectedOutStream));
		System.setErr(new PrintStream(redirectedErrStream));
		// on force la sortie du NSLog (qui était initialisé par défaut sur System.xxx AVANT le changement)
		((NSLog.PrintStreamLogger) NSLog.out).setPrintStream(System.out);
		((NSLog.PrintStreamLogger) NSLog.debug).setPrintStream(System.out);
		((NSLog.PrintStreamLogger) NSLog.err).setPrintStream(System.err);
	}

	// Classe pour rediriger les logs vers la sortie initiale tout en les conservant dans un ByteArray...
	// Garde en mémoire un log de taille entre maxCount/2 et maxCount octets
	private class MyByteArrayOutputStream extends ByteArrayOutputStream {
		protected PrintStream out;
		protected int maxCount;

		public MyByteArrayOutputStream() {
			this(System.out, 0);
		}

		public MyByteArrayOutputStream(PrintStream out) {
			this(out, 0);
		}

		public MyByteArrayOutputStream(PrintStream out, int maxCount) {
			super(maxCount);
			this.out = out;
			this.maxCount = maxCount;
		}

		public synchronized void write(int b) {
			if (maxCount > 0 && count + 1 > maxCount) {
				shift(Math.min(maxCount >> 1, count));
			}
			super.write(b);
			out.write(b);
		}

		public synchronized void write(byte[] b, int off, int len) {
			if (maxCount > 0 && count + len > maxCount) {
				shift(Math.min(maxCount >> 1, count));
			}
			super.write(b, off, len);
			out.write(b, off, len);
		}

		private void shift(int shift) {
			for (int i = shift; i < count; i++) {
				buf[i - shift] = buf[i];
			}
			count = count - shift;
		}
	}

	public void playSound(String soundFilename) {
		NSData data = ServerProxy.readResourceFile(editingContext(), soundFilename);
		new PlayAudio(data).start();
	}

	private final class PlayAudio extends Thread {
		NSData audioData;

		PlayAudio(NSData audioData) {
			this.audioData = audioData;
		}

		public void run() {
			AudioInputStream in = null;
			try {
				in = AudioSystem.getAudioInputStream(audioData.stream());
			} catch (UnsupportedAudioFileException e1) {
				System.err.println("Unsupported audio file exception : " + e1);
				return;
			} catch (IOException e2) {
				System.err.println("IO exception : " + e2);
				return;
			}
			AudioFormat audioFormat = in.getFormat();
			// System.out.println("Audio format: " + audioFormat);

			DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
			if (!AudioSystem.isLineSupported(info)) {
				System.out.println("Codage audio non supporté");
				return;
			}
			try {
				SourceDataLine dataLine = (SourceDataLine) AudioSystem.getLine(info);
				dataLine.open(audioFormat);
				dataLine.start();

				int bufferSize = (int) audioFormat.getSampleRate() * audioFormat.getFrameSize();
				byte[] buffer = new byte[bufferSize];
				try {
					int bytesRead = 0;
					while (bytesRead >= 0) {
						bytesRead = in.read(buffer, 0, buffer.length);
						if (bytesRead >= 0) {
							dataLine.write(buffer, 0, bytesRead);
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
				dataLine.drain();
				dataLine.close();
			} catch (LineUnavailableException e) {
				e.printStackTrace();
			}
		}
	}

	public NumberFormat getCurrencyFormatDisplay() {
		return currencyFormatDisplay;
	}

	public void setCurrencyFormatDisplay(NumberFormat currencyFormatDisplay) {
		this.currencyFormatDisplay = currencyFormatDisplay;
	}

	public NumberFormat getCurrencyFormatEdit() {
		return currencyFormatEdit;
	}

	public void setCurrencyFormatEdit(NumberFormat currencyFormatEdit) {
		this.currencyFormatEdit = currencyFormatEdit;
	}

	public NumberFormat getCurrencyPrecisionFormatDisplay() {
		return currencyPrecisionFormatDisplay;
	}

	public void setCurrencyPrecisionFormatDisplay(NumberFormat currencyPrecisionFormatDisplay) {
		this.currencyPrecisionFormatDisplay = currencyPrecisionFormatDisplay;
	}

	public NumberFormat getCurrencyPrecisionFormatEdit() {
		return currencyPrecisionFormatEdit;
	}

	public void setCurrencyPrecisionFormatEdit(NumberFormat currencyPrecisionFormatEdit) {
		this.currencyPrecisionFormatEdit = currencyPrecisionFormatEdit;
	}

	public NumberFormat getNumeroFormatDisplay() {
		return numeroFormatDisplay;
	}

	public String getTemporaryDir() {
		return temporaryDir;
	}

	public void setTemporaryDir(String temporaryDir) {
		this.temporaryDir = temporaryDir;
	}

	protected NSArray defaultActions() {
		return new NSArray();
	}

	public int nbDecimalesImposees(EOEditingContext edc, Number exercice) {
		if (!PARAM_MONETAIRE_PAR_ANNEE.containsKey(exercice)) {
			PARAM_MONETAIRE_PAR_ANNEE.put(exercice, loadParamMonetaire(editingContext(), exercice));
		}
		return PARAM_MONETAIRE_PAR_ANNEE.get(exercice);
	}

	private Integer loadParamMonetaire(EOEditingContext edc, Number exercice) {
		boolean useDecimal = getParamJefyAdminBoolean(EOParametreJefyAdmin.PARAM_USE_DECIMAL, exercice, true);
		if (useDecimal) {
			return AbstractFacturationService.DEUX_DECIMALES;
		} else {
			return AbstractFacturationService.ZERO_DECIMALE;
		}
	}

}