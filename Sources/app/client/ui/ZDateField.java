/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client.ui;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.text.JTextComponent;

import app.client.tools.Constants;

import com.webobjects.foundation.NSTimestamp;

public class ZDateField extends ZTextField {

	private static final DateFormat	DEFAULT_FORMAT	= Constants.FORMAT_DATESHORT;
	private DateFormat				format;

	public ZDateField(int cols) {
		super(cols);
		init(null);
	}

	public ZDateField(int cols, DateFormat format) {
		super(cols);
		init(format);
	}

	private void init(DateFormat format) {
		if (format != null) {
			this.format = format;
		}
		else {
			this.format = DEFAULT_FORMAT;
		}
		addFocusListener(new DateFocusListener());
	}

	public void setDate(Date date) {
		String s = null;
		if (date != null) {
			s = format.format(date);
		}
		super.setText(s);
	}

	public void setDate(Date date, boolean editable) {
		setDate(date);
		setEditable(editable);
	}

	public Date getDate() {
		String s = getText();
		if (s == null) {
			return null;
		}
		Date d = null;
		try {
			d = format.parse(s);
		}
		catch (ParseException e) {
		}
		return d;
	}

	public NSTimestamp getNSTimestamp() {
		Date d = getDate();
		if (d == null) {
			return null;
		}
		return new NSTimestamp(d);
	}

	private class DateFocusListener implements FocusListener {
		public void focusGained(FocusEvent e) {
			((JTextComponent) e.getComponent()).selectAll();
		}

		public void focusLost(FocusEvent e) {
			makeItPretty();
		}

		private void makeItPretty() {
			String input = getText();
			if (input == null) {
				return;
			}
			Date d = null;
			// si colle deja au format, cooool...
			try {
				d = format.parse(input);
			}
			catch (ParseException e) {
				// sinon...
				d = dateCompletion(input);
			}
			if (d == null) {
				requestFocus();
				selectAll();
			}
			else {
				setText(format.format(d));
			}
		}

		public Date dateCompletion(String aDate) {
			GregorianCalendar calendar = new GregorianCalendar();
			String annee = String.valueOf(calendar.get(Calendar.YEAR));
			String mois = String.valueOf(calendar.get(Calendar.MONTH) + 1);
			if (mois.length() <= 1)
				mois = "0" + mois;
			String jour = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
			if (jour.length() <= 1)
				jour = "0" + jour;
			aDate = aDate.replaceAll("\\D", "");
			switch (aDate.length()) {
			case 0:
				break;
			case 1:
				jour = "0" + aDate;
				break;
			case 2:
				jour = aDate;
				break;
			case 3:
				jour = aDate.substring(0, 2);
				mois = "0" + aDate.substring(2);
				break;
			case 4:
				jour = aDate.substring(0, 2);
				mois = aDate.substring(2);
				break;
			case 5:
				jour = aDate.substring(0, 2);
				mois = aDate.substring(2).substring(0, 2);
				annee = annee.substring(0, 3) + aDate.substring(4);
				break;

			case 6:
				jour = aDate.substring(0, 2);
				mois = aDate.substring(2).substring(0, 2);
				annee = annee.substring(0, 2) + aDate.substring(4);
				break;

			case 7:
				jour = aDate.substring(0, 2);
				mois = aDate.substring(2).substring(0, 2);
				annee = annee.substring(0, 1) + aDate.substring(4);
				break;

			default:
				jour = aDate.substring(0, 2);
				mois = aDate.substring(2).substring(0, 2);
				annee = aDate.substring(4).substring(0, 4);
			}
			try {
				return new SimpleDateFormat("ddMMyyyy").parse(jour + mois + annee);
			}
			catch (ParseException e) {
			}
			return null;
		}
	}
}
