/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client.ui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import app.client.tools.Constants;

public class UIUtilities {

	// Default alignment between label and component
	public static final int		DEFAULT_ALIGNMENT	= SwingConstants.VERTICAL;

	// default boxed or not
	public static final boolean	DEFAULT_BOXED		= false;

	/**
	 * @param label
	 *            Can be null ( == no label)
	 * @param component
	 * @param action
	 *            Can be null ( == no button)
	 * @return
	 */
	public static JPanel labeledComponent(String label, JComponent component, Action action) {
		return labeledComponent(label, component, action == null ? null : new Action[] { action }, DEFAULT_BOXED, DEFAULT_ALIGNMENT);
	}

	/**
	 * @param label
	 *            Can be null ( == no label)
	 * @param component
	 * @param action
	 *            Can be null ( == no button)
	 * @param boxed
	 *            Component in a [titled] box or not (titled with label if not null) ?
	 * @return
	 */
	public static JPanel labeledComponent(String label, JComponent component, Action action, boolean boxed) {
		return labeledComponent(label, component, action == null ? null : new Action[] { action }, boxed, DEFAULT_ALIGNMENT);
	}

	/**
	 * @param label
	 *            Can be null ( == no label)
	 * @param component
	 * @param action
	 *            Can be null ( == no button)
	 * @param alignment
	 *            Determines the alignment between the label and the component. Must be one of the following: SwingConstants.VERTICAL,
	 *            SwingConstants.HORIZONTAL
	 * @return
	 */
	public static JPanel labeledComponent(String label, JComponent component, Action action, int alignment) {
		return labeledComponent(label, component, action == null ? null : new Action[] { action }, DEFAULT_BOXED, alignment);
	}

	/**
	 * @param label
	 *            Can be null ( == no label)
	 * @param component
	 * @param actions
	 *            Can be null ( == no button)
	 * @param boxed
	 *            Component in a [titled] box or not (titled with label if not null) ?
	 * @param alignment
	 *            Determines the alignment between the label and the component. Must be one of the following: SwingConstants.VERTICAL,
	 *            SwingConstants.HORIZONTAL
	 * @return
	 */
	public static JPanel labeledComponent(String label, JComponent component, Action[] actions, boolean boxed, int alignment) {
		JButton[] buttons = null;
		if (actions != null) {
			buttons = new JButton[actions.length];
			for (int i = 0; i < buttons.length; i++) {
				buttons[i] = new JButton(actions[i]);
				buttons[i].setFocusPainted(false);
				buttons[i].setPreferredSize(new Dimension(16, 16));
				buttons[i].setHorizontalAlignment(SwingConstants.CENTER);
				buttons[i].setVerticalAlignment(SwingConstants.CENTER);
			}
		}
		return labeledComponentAndButtons(label, component, buttons, boxed, alignment);
	}

	/**
	 * @param label
	 *            Can be null ( == no label)
	 * @param component
	 * @param buttons
	 *            Can be null ( == no button)
	 * @return
	 */
	public static JPanel labeledComponentAndButtons(String label, JComponent component, JButton[] buttons) {
		return labeledComponentAndButtons(label, component, buttons, DEFAULT_BOXED, DEFAULT_ALIGNMENT);
	}

	/**
	 * @param label
	 *            Can be null ( == no label)
	 * @param component
	 * @param buttons
	 *            Can be null ( == no button)
	 * @param boxed
	 *            Component in a [titled] box or not (titled with label if not null) ?
	 * @param alignment
	 *            Determines the alignment between the label and the component. Must be one of the following: SwingConstants.VERTICAL,
	 *            SwingConstants.HORIZONTAL
	 * @return
	 */
	public static JPanel labeledComponentAndButtons(String label, JComponent component, JButton[] buttons, boolean boxed, int alignment) {
		JPanel panel = new JPanel();
		if (alignment != SwingConstants.VERTICAL && alignment != SwingConstants.HORIZONTAL) {
			alignment = DEFAULT_ALIGNMENT;
		}
		int boxLayoutAxis = (alignment == SwingConstants.HORIZONTAL ? BoxLayout.LINE_AXIS : BoxLayout.PAGE_AXIS);
		panel.setLayout(new BoxLayout(panel, boxLayoutAxis));

		if (boxed) {
			panel.setBorder(BorderFactory.createTitledBorder(null, label, TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION,
					null, Constants.COLOR_LABEL_FGD_LIGHT));
		}
		else {
			panel.setBorder(BorderFactory.createEmptyBorder());
		}

		if (label != null && !boxed) {
			JLabel jl = new JLabel(label);
			jl.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);
			jl.setAlignmentX(0);
			jl.setLabelFor(component);
			panel.add(jl);
			if (boxLayoutAxis == BoxLayout.LINE_AXIS) {
				panel.add(Box.createRigidArea(new Dimension(3, 0)));
			}
		}

		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.LINE_AXIS));
		p.setBorder(BorderFactory.createEmptyBorder());
		p.setAlignmentX(0);
		p.add(component);
		if (buttons != null) {
			for (int i = 0; i < buttons.length; i++) {
				p.add(Box.createRigidArea(new Dimension(3, 0)));
				p.add(buttons[i]);
				if (i == 0 && component instanceof JTextField) {
					((JTextField) component).addActionListener(new DoClickActionListener(buttons[i]));
				}
			}
		}
		panel.add(p);
		panel.setAlignmentX(0);

		JPanel finalPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		finalPanel.setBorder(BorderFactory.createEmptyBorder());
		finalPanel.add(panel);

		return finalPanel;
	}

	private static class DoClickActionListener implements ActionListener {
		private JButton	button;

		public DoClickActionListener(JButton button) {
			this.button = button;
		}

		public void actionPerformed(ActionEvent arg0) {
			this.button.doClick();
		}

	}

	public static JButton makeButton(Action a) {
		Dimension d = new Dimension(24, 24);
		JButton bt = new JButton(a);
		bt.setPreferredSize(d);
		bt.setMaximumSize(d);
		bt.setMinimumSize(d);
		bt.setHorizontalAlignment(SwingConstants.CENTER);
		bt.setBorderPainted(true);
		bt.setFocusPainted(false);
		return bt;
	}

	public static JButton makeButtonLib(Action a) {
		Dimension d = new Dimension(115, 24);
		JButton bt = new JButton(a);
		bt.setPreferredSize(d);
		bt.setMaximumSize(d);
		bt.setMinimumSize(d);
		bt.setHorizontalAlignment(SwingConstants.CENTER);
		bt.setBorderPainted(true);
		bt.setFocusPainted(false);
		return bt;
	}
}
