/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */
package app.client.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;

import javax.swing.AbstractButton;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputAdapter;

/**
 * Glasspane blocking all mouse events except the ones bound to the specified button.
 * @author flagouey
 */
public class DisabledGlassPane extends JPanel {

	/** Serial version Id. */
	private static final long serialVersionUID = 1L;

	/** Background color. */
	private static final Color COLOR_WASH = new Color(64, 64, 64, 32);

	/** White margin. */
	private static final int WHITE_MARGIN = 3;
	/** Black margin. */
	private static final int BLACK_MARGIN = 4;
	/** Color point step. */
	private static final int COLOR_POINT_STEP = 10;
	/** Bottom height. */
	private static final int BOTTOM_HEIGHT = 35;


	/**
	 * Constructor.
	 * @param contentPane content pane.
	 * @param button      button listening to mouve events.
	 */
	public DisabledGlassPane(Container contentPane, AbstractButton button) {
		super.setOpaque(false);
		super.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

		CloseGlassPaneListener listener = new CloseGlassPaneListener(contentPane, this, button);
        addMouseListener(listener);
        addMouseMotionListener(listener);
	}

	/**
	 * Paint.
	 * @param graphics graphics.
	 */
	@Override
	public final void paintComponent(Graphics graphics) {
		Dimension size = super.getSize();

		// Wash the pane with translucent gray.
		graphics.setColor(COLOR_WASH);
		graphics.fillRect(0, 0, size.width, size.height);

		// Paint a grid of white/black dots.
		graphics.setColor(Color.white);
		for (int j = WHITE_MARGIN; j < size.height - BOTTOM_HEIGHT; j += COLOR_POINT_STEP) {
			for (int i = WHITE_MARGIN; i < size.width; i += COLOR_POINT_STEP) {
				graphics.fillRect(i, j, 1, 1);
			}
		}
		graphics.setColor(Color.black);
		for (int j = BLACK_MARGIN; j < size.height - BOTTOM_HEIGHT; j += COLOR_POINT_STEP) {
			for (int i = BLACK_MARGIN; i < size.width; i += COLOR_POINT_STEP) {
				graphics.fillRect(i, j, 1, 1);
			}
		}
	}
	/**
	 * Listen for all events that our button is likely to be interested in.
	 * Redispatch them to the check box.
	 */
	private class CloseGlassPaneListener extends MouseInputAdapter {
		/** Button that can receive mouse events. */
	    private Component closeButton;
	    /** Disabled Glass pane. */
	    private DisabledGlassPane glassPane;
	    /** Content pane. */
	    private Container contentPane;
	    /** In drag flag. */
	    private boolean inDrag;

	    /**
	     * Constructor.
	     * @param contentPane contentPane
	     * @param glassPane   glassPane
	     * @param closeButton button listening to mouse events.
	     */
	    public CloseGlassPaneListener(Container contentPane, DisabledGlassPane glassPane, Component closeButton) {
	        this.closeButton = closeButton;
	        this.glassPane = glassPane;
	        this.contentPane = contentPane;
	        this.inDrag = false;
	    }

	    /**
	     * Mouse moved action.
	     * @param e mouse event.
	     */
	    public void mouseMoved(MouseEvent e) {
	        redispatchMouseEvent(e, false);
	    }

	    /**
	     * Mouse dragged action.
	     * We must forward at least the mouse drags that started
	     * with mouse presses over the check box.  Otherwise,
	     * when the user presses the check box then drags off,
	     * the check box isn't disarmed -- it keeps its dark
	     * gray background or whatever its L&F uses to indicate
	     * that the button is currently being pressed.
	     *
	     * @param e mouse event.
	     */
	    public void mouseDragged(MouseEvent e) {
	        redispatchMouseEvent(e, false);
	    }

	    /**
	     * Mouse clicked action.
	     * @param e mouse event.
	     */
	    public void mouseClicked(MouseEvent e) {
	        redispatchMouseEvent(e, false);
	    }

	    /**
	     * Mouse entered action.
	     * @param e mouse event.
	     */
	    public void mouseEntered(MouseEvent e) {
	        redispatchMouseEvent(e, false);
	    }

	    /**
	     * Mouse exited action.
	     * @param e mouse event.
	     */
	    public void mouseExited(MouseEvent e) {
	        redispatchMouseEvent(e, false);
	    }

	    /**
	     * Mouse pressed action.
	     * @param e mouse event.
	     */
	    public void mousePressed(MouseEvent e) {
	        redispatchMouseEvent(e, false);
	    }

	    /**
	     * Mouse released action.
	     * @param e mouse event.
	     */
	    public void mouseReleased(MouseEvent e) {
	        redispatchMouseEvent(e, true);
	        this.inDrag = false;
	    }

	    /**
	     * Handle mouse event.
	     * @param e      mouse event.
	     * @param repaint must repaint the glasspane.
	     */
	    private void redispatchMouseEvent(MouseEvent e, boolean repaint) {
	        boolean inButton = false;
	        Point glassPanePoint = e.getPoint();
	        Component component = null;
	        Container container = contentPane;
	        Point containerPoint = SwingUtilities.convertPoint(glassPane, glassPanePoint, contentPane);
	        int eventID = e.getID();

	        //If the event is from a component in a popped-up menu,
	        //then the container should probably be the menu's
	        //JPopupMenu, and containerPoint should be adjusted accordingly.
	        component = SwingUtilities.getDeepestComponentAt(container, containerPoint.x, containerPoint.y);

	        if (component == null) {
	            return;
	        }

	        if (component.equals(closeButton)) {
	            inButton = true;
	            testForDrag(eventID);
	        }

	        if (inButton || inDrag) {
	            Point componentPoint = SwingUtilities.convertPoint(glassPane, glassPanePoint, component);
	            component.dispatchEvent(new MouseEvent(component,
	                                                 eventID,
	                                                 e.getWhen(),
	                                                 e.getModifiers(),
	                                                 componentPoint.x,
	                                                 componentPoint.y,
	                                                 e.getClickCount(),
	                                                 e.isPopupTrigger()));
	        }

	        if (repaint) {
	            glassPane.repaint();
	        }
	    }

	    /**
	     * Switch inDrag flag to TRUE if the mouse is currently dragging something.
	     * @param eventID eventID.
	     */
	    private void testForDrag(int eventID) {
	        if (eventID == MouseEvent.MOUSE_PRESSED) {
	            inDrag = true;
	        }
	    }
	}
}
