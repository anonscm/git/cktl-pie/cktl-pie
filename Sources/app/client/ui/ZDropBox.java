/*
 * Copyright COCKTAIL, 1995-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package app.client.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.text.JTextComponent;

public class ZDropBox extends JPanel implements DropTargetListener {
	private static DataFlavor	FILE_FLAVOR		= DataFlavor.javaFileListFlavor;
	private static DataFlavor	IMAGE_FLAVOR	= DataFlavor.imageFlavor;
	private JTextComponent		tc;

	public ZDropBox(JTextComponent tc) {
		super(new BorderLayout());

		this.tc = tc;
		add(tc, BorderLayout.CENTER);
		setBorder(BorderFactory.createEmptyBorder());
		new DropTarget(this, this);
	}

	public void dragEnter(DropTargetDragEvent dtde) {
		System.out.println("Enter !!!!!!!");
		setBorder(BorderFactory.createLineBorder(Color.RED));
	}

	public void dragExit(DropTargetEvent dte) {
		System.out.println("Exit !!!!!!!");
		setBorder(null);
	}

	public void dragOver(DropTargetDragEvent dtde) {
	}

	public void dropActionChanged(DropTargetDragEvent dtde) {
	}

	public void drop(DropTargetDropEvent evt) {
		System.out.println("Drop !!!!!!!");
		if (canDrop()) {
			try {
				final Transferable t = evt.getTransferable();
				if (t.isDataFlavorSupported(FILE_FLAVOR) || t.isDataFlavorSupported(IMAGE_FLAVOR)) {
					evt.acceptDrop(DnDConstants.ACTION_COPY_OR_MOVE);

					final List s = (List) t.getTransferData(DataFlavor.javaFileListFlavor);
					evt.getDropTargetContext().dropComplete(true);
					final File f = (File) s.get(0);
					tc.setText(f.getAbsolutePath());
				}
				else {
					evt.rejectDrop();
				}
			}
			catch (IOException e) {
				evt.rejectDrop();
			}
			catch (UnsupportedFlavorException e) {
				evt.rejectDrop();
			}
		}
		tc.setBorder(null);
	}

	public boolean canDrop() {
		return tc.isEditable();
	}

	// public void setEditable(boolean editable) {
	// tf.setEditable(editable);
	// }
	//
	// public String getText() {
	// return tf.getText();
	// }
	//
	// public void setText(String s) {
	// tf.setText(s);
	// }
	//
	// public void clean() {
	// tf.clean();
	// }
	//
	// public void showRequired(boolean show) {
	// tf.showRequired(show);
	// }
}
