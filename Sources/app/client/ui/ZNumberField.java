/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client.ui;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;

import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

import app.client.tools.Constants;

public class ZNumberField extends ZTextField {

	private static final NumberFormat	DEFAULT_FORMAT	= Constants.FORMAT_NUMBER;
	private NumberFormat				format;

	public ZNumberField() {
		super();
		init(null);
	}

	public ZNumberField(int cols) {
		super(cols);
		init(null);
	}

	public ZNumberField(NumberFormat format) {
		super();
		init(format);
	}

	public ZNumberField(int cols, NumberFormat format) {
		super(cols);
		init(format);
	}

	private void init(NumberFormat format) {
		if (format != null) {
			this.format = format;
		}
		else {
			this.format = DEFAULT_FORMAT;
		}
		setHorizontalAlignment(JTextField.RIGHT);
		addFocusListener(new NumberFocusListener());
	}

	public void setNumber(Number num) {
		String s = null;
		if (num != null) {
			s = format.format(num);
		}
		super.setText(s);
	}

	public void setNumber(Number num, boolean editable) {
		setNumber(num);
		setEditable(editable);
	}

	public Number getNumber() {
		String s = getText();
		if (s == null) {
			return null;
		}
		Number n = null;
		try {
			s = s.replace('.', ',');
			n = format.parse(s);
		}
		catch (ParseException e) {
		}
		return n;
	}

	public BigDecimal getBigDecimal() {
		Number n = getNumber();
		if (n == null) {
			return null;
		}
		return new BigDecimal(n.doubleValue()).setScale(format.getMaximumFractionDigits(), BigDecimal.ROUND_HALF_UP);
	}

	public Integer getInteger() {
		Number n = getNumber();
		if (n == null) {
			return null;
		}
		return new Integer(n.intValue());
	}

	private class NumberFocusListener implements FocusListener {
		public void focusGained(FocusEvent e) {
			((JTextComponent) e.getComponent()).selectAll();
		}

		public void focusLost(FocusEvent e) {
			makeItPretty();
		}

		private void makeItPretty() {
			String input = getText();
			if (input == null) {
				return;
			}
			Number n = null;
			// si colle deja au format, cooool...
			try {
				input = input.replace('.', ',');
				n = format.parse(input);
			}
			catch (ParseException e) {
				// sinon...
				n = null;
			}
			if (n == null) {
				requestFocus();
				selectAll();
			}
			else {
				String s = format.format(n);
				if (!s.equals(getText())) {
					setText(format.format(n));
				}
			}
		}
	}
}
