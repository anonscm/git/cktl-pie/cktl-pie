/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Enumeration;

import javax.swing.BoundedRangeModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableColumn;
import javax.swing.text.JTextComponent;

import app.client.tools.Constants;

import com.webobjects.eoapplication.EOInterfaceController;
import com.webobjects.eoapplication.client.EOClientResourceBundle;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.EOMasterDetailAssociation;
import com.webobjects.eointerface.EOTableAssociation;
import com.webobjects.eointerface.swing.EOImageView;
import com.webobjects.eointerface.swing.EOMatrix;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class DeprecatedUIUtilities {
	private static final EOClientResourceBundle	lesRessources	= new EOClientResourceBundle();

	/** Rendre une table (EOTable) non editable : a utiliser partout ou il y a des tables */
	public static void setNonEditableTable(EOTable aTable) {
		JTable actualTable = aTable.table();
		for (Enumeration e = actualTable.getColumnModel().getColumns(); e.hasMoreElements();)
			((TableColumn) e.nextElement()).setCellEditor(null);
	}

	public static void displayComponents(Component[] c) {
		for (int i = 0; i < c.length; i++) {
			Component one = c[i];
			if (one instanceof JTextField) {
				System.out.println("[JTextField] one = " + one);
			}
			else {
				if (one instanceof Container) {
					System.out.println("[Container] one = " + one);
					displayComponents(((Container) one).getComponents());
				}
				else {
					System.out.println("[Inconnu] one = " + one);
				}
			}
		}
	}

	/**
	 * Affecte une image a un bouton. L'image est prise dans les Web Server Resources de l'application.
	 * 
	 * @param sender
	 *            Controleur parent
	 * @param nomImage
	 *            Nom de l'image
	 * @param text
	 *            Text du bouton
	 * @param leBouton
	 *            Bouton a modifier
	 * @param toolTip
	 *            le toolTip
	 */
	public static void affecterImageEtTextAuBouton(EOInterfaceController sender, ImageIcon icon, String text, JButton button, String toolTip) {
		button.setIcon(icon);
		button.setText(text);
		button.setSelected(false);
		button.setFocusPainted(false);
		button.setToolTipText(toolTip);
		button.setDefaultCapable(false);
	}

	public static void affecterImageEtTextAuBouton(EOInterfaceController sender, String image, String text, JButton button, String toolTip) {
		ImageIcon icon = null;
		try {
			icon = (ImageIcon) lesRessources.getObject(image);
		}
		catch (Exception e) {
		}
		affecterImageEtTextAuBouton(sender, icon, text, button, toolTip);
	}

	public static void affecterImageEtTextAuBouton(EOInterfaceController sender, String image, String text, JButton button) {
		affecterImageEtTextAuBouton(sender, image, text, button, null);
	}

	public static void afficheTextField(EOView view, JTextField textfield) {
		view.removeAll();
		view.add(textfield);
		view.validate();
	}

	public static void setImageForImageView(String nomImage, EOImageView laVue) {
		laVue.setImageScaling(EOImageView.ScaleToFit);

		if (nomImage != null)
			laVue.setImage(((ImageIcon) lesRessources.getObject(nomImage)).getImage());
		else
			laVue.setImage(null);
	}

	public static void setTextComponentEditable(JTextComponent tc, boolean editable) {
		if (editable) {
			tc.setForeground(Constants.COLOR_TF_FGD_ENABLED);
			tc.setBackground(Constants.COLOR_TF_BGD_ENABLED);
		}
		else {
			tc.setForeground(Constants.COLOR_TF_FGD_DISABLED);
			tc.setBackground(Constants.COLOR_TF_BGD_DISABLED);
		}
		tc.setEditable(editable);
		tc.setFocusable(editable);
	}

	public static void setTextTextField(JTextField tf, String text) {
		tf.setText(text);
		tf.moveCaretPosition(0);
		tf.setToolTipText(null);
		if (text != null) {
			BoundedRangeModel tmpBoundedRangeModel = tf.getHorizontalVisibility();
			if (tmpBoundedRangeModel.getMaximum() > tf.getWidth()) {
				tf.setToolTipText(text);
			}
		}
	}

	public static void setTextTextField(JTextField tf, String text, boolean editable) {
		setTextTextField(tf, text);
		setTextComponentEditable(tf, editable);
	}

	public static void setTextTextField(JTextField tf, Date date, DateFormat formatter) {
		String text = null;
		if (date != null) {
			DateFormat f = formatter;
			if (f == null) {
				f = Constants.FORMAT_DATESHORT;
			}
			text = f.format(date);
		}
		setTextTextField(tf, text);
	}

	public static void setTextTextField(JTextField tf, Date date) {
		setTextTextField(tf, date, null);
	}

	public static void setTextTextField(JTextField tf, Date date, boolean editable) {
		setTextTextField(tf, date);
		setTextComponentEditable(tf, editable);
	}

	public static void setTextTextField(JTextField tf, Number number) {
		String text = null;
		if (number != null) {
			text = Constants.FORMAT_NUMBER.format(number);
		}
		setTextTextField(tf, text);
	}

	public static void setTextTextField(JTextField tf, Number number, boolean editable) {
		setTextTextField(tf, number);
		setTextComponentEditable(tf, editable);
	}

	public static void showRequired(JTextField tf, boolean show) {
		if (tf.isEditable()) {
			if (show) {
				tf.setBackground(Constants.COLOR_BGD_REQUIRED);
			}
			else {
				setTextComponentEditable(tf, tf.isEditable());
			}
		}
	}

	public static void showRequired(JComboBox cb, boolean show) {
		if (cb.isOpaque()) {
			if (show) {
				cb.setBackground(Constants.COLOR_BGD_REQUIRED);
			}
			else {
				cb.setBackground(new Color(0, 0, 0, 0));
			}
		}
	}

	public static void showRequired(EOMatrix mat, boolean show) {
		// marche pas :-( on fait rien...
		// if (mat.isOpaque()) {
		// NSArray components = new NSArray(mat.getComponents());
		// for (int i = 0; i < components.count(); i++) {
		// if (show) {
		// ((AbstractButton) components.objectAtIndex(i)).setBackground(Constants.COLOR_TF_BGD_REQUIRED);
		// }
		// else {
		// ((AbstractButton) components.objectAtIndex(i)).setBackground(Constants.COLOR_TRANSPARENT);
		// }
		// }
		// }
	}

	public static void cleanTextField(JTextField tf) {
		tf.setText(null);
		tf.setToolTipText(null);
	}

	public static void cleanTextField(JTextField tf, boolean editable) {
		cleanTextField(tf);
		setTextComponentEditable(tf, editable);
	}

	public static void selectAndFocusTextField(JTextField tf) {
		tf.selectAll();
		tf.setFocusable(true);
		tf.requestFocus();
	}

	public static void addInPanel(JPanel panel, String label, Component component) {
		JLabel l = new JLabel(label, JLabel.TRAILING);
		panel.add(l);
		l.setLabelFor(component);
		panel.add(component);
	}

	public static void addInPanel(JPanel panel, String[] labels, Component[] components) {
		if (panel == null || labels == null || components == null || labels.length != components.length) {
			System.err.println("[UIUtilities:addInPanel(JPanel, String[], Component[])] erreur... panel = " + panel + ", labels = " + labels
					+ ", components = " + components);
			return;
		}
		for (int i = 0; i < labels.length; i++) {
			addInPanel(panel, labels[i], components[i]);
		}
	}

	public static String getString(JTextField tf) {
		String s = tf.getText();
		if (s == null || s.trim().length() == 0) {
			return null;
		}
		return s.trim();
	}

	public static String getStringNotNull(JTextField tf) throws Exception {
		String s = getString(tf);
		if (s == null) {
			throw new Exception("Le champ est vide!");
		}
		return s;
	}

	public static NSTimestamp getNSTimestamp(String s, DateFormat formatter) throws Exception {
		if (s == null) {
			return null;
		}
		try {
			return new NSTimestamp(formatter.parse(s));
		}
		catch (ParseException e) {
			System.err.println("Erreur de format de date: " + s + " - " + e);
			throw new Exception("Erreur de format de date: " + s);
		}
	}

	public static NSTimestamp getNSTimestamp(JTextField tf, DateFormat formatter) throws Exception {
		String s = getString(tf);
		return getNSTimestamp(s, formatter);
	}

	public static NSTimestamp getNSTimestamp(JTextField tf) throws Exception {
		return getNSTimestamp(tf, Constants.FORMAT_DATESHORT);
	}

	public static NSTimestamp getNSTimestampNotNull(String s, DateFormat formatter) throws Exception {
		NSTimestamp res = getNSTimestamp(s, formatter);
		if (res == null) {
			throw new Exception("Date vide!");
		}
		return res;
	}

	public static NSTimestamp getNSTimestampNotNull(JTextField tf, DateFormat formatter) throws Exception {
		NSTimestamp res = getNSTimestamp(tf, formatter);
		if (res == null) {
			throw new Exception("Champ date vide!");
		}
		return res;
	}

	public static NSTimestamp getNSTimestampNotNull(JTextField tf) throws Exception {
		NSTimestamp res = getNSTimestamp(tf);
		if (res == null) {
			throw new Exception("Champ date vide!");
		}
		return res;
	}

	public static Number getNumber(String s) throws Exception {
		if (s == null) {
			return null;
		}
		try {
			return Constants.FORMAT_NUMBER.parse(s);
		}
		catch (ParseException e) {
			System.err.println("Erreur de format de nombre: " + s + " - " + e);
			throw new Exception("Erreur de format de nombre: " + s);
		}
	}

	public static Number getNumber(JTextField tf) throws Exception {
		String s = getString(tf);
		return getNumber(s);
	}

	public static Number getNumbernotNull(JTextField tf) throws Exception {
		Number n = getNumber(tf);
		if (n == null) {
			throw new Exception("Champ nombre vide!");
		}
		return n;
	}

	public static void selectMatrix(EOMatrix m, int i) {
		NSArray components = new NSArray(m.getComponents());
		((JRadioButton) components.objectAtIndex(i)).setSelected(true);
	}

	public static void selectMatrix(EOMatrix m, int i, boolean enabled) {
		selectMatrix(m, i);
		m.setEnabled(enabled);
	}

	public static void selectMatrix(EOMatrix m, String s) {
		NSArray components = new NSArray(m.getComponents());
		for (int i = 0; i < components.count(); i++) {
			if (((JRadioButton) components.objectAtIndex(i)).getText().equals(s)) {
				((JRadioButton) components.objectAtIndex(i)).setSelected(true);
				break;
			}
		}
	}

	public static void selectMatrix(EOMatrix m, String s, boolean enabled) {
		selectMatrix(m, s);
		m.setEnabled(enabled);
	}

	public static int selectedIndexMatrix(EOMatrix m) {
		NSArray components = new NSArray(m.getComponents());
		int btnSelectionne;
		for (btnSelectionne = 0; btnSelectionne < components.count(); btnSelectionne++) {
			if (((JRadioButton) components.objectAtIndex(btnSelectionne)).isSelected())
				break;
		}
		return btnSelectionne;
	}

	public static String selectedTextMatrix(EOMatrix m) {
		return ((JRadioButton) m.getComponents()[selectedIndexMatrix(m)]).getText();
	}

	/**
	 * Informe les EOTableAssociation et les EOMasterDetailAssociation que le DisplayGroup peut avoir change // Il arrive que les associations ne
	 * soient pas correctement mises a jour
	 */
	public static void informObservingAssociations(EODisplayGroup aTable) {
		NSArray observingAssociations = aTable.observingAssociations();

		// Informer les EOTableAssociation et EOMasterDetailAssociation
		for (int i = 0; i < (observingAssociations.count()); i++) {
			if (observingAssociations.objectAtIndex(i).getClass().getName().equals("com.webobjects.eointerface.EOTableAssociation"))
				((EOTableAssociation) observingAssociations.objectAtIndex(i)).subjectChanged();

			if (observingAssociations.objectAtIndex(i).getClass().getName().equals("com.webobjects.eointerface.EOMasterDetailAssociation")) {
				EOMasterDetailAssociation anAssociation = (EOMasterDetailAssociation) observingAssociations.objectAtIndex(i);
				anAssociation.subjectChanged();
				// Informer les observers de l'objet detail
				informObservingAssociations((EODisplayGroup) anAssociation.object());
			}
		}
	}

	/** Place une vue dans une autre. La swapView et la vue sont passes en parametres */
	public static void putView(EOView maSwapView, EOView maView) {
		maSwapView.getComponent(0).setVisible(false);
		maSwapView.removeAll();
		maSwapView.setLayout(new BorderLayout());
		maSwapView.add(maView, BorderLayout.CENTER);
		maView.setVisible(true);
		maSwapView.validate();
	}

	public static void refreshDisplayGroup(EODisplayGroup eod, EOGenericRecord currentRecord) {
		if (currentRecord != null) {
			eod.setSelectedObject(null);
			DeprecatedUIUtilities.informObservingAssociations(eod);
			eod.setSelectedObject(currentRecord);
		}
		DeprecatedUIUtilities.informObservingAssociations(eod);
	}

	public static void refreshDisplayGroupAndTable(EODisplayGroup eod, EOGenericRecord currentRecord, EOTable table) {
		refreshDisplayGroup(eod, currentRecord);
		if (eod.selectionIndexes().count() > 0)
			table.table().changeSelection(((Number) eod.selectionIndexes().objectAtIndex(0)).intValue(), 0, false, false);
	}

}
