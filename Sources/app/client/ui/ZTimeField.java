/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client.ui;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.text.JTextComponent;

import app.client.tools.Constants;

import com.webobjects.foundation.NSTimestamp;

public class ZTimeField extends ZTextField {

	private static final DateFormat	DEFAULT_FORMAT	= Constants.FORMAT_TIMESHORT;
	private DateFormat				format;

	public ZTimeField(int cols) {
		super(cols);
		init(null);
	}

	public ZTimeField(int cols, DateFormat format) {
		super(cols);
		init(format);
	}

	private void init(DateFormat format) {
		if (format != null) {
			this.format = format;
		}
		else {
			this.format = DEFAULT_FORMAT;
		}
		addFocusListener(new TimeFocusListener());
	}

	public void setTime(Date time) {
		String s = null;
		if (time != null) {
			s = format.format(time);
		}
		super.setText(s);
	}

	public void setTime(Date time, boolean editable) {
		setTime(time);
		setEditable(editable);
	}

	public Date getTime() {
		String s = getText();
		if (s == null) {
			return null;
		}
		Date d = null;
		try {
			d = format.parse(s);
		}
		catch (ParseException e) {
		}
		return d;
	}

	public NSTimestamp getNSTimestamp() {
		return new NSTimestamp(getTime());
	}

	private class TimeFocusListener implements FocusListener {
		public void focusGained(FocusEvent e) {
			((JTextComponent) e.getComponent()).selectAll();
		}

		public void focusLost(FocusEvent e) {
			makeItPretty();
		}

		private void makeItPretty() {
			String input = getText();
			if (input == null) {
				return;
			}
			Date d = null;
			// si colle deja au format, cooool...
			try {
				d = format.parse(input);
			}
			catch (ParseException e) {
				// sinon...
				d = timeCompletion(input);
			}
			if (d == null) {
				requestFocus();
				selectAll();
			}
			else {
				setText(format.format(d));
			}
		}

		public Date timeCompletion(String aTime) {
			if (aTime == null) {
				return new Date();
			}
			aTime = aTime.replaceAll("\\D", "");
			if (aTime.length() == 0) {
				return new Date();
			}
			String second = "00", minute = "00", heure = "00";
			switch (aTime.length()) {
			case 0:
				break;
			case 1:
				heure = "0" + aTime;
				break;
			case 2:
				heure = aTime;
				break;
			case 3:
				heure = aTime.substring(0, 2);
				minute = "0" + aTime.substring(2);
				break;
			case 4:
				heure = aTime.substring(0, 2);
				minute = aTime.substring(2);
				break;
			case 5:
				heure = aTime.substring(0, 2);
				minute = aTime.substring(2).substring(0, 2);
				second = "0" + aTime.substring(4);
				break;
			default:
				heure = aTime.substring(0, 2);
				minute = aTime.substring(2).substring(0, 2);
				second = aTime.substring(4).substring(0, 2);
			}
			try {
				return new SimpleDateFormat("HHmmss").parse(heure + minute + second);
			}
			catch (ParseException e) {
			}
			return null;
		}
	}

}
