/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client.ui;

import java.text.Format;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComponent;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSKeyValueCodingAdditions;

/**
 * Classe pour creer un modele pour les listes deroulantes.<br>
 * Ce modele permet d'affecter un tableau d'objets respectant l'interface NSKeyValueCoding (par exmple un NSArray d'EOEnterpriseObjects ou
 * d'EOGenericRecord), en specifiant la cle a utiliser pour afficher les valeurs dans la liste deroulante.<br>
 * <br>
 * Les Exemple:<br>
 * <code>
 * ...<br>
 * 		NSArray res = fetchDataCatalogues(currentFournis); //res contient un tableau d'EOGenericRecord<br>
 *		ZEOComboBoxModel myZEOComboBoxModel = new ZEOComboBoxModel(res, "catLibelle");  //le combobox affichera le contenu de l'attribut catLibelle<br>
 *		ldCatalogues.setModel(myZEOComboBoxModel); //ldCatalogue est un JComboBox<br>
 *		if ((res!=null) && (res.count()>0)) {<br>
 *			myZEOComboBoxModel.setSelectedEObject(res.objectAtIndex(0));<br>
 *		}<br>
 *		<br>
 *...<br>
 * </code>
 * 
 * @author rprin
 */
public class ZEOComboBoxModel extends DefaultComboBoxModel {
	private ArrayList			myObjects;
	private final String		displayKey;
	private final String		nullValue;
	private final Format		displayFormat;
	private final JComponent	componentForComplement;

	/**
	 * Contructeur du modele.
	 * 
	 * @param objects
	 *            Tableau contenant des objets implementant l'interface NSKeyValueCoding.
	 * @param displayKey
	 *            Nom de la propriete qui doit permettre d'afficher le libelle de chaque element de la liste deroulante (en NSKeyValueCoding).
	 *            Sera affichee avec valueForKey(). Si null, la valeur affichee sera obtenue via un toString().
	 * @param labelForNullValue
	 *            si non null, une valeur sera affichee en debut de modele et getSelectedEOObject renverra null si selectionne.
	 * @param displayFormat
	 * @param componentForComplement
	 *            le component a utiliser (typiquement a rendre visible, comme un textfield) si on veut un complement en fonction de la Selection
	 *            du combobox
	 */
	public ZEOComboBoxModel(NSArray objects, String displayKey, String labelForNullValue, Format displayFormat, JComponent componentForComplement)
			throws Exception {
		super();
		this.displayKey = displayKey;
		this.nullValue = labelForNullValue;
		this.displayFormat = displayFormat;
		this.componentForComplement = componentForComplement;
		updateListWithData(objects);
	}

	/**
	 * Renvoie l'objet correspondant a la Selection.
	 */
	public NSKeyValueCoding getSelectedEOObject() {
		if (super.getSelectedItem() == null) {
			return null;
		}
		final int i = super.getIndexOf(super.getSelectedItem());
		if (i == -1) {
			return null;
		}
		return (NSKeyValueCoding) myObjects.get(i);
	}

	/**
	 * Definit quel est l'element qui doit etre selectionne dans la liste deroulante.
	 * 
	 * @param selectedObject
	 */
	public void setSelectedEOObject(final NSKeyValueCoding selectedObject) {
		if (selectedObject == null) {
			if (nullValue == null) {
				super.setSelectedItem(null);
			}
			else {
				super.setSelectedItem(nullValue);
			}
		}
		else {
			Object obj = null;
			if (displayKey != null && displayKey.indexOf(".") > 0) {
				obj = ((NSKeyValueCodingAdditions) selectedObject).valueForKeyPath(displayKey);
			}
			else {
				obj = selectedObject.valueForKey(displayKey);
			}

			if (displayFormat == null) {
				super.setSelectedItem(obj.toString());
			}
			else {
				super.setSelectedItem(displayFormat.format(obj));
			}

		}
	}

	public JComponent getComponentForComplement() {
		return componentForComplement;
	}

	public void setObjects(NSArray objects) throws Exception {
		updateListWithData(objects);
	}

	/**
	 * Met a jour les donnees.
	 * 
	 * @param objects
	 *            Tableau contenant des objets implementant l'interface NSKeyValueCoding.
	 */

	private final void updateListWithData(NSArray objects) throws Exception {
		this.removeAllElements();

		myObjects = new ArrayList();

		// Gerer l'affichage ou non d'un element null
		if (nullValue != null) {
			myObjects.add(null);
			this.addElement(nullValue);
		}

		if (objects == null) {
			return;
		}

		if (displayKey != null) {
			objects = EOSortOrdering.sortedArrayUsingKeyOrderArray(objects, new NSArray(EOSortOrdering.sortOrderingWithKey(displayKey,
					EOSortOrdering.CompareCaseInsensitiveAscending)));
		}

		// Gerer l'affichage des infos
		for (int i = 0; i < objects.objects().length; i++) {
			myObjects.add(objects.objects()[i]);
		}

		for (int i = 0; i < objects.count(); i++) {
			final Object tmp = objects.objectAtIndex(i);
			if (!(tmp instanceof NSKeyValueCoding)) {
				throw new Exception("L'objet ne gere pas l'interface NSKeyValueCoding.");
			}
			if (displayKey == null) {
				this.addElement(((NSKeyValueCoding) tmp).toString());
			}
			else {
				Object obj = null;
				if (displayKey.indexOf(".") > 0) {
					obj = ((NSKeyValueCodingAdditions) tmp).valueForKeyPath(displayKey);
				}
				else {
					obj = ((NSKeyValueCoding) tmp).valueForKey(displayKey);
				}
				if (displayFormat == null) {
					if (obj != null) {
						this.addElement(obj.toString());
					}
					else {
						this.addElement(((NSKeyValueCoding) tmp).toString());
					}
				}
				else {
					this.addElement(displayFormat.format(obj));
				}
			}
		}
	}

}
