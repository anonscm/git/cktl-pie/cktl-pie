/*
CRI - Universite de La Rochelle, 2006

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client.ui.table;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;


import app.client.tools.Constants;
import app.client.ui.table.ZTablePanel.IZTablePanelMdl;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public abstract class ZExtendedTablePanel extends JPanel {

	protected abstract void onAdd();

	protected abstract void onUpdate();

	protected abstract void onDelete();

	protected abstract void onSelectionChanged();

	private ZTablePanel	table;
	public Action				actionAdd, actionUpdate, actionDelete;

	private String				label;

	private boolean				addEnabled, updateEnabled, deleteEnabled;
	private boolean				editable;

	public ZExtendedTablePanel(String label) {
		init(label, true, true, true);
	}

	public ZExtendedTablePanel(String label, boolean addEnabled, boolean updateEnabled, boolean deleteEnabled) {
		init(label, addEnabled, updateEnabled, deleteEnabled);
	}

	private void init(String label, boolean addEnabled, boolean updateEnabled, boolean deleteEnabled) {
		this.label = label;
		this.addEnabled = addEnabled;
		this.updateEnabled = updateEnabled;
		this.deleteEnabled = deleteEnabled;

		actionAdd = new ActionAdd();
		actionUpdate = new ActionUpdate();
		actionDelete = new ActionDelete();

		table = new TablePanel(new TableListener());
	}

	public void initGUI() {
		initTable();
		initGUI(label, addEnabled, updateEnabled, deleteEnabled);
	}

	public void clean() {
		setObjectArray(null);
	}

	public void clearCols() {
		table.cols.clear();
	}

	public void add(ZEOTableModelColumn col) {
		table.cols.add(col);
	}

	public void addCol(ZEOTableModelColumn col) {
		table.cols.add(col);
	}

	public void setObjectArray(NSArray objectArray) {
		table.setObjectArray(objectArray);
	}

	public NSArray displayedObjects() {
		return table.displayedObjects();
	}

	public EOGenericRecord selectedObject() {
		return (EOGenericRecord) table.selectedObject();
	}

	public boolean setSelectedObject(EOGenericRecord eo) {
		return table.setSelectedObject(eo);
	}

	public NSArray selectedObjects() {
		return table.selectedObjects();
	}

	public void setSelectedObjects(NSArray objects) {
		table.setSelectedObjects(objects);
	}

	public void setFilteringQualifier(EOQualifier qualifier) {
		table.setFilteringQualifier(qualifier);
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
		table.setEnabled(editable);
		actionAdd.setEnabled(editable);
		updateButtons();
	}

	public boolean isEditable() {
		return editable;
	}

	public final void showRequired(boolean show) {
		table.showRequired(show);
	}

	public EODisplayGroup getDG() {
		return table.getDG();
	}

	// default action - overwrite pour customiser la reponse au double-clique...
	public void onDbClick(final MouseEvent e) {
		actionUpdate.actionPerformed(null);
	}

	/**
	 * met a jour les donnees depuis le displayGroup (donc celles deja existantes) et force la Selection (declenche le onSelectionChanged)
	 */
	public void updateData() {
		table.updateData();
	}

	public ZTablePanel getTablePanel() {
		return table;
	}

	private void initGUI(String label, boolean addEnabled, boolean updateEnabled, boolean deleteEnabled) {
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder());

		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.PAGE_AXIS));
		buttonsPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

		Dimension d = new Dimension(20, 20);
		if (addEnabled) {
			JButton btAdd = new JButton(actionAdd);
			btAdd.setPreferredSize(d);
			btAdd.setMaximumSize(d);
			btAdd.setMinimumSize(d);
			btAdd.setHorizontalAlignment(SwingConstants.CENTER);
			btAdd.setBorderPainted(false);
			btAdd.setFocusPainted(false);
			buttonsPanel.add(btAdd);
		}
		if (updateEnabled) {
			JButton btUpdate = new JButton(actionUpdate);
			btUpdate.setPreferredSize(d);
			btUpdate.setMaximumSize(d);
			btUpdate.setMinimumSize(d);
			btUpdate.setHorizontalAlignment(SwingConstants.CENTER);
			btUpdate.setBorderPainted(false);
			btUpdate.setFocusPainted(false);
			buttonsPanel.add(btUpdate);
		}
		if (deleteEnabled) {
			JButton btDelete = new JButton(actionDelete);
			btDelete.setPreferredSize(d);
			btDelete.setMaximumSize(d);
			btDelete.setMinimumSize(d);
			btDelete.setHorizontalAlignment(SwingConstants.CENTER);
			btDelete.setBorderPainted(false);
			btDelete.setFocusPainted(false);
			buttonsPanel.add(btDelete);
		}

		if (label != null) {
			JLabel jl = new JLabel(label);
			jl.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);
			add(jl, BorderLayout.NORTH);
		}
		add(table, BorderLayout.CENTER);
		add(buttonsPanel, BorderLayout.LINE_END);

		validate();
	}

	private void updateButtons() {
		// actionAdd.setEnabled(editable);
		actionUpdate.setEnabled(editable && table.selectedObject() != null);
		actionDelete.setEnabled(editable && table.selectedObject() != null);
	}

	private void initTable() {
		table.initGUI();

		table.getTable().setBackground(Constants.COLOR_COL_BGD_NORMAL);
		table.getTable().setSelectionBackground(Constants.COLOR_COL_BGD_SELECTED);
		table.getTable().setForeground(Constants.COLOR_COL_FGD_NORMAL);
		table.getTable().setSelectionForeground(Constants.COLOR_COL_FGD_SELECTED);
	}


	private final class TablePanel extends ZTablePanel {

		public TablePanel(ZTablePanel.IZTablePanelMdl listener) {
			super(listener);
			cols.clear();
		}
	}

	private final class TableListener implements IZTablePanelMdl {
		public void selectionChanged() {
			updateButtons();
			onSelectionChanged();
		}

		public void onDbClick(final MouseEvent e) {
			ZExtendedTablePanel.this.onDbClick(e);
		}
	}

	private class ActionAdd extends AbstractAction {
		public ActionAdd() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_ADD_16_LIGHTENED);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onAdd();
			}
		}
	}

	private class ActionUpdate extends AbstractAction {
		public ActionUpdate() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_UPDATE_16_LIGHTENED);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onUpdate();
			}
		}
	}

	private class ActionDelete extends AbstractAction {
		public ActionDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_16_LIGHTENED);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onDelete();
			}
		}
	}

	public void ajouterInDg(Object detail, int row) {
		table.ajouterInDg(detail, row);
	}

	public void fireSelectedTableRowUpdated() {
		table.fireSelectedTableRowUpdated();
	}

	public void deleteSelectionInDg() {
		table.deleteSelectionInDg();
	}

}
