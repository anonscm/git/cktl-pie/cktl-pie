/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client.ui.table;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumn;

import app.client.tools.Constants;
import app.client.tools.TableSorter;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author rodolphe.prin@univ-lr.fr
 */
public abstract class ZTablePanel extends JPanel implements ZEOTable.ZEOTableListener {
	protected IZTablePanelMdl		ctrl;

	protected ZEOTable				myEOTable;
	protected ZEOTableModel			myTableModel;
	protected final EODisplayGroup	displayGroup	= new EODisplayGroup();
	protected TableSorter			myTableSorter;
	protected Vector				cols			= new Vector();

	public ZTablePanel(IZTablePanelMdl listener) {
		super();
		ctrl = listener;
	}

	protected void initTableModel() {
		myTableModel = new ZEOTableModel(displayGroup, cols);
		myTableSorter = new TableSorter(myTableModel);
	}

	/**
	 * Initialise la table et affiche (le modele doit exister)
	 */
	protected void initTable() {
		myEOTable = new ZEOTable(myTableSorter);
		myEOTable.addListener(this);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());
		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}

	public void initGUI() {
		initTableModel();
		initTable();
		setLayout(new BorderLayout());
		add(new JScrollPane(myEOTable), BorderLayout.CENTER);
	}

	public void setTableModel(ZEOTableModel mdl) {
		myTableModel = mdl;
		myEOTable.setModel(mdl);
	}

	public void setTableListener(IZTablePanelMdl listener) {
		ctrl = listener;
	}
	
	
	/**
	  @param selectionMode
	             One of ListSelectionModel.SINGLE_SELECTION / ListSelectionModel.MULTIPLE_INTERVAL_SELECTION /
	             ListSelectionModel.SINGLE_INTERVAL_SELECTION
	 */
	
	public void setSelectionMode(int selectionMode) {
		if (myEOTable != null) {
			myEOTable.setSelectionMode(selectionMode);
		}
	}

	public void updateData() {
		myEOTable.updateData();
	}

	public static interface IZTablePanelMdl {
		public void selectionChanged();

		public void onDbClick(final MouseEvent e);
	}

	public void onDbClick(final MouseEvent e) {
		ctrl.onDbClick(e);
	}

	public void onSelectionChanged() {
		ctrl.selectionChanged();
	}

	// managing display group...
	public void setObjectArray(NSArray objects) {
		displayGroup.setObjectArray(objects);
		this.updateData();
	}

	public void setFilteringQualifier(EOQualifier qualifier) {
		displayGroup.setQualifier(qualifier);
		displayGroup.updateDisplayedObjects();
		this.updateData();
	}

	public EOEnterpriseObject selectedObject() {
		return (EOEnterpriseObject) myEOTable.getSelectedObject();
	}

	public boolean setSelectedObject(EOEnterpriseObject object) {
		if (object == null) {
			myEOTable.forceNewSelection((NSArray) null);
			return true;
		}
		for (int i = 0; i < displayGroup.displayedObjects().count(); i++) {
			EOEnterpriseObject eo = (EOEnterpriseObject) myEOTable.getObjectAtRow(i);
			if (eo.equals(object)) {
				myEOTable.forceNewSelection(new NSArray(new Integer(i)));
				return true;
			}
		}
		return false;
	}

	public NSArray selectedObjects() {
		return displayGroup.selectedObjects();
	}

	public void setSelectedObjects(NSArray objects) {
		if (objects == null || objects.count() == 0) {
			myEOTable.forceNewSelection((NSArray) null);
			return;
		}
		if (myEOTable.getSelectionModel().getSelectionMode() == ListSelectionModel.SINGLE_SELECTION || objects.count() == 1) {
			setSelectedObject((EOEnterpriseObject) objects.objectAtIndex(0));
			return;
		}
		NSMutableArray ma = new NSMutableArray();
		int count = displayGroup.displayedObjects().count();
		for (int i = 0; i < count; i++) {
			EOEnterpriseObject eo = (EOEnterpriseObject) myEOTable.getObjectAtRow(i);
			if (objects.containsObject(eo)) {
				ma.addObject(new Integer(i));
			}
		}
		myEOTable.forceNewSelection(ma);
	}

	public NSArray displayedObjects() {
		return displayGroup.displayedObjects();
	}

	public int selectedRowIndex() {
		return myEOTable.getSelectedRow();
	}

	public Object getObjectAtRow(final int rowIndex) {
		return myEOTable.getObjectAtRow(rowIndex);
	}

	public void showRequired(boolean show) {
		if (show) {
			myEOTable.getTableHeader().setBackground(Constants.COLOR_BGD_REQUIRED);
		}
		else {
			myEOTable.getTableHeader().setBackground(null);
		}
	}

	/**
	 * Annule la saisie en cours dans la cellule si celle-ci est en mode edition.
	 */
	public void cancelCellEditing() {
		myEOTable.cancelCellEditing();
	}

	public boolean isEditing() {
		return myEOTable.isEditing();
	}

	/**
	 * Termine l'edition en cours dans la cellule eventuellement en mode edition.
	 * 
	 * @return True si l'edition a bien ete validee.
	 */
	public boolean stopCellEditing() {
		return myEOTable.stopCellEditing();
	}

	// only for short writing
	public EODisplayGroup getDG() {
		return getDisplayGroup();
	}

	public EODisplayGroup getDisplayGroup() {
		return displayGroup;
	}

	public void fireTableDataChanged() {
		myTableModel.fireTableDataChanged();
	}

	public void fireTableCellUpdated(final int row, final int col) {
		myEOTable.getDataModel().fireTableCellUpdated(row, myEOTable.convertColumnIndexToModel(col));
	}

	public void fireTableRowUpdated(final int row) {
		myEOTable.getDataModel().fireTableRowUpdated(row);
	}

	public void fireSelectedTableRowUpdated() {
		myEOTable.getDataModel().fireTableRowUpdated(myEOTable.getSelectedRow());
	}

	public ZEOTable getTable() {
		return myEOTable;
	}

	/**
	 * 
	 * @param columnModelIndex
	 * @return
	 */
	public TableColumn getTableColumnAtIndex(int columnModelIndex) {
		final Enumeration enumeration = myEOTable.getColumnModel().getColumns();
		for (; enumeration.hasMoreElements();) {
			final TableColumn col = (TableColumn) enumeration.nextElement();
			if (col.getModelIndex() == columnModelIndex) {
				return col;
			}
		}
		return null;
	}

	public void ajouterInDg(Object detail, int row) {
		displayGroup.insertObjectAtIndex(detail, row);
		displayGroup.setSelectedObject(detail);

		myTableModel.updateInnerRowCount();
		myTableModel.fireTableDataChanged();

		myEOTable.requestFocusInWindow();
		if (myEOTable.editCellAt(row, 0)) {
			myEOTable.changeSelection(row, 0, false, false);
		}

	}

	public void deleteSelectionInDg() {
		displayGroup.deleteSelection();

		myTableModel.updateInnerRowCount();
		myTableModel.fireTableDataChanged();
	}

}
