/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import app.client.ApplicationClient;
import app.client.tools.Constants;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;

/**
 * Classe dont doivent heriter les boites de dialoques de l'application.
 * 
 * @author Rodolphe Prin
 */
public class ZDialog extends JDialog {
	public static final int		MROK		= 1;
	public static final int		MRCANCEL	= 0;
	protected ApplicationClient	app;
	protected EOEditingContext	ec;
	protected int				modalResult;
	private BusyGlassPanel		myBusyGlassPanel;
	protected Action			defaultOkAction, defaultCancelAction, defaultCloseAction;

	/**
	 * @param owner
	 * @param title
	 * @param modal
	 * @throws java.awt.HeadlessException
	 */
	public ZDialog(Dialog owner, String title, boolean modal) throws HeadlessException {
		super(owner, title, modal);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		app = (ApplicationClient) EOApplication.sharedApplication();
		ec = app.editingContext();
		defaultOkAction = new ActionOk();
		defaultCancelAction = new ActionCancel();
		defaultCloseAction = new ActionClose();
		myBusyGlassPanel = new BusyGlassPanel();
		setGlassPane(myBusyGlassPanel);
	}

	/**
	 * @param owner
	 * @param title
	 * @param modal
	 * @throws java.awt.HeadlessException
	 */
	public ZDialog(Frame owner, String title, boolean modal) throws HeadlessException {
		super(owner, title, modal);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		app = (ApplicationClient) EOApplication.sharedApplication();
		ec = app.editingContext();
		defaultOkAction = new ActionOk();
		defaultCancelAction = new ActionCancel();
		defaultCloseAction = new ActionClose();
		myBusyGlassPanel = new BusyGlassPanel();
		setGlassPane(myBusyGlassPanel);
	}

	public int getModalResult() {
		return modalResult;
	}

	public void setModalResult(int i) {
		modalResult = i;
	}

	protected void onOkClick() {
		setModalResult(MROK);
		hide();
	}

	protected void onCancelClick() {
		setModalResult(MRCANCEL);
		hide();
	}

	protected void onCloseClick() {
		setModalResult(MRCANCEL);
		hide();
	}

	public EOEditingContext getDefaultEditingContext() {
		return app.editingContext();
	}

	/**
	 * Renvoie l'editing context du composant. Par defaut, renvoie getDefaultEditingContext. A Surcharger si besoin d'un autre...
	 */
	public EOEditingContext getEditingContext() {
		return getDefaultEditingContext();
	}

	public void setWaitCursor(boolean bool) {
		if (bool) {
			this.getGlassPane().setVisible(true);
		}
		else {
			this.getGlassPane().setVisible(false);
		}
	}

	public boolean open() {
		setModalResult(MRCANCEL);
		centerWindow();
		show();
		// en modal la fenetre reste active jusqu'au closeWindow...
		return getModalResult() == MROK;
	}

	public final void centerWindow() {
		int screenWidth = (int) this.getGraphicsConfiguration().getBounds().getWidth();
		int screenHeight = (int) this.getGraphicsConfiguration().getBounds().getHeight();
		this.setLocation((screenWidth / 2) - ((int) this.getSize().getWidth() / 2),
				((screenHeight / 2) - ((int) this.getSize().getHeight() / 2)));

	}

	public class ActionOk extends AbstractAction {
		public ActionOk() {
			super("OK");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
		}

		public void actionPerformed(ActionEvent e) {
			onOkClick();
		}
	}

	public class ActionCancel extends AbstractAction {
		public ActionCancel() {
			super("Annuler");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CANCEL_16);
		}

		public void actionPerformed(ActionEvent e) {
			onCancelClick();
		}
	}

	public class ActionClose extends AbstractAction {
		public ActionClose() {
			super("Fermer");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CLOSE_16);
		}

		public void actionPerformed(ActionEvent e) {
			onCloseClick();
		}
	}

	/**
	 * Permet de definir un panel qui sert a afficher un waitcursor et a interdire toute modif sur le panel.
	 * 
	 * @author rprin
	 */
	public final class BusyGlassPanel extends JPanel {
		public final Color	COLOR_WASH	= new Color(64, 64, 64, 32);

		public BusyGlassPanel() {
			super.setOpaque(false);
			super.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

			// Suck up them events!!!
			super.addKeyListener((new KeyAdapter() {
			}));
			super.addMouseListener((new MouseAdapter() {
			}));
			super.addMouseMotionListener((new MouseMotionAdapter() {
			}));
		}

		public final void paintComponent(Graphics p_graphics) {
			Dimension l_size = super.getSize();

			// Wash the pane with translucent gray.
			p_graphics.setColor(COLOR_WASH);
			p_graphics.fillRect(0, 0, l_size.width, l_size.height);

			// Paint a grid of white/black dots.
			p_graphics.setColor(Color.white);
			for (int j = 3; j < l_size.height; j += 8) {
				for (int i = 3; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
			p_graphics.setColor(Color.black);
			for (int j = 4; j < l_size.height; j += 8) {
				for (int i = 4; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
		}
	}

	/**
	 * @param actions
	 * @return Une box contenant les boutons construits a partir des actions.
	 */
	public static Box buildHorizontalButtonsFromActions(ArrayList actions) {
		Box tmpBox = Box.createHorizontalBox();
		// Remplacer la ligne suivante par un
		// Box.createHorizontalStrut(2);
		tmpBox.add(Box.createRigidArea(new Dimension(2, 40)));
		tmpBox.add(Box.createGlue());
		Iterator iterator = actions.iterator();
		while (iterator.hasNext()) {
			Action element = (Action) iterator.next();
			JButton button = new JButton(element);
			button.setMinimumSize(new Dimension(100, 30));
			button.setPreferredSize(new Dimension(100, 30));
			button.setSize(button.getPreferredSize());
			tmpBox.add(button);
			tmpBox.add(Box.createRigidArea(new Dimension(25, 30)));
		}
		return tmpBox;
	}

	/**
	 * Construit un panel de boutons a partir d'une liste d'actions.
	 * 
	 * @param actions
	 * @return
	 */
	public static JPanel buildVerticalPanelOfButtonsFromActions(ArrayList actions) {
		JPanel p = new JPanel();
		Iterator iterator = actions.iterator();
		GridLayout thisLayout = new GridLayout(actions.size(), 1);
		thisLayout.setVgap(20);
		p.setLayout(thisLayout);
		while (iterator.hasNext()) {
			Action element = (Action) iterator.next();
			if (element != null) {
				JButton button = new JButton(element);
				button.setHorizontalAlignment(JButton.LEFT);
				p.add(button);
			}
			else {
				p.add(new JPanel(new BorderLayout()));
			}
		}
		return p;
	}

	public static JPanel buildVerticalPanelOfButtonsFromActions(Action[] actions) {
		JPanel p = new JPanel();
		GridLayout thisLayout = new GridLayout(actions.length, 1);
		thisLayout.setVgap(15);
		p.setLayout(thisLayout);
		for (int i = 0; i < actions.length; i++) {
			Action element = actions[i];
			JButton button = new JButton(element);
			button.setHorizontalAlignment(JButton.CENTER);
			p.add(button);
		}
		return p;
	}

	public final void showErrorDialog(Exception e) {
		app.showErrorDialog(e);
	}

	public final boolean showConfirmationDialog(String titre, String message, String defaultRep) {
		return app.showConfirmationDialog(titre, message, defaultRep);
	}

	public final void showInfoDialog(String text) {
		app.showInfoDialog(text);
	}

	public final void showinfoDialogFonctionNonImplementee() {
		app.showInfoDialogFonctionNonImplementee();
	}

}
