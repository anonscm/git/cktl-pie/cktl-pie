/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client.ui;

import java.awt.GridLayout;
import java.awt.event.ItemListener;
import java.util.Enumeration;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import app.client.tools.Constants;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

/**
 * Classe controlant une matrice de boutons radio, a partir d'un displayGroup.
 * 
 * @author rodolphe.prin@univ-lr.fr
 * 
 */
public class ZEOMatrix extends JPanel {
	private ButtonGroup	matrix;

	/**
	 * Cree et initialise une matrice de boutons radio.
	 * 
	 * @param objects
	 *            les objets qui doivent etre associe a chaque bouton radio.
	 * @param displayField
	 *            nom du champ qui doit etre utilise pour afficher le libelle de chaque bouton radio.
	 * @param libelleForNull
	 *            Libelle de l'item associe a aucun objet (par exemple pour avoir un bouton radio "Tous". Mettre null si on ne veut pas de radio
	 *            bouton associe a aucun objet.
	 * @param itemListener
	 *            listener a positionner sur chaque bouton.
	 * @param alignment
	 *            SwingConstants.VERTICAL or SwingConstants.HORIZONTAL
	 */
	public ZEOMatrix(NSArray objects, String displayField, String libelleForNull, ItemListener itemListener, int alignment) {
		initGUI(alignment);
		initMatrix(objects, displayField, libelleForNull, itemListener);
	}

	private void initMatrix(NSArray objects, String displayField, String libelleForNull, ItemListener itemListener) {
		matrix = new ButtonGroup();
		// verifier si on veut avoir un bouton radio associe a aucun objet (null)
		if (libelleForNull != null) {
			addRadioButton(null, libelleForNull, false, itemListener);
		}

		if (displayField != null) {
			objects = EOSortOrdering.sortedArrayUsingKeyOrderArray(objects, new NSArray(EOSortOrdering.sortOrderingWithKey(displayField,
					EOSortOrdering.CompareCaseInsensitiveAscending)));
		}

		for (int i = 0; i < objects.count(); i++) {
			EOEnterpriseObject leRecord = (EOEnterpriseObject) objects.objectAtIndex(i);
			addRadioButton(leRecord, leRecord.valueForKey(displayField).toString(), false, itemListener);
		}
	}

	private void initGUI(int alignment) {
		if (alignment == SwingConstants.HORIZONTAL) {
			setLayout(new GridLayout(1, 0));
		}
		else {
			setLayout(new GridLayout(0, 1));
		}
		setBorder(BorderFactory.createEmptyBorder());
	}

	/**
	 * Cree et ajoute un bouton radio a la matrice.
	 */
	public void addRadioButton(EOEnterpriseObject peo, String libelle, boolean selected, ItemListener itemListener) {
		ZJRadioButton radioTmp = new ZJRadioButton(peo, libelle, selected);
		if (itemListener != null) {
			radioTmp.addItemListener(itemListener);
		}
		matrix.add(radioTmp);
		add(radioTmp);
	}

	public void showRequired(boolean show) {
		if (show) {
			this.setBackground(Constants.COLOR_BGD_REQUIRED);
		}
		else {
			this.setBackground(null);
		}
	}

	public void clean() {
		selectRadioButtonByIndex(0);
	}
	
	/**
	 * Renvoie un radio bouton par son EOEnterpriseobject associe.
	 */
	public ZJRadioButton getRadioButtonByEo(EOEnterpriseObject eo) {
		Enumeration components = matrix.getElements();
		ZJRadioButton btSelectionne = null;

		while (components.hasMoreElements()) {
			btSelectionne = (ZJRadioButton) components.nextElement();
			if (btSelectionne.eo() == eo) {
				break;
			}
		}
		return btSelectionne;
	}

	/**
	 * Renvoie un radiobouton par son index.
	 */
	public ZJRadioButton getRadioButtonByIndex(int index) {
		Enumeration components = matrix.getElements();
		for (int i=0; i < index && components.hasMoreElements(); i++) {
			components.nextElement();
		}
		if (components.hasMoreElements()) {
			return (ZJRadioButton) components.nextElement();
		}
		return null;
	}

	/**
	 * Permet de Selectionner un radioBouton par son EOEnterpriseObject associe
	 */
	public void selectRadioButtonByEo(EOEnterpriseObject eo) {
		ZJRadioButton btSelectionne = null;
		btSelectionne = getRadioButtonByEo(eo);
		if (btSelectionne != null) {
			btSelectionne.setSelected(true);
		}
	}

	/**
	 * Permet de Selectionner un radiob bouton par son index.
	 */
	public void selectRadioButtonByIndex(int index) {
		ZJRadioButton b = getRadioButtonByIndex(index);
		if (b != null) {
			b.setSelected(true);
		}
	}

	/**
	 * Renvoie radio bouton actuellement selectionne
	 */
	public ZJRadioButton getSelectedRadioButton() {
		Enumeration components = matrix.getElements();
		ZJRadioButton btSelectionne = null;

		while (components.hasMoreElements()) {
			btSelectionne = (ZJRadioButton) components.nextElement();
			if (btSelectionne.isSelected()) {
				break;
			}
		}
		return btSelectionne;
	}

	/**
	 * Renvoie l'objet EOEnterpriseObject associe au radio bouton actuellement selectionne.
	 */
	public EOEnterpriseObject getSelectedEOObject() {
		ZJRadioButton btSelectionne = getSelectedRadioButton();
		if (btSelectionne == null) {
			return null;
		}
		else {
			return btSelectionne.eo();
		}
	}

	public void setEnabled(boolean b) {
		Enumeration components = matrix.getElements();
		while (components.hasMoreElements()) {
			((ZJRadioButton) components.nextElement()).setEnabled(b);
		}

	}

	public boolean isEnabled() {
		Enumeration components = matrix.getElements();
		while (components.hasMoreElements()) {
			if (((ZJRadioButton) components.nextElement()).isEnabled()) {
				return true;
			}
		}
		return false;
	}
}