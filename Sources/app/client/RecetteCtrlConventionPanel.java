/*
CRI - Universite de La Rochelle, 2006

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.awt.Dimension;
import java.math.BigDecimal;

import javax.swing.JTextField;

import org.cocktail.kava.client.factory.FactoryRecetteCtrlConvention;
import org.cocktail.kava.client.metier.EOConvention;
import org.cocktail.kava.client.metier.EORecette;
import org.cocktail.kava.client.metier.EORecetteCtrlConvention;

import app.client.select.ConventionSelect;
import app.client.ui.table.ZEOTableModelColumn;
import app.client.ui.table.ZExtendedTablePanel;

public class RecetteCtrlConventionPanel extends CtrlPanel {

	private static final boolean	DEFAULT_MULTIPLE_INIT			= true;	// demarre par defaut en multiple ou single ?
	private static final boolean	DEFAULT_MULTIPLE_INIT_LOCKED	= false;	// interdit ou non le changement

	private ZEOTableModelColumn		colHT, colTTC;

	private ConventionSelect		conventionSelect;

	private EORecette				currentObject;

	public RecetteCtrlConventionPanel() {
		super("Convention", 25);
		conventionSelect = new ConventionSelect();
	}

	public void setEditable(boolean editable) {
		colHT.setEditable(editable);
		colTTC.setEditable(editable);
		super.setEditable(editable);
	}

	public void setColumnsEditable(boolean editable) {
		colHT.setEditable(editable);
		colTTC.setEditable(editable);
	}

	public void setCurrentObject(EORecette object) {
		this.currentObject = object;
		updateData();
	}

	public void add(EOConvention eo) {
		if (currentObject == null || eo == null) {
			return;
		}
		// si on est en mode multiple ou qu'il n'y a pas encore de ctrl, on ajoute
		if (multipleEnable || currentObject.recetteCtrlConventions() == null || currentObject.recetteCtrlConventions().count() == 0) {
			EORecetteCtrlConvention object = FactoryRecetteCtrlConvention.newObject(ec);
			object.setConventionRelationship(eo);
			object.setExerciceRelationship(currentObject.exercice());
			setMontants(object);
			currentObject.addToRecetteCtrlConventionsRelationship(object);
		}
		else {
			// on est en mode single et y'a deja un ctrl, on le modifie
			((EORecetteCtrlConvention) currentObject.recetteCtrlConventions().objectAtIndex(0)).setConventionRelationship(eo);
			setMontants((EORecetteCtrlConvention) currentObject.recetteCtrlConventions().objectAtIndex(0));
		}
		updateData();
	}

	protected void onAdd() {
		if (conventionSelect.openRec(currentObject.utilisateur(), currentObject.exercice(), currentObject.facture().organ(), currentObject
				.facture().typeCreditRec(), null,null)) {
			try {
				add(conventionSelect.getSelected());
			}
			catch (Exception e) {
				System.err.println("Erreur lors de la tentative d'insertion d'un nouvel objet RecetteCtrlConvention: " + e);
			}
		}
	}

	protected void onSelect() {
		EOConvention current = null;
		if (currentObject.recetteCtrlConventions() != null && currentObject.recetteCtrlConventions().count() > 0) {
			current = ((EORecetteCtrlConvention) currentObject.recetteCtrlConventions().objectAtIndex(0)).convention();
		}
		if (conventionSelect.openRec(currentObject.utilisateur(), currentObject.exercice(), currentObject.facture().organ(), currentObject
				.facture().typeCreditRec(), current,null)) {
			try {
				add(conventionSelect.getSelected());
			}
			catch (Exception e) {
				System.err.println("Erreur lors de la tentative d'insertion/modification d'un nouvel objet RecetteCtrlConvention: " + e);
			}
		}
	}

	protected void onUpdate() {
		if (table.selectedObject() != null) {
			if (conventionSelect.openRec(currentObject.utilisateur(), currentObject.exercice(), currentObject.facture().organ(), currentObject
					.facture().typeCreditRec(), ((EORecetteCtrlConvention) table.selectedObject()).convention(),null)) {
				((EORecetteCtrlConvention) table.selectedObject()).setConventionRelationship(conventionSelect.getSelected());
				table.updateUI();
			}
		}
	}

	protected void onDelete() {
		if (table.selectedObject() != null && app.showConfirmationDialog("ATTENTION", "Supprimer cette ligne??", "OUI", "NON")) {
			currentObject.removeFromRecetteCtrlConventionsRelationship((EORecetteCtrlConvention) table.selectedObject());
			ec.deleteObject(table.selectedObject());
			updateData();
		}
	}

	private void setMontants(EORecetteCtrlConvention ctrl) {
		// on fixe le ht par rapport soit au restant du montant ht de la recette, soit au montant ht de la recette
		BigDecimal newHt = currentObject.recHtSaisie();
		if (multipleEnable && newHt != null) {
			newHt = newHt.subtract((BigDecimal) currentObject.recetteCtrlConventions().valueForKey(
					"@sum." + EORecetteCtrlConvention.RCON_HT_SAISIE_KEY));
		}
		if (newHt == null || newHt.doubleValue() < 0.0) {
			newHt = new BigDecimal(0.0);
		}
		// on fixe le ttc par rapport soit au restant du montant ttc de la recette, soit au montant ttc de la recette
		BigDecimal newTtc = currentObject.recTtcSaisie();
		if (multipleEnable && newTtc != null) {
			newTtc = newTtc.subtract((BigDecimal) currentObject.recetteCtrlConventions().valueForKey(
					"@sum." + EORecetteCtrlConvention.RCON_TTC_SAISIE_KEY));
		}
		if (newTtc == null || newTtc.doubleValue() < 0.0) {
			newTtc = new BigDecimal(0.0);
		}
		ctrl.setRconHtSaisie(newHt);
		ctrl.setRconTtcSaisie(newTtc);
		table.updateData();
	}

	public void updateMontants() {
		if (currentObject.recetteCtrlConventions() == null || currentObject.recetteCtrlConventions().count() != 1) {
			return;
		}
		((EORecetteCtrlConvention) currentObject.recetteCtrlConventions().objectAtIndex(0)).setRconHtSaisie(currentObject.recHtSaisie());
		((EORecetteCtrlConvention) currentObject.recetteCtrlConventions().objectAtIndex(0)).setRconTtcSaisie(currentObject.recTtcSaisie());
		table.updateData();
	}

	protected void updateData() {
		if (currentObject == null) {
			table.setObjectArray(null);
			tfCtrl.clean();
		}
		else {
			if (multipleEnable) {
				table.setObjectArray(currentObject.recetteCtrlConventions());
			}
			else {
				if (currentObject.recetteCtrlConventions() == null || currentObject.recetteCtrlConventions().count() == 0) {
					tfCtrl.clean();
				}
				else {
					// on vire ceux qui sont en trop ! et on en garde un... le dernier...
					while (currentObject.recetteCtrlConventions().count() > 1) {
						EORecetteCtrlConvention ctrl = (EORecetteCtrlConvention) currentObject.recetteCtrlConventions().objectAtIndex(0);
						currentObject.removeFromRecetteCtrlConventionsRelationship(ctrl);
						ec.deleteObject(ctrl);
					}
					EORecetteCtrlConvention ctrl = (EORecetteCtrlConvention) currentObject.recetteCtrlConventions().objectAtIndex(0);
					setMontants(ctrl);
					tfCtrl.setText(ctrl.convention().conReferenceExterne());
				}
			}
		}
	}

	private final class TablePanel extends ZExtendedTablePanel {

		public TablePanel() {
			super("Conventions");

			final ZEOTableModelColumn.ZEONumFieldTableCellEditor _editor = new ZEOTableModelColumn.ZEONumFieldTableCellEditor(new JTextField(),
					app.getCurrencyFormatEdit());

			addCol(new ZEOTableModelColumn(getDG(), EORecetteCtrlConvention.CONVENTION_KEY + "." + EOConvention.CON_REFERENCE_EXTERNE_KEY,
					"Ref", 60));
			// cols.add(new ZEOTableModelColumn(getDG(), EORecetteCtrlConvention.CONVENTION_KEY + "." + EOConvention.CAN_LIBELLE_KEY,
			// "Lib", 100));
			colHT = new ZEOTableModelColumn(getDG(), EORecetteCtrlConvention.RCON_HT_SAISIE_KEY, "HT", 60, app.getCurrencyFormatDisplay());
			colHT.setEditable(true);
			colHT.setTableCellEditor(_editor);
			colHT.setFormatEdit(app.getCurrencyFormatEdit());
			colHT.setColumnClass(BigDecimal.class);
			addCol(colHT);
			colTTC = new ZEOTableModelColumn(getDG(), EORecetteCtrlConvention.RCON_TTC_SAISIE_KEY, "TTC", 60, app.getCurrencyFormatDisplay());
			colTTC.setEditable(true);
			colTTC.setTableCellEditor(_editor);
			colTTC.setFormatEdit(app.getCurrencyFormatEdit());
			colTTC.setColumnClass(BigDecimal.class);
			addCol(colTTC);

			initGUI();

			setPreferredSize(new Dimension(0, 70));
		}

		protected void onAdd() {
			RecetteCtrlConventionPanel.this.onAdd();
		}

		protected void onUpdate() {
			RecetteCtrlConventionPanel.this.onUpdate();
		}

		protected void onDelete() {
			RecetteCtrlConventionPanel.this.onDelete();
		}

		protected void onSelectionChanged() {
		}

	}

	protected boolean defaultMultipleInit() {
		return DEFAULT_MULTIPLE_INIT;
	}

	protected boolean defaultMultipleInitLocked() {
		return DEFAULT_MULTIPLE_INIT_LOCKED;
	}

	protected ZExtendedTablePanel getTablePanel() {
		return new TablePanel();
	}

}
