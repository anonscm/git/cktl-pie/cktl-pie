/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.text.NumberFormat;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;

import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeEtat;
import org.cocktail.kava.client.factory.FactoryFacturePapier;
import org.cocktail.kava.client.factory.FactoryPrestation;
import org.cocktail.kava.client.finder.FinderFournisUlr;
import org.cocktail.kava.client.finder.FinderPrestation;
import org.cocktail.kava.client.finder.FinderTypeApplication;
import org.cocktail.kava.client.finder.FinderTypeEtat;
import org.cocktail.kava.client.metier.EOCatalogue;
import org.cocktail.kava.client.metier.EOEngageBudget;
import org.cocktail.kava.client.metier.EOFacturePapier;
import org.cocktail.kava.client.metier.EOFonction;
import org.cocktail.kava.client.metier.EOFournisUlr;
import org.cocktail.kava.client.metier.EOGrhumParametres;
import org.cocktail.kava.client.metier.EOPrestation;
import org.cocktail.kava.client.metier.EOPrestationBudgetClient;
import org.cocktail.kava.client.metier.EOTypePublic;
import org.cocktail.kava.client.metier.EOUtilisateur;
import org.cocktail.pie.client.interfaces.FinderPrestationRechercheAvance;
import org.cocktail.pieFwk.common.exception.EntityInitializationException;

import app.client.configuration.ConfigurationCouleur;
import app.client.reports.ReportFactoryClient;
import app.client.tools.Constants;
import app.client.ui.UIUtilities;
import app.client.ui.ZEOComboBox;
import app.client.ui.ZNumberField;
import app.client.ui.ZTextField;
import app.client.ui.table.ZEOTableCellRenderer;
import app.client.ui.table.ZEOTableModelColumn;
import app.client.ui.table.ZExtendedTablePanel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class Prestation extends JPanel implements IPieTab {

	private static final String WINDOW_TITLE = "Prestations";

	protected ApplicationClient app;
	protected EOEditingContext ec;

	private static Prestation sharedInstance;

	private ConfigurationCouleur configurationCouleur;

	private ZNumberField tfPrestNumero, tfEngNumero;
	private ZTextField tfPrestLibelle, tfExercice;
	private ZEOComboBox cbFournisUlr;
	private JComboBox listMaxResult;

	private PrestationTablePanel prestationTablePanel;
	private FacturePapierTablePanel facturePapierTablePanel;

	private Action actionValideClient, actionValidePrest, actionClorePrestation;
	private Action actionGenereFacture;
	private Action actionPrint, actionMail, actionLegend;
	private Action actionClose;
	private Action actionRechercheAvance;
	private Action actionSrch;
	private FinderPrestationRechercheAvance monFinderPrestationRechercheAvance;

	public Boolean atLeastOnSearchHasBeenMade = Boolean.FALSE;
	private boolean displayArchives = false;
	private JSplitPane tablesPanel;

	public Prestation(String windowTitle) {
		super();

		app = (ApplicationClient) EOApplication.sharedApplication();
		ec = app.editingContext();

		this.configurationCouleur = ConfigurationCouleur.instance();


		actionValideClient = new ActionValideClient();
		actionValidePrest = new ActionValidePrestataire();
		actionClorePrestation = new ActionClorePrestation();
		actionGenereFacture = new ActionGenereFacture();
		actionPrint = new ActionPrint();
		actionMail = new ActionMail();
		actionLegend = new ActionLegend();
		actionClose = new ActionClose();
		actionRechercheAvance = new ActionRechercheAvance();
		actionSrch = new ActionSrch();

		NumberFormat integerFormat = NumberFormat.getIntegerInstance();
		integerFormat.setGroupingUsed(false);
		tfPrestNumero = new ZNumberField(5, integerFormat);
		tfEngNumero = new ZNumberField(6, integerFormat);
		tfPrestLibelle = new ZTextField(15);
		tfExercice = new ZTextField(4);

		//cbActionListener = new CbActionListener();
		cbFournisUlr = new ZEOComboBox(FinderFournisUlr.find(ec, app.getParam(EOGrhumParametres.PARAM_ANNUAIRE_FOU_VALIDE_INTERNE)),
				EOFournisUlr.PERSONNE_PERS_NOM_PRENOM_KEY, "Tous", null, null, 150);

		MyActionListener defaultActionListener = new MyActionListener(actionSrch);
		tfPrestNumero.addActionListener(defaultActionListener);
		tfEngNumero.addActionListener(defaultActionListener);
		tfPrestLibelle.addActionListener(defaultActionListener);

		prestationTablePanel = new PrestationTablePanel();
		prestationTablePanel.setEditable(true);
		facturePapierTablePanel = new FacturePapierTablePanel();
		facturePapierTablePanel.setEditable(false);
		
		listMaxResult = new JComboBox(new String[] {
				"25", "50", "100", "Tous"
		});



		initGUI();
	}

	private class MyActionListener implements ActionListener {
		private final Action action;

		/**
	         *
	         */
		public MyActionListener(Action a) {
			super();
			action = a;
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(final ActionEvent e) {
			action.actionPerformed(e);
		}

	}
	public static Prestation sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new Prestation(WINDOW_TITLE);
		}
		return sharedInstance;
	}

	/**
	 * Permet eventuellement de configurer au lancement l'interface selon des prefs utilisateur
	 *
	 * @param utilisateur
	 */
	public void init(org.cocktail.application.client.eof.EOUtilisateur utilisateur) {
		tablesPanel.setDividerLocation(0.8);
	}


	public void update() {
		EOExercice exercice = app.superviseur().currentExercice();
		tfExercice.setText(exercice.exeExercice().toString());

		// TODO autoriser tout ca plus tard meme sur un exercice clos (pour consultation / traitement des annees precedentes)
		prestationTablePanel.setEditable(false);
		if (app.canUseFonction(EOFonction.DROIT_GERER_PRESTATION, exercice)) {
			if (exercice.exeStatFac().equalsIgnoreCase(EOExercice.EXERCICE_OUVERT)) {
				prestationTablePanel.setEditable(true);
			}
			else {
				if (exercice.exeStatFac().equalsIgnoreCase(EOExercice.EXERCICE_RESTREINT)) {
					if (app.canUseFonction(EOFonction.DROIT_FACTURER_PERIODE_BLOCAGE, exercice)) {
						prestationTablePanel.setEditable(true);
					}
				}
			}
		}

		facturePapierTablePanel.setEditable(false);
		if (exercice.exeStatFac().equalsIgnoreCase(EOExercice.EXERCICE_OUVERT)) {
			if (app.canUseFonction(EOFonction.DROIT_FACTURER, exercice)) {
				facturePapierTablePanel.setEditable(true);
			}
		}
		else {
			if (exercice.exeStatFac().equalsIgnoreCase(EOExercice.EXERCICE_RESTREINT)) {
				if (app.canUseFonction(EOFonction.DROIT_FACTURER_PERIODE_BLOCAGE, exercice)) {
					facturePapierTablePanel.setEditable(true);
				}
			}
		}
		updateData();
	}

	public void updateData() {
		if (atLeastOnSearchHasBeenMade) {
			long start = System.currentTimeMillis();
			app.superviseur().setWaitCursor(this, true);
			Integer fetchLimit = null;
			try {
				fetchLimit = Integer.valueOf((String) listMaxResult.getSelectedItem());
			} catch (NumberFormatException e) {
				//on ne fait rien, on laisse fetchlimit à null
			}
			prestationTablePanel.reloadData(updateFilteringQualifier(), fetchLimit);
			updateInterfaceEnabling();
			app.superviseur().setWaitCursor(this, false);
			System.out.println("Chargement prestations : " + (System.currentTimeMillis() - start) + "ms");
		}
	}

	public void updateInterfaceEnabling() {
		EOPrestation selected = (EOPrestation) prestationTablePanel.selectedObject();
		if (selected != null) {
			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(selected)));
		}
		EOUtilisateur loggedUser = app.appUserInfo().utilisateur();
		boolean canManage = prestationTablePanel.isEditable();
		boolean hasPermissionPrestationsRepro = app.canUseFonction(EOFonction.DROIT_GERER_PRESTATIONS_REPRO, null);
		boolean hasPermissionOrganPrestataire = true;
		if (selected != null && selected.organ() != null) {
			hasPermissionOrganPrestataire = selected.organ().hasPermission(loggedUser);
			canManage &= hasPermissionOrganPrestataire;
		}
		boolean hasPermissionOrganClient = hasPermissionOrganPrestataire;
		if (selected != null
				&& isPrestationInterne(selected)
				&& selected.prestationBudgetClient() != null
				&& selected.prestationBudgetClient().organ() != null) {
			hasPermissionOrganClient |= selected.prestationBudgetClient().organ().hasPermission(loggedUser);
			hasPermissionOrganClient |= hasPermissionPrestationsRepro;
		}

		actionValideClient.setEnabled(
				selected != null
				&& (canManage || hasPermissionOrganClient)
				&& selected.typeEtat().equals(FinderTypeEtat.typeEtatValide(ec))
				&& selected.prestDateFacturation() == null);

		if (selected != null && selected.prestDateValideClient() != null) {
			// si c'est de la prestation interne avec un engagement mais pas de ligne budgetaire cliente, c'est que
			// c'est une prestation interne issue d'une commande carambole, donc interdit de la devalider cote client !
			if (selected.typePublic().typeApplication().equals(
						FinderTypeApplication.typeApplicationPrestationInterne(ec))
					&& selected.prestationBudgetClient() != null
					&& selected.prestationBudgetClient().engageBudget() != null
					&& selected.prestationBudgetClient().organ() == null) {
				actionValideClient.setEnabled(false);
				actionValideClient.putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
				actionValideClient.putValue(AbstractAction.SHORT_DESCRIPTION,
						"Prestation interne issue d'une commande Carambole, interdit de la d\u00E9valider !");
			} else {
				actionValideClient.putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
				actionValideClient.putValue(AbstractAction.SHORT_DESCRIPTION, "D\u00E9valider la prestation c\u00F4t\u00E9 client");
			}
		} else {
			if (selected != null && selected.isValidableClient()) {
				actionValideClient.putValue(AbstractAction.SMALL_ICON, Constants.ICON_NOTVALID_16);
			} else {
				actionValideClient.putValue(AbstractAction.SMALL_ICON, Constants.ICON_NOTVALID_WARNING_16);
			}
			actionValideClient.putValue(AbstractAction.SHORT_DESCRIPTION, "Valider la prestation c\u00F4t\u00E9 client");
		}

		actionValidePrest.setEnabled(canManage
				&& selected != null
				&& selected.typeEtat().equals(FinderTypeEtat.typeEtatValide(ec))
				&& selected.prestDateFacturation() == null);
		if (selected != null && selected.prestDateValidePrest() != null) {
			actionValidePrest.putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
			actionValidePrest.putValue(AbstractAction.SHORT_DESCRIPTION, "D\u00E9valider la prestation c\u00F4t\u00E9 prestataire");
		} else {
			if (selected != null && selected.isValidablePrest()) {
				actionValidePrest.putValue(AbstractAction.SMALL_ICON, Constants.ICON_NOTVALID_16);
			} else {
				actionValidePrest.putValue(AbstractAction.SMALL_ICON, Constants.ICON_NOTVALID_WARNING_16);
			}
			actionValidePrest.putValue(AbstractAction.SHORT_DESCRIPTION, "Valider la prestation c\u00F4t\u00E9 prestataire");
		}

		actionClorePrestation.setEnabled(canManage
				&& selected != null
				&& selected.typeEtat().equals(FinderTypeEtat.typeEtatValide(ec))
				&& selected.prestDateFacturation() == null);
		if (selected != null && selected.prestDateCloture() != null) {
			actionClorePrestation.putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
			actionClorePrestation.putValue(AbstractAction.SHORT_DESCRIPTION, "R\u00E9ouvrir la prestation");
		} else {
			if (selected != null && selected.isCloturable()) {
				actionClorePrestation.putValue(AbstractAction.SMALL_ICON, Constants.ICON_NOTVALID_16);
			} else {
				actionClorePrestation.putValue(AbstractAction.SMALL_ICON, Constants.ICON_NOTVALID_WARNING_16);
			}
			actionClorePrestation.putValue(AbstractAction.SHORT_DESCRIPTION, "Clore la prestation");
		}

		actionGenereFacture.setEnabled(canManage
				&& selected != null
				&& selected.typeEtat().equals(FinderTypeEtat.typeEtatValide(ec))
				&& selected.prestDateCloture() != null);
		if (selected != null && selected.prestDateFacturation() != null) {
			actionGenereFacture.putValue(AbstractAction.SHORT_DESCRIPTION, "Facturer les restes (s'il y en a)");
		} else {
			actionGenereFacture.putValue(AbstractAction.SHORT_DESCRIPTION, "G\u00E9n\u00E9rer la (les) facture(s)");
		}

		actionPrint.setEnabled(selected != null
				&& selected.typeEtat().equals(FinderTypeEtat.typeEtatValide(ec)));
		actionMail.setEnabled(hasPermissionOrganPrestataire
				&& selected != null
				&& selected.typeEtat().equals(FinderTypeEtat.typeEtatValide(ec)));

		// disable action buttons if current user doesn't have required permission on the 'organ' object.
		if (!hasPermissionOrganPrestataire) {
			prestationTablePanel.actionDelete.setEnabled(false);
		}

	}

	public boolean canQuit() {
		return PrestationAddUpdate.sharedInstance().canQuit();
	}

	public EOPrestation getSelected() {
		return (EOPrestation) prestationTablePanel.selectedObject();
	}

	private boolean isPrestationInterne(EOPrestation prestation) {
		return prestation != null
				&& prestation.typePublic() != null
				&& prestation.typePublic().typeApplication().equals(
						FinderTypeApplication.typeApplicationPrestationInterne(ec));
	}


	private NSArray updateFilteringQualifier(){
		NSMutableArray andQuals = new NSMutableArray();
		if (tfPrestNumero.getNumber() != null) {
			andQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PREST_NUMERO_KEY + " = %@", new NSArray(tfPrestNumero.getNumber())));
		}
		if (tfEngNumero.getNumber() != null) {
			andQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PRESTATION_BUDGET_CLIENT_KEY+"."+
					EOPrestationBudgetClient.ENGAGE_BUDGET_KEY+"."+EOEngageBudget.ENG_NUMERO_KEY+"=%@", new NSArray(tfEngNumero.getNumber())));
		}
		if (tfPrestLibelle.getText() != null) {
			NSMutableArray quals = new NSMutableArray();
			// libell\u00E9
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PREST_LIBELLE_KEY + " caseInsensitiveLike %@", new NSArray("*"
					+ tfPrestLibelle.getText() + "*")));
			// client
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PERSONNE_PERS_NOM_PRENOM_KEY + " caseInsensitiveLike %@",
					new NSArray("*" + tfPrestLibelle.getText() + "*")));
			// catalogue
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.CATALOGUE_KEY + "." + EOCatalogue.CAT_LIBELLE_KEY
					+ " caseInsensitiveLike %@", new NSArray("*" + tfPrestLibelle.getText() + "*")));

			andQuals.addObject(new EOOrQualifier(quals));
		}
		if (cbFournisUlr.getSelectedEOObject() != null) {
			andQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.CATALOGUE_KEY + "." + EOCatalogue.FOURNIS_ULR_KEY + " = %@",
					new NSArray((EOFournisUlr) cbFournisUlr.getSelectedEOObject())));
		}

		NSArray advSearchQualifiers = getMonFinderPrestationRechercheAvance().getResultat();
		if (advSearchQualifiers != null && advSearchQualifiers.count() > 0) {
			andQuals.addObjectsFromArray(advSearchQualifiers);
		}
		return andQuals;
	}

	private void onClose() {
		this.setVisible(false);
	}

	public FinderPrestationRechercheAvance getMonFinderPrestationRechercheAvance() {
		if(monFinderPrestationRechercheAvance==null)
		{
			monFinderPrestationRechercheAvance = new FinderPrestationRechercheAvance(app,0,0,300,200);
			monFinderPrestationRechercheAvance.setParent(this);
		}
		return monFinderPrestationRechercheAvance;
	}


	private void onPrint() {
		if (prestationTablePanel.selectedObject() == null) {
			return;
		}
		EOPrestation prestation = (EOPrestation) prestationTablePanel.selectedObject();
		ReportFactoryClient.printDevis(prestation, null);
	}

	private void onMail() {
		if (prestationTablePanel.selectedObject() == null) {
			return;
		}
		EOPrestation prestation = (EOPrestation) prestationTablePanel.selectedObject();
		Mail.sharedInstance().open(prestation);
	}

	private void onLegend() {
		JFrame frame = new JFrame("L\u00E9gende des couleurs");
		Box contentPanel = Box.createVerticalBox();

		JPanel panel1 = new JPanel(new FlowLayout(FlowLayout.LEFT, 20, 0));
		panel1.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		JTextField label1 = new JTextField("Texte", 6);
		label1.setBorder(null);
		label1.setEditable(false);
		label1.setBackground(null);
		label1.setForeground(configurationCouleur.prestationNonValideClientForeground(app));
		panel1.add(label1);
		panel1.add(new JLabel("Prestation non valid\u00E9e."));
		contentPanel.add(panel1);

		JPanel panel2 = new JPanel(new FlowLayout(FlowLayout.LEFT, 20, 0));
		panel2.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		JTextField label2 = new JTextField("Texte", 6);
		label2.setBorder(null);
		label2.setEditable(false);
		label2.setBackground(null);
		label2.setForeground(configurationCouleur.prestationValideClientForeground(app));
		panel2.add(label2);
		panel2.add(new JLabel("Prestation valid\u00E9e par le client."));
		contentPanel.add(panel2);

		JPanel panel2b = new JPanel(new FlowLayout(FlowLayout.LEFT, 20, 0));
		panel2b.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		JTextField label2b = new JTextField("Texte", 6);
		label2b.setBorder(null);
		label2b.setEditable(false);
		label2b.setBackground(null);
		label2b.setForeground(configurationCouleur.prestationValideForeground(app));
		panel2b.add(label2b);
		panel2b.add(new JLabel("Prestation valid\u00E9e entierement."));
		contentPanel.add(panel2b);

		JPanel panel3 = new JPanel(new FlowLayout(FlowLayout.LEFT, 20, 0));
		panel3.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		JTextField label3 = new JTextField("", 6);
		label3.setBorder(null);
		label3.setEditable(false);
		label3.setBackground(configurationCouleur.prestationFactureeBackground(app));
		panel3.add(label3);
		panel3.add(new JLabel("Prestation factur\u00E9e."));
		contentPanel.add(panel3);

		JPanel panel4 = new JPanel(new FlowLayout(FlowLayout.LEFT, 20, 0));
		panel4.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		JTextField label4 = new JTextField("Texte", 6);
		label4.setBorder(null);
		label4.setEditable(false);
		label4.setBackground(null);
		label4.setForeground(configurationCouleur.prestationArchiveeForeground(app));
		panel4.add(label4);
		panel4.add(new JLabel("Prestation archiv\u00E9e."));
		contentPanel.add(panel4);

		frame.setContentPane(contentPanel);
		frame.setLocationRelativeTo(app.superviseur());
		frame.setResizable(false);
		frame.pack();
		frame.show();
	}

	private class PrestationTablePanel extends ZExtendedTablePanel {



		public PrestationTablePanel() {
			super(null);

			ZEOTableModelColumn col1 = new ZEOTableModelColumn(getDG(), EOPrestation.PREST_NUMERO_KEY, "No", 60);
			col1.setColumnClass(Integer.class);
			col1.setAlignment(SwingConstants.LEFT);
			addCol(col1);
			addCol(new ZEOTableModelColumn(getDG(), EOPrestation.PREST_LIBELLE_KEY, "Lib", 180));

			addCol(new ZEOTableModelColumn(getDG(), EOPrestation.PERSONNE_PERS_NOM_PRENOM_KEY, "Client", 120));
			ZEOTableModelColumn col4 = new ZEOTableModelColumn(getDG(), EOPrestation.PREST_DATE_KEY, "Date", 60, Constants.FORMAT_DATESHORT);
			col4.setColumnClass(NSTimestamp.class);
			addCol(col4);
			ZEOTableModelColumn col6 = new ZEOTableModelColumn(getDG(), EOPrestation.PREST_DATE_FACTURATION_KEY, "Facturation", 60,
					Constants.FORMAT_DATESHORT);
			col6.setColumnClass(NSTimestamp.class);
			addCol(col6);
			addCol(new ZEOTableModelColumn(getDG(), EOPrestation.TYPE_PUBLIC_KEY + "." + EOTypePublic.TYPU_LIBELLE_KEY, "Type", 100));
			addCol(new ZEOTableModelColumn(getDG(), EOPrestation.CATALOGUE_KEY + "." + EOCatalogue.CAT_LIBELLE_KEY, "Catalogue", 150));

			initGUI();

			getTablePanel().getTable().setMyRenderer(new DataTableCellRenderer());

			buildPopup();
		}



		public void reloadData(NSArray qualifiers, Integer fetchLimit) {
			EOUtilisateur utilisateur = app.appUserInfo().utilisateur();
			if (app.canUseFonction(EOFonction.DROIT_VOIR_TOUTES_LES_PRESTATIONS, null)) {
				utilisateur = null;
			}
			EOTypeEtat typeEtat = null;
			if (!displayArchives) {
				typeEtat = FinderTypeEtat.typeEtatValide(ec);
			}
			NSMutableArray quals = new NSMutableArray();
	        if (utilisateur != null) {
	            NSMutableArray orQuals = new NSMutableArray();
	            orQuals.addObject(EOQualifier.qualifierWithQualifierFormat("utilisateur = %@", new NSArray(utilisateur)));
	            orQuals.addObject(EOQualifier.qualifierWithQualifierFormat("catalogue.catalogueResponsables.utilisateur = %@", new NSArray(utilisateur)));
	            orQuals.addObject(EOQualifier.qualifierWithQualifierFormat("individuUlr = %@", new NSArray(utilisateur.personne().individuUlr())));
	            orQuals.addObject(EOQualifier.qualifierWithQualifierFormat("personne.structureUlr.secretariats.individuUlr = %@", new NSArray(utilisateur.personne().individuUlr())));
	            orQuals.addObject(EOQualifier.qualifierWithQualifierFormat("personne.structureUlr.repartStructures.personne = %@", new NSArray(utilisateur.personne())));

	            quals.addObject(new EOOrQualifier(orQuals));
	        }
	        if(app.superviseur().currentExercice() != null)
	            quals.addObject(EOQualifier.qualifierWithQualifierFormat("exercice = %@", new NSArray(app.superviseur().currentExercice())));
	        if(typeEtat != null)
	            quals.addObject(EOQualifier.qualifierWithQualifierFormat("typeEtat = %@", new NSArray(typeEtat)));
	        if(qualifiers !=null && quals.count()>0)
	        	quals.addObjectsFromArray(qualifiers);
			setObjectArray(FinderPrestation.find(ec, new EOAndQualifier(quals), fetchLimit));
		}

		protected void onAdd() {
			try {
				app.superviseur().setWaitCursor(this, true);
				if (PrestationAddUpdate.sharedInstance().openNew(app.superviseur().currentExercice())) {
					//					reloadData();
					System.out.println("PrestationAddUpdate.sharedInstance().getLastOpened() = " + PrestationAddUpdate.sharedInstance().getLastOpened());
					ajouterInDg(PrestationAddUpdate.sharedInstance().getLastOpened(), 0);
				}
			} catch (EntityInitializationException eie) {
				app.superviseur().setWaitCursor(this, false);
				app.showErrorDialog(eie);
			}
 finally {
				app.superviseur().setWaitCursor(this, false);
			}
		}

		protected void onUpdate() {
			if (selectedObject() == null) {
				return;
			}
			if (((EOPrestation) selectedObject()).typeEtat().equals(FinderTypeEtat.typeEtatAnnule(ec))) {
				app.showInfoDialog("Cette prestation est archiv\u00E9e, vous ne pouvez pas la modifier !");
				return;
			}
			app.superviseur().setWaitCursor(this, true);
			if (PrestationAddUpdate.sharedInstance().open((EOPrestation) selectedObject())) {
				//updateData();
				fireSelectedTableRowUpdated();
			}
			app.superviseur().setWaitCursor(this, false);
		}

		protected void onDelete() {
			if (selectedObject() != null) {
				EOPrestation prestation = (EOPrestation) selectedObject();
				if (prestation.typeEtat().equals(FinderTypeEtat.typeEtatAnnule(ec))) {
					if (app.showConfirmationDialog("ATTENTION", "Cette prestation est archiv\u00E9e. Souhaitez-vous la r\u00E9activer?", "OUI", "NON")) {
						try {
							FactoryPrestation.reactivePrestation(ec, prestation);
							Prestation.this.updateData();
						}
						catch (Exception e) {
							app.showErrorDialog(e);
						}
					}
				}
				else {
					// verif de l'\u00E9tat de la prestation
					if (prestation.prestDateFacturation() != null) {
						app.showErrorDialog("La prestation " + prestation.prestNumero()
								+ " est d\u00E9j\u00E0 factur\u00E9e, impossible de la supprimer (ou essayer de supprimer les factures d'abord) !");
						return;
					}
					String question = "Archiver cette prestation ??";
					if (prestation.prestDateValideClient() != null) {
						question = question + "\nCeci d\u00E9validera totalement la prestation...";
						if (prestation.typePublic().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(ec))) {
							// si c'est de la prestation interne avec un engagement mais pas de ligne budgetaire cliente, c'est que
							// c'est une prestation interne issue d'une commande carambole, donc on ne supprime pas l'engagement
							if (prestation.prestationBudgetClient() != null && prestation.prestationBudgetClient().engageBudget() != null
									&& prestation.prestationBudgetClient().organ() == null) {
								question = question
										+ " et lib\u00E9rera l'engagement client correspondant (sans le supprimer, puisque cette PI est issue d'une commande Carambole)...";
							}
							else {
								question = question + " et annulera l'engagement client correspondant...";
							}
						}
					}
					if (app.showConfirmationDialog("ATTENTION", question, "OUI", "NON")) {
						try {
							FactoryPrestation.archivePrestation(ec, prestation, app.appUserInfo().utilisateur());
							//reloadData();
							deleteSelectionInDg();
						} catch (Exception e) {
							app.showErrorDialog(e);
						}
					}
				}
			}
		}

		protected void onSelectionChanged() {
			facturePapierTablePanel.reloadData();
			EOPrestation selected = (EOPrestation) prestationTablePanel.selectedObject();
			if (selected != null && selected.prestDateFacturation() != null) {
				FacturePapier.sharedInstance().setSelected(selected.facturePapiers());
			}
			updateInterfaceEnabling();
		}

		private void buildPopup() {
			final JCheckBoxMenuItem menuItem = new JCheckBoxMenuItem("Afficher les prestations archiv\u00E9es");
			menuItem.setSelected(displayArchives);
			menuItem.addItemListener(new MyPopupItemListener());
			JPopupMenu popup = new JPopupMenu();
			popup.add(menuItem);
			getTablePanel().getTable().setPopup(popup);
		}

		private final class MyPopupItemListener implements ItemListener {
			public void itemStateChanged(ItemEvent e) {
				displayArchives = ((JCheckBoxMenuItem) e.getItem()).isSelected();
				updateData();
				//reloadData();
			}
		}

		private class DataTableCellRenderer extends ZEOTableCellRenderer {
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

				EOPrestation prestation = (EOPrestation) getTablePanel().getObjectAtRow(row);
				if (prestation.typeEtat().equals(FinderTypeEtat.typeEtatAnnule(ec))) {
					label.setForeground(configurationCouleur.prestationArchiveeForeground(app));
				} else {
					if (prestation.prestDateValideClient() == null) {
						label.setForeground(configurationCouleur.prestationNonValideClientForeground(app));
					} else {
						if (prestation.prestDateValidePrest() != null && prestation.prestDateCloture() != null) {
							label.setForeground(configurationCouleur.prestationValideForeground(app));
						} else {
							label.setForeground(configurationCouleur.prestationValideClientForeground(app));
						}
					}
				}
				if (isSelected) {
					label.setBackground(table.getSelectionBackground());
				} else {
					if (prestation.prestDateFacturation() == null) {
						label.setBackground(table.getBackground());
					} else {
						label.setBackground(configurationCouleur.prestationFactureeBackground(app));
					}
				}
				return label;
			}
		}

	}

	private class FacturePapierTablePanel extends ZExtendedTablePanel {

		public FacturePapierTablePanel() {
			super("Factures g\u00E9n\u00E9r\u00E9es \u00E0 partir de la prestation s\u00E9lectionn\u00E9e", false, true, true);

			addCol(new ZEOTableModelColumn(getDG(), EOFacturePapier.FAP_NUMERO_KEY, "No", 50));
			addCol(new ZEOTableModelColumn(getDG(), EOFacturePapier.FAP_REF_KEY, "Ref", 60));
			addCol(new ZEOTableModelColumn(getDG(), EOFacturePapier.FAP_LIB_KEY, "Libell\u00E9", 150));
			addCol(new ZEOTableModelColumn(getDG(), EOFacturePapier.PERSONNE_PERS_NOM_PRENOM_KEY, "Client", 120));
			ZEOTableModelColumn col3 = new ZEOTableModelColumn(getDG(), EOFacturePapier.FAP_DATE_KEY, "Date", 50, Constants.FORMAT_DATESHORT);
			col3.setColumnClass(NSTimestamp.class);
			addCol(col3);
			ZEOTableModelColumn col4 = new ZEOTableModelColumn(getDG(), EOFacturePapier.FAP_TOTAL_HT_KEY, "HT", 60, app.getCurrencyFormatDisplay());
			col4.setColumnClass(BigDecimal.class);
			col4.setAlignment(SwingConstants.RIGHT);
			addCol(col4);
			ZEOTableModelColumn col5 = new ZEOTableModelColumn(getDG(), EOFacturePapier.FAP_TOTAL_TVA_KEY, "TVA", 50, app.getCurrencyFormatDisplay());
			col5.setColumnClass(BigDecimal.class);
			col5.setAlignment(SwingConstants.RIGHT);
			addCol(col5);
			ZEOTableModelColumn col6 = new ZEOTableModelColumn(getDG(), EOFacturePapier.FAP_TOTAL_TTC_KEY, "TTC", 60, app.getCurrencyFormatDisplay());
			col6.setColumnClass(BigDecimal.class);
			col6.setAlignment(SwingConstants.RIGHT);
			addCol(col6);

			initGUI();
		}

		public void reloadData() {
			if (prestationTablePanel.selectedObject() == null) {
				setEditable(false);
				setObjectArray(null);
			}
			else {
				setEditable(true);
				setObjectArray(((EOPrestation) prestationTablePanel.selectedObject()).facturePapiers());
			}
		}

		protected void onAdd() {
		}

		protected void onUpdate() {
			EOFacturePapier facturePapierSelected = (EOFacturePapier) selectedObject();
			if (selectedObject() != null) {
				invaliderFacturePapier(facturePapierSelected);
			}

			if (FacturePapierAddUpdate.sharedInstance().open(facturePapierSelected)) {
				updateData();
			}
		}

		protected void onDelete() {
			if (selectedObject() != null) {
				EOFacturePapier facture = (EOFacturePapier) selectedObject();
				invaliderFacturePapier(facture);

				if (!app.canUseFonction(EOFonction.DROIT_SUPPRIMER_FACTURES, facture.exercice())) {
					app.showInfoDialog("Vous n'\u00EAtes pas autoris\u00E9 \u00E0 supprimer des factures !");
					return;
				}
				if (facture.fapDateValidationPrest() != null) {
					app.showInfoDialog("Cette facture est valid\u00E9e par le prestataire, impossible de la supprimer !");
					return;
				}
				if (!app.showConfirmationDialog("ATTENTION", "Supprimer cette facture??", "OUI", "NON")) {
					return;
				}

				if (facture.hasEcheancierSepa(ec)) {
					app.showErrorDialog("Un \u00E9ch\u00E9ancier SEPA est rattach\u00E9 \u00E0 cette facture. Il doit être supprimé avant de pouvoir poursuivre cette opération.");
					return;
				}

				if (facture.hasEcheancierNational()) {
					if (!app.showConfirmationDialog("Attention !",
							"Un \u00E9ch\u00E9ancier est rattach\u00E9 \u00E0 cette facture, il sera supprim\u00E9 en m\u00EAme temps... Tout supprimer ?", "OUI", "NON")) {
						return;
					}
				}

				try {
					if (facture.hasEcheancierNational()) {
						new org.cocktail.echeancier.client.process.EcheancierProcess(ec, Boolean.TRUE).supprimerEcheancier(facture.echeId());
					}
					FactoryFacturePapier.delFacturePapier(ec, facture, app.appUserInfo().utilisateur());
					prestationTablePanel.updateData();
					FacturePapier.sharedInstance().updateData();
				} catch (Exception e) {
					app.showErrorDialog(e);
					return;
				}
			}
		}

		protected void onSelectionChanged() {
			// disable action buttons if current user doesn't have required permission on the 'organ' object.
			EOFacturePapier facturePapier = (EOFacturePapier) selectedObject();
			if (facturePapier != null
					&& facturePapier.organ() != null
					&& !facturePapier.organ().hasPermission(app.appUserInfo().utilisateur())) {
				this.actionDelete.setEnabled(false);
			}
		}

		protected void invaliderFacturePapier(EOFacturePapier facturePapier) {
			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(facturePapier)));
		}
	}

	private void initGUI() {
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));

		JPanel centerTablePanel = new JPanel(new BorderLayout());
		centerTablePanel.setBorder(BorderFactory.createEmptyBorder());
		centerTablePanel.add(prestationTablePanel, BorderLayout.CENTER);
		centerTablePanel.add(buildToolBar(), BorderLayout.LINE_END);

		tablesPanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true, centerTablePanel, facturePapierTablePanel);
		tablesPanel.setBorder(BorderFactory.createEmptyBorder());
		tablesPanel.setDividerSize(5);

		add(buildSearchBar(), BorderLayout.NORTH);
		add(tablesPanel, BorderLayout.CENTER);

		initInputMap();

	}


	private final JPanel buildSearchBar() {
		JPanel p = new JPanel(new BorderLayout());
		p.setBorder(BorderFactory.createLineBorder(Color.gray, 1));
		JPanel p1 = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
		p1.add(UIUtilities.labeledComponent("Num\u00E9ro", tfPrestNumero, null));
		p1.add(UIUtilities.labeledComponent("Engagement", tfEngNumero, null));
		p1.add(Box.createHorizontalStrut(5));
		p1.add(UIUtilities.labeledComponent("Lib / Client / Catalogue", tfPrestLibelle, null));
		p1.add(Box.createHorizontalStrut(5));
		p1.add(UIUtilities.labeledComponent("Prestataire", cbFournisUlr, null));
		p1.add(UIUtilities.labeledComponent("Nombre de résultats", listMaxResult, null));
		p1.add(UIUtilities.labeledComponent(" ", UIUtilities.makeButtonLib(actionSrch), null));

		p.add(p1, BorderLayout.LINE_START);

		p.add(new JPanel(), BorderLayout.CENTER);

		JPanel p2 = new JPanel(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		tfExercice.setViewOnly();
		tfExercice.setHorizontalAlignment(SwingConstants.CENTER);
		p2.add(UIUtilities.labeledComponent("Exercice", tfExercice, null));
		p.add(p2, BorderLayout.LINE_END);

		JPanel p3 = new JPanel(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		p3.add(UIUtilities.labeledComponent("Recherche Avancée", UIUtilities.makeButton(actionRechercheAvance), null));
		p.add(p3, BorderLayout.LINE_END);

		JPanel finalPanel = new JPanel(new BorderLayout());
		finalPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 5, 20));
		finalPanel.add(p, BorderLayout.CENTER);
		return finalPanel;
	}

	private final JComponent buildToolBar() {
		JToolBar toolBarPanel = new JToolBar("Prestations Toolbar", JToolBar.VERTICAL);
		toolBarPanel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		toolBarPanel.add(UIUtilities.makeButton(actionValideClient));
		toolBarPanel.add(UIUtilities.makeButton(actionValidePrest));
		toolBarPanel.add(UIUtilities.makeButton(actionClorePrestation));
		toolBarPanel.addSeparator(new Dimension(20, 20));
		toolBarPanel.add(UIUtilities.makeButton(actionGenereFacture));
		toolBarPanel.addSeparator(new Dimension(20, 20));
		// TODO
		toolBarPanel.add(UIUtilities.makeButton(actionMail));
		toolBarPanel.add(UIUtilities.makeButton(actionPrint));
		toolBarPanel.addSeparator(new Dimension(20, 20));
		toolBarPanel.addSeparator(new Dimension(20, 20));
		toolBarPanel.add(UIUtilities.makeButton(actionLegend));

		return toolBarPanel;
	}



	// private final JPanel buildBottomPanel() {
	// JPanel p = new JPanel();
	// p.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
	// p.setLayout(new BoxLayout(p, BoxLayout.LINE_AXIS));
	// p.add(new JButton(actionClose));
	// p.add(Box.createHorizontalGlue());
	// return p;
	// }

	private void initInputMap() {
		getActionMap().put("ESCAPE", actionClose);
		getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "ESCAPE");
		getActionMap().put("F5", new ActionUpdate());
		getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0), "F5");
	}

	private class ActionValideClient extends AbstractAction {
		public ActionValideClient() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Valider / d\u00E9valider la prestation c\u00F4t\u00E9 client");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				EOPrestation prestation = (EOPrestation) prestationTablePanel.selectedObject();
				if (prestation == null) {
					return;
				}
				try {
					if (prestation.updateTauxProrata()) {
						ec.saveChanges();
					}
					if (prestation.prestDateValideClient() == null) {
						FactoryPrestation.valideClient(ec, prestation, app.appUserInfo().utilisateur());
					} else {
						FactoryPrestation.devalideClient(ec, prestation, app.appUserInfo().utilisateur());
					}
				} catch (Exception ex) {
					app.showErrorDialog(ex);
				}
				prestationTablePanel.updateData();
			}
		}
	}

	private class ActionValidePrestataire extends AbstractAction {
		public ActionValidePrestataire() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Valider / d\u00E9valider la prestation c\u00F4t\u00E9 prestataire");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				EOPrestation prestation = (EOPrestation) prestationTablePanel.selectedObject();
				if (prestation == null) {
					return;
				}
				try {
					if (prestation.updateTauxProrata()) {
						ec.saveChanges();
					}
					if (prestation.prestDateValidePrest() == null) {
						FactoryPrestation.validePrestataire(ec, prestation, app.appUserInfo().utilisateur());
					} else {
						FactoryPrestation.devalidePrestataire(ec, prestation, app.appUserInfo().utilisateur());
					}
				} catch (Exception ex) {
					app.showErrorDialog(ex);
				}
				prestationTablePanel.updateData();
			}
		}
	}

	private class ActionClorePrestation extends AbstractAction {
		public ActionClorePrestation() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Clore la prestation");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				EOPrestation prestation = (EOPrestation) prestationTablePanel.selectedObject();
				if (prestation == null) {
					return;
				}
				try {
					if (prestation.updateTauxProrata()) {
						ec.saveChanges();
					}
					if (prestation.prestDateCloture() == null) {
						FactoryPrestation.cloture(ec, prestation, app.appUserInfo().utilisateur());
					} else {
						FactoryPrestation.decloture(ec, prestation, app.appUserInfo().utilisateur());
					}
				} catch (Exception ex) {
					app.showErrorDialog(ex);
				}
				prestationTablePanel.updateData();
			}
		}
	}

	private class ActionGenereFacture extends AbstractAction {
		public ActionGenereFacture() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_EXECUTE_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "G\u00E9n\u00E9rer la (les) facture(s)");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				try {
					EOPrestation prestation = (EOPrestation) prestationTablePanel.selectedObject();
					if (prestation.updateTauxProrata()) {
						ec.saveChanges();
					}
					FactoryPrestation.genereFacture(
							ec, prestation, app.appUserInfo().utilisateur());
				} catch (Exception ex) {
					app.showErrorDialog(ex);
					return;
				}
				FacturePapier.sharedInstance().updateData();
				prestationTablePanel.updateData();
			}
		}
	}

	private class ActionPrint extends AbstractAction {
		public ActionPrint() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_PRINT_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer un devis");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onPrint();
			}
		}
	}

	private class ActionMail extends AbstractAction {
		public ActionMail() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_EMAIL_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Envoyer le devis par mail...");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onMail();
			}
		}
	}

	private class ActionLegend extends AbstractAction {
		public ActionLegend() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_COLORS_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "L\u00E9gende des couleurs");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onLegend();
			}
		}
	}

	private class ActionClose extends AbstractAction {
		public ActionClose() {
			super("Fermer");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CLOSE_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onClose();
			}
		}
	}

	//
	//	private class ActionSearchPrestNumero extends AbstractAction {
	//		public ActionSearchPrestNumero() {
	//			super();
	//			putValue(AbstractAction.SMALL_ICON, Constants.ICON_SEARCH_16);
	//		}
	//
	//		public void actionPerformed(ActionEvent e) {
	//			if (isEnabled()) {
	//				onSearchPrestNumero();
	//			}
	//		}
	//	}
	//
	//	private class ActionSearchEngNumero extends AbstractAction {
	//		public ActionSearchEngNumero() {
	//			super();
	//			putValue(AbstractAction.SMALL_ICON, Constants.ICON_SEARCH_16);
	//		}
	//
	//		public void actionPerformed(ActionEvent e) {
	//			if (isEnabled()) {
	//				onSearchEngNumero();
	//			}
	//		}
	//	}

	private class ActionUpdate extends AbstractAction {
		public ActionUpdate() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_REFRESH_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Refresh");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				update();
			}
		}
	}

	private class ActionSrch extends AbstractAction {
		public ActionSrch() {
			super("Rechercher");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_SEARCH_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Lancer la recherche");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				atLeastOnSearchHasBeenMade = true;
				updateData();
			}
		}
	}

	private class ActionRechercheAvance extends AbstractAction {

		public ActionRechercheAvance() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.getIconForName(IconeCocktail.RECHERCHER));
		}

		public void actionPerformed(ActionEvent e) {
			getMonFinderPrestationRechercheAvance().setOpen(true);
			getMonFinderPrestationRechercheAvance().getMonInterfaceRechercheAvance().setEnableSearchRecette(true);
			getMonFinderPrestationRechercheAvance().afficherFenetre();
		}
	}

	public void afficheRechercheAvanceeIfOpen() {
		if (isRechercheAvanceeOpen()) {
			getMonFinderPrestationRechercheAvance().afficherFenetre();
		}
	}

	public boolean isRechercheAvanceeOpen() {
		if (monFinderPrestationRechercheAvance == null) {
			return false;
		}
		return getMonFinderPrestationRechercheAvance().isOpen();
	}

	public void masquerRechercheAvancee() {
		if (monFinderPrestationRechercheAvance == null) {
			return;
		}
		getMonFinderPrestationRechercheAvance().masquerFenetre();
	}

}
