/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.math.BigDecimal;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;

import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.kava.client.finder.FinderLolfNomenclatureDepense;
import org.cocktail.kava.client.finder.FinderPlanComptable;
import org.cocktail.kava.client.finder.FinderPlancoCreditDep;
import org.cocktail.kava.client.finder.FinderTauxProrata;
import org.cocktail.kava.client.finder.FinderTypeApplication;
import org.cocktail.kava.client.finder.FinderTypeCreditDep;
import org.cocktail.kava.client.metier.EOFonction;
import org.cocktail.kava.client.metier.EOParametreDepense;
import org.cocktail.kava.client.metier.EOPlanComptable;
import org.cocktail.kava.client.metier.EOPrestation;
import org.cocktail.kava.client.metier.EOTauxProrata;
import org.cocktail.kava.client.metier.EOUtilisateur;
import org.cocktail.kava.client.procedures.GetDisponible;

import app.client.select.CodeAnalytiqueSelect;
import app.client.select.ConventionSelect;
import app.client.select.LolfNomenclatureDepenseSelect;
import app.client.select.OrganSelect;
import app.client.select.PlancoCreditDepSelect;
import app.client.tools.Constants;
import app.client.ui.UIUtilities;
import app.client.ui.ZEOComboBox;
import app.client.ui.ZLabel;
import app.client.ui.ZNumberField;
import app.client.ui.ZTextField;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class PrestationBudgetClientAddUpdate extends JDialog {

	// private static final Dimension WINDOW_DIMENSION = new Dimension(500, 400);
	private static final String WINDOW_TITLE = "Infos budgetaires client (interne)";

	protected ApplicationClient app;
	protected EOEditingContext ec;

	private EOPrestation eoPrestation;

	private Action actionClose, actionValid, actionCancel;
	private Action actionShowRequired, actionUnshowRequired;
	private Action actionOrganSelect, actionOrganDelete;
	private Action actionPlancoCreditDepSelect, actionPlancoCreditDepDelete;
	private Action actionLolfNomenclatureDepenseSelect, actionLolfNomenclatureDepenseDelete;
	private Action actionCanalSelect, actionCanalDelete;
	private Action actionConventionSelect, actionConventionDelete;

	private CbActionListener cbActionListener;

	private ZTextField tfOrgan;
	private ZEOComboBox cbTypeCreditDep, cbTauxProrata;
	private ZNumberField tfDisponible;
	private ZTextField tfPlancoCreditDep;
	private PlancoCreditDepSelect plancoCreditDepSelect;
	private ZTextField tfLolfNomenclatureDepense;
	private ZTextField tfCanal;
	private ZLabel labelEngagement;

	private OrganSelect organSelect;
	private LolfNomenclatureDepenseSelect lolfNomenclatureDepenseSelect;
	private CodeAnalytiqueSelect canalSelect;
	private ZTextField tfConvention;
	private ConventionSelect conventionSelect;

	private JButton btValid, btCancel;

	private JPanel centerPanel, panelOrganTypCredTap, panelLolfPlanco, panelCanalConvention, panelEngagement;

	private boolean result;

	public PrestationBudgetClientAddUpdate() {
		super((JFrame) null, WINDOW_TITLE, true);

		app = (ApplicationClient) EOApplication.sharedApplication();
		ec = app.editingContext();

		setGlassPane(new BusyGlassPanel());
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new LocalWindowListener());

		actionClose = new ActionClose();
		actionValid = new ActionValid();
		actionCancel = new ActionCancel();
		actionShowRequired = new ActionShowRequired();
		actionUnshowRequired = new ActionUnshowRequired();
		actionOrganSelect = new ActionOrganSelect();
		actionOrganDelete = new ActionOrganDelete();
		actionPlancoCreditDepSelect = new ActionPlancoCreditDepSelect();
		actionPlancoCreditDepDelete = new ActionPlancoCreditDepDelete();
		actionLolfNomenclatureDepenseSelect = new ActionLolfNomenclatureDepenseSelect();
		actionLolfNomenclatureDepenseDelete = new ActionLolfNomenclatureDepenseDelete();
		actionCanalSelect = new ActionCanalSelect();
		actionCanalDelete = new ActionCanalDelete();
		actionConventionSelect = new ActionConventionSelect();
		actionConventionDelete = new ActionConventionDelete();

		cbActionListener = new CbActionListener();

		tfOrgan = new ZTextField(20);
		tfOrgan.setViewOnly();
		cbTauxProrata = new ZEOComboBox(new NSArray(), EOTauxProrata.TAP_TAUX_KEY, null, null, null, 50);
		cbTauxProrata.addActionListener(cbActionListener);
		cbTypeCreditDep = new ZEOComboBox(new NSArray(), EOTypeCredit.LIB_KEY, null, null, null, 150);
		cbTypeCreditDep.addActionListener(cbActionListener);
		tfDisponible = new ZNumberField(10, app.getCurrencyFormatDisplay());
		tfDisponible.setViewOnly();
		tfLolfNomenclatureDepense = new ZTextField(24);
		tfLolfNomenclatureDepense.setViewOnly();
		tfPlancoCreditDep = new ZTextField(22);
		tfPlancoCreditDep.setViewOnly();
		tfCanal = new ZTextField(24);
		tfCanal.setViewOnly();
		tfConvention = new ZTextField(22);
		tfConvention.setViewOnly();

		labelEngagement = new ZLabel();

		btCancel = new JButton(actionCancel);
		btValid = new JButton(actionValid);

		initGUI();
	}

	public void updateData(EOPrestation prestation) {
		this.eoPrestation = prestation;
		updateData();
	}

	public JPanel getCenterPanel() {
		return centerPanel;
	}

	public boolean open(EOPrestation prestation) {
		updateData(prestation);

		result = false;
		// UP : facilité dans Swing
		this.setLocationRelativeTo(null);

		setVisible(true);

		return result;
	}

	public boolean hasPermissionOrganClient() {
		if (this.eoPrestation == null) {
			return false;
		}
		return hasPermissionOrganClient(this.eoPrestation, app.appUserInfo().utilisateur());
	}

	private void onValid() {
		try {
			result = true;
			setVisible(false);
		} catch (Exception e) {
			app.showErrorDialog(e);
		}
	}

	private void onCancel() {
		if (app.showConfirmationDialog("ATTENTION", "Annuler???", "OUI!", "NON")) {
			result = false;
			setVisible(false);
		}
	}

	private void onClose() {
		actionCancel.actionPerformed(null);
	}

	private void setEditable(boolean editable) {
		actionOrganSelect.setEnabled(editable);
		actionOrganDelete.setEnabled(editable);

		cbTauxProrata.setEnabled(editable);

		cbTypeCreditDep.setEnabled(editable);

		actionLolfNomenclatureDepenseSelect.setEnabled(editable);
		actionLolfNomenclatureDepenseDelete.setEnabled(editable);

		actionPlancoCreditDepSelect.setEnabled(editable);
		actionPlancoCreditDepDelete.setEnabled(editable);

		actionCanalSelect.setEnabled(editable);
		actionCanalDelete.setEnabled(editable);

		actionConventionSelect.setEnabled(editable);
		actionConventionDelete.setEnabled(editable);
	}

	public void updateInterfaceEnabling() {
		panelOrganTypCredTap.setVisible(true);
		panelLolfPlanco.setVisible(true);
		panelCanalConvention.setVisible(true);
		panelEngagement.setVisible(false);

		boolean isPrestationInconsistante = isPrestationInconsistante(eoPrestation);
		boolean isPrestationInterne = isPrestationInterne(eoPrestation);

		if (isPrestationInconsistante || !isPrestationInterne) {
			setEditable(false);
			return;
		}

		// si c'est de la prestation interne avec un engagement mais pas de ligne budgetaire cliente, c'est que
		// c'est une prestation interne issue d'une commande carambole, donc pas de modif !
		if (isPrestationFromCarambole(eoPrestation, isPrestationInterne)) {
			setEditable(false);
			panelOrganTypCredTap.setVisible(false);
			panelLolfPlanco.setVisible(false);
			panelCanalConvention.setVisible(false);
			panelEngagement.setVisible(true);
			labelEngagement.setTextHtml(
					"\n<b>Cette prestation interne est issue d'une commande Carambole.</b>\nEngagement No <b>"
					+ eoPrestation.prestationBudgetClient().engageBudget().engNumero() + "</b>\n");
			return;
		}
		updateInterfaceBudgetClient(
				hasPermissionOrganClient(eoPrestation, app.appUserInfo().utilisateur())
				|| app.canUseFonction(EOFonction.DROIT_GERER_PRESTATIONS_REPRO, null));
	}

	private void updateInterfaceBudgetClient(boolean hasFullAccess) {
		actionOrganSelect.setEnabled(hasFullAccess && eoPrestation.prestDateValideClient() == null);
		actionOrganDelete.setEnabled(hasFullAccess
				&& eoPrestation.prestationBudgetClient().organ() != null
				&& eoPrestation.prestDateValideClient() == null);

		cbTauxProrata.setEnabled(hasFullAccess
				&& eoPrestation.prestationBudgetClient().organ() != null
				&& eoPrestation.prestDateValideClient() == null);

		cbTypeCreditDep.setEnabled(hasFullAccess
				&& eoPrestation.prestationBudgetClient().organ() != null
				&& eoPrestation.prestDateValideClient() == null);

		actionLolfNomenclatureDepenseSelect.setEnabled(hasFullAccess && eoPrestation.prestDateValideClient() == null);
		actionLolfNomenclatureDepenseDelete.setEnabled(hasFullAccess
				&& eoPrestation.prestationBudgetClient().lolfNomenclatureDepense() != null
				&& eoPrestation.prestDateValideClient() == null);

		actionPlancoCreditDepSelect.setEnabled(hasFullAccess
				&& eoPrestation.prestationBudgetClient().typeCreditDep() != null
				&& eoPrestation.prestDateValideClient() == null);
		actionPlancoCreditDepDelete.setEnabled(hasFullAccess
				&& eoPrestation.prestationBudgetClient().pcoNum() != null
				&& eoPrestation.prestDateValideClient() == null);

		actionCanalSelect.setEnabled(hasFullAccess
				&& eoPrestation.prestationBudgetClient().organ() != null
				&& eoPrestation.prestDateValideClient() == null);
		actionCanalDelete.setEnabled(hasFullAccess
				&& eoPrestation.prestationBudgetClient().codeAnalytique() != null
				&& eoPrestation.prestDateValideClient() == null);

		actionConventionSelect.setEnabled(hasFullAccess
				&& eoPrestation.prestationBudgetClient().organ() != null
				&& eoPrestation.prestationBudgetClient().typeCreditDep() != null
				&& eoPrestation.prestDateValideClient() == null);
		actionConventionDelete.setEnabled(hasFullAccess
				&& eoPrestation.prestationBudgetClient().convention() != null
				&& eoPrestation.prestDateValideClient() == null);
	}

	private boolean isPrestationInconsistante(EOPrestation prestation) {
		return prestation == null || prestation.prestationBudgetClient() == null;
	}

	private boolean isPrestationFromCarambole(EOPrestation prestation, boolean isInterne) {
		return isInterne
				&& prestation.prestationBudgetClient() != null
				&& prestation.prestationBudgetClient().engageBudget() != null
				&& prestation.prestationBudgetClient().organ() == null;
	}

	private boolean hasPermissionOrganClient(EOPrestation prestation, EOUtilisateur loggedUser) {
		if (!isPrestationInterne(prestation)) {
			return false;
		}

		boolean currentPermissionOrganClient = true;
		if (isPrestationInterne(prestation)
				&& prestation.prestationBudgetClient() != null
				&& prestation.prestationBudgetClient().organ() != null) {
			currentPermissionOrganClient = prestation.prestationBudgetClient().organ().hasPermission(loggedUser);
		}
		return currentPermissionOrganClient;
	}

	private boolean isPrestationInterne(EOPrestation prestation) {
		return prestation != null
				&& prestation.typePublic() != null
				&& prestation.typePublic().typeApplication().equals(
						FinderTypeApplication.typeApplicationPrestationInterne(ec));
	}

	private void updateData() {
		if (eoPrestation.prestationBudgetClient() != null && eoPrestation.prestationBudgetClient().organ() != null) {
			tfOrgan.setText(eoPrestation.prestationBudgetClient().organ().orgLib());
		} else {
			tfOrgan.setText(null);
		}

		if (eoPrestation.prestationBudgetClient() != null && eoPrestation.prestationBudgetClient().organ() != null) {
			cbTauxProrata.removeActionListener(cbActionListener);
			cbTypeCreditDep.removeActionListener(cbActionListener);
			cbTauxProrata.setObjects(FinderTauxProrata.find(ec, eoPrestation.prestationBudgetClient().organ(), eoPrestation.exercice()));
			cbTypeCreditDep.setObjects(FinderTypeCreditDep.find(ec, eoPrestation.exercice(), eoPrestation.prestationBudgetClient().organ()));
			if (eoPrestation.prestationBudgetClient().tauxProrata() != null) {
				cbTauxProrata.setSelectedEOObject(eoPrestation.prestationBudgetClient().tauxProrata());
			}
			if (eoPrestation.prestationBudgetClient().typeCreditDep() != null) {
				cbTypeCreditDep.setSelectedEOObject(eoPrestation.prestationBudgetClient().typeCreditDep());
			}
			cbTauxProrata.addActionListener(cbActionListener);
			cbTypeCreditDep.addActionListener(cbActionListener);
		} else {
			cbTauxProrata.setObjects(null);
			cbTauxProrata.clean();
			cbTypeCreditDep.setObjects(null);
			cbTypeCreditDep.clean();
		}

		updateDataDisponible();

		if (eoPrestation.prestationBudgetClient() != null
				&& eoPrestation.prestationBudgetClient().lolfNomenclatureDepense() != null) {
			tfLolfNomenclatureDepense.setText(
					eoPrestation.prestationBudgetClient().lolfNomenclatureDepense().lolfCode() + " - "
					+ eoPrestation.prestationBudgetClient().lolfNomenclatureDepense().lolfLibelle());
		} else {
			tfLolfNomenclatureDepense.setText(null);
		}

		if (eoPrestation.prestationBudgetClient() != null && eoPrestation.prestationBudgetClient().pcoNum() != null) {
			EOPlanComptable prestBudgetClientPlanComptable = FinderPlanComptable.findOnlyValid(
					ec, eoPrestation.exercice(), eoPrestation.prestationBudgetClient().pcoNum());
			changeLibellePlanComptableCreditDepense(prestBudgetClientPlanComptable);
		} else {
			tfPlancoCreditDep.setText(null);
		}

		if (eoPrestation.prestationBudgetClient() != null
				&& eoPrestation.prestationBudgetClient().codeAnalytique() != null) {
			tfCanal.setText(eoPrestation.prestationBudgetClient().codeAnalytique().canCode() + " - "
					+ eoPrestation.prestationBudgetClient().codeAnalytique().canLibelle());
		} else {
			tfCanal.setText(null);
		}

		if (eoPrestation.prestationBudgetClient() != null
				&& eoPrestation.prestationBudgetClient().convention() != null) {
			tfConvention.setText(eoPrestation.prestationBudgetClient().convention().conReferenceExterne());
		} else {
			tfConvention.setText(null);
		}

		updateInterfaceEnabling();
	}

	private void changeLibellePlanComptableCreditDepense(EOPlanComptable prestBudgetClientPlanComptable) {
		tfPlancoCreditDep.clean();
		if (prestBudgetClientPlanComptable != null) {
			tfPlancoCreditDep.setText(
					prestBudgetClientPlanComptable.pcoNum() + " - "	+ prestBudgetClientPlanComptable.pcoLibelle());
		}
	}

	public void updateDataDisponible() {
		if (eoPrestation != null && eoPrestation.prestationBudgetClient() != null) {
			BigDecimal bd = GetDisponible.get(
					ec, eoPrestation.exercice(), eoPrestation.prestationBudgetClient().organ(),
					eoPrestation.prestationBudgetClient().typeCreditDep());
			tfDisponible.setNumber(bd);
			if (bd != null && eoPrestation.prestTotalTtc() != null && eoPrestation.prestDateValideClient() == null
					&& bd.compareTo(eoPrestation.prestTotalTtc()) == -1) {
				tfDisponible.setForeground(Color.red);
			} else {
				tfDisponible.setForeground(Constants.COLOR_TF_FGD_DISABLED);
			}
		} else {
			tfDisponible.setNumber(null);
			tfDisponible.setForeground(Constants.COLOR_TF_FGD_DISABLED);
		}
	}

	public void showRequired(boolean show) {
		tfOrgan.showRequired(show);
		cbTauxProrata.showRequired(show);
		cbTypeCreditDep.showRequired(show);
		tfLolfNomenclatureDepense.showRequired(show);
		tfPlancoCreditDep.showRequired(show);
	}



	private void onOrganSelect() {
		if (organSelect == null) {
			organSelect = new OrganSelect();
		}
		if (organSelect.openDepense(app.appUserInfo().utilisateur(), eoPrestation.exercice(), eoPrestation.prestationBudgetClient().organ(),
				new Integer(3))) {
			eoPrestation.prestationBudgetClient().setOrganRelationship(organSelect.getSelected());
			if (eoPrestation.prestationBudgetClient().organ() != null) {
				tfOrgan.setText(eoPrestation.prestationBudgetClient().organ().orgLib());
				cbTauxProrata.setObjects(FinderTauxProrata.find(ec, eoPrestation.prestationBudgetClient().organ(), eoPrestation.exercice()));
				cbTypeCreditDep.setObjects(FinderTypeCreditDep.find(ec, eoPrestation.exercice(), eoPrestation.prestationBudgetClient().organ()));

				// si on controle la destination par rapport a l'organ et au type cred, on verifie que s'il y en a un de selectionne deja, il est
				// toujours autorise, sinon on le vire
				if (app.getParamDepenseBoolean(EOParametreDepense.PARAM_CONTROLE_ORGAN_DEST, eoPrestation.exercice())) {
					if (eoPrestation.prestationBudgetClient().organ() != null && eoPrestation.prestationBudgetClient().typeCreditDep() != null
							&& eoPrestation.prestationBudgetClient().lolfNomenclatureDepense() != null) {
						NSArray a = FinderLolfNomenclatureDepense.find(ec, eoPrestation.exercice(), eoPrestation.prestationBudgetClient()
								.organ(), eoPrestation.prestationBudgetClient().typeCreditDep());
						if (!a.containsObject(eoPrestation.prestationBudgetClient().lolfNomenclatureDepense())) {
							eoPrestation.prestationBudgetClient().setLolfNomenclatureDepenseRelationship(null);
							tfLolfNomenclatureDepense.setText(null);
						}
					}
				}
			} else {
				tfOrgan.setText(null);
				cbTauxProrata.setObjects(null);
				cbTauxProrata.clean();
				cbTypeCreditDep.setObjects(null);
				cbTypeCreditDep.clean();
			}
			updateDataDisponible();
			updateInterfaceEnabling();
		}
	}

	private void onOrganDelete() {
		eoPrestation.prestationBudgetClient().setOrganRelationship(null);
		tfOrgan.setText(null);
		cbTauxProrata.setObjects(null);
		cbTauxProrata.clean();
		cbTypeCreditDep.setObjects(null);
		cbTypeCreditDep.clean();
		updateInterfaceEnabling();
	}

	private void onLolfNomenclatureDepenseSelect() {
		if (lolfNomenclatureDepenseSelect == null) {
			lolfNomenclatureDepenseSelect = new LolfNomenclatureDepenseSelect();
		}
		if (lolfNomenclatureDepenseSelect
				.open(app.appUserInfo().utilisateur(), eoPrestation.exercice(), eoPrestation.prestationBudgetClient().organ(), eoPrestation
						.prestationBudgetClient().typeCreditDep(), eoPrestation.prestationBudgetClient().lolfNomenclatureDepense())) {
			eoPrestation.prestationBudgetClient().setLolfNomenclatureDepenseRelationship(lolfNomenclatureDepenseSelect.getSelected());
			if (eoPrestation.prestationBudgetClient().lolfNomenclatureDepense() != null) {
				tfLolfNomenclatureDepense.setText(eoPrestation.prestationBudgetClient().lolfNomenclatureDepense().lolfCode() + "."
						+ eoPrestation.prestationBudgetClient().lolfNomenclatureDepense().lolfLibelle());
			}
			updateInterfaceEnabling();
		}
	}

	private void onLolfNomenclatureDepenseDelete() {
		eoPrestation.prestationBudgetClient().setLolfNomenclatureDepenseRelationship(null);
		tfLolfNomenclatureDepense.setText(null);
		updateInterfaceEnabling();
	}

	private void onPlancoCreditDepSelect() {
		if (plancoCreditDepSelect == null) {
			plancoCreditDepSelect = new PlancoCreditDepSelect();
		}
		if (plancoCreditDepSelect.open(app.appUserInfo().utilisateur(), eoPrestation.exercice(),
				eoPrestation.prestationBudgetClient().typeCreditDep(),
				eoPrestation.prestationBudgetClient().pcoNum())) {
			if (plancoCreditDepSelect.getSelected() != null) {
				eoPrestation.prestationBudgetClient().setPcoNum(plancoCreditDepSelect.getSelected().pcoNum());
			}
			if (eoPrestation.prestationBudgetClient().pcoNum() != null) {
				EOPlanComptable prestBudgetClientPlanComptable = FinderPlanComptable.findOnlyValid(
						ec, eoPrestation.exercice(), eoPrestation.prestationBudgetClient().pcoNum());
				changeLibellePlanComptableCreditDepense(prestBudgetClientPlanComptable);
			}
			updateInterfaceEnabling();
		}
	}

	private void onPlancoCreditDepDelete() {
		eoPrestation.prestationBudgetClient().setPcoNum(null);
		tfPlancoCreditDep.setText(null);
		updateInterfaceEnabling();
	}

	private void onCanalSelect() {
		if (canalSelect == null) {
			canalSelect = new CodeAnalytiqueSelect();
		}
		if (canalSelect.open(app.appUserInfo().utilisateur(), eoPrestation.exercice(), eoPrestation.prestationBudgetClient().organ(),
				eoPrestation.prestationBudgetClient().codeAnalytique())) {
			eoPrestation.prestationBudgetClient().setCodeAnalytiqueRelationship(canalSelect.getSelected());
			if (eoPrestation.prestationBudgetClient().codeAnalytique() != null) {
				tfCanal.setText(eoPrestation.prestationBudgetClient().codeAnalytique().canCode() + " - "
						+ eoPrestation.prestationBudgetClient().codeAnalytique().canLibelle());
			}
			updateInterfaceEnabling();
		}
	}

	private void onCanalDelete() {
		eoPrestation.prestationBudgetClient().setCodeAnalytiqueRelationship(null);
		tfCanal.setText(null);
		updateInterfaceEnabling();
	}

	private void onConventionSelect() {
		if (conventionSelect == null) {
			conventionSelect = new ConventionSelect();
		}
		if (conventionSelect.openDep(app.appUserInfo().utilisateur(), eoPrestation.exercice(), eoPrestation.prestationBudgetClient().organ(),
				eoPrestation.prestationBudgetClient().typeCreditDep(), eoPrestation.prestationBudgetClient().convention(), null)) {
			eoPrestation.prestationBudgetClient().setConventionRelationship(conventionSelect.getSelected());
			if (eoPrestation.prestationBudgetClient().convention() != null) {
				tfConvention.setText(eoPrestation.prestationBudgetClient().convention().conReferenceExterne());
			}
			updateInterfaceEnabling();
		}
	}

	private void onConventionDelete() {
		eoPrestation.prestationBudgetClient().setConventionRelationship(null);
		tfConvention.setText(null);
		updateInterfaceEnabling();
	}

	private void initGUI() {
		JPanel contentPanel = new JPanel(new BorderLayout());
		contentPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		// contentPanel.setPreferredSize(WINDOW_DIMENSION);
		setContentPane(contentPanel);

		panelOrganTypCredTap = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panelOrganTypCredTap.add(UIUtilities.labeledComponent("Ligne budg\u00E9taire", tfOrgan,
				new Action[] {
						actionOrganSelect, actionOrganDelete
				}, UIUtilities.DEFAULT_BOXED, UIUtilities.DEFAULT_ALIGNMENT));
		panelOrganTypCredTap.add(Box.createHorizontalStrut(10));
		panelOrganTypCredTap.add(UIUtilities.labeledComponent("Taux de prorata", cbTauxProrata, null));
		panelOrganTypCredTap.add(Box.createHorizontalStrut(10));
		panelOrganTypCredTap.add(UIUtilities.labeledComponent("Type de cr\u00E9dit", cbTypeCreditDep, null));
		panelOrganTypCredTap.add(Box.createHorizontalStrut(10));
		panelOrganTypCredTap.add(UIUtilities.labeledComponent("Disponible", tfDisponible, null));

		panelLolfPlanco = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panelLolfPlanco.add(UIUtilities.labeledComponent("Action Lolf", tfLolfNomenclatureDepense, new Action[] {
				actionLolfNomenclatureDepenseSelect, actionLolfNomenclatureDepenseDelete
		}, UIUtilities.DEFAULT_BOXED,
				UIUtilities.DEFAULT_ALIGNMENT));
		panelLolfPlanco.add(Box.createHorizontalStrut(10));
		panelLolfPlanco.add(UIUtilities.labeledComponent("Imputation comptable", tfPlancoCreditDep, new Action[] {
				actionPlancoCreditDepSelect,
				actionPlancoCreditDepDelete
		}, UIUtilities.DEFAULT_BOXED, UIUtilities.DEFAULT_ALIGNMENT));

		panelCanalConvention = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panelCanalConvention.add(UIUtilities.labeledComponent("Code analytique", tfCanal, new Action[] {
				actionCanalSelect, actionCanalDelete
		},
				UIUtilities.DEFAULT_BOXED, UIUtilities.DEFAULT_ALIGNMENT));
		panelCanalConvention.add(Box.createHorizontalStrut(10));
		panelCanalConvention.add(UIUtilities.labeledComponent("Convention", tfConvention, new Action[] {
				actionConventionSelect,
				actionConventionDelete
		}, UIUtilities.DEFAULT_BOXED, UIUtilities.DEFAULT_ALIGNMENT));

		panelEngagement = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panelEngagement.add(UIUtilities.labeledComponent(null, labelEngagement, null));

		centerPanel = new JPanel();
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.PAGE_AXIS));
		centerPanel.setBorder(BorderFactory.createEmptyBorder());
		centerPanel.add(panelOrganTypCredTap);
		centerPanel.add(panelLolfPlanco);
		centerPanel.add(panelCanalConvention);
		centerPanel.add(panelEngagement);

		getContentPane().add(centerPanel, BorderLayout.CENTER);
		getContentPane().add(buildBottomPanel(), BorderLayout.PAGE_END);

		initInputMap();

		this.validate();
		this.pack();
	}

	private final JPanel buildBottomPanel() {
		JPanel p = new JPanel();
		p.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.add(Box.createHorizontalGlue());
		p.add(btCancel);
		p.add(Box.createRigidArea(new Dimension(5, 0)));
		p.add(btValid);
		return p;
	}

	private void initInputMap() {
		((JComponent) getContentPane()).getActionMap().put("CTRL_Q", app.superviseur().mainMenu.actionQuit);
		((JComponent) getContentPane()).getActionMap().put("CTRL_S", actionValid);
		((JComponent) getContentPane()).getActionMap().put("F10", actionValid);
		((JComponent) getContentPane()).getActionMap().put("ESCAPE", actionCancel);
		((JComponent) getContentPane()).getActionMap().put("F8", actionShowRequired);
		((JComponent) getContentPane()).getActionMap().put("ALT_F8", actionUnshowRequired);
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
				KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK), "CTRL_Q");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
				KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK), "CTRL_S");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F10, 0), "F10");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"ESCAPE");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("F8"), "F8");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("released F8"), "ALT_F8");
	}

	private class ActionValid extends AbstractAction {
		public ActionValid() {
			super("Valider");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onValid();
			}
		}
	}

	private class ActionCancel extends AbstractAction {
		public ActionCancel() {
			super("Annuler");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CANCEL_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onCancel();
			}
		}
	}

	private class ActionClose extends AbstractAction {
		public ActionClose() {
			super("Fermer");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CLOSE_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onClose();
			}
		}
	}

	private class ActionShowRequired extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				showRequired(true);
			}
		}
	}

	private class ActionUnshowRequired extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				showRequired(false);
			}
		}
	}

	private class ActionOrganSelect extends AbstractAction {
		public ActionOrganSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onOrganSelect();
			}
		}
	}

	private class ActionOrganDelete extends AbstractAction {
		public ActionOrganDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onOrganDelete();
			}
		}
	}

	private class ActionPlancoCreditDepSelect extends AbstractAction {
		public ActionPlancoCreditDepSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onPlancoCreditDepSelect();
			}
		}
	}

	private class ActionPlancoCreditDepDelete extends AbstractAction {
		public ActionPlancoCreditDepDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onPlancoCreditDepDelete();
			}
		}
	}

	private class ActionLolfNomenclatureDepenseSelect extends AbstractAction {
		public ActionLolfNomenclatureDepenseSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onLolfNomenclatureDepenseSelect();
			}
		}
	}

	private class ActionLolfNomenclatureDepenseDelete extends AbstractAction {
		public ActionLolfNomenclatureDepenseDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onLolfNomenclatureDepenseDelete();
			}
		}
	}

	private class ActionCanalSelect extends AbstractAction {
		public ActionCanalSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onCanalSelect();
			}
		}
	}

	private class ActionCanalDelete extends AbstractAction {
		public ActionCanalDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onCanalDelete();
			}
		}
	}

	private class ActionConventionSelect extends AbstractAction {
		public ActionConventionSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onConventionSelect();
			}
		}
	}

	private class ActionConventionDelete extends AbstractAction {
		public ActionConventionDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onConventionDelete();
			}
		}
	}

	private class CbActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JComboBox cb = (JComboBox) e.getSource();
			if (cb == cbTauxProrata && eoPrestation != null) {
				eoPrestation.prestationBudgetClient().setTauxProrataRelationship((EOTauxProrata) cbTauxProrata.getSelectedEOObject());
				updateInterfaceEnabling();
			}
			if (cb == cbTypeCreditDep && eoPrestation != null) {
				eoPrestation.prestationBudgetClient().setTypeCreditDepRelationship((EOTypeCredit) cbTypeCreditDep.getSelectedEOObject());
				// si on a un type de credit et qu'on avait deja un plan comptable, on verifie que ce plan comptable est toujours autorise pour
				// ce nouveau type de credit, sinon on le vire
				if (eoPrestation.prestationBudgetClient().typeCreditDep() != null
						&& eoPrestation.prestationBudgetClient().pcoNum() != null) {

					// UP 02/09/2009 : tenir compte de l'exercice
					NSArray a = FinderPlancoCreditDep.find(ec, eoPrestation.prestationBudgetClient().typeCreditDep(), eoPrestation.exercice());
					EOPlanComptable prestBudgetClientPlanComptable = FinderPlanComptable.findOnlyValid(
							ec, eoPrestation.exercice(), eoPrestation.prestationBudgetClient().pcoNum());
					if (!a.containsObject(prestBudgetClientPlanComptable)) {
						eoPrestation.prestationBudgetClient().setPcoNum(null);
						tfPlancoCreditDep.setText(null);
					}
				}
				// si on controle la destination par rapport a l'organ et au type cred, on verifie que s'il y en a un de selectionne deja, il est
				// toujours autorise, sinon on le vire
				if (app.getParamDepenseBoolean(EOParametreDepense.PARAM_CONTROLE_ORGAN_DEST, eoPrestation.exercice())) {
					if (eoPrestation.prestationBudgetClient().organ() != null && eoPrestation.prestationBudgetClient().typeCreditDep() != null
							&& eoPrestation.prestationBudgetClient().lolfNomenclatureDepense() != null) {
						NSArray a = FinderLolfNomenclatureDepense.find(ec, eoPrestation.exercice(), eoPrestation.prestationBudgetClient()
								.organ(), eoPrestation.prestationBudgetClient().typeCreditDep());
						if (!a.containsObject(eoPrestation.prestationBudgetClient().lolfNomenclatureDepense())) {
							eoPrestation.prestationBudgetClient().setLolfNomenclatureDepenseRelationship(null);
							tfLolfNomenclatureDepense.setText(null);
						}
					}
				}
				updateDataDisponible();
				updateInterfaceEnabling();
			}
		}
	}

	private class LocalWindowListener implements WindowListener {
		public void windowActivated(WindowEvent arg0) {
		}

		public void windowClosed(WindowEvent arg0) {
		}

		public void windowClosing(WindowEvent arg0) {
			actionClose.actionPerformed(null);
		}

		public void windowDeactivated(WindowEvent arg0) {
		}

		public void windowDeiconified(WindowEvent arg0) {
		}

		public void windowIconified(WindowEvent arg0) {
		}

		public void windowOpened(WindowEvent arg0) {
		}
	}

	public void setWaitCursor(boolean bool) {
		if (bool) {
			this.getGlassPane().setVisible(true);
		}
		else {
			this.getGlassPane().setVisible(false);
		}
	}

	/**
	 * Permet de definir un panel qui sert a afficher un waitcursor et a interdire toute modif sur le panel.
	 *
	 * @author rprin
	 */
	public final class BusyGlassPanel extends JPanel {
		public final Color COLOR_WASH = new Color(64, 64, 64, 32);

		public BusyGlassPanel() {
			super.setOpaque(false);
			super.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

			// Suck up them events!!!
			super.addKeyListener((new KeyAdapter() {
			}));
			super.addMouseListener((new MouseAdapter() {
			}));
			super.addMouseMotionListener((new MouseMotionAdapter() {
			}));
		}

		public final void paintComponent(Graphics p_graphics) {
			Dimension l_size = super.getSize();

			// Wash the pane with translucent gray.
			p_graphics.setColor(COLOR_WASH);
			p_graphics.fillRect(0, 0, l_size.width, l_size.height);

			// Paint a grid of white/black dots.
			p_graphics.setColor(Color.white);
			for (int j = 3; j < l_size.height; j += 8) {
				for (int i = 3; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
			p_graphics.setColor(Color.black);
			for (int j = 4; j < l_size.height; j += 8) {
				for (int i = 4; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
		}
	}

}
