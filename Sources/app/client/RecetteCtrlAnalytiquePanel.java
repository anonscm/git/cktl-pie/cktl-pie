/*
CRI - Universite de La Rochelle, 2006

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.awt.Dimension;
import java.math.BigDecimal;

import javax.swing.JTextField;

import org.cocktail.kava.client.factory.FactoryRecetteCtrlAnalytique;
import org.cocktail.kava.client.metier.EOCodeAnalytique;
import org.cocktail.kava.client.metier.EORecette;
import org.cocktail.kava.client.metier.EORecetteCtrlAnalytique;

import app.client.select.CodeAnalytiqueSelect;
import app.client.ui.table.ZEOTableModelColumn;
import app.client.ui.table.ZExtendedTablePanel;

public class RecetteCtrlAnalytiquePanel extends CtrlPanel {

	private static final boolean	DEFAULT_MULTIPLE_INIT			= true;	// demarre par defaut en multiple ou single ?
	private static final boolean	DEFAULT_MULTIPLE_INIT_LOCKED	= false;	// interdit ou non le changement

	private ZEOTableModelColumn		colHT, colTTC;

	private CodeAnalytiqueSelect	codeAnalytiqueSelect;

	private EORecette				currentObject;

	public RecetteCtrlAnalytiquePanel() {
		super("Code analytique", 25);
		codeAnalytiqueSelect = new CodeAnalytiqueSelect();
	}

	public void setEditable(boolean editable) {
		colHT.setEditable(editable);
		colTTC.setEditable(editable);
		super.setEditable(editable);
	}

	public void setColumnsEditable(boolean editable) {
		colHT.setEditable(editable);
		colTTC.setEditable(editable);
	}

	public void setCurrentObject(EORecette object) {
		this.currentObject = object;
		updateData();
	}

	public void add(EOCodeAnalytique eo) {
		if (currentObject == null || eo == null) {
			return;
		}
		// si on est en mode multiple ou qu'il n'y a pas encore de ctrl, on ajoute
		if (multipleEnable || currentObject.recetteCtrlAnalytiques() == null || currentObject.recetteCtrlAnalytiques().count() == 0) {
			EORecetteCtrlAnalytique object = FactoryRecetteCtrlAnalytique.newObject(ec);
			object.setCodeAnalytiqueRelationship(eo);
			object.setExerciceRelationship(currentObject.exercice());
			setMontants(object);
			currentObject.addToRecetteCtrlAnalytiquesRelationship(object);
		}
		else {
			// on est en mode single et y'a deja un ctrl, on le modifie
			((EORecetteCtrlAnalytique) currentObject.recetteCtrlAnalytiques().objectAtIndex(0)).setCodeAnalytiqueRelationship(eo);
			setMontants((EORecetteCtrlAnalytique) currentObject.recetteCtrlAnalytiques().objectAtIndex(0));
		}
		updateData();
	}

	protected void onAdd() {
		if (codeAnalytiqueSelect.open(currentObject.utilisateur(), currentObject.exercice(), currentObject.facture().organ(), null)) {
			try {
				add(codeAnalytiqueSelect.getSelected());
			}
			catch (Exception e) {
				System.err.println("Erreur lors de la tentative d'insertion d'un nouvel objet RecetteCtrlAnalytique: " + e);
			}
		}
	}

	protected void onSelect() {
		EOCodeAnalytique current = null;
		if (currentObject.recetteCtrlAnalytiques() != null && currentObject.recetteCtrlAnalytiques().count() > 0) {
			current = ((EORecetteCtrlAnalytique) currentObject.recetteCtrlAnalytiques().objectAtIndex(0)).codeAnalytique();
		}
		if (codeAnalytiqueSelect.open(currentObject.utilisateur(), currentObject.exercice(), currentObject.facture().organ(), current)) {
			try {
				add(codeAnalytiqueSelect.getSelected());
			}
			catch (Exception e) {
				System.err.println("Erreur lors de la tentative d'insertion/modification d'un nouvel objet RecetteCtrlAnalytique: " + e);
			}
		}
	}

	protected void onUpdate() {
		if (table.selectedObject() != null) {
			if (codeAnalytiqueSelect.open(currentObject.utilisateur(), currentObject.exercice(), currentObject.facture().organ(),
					((EORecetteCtrlAnalytique) table.selectedObject()).codeAnalytique())) {
				((EORecetteCtrlAnalytique) table.selectedObject()).setCodeAnalytiqueRelationship(codeAnalytiqueSelect.getSelected());
				table.updateUI();
			}
		}
	}

	protected void onDelete() {
		if (table.selectedObject() != null && app.showConfirmationDialog("ATTENTION", "Supprimer cette ligne??", "OUI", "NON")) {
			currentObject.removeFromRecetteCtrlAnalytiquesRelationship((EORecetteCtrlAnalytique) table.selectedObject());
			ec.deleteObject(table.selectedObject());
			updateData();
		}
	}

	private void setMontants(EORecetteCtrlAnalytique ctrl) {
		// on fixe le ht par rapport soit au restant du montant ht de la recette, soit au montant ht de la recette
		BigDecimal newHt = currentObject.recHtSaisie();
		if (multipleEnable && newHt != null) {
			newHt = newHt.subtract((BigDecimal) currentObject.recetteCtrlAnalytiques().valueForKey(
					"@sum." + EORecetteCtrlAnalytique.RANA_HT_SAISIE_KEY));
		}
		if (newHt == null || newHt.doubleValue() < 0.0) {
			newHt = new BigDecimal(0.0);
		}
		// on fixe le ttc par rapport soit au restant du montant ttc de la recette, soit au montant ttc de la recette
		BigDecimal newTtc = currentObject.recTtcSaisie();
		if (multipleEnable && newTtc != null) {
			newTtc = newTtc.subtract((BigDecimal) currentObject.recetteCtrlAnalytiques().valueForKey(
					"@sum." + EORecetteCtrlAnalytique.RANA_TTC_SAISIE_KEY));
		}
		if (newTtc == null || newTtc.doubleValue() < 0.0) {
			newTtc = new BigDecimal(0.0);
		}
		ctrl.setRanaHtSaisie(newHt);
		ctrl.setRanaTtcSaisie(newTtc);
		table.updateData();
	}

	public void updateMontants() {
		if (currentObject.recetteCtrlAnalytiques() == null || currentObject.recetteCtrlAnalytiques().count() != 1) {
			return;
		}
		((EORecetteCtrlAnalytique) currentObject.recetteCtrlAnalytiques().objectAtIndex(0)).setRanaHtSaisie(currentObject.recHtSaisie());
		((EORecetteCtrlAnalytique) currentObject.recetteCtrlAnalytiques().objectAtIndex(0)).setRanaTtcSaisie(currentObject.recTtcSaisie());
		table.updateData();
	}

	protected void updateData() {
		if (currentObject == null) {
			table.setObjectArray(null);
			tfCtrl.clean();
		}
		else {
			if (multipleEnable) {
				table.setObjectArray(currentObject.recetteCtrlAnalytiques());
			}
			else {
				if (currentObject.recetteCtrlAnalytiques() == null || currentObject.recetteCtrlAnalytiques().count() == 0) {
					tfCtrl.clean();
				}
				else {
					// on vire ceux qui sont en trop ! et on en garde un... le dernier...
					while (currentObject.recetteCtrlAnalytiques().count() > 1) {
						EORecetteCtrlAnalytique ctrl = (EORecetteCtrlAnalytique) currentObject.recetteCtrlAnalytiques().objectAtIndex(0);
						currentObject.removeFromRecetteCtrlAnalytiquesRelationship(ctrl);
						ec.deleteObject(ctrl);
					}
					EORecetteCtrlAnalytique ctrl = (EORecetteCtrlAnalytique) currentObject.recetteCtrlAnalytiques().objectAtIndex(0);
					setMontants(ctrl);
					tfCtrl.setText(ctrl.codeAnalytique().canCode() + " - " + ctrl.codeAnalytique().canLibelle());
				}
			}
		}
	}

	private final class TablePanel extends ZExtendedTablePanel {

		public TablePanel() {
			super("Codes analytiques");

			final ZEOTableModelColumn.ZEONumFieldTableCellEditor _editor = new ZEOTableModelColumn.ZEONumFieldTableCellEditor(new JTextField(),
					app.getCurrencyFormatEdit());

			addCol(new ZEOTableModelColumn(getDG(), EORecetteCtrlAnalytique.CODE_ANALYTIQUE_KEY + "." + EOCodeAnalytique.CAN_CODE_KEY, "Code",
					40));
			addCol(new ZEOTableModelColumn(getDG(), EORecetteCtrlAnalytique.CODE_ANALYTIQUE_KEY + "." + EOCodeAnalytique.CAN_LIBELLE_KEY, "Lib",
					100));
			colHT = new ZEOTableModelColumn(getDG(), EORecetteCtrlAnalytique.RANA_HT_SAISIE_KEY, "HT", 60, app.getCurrencyFormatDisplay());
			colHT.setEditable(true);
			colHT.setTableCellEditor(_editor);
			colHT.setFormatEdit(app.getCurrencyFormatEdit());
			colHT.setColumnClass(BigDecimal.class);
			addCol(colHT);
			colTTC = new ZEOTableModelColumn(getDG(), EORecetteCtrlAnalytique.RANA_TTC_SAISIE_KEY, "TTC", 60, app.getCurrencyFormatDisplay());
			colTTC.setEditable(true);
			colTTC.setTableCellEditor(_editor);
			colTTC.setFormatEdit(app.getCurrencyFormatEdit());
			colTTC.setColumnClass(BigDecimal.class);
			addCol(colTTC);

			initGUI();

			setPreferredSize(new Dimension(0, 70));
		}

		protected void onAdd() {
			RecetteCtrlAnalytiquePanel.this.onAdd();
		}

		protected void onUpdate() {
			RecetteCtrlAnalytiquePanel.this.onUpdate();
		}

		protected void onDelete() {
			RecetteCtrlAnalytiquePanel.this.onDelete();
		}

		protected void onSelectionChanged() {
		}

	}

	protected boolean defaultMultipleInit() {
		return DEFAULT_MULTIPLE_INIT;
	}

	protected boolean defaultMultipleInitLocked() {
		return DEFAULT_MULTIPLE_INIT_LOCKED;
	}

	protected ZExtendedTablePanel getTablePanel() {
		return new TablePanel();
	}

}
