/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.math.BigDecimal;
import java.util.Date;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.cocowork.client.exception.ExceptionConventionModification;
import org.cocktail.cocowork.client.facade.convention.FacadeEditionConventionFormation;
import org.cocktail.cocowork.client.facade.convention.FacadeEditionGeneralitesConventionFormation;
import org.cocktail.cocowork.client.metier.convention.ModeGestion;
import org.cocktail.cocowork.client.metier.convention.TypeContrat;
import org.cocktail.cocowork.client.metier.convention.TypeReconduction;
import org.cocktail.cocowork.client.proxy.ProxyPrestationConventionFormation;
import org.cocktail.kava.client.factory.FactoryFacturePapier;
import org.cocktail.kava.client.factory.FactoryFacturePapierAdrClient;
import org.cocktail.kava.client.factory.FactoryFacturePapierLigne;
import org.cocktail.kava.client.finder.FinderLolfNomenclatureRecette;
import org.cocktail.kava.client.finder.FinderModeRecouvrement;
import org.cocktail.kava.client.finder.FinderPlanComptable;
import org.cocktail.kava.client.finder.FinderPlancoCreditRec;
import org.cocktail.kava.client.finder.FinderPlancoVisa;
import org.cocktail.kava.client.finder.FinderTypeApplication;
import org.cocktail.kava.client.finder.FinderTypeArticle;
import org.cocktail.kava.client.finder.FinderTypeCreditRec;
import org.cocktail.kava.client.finder.FinderTypeEtat;
import org.cocktail.kava.client.finder.FinderTypePublic;
import org.cocktail.kava.client.metier.EOAdresse;
import org.cocktail.kava.client.metier.EOCatalogueArticle;
import org.cocktail.kava.client.metier.EOConvention;
import org.cocktail.kava.client.metier.EOFacturePapier;
import org.cocktail.kava.client.metier.EOFacturePapierAdrClient;
import org.cocktail.kava.client.metier.EOFacturePapierLigne;
import org.cocktail.kava.client.metier.EOModeRecouvrement;
import org.cocktail.kava.client.metier.EOParametres;
import org.cocktail.kava.client.metier.EOPlanComptable;
import org.cocktail.kava.client.metier.EOTva;
import org.cocktail.kava.client.metier.EOTypeArticle;
import org.cocktail.kava.client.metier.EOTypePublic;
import org.cocktail.kava.client.metier.EOUtilisateur;
import org.cocktail.kava.client.service.FacturePapierService;
import org.cocktail.pieFwk.common.exception.EntityInitializationException;
import org.cocktail.pieFwk.common.metier.FacturePapierLigne;

import app.client.select.CatalogueCatalogueArticleSelect;
import app.client.select.ClientSelect;
import app.client.select.ContactSelect;
import app.client.select.ConventionSelect;
import app.client.select.FournisUlrInterneSelect;
import app.client.select.LolfNomenclatureRecetteSelect;
import app.client.select.OrganSelect;
import app.client.select.PlanComptableCtpSelect;
import app.client.select.PlanComptableTvaSelect;
import app.client.select.PlancoCreditRecSelect;
import app.client.select.RibfourUlrSelect;
import app.client.select.validator.FournisseurEtatValideValidator;
import app.client.select.validator.SelectValidatorSeverity;
import app.client.tools.Constants;
import app.client.ui.DisabledGlassPane;
import app.client.ui.UIUtilities;
import app.client.ui.ZDateField;
import app.client.ui.ZEOComboBox;
import app.client.ui.ZLabel;
import app.client.ui.ZNumberField;
import app.client.ui.ZTextArea;
import app.client.ui.ZTextField;
import app.client.ui.table.ZEOTableModelColumn;
import app.client.ui.table.ZEOTableModelColumn.Modifier;
import app.client.ui.table.ZExtendedTablePanel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOTemporaryGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSTimestamp;

public class FacturePapierAddUpdate extends JDialog {

	private static final String WINDOW_TITLE = "Facture";
	private static final String MSG_MODERECOUVREMENT_VISA =
			"Le compte d'imputation contrepartie a été mis à jour. ";
	private static final String MSG_CLIENT_ATTACHE_A_UN_ECHEANCIER =
			"La facture est réliée à un échéancier SEPA.\nImpossible de modifier le client.\nAu besoin, supprimez l'échéancier.";

	protected ApplicationClient app;
	protected EOEditingContext ec;

	private static FacturePapierAddUpdate sharedInstance;

	private EOFacturePapier eoFacturePapier;

	private FacturePapierBudgetClientAddUpdate facturePapierBudgetClient;

	private Action actionClose, actionValid, actionCancel;
	private Action actionShowRequired, actionUnshowRequired;
	private Action actionFournisUlrSelect, actionClientSelect, actionContactClientSelect,
			actionContactClientDelete, actionRibfourUlrSelect, actionRibfourUlrDelete;
	private Action actionOrganSelect, actionOrganDelete;
	private Action actionLolfNomenclatureRecetteSelect, actionLolfNomenclatureRecetteDelete;
	private Action actionConventionSelect, actionConventionDelete, actionConventionOpen;
	private Action actionPlancoCreditRecSelect, actionPlancoCreditRecDelete;
	private Action actionPlancoTvaSelect, actionPlancoTvaDelete;
	private Action actionPlancoCtpSelect, actionPlancoCtpDelete;
	private Action actionCloseModeRecouvrementTooltip;
	private Action actionCommentaires;

	private ZNumberField tfFapNumero;
	private ZTextField tfExercice;
	private ZEOComboBox cbTypePublic;
	private CbActionListener cbActionListener;

	private ZTextField tfClient, tfContactClient, tfRibClient;
	private ClientSelect clientSelect;
	private ContactSelect contactClientSelect;
	private FournisUlrInterneSelect fournisUlrInterneSelect;
	private RibfourUlrSelect ribfourUlrSelect;

	private ZTextField tfFournisseur;

	private ZLabel tfPrestationReference;

	private ZDateField tfDate, tfDateLimitePaiement;
	private ZTextField tfRef;
	private ZTextField tfFapLib;
	private JCheckBox checkBoxProForma;
	private Action actionCheckProForma;
	private JCheckBox checkBoxLasm;
	private Action actionCheckLasm;

	private FacturePapierLigneTablePanel facturePapierLigneTablePanel;
	private FacturePapierLigneORTablePanel facturePapierLigneORTablePanel;

	private CatalogueCatalogueArticleSelect catalogueArticleSelect;

	private ZTextArea tfCommentairePrest, tfCommentaireClient;

	private JTabbedPane jtpBudget;

	private ZTextField tfOrgan;
	private ZEOComboBox cbTypeCreditRec;
	private ZTextField tfLolfNomenclatureRecette;
	private LolfNomenclatureRecetteSelect lolfNomenclatureRecetteSelect;
	private ZTextField tfConvention;
	private ConventionSelect conventionSelect;
	private ZEOComboBox cbModeRecouvrement;
	private OrganSelect organSelect;
	private ZTextField tfPlancoCreditRec;
	private PlancoCreditRecSelect plancoCreditRecSelect;
	private ZTextField tfPlancoTva;
	private PlanComptableTvaSelect planComptableTvaSelect;
	private ZTextField tfPlancoCtp;
	private PlanComptableCtpSelect planComptableCtpSelect;

	private ZNumberField tfFapRemiseGlobale, tfTotalHt, tfTotalTva, tfTotalTtc;

	private JButton btValid, btCancel;

	private JDialog dialogCommentaires;

	private boolean result;

	private EOAdresse adresseClientSelected;
	private FacturePapierService facturePapierService = FacturePapierService.instance();

	/** Mode de recouvrement tooltip. */
	private ZLabel labelModeRecouvrementTooltip;
	/** Mode de recouvrement tool tip Panel. */
	private JPanel panelModeRecouvrementTooltip;

	/**
	 * Pour fwk Cocowork.
	 */
	private EOEditingContext ecCocowork;
	private FacadeEditionConventionFormation facadeEditionConventionFormation;
	private EOTypePublic _typeFC;

	public FacturePapierAddUpdate() {
		super((JFrame) null, WINDOW_TITLE, true);

		app = (ApplicationClient) EOApplication.sharedApplication();
		ec = app.editingContext();

		_typeFC = FinderTypePublic.typePublicFormationContinue(ec);

		setGlassPane(new BusyGlassPanel());
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new LocalWindowListener());

		facturePapierBudgetClient = new FacturePapierBudgetClientAddUpdate();

		actionClose = new ActionClose();
		actionValid = new ActionValid();
		actionCancel = new ActionCancel();
		actionShowRequired = new ActionShowRequired();
		actionUnshowRequired = new ActionUnshowRequired();
		actionFournisUlrSelect = new ActionFournisUlrSelect();
		actionClientSelect = new ActionClientSelect();
		actionContactClientSelect = new ActionContactClientSelect();
		actionContactClientDelete = new ActionContactClientDelete();
		actionRibfourUlrSelect = new ActionRibfourUlrSelect();
		actionRibfourUlrDelete = new ActionRibfourUlrDelete();
		actionOrganSelect = new ActionOrganSelect();
		actionOrganDelete = new ActionOrganDelete();
		actionLolfNomenclatureRecetteSelect = new ActionLolfNomenclatureRecetteSelect();
		actionLolfNomenclatureRecetteDelete = new ActionLolfNomenclatureRecetteDelete();
		actionConventionSelect = new ActionConventionSelect();
		actionConventionOpen = new ActionConventionOpen();
		actionConventionDelete = new ActionConventionDelete();
		actionPlancoCreditRecSelect = new ActionPlancoCreditRecSelect();
		actionPlancoCreditRecDelete = new ActionPlancoCreditRecDelete();
		actionPlancoTvaSelect = new ActionPlancoTvaSelect();
		actionPlancoTvaDelete = new ActionPlancoTvaDelete();
		actionPlancoCtpSelect = new ActionPlancoCtpSelect();
		actionPlancoCtpDelete = new ActionPlancoCtpDelete();
		actionCloseModeRecouvrementTooltip = new ActionCloseModeRecouvrementTooltip();
		actionCommentaires = new ActionCommentaires();

		tfFapNumero = new ZNumberField(4, app.getNumeroFormatDisplay());
		tfExercice = new ZTextField(4);

		cbTypePublic = new ZEOComboBox(
				FinderTypePublic.find(ec, FinderTypeApplication.typeApplicationPrestationExterne(ec)),
				EOTypePublic.TYPU_LIBELLE_KEY, null, null, null, 200);
		cbActionListener = new CbActionListener();
		cbTypePublic.addActionListener(cbActionListener);

		tfClient = new ZTextField(50);
		tfClient.setViewOnly();
		tfContactClient = new ZTextField(20);
		tfContactClient.setViewOnly();
		tfRibClient = new ZTextField(15);
		tfRibClient.setViewOnly();

		tfFournisseur = new ZTextField(40);
		tfFournisseur.setViewOnly();

		tfPrestationReference = new ZLabel();

		tfDate = new ZDateField(10);
		tfRef = new ZTextField(16);
		tfRef.setViewOnly();
		tfDateLimitePaiement = new ZDateField(10);
		tfFapLib = new ZTextField(60);
		actionCheckProForma = new ActionCheckProForma();
		checkBoxProForma = new JCheckBox(actionCheckProForma);
		actionCheckLasm = new ActionCheckLasm();
		checkBoxLasm = new JCheckBox(actionCheckLasm);

		tfCommentairePrest = new ZTextArea(6, 60);
		tfCommentaireClient = new ZTextArea(6, 60);
		dialogCommentaires = new JDialog(this, "Commentaires", false);

		tfOrgan = new ZTextField(20);
		tfOrgan.setViewOnly();
		cbTypeCreditRec = new ZEOComboBox(new NSArray(), EOTypeCredit.LIB_KEY, null, null, null, 150);
		cbTypeCreditRec.addActionListener(cbActionListener);
		tfLolfNomenclatureRecette = new ZTextField(28);
		tfLolfNomenclatureRecette.setViewOnly();
		tfConvention = new ZTextField(22);
		tfConvention.setViewOnly();
		tfPlancoCreditRec = new ZTextField(40);
		tfPlancoCreditRec.setViewOnly();
		tfPlancoTva = new ZTextField(25);
		tfPlancoTva.setViewOnly();
		tfPlancoCtp = new ZTextField(25);
		tfPlancoCtp.setViewOnly();
		cbModeRecouvrement = new ZEOComboBox(new NSArray(), EOModeRecouvrement.LIB_KEY, "", null, null, 250);
		cbModeRecouvrement.addActionListener(cbActionListener);

		tfFapRemiseGlobale = new ZNumberField(10, Constants.FORMAT_DECIMAL_EDIT);
		tfFapRemiseGlobale.getDocument().addDocumentListener(new RemiseGlobaleDocumentListener());
		tfTotalHt = new ZNumberField(10, app.getCurrencyFormatDisplay());
		tfTotalHt.setViewOnly();
		tfTotalTva = new ZNumberField(10, app.getCurrencyFormatDisplay());
		tfTotalTva.setViewOnly();
		tfTotalTtc = new ZNumberField(10, app.getCurrencyFormatDisplay());
		tfTotalTtc.setViewOnly();

		facturePapierLigneTablePanel = new FacturePapierLigneTablePanel();
		facturePapierLigneORTablePanel = new FacturePapierLigneORTablePanel();

		btCancel = new JButton(actionCancel);
		btValid = new JButton(actionValid);

		ecCocowork = new EOEditingContext();
		facadeEditionConventionFormation = null;

		initGUI();
	}

	public static FacturePapierAddUpdate sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new FacturePapierAddUpdate();
		}
		return sharedInstance;
	}

	public boolean openNew(EOExercice exercice) throws EntityInitializationException {
		if (exercice == null) {
			System.err.println("[FacturePapierAddUpdate:open(EOExercice)] Aucun exercice pass\u00E9 pour ouvrir en cr\u00E9ation !");
			throw new EntityInitializationException("Aucun exercice passé pour ouvrir en création.");
		}
		app.superviseur().setWaitCursor(this, true);
		eoFacturePapier = FactoryFacturePapier.newObject(ec);
		eoFacturePapier.setExerciceRelationship(exercice);
		eoFacturePapier.setUtilisateurRelationship(app.appUserInfo().utilisateur());

		// init du type de public
		onTypePublicUpdate((EOTypePublic) cbTypePublic.getSelectedEOObject());

		setTitle(WINDOW_TITLE + " - Cr\u00E9ation");

		updateData();

		return open();
	}

	public boolean open(EOFacturePapier facturePapier) {
		if (facturePapier == null) {
			System.err.println("[FacturePapierAddUpdate:open(EOFacturePapier)] Aucune facture papier pass\u00E9e pour ouvrir en modif !");
			return false;
		}
		app.superviseur().setWaitCursor(this, true);
		this.eoFacturePapier = facturePapier;

		setTitle(WINDOW_TITLE + " - Modification");

		updateData();

		// charge la facade d'edition de convention a partir de la convention liee a la presta
		if (eoFacturePapier.prestation() != null) {
			try {
				conventionChargerFacade(eoFacturePapier.prestation().convention());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return open();
	}

	public boolean canQuit() {
		if (isShowing()) {
			actionCancel.actionPerformed(null);
		}
		return !isShowing();
	}

	public EOFacturePapier getLastOpened() {
		return eoFacturePapier;
	}

	private boolean open() {
		app.superviseur().setWaitCursor(this, true);
		result = false;
		adresseClientSelected = null;
		int screenWidth = (int) this.getGraphicsConfiguration().getBounds().getWidth();
		int screenHeight = (int) this.getGraphicsConfiguration().getBounds().getHeight();
		this.setLocation((screenWidth / 2) - ((int) this.getSize().getWidth() / 2),
				((screenHeight / 2) - ((int) this.getSize().getHeight() / 2)));

		app.superviseur().setWaitCursor(this, false);
		setVisible(true);
		return result;
	}

	private void onValid() {
		try {
		    
			if (eoFacturePapier.echeId() != null || (!app.isNewObject(ec, eoFacturePapier) && eoFacturePapier.hasEcheancierSepa(ec))) {
				app.showInfoDialog(
						"Un \u00E9ch\u00E9ancier est rattach\u00E9 \u00E0 cette facture, pensez \u00E0 le v\u00E9rifier si vous changez " +
								"des informations de cette facture (surtout les montants)...");
			}
			app.superviseur().setWaitCursor(this, true);
			eoFacturePapier.setFapDate(tfDate.getNSTimestamp());
			eoFacturePapier.setFapDateLimitePaiement(tfDateLimitePaiement.getNSTimestamp());
			eoFacturePapier.setFapLib(tfFapLib.getText());
			eoFacturePapier.setFapCommentairePrest(tfCommentairePrest.getText());
			eoFacturePapier.setFapCommentaireClient(tfCommentaireClient.getText());

			setTypeCreditRec();
			setModeRecouvrement();

			if (eoFacturePapier.organ() == null) {
				eoFacturePapier.setFapRef(null);
			}

			EOFacturePapierAdrClient currentFapAdrCli = eoFacturePapier.currentFactPapierAdresseClient();
			EOAdresse currentAdresse = currentFapAdrCli == null ? null : currentFapAdrCli.toAdresse();
			if (adresseClientSelected != null && !adresseClientSelected.equals(currentAdresse)) {
				EOFacturePapierAdrClient eoNewAdrClient = FactoryFacturePapierAdrClient.newObject(
						ec, app.getCurrentUtilisateur().personne(), eoFacturePapier, adresseClientSelected);
				eoFacturePapier.addToToFacturePapierAdrClientsRelationship(eoNewAdrClient);
				if (currentFapAdrCli != null) {
					// Tips : pour l'instant il est difficile de gerer un historique pour les recettes
					// par souci d'uniformite on applique la meme logique sur les prestations.
					// on a une table historique contenant 1 seule ligne.
					eoFacturePapier.removeFromToFacturePapierAdrClientsRelationship(currentFapAdrCli);
					ec.deleteObject(currentFapAdrCli);
				}
			}

			setNumeroFacturePapier(ec);
			ec.saveChanges();

			// maj convention de formation eventuelle
			conventionMettreAJour();

			result = true;

			if (eoFacturePapier.organ() != null) {
				try {
					FactoryFacturePapier.updFapRef(ec, eoFacturePapier);
					ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(eoFacturePapier)));
				} catch (Exception e) {
					app.superviseur().setWaitCursor(this, false);
					app.showErrorDialog("La facture est enregistr\u00E9e, mais elle n'a pas pu \u00EAtre r\u00E9f\u00E9renc\u00E9e. Erreur : " + e);
				}
			}

			setVisible(false);
		} catch (Exception e) {
			e.printStackTrace();
			app.superviseur().setWaitCursor(this, false);
			app.showErrorDialog(e);
		}
 finally {
			app.superviseur().setWaitCursor(this, false);
		}
	}

	/*
	 * pour resoudre le problème des factures avec plusieur fois le même numéro on separe le set du numéro du saveChanges !!!!
	 */
	private void setNumeroFacturePapier(EOEditingContext ec) {
		System.out.println("setNumeroFacturePapier");
		try {
			if (eoFacturePapier.fapNumero() == null) {
				eoFacturePapier.setFapNumero(Float.valueOf(ServerProxy.getNumerotation(
						ec, eoFacturePapier.exercice(), null, "FACTURE_PAPIER").intValue()));
			}
		} catch (Throwable e) {
			System.err.println("Erreur lors de la mise a jour du numéro de facture !!!");
			e.printStackTrace();
		}
	}

	private void onCancel() {
		if (app.showConfirmationDialog("ATTENTION", "Annuler???", "OUI!", "NON")) {
			ec.revert();
			result = false;
			setVisible(false);
		}
	}

	private void onClose() {
		if (ecCocowork != null && ecCocowork.hasChanges()) {
			ecCocowork.revert();
		}
		actionCancel.actionPerformed(null);
	}

	/**
	 * Charge la facade d'edition a partir de la convention specifiee.
	 *
	 * @throws Exception Impossible de charger la facade
	 */
	protected void conventionChargerFacade(EOConvention convention) throws Exception {
		if (convention != null) {
			getFacadeEditionConventionFormation().ouvrirConvention(convention);
			getFacadeEditionConventionFormation().setUtilisateur(app.appUserInfo().utilisateur());
		}
	}

	/**
	 * Collecte et transmet les infos concernant la convention qui pourra etre creee sur demande dans ConventionSelect.
	 *
	 * @throws Exception
	 */
	protected void conventionChargerFacadeConvention() throws Exception {
		try {
			if (getFacadeEditionConventionFormation().getEtablissementGestionnaire() == null)
				getFacadeEditionConventionFormation().setEtablissementsGestionnairesPossibles();
			getFacadeEditionConventionFormation().setCentreResponsabiliteGestionnaire(
					eoFacturePapier.prestation().catalogue().fournisUlr().personne().structureUlr());
			getFacadeEditionConventionFormation().setTypeContrat(TypeContrat.TYCON_ID_INTERNE_CONVENTION_FORMATION);
			getFacadeEditionConventionFormation().setModeGestion(ModeGestion.LIBELLE_COURT_CS);
			getFacadeEditionConventionFormation().setTypeReconduction(TypeReconduction.TR_ID_INTERNE_EXPRESSE);

			getFacadeEditionConventionFormation().setUtilisateur(app.appUserInfo().utilisateur());

			// partenaires = client prestation + clients factures (peuvent etre differents)
			getFacadeEditionConventionFormation().supprimerPartenaires();// au cas ou une facture a ete supprimee
			getFacadeEditionConventionFormation().modifierPartenaire(
					eoFacturePapier.prestation().personne(),
					Boolean.TRUE,
					null,
					new BigDecimal(0.0), // montant apporte mis a 0 puis augmente ci-apres
					true);
			getFacadeEditionConventionFormation().modifierPartenaire(
					eoFacturePapier.personne(),
					Boolean.TRUE,
					null,
					new BigDecimal(0.0), // montant apporte mis a 0 puis augmente ci-apres
					true);
			NSArray facturesPapiers = (NSArray) eoFacturePapier.prestation().facturePapiers();
			for (int i = 0; i < facturesPapiers.count(); i++) {
				EOFacturePapier facturePapier = (EOFacturePapier) facturesPapiers.objectAtIndex(i);
				System.out.println("[" + new Date() + "] facture papier : " + facturePapier.fapNumero() + " " + facturePapier.personne_persNomPrenom());
				getFacadeEditionConventionFormation().ajouterAuMontantApporteParLePartenaire(
						facturePapier.personne(),
						facturePapier.companion().totalTTCLive(),
						true);
			}

			// dates debut et fin
			NSTimestamp dateDebut = (NSTimestamp) eoFacturePapier.prestation().prestationLignes().valueForKeyPath(
					"@min.catalogueArticle.article.articlePrestation.artpCfcDateDebut");
			NSTimestamp dateFin = (NSTimestamp) eoFacturePapier.prestation().prestationLignes().valueForKeyPath(
					"@max.catalogueArticle.article.articlePrestation.artpCfcDateFin");
			getFacadeEditionConventionFormation().setDateDebut(dateDebut);
			getFacadeEditionConventionFormation().setDateFin(dateFin);
			getFacadeEditionConventionFormation().setDateDebutExec(dateDebut);
			getFacadeEditionConventionFormation().setDateFinExec(dateFin);

			// autres parametres necessaire
			getFacadeEditionConventionFormation().setTauxTva(null);
			getFacadeEditionConventionFormation().setMontantGlobalHT(eoFacturePapier.prestation().companion().totalTTCLive(), true);
			getFacadeEditionConventionFormation().setObjet(eoFacturePapier.prestation().prestLibelle());
			getFacadeEditionConventionFormation().setReferenceExterne(
					FacadeEditionGeneralitesConventionFormation.GetDefaultReferenceExterneAvecPrestation(
							eoFacturePapier.prestation().personne().persNomPrenom()));
			getFacadeEditionConventionFormation().setObservations(
					FacadeEditionGeneralitesConventionFormation.GetDefaultObservationsAvecPrestation(
							eoFacturePapier.prestation().prestNumero()));
			getFacadeEditionConventionFormation().setCreditsLimitatifs(Boolean.FALSE);
			getFacadeEditionConventionFormation().setLucratif(Boolean.FALSE);
			getFacadeEditionConventionFormation().setTvaRecuperable(Boolean.FALSE);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(
					"Impossible de pr\u00E9parer les infos permettant de cr\u00E9er une convention " +
							"\u00E0 partir de cette prestation/facture :\n\n" +
							e.getMessage(),
					e);
		}

		System.out.println("[" + new Date() + "] FacturePapierAddUpdate.takeValuesForConvention() facade edition convention = ");
		System.out.println(getFacadeEditionConventionFormation());
	}

	/**
	 * <p>
	 * Mise a jour de la convention eventuellement associee a la facture papier en cours
	 * <ul>
	 * <li>ajout eventuel d'un partenaire : le client de la facture papier</li>
	 * <li>maj du montant apporte par le partenaire client de la prestation</li>
	 * </ul>
	 *
	 * @throws Exception
	 */
	protected void conventionMettreAJour() throws Exception {

		if (eoFacturePapier != null && _typeFC.equals(eoFacturePapier.typePublic()) && eoFacturePapier.convention() != null) {

			try {
				conventionChargerFacadeConvention();
				ProxyPrestationConventionFormation conventionProxy = new ProxyPrestationConventionFormation(getFacadeEditionConventionFormation(), this, Boolean.TRUE);
				conventionProxy.mettreAJourConvention(eoFacturePapier.convention());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * @throws Exception
	 */
	protected void conventionAnnulerCreation() throws Exception {

		if (eoFacturePapier != null && eoFacturePapier.convention() != null) {

			ProxyPrestationConventionFormation conventionProxy = new ProxyPrestationConventionFormation(
					getFacadeEditionConventionFormation(),
					this,
					Boolean.TRUE);

			try {
				conventionProxy.annulerCreationConvention(eoFacturePapier.convention());
			} catch (ExceptionConventionModification e) {
				e.printStackTrace();
				throw new Exception(
						"Impossible d'annuler la cr\u00E9ation de la convention associ\u00E9e \u00E0 cette prestation :\n\n" +
								e.getMessage(),
						e);
			}
		}
	}

	private void onRemiseGlobaleUpdate() {
		if (eoFacturePapier == null) {
			return;
		}
		eoFacturePapier.setFapRemiseGlobale(tfFapRemiseGlobale.getBigDecimal());
		facturePapierLigneTablePanel.updateData();
		facturePapierLigneORTablePanel.updateData();
		updateTotaux();
	}

	private void onQteUpdate(Object old, Object value, int row) {
		EOFacturePapierLigne flig = (EOFacturePapierLigne) facturePapierLigneTablePanel.displayedObjects().objectAtIndex(row);
		if (flig.prestationLigne() != null) {
			try {
				BigDecimal oldValue = (BigDecimal) old;
				BigDecimal newValue = (BigDecimal) value;
				flig.prestationLigne().setPrligQuantiteReste(flig.prestationLigne().prligQuantiteReste().add(oldValue.subtract(newValue)));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		facturePapierLigneTablePanel.updateData();
		facturePapierLigneORTablePanel.reloadData();
		updateTotaux();
	}

	private void updateData() {
		if (eoFacturePapier == null) {
			return;
		}

		tfFapNumero.setNumber(eoFacturePapier.fapNumero());
		tfExercice.setText(eoFacturePapier.exercice().exeExercice().toString());

		cbTypePublic.removeActionListener(cbActionListener);
		cbTypePublic.setSelectedEOObject(eoFacturePapier.typePublic());
		cbTypePublic.addActionListener(cbActionListener);

		if (eoFacturePapier.personne() != null) {
			StringBuilder txtBuilder = new StringBuilder(eoFacturePapier.personne().persNomPrenom());
			EOAdresse currentAdr = facturePapierService.currentAdresseClient(ec, eoFacturePapier);
			if (currentAdr != null) {
				txtBuilder.append(" ").append(currentAdr.toInlineString());
			}
			tfClient.setText(txtBuilder.toString());
		} else {
			tfClient.setText(null);
		}

		if (eoFacturePapier.individuUlr() != null) {
			tfContactClient.setText(eoFacturePapier.individuUlr().personne().persNomPrenom());
		} else {
			tfContactClient.setText(null);
		}

		if (eoFacturePapier.ribfourUlr() != null) {
			tfRibClient.setText(eoFacturePapier.ribfourUlr().cBanque() + " " + eoFacturePapier.ribfourUlr().cGuichet()
					+ " " + eoFacturePapier.ribfourUlr().noCompte() + " - " + eoFacturePapier.ribfourUlr().ribTitco());
		} else {
			tfRibClient.setText(null);
		}

		if (eoFacturePapier.fournisUlrPrest() != null) {
			tfFournisseur.setText(eoFacturePapier.fournisUlrPrest().personne_persNomPrenom());
		} else {
			tfFournisseur.setText(null);
		}

		if (eoFacturePapier.prestation() != null) {
			tfPrestationReference.setTextHtml(eoFacturePapier.prestation().prestNumero()
					+ " - "
					+ eoFacturePapier.prestation().prestLibelle()
					+ "\n(Catalogue : "
					+ (eoFacturePapier.prestation().catalogue() == null
						? "Sans catalogue"
						: (eoFacturePapier.prestation().catalogue().cataloguePrestation().catNumero()
							+ " - " + eoFacturePapier.prestation().catalogue().catLibelle())) + ")");
		} else {
			tfPrestationReference.setTextHtml("Cette facture n'est pas issue d'une prestation.");
		}

		tfDate.setDate(eoFacturePapier.fapDate());
		tfRef.setText(eoFacturePapier.fapRef());
		tfDateLimitePaiement.setDate(eoFacturePapier.fapDateLimitePaiement());
		checkBoxProForma.setSelected(eoFacturePapier.typeEtat() != null && eoFacturePapier.typeEtat().equals(FinderTypeEtat.typeEtatNon(ec)));

		checkBoxLasm.setSelected("O".equals(eoFacturePapier.fapApplyTva()));
		if (eoFacturePapier.typePublic() != null
				&& eoFacturePapier.typePublic().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(ec))) {
			// TODO activer quand ce sera ok dans Abricot...
			checkBoxLasm.setVisible(false);
		} else {
			checkBoxLasm.setVisible(false);
		}

		tfFapLib.setText(eoFacturePapier.fapLib());

		facturePapierLigneTablePanel.reloadData();
		facturePapierLigneORTablePanel.reloadData();

		tfCommentairePrest.setText(eoFacturePapier.fapCommentairePrest());
		tfCommentaireClient.setText(eoFacturePapier.fapCommentaireClient());
		if (eoFacturePapier.fapCommentairePrest() != null || eoFacturePapier.fapCommentaireClient() != null) {
			actionCommentaires.putValue(AbstractAction.SMALL_ICON, Constants.ICON_NOTE2_16);
		} else {
			actionCommentaires.putValue(AbstractAction.SMALL_ICON, Constants.ICON_NOTE_16);
		}

		updateInfosBudgetairesPrestataire();
		facturePapierBudgetClient.updateData(eoFacturePapier);

		tfFapRemiseGlobale.setNumber(eoFacturePapier.fapRemiseGlobale());
		updateTotaux();

		updateInterfaceEnabling();
	}

	private void updateInfosBudgetairesPrestataire() {
		if (eoFacturePapier.organ() != null) {
			tfOrgan.setText(eoFacturePapier.organ().orgLib());
		} else {
			tfOrgan.setText(null);
		}

		if (eoFacturePapier.typeCreditRec() != null) {
			cbTypeCreditRec.removeActionListener(cbActionListener);
			cbTypeCreditRec.setObjects(FinderTypeCreditRec.find(ec, eoFacturePapier.exercice()));
			cbTypeCreditRec.setSelectedEOObject(eoFacturePapier.typeCreditRec());
			cbTypeCreditRec.addActionListener(cbActionListener);
		} else {
			cbTypeCreditRec.setObjects(FinderTypeCreditRec.find(ec, eoFacturePapier.exercice()));
			cbTypeCreditRec.clean();
		}

		if (eoFacturePapier.lolfNomenclatureRecette() != null) {
			tfLolfNomenclatureRecette.setText(eoFacturePapier.lolfNomenclatureRecette().lolfCode() + " - "
					+ eoFacturePapier.lolfNomenclatureRecette().lolfLibelle());
		} else {
			tfLolfNomenclatureRecette.setText(null);
		}

		if (eoFacturePapier.convention() != null) {
			tfConvention.setText(eoFacturePapier.convention().conReferenceExterne());
		} else {
			tfConvention.setText(null);
		}

		if (eoFacturePapier.planComptable() != null) {
			tfPlancoCreditRec.setText(
					eoFacturePapier.planComptable().pcoNum() + " - " + eoFacturePapier.planComptable().pcoLibelle());
		} else {
			tfPlancoCreditRec.setText(null);
		}

		if (eoFacturePapier.planComptableTva() != null) {
			tfPlancoTva.setText(eoFacturePapier.planComptableTva().pcoNum() + " - "
					+ eoFacturePapier.planComptableTva().pcoLibelle());
		} else {
			tfPlancoTva.setText(null);
		}

		if (eoFacturePapier.planComptableCtp() != null) {
			tfPlancoCtp.setText(eoFacturePapier.planComptableCtp().pcoNum() + " - "
					+ eoFacturePapier.planComptableCtp().pcoLibelle());
		} else {
			tfPlancoCtp.setText(null);
		}

		System.out.println("eoFacturePapier.modeRecouvrement()=" + eoFacturePapier.modeRecouvrement());
		if (eoFacturePapier.modeRecouvrement() != null) {
			cbModeRecouvrement.removeActionListener(cbActionListener);
			if (eoFacturePapier.typePublic() != null
					&& eoFacturePapier.typePublic().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(ec))) {
				cbModeRecouvrement.setObjects(FinderModeRecouvrement.find(ec, eoFacturePapier.exercice()));
			} else {
				cbModeRecouvrement.setObjects(FinderModeRecouvrement.findSansPrestationInterne(ec, eoFacturePapier.exercice()));
			}
			cbModeRecouvrement.setSelectedEOObject(eoFacturePapier.modeRecouvrement());
			cbModeRecouvrement.addActionListener(cbActionListener);
		} else {
			cbModeRecouvrement.removeActionListener(cbActionListener);
			cbModeRecouvrement.clean();
			if (eoFacturePapier.typePublic() != null
					&& eoFacturePapier.typePublic().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(ec))) {
				cbModeRecouvrement.setObjects(FinderModeRecouvrement.find(ec, eoFacturePapier.exercice()));
			} else {
				cbModeRecouvrement.setObjects(FinderModeRecouvrement.findSansPrestationInterne(ec, eoFacturePapier.exercice()));
			}
			cbModeRecouvrement.addActionListener(cbActionListener);
		}
	}

	private void updateTotaux() {
		tfTotalHt.setNumber(eoFacturePapier.companion().totalHTLive());
		tfTotalTva.setNumber(eoFacturePapier.companion().totalTVALive());
		tfTotalTtc.setNumber(eoFacturePapier.companion().totalTTCLive());
	}

	private void updateInterfaceEnabling() {
		if (eoFacturePapier == null) {
			return;
		}

		// FLA : verifier pour ameliorer cet aspect car updateInterfaceEnabling est TRES sollicitee
		EOUtilisateur loggedInUser = app.appUserInfo().utilisateur();
		boolean hasPermissionOrganPrestataire = hasPermissionOrganPrestataire(eoFacturePapier, loggedInUser);
		boolean hasPermissionOrganClient = facturePapierBudgetClient.hasPermissionOrganClient();
		updateInterfacePrestataire(hasPermissionOrganPrestataire);
		updateDefaultBudgetTab();
		updateValidAction(hasPermissionOrganPrestataire || hasPermissionOrganClient);
		updateCommentaireClient(hasPermissionOrganPrestataire || hasPermissionOrganClient);
		updateCommentairePrestataire(hasPermissionOrganPrestataire);
	}

	private boolean hasPermissionOrganPrestataire(EOFacturePapier facturePapier, EOUtilisateur loggedInUser) {
		boolean hasPermissionOrganPrest = true;
		if (facturePapier != null && facturePapier.organ() != null) {
			hasPermissionOrganPrest = facturePapier.organ().hasPermission(loggedInUser);
		}
		return hasPermissionOrganPrest;
	}

	private void updateInterfacePrestataire(boolean hasFullAccess) {
		boolean noPrest = eoFacturePapier.prestation() == null;
		boolean prestInterne = isPrestationInterne(eoFacturePapier);

		cbTypePublic.setEnabled(hasFullAccess && noPrest && eoFacturePapier.personne() == null);
		actionClientSelect.setEnabled(hasFullAccess
				&& eoFacturePapier.typePublic() != null
				&& eoFacturePapier.fapDateValidationClient() == null);
		actionContactClientSelect.setEnabled(hasFullAccess
				&& eoFacturePapier.personne() != null
				&& eoFacturePapier.personne().isPersonneMorale());
		actionContactClientDelete.setEnabled(hasFullAccess && eoFacturePapier.individuUlr() != null);
		actionRibfourUlrSelect.setEnabled(hasFullAccess
				&& eoFacturePapier.personne() != null
				&& eoFacturePapier.fournisUlr() != null
				&& !prestInterne);
		actionRibfourUlrDelete.setEnabled(hasFullAccess
				&& eoFacturePapier.ribfourUlr() != null
				&& eoFacturePapier.fapDateValidationPrest() == null);
		actionFournisUlrSelect.setEnabled(hasFullAccess
				&& noPrest
				&& eoFacturePapier.typePublic() != null
				&& eoFacturePapier.fapDateValidationClient() == null
				&& (eoFacturePapier.facturePapierLignes() == null || eoFacturePapier.facturePapierLignes().count() == 0));

		actionCheckProForma.setEnabled(hasFullAccess && eoFacturePapier.fapDateValidationClient() == null);
		actionCheckLasm.setEnabled(hasFullAccess && noPrest && eoFacturePapier.fapDateValidationClient() == null);

		// champs date
		tfDate.setViewOnly();
		tfDateLimitePaiement.setEditable(hasFullAccess);

		// lib
		tfFapLib.setEditable(hasFullAccess && eoFacturePapier.fapDateValidationPrest() == null);

		if (noPrest) {
			facturePapierLigneTablePanel.setEditable(hasFullAccess
					&& eoFacturePapier.fournisUlrPrest() != null
					&& eoFacturePapier.fapDateValidationClient() == null);
			facturePapierLigneORTablePanel.setEditable(hasFullAccess
					&& eoFacturePapier.fournisUlrPrest() != null
					&& eoFacturePapier.fapDateValidationClient() == null);
		} else {
			facturePapierLigneTablePanel.colQte.setEditable(
					hasFullAccess && eoFacturePapier.fapDateValidationClient() == null);
			facturePapierLigneTablePanel.actionAdd.setEnabled(false);
			facturePapierLigneTablePanel.actionUpdate.setEnabled(false);
			facturePapierLigneTablePanel.actionDelete.setEnabled(
					hasFullAccess && eoFacturePapier.fapDateValidationClient() == null);
			facturePapierLigneORTablePanel.setEditable(false);
		}
		actionOrganSelect.setEnabled(hasFullAccess && eoFacturePapier.fapDateValidationPrest() == null);
		actionOrganDelete.setEnabled(hasFullAccess
				&& eoFacturePapier.organ() != null
				&& eoFacturePapier.fapDateValidationPrest() == null);
		cbTypeCreditRec.setEnabled(hasFullAccess && eoFacturePapier.fapDateValidationPrest() == null);

		actionLolfNomenclatureRecetteSelect.setEnabled(
				hasFullAccess && eoFacturePapier.fapDateValidationPrest() == null);
		actionLolfNomenclatureRecetteDelete.setEnabled(hasFullAccess
				&& eoFacturePapier.lolfNomenclatureRecette() != null
				&& eoFacturePapier.fapDateValidationPrest() == null);

		actionConventionSelect.setEnabled(hasFullAccess && eoFacturePapier.fapDateValidationPrest() == null);
		actionConventionOpen.setEnabled(hasFullAccess && eoFacturePapier.convention() != null);
		actionConventionDelete.setEnabled(hasFullAccess
				&& eoFacturePapier.convention() != null
				&& eoFacturePapier.fapDateValidationPrest() == null);

		actionPlancoCreditRecSelect.setEnabled(hasFullAccess && eoFacturePapier.fapDateValidationPrest() == null);
		actionPlancoCreditRecDelete.setEnabled(hasFullAccess
				&& eoFacturePapier.planComptable() != null
				&& eoFacturePapier.fapDateValidationPrest() == null);

		actionPlancoTvaSelect.setEnabled(hasFullAccess && eoFacturePapier.fapDateValidationPrest() == null);
		actionPlancoTvaDelete.setEnabled(hasFullAccess
				&& eoFacturePapier.planComptableTva() != null && eoFacturePapier.fapDateValidationPrest() == null);

		actionPlancoCtpSelect.setEnabled(hasFullAccess && eoFacturePapier.fapDateValidationPrest() == null);
		actionPlancoCtpDelete.setEnabled(hasFullAccess
				&& eoFacturePapier.planComptableCtp() != null && eoFacturePapier.fapDateValidationPrest() == null);

		cbModeRecouvrement.setEnabled(hasFullAccess
				&& eoFacturePapier.fapDateValidationPrest() == null && !prestInterne);

		facturePapierBudgetClient.updateInterfaceEnabling();

		tfFapRemiseGlobale.setEditable(hasFullAccess && eoFacturePapier.fapDateValidationClient() == null);
	}

	private void updateDefaultBudgetTab() {
		if (isPrestationInterne(eoFacturePapier)) {
			jtpBudget.setEnabledAt(1, true);
		} else {
			jtpBudget.setEnabledAt(1, false);
			jtpBudget.setSelectedIndex(0);
		}
	}

	private void updateValidAction(boolean permission) {
		this.btValid.setEnabled(permission);
	}

	private void updateCommentaireClient(boolean writeAccess) {
		tfCommentaireClient.setEditable(writeAccess);
	}

	private void updateCommentairePrestataire(boolean writeAccess) {
		tfCommentairePrest.setEditable(writeAccess);
	}

	private boolean isPrestationInterne(EOFacturePapier facturePapier) {
		return facturePapier != null
				&& facturePapier.typePublic() != null
				&& facturePapier.typePublic().typeApplication().equals(
						FinderTypeApplication.typeApplicationPrestationInterne(ec));
	}

	private void showRequired(boolean show) {
		cbTypePublic.showRequired(show);
		tfClient.showRequired(show);
		tfFournisseur.showRequired(show);
		tfFapLib.showRequired(show);
		facturePapierLigneTablePanel.showRequired(show);
	}

	private void checkClientSelection() throws Exception {
		if (!(ec.globalIDForObject(eoFacturePapier) instanceof EOTemporaryGlobalID)) {
			if (FactoryFacturePapier.hasEcheancierSepa(ec, eoFacturePapier)) {
				throw new Exception(MSG_CLIENT_ATTACHE_A_UN_ECHEANCIER);
			}
		}
	}

	private void onClientSelect() {
		if (eoFacturePapier.typePublic() == null) {
			app.showInfoDialog("Choisissez le type de public d'abord !");
			return;
		}

		try {
			checkClientSelection();
		} catch (Exception e) {
			app.showErrorDialog(e);
			return;
		}

		adresseClientSelected = null;
		if (eoFacturePapier.typePublic().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(ec))) {
			// on recherche parmi les fournisseurs valides internes uniquement
			if (fournisUlrInterneSelect == null) {
				fournisUlrInterneSelect = new FournisUlrInterneSelect();
			}
			if (fournisUlrInterneSelect.open(app.appUserInfo().utilisateur(), null, null)) {
				eoFacturePapier.setPersonneRelationship(fournisUlrInterneSelect.getSelected().personne());
				eoFacturePapier.setFournisUlrRelationship(fournisUlrInterneSelect.getSelected());
			}
		} else {
			if (clientSelect == null) {
				clientSelect = new FacturePapierAddUpdateClientSelect();
			}
			if (clientSelect.open(app.appUserInfo().utilisateur(), eoFacturePapier.exercice(), null, true, true, true)) {
				eoFacturePapier.setPersonneRelationship(clientSelect.getSelected());
				eoFacturePapier.setFournisUlrRelationship(clientSelect.getSelectedFournisUlr());
				adresseClientSelected = clientSelect.getAdresseSelected();
			}
		}
		if (eoFacturePapier.personne() != null) {
			StringBuilder txtBuilder = new StringBuilder(eoFacturePapier.personne().persNomPrenom());
			if (adresseClientSelected != null) {
				txtBuilder.append(" ").append(adresseClientSelected.toInlineString());
			}
			tfClient.setText(txtBuilder.toString());
		}

		// si personne physique, on vire le contact
		if (eoFacturePapier.personne() != null && !eoFacturePapier.personne().isPersonneMorale()) {
			eoFacturePapier.setIndividuUlrRelationship(null);
			tfContactClient.setText(null);
		}

		updateInterfaceEnabling();
	}

	private void onContactClientSelect() {
		if (contactClientSelect == null) {
			contactClientSelect = new ContactSelect();
		}
		if (contactClientSelect.open(app.appUserInfo().utilisateur(), eoFacturePapier.exercice(), eoFacturePapier.personne().structureUlr())) {
			eoFacturePapier.setIndividuUlrRelationship(contactClientSelect.getSelectedIndividuUlr());
			if (eoFacturePapier.individuUlr() != null) {
				tfContactClient.setText(eoFacturePapier.individuUlr().personne().persNomPrenom());
			}
			updateInterfaceEnabling();
		}
	}

	private void onContactClientDelete() {
		eoFacturePapier.setIndividuUlrRelationship(null);
		tfContactClient.setText(null);
		updateInterfaceEnabling();
	}

	private void onRibfourUlrSelect() {
		if (ribfourUlrSelect == null) {
			ribfourUlrSelect = new RibfourUlrSelect();
		}
		if (ribfourUlrSelect.open(app.appUserInfo().utilisateur(), eoFacturePapier.exercice(), eoFacturePapier.fournisUlr(), eoFacturePapier
				.ribfourUlr())) {
			eoFacturePapier.setRibfourUlrRelationship(ribfourUlrSelect.getSelected());
			if (eoFacturePapier.ribfourUlr() != null) {
				tfRibClient.setText(eoFacturePapier.ribfourUlr().cBanque() + " " + eoFacturePapier.ribfourUlr().cGuichet() + " "
						+ eoFacturePapier.ribfourUlr().noCompte() + " - " + eoFacturePapier.ribfourUlr().ribTitco());
			}
			updateInterfaceEnabling();
		}
	}

	private void onRibfourUlrDelete() {
		eoFacturePapier.setRibfourUlrRelationship(null);
		tfRibClient.setText(null);
		updateInterfaceEnabling();
	}

	private void onFournisUlrSelect() {
		if (eoFacturePapier.facturePapierLignes() != null && eoFacturePapier.facturePapierLignes().count() > 0) {
			app.showInfoDialog("Le panier n'est pas vide, vous ne pouvez pas changer de fournisseur prestataire !");
			return;
		}
		if (eoFacturePapier.typePublic() == null) {
			app.showInfoDialog("Choisissez le type de public d'abord !");
			return;
		}
		if (fournisUlrInterneSelect == null) {
			fournisUlrInterneSelect = new FournisUlrInterneSelect();
		}
		if (fournisUlrInterneSelect.open(app.appUserInfo().utilisateur(), null, null)) {
			eoFacturePapier.setFournisUlrPrestRelationship(fournisUlrInterneSelect.getSelected());
			if (eoFacturePapier.fournisUlrPrest() != null) {
				tfFournisseur.setText(eoFacturePapier.fournisUlrPrest().personne_persNomPrenom());
			}
			updateInterfaceEnabling();
		}
	}

	private void onOrganSelect() {
		if (organSelect == null) {
			organSelect = new OrganSelect();
		}
		if (organSelect.open(app.appUserInfo().utilisateur(), eoFacturePapier.exercice(), null, null)) {
			eoFacturePapier.setOrganRelationship(organSelect.getSelected());
			updateOrgan();
			updateInterfaceEnabling();
		}
	}

	private void updateOrgan() {
		if (eoFacturePapier.organ() != null) {
			tfOrgan.setText(eoFacturePapier.organ().orgLib());
			// si on contr\u00E9le la destination par rapport \u00E9 l'organ et au type cred, on v\u00E9rifie que s'il y en a un de s\u00E9lectionn\u00E9 d\u00E9j\u00E9, il est
			// toujours autoris\u00E9, sinon on le vire
			if (app.getParamBoolean(EOParametres.PARAM_CTRL_ORGAN_DEST, eoFacturePapier.exercice())) {
				if (eoFacturePapier.typeCreditRec() != null && eoFacturePapier.lolfNomenclatureRecette() != null) {
					NSArray a = FinderLolfNomenclatureRecette.find(ec, eoFacturePapier.exercice(), eoFacturePapier.organ(), eoFacturePapier
							.typeCreditRec());
					if (!a.containsObject(eoFacturePapier.lolfNomenclatureRecette())) {
						eoFacturePapier.setLolfNomenclatureRecetteRelationship(null);
						tfLolfNomenclatureRecette.setText(null);
					}
				}
			}
		} else {
			tfOrgan.setText(null);
		}
	}

	private void onOrganDelete() {
		eoFacturePapier.setOrganRelationship(null);
		tfOrgan.setText(null);
		updateInterfaceEnabling();
	}

	private void onLolfNomenclatureRecetteSelect() {
		if (lolfNomenclatureRecetteSelect == null) {
			lolfNomenclatureRecetteSelect = new LolfNomenclatureRecetteSelect();
		}
		if (lolfNomenclatureRecetteSelect.open(app.appUserInfo().utilisateur(), eoFacturePapier.exercice(), eoFacturePapier.organ(),
				eoFacturePapier.typeCreditRec(), eoFacturePapier.lolfNomenclatureRecette())) {
			eoFacturePapier.setLolfNomenclatureRecetteRelationship(lolfNomenclatureRecetteSelect.getSelected());
			if (eoFacturePapier.lolfNomenclatureRecette() != null) {
				tfLolfNomenclatureRecette.setText(eoFacturePapier.lolfNomenclatureRecette().lolfCode() + "."
						+ eoFacturePapier.lolfNomenclatureRecette().lolfLibelle());
			}
			updateInterfaceEnabling();
		}
	}

	private void onLolfNomenclatureRecetteDelete() {
		eoFacturePapier.setLolfNomenclatureRecetteRelationship(null);
		tfLolfNomenclatureRecette.setText(null);
		updateInterfaceEnabling();
	}

	private void onConventionSelect() {
		if (eoFacturePapier.convention() != null) {
			JOptionPane.showMessageDialog(
					this,
					"Vous devez d'abord \"d\u00E9tacher\" la convention existante \n" +
							"avant de pouvoir en s\u00E9lectionner une autre.");
			return;
		}

		if (conventionSelect == null) {
			conventionSelect = new ConventionSelect();
		}

		// si formation continue : possibilite de creer une convention de formation a la volee
		if (_typeFC.equals(eoFacturePapier.typePublic())) {

			if (eoFacturePapier.companion().totalTTCLive() == null) {
				JOptionPane.showMessageDialog(this, "Le total de la facture est inconnu.");
				return;
			}
			if (eoFacturePapier.personne() == null) {
				JOptionPane.showMessageDialog(this, "La facture doit avoir un client.");
				return;
			}

			if (eoFacturePapier.prestation() == null) {
				JOptionPane.showMessageDialog(
						this,
						"Cette facture n'est pas associ\u00E9e \u00E0 une prestation.\n" +
								"Notez que la cr\u00E9ation \"\u00E0 la vol\u00E9e\" d'une convention de formation \n" +
								"correspondante ne sera donc pas possible...");
				conventionSelect.setFacadeEditionConventionFormation(null);
			}
			else {
				try {
					// determination des parametres de creation d'une convention
					conventionChargerFacadeConvention();
					// passe ces proprietes au controller ou l'on peut demander la creation de la convention
					conventionSelect.setFacadeEditionConventionFormation(getFacadeEditionConventionFormation());
				} catch (Exception e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(
							this,
							"Une erreur s'est produite, qui vous emp\u00E9chera de cr\u00E9er une convention \n" +
									"de formation depuis PIE.",
							"Avertissement",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		} else {
			conventionSelect.setFacadeEditionConventionFormation(null);
		}

		if (conventionSelect.openRec(
				app.appUserInfo().utilisateur(),
				eoFacturePapier.exercice(),
				eoFacturePapier.organ(),
				eoFacturePapier.typeCreditRec(),
				eoFacturePapier.convention(),
				eoFacturePapier.typePublic())) {

			eoFacturePapier.setConventionRelationship(conventionSelect.getSelected());
			if (eoFacturePapier.convention() != null) {
				tfConvention.setText(eoFacturePapier.convention().conReferenceExterne());
			}
			updateInterfaceEnabling();
		}
	}

	private void onConventionOpen() {

		// affichage des generalites de la convention associee
		if (eoFacturePapier.convention() != null) {

			conventionAfficher();
		}
		updateInterfaceEnabling();
	}

	/**
	 * Demande d'affichage de la fenetre d'edition de la convention selectionnee.
	 */
	protected void conventionAfficher() {

		ProxyPrestationConventionFormation conventionProxy = new ProxyPrestationConventionFormation(
				getFacadeEditionConventionFormation(),
				this,
				Boolean.TRUE);
		try {
			conventionProxy.afficherConvention(
					eoFacturePapier.convention(),
					app.appUserInfo().utilisateur(),
					eoFacturePapier.personne(),
					eoFacturePapier.prestation(),
					eoFacturePapier.organ(),
					true);
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(
					this,
					"Impossible d'ouvrir la convention : \n\n" + e.getMessage(),
					"Erreur",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	public FacadeEditionConventionFormation getFacadeEditionConventionFormation() {
		if (facadeEditionConventionFormation == null)
			facadeEditionConventionFormation = new FacadeEditionConventionFormation(ecCocowork, Boolean.TRUE);
		return this.facadeEditionConventionFormation;
	}

	public void setFacadeEditionConventionFormation(FacadeEditionConventionFormation conventionFormationEditionFacade) {
		this.facadeEditionConventionFormation = conventionFormationEditionFacade;
	}

	private void onConventionDelete() {
		try {
			// permet d'annuler la creation eventuelle de la convention a la volee
			conventionAnnulerCreation();
		} catch (Exception e) {
			e.printStackTrace();
		}

		eoFacturePapier.setConventionRelationship(null);
		tfConvention.setText(null);
		updateInterfaceEnabling();
	}

	private void onPlancoCreditRecSelect() {
		if (plancoCreditRecSelect == null) {
			plancoCreditRecSelect = new PlancoCreditRecSelect();
		}
		if (plancoCreditRecSelect.open(app.appUserInfo().utilisateur(), eoFacturePapier.exercice(), eoFacturePapier.typeCreditRec(),
				eoFacturePapier.planComptable())) {
			eoFacturePapier.setPlanComptable(plancoCreditRecSelect.getSelected());
			updatePlancos();
			updateInterfaceEnabling();
		}
	}

	private void updatePlancos() {
		if (eoFacturePapier.planComptable() != null) {
			tfPlancoCreditRec.setText(eoFacturePapier.planComptable().pcoNum() + " - " + eoFacturePapier.planComptable().pcoLibelle());
			if (eoFacturePapier.companion().totalTVALive().compareTo(new BigDecimal(0.0)) > 0) {
				eoFacturePapier.setPlanComptableTva(FinderPlancoVisa.findPlancoTva(
						ec, eoFacturePapier.planComptable(), eoFacturePapier.exercice()));
			}
			else {
				eoFacturePapier.setPcoNumTva(null);
			}
			if (eoFacturePapier.planComptableTva() != null) {
				tfPlancoTva.setText(eoFacturePapier.planComptableTva().pcoNum() + " - " + eoFacturePapier.planComptableTva().pcoLibelle());
			}
			else {
				tfPlancoTva.setText(null);
			}
			eoFacturePapier.setPlanComptableCtp(FinderPlancoVisa.findPlancoCtp(
					ec, eoFacturePapier.modeRecouvrement(), eoFacturePapier.planComptable()));
			if (eoFacturePapier.planComptableCtp() != null) {
				tfPlancoCtp.setText(eoFacturePapier.planComptableCtp().pcoNum() + " - "
						+ eoFacturePapier.planComptableCtp().pcoLibelle());
			}
			else {
				tfPlancoCtp.setText(null);
			}
		}
	}

	private void onPlancoCreditRecDelete() {
		eoFacturePapier.setPlanComptable(null);
		tfPlancoCreditRec.setText(null);
		updateInterfaceEnabling();
	}

	private void onPlancoTvaSelect() {
		if (planComptableTvaSelect == null) {
			planComptableTvaSelect = new PlanComptableTvaSelect();
		}
		if (planComptableTvaSelect.open(app.appUserInfo().utilisateur(), eoFacturePapier.exercice(), eoFacturePapier.planComptableTva())) {
			eoFacturePapier.setPlanComptableTva(planComptableTvaSelect.getSelected());
			if (eoFacturePapier.planComptableTva() != null) {
				tfPlancoTva.setText(eoFacturePapier.planComptableTva().pcoNum() + " - "
						+ eoFacturePapier.planComptableTva().pcoLibelle());
			}
			else {
				tfPlancoTva.setText(null);
			}
			updateInterfaceEnabling();
		}
	}

	private void onPlancoTvaDelete() {
		eoFacturePapier.setPlanComptableTva(null);
		tfPlancoTva.setText(null);
		updateInterfaceEnabling();
	}

	private void onPlancoCtpSelect() {
		if (planComptableCtpSelect == null) {
			planComptableCtpSelect = new PlanComptableCtpSelect();
		}
		if (planComptableCtpSelect.open(app.appUserInfo().utilisateur(), eoFacturePapier.exercice(), eoFacturePapier.planComptableCtp())) {
			majCompteContrepartie(planComptableCtpSelect.getSelected());
			majUICompteContrepartie();
		}
	}

	private void majCompteContrepartie(EOPlanComptable compteContrepartie) {
		if (compteContrepartie == null) {
			return;
		}
		eoFacturePapier.setPlanComptableCtp(compteContrepartie);
	}

	private void majUICompteContrepartie() {
		if (eoFacturePapier.planComptableCtp() != null) {
			tfPlancoCtp.setText(eoFacturePapier.planComptableCtp().pcoNum() + " - "
					+ eoFacturePapier.planComptableCtp().pcoLibelle());
		} else {
			tfPlancoCtp.clean();
		}
		updateInterfaceEnabling();
	}

	private void onPlancoCtpDelete() {
		eoFacturePapier.setPlanComptableCtp(null);
		tfPlancoCtp.setText(null);
		updateInterfaceEnabling();
	}

	private void onCommentaires() {
		dialogCommentaires.setLocationRelativeTo(this);
		dialogCommentaires.show();
	}

	private void onTypePublicUpdate(EOTypePublic typePublic) {
		if (eoFacturePapier != null) {
			if (typePublic != null && typePublic.equals(eoFacturePapier.typePublic())) {
				return;
			}
			eoFacturePapier.setTypePublicRelationship(typePublic);
			if (typePublic != null && typePublic.typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(ec))) {
				// mode recouvrement interne par d\u00E9faut
				cbModeRecouvrement.setObjects(FinderModeRecouvrement.find(ec, eoFacturePapier.exercice()));
				EOModeRecouvrement modeRecouvrement = FinderModeRecouvrement.modeRecouvrementPrestationInterne(eoFacturePapier.exercice());
				cbModeRecouvrement.setSelectedEOObject(modeRecouvrement);
				eoFacturePapier.setModeRecouvrementRelationship(modeRecouvrement);

				// autorise la lasm...
				// TODO activer quand ce sera ok dans Abricot...
				checkBoxLasm.setVisible(false);
				// on initialise \u00E9 non par d\u00E9faut
				checkBoxLasm.setSelected(false);
				eoFacturePapier.setFapApplyTva("N");

				updateInterfaceEnabling();

			}
			else {
				cbModeRecouvrement.setObjects(FinderModeRecouvrement.findSansPrestationInterne(ec, eoFacturePapier.exercice()));
				cbModeRecouvrement.setSelectedEOObject(eoFacturePapier.modeRecouvrement());
				// interdit la lasm...
				checkBoxLasm.setVisible(false);
				// on initialise a oui toujours en non interne
				checkBoxLasm.setSelected(true);
				eoFacturePapier.setFapApplyTva("O");
			}
			facturePapierLigneTablePanel.reloadData();
			facturePapierLigneORTablePanel.reloadData();
			updateTotaux();
		}
		updateInterfaceEnabling();
	}

	private class FacturePapierLigneTablePanel extends ZExtendedTablePanel {

		private ZEOTableModelColumn colQte, colTotalTtc;
		private Modifier totalTtcModifier;

		public FacturePapierLigneTablePanel() {
			super(null, true, true, true);

			final ZEOTableModelColumn.ZEONumFieldTableCellEditor _editor =
					new ZEOTableModelColumn.ZEONumFieldTableCellEditor(new JTextField(), Constants.FORMAT_DECIMAL_EDIT);
			final ZEOTableModelColumn.ZEONumFieldTableCellEditor _editorPrix =
					new ZEOTableModelColumn.ZEONumFieldTableCellEditor(new JTextField(), app.getCurrencyFormatEdit());

			addCol(new ZEOTableModelColumn(getDG(), EOFacturePapierLigne.FLIG_REFERENCE_KEY, "Ref", 80));
			addCol(new ZEOTableModelColumn(getDG(), EOFacturePapierLigne.FLIG_DESCRIPTION_KEY, "Description", 150));

			ZEOTableModelColumn col3 = new ZEOTableModelColumn(
					getDG(), EOFacturePapierLigne.FLIG_ART_HT_KEY, "HT", 50, app.getCurrencyPrecisionFormatDisplay());
			col3.setColumnClass(BigDecimal.class);
			col3.setAlignment(SwingConstants.RIGHT);
			addCol(col3);

			ZEOTableModelColumn col4 = new ZEOTableModelColumn(
					getDG(),
					EOFacturePapierLigne.TVA_KEY + "." + EOTva.TVA_TAUX_KEY,
					"TVA", 50, Constants.FORMAT_DECIMAL_DISPLAY);
			col4.setColumnClass(BigDecimal.class);
			col4.setAlignment(SwingConstants.RIGHT);
			addCol(col4);

			ZEOTableModelColumn col5 = new ZEOTableModelColumn(
					getDG(), EOFacturePapierLigne.FLIG_ART_TTC_KEY, "TTC", 50, app.getCurrencyPrecisionFormatDisplay());
			col5.setColumnClass(BigDecimal.class);
			col5.setAlignment(SwingConstants.RIGHT);
			addCol(col5);

			colQte = new ZEOTableModelColumn(getDG(), EOFacturePapierLigne.FLIG_QUANTITE_KEY, "Qt\u00E9", 60,
					Constants.FORMAT_DECIMAL_DISPLAY);
			colQte.setEditable(true);
			colQte.setTableCellEditor(_editor);
			colQte.setFormatEdit(Constants.FORMAT_DECIMAL_EDIT);
			colQte.setColumnClass(BigDecimal.class);
			colQte.setMyModifier(new FacturePapierLigneQteModifier());
			colQte.setAlignment(SwingConstants.RIGHT);
			addCol(colQte);

			colTotalTtc = new ZEOTableModelColumn(getDG(), EOFacturePapierLigne.FLIG_TOTAL_TTC_KEY, "Total TTC", 70, app
					.getCurrencyPrecisionFormatDisplay());
			colTotalTtc.setEditable(true);
			colTotalTtc.setTableCellEditor(_editorPrix);
			colTotalTtc.setFormatEdit(app.getCurrencyPrecisionFormatEdit());
			colTotalTtc.setColumnClass(BigDecimal.class);
			totalTtcModifier = new TotalTtcModifier();
			colTotalTtc.setMyModifier(totalTtcModifier);
			colTotalTtc.setAlignment(SwingConstants.RIGHT);
			addCol(colTotalTtc);

			initGUI();

			setPreferredSize(new Dimension(0, 80));

			// pour n'afficher que les articles ici
			setFilteringQualifier(EOQualifier.qualifierWithQualifierFormat(
					EOFacturePapierLigne.TYPE_ARTICLE_KEY + "=%@",
					new NSArray(FinderTypeArticle.typeArticleArticle(ec))));
		}

		public void reloadData() {
			if (eoFacturePapier != null) {
				setObjectArray(eoFacturePapier.facturePapierLignes());
			} else {
				setObjectArray(null);
			}
			updateInterfaceEnabling();
		}

		protected void onAdd() {
			if (eoFacturePapier == null) {
				return;
			}
			if (eoFacturePapier.fournisUlrPrest() == null) {
				app.showInfoDialog("Choisissez un fournisseur prestataire d'abord !");
				return;
			}
			if (catalogueArticleSelect == null) {
				catalogueArticleSelect = new CatalogueCatalogueArticleSelect();
			}
			if (app.showConfirmationDialog("Ajout de ligne facture",
					"Choisir un article existant dans un catalogue du fournisseur, ou saisir la nouvelle ligne ?", "Article existant",
					"Saisir la ligne")) {
				if (catalogueArticleSelect.open(eoFacturePapier.utilisateur(), eoFacturePapier.fournisUlrPrest(), eoFacturePapier.typePublic())) {
					if (catalogueArticleSelect.getSelecteds() != null && catalogueArticleSelect.getSelecteds().count() > 0) {
						NSArray a = catalogueArticleSelect.getSelecteds();
						for (int i = 0; i < a.count(); i++) {
							EOCatalogueArticle caar = (EOCatalogueArticle) a.objectAtIndex(i);
							EOFacturePapierLigne pl = FactoryFacturePapierLigne.newObject(ec, eoFacturePapier, caar);
							pl.companion().modifierQuantite(BigDecimal.ONE);
							// si pas encore de libelle, on initialise au libelle de l'article ajoute...
							if (tfFapLib.getText() == null || tfFapLib.getText().equals("")) {
								tfFapLib.setText(pl.fligDescription());
							}
						}
						// si pas encore d'infos budgetaires saisies, on regarde si on en a par defaut dans le catalogue d'un article choisi
						EOCatalogueArticle caar = (EOCatalogueArticle) a.objectAtIndex(0);
						if (eoFacturePapier.organ() == null) {
							eoFacturePapier.setOrganRelationship(caar.catalogue().cataloguePrestation().organRecette());
							updateOrgan();
						}
						if (eoFacturePapier.lolfNomenclatureRecette() == null) {
							eoFacturePapier.setLolfNomenclatureRecetteRelationship(caar.catalogue().cataloguePrestation()
									.lolfNomenclatureRecette());
							if (eoFacturePapier.lolfNomenclatureRecette() != null) {
								tfLolfNomenclatureRecette.setText(eoFacturePapier.lolfNomenclatureRecette().lolfCode() + "."
										+ eoFacturePapier.lolfNomenclatureRecette().lolfLibelle());
							}
						}
						if (eoFacturePapier.planComptable() == null) {
							eoFacturePapier.setPlanComptable(FinderPlanComptable.findOnlyValid(
									ec,
									eoFacturePapier.exercice(),
									caar.catalogue().cataloguePrestation().pcoNumRecette()));
							updatePlancos();
						}
						reloadData();
						updateTotaux();
					}
				}
			}
			else {
				if (FacturePapierLigneAddUpdate.sharedInstance().openNew(eoFacturePapier)) {
					reloadData();
					updateTotaux();
				}
			}
		}

		protected void onUpdate() {
			if (FacturePapierLigneAddUpdate.sharedInstance().open((EOFacturePapierLigne) selectedObject())) {
				updateData();
				updateTotaux();
			}
		}

		protected void onDelete() {
			if (selectedObject() != null) {
				EOFacturePapierLigne object = (EOFacturePapierLigne) selectedObject();
				if (object.facturePapierLignes() != null) {
					while (object.facturePapierLignes().count() > 0) {
						EOFacturePapierLigne flig = (EOFacturePapierLigne) object.facturePapierLignes().objectAtIndex(0);
						// traite le cas des lignes de facture issues d'une prestation... remonter le reste de la ligne de prestation...
						if (flig.prestationLigne() != null) {
							flig.prestationLigne().setPrligQuantiteReste(flig.prestationLigne().prligQuantiteReste().add(flig.fligQuantite()));
						}
						object.removeFromFacturePapierLignesRelationship(flig);
						ec.deleteObject(flig);
					}
				}
				// traite le cas des lignes de facture issues d'une prestation... remonter le reste de la ligne de prestation...
				if (object.prestationLigne() != null) {
					object.prestationLigne().setPrligQuantiteReste(object.prestationLigne().prligQuantiteReste().add(object.fligQuantite()));
				}
				eoFacturePapier.removeFromFacturePapierLignesRelationship(object);
				ec.deleteObject(object);
				reloadData();
				updateTotaux();
			}
		}

		protected void onSelectionChanged() {
			facturePapierLigneORTablePanel.reloadData();
		}

		public void setEditable(boolean editable) {
			colQte.setEditable(editable);
			colTotalTtc.setEditable(editable);
			super.setEditable(editable);
		}

		private class FacturePapierLigneQteModifier implements ZEOTableModelColumn.Modifier {
			public void setValueAtRow(Object value, int row) {
				Object oldValue = ((NSKeyValueCoding) (displayedObjects().objectAtIndex(row))).valueForKey(colQte.getAttributeName());
				FacturePapierLigne factureLigne = (FacturePapierLigne) displayedObjects().objectAtIndex(row);
				factureLigne.companion().modifierQuantite((BigDecimal) value);
				onQteUpdate(oldValue, value, row);
			}
		}

		public class TotalTtcModifier implements ZEOTableModelColumn.Modifier {
			public void setValueAtRow(Object value, int row) {
				Object oldQteValue = ((NSKeyValueCoding) (displayedObjects().objectAtIndex(row))).valueForKey(colQte.getAttributeName());
				((NSKeyValueCoding) (displayedObjects().objectAtIndex(row))).takeValueForKey(value, colTotalTtc.getAttributeName());

				BigDecimal fligArtTtc = (BigDecimal) ((NSKeyValueCoding) (displayedObjects().objectAtIndex(row)))
						.valueForKey(EOFacturePapierLigne.FLIG_ART_TTC_KEY);
				BigDecimal fligTotalTtc = (BigDecimal) ((NSKeyValueCoding) (displayedObjects().objectAtIndex(row)))
						.valueForKey(EOFacturePapierLigne.FLIG_TOTAL_TTC_KEY);
				if (fligArtTtc != null && fligArtTtc.floatValue() != 0.0 && fligTotalTtc != null) {
					((NSKeyValueCoding) (displayedObjects().objectAtIndex(row))).takeValueForKey(fligTotalTtc.divide(fligArtTtc, 2,
							BigDecimal.ROUND_HALF_UP), EOFacturePapierLigne.FLIG_QUANTITE_KEY);
				}
				else {
					((NSKeyValueCoding) (displayedObjects().objectAtIndex(row))).takeValueForKey(null, EOFacturePapierLigne.FLIG_QUANTITE_KEY);
				}
				Object newQteValue = ((NSKeyValueCoding) (displayedObjects().objectAtIndex(row)))
						.valueForKey(EOFacturePapierLigne.FLIG_QUANTITE_KEY);

				onQteUpdate(oldQteValue, newQteValue, row);
			}
		}
	}

	private class FacturePapierLigneORTablePanel extends ZExtendedTablePanel {

		public FacturePapierLigneORTablePanel() {
			super("Options / Remises", true, true, true);

			addCol(new ZEOTableModelColumn(getDG(), EOFacturePapierLigne.TYPE_ARTICLE_KEY + "." + EOTypeArticle.TYAR_LIBELLE_KEY, "Type", 80));

			addCol(new ZEOTableModelColumn(getDG(), EOFacturePapierLigne.FLIG_REFERENCE_KEY, "Ref", 80));

			addCol(new ZEOTableModelColumn(getDG(), EOFacturePapierLigne.FLIG_DESCRIPTION_KEY, "Description", 200));

			ZEOTableModelColumn col4 = new ZEOTableModelColumn(getDG(), EOFacturePapierLigne.FLIG_ART_HT_KEY, "HT", 50, app
					.getCurrencyPrecisionFormatDisplay());
			col4.setColumnClass(BigDecimal.class);
			col4.setAlignment(SwingConstants.RIGHT);
			addCol(col4);

			ZEOTableModelColumn col5 = new ZEOTableModelColumn(getDG(), EOFacturePapierLigne.TVA_KEY + "." + EOTva.TVA_TAUX_KEY, "TVA", 50,
					Constants.FORMAT_DECIMAL_DISPLAY);
			col5.setColumnClass(BigDecimal.class);
			col5.setAlignment(SwingConstants.RIGHT);
			addCol(col5);

			ZEOTableModelColumn col6 = new ZEOTableModelColumn(getDG(), EOFacturePapierLigne.FLIG_ART_TTC_KEY, "TTC", 50, app
					.getCurrencyPrecisionFormatDisplay());
			col6.setColumnClass(BigDecimal.class);
			col6.setAlignment(SwingConstants.RIGHT);
			addCol(col6);

			ZEOTableModelColumn col7 = new ZEOTableModelColumn(getDG(), EOFacturePapierLigne.FLIG_QUANTITE_KEY, "Qt\u00E9", 60,
					Constants.FORMAT_DECIMAL_DISPLAY);
			col7.setColumnClass(BigDecimal.class);
			col7.setAlignment(SwingConstants.RIGHT);
			addCol(col7);

			ZEOTableModelColumn col8 = new ZEOTableModelColumn(getDG(), EOFacturePapierLigne.FLIG_TOTAL_TTC_KEY, "Total TTC", 60, app
                    .getCurrencyPrecisionFormatDisplay());
			col8.setColumnClass(BigDecimal.class);
			col8.setAlignment(SwingConstants.RIGHT);
			addCol(col8);

			initGUI();

			setPreferredSize(new Dimension(0, 70));
		}

		public void reloadData() {
			if (facturePapierLigneTablePanel.selectedObject() != null) {
				setEditable(true);
				setObjectArray(((EOFacturePapierLigne) facturePapierLigneTablePanel.selectedObject()).facturePapierLignes());
			}
			else {
				setEditable(false);
				setObjectArray(null);
			}
			updateInterfaceEnabling();
		}

		protected void onAdd() {
			if (facturePapierLigneTablePanel.selectedObject() == null) {
				return;
			}
			if (FacturePapierLigneAddUpdate.sharedInstance().openNew((EOFacturePapierLigne) facturePapierLigneTablePanel.selectedObject())) {
				reloadData();
				updateTotaux();
			}
		}

		protected void onUpdate() {
			if (FacturePapierLigneAddUpdate.sharedInstance().open((EOFacturePapierLigne) selectedObject())) {
				updateData();
				updateTotaux();
			}
		}

		protected void onDelete() {
			if (selectedObject() != null) {
				EOFacturePapierLigne object = (EOFacturePapierLigne) selectedObject();
				if (facturePapierLigneTablePanel.selectedObject() != null) {
					((EOFacturePapierLigne) facturePapierLigneTablePanel.selectedObject()).removeFromFacturePapierLignesRelationship(object);
				}
				ec.deleteObject(object);
				reloadData();
				updateTotaux();
			}
		}

		protected void onSelectionChanged() {
		}
	}

	private void initGUI() {
		JPanel contentPanel = new JPanel(new BorderLayout());
		contentPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		// contentPanel.setPreferredSize(WINDOW_DIMENSION);
		setContentPane(contentPanel);

		// numero
		JPanel panelNumero = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel jlNumero = new JLabel("Facture N\u00B0 ");
		jlNumero.setFont(jlNumero.getFont().deriveFont(Font.BOLD, 20));
		jlNumero.setForeground(Constants.COLOR_LABEL_FGD_NORMAL);
		panelNumero.add(jlNumero);
		tfFapNumero.setFont(tfFapNumero.getFont().deriveFont(Font.BOLD, 24));
		tfFapNumero.setHorizontalAlignment(SwingConstants.RIGHT);
		tfFapNumero.setViewOnly();
		panelNumero.add(tfFapNumero);

		JPanel tmpPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JLabel jlExercice = new JLabel("Exercice ");
		jlExercice.setFont(jlExercice.getFont().deriveFont(Font.BOLD, 16));
		jlExercice.setForeground(Constants.COLOR_LABEL_FGD_NORMAL);
		tmpPanel.add(jlExercice);
		tfExercice.setFont(tfExercice.getFont().deriveFont(Font.BOLD, 18));
		tfExercice.setHorizontalAlignment(SwingConstants.CENTER);
		tfExercice.setViewOnly();
		tmpPanel.add(tfExercice);
		JPanel panelExercice = new JPanel(new GridBagLayout());
		panelExercice.add(tmpPanel, new GridBagConstraints());

		JPanel panelNumeroExercice = new JPanel(new BorderLayout());
		panelNumeroExercice.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		// panelRecNumeroExercice.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.gray));
		panelNumeroExercice.add(panelNumero, BorderLayout.LINE_START);
		panelNumeroExercice.add(new JPanel(), BorderLayout.CENTER);
		panelNumeroExercice.add(panelExercice, BorderLayout.LINE_END);

		// type client
		JPanel panelTypePublic = new JPanel(new FlowLayout(FlowLayout.LEFT));
		panelTypePublic.setBorder(BorderFactory.createTitledBorder(null, "Type de client", TitledBorder.DEFAULT_JUSTIFICATION,
				TitledBorder.DEFAULT_POSITION, null, Constants.COLOR_LABEL_FGD_LIGHT));
		panelTypePublic.add(UIUtilities.labeledComponent(null, cbTypePublic, null));

		// client
		Box panelClient = Box.createVerticalBox();
		panelClient.setBorder(BorderFactory.createTitledBorder(null, "Client", TitledBorder.DEFAULT_JUSTIFICATION,
				TitledBorder.DEFAULT_POSITION, null, Constants.COLOR_LABEL_FGD_LIGHT));
		panelClient.add(UIUtilities.labeledComponent(null, tfClient, actionClientSelect));
		JPanel lignePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 5));
		lignePanel.add(UIUtilities.labeledComponent("Contact client (facultatif)", tfContactClient, new Action[] {
				actionContactClientSelect,
				actionContactClientDelete
		}, UIUtilities.DEFAULT_BOXED, UIUtilities.DEFAULT_ALIGNMENT));
		lignePanel.add(Box.createHorizontalStrut(10));
		lignePanel.add(UIUtilities.labeledComponent("Rib client", tfRibClient, new Action[] {
				actionRibfourUlrSelect, actionRibfourUlrDelete
		},
				UIUtilities.DEFAULT_BOXED, UIUtilities.DEFAULT_ALIGNMENT));
		panelClient.add(lignePanel);

		Box panelTypePublicClient = Box.createHorizontalBox();
		panelTypePublicClient.setBorder(BorderFactory.createEmptyBorder());
		panelTypePublicClient.add(panelTypePublic);
		panelTypePublicClient.add(panelClient);

		// fournisseur + prestation de reference
		// Box panelFournisPrestation = Box.createHorizontalBox();
		JPanel panelFournisPrestation = new JPanel(new BorderLayout());
		panelFournisPrestation.setBorder(BorderFactory.createEmptyBorder());
		panelFournisPrestation.add(UIUtilities.labeledComponent("Fournisseur", tfFournisseur, actionFournisUlrSelect, true), BorderLayout.WEST);
		panelFournisPrestation.add(new JPanel(), BorderLayout.CENTER);
		panelFournisPrestation
				.add(UIUtilities.labeledComponent("Prestation de r\u00E9f\u00E9rence", tfPrestationReference, null, true), BorderLayout.EAST);

		// libell\u00E9
		Box panelLibelle = Box.createVerticalBox();
		panelLibelle.setBorder(BorderFactory.createTitledBorder(null, null, TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION,
				null, Constants.COLOR_LABEL_FGD_LIGHT));
		JPanel panelLigne = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panelLigne.setBorder(BorderFactory.createEmptyBorder(0, 0, 5, 0));
		panelLigne.add(UIUtilities.labeledComponent("Date", tfDate, null));
		panelLigne.add(Box.createHorizontalStrut(20));
		panelLigne.add(UIUtilities.labeledComponent("R\u00E9f", tfRef, null));
		panelLigne.add(Box.createHorizontalStrut(20));
		panelLigne.add(UIUtilities.labeledComponent("Date limite de paiement", tfDateLimitePaiement, null));
		panelLigne.add(Box.createHorizontalStrut(50));
		checkBoxProForma.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);
		checkBoxProForma.setVerticalTextPosition(SwingConstants.TOP);
		checkBoxProForma.setHorizontalTextPosition(SwingConstants.CENTER);
		checkBoxProForma.setIconTextGap(0);
		checkBoxProForma.setFocusPainted(false);
		checkBoxProForma.setBorderPaintedFlat(true);
		panelLigne.add(UIUtilities.labeledComponent(null, checkBoxProForma, null));
		panelLigne.add(Box.createHorizontalStrut(50));
		checkBoxLasm.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);
		checkBoxLasm.setVerticalTextPosition(SwingConstants.TOP);
		checkBoxLasm.setHorizontalTextPosition(SwingConstants.CENTER);
		checkBoxLasm.setIconTextGap(0);
		checkBoxLasm.setFocusPainted(false);
		checkBoxLasm.setBorderPaintedFlat(true);
		panelLigne.add(UIUtilities.labeledComponent(null, checkBoxLasm, null));
		panelLibelle.add(panelLigne);
		panelLibelle.add(UIUtilities.labeledComponent("Libell\u00E9", tfFapLib, null));

		// panier
		Box panelPanier = Box.createVerticalBox();
		panelPanier.setBorder(BorderFactory.createTitledBorder(null, "Lignes de facture", TitledBorder.DEFAULT_JUSTIFICATION,
				TitledBorder.DEFAULT_POSITION, null, Constants.COLOR_LABEL_FGD_LIGHT));
		panelPanier.add(facturePapierLigneTablePanel);
		panelPanier.add(facturePapierLigneORTablePanel);

		// budget prestataire
		JPanel panelOrganTypCredTap = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panelOrganTypCredTap.add(UIUtilities.labeledComponent("Ligne budg\u00E9taire", tfOrgan,
				new Action[] {
						actionOrganSelect, actionOrganDelete
				}, UIUtilities.DEFAULT_BOXED, UIUtilities.DEFAULT_ALIGNMENT));
		panelOrganTypCredTap.add(Box.createHorizontalStrut(10));
		panelOrganTypCredTap.add(UIUtilities.labeledComponent("Type de cr\u00E9dit", cbTypeCreditRec, null));

		JPanel panelLolfConvention = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panelLolfConvention.add(UIUtilities.labeledComponent("Action Lolf", tfLolfNomenclatureRecette, new Action[] {
				actionLolfNomenclatureRecetteSelect, actionLolfNomenclatureRecetteDelete
		}, UIUtilities.DEFAULT_BOXED,
				UIUtilities.DEFAULT_ALIGNMENT));
		panelLolfConvention.add(Box.createHorizontalStrut(10));
		panelLolfConvention.add(UIUtilities.labeledComponent("Convention", tfConvention, new Action[] {
				actionConventionSelect,
				actionConventionOpen, actionConventionDelete
		}, UIUtilities.DEFAULT_BOXED, UIUtilities.DEFAULT_ALIGNMENT));

		JPanel panelPlanco = UIUtilities.labeledComponent("Imputation recette", tfPlancoCreditRec, new Action[] {
				actionPlancoCreditRecSelect,
				actionPlancoCreditRecDelete
		}, UIUtilities.DEFAULT_BOXED, UIUtilities.DEFAULT_ALIGNMENT);

		JPanel panelPlancoTvaCtp = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panelPlancoTvaCtp.add(UIUtilities.labeledComponent("Imputation TVA", tfPlancoTva, new Action[] {
				actionPlancoTvaSelect,
				actionPlancoTvaDelete
		}, UIUtilities.DEFAULT_BOXED, UIUtilities.DEFAULT_ALIGNMENT));
		panelPlancoTvaCtp.add(Box.createHorizontalStrut(10));
		panelPlancoTvaCtp.add(UIUtilities.labeledComponent("Imputation Contrepartie", tfPlancoCtp, new Action[] {
				actionPlancoCtpSelect,
				actionPlancoCtpDelete
		}, UIUtilities.DEFAULT_BOXED, UIUtilities.DEFAULT_ALIGNMENT));

		// UI Mode de recouvrement tooltip.
		labelModeRecouvrementTooltip = new ZLabel(
				MSG_MODERECOUVREMENT_VISA, Constants.ICON_INFO_16, SwingConstants.LEADING);
		labelModeRecouvrementTooltip.setBackground(Color.white);
		labelModeRecouvrementTooltip.setOpaque(true);
		panelModeRecouvrementTooltip = UIUtilities.labeledComponent(null, labelModeRecouvrementTooltip, new Action[] {
				actionCloseModeRecouvrementTooltip
		}, UIUtilities.DEFAULT_BOXED, UIUtilities.DEFAULT_ALIGNMENT);
		panelModeRecouvrementTooltip.setVisible(false);
		JPanel panelModeRecouvrement = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panelModeRecouvrement.add(UIUtilities.labeledComponent("Mode de recouvrement", cbModeRecouvrement, null));
		panelModeRecouvrement.add(Box.createHorizontalStrut(10));
		panelModeRecouvrement.add(panelModeRecouvrementTooltip);

		Box panelBudgetairePrest = Box.createVerticalBox();
		panelBudgetairePrest.setBorder(BorderFactory.createEmptyBorder());
		panelBudgetairePrest.add(panelOrganTypCredTap);
		panelBudgetairePrest.add(panelLolfConvention);
		panelBudgetairePrest.add(panelModeRecouvrement);
		panelBudgetairePrest.add(panelPlanco);
		panelBudgetairePrest.add(panelPlancoTvaCtp);

		jtpBudget = new JTabbedPane();
		jtpBudget.setBorder(BorderFactory.createTitledBorder(null, "Informations budg\u00E9taires", TitledBorder.DEFAULT_JUSTIFICATION,
				TitledBorder.DEFAULT_POSITION, null, Constants.COLOR_LABEL_FGD_LIGHT));
		jtpBudget.add("Prestataire", panelBudgetairePrest);
		jtpBudget.add("Client (interne)", facturePapierBudgetClient.getCenterPanel());
		// jtpBudget.setForegroundAt(0, Constants.COLOR_LABEL_FGD_LIGHT);
		// jtpBudget.setForegroundAt(1, Constants.COLOR_LABEL_FGD_LIGHT);

		// totaux
		JPanel panelRemise = new JPanel(new FlowLayout(FlowLayout.LEFT));
		panelRemise.setBorder(BorderFactory.createTitledBorder(null, "Remise globale", TitledBorder.DEFAULT_JUSTIFICATION,
				TitledBorder.DEFAULT_POSITION, null, Constants.COLOR_LABEL_FGD_LIGHT));
		panelRemise.add(UIUtilities.labeledComponent("%", tfFapRemiseGlobale, null));

		JPanel tempPanel = new JPanel(new GridLayout(3, 2, 5, 5));
		tempPanel.setBorder(BorderFactory.createEmptyBorder());
		JLabel label2 = new JLabel("Total HT");
		label2.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);
		tempPanel.add(label2);
		tempPanel.add(UIUtilities.labeledComponent(null, tfTotalHt, null));
		JLabel label3 = new JLabel("Total TVA");
		label3.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);
		tempPanel.add(label3);
		tempPanel.add(UIUtilities.labeledComponent(null, tfTotalTva, null));
		JLabel label4 = new JLabel("Total TTC");
		label4.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);
		tempPanel.add(label4);
		tempPanel.add(UIUtilities.labeledComponent(null, tfTotalTtc, null));

		JPanel panelTotaux = new JPanel(new BorderLayout());
		panelTotaux.setBorder(BorderFactory.createTitledBorder(null, "Totaux", TitledBorder.DEFAULT_JUSTIFICATION,
				TitledBorder.DEFAULT_POSITION, null, Constants.COLOR_LABEL_FGD_LIGHT));
		panelTotaux.add(new JPanel(), BorderLayout.CENTER);
		panelTotaux.add(tempPanel, BorderLayout.EAST);

		Box panelRemiseTotaux = Box.createVerticalBox();
		panelRemiseTotaux.setBorder(BorderFactory.createEmptyBorder());
		panelRemiseTotaux.add(panelRemise);
		panelRemiseTotaux.add(Box.createVerticalStrut(30));
		panelRemiseTotaux.add(panelTotaux);

		Box panelBudgetTotaux = Box.createHorizontalBox();
		panelBudgetTotaux.setBorder(BorderFactory.createEmptyBorder());
		panelBudgetTotaux.add(jtpBudget);
		panelBudgetTotaux.add(panelRemiseTotaux);

		Box top = Box.createVerticalBox();
		top.setBorder(BorderFactory.createEmptyBorder());
		// top.add(panelNumeroExercice);
		top.add(panelTypePublicClient);
		top.add(panelFournisPrestation);
		top.add(panelLibelle);

		Box bottom = Box.createVerticalBox();
		bottom.setBorder(BorderFactory.createEmptyBorder());
		bottom.add(panelBudgetTotaux);

		JPanel tempCenter = new JPanel(new BorderLayout());
		tempCenter.setBorder(BorderFactory.createEmptyBorder());
		tempCenter.add(top, BorderLayout.NORTH);
		tempCenter.add(panelPanier, BorderLayout.CENTER);
		tempCenter.add(bottom, BorderLayout.SOUTH);

		JPanel center = new JPanel(new BorderLayout());
		center.setBorder(BorderFactory.createEmptyBorder());
		center.add(tempCenter, BorderLayout.CENTER);
		center.add(buildToolBar(), BorderLayout.LINE_END);

		getContentPane().add(panelNumeroExercice, BorderLayout.NORTH);
		getContentPane().add(center, BorderLayout.CENTER);
		getContentPane().add(buildBottomPanel(), BorderLayout.PAGE_END);

		// panneau de commentaires
		Box panelCommentaires = Box.createVerticalBox();
		panelCommentaires.add(UIUtilities.labeledComponent("Commentaire client", new JScrollPane(tfCommentaireClient), null, true));
		panelCommentaires.add(UIUtilities.labeledComponent("Commentaire prestataire (imprim\u00E9 en bas de la facture)", new JScrollPane(
				tfCommentairePrest), null, true));
		dialogCommentaires.setContentPane(panelCommentaires);
		dialogCommentaires.setResizable(false);
		dialogCommentaires.validate();
		dialogCommentaires.pack();

		setGlassPane(new DisabledGlassPane(getContentPane(), btCancel));
		initInputMap();

		this.validate();
		this.pack();
	}

	private final JPanel buildBottomPanel() {
		JPanel p = new JPanel();
		p.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.add(Box.createHorizontalGlue());
		p.add(btCancel);
		p.add(Box.createRigidArea(new Dimension(5, 0)));
		p.add(btValid);
		return p;
	}

	private final JComponent buildToolBar() {
		JToolBar toolBarPanel = new JToolBar("Facture Toolbar", JToolBar.VERTICAL);
		toolBarPanel.setBorder(BorderFactory.createEmptyBorder(15, 4, 4, 4));
		toolBarPanel.add(makeButton(actionCommentaires));
		// toolBarPanel.addSeparator(new Dimension(20, 20));
		return toolBarPanel;
	}

	private JButton makeButton(Action a) {
		Dimension d = new Dimension(24, 24);
		JButton bt = new JButton(a);
		bt.setPreferredSize(d);
		bt.setMaximumSize(d);
		bt.setMinimumSize(d);
		bt.setHorizontalAlignment(SwingConstants.CENTER);
		bt.setBorderPainted(true);
		bt.setFocusPainted(false);
		return bt;
	}

	private void initInputMap() {
		((JComponent) getContentPane()).getActionMap().put("CTRL_Q", app.superviseur().mainMenu.actionQuit);
		((JComponent) getContentPane()).getActionMap().put("CTRL_S", actionValid);
		((JComponent) getContentPane()).getActionMap().put("F10", actionValid);
		((JComponent) getContentPane()).getActionMap().put("ESCAPE", actionCancel);
		((JComponent) getContentPane()).getActionMap().put("F8", actionShowRequired);
		((JComponent) getContentPane()).getActionMap().put("ALT_F8", actionUnshowRequired);
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
				KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK), "CTRL_Q");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
				KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK), "CTRL_S");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F10, 0), "F10");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"ESCAPE");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("F8"), "F8");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("released F8"), "ALT_F8");
	}

	private class ActionValid extends AbstractAction {
		public ActionValid() {
			super("Enregistrer");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onValid();
			}
		}
	}

	private class ActionCancel extends AbstractAction {
		public ActionCancel() {
			super("Annuler");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CANCEL_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onCancel();
			}
		}
	}

	private class ActionClose extends AbstractAction {
		public ActionClose() {
			super("Fermer");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CLOSE_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onClose();
			}
		}
	}

	private class ActionShowRequired extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				showRequired(true);
			}
		}
	}

	private class ActionUnshowRequired extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				showRequired(false);
			}
		}
	}

	private class ActionFournisUlrSelect extends AbstractAction {
		public ActionFournisUlrSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onFournisUlrSelect();
			}
		}
	}

	private class ActionClientSelect extends AbstractAction {
		public ActionClientSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onClientSelect();
			}
		}
	}

	private class ActionContactClientSelect extends AbstractAction {
		public ActionContactClientSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onContactClientSelect();
			}
		}
	}

	private class ActionContactClientDelete extends AbstractAction {
		public ActionContactClientDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onContactClientDelete();
			}
		}
	}

	private class ActionRibfourUlrSelect extends AbstractAction {
		public ActionRibfourUlrSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onRibfourUlrSelect();
			}
		}
	}

	private class ActionRibfourUlrDelete extends AbstractAction {
		public ActionRibfourUlrDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onRibfourUlrDelete();
			}
		}
	}

	private class ActionOrganSelect extends AbstractAction {
		public ActionOrganSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onOrganSelect();
			}
		}
	}

	private class ActionOrganDelete extends AbstractAction {
		public ActionOrganDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onOrganDelete();
			}
		}
	}

	private class ActionLolfNomenclatureRecetteSelect extends AbstractAction {
		public ActionLolfNomenclatureRecetteSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onLolfNomenclatureRecetteSelect();
			}
		}
	}

	private class ActionLolfNomenclatureRecetteDelete extends AbstractAction {
		public ActionLolfNomenclatureRecetteDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onLolfNomenclatureRecetteDelete();
			}
		}
	}

	private class ActionConventionSelect extends AbstractAction {
		public ActionConventionSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onConventionSelect();
			}
		}
	}

	private class ActionConventionOpen extends AbstractAction {
		public ActionConventionOpen() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_LOUPE_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onConventionOpen();
			}
		}
	}

	private class ActionConventionDelete extends AbstractAction {
		public ActionConventionDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onConventionDelete();
			}
		}
	}

	private class ActionPlancoCreditRecSelect extends AbstractAction {
		public ActionPlancoCreditRecSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onPlancoCreditRecSelect();
			}
		}
	}

	private class ActionPlancoCreditRecDelete extends AbstractAction {
		public ActionPlancoCreditRecDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onPlancoCreditRecDelete();
			}
		}
	}

	private class ActionPlancoTvaSelect extends AbstractAction {
		public ActionPlancoTvaSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onPlancoTvaSelect();
			}
		}
	}

	private class ActionPlancoTvaDelete extends AbstractAction {
		public ActionPlancoTvaDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onPlancoTvaDelete();
			}
		}
	}

	private class ActionPlancoCtpSelect extends AbstractAction {
		public ActionPlancoCtpSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onPlancoCtpSelect();
			}
		}
	}

	private class ActionPlancoCtpDelete extends AbstractAction {
		public ActionPlancoCtpDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onPlancoCtpDelete();
			}
		}
	}

	private class ActionCloseModeRecouvrementTooltip extends AbstractAction {
		public ActionCloseModeRecouvrementTooltip() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				panelModeRecouvrementTooltip.setVisible(false);
			}
		}
	}

	private class ActionCommentaires extends AbstractAction {
		public ActionCommentaires() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_NOTE_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Commentaires client et prestataire...");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onCommentaires();
			}
		}
	}

	private class ActionCheckProForma extends AbstractAction {
		public ActionCheckProForma() {
			super("Pro Forma");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Typer la facture PRO FORMA");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				if (checkBoxProForma.isSelected()) {
					eoFacturePapier.setTypeEtatRelationship(FinderTypeEtat.typeEtatNon(ec));
					tfDate.setViewOnly();
				} else {
					eoFacturePapier.setTypeEtatRelationship(FinderTypeEtat.typeEtatValide(ec));
					tfDate.setDate(new NSTimestamp());
				}
			}
		}
	}

	private class ActionCheckLasm extends AbstractAction {
		public ActionCheckLasm() {
			super("Lasm");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Prestation interne dans le cadre de la LASM (Livraison A Soi-M\u00EAme) = inclure la TVA");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				if (checkBoxLasm.isSelected()) {
					eoFacturePapier.setFapApplyTva("O");
				}
				else {
					eoFacturePapier.setFapApplyTva("N");
				}
				facturePapierLigneTablePanel.reloadData();
				facturePapierLigneORTablePanel.reloadData();
				updateTotaux();
			}
		}
	}

	private void setTypeCreditRec() {

		if (eoFacturePapier != null) {
			eoFacturePapier.setTypeCreditRecRelationship((EOTypeCredit) cbTypeCreditRec.getSelectedEOObject());
			// si on a un type de credit et qu'on avait deja un plan comptable,
			// on verifie que ce plan comptable est toujours autorise pour ce nouveau type de credit, sinon on le vire.
			if (eoFacturePapier.typeCreditRec() != null && eoFacturePapier.planComptable() != null) {
				// UP 02/09/009 : tenir compte de l'exercice
				NSArray a = FinderPlancoCreditRec.find(ec, eoFacturePapier.typeCreditRec(), eoFacturePapier.exercice());
				if (!a.containsObject(eoFacturePapier.planComptable())) {
					eoFacturePapier.setPcoNum(null);
					tfPlancoCreditRec.setText(null);
				}
			}
			// si on controle la destination par rapport a l'organ et au type cred,
			// on verifie que s'il y en a un de selectionne deja, il est toujours autorisé, sinon on le vire.
			if (app.getParamBoolean(EOParametres.PARAM_CTRL_ORGAN_DEST, eoFacturePapier.exercice())) {
				if (eoFacturePapier.organ() != null && eoFacturePapier.typeCreditRec() != null
						&& eoFacturePapier.lolfNomenclatureRecette() != null) {
					NSArray a = FinderLolfNomenclatureRecette.find(ec, eoFacturePapier.exercice(), eoFacturePapier.organ(), eoFacturePapier
							.typeCreditRec());
					if (!a.containsObject(eoFacturePapier.lolfNomenclatureRecette())) {
						eoFacturePapier.setLolfNomenclatureRecetteRelationship(null);
						tfLolfNomenclatureRecette.setText(null);
					}
				}
			}
		}
	}

	private void setModeRecouvrement() {
		if (eoFacturePapier != null) {
			eoFacturePapier.setModeRecouvrementRelationship(
					(EOModeRecouvrement) cbModeRecouvrement.getSelectedEOObject());
		}
	}

	private void setLabelModeRecouvrementTooltip(String text) {
		this.labelModeRecouvrementTooltip.setTextHtml(text);
	}

	private class CbActionListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			JComboBox cb = (JComboBox) e.getSource();
			if (cb == cbTypePublic) {
				onTypePublicUpdate((EOTypePublic) cbTypePublic.getSelectedEOObject());
			}
			if (cb == cbTypeCreditRec) {
				setTypeCreditRec();
			}
			if (cb == cbModeRecouvrement) {
				setModeRecouvrement();
				majCompteContrepartie();
			}
		}

		private void majCompteContrepartie() {
			if (eoFacturePapier != null
					&& hasModeRecouvrementVisa(eoFacturePapier.modeRecouvrement())
					&& !isSameCompteContrepartie(
							eoFacturePapier.modeRecouvrement().planComptableVisa(),
							eoFacturePapier.planComptableCtp())) {
				FacturePapierAddUpdate.this.majCompteContrepartie(
						eoFacturePapier.modeRecouvrement().planComptableVisa());
				FacturePapierAddUpdate.this.majUICompteContrepartie();
				FacturePapierAddUpdate.this.panelModeRecouvrementTooltip.setVisible(true);
			}
		}

		private boolean hasModeRecouvrementVisa(EOModeRecouvrement modeRecouvrement) {
			return modeRecouvrement != null && modeRecouvrement.planComptableVisa() != null;
		}

		private boolean isSameCompteContrepartie(
				EOPlanComptable compteCtpModeRecouvrement, EOPlanComptable compteCtpFacturePapier) {
			return compteCtpModeRecouvrement.equals(compteCtpFacturePapier);
		}
	}

	private class RemiseGlobaleDocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent arg0) {
			onRemiseGlobaleUpdate();
		}

		public void insertUpdate(DocumentEvent arg0) {
			onRemiseGlobaleUpdate();
		}

		public void removeUpdate(DocumentEvent arg0) {
			onRemiseGlobaleUpdate();
		}
	}

	private class LocalWindowListener implements WindowListener {
		public void windowActivated(WindowEvent arg0) {
		}

		public void windowClosed(WindowEvent arg0) {
		}

		public void windowClosing(WindowEvent arg0) {
			actionClose.actionPerformed(null);
		}

		public void windowDeactivated(WindowEvent arg0) {
		}

		public void windowDeiconified(WindowEvent arg0) {
		}

		public void windowIconified(WindowEvent arg0) {
		}

		public void windowOpened(WindowEvent arg0) {
		}
	}

	public void setWaitCursor(boolean bool) {
		if (bool) {
			this.getGlassPane().setVisible(true);
		} else {
			this.getGlassPane().setVisible(false);
		}
	}

	/**
	 * Permet de d\u00E9finir un panel qui sert a afficher un waitcursor et a interdire toute modif sur le panel.
	 *
	 * @author rprin
	 */
	public final class BusyGlassPanel extends JPanel {
		public final Color COLOR_WASH = new Color(64, 64, 64, 32);

		public BusyGlassPanel() {
			super.setOpaque(false);
			super.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

			// Suck up them events!!!
			super.addKeyListener((new KeyAdapter() {
			}));
			super.addMouseListener((new MouseAdapter() {
			}));
			super.addMouseMotionListener((new MouseMotionAdapter() {
			}));
		}

		public final void paintComponent(Graphics p_graphics) {
			Dimension l_size = super.getSize();

			// Wash the pane with translucent gray.
			p_graphics.setColor(COLOR_WASH);
			p_graphics.fillRect(0, 0, l_size.width, l_size.height);

			// Paint a grid of white/black dots.
			p_graphics.setColor(Color.white);
			for (int j = 3; j < l_size.height; j += 8) {
				for (int i = 3; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
			p_graphics.setColor(Color.black);
			for (int j = 4; j < l_size.height; j += 8) {
				for (int i = 4; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
		}
	}

	/**
	 * Client select implementation.
	 *
	 * @author flagouey
	 */
	private final class FacturePapierAddUpdateClientSelect extends ClientSelect {
		/** Serial Version ID. */
		private static final long serialVersionUID = 1L;

		@Override
		protected void registerValidators() {
			super.registerValidators();
			addPersonneValidators(new FournisseurEtatValideValidator(
					SelectValidatorSeverity.WARNING, FournisseurEtatValideValidator.WARN_FOURNISSEUR_ETAT_INVALIDE));
		}
	}
}
