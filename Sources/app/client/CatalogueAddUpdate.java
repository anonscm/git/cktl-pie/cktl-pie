/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.math.BigDecimal;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import org.cocktail.kava.client.factory.FactoryCatalogue;
import org.cocktail.kava.client.factory.FactoryCataloguePublic;
import org.cocktail.kava.client.factory.FactoryCatalogueResponsable;
import org.cocktail.kava.client.metier.EOCatalogue;
import org.cocktail.kava.client.metier.EOCataloguePublic;
import org.cocktail.kava.client.metier.EOCatalogueResponsable;
import org.cocktail.kava.client.metier.EOFournisUlr;
import org.cocktail.kava.client.metier.EOTypePublic;
import org.cocktail.kava.client.metier.EOUtilisateur;

import app.client.select.FournisUlrInterneSelect;
import app.client.select.LolfNomenclatureRecetteSelect;
import app.client.select.OrganSelect;
import app.client.select.PlancoCreditDepSelect;
import app.client.select.PlancoCreditRecSelect;
import app.client.select.TypePublicSelect;
import app.client.select.UtilisateurSelect;
import app.client.tools.Constants;
import app.client.ui.UIUtilities;
import app.client.ui.ZDateField;
import app.client.ui.ZNumberField;
import app.client.ui.ZTextArea;
import app.client.ui.ZTextField;
import app.client.ui.table.ZEOTableModelColumn;
import app.client.ui.table.ZExtendedTablePanel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class CatalogueAddUpdate extends JDialog{

	// private static final Dimension WINDOW_DIMENSION = new Dimension(620, 640);
	private static final String				WINDOW_TITLE	= "Catalogue";

	protected ApplicationClient				app;
	protected EOEditingContext				ec;

	private static CatalogueAddUpdate		sharedInstance;

	private EOCatalogue						eoCatalogue;

	private Action							actionClose, actionValid, actionCancel;
	private Action							actionShowRequired, actionUnshowRequired;
	private Action							actionFournisUlrSelect;
	private Action							actionPlancoCreditRecSelect, actionPlancoCreditRecDelete;
	private Action							actionPlancoCreditDepSelect, actionPlancoCreditDepDelete;
	private Action							actionOrganSelect, actionOrganDelete;
	private Action							actionLolfNomenclatureRecetteSelect, actionLolfNomenclatureRecetteDelete;
	private Action							actionCheckCatPublieWeb;

	private ZNumberField					tfCatNumero;
	private ZDateField						tfCatDateVote;
	private ZDateField						tfCatDateDebut, tfCatDateFin;
	private ZTextField						tfCatLibelle;
	private ZTextField						tfFournisUlr;
	private JCheckBox						checkBoxCatPublieWeb;
	private ZTextArea						tfCatCommentaire;
	private ZTextField						tfPlancoCreditRec, tfPlancoCreditDep, tfOrgan, tfLolfNomenclatureRecette;

	private CatalogueResponsableTablePanel	catalogueResponsable;
	private CataloguePublicTablePanel		cataloguePublic;

	private JButton							btValid, btCancel;

	private FournisUlrInterneSelect			fournisUlrInterneSelect;
	private UtilisateurSelect				utilisateurSelect;
	private TypePublicSelect				typePublicSelect;
	private PlancoCreditRecSelect			plancoCreditRecSelect;
	private PlancoCreditDepSelect			plancoCreditDepSelect;
	private OrganSelect						organSelect;
	private LolfNomenclatureRecetteSelect	lolfNomenclatureRecetteSelect;

	private boolean							result;

	public CatalogueAddUpdate() {
		super((JFrame) null, WINDOW_TITLE, true);
		app = (ApplicationClient) EOApplication.sharedApplication();
		ec = app.editingContext();
		setGlassPane(new BusyGlassPanel());
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new LocalWindowListener());

		actionClose = new ActionClose();
		actionValid = new ActionValid();
		actionCancel = new ActionCancel();
		actionShowRequired = new ActionShowRequired();
		actionUnshowRequired = new ActionUnshowRequired();
		actionFournisUlrSelect = new ActionFournisUlrSelect();
		actionPlancoCreditRecSelect = new ActionPlancoCreditRecSelect();
		actionPlancoCreditRecDelete = new ActionPlancoCreditRecDelete();
		actionPlancoCreditDepSelect = new ActionPlancoCreditDepSelect();
		actionPlancoCreditDepDelete = new ActionPlancoCreditDepDelete();
		actionCheckCatPublieWeb = new ActionCheckCatPublieWeb();
		actionOrganSelect = new ActionOrganSelect();
		actionOrganDelete = new ActionOrganDelete();
		actionLolfNomenclatureRecetteSelect = new ActionLolfNomenclatureRecetteSelect();
		actionLolfNomenclatureRecetteDelete = new ActionLolfNomenclatureRecetteDelete();

		tfCatNumero = new ZNumberField(4);
		tfCatDateVote = new ZDateField(10);
		tfCatDateDebut = new ZDateField(10);
		tfCatDateFin = new ZDateField(10);
		tfCatLibelle = new ZTextField(60);

		tfFournisUlr = new ZTextField(50);
		tfFournisUlr.setViewOnly();

		checkBoxCatPublieWeb = new JCheckBox(actionCheckCatPublieWeb);

		tfCatCommentaire = new ZTextArea(4, 60);
		tfPlancoCreditRec = new ZTextField(40);
		tfPlancoCreditRec.setViewOnly();
		tfPlancoCreditDep = new ZTextField(40);
		tfPlancoCreditDep.setViewOnly();
		tfOrgan = new ZTextField(20);
		tfOrgan.setViewOnly();
		tfLolfNomenclatureRecette = new ZTextField(30);
		tfLolfNomenclatureRecette.setViewOnly();

		catalogueResponsable = new CatalogueResponsableTablePanel();
		cataloguePublic = new CataloguePublicTablePanel();

		btCancel = new JButton(actionCancel);
		btValid = new JButton(actionValid);

		initGUI();

		setEditable(true, false);
		checkBoxCatPublieWeb.getAction().actionPerformed(null);
	}

	public static CatalogueAddUpdate sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new CatalogueAddUpdate();
		}
		return sharedInstance;
	}

	public boolean openNew(EOUtilisateur utilisateur, EOFournisUlr fournisUlr) {
		if (utilisateur == null) {
			System.err.println("[CatalogueAddUpdate:open(EOUtilisateur)] Aucun utilisateur pass\u00E9 pour ouvrir en cr\u00E9ation !");
			return false;
		}
		app.superviseur().setWaitCursor(this, true);
		setTitle(WINDOW_TITLE + " - Cr\u00E9ation");
		setEditable(true, false);

		eoCatalogue = FactoryCatalogue.newObject(ec);
		FactoryCatalogueResponsable.newObject(ec, eoCatalogue, utilisateur);
		if (fournisUlr != null) {
			System.out.println("Affectation fournisUlr");
			eoCatalogue.setFournisUlrRelationship(fournisUlr);
		}
		updateData();

		return open();
	}

	public boolean open(EOCatalogue catalogue) {
		if (catalogue == null) {
			System.err.println("[CatalogueAddUpdate:open(EOCatalogue)] Aucun catalogue pass\u00E9 pour ouvrir en modif !");
			return false;
		}
		app.superviseur().setWaitCursor(this, true);
		setTitle(WINDOW_TITLE + " - Modification");
		setEditable(true, true);

		this.eoCatalogue = catalogue;
		updateData();

		return open();
	}

	public boolean canQuit() {
		if (isShowing()) {
			actionCancel.actionPerformed(null);
		}
		return !isShowing();
	}

	public EOCatalogue getLastOpened() {
		return eoCatalogue;
	}

	private boolean open() {
		result = false;
		int screenWidth = (int) this.getGraphicsConfiguration().getBounds().getWidth();
		int screenHeight = (int) this.getGraphicsConfiguration().getBounds().getHeight();
		this.setLocation((screenWidth / 2) - ((int) this.getSize().getWidth() / 2),
				((screenHeight / 2) - ((int) this.getSize().getHeight() / 2)));

		app.superviseur().setWaitCursor(this, false);
		setVisible(true);

		return result;
	}

	private void onValid() {
		try {
			eoCatalogue.setCatLibelle(tfCatLibelle.getText());
			eoCatalogue.cataloguePrestation().setCatDateVote(tfCatDateVote.getNSTimestamp());
			eoCatalogue.setCatDateDebut(tfCatDateDebut.getNSTimestamp());
			eoCatalogue.setCatDateFin(tfCatDateFin.getNSTimestamp());
			eoCatalogue.setCatCommentaire(tfCatCommentaire.getText());
			eoCatalogue.cataloguePrestation().setCatPublieWeb(checkBoxCatPublieWeb.isSelected() ? "O" : "N");

			ec.saveChanges();
			result = true;
			setVisible(false);
		}
		catch (Exception e) {
			e.printStackTrace();
			app.showErrorDialog(e);
		}
	}

	private void onCancel() {
		if (app.showConfirmationDialog("Attention", "Voulez vous vraiment annuler ?", "Oui", "Non")) {
			System.out.println("CatalogueAddUpdate:onCancel");

			ec.revert();


			eoCatalogue = null;
			result = false;
			app.retirerLesGlassPane();
			setVisible(false);
		}
	}

	private void onClose() {
		System.out.println("CatalogueAddUpdate:onClose");
		actionCancel.actionPerformed(null);
	}

	private void updateData() {
		cleanData();
		if (eoCatalogue == null) {
			return;
		}
		tfCatNumero.setNumber(eoCatalogue.cataloguePrestation().catNumero());
		tfCatLibelle.setText(eoCatalogue.catLibelle());
		tfCatDateVote.setDate(eoCatalogue.cataloguePrestation().catDateVote());
		tfCatDateDebut.setDate(eoCatalogue.catDateDebut());
		tfCatDateFin.setDate(eoCatalogue.catDateFin());
		if (eoCatalogue.fournisUlr() != null) {
			tfFournisUlr.setText(eoCatalogue.fournisUlr().personne().persNomPrenom());
		} else {
			tfFournisUlr.setText(null);
		}

		checkBoxCatPublieWeb.setSelected("O".equalsIgnoreCase(eoCatalogue.cataloguePrestation().catPublieWeb()));
		tfCatCommentaire.setText(eoCatalogue.catCommentaire());

		catalogueResponsable.reloadData();
		cataloguePublic.reloadData();

		if (eoCatalogue.cataloguePrestation() != null && eoCatalogue.cataloguePrestation().pcoNumRecette() != null) {
			tfPlancoCreditRec.setText(eoCatalogue.cataloguePrestation().pcoNumRecette());
		} else {
			tfPlancoCreditRec.setText(null);
		}

		if (eoCatalogue.cataloguePrestation() != null && eoCatalogue.cataloguePrestation().pcoNumDepense() != null) {
			tfPlancoCreditDep.setText(eoCatalogue.cataloguePrestation().pcoNumDepense());
		} else {
			tfPlancoCreditDep.setText(null);
		}

		if (eoCatalogue.cataloguePrestation() != null && eoCatalogue.cataloguePrestation().organRecette() != null) {
			tfOrgan.setText(eoCatalogue.cataloguePrestation().organRecette().orgLib());
		} else {
			tfOrgan.setText(null);
		}

		if (eoCatalogue.cataloguePrestation() != null
				&& eoCatalogue.cataloguePrestation().lolfNomenclatureRecette() != null) {
			tfLolfNomenclatureRecette.setText(
					eoCatalogue.cataloguePrestation().lolfNomenclatureRecette().lolfCode() + " - "
					+ eoCatalogue.cataloguePrestation().lolfNomenclatureRecette().lolfLibelle());
		} else {
			tfLolfNomenclatureRecette.setText(null);
		}
		updateInterfaceEnabling();
	}

	private void updateInterfaceEnabling() {
		if (eoCatalogue == null || eoCatalogue.cataloguePrestation() == null) {
			return;
		}

		actionPlancoCreditRecDelete.setEnabled(eoCatalogue.cataloguePrestation().pcoNumRecette() != null);
		actionPlancoCreditDepDelete.setEnabled(eoCatalogue.cataloguePrestation().pcoNumDepense() != null);
		actionOrganDelete.setEnabled(eoCatalogue.cataloguePrestation().organRecette() != null);
		actionLolfNomenclatureRecetteDelete.setEnabled(
				eoCatalogue.cataloguePrestation().lolfNomenclatureRecette() != null);
	}

	private void setEditable(boolean editable, boolean forUpdate) {
		tfCatLibelle.setEditable(editable);
		tfCatDateVote.setEditable(editable);
		tfCatDateDebut.setEditable(editable);
		tfCatDateFin.setEditable(editable);
		checkBoxCatPublieWeb.setEnabled(editable);
		tfCatCommentaire.setEditable(editable);

		actionCancel.setEnabled(editable);
		actionValid.setEnabled(editable);
		actionClose.setEnabled(!editable);

		catalogueResponsable.setEditable(editable);
		cataloguePublic.setEditable(editable);

		actionFournisUlrSelect.setEnabled(editable && !forUpdate);
		actionPlancoCreditRecSelect.setEnabled(editable);
		actionPlancoCreditRecDelete.setEnabled(editable);
		actionPlancoCreditDepSelect.setEnabled(editable);
		actionPlancoCreditDepDelete.setEnabled(editable);
		actionOrganSelect.setEnabled(editable);
		actionOrganDelete.setEnabled(editable);
		actionLolfNomenclatureRecetteSelect.setEnabled(editable);
		actionLolfNomenclatureRecetteDelete.setEnabled(editable);
	}

	private void cleanData() {
		tfCatNumero.clean();
		tfCatLibelle.clean();
		tfCatDateVote.clean();
		tfCatDateDebut.clean();
		tfCatDateFin.clean();
		tfFournisUlr.clean();
		checkBoxCatPublieWeb.setSelected(false);
		tfCatCommentaire.setText(null);
		catalogueResponsable.clean();
		cataloguePublic.clean();
		tfPlancoCreditRec.clean();
		tfPlancoCreditDep.clean();
		tfOrgan.clean();
		tfLolfNomenclatureRecette.clean();
	}

	private void showRequired(boolean show) {
		tfCatLibelle.showRequired(show);
		tfCatDateDebut.showRequired(show);
		tfFournisUlr.showRequired(show);
		tfCatCommentaire.showRequired(show);
	}

	private void onFournisUlrSelect() {
		if (fournisUlrInterneSelect == null) {
			fournisUlrInterneSelect = new FournisUlrInterneSelect();
		}
		if (fournisUlrInterneSelect.open(app.appUserInfo().utilisateur(), null, eoCatalogue.fournisUlr())) {
			eoCatalogue.setFournisUlrRelationship(fournisUlrInterneSelect.getSelected());
			if (eoCatalogue.fournisUlr() != null) {
				tfFournisUlr.setText(eoCatalogue.fournisUlr().personne().persNomPrenom());
			}
		}
	}

	private void onCheckCatPublieWeb() {
		if (eoCatalogue != null && eoCatalogue.cataloguePrestation() != null) {
			eoCatalogue.cataloguePrestation().setCatPublieWeb(checkBoxCatPublieWeb.isSelected() ? "O" : "N");
		}
	}

	private void onPlancoCreditRecSelect() {
		if (plancoCreditRecSelect == null) {
			plancoCreditRecSelect = new PlancoCreditRecSelect();
		}
		if (plancoCreditRecSelect.open(app.appUserInfo().utilisateur(), app.superviseur().currentExercice(),
				eoCatalogue.cataloguePrestation().pcoNumRecette())) {
			eoCatalogue.cataloguePrestation().setPcoNumRecette(plancoCreditRecSelect.getSelected().pcoNum());
			if (eoCatalogue.cataloguePrestation().pcoNumRecette() != null) {
				tfPlancoCreditRec.setText(eoCatalogue.cataloguePrestation().pcoNumRecette());
			}
		}
		updateInterfaceEnabling();
	}

	private void onPlancoCreditRecDelete() {
		eoCatalogue.cataloguePrestation().setPcoNumRecette(null);
		tfPlancoCreditRec.setText(null);
		updateInterfaceEnabling();
	}

	private void onPlancoCreditDepSelect() {
		if (plancoCreditDepSelect == null) {
			plancoCreditDepSelect = new PlancoCreditDepSelect();
		}
		if (plancoCreditDepSelect.open(app.appUserInfo().utilisateur(), app.superviseur().currentExercice(), null,
				eoCatalogue.cataloguePrestation().pcoNumDepense())) {
			eoCatalogue.cataloguePrestation().setPcoNumDepense(plancoCreditDepSelect.getSelected().pcoNum());
			if (eoCatalogue.cataloguePrestation().pcoNumDepense() != null) {
				tfPlancoCreditDep.setText(eoCatalogue.cataloguePrestation().pcoNumDepense());
			}
		}
		updateInterfaceEnabling();
	}

	private void onPlancoCreditDepDelete() {
		eoCatalogue.cataloguePrestation().setPcoNumDepense(null);
		tfPlancoCreditDep.setText(null);
		updateInterfaceEnabling();
	}

	private void onOrganSelect() {
		if (organSelect == null) {
			organSelect = new OrganSelect();
		}
		if (organSelect.open(app.appUserInfo().utilisateur(), app.superviseur().currentExercice(), eoCatalogue.cataloguePrestation()
				.organRecette(), null)) {
			eoCatalogue.cataloguePrestation().setOrganRecetteRelationship(organSelect.getSelected());
			if (eoCatalogue.cataloguePrestation().organRecette() != null) {
				tfOrgan.setText(eoCatalogue.cataloguePrestation().organRecette().orgLib());
			}
			else {
				tfOrgan.setText(null);
			}
		}
		updateInterfaceEnabling();
	}

	private void onOrganDelete() {
		eoCatalogue.cataloguePrestation().setOrganRecetteRelationship(null);
		tfOrgan.setText(null);
		updateInterfaceEnabling();
	}

	private void onLolfNomenclatureRecetteSelect() {
		if (lolfNomenclatureRecetteSelect == null) {
			lolfNomenclatureRecetteSelect = new LolfNomenclatureRecetteSelect();
		}
		if (lolfNomenclatureRecetteSelect.open(app.appUserInfo().utilisateur(), app.superviseur().currentExercice(), eoCatalogue
				.cataloguePrestation().organRecette(), null, eoCatalogue.cataloguePrestation().lolfNomenclatureRecette())) {
			eoCatalogue.cataloguePrestation().setLolfNomenclatureRecetteRelationship(lolfNomenclatureRecetteSelect.getSelected());
			if (eoCatalogue.cataloguePrestation().lolfNomenclatureRecette() != null) {
				tfLolfNomenclatureRecette.setText(eoCatalogue.cataloguePrestation().lolfNomenclatureRecette().lolfCode() + "."
						+ eoCatalogue.cataloguePrestation().lolfNomenclatureRecette().lolfLibelle());
			}
			else {
				tfLolfNomenclatureRecette.setText(null);
			}
		}
		updateInterfaceEnabling();
	}

	private void onLolfNomenclatureRecetteDelete() {
		eoCatalogue.cataloguePrestation().setLolfNomenclatureRecetteRelationship(null);
		tfLolfNomenclatureRecette.setText(null);
		updateInterfaceEnabling();
	}

	private final class CatalogueResponsableTablePanel extends ZExtendedTablePanel {

		public CatalogueResponsableTablePanel() {
			super("Responsables du catalogue", true, false, true);

			addCol(new ZEOTableModelColumn(getDG(), EOCatalogueResponsable.UTILISATEUR_KEY + "." + EOUtilisateur.PERSONNE_PERS_NOM_PRENOM_KEY,
					"Utilisateur", 150));

			initGUI();

			setPreferredSize(new Dimension(0, 80));
		}

		public void reloadData() {
			if (eoCatalogue != null) {
				setObjectArray(eoCatalogue.catalogueResponsables());
			}
			else {
				setObjectArray(null);
			}
		}

		protected void onAdd() {
			if (utilisateurSelect == null) {
				utilisateurSelect = new UtilisateurSelect();
			}
			if (utilisateurSelect.open(app.appUserInfo().utilisateur(), null, null)) {
				if (utilisateurSelect.getSelecteds() != null) {
					NSArray a = utilisateurSelect.getSelecteds();
					for (int i = 0; i < a.count(); i++) {
						EOUtilisateur utilisateur = (EOUtilisateur) a.objectAtIndex(i);
						if (!((NSArray) eoCatalogue.catalogueResponsables().valueForKey(EOCatalogueResponsable.UTILISATEUR_KEY))
								.containsObject(utilisateur)) {
							FactoryCatalogueResponsable.newObject(ec, eoCatalogue, utilisateur);
						}
					}
					reloadData();
				}
			}
		}

		protected void onUpdate() {
		}

		protected void onDelete() {
			if (selectedObject() != null) {
				EOCatalogueResponsable object = (EOCatalogueResponsable) selectedObject();
				eoCatalogue.removeFromCatalogueResponsablesRelationship(object);
				ec.deleteObject(object);
				reloadData();
			}
		}

		protected void onSelectionChanged() {
		}

	}

	private final class CataloguePublicTablePanel extends ZExtendedTablePanel {

		private ZEOTableModelColumn	colRemise;

		public CataloguePublicTablePanel() {
			super("Type de public concern\u00E9", true, false, true);

			final ZEOTableModelColumn.ZEONumFieldTableCellEditor _editor = new ZEOTableModelColumn.ZEONumFieldTableCellEditor(new JTextField(),
					Constants.FORMAT_DECIMAL_EDIT);

			addCol(new ZEOTableModelColumn(getDG(), EOCataloguePublic.TYPE_PUBLIC_KEY + "." + EOTypePublic.TYPU_LIBELLE_KEY, "Type client", 150));

			colRemise = new ZEOTableModelColumn(getDG(), EOCataloguePublic.CAP_POURCENTAGE_KEY, "Remise", 60, Constants.FORMAT_DECIMAL_DISPLAY);
			colRemise.setEditable(true);
			colRemise.setTableCellEditor(_editor);
			colRemise.setFormatEdit(Constants.FORMAT_DECIMAL_EDIT);
			colRemise.setColumnClass(BigDecimal.class);
			addCol(colRemise);

			initGUI();

			setPreferredSize(new Dimension(0, 80));
		}

		public void reloadData() {
			if (eoCatalogue != null) {
				setObjectArray(eoCatalogue.cataloguePublics());
			}
			else {
				setObjectArray(null);
			}
		}

		protected void onAdd() {
			if (typePublicSelect == null) {
				typePublicSelect = new TypePublicSelect();
			}
			if (typePublicSelect.open(app.appUserInfo().utilisateur(), null, null)) {
				if (typePublicSelect.getSelecteds() != null) {
					NSArray a = typePublicSelect.getSelecteds();
					for (int i = 0; i < a.count(); i++) {
						EOTypePublic typePublic = (EOTypePublic) a.objectAtIndex(i);
						if (!((NSArray) eoCatalogue.cataloguePublics().valueForKey(EOCataloguePublic.TYPE_PUBLIC_KEY))
								.containsObject(typePublic)) {
							FactoryCataloguePublic.newObject(ec, eoCatalogue, typePublic);
						}
					}
					reloadData();
				}
			}
		}

		protected void onUpdate() {
		}

		protected void onDelete() {
			if (selectedObject() != null) {
				EOCataloguePublic object = (EOCataloguePublic) selectedObject();
				eoCatalogue.removeFromCataloguePublicsRelationship(object);
				ec.deleteObject(object);
				reloadData();
			}
		}

		protected void onSelectionChanged() {
		}

		public void setEditable(boolean editable) {
			colRemise.setEditable(editable);
			super.setEditable(editable);
		}
	}

	private void initGUI() {
		JPanel contentPanel = new JPanel(new BorderLayout());
		contentPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		// contentPanel.setPreferredSize(WINDOW_DIMENSION);
		setContentPane(contentPanel);

		Border commonEmptyThickBorder = BorderFactory.createEmptyBorder(12, 3, 12, 3);
		Border commonEmptyThinBorder = BorderFactory.createEmptyBorder(0, 3, 0, 3);

		// num\u00E9ro
		JPanel panelCatNumero = new JPanel(new FlowLayout(FlowLayout.LEFT));
		panelCatNumero.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		JLabel jlCatNumero = new JLabel("Catalogue N\u00B0 ");
		jlCatNumero.setFont(jlCatNumero.getFont().deriveFont(Font.BOLD, 20));
		jlCatNumero.setForeground(Constants.COLOR_LABEL_FGD_NORMAL);
		panelCatNumero.add(jlCatNumero);
		tfCatNumero.setFont(tfCatNumero.getFont().deriveFont(Font.BOLD, 24));
		tfCatNumero.setHorizontalAlignment(SwingConstants.RIGHT);
		tfCatNumero.setViewOnly();
		panelCatNumero.add(tfCatNumero);

		// dates debut/fin + date de vote
		JPanel panelDates = new JPanel(new BorderLayout(0, 0));
		panelDates.setBorder(commonEmptyThickBorder);
		JPanel panelDatesDebutFin = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panelDatesDebutFin.setBorder(BorderFactory.createEmptyBorder());
		panelDatesDebutFin.add(UIUtilities.labeledComponent("Date d\u00E9but", tfCatDateDebut, null));
		panelDatesDebutFin.add(Box.createHorizontalStrut(20));
		panelDatesDebutFin.add(UIUtilities.labeledComponent("Date fin", tfCatDateFin, null));
		panelDates.add(panelDatesDebutFin, BorderLayout.LINE_START);
		panelDates.add(new JPanel(), BorderLayout.CENTER);
		panelDates.add(UIUtilities.labeledComponent("Date de vote", tfCatDateVote, null), BorderLayout.LINE_END);

		// fournisseur
		JPanel panelFournis = UIUtilities.labeledComponent("Fournisseur", tfFournisUlr, actionFournisUlrSelect, true);
		panelFournis.setBorder(commonEmptyThinBorder);

		// libell\u00E9
		JPanel panelLibelle = UIUtilities.labeledComponent("Libell\u00E9", tfCatLibelle, null);
		panelLibelle.setBorder(commonEmptyThickBorder);

		// publieWeb
		JPanel panelPublieWeb = UIUtilities.labeledComponent(null, checkBoxCatPublieWeb, null);
		panelPublieWeb.setBorder(commonEmptyThinBorder);

		// commentaire
		JPanel panelCommentaire = UIUtilities.labeledComponent("Commentaire (visible sur le web)", tfCatCommentaire, null);
		panelCommentaire.setBorder(commonEmptyThinBorder);
		panelCommentaire.setMinimumSize(new Dimension(0, 80));

		// responsables + publics
		JPanel panelResponsablesPublics = new JPanel(new GridLayout(1, 2, 10, 0));
		panelResponsablesPublics.setBorder(commonEmptyThickBorder);
		panelResponsablesPublics.add(catalogueResponsable);
		panelResponsablesPublics.add(cataloguePublic);

		// planco
		Box panelBudget = Box.createVerticalBox();
		panelBudget.setBorder(BorderFactory.createTitledBorder(null, "Informations budg\u00E9taires par d\u00E9faut", TitledBorder.DEFAULT_JUSTIFICATION,
				TitledBorder.DEFAULT_POSITION, null, Constants.COLOR_LABEL_FGD_LIGHT));
		panelBudget.add(UIUtilities.labeledComponent("Imputation recette", tfPlancoCreditRec, new Action[] { actionPlancoCreditRecSelect,
				actionPlancoCreditRecDelete }, UIUtilities.DEFAULT_BOXED, UIUtilities.DEFAULT_ALIGNMENT));
		panelBudget.add(UIUtilities.labeledComponent("Ligne budg\u00E9taire recette", tfOrgan, new Action[] { actionOrganSelect, actionOrganDelete },
				UIUtilities.DEFAULT_BOXED, UIUtilities.DEFAULT_ALIGNMENT));
		panelBudget.add(UIUtilities.labeledComponent("Action Lolf recette", tfLolfNomenclatureRecette, new Action[] {
				actionLolfNomenclatureRecetteSelect, actionLolfNomenclatureRecetteDelete }, UIUtilities.DEFAULT_BOXED,
				UIUtilities.DEFAULT_ALIGNMENT));
		panelBudget.add(UIUtilities.labeledComponent("Imputation d\u00E9pense", tfPlancoCreditDep, new Action[] { actionPlancoCreditDepSelect,
				actionPlancoCreditDepDelete }, UIUtilities.DEFAULT_BOXED, UIUtilities.DEFAULT_ALIGNMENT));

		Box top = Box.createVerticalBox();
		top.setBorder(BorderFactory.createEmptyBorder());
		top.add(panelCatNumero);
		top.add(panelDates);
		top.add(panelFournis);
		top.add(panelLibelle);
		top.add(panelPublieWeb);
		top.add(panelCommentaire);

		Box bottom = Box.createVerticalBox();
		bottom.setBorder(BorderFactory.createEmptyBorder());
		bottom.add(panelBudget);

		JPanel center = new JPanel(new BorderLayout());
		center.setBorder(BorderFactory.createEmptyBorder());
		center.add(top, BorderLayout.NORTH);
		center.add(panelResponsablesPublics, BorderLayout.CENTER);
		center.add(bottom, BorderLayout.SOUTH);

		getContentPane().add(center, BorderLayout.CENTER);
		getContentPane().add(buildBottomPanel(), BorderLayout.PAGE_END);

		initInputMap();

		this.validate();
		this.pack();
	}

	private final JPanel buildBottomPanel() {
		JPanel p = new JPanel();
		p.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.add(Box.createHorizontalGlue());
		p.add(btCancel);
		p.add(Box.createRigidArea(new Dimension(5, 0)));
		p.add(btValid);
		return p;
	}

	private void initInputMap() {
		// ((JComponent) getContentPane()).getActionMap().put("F1", new ActionHelp());
		// ((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0), "F1");

		((JComponent) getContentPane()).getActionMap().put("CTRL_Q", app.superviseur().mainMenu.actionQuit);
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
				KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK), "CTRL_Q");

		((JComponent) getContentPane()).getActionMap().put("CTRL_S", actionValid);
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
				KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK), "CTRL_S");

		((JComponent) getContentPane()).getActionMap().put("F10", actionValid);
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F10, 0), "F10");

		((JComponent) getContentPane()).getActionMap().put("ESCAPE", actionCancel);
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"ESCAPE");

		((JComponent) getContentPane()).getActionMap().put("F8", actionShowRequired);
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("F8"), "F8");

		((JComponent) getContentPane()).getActionMap().put("ALT_F8", actionUnshowRequired);
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("released F8"), "ALT_F8");
	}

	private class ActionValid extends AbstractAction {
		public ActionValid() {
			super("Valider");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onValid();
			}
		}
	}

	private class ActionCancel extends AbstractAction {
		public ActionCancel() {
			super("Annuler");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CANCEL_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onCancel();
			}
		}
	}

	private class ActionClose extends AbstractAction {
		public ActionClose() {
			super("Fermer");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CLOSE_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onClose();
			}
		}
	}

	private class ActionShowRequired extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				showRequired(true);
			}
		}
	}

	private class ActionUnshowRequired extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				showRequired(false);
			}
		}
	}

	private class ActionFournisUlrSelect extends AbstractAction {
		public ActionFournisUlrSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onFournisUlrSelect();
			}
		}
	}

	private class ActionPlancoCreditRecSelect extends AbstractAction {
		public ActionPlancoCreditRecSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onPlancoCreditRecSelect();
			}
		}
	}

	private class ActionPlancoCreditRecDelete extends AbstractAction {
		public ActionPlancoCreditRecDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onPlancoCreditRecDelete();
			}
		}
	}

	private class ActionPlancoCreditDepSelect extends AbstractAction {
		public ActionPlancoCreditDepSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onPlancoCreditDepSelect();
			}
		}
	}

	private class ActionPlancoCreditDepDelete extends AbstractAction {
		public ActionPlancoCreditDepDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onPlancoCreditDepDelete();
			}
		}
	}

	private class ActionOrganSelect extends AbstractAction {
		public ActionOrganSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onOrganSelect();
			}
		}
	}

	private class ActionOrganDelete extends AbstractAction {
		public ActionOrganDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onOrganDelete();
			}
		}
	}

	private class ActionLolfNomenclatureRecetteSelect extends AbstractAction {
		public ActionLolfNomenclatureRecetteSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onLolfNomenclatureRecetteSelect();
			}
		}
	}

	private class ActionLolfNomenclatureRecetteDelete extends AbstractAction {
		public ActionLolfNomenclatureRecetteDelete() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_DELETE_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onLolfNomenclatureRecetteDelete();
			}
		}
	}

	private class ActionCheckCatPublieWeb extends AbstractAction {
		public ActionCheckCatPublieWeb() {
			super("Le catalogue sera visible sur le web");
			putValue(AbstractAction.SHORT_DESCRIPTION, "Permet de publier le catalogue sur le web, pour les types de public d\u00E9sign\u00E9s");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onCheckCatPublieWeb();
			}
		}
	}

	private class LocalWindowListener implements WindowListener {
		public void windowActivated(WindowEvent arg0) {
		}

		public void windowClosed(WindowEvent arg0) {
		}

		public void windowClosing(WindowEvent arg0) {
			actionClose.actionPerformed(null);
		}

		public void windowDeactivated(WindowEvent arg0) {
		}

		public void windowDeiconified(WindowEvent arg0) {
		}

		public void windowIconified(WindowEvent arg0) {
		}

		public void windowOpened(WindowEvent arg0) {
		}
	}

	public void setWaitCursor(boolean bool) {
		if (bool) {
			this.getGlassPane().setVisible(true);
		} else {
			this.getGlassPane().setVisible(false);
		}
	}

	/**
	 * Permet de d\u00E9finir un panel qui sert à afficher un waitcursor et à interdire toute modif sur le panel.
	 *
	 * @author rprin
	 */
	public final class BusyGlassPanel extends JPanel {
		public final Color	COLOR_WASH	= new Color(64, 64, 64, 32);

		public BusyGlassPanel() {
			super.setOpaque(false);
			super.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

			// Suck up them events!!!
			super.addKeyListener((new KeyAdapter() {
			}));
			super.addMouseListener((new MouseAdapter() {
			}));
			super.addMouseMotionListener((new MouseMotionAdapter() {
			}));
		}

		public final void paintComponent(Graphics p_graphics) {
			Dimension l_size = super.getSize();

			// Wash the pane with translucent gray.
			p_graphics.setColor(COLOR_WASH);
			p_graphics.fillRect(0, 0, l_size.width, l_size.height);

			// Paint a grid of white/black dots.
			p_graphics.setColor(Color.white);
			for (int j = 3; j < l_size.height; j += 8) {
				for (int i = 3; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
			p_graphics.setColor(Color.black);
			for (int j = 4; j < l_size.height; j += 8) {
				for (int i = 4; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
		}
	}
}
