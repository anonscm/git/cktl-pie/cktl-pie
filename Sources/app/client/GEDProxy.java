/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.io.File;
import java.io.FileInputStream;
import java.util.StringTokenizer;

import org.cocktail.fwkcktlwebapp.common.util.GEDClient;
import org.cocktail.fwkcktlwebapp.common.util.GEDDescription;
import org.cocktail.kava.client.metier.EOGrhumParametres;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class GEDProxy {

	public static final String	UNIX_FILE_PATH_SEPARATOR		= "/";
	public static final String	WINDOWS_FILE_PATH_SEPARATOR		= "\\";
	public static final String	APPLICATION_FILE_PATH_SEPARATOR	= "/";

	protected ApplicationClient	app;
	protected EOEditingContext	ec;

	GEDClient					clientGED;

	public GEDProxy() {
		app = (ApplicationClient) EOApplication.sharedApplication();
		ec = app.editingContext();

		try {
			try {
				clientGED = new GEDClient(app.getParam(EOGrhumParametres.PARAM_GEDFS_SERVICE_HOST),
						Integer.valueOf(app.getParam(EOGrhumParametres.PARAM_GEDFS_SERVICE_PORT)).intValue(),
						((Integer) app.appUserInfo().noIndividu()).intValue(), "COUR");
			} catch (Exception e) {
				System.out.println("Pas de serveur GEDFS dispo a l'@ : " + app.getParam(EOGrhumParametres.PARAM_GEDFS_SERVICE_HOST) + ":"
						+ app.getParam(EOGrhumParametres.PARAM_GEDFS_SERVICE_PORT) + "... on fait sans...");
				clientGED = null;
			}
		} catch (Exception e) {
			System.out.println("Pas de serveur GEDFS dispo... on fait sans...");
			clientGED = null;
		}
	}

	public boolean isAvailable() {
		return clientGED != null;
	}

	/**
	 * Enregistre un fichier present en local sur le serveur de GED Lit le fichier et fournit les informations necessaires au serveur de GED
	 * Parametres : - cheminFichier : le chemin complet du fichier - titre : le titre du document - categorie : categorie du document (voir
	 * gedServeur) - lien : URL du document donnee en retour de l'enregistrement
	 */
	public Integer enregistrerDocument(String cheminFichier, String titre, String categorie) {
		File aFile;
		FileInputStream aFileStream;
		String nomFichier;
		long availableSize;
		int returnedValue;

		// re-initialisation du client GED
		clientGED.reset();
		// Lecture du fichier et recuperation du contenu
		try {
			aFile = new File(cheminFichier);
			nomFichier = aFile.getName();
			aFileStream = new FileInputStream(aFile);
			availableSize = aFile.length();
			// Ecriture vers le GEDServer
			returnedValue = clientGED.enregistrerDocument(aFileStream, titre, availableSize, nomFichier, categorie);
			aFileStream.close();
		} catch (Throwable exception) {
			app.showErrorDialog(new Exception(exception));
			return new Integer(-1);
		}

		// La returnedValue vaut le ID du document ou -1 en cas d'erreur
		return new Integer(returnedValue);
	}

	/**
	 * Enregistre un fichier present en local sur le serveur de GED // Lit le fichier et fournit les informations necessaires au serveur de GED
	 *
	 * @param cheminFichier
	 *            le chemin complet du fichier
	 * @param titre
	 *            le titre du document
	 * @param cheminPere
	 *            chemin du repertoire pere
	 * @param pidPere
	 *            pid du repertoire pere dans lequel le nouveau document doit etre cree
	 * @param categorie
	 *            categorie du document (voir gedServeur)
	 * @return
	 */
	public Integer enregistrerDocument(String cheminFichier, String titre, String cheminPere, Integer pidPere, String categorie) {
		File aFile;
		FileInputStream aFileStream;
		String nomFichier;
		String cheminRelatif;
		int indexDebut, indexFin;
		long availableSize;
		int returnedValue;

		// re-initialisation du client GED
		clientGED.reset();

		// Creation de l'objet fichier
		aFile = new File(cheminFichier);
		nomFichier = aFile.getName();
		// Verifier que le chemin du fichier est bien sous le repertoire
		if (!aFile.getPath().startsWith(cheminPere)) {
			app.showErrorDialog("enregistrerDocument() : le fichier " + aFile.getPath() + " n'est pas contenu dans " + cheminPere);
			return new Integer(-1);
		}
		// Recherche du chemin relatif du fichier (/ au repertoire passe)
		indexDebut = cheminPere.length() + 1;
		indexFin = aFile.getPath().indexOf(aFile.getName());
		cheminRelatif = aFile.getPath().substring(indexDebut, indexFin);
		cheminRelatif = convertWindowsPathSeparator(cheminRelatif);

		try {
			aFileStream = new FileInputStream(aFile);
			availableSize = aFile.length();
			// Ecriture vers le GEDServer
			returnedValue = clientGED.enregistrerDocument(aFileStream, titre, availableSize, nomFichier, cheminRelatif, pidPere.intValue(),
					categorie);
			aFileStream.close();
		} catch (Throwable exception) {
			app.showErrorDialog(new Exception(exception));
			return new Integer(-1);
		}

		// La returnedValue vaut le ID du document ou -1 en cas d'erreur
		return new Integer(returnedValue);
	}

	/**
	 * Modification d'un document existant
	 */
	public boolean remplacerDocument(String cheminFichier, String titre, Integer pid) {
		File aFile;
		String nomFichier;
		FileInputStream aFileStream;
		boolean returnedValue;
		long availableSize;

		// re-initialisation du client GED
		clientGED.reset();

		// Lecture du contenu du fichier
		try {
			aFile = new File(cheminFichier);
			nomFichier = aFile.getName();
			aFileStream = new FileInputStream(aFile);
			availableSize = aFile.length();
			// Remplacement du document sur le GEDServer
			returnedValue = clientGED.remplacerDocument(aFileStream, titre, availableSize, nomFichier, pid.intValue());
			aFileStream.close();
		} catch (Exception e) {
			app.showErrorDialog(e);
			return false;
		}
		return returnedValue;
	}

	/**
	 * Suppression du document dont l'Id est passee en parametre
	 */
	public boolean supprimerDocument(Integer documentID) {
		clientGED.reset();
		try {
			return clientGED.supprimerDocument(documentID.intValue());
		} catch (Exception e) {
			app.showErrorDialog(e);
			return false;
		}
	}

	/**
	 * Retourne la reference URL du document dont l'Id est passee en parametre
	 */
	public String referenceDocument(Integer documentId) {
		if (documentId == null) {
			return null;
		}

		clientGED.reset();

		try {
			if (!clientGED.inspecterDocument(documentId.intValue())) {
				throw new Exception("getDescription()  : le document " + documentId + " n'a pas \u00E9t\u00E9 trouv\u00E9");
			}
			return clientGED.description().reference;
		} catch (Exception e) {
			app.showErrorDialog(e);
			return null;
		}
	}

	/**
	 * Retourne la description d'un document sous la forme d'un objet de type GEDDescription.
	 */
	public GEDDescription descriptionDocument(Integer documentId) {
		if (documentId == null) {
			return null;
		}
		clientGED.reset();
		try {
			if (!clientGED.inspecterDocument(documentId.intValue())) {
				throw new Exception("getDescription() : le document " + documentId + " n'a pas \u00e9t\u00e9 trouv\u00e9");
			}
			return clientGED.description();
		} catch (Exception e) {
			app.showErrorDialog(e);
			return null;
		}
	}

	/**
	 * Creation d'un repertoire reserve pour gerer une arborescence
	 */
	public Integer creerRepertoire(String categorie) {
		clientGED.reset();
		int pidRepertoire = clientGED.reserverDocument(categorie);
		return new Integer(pidRepertoire);
	}

	/**
	 * Suppression du repertoire dont la reference est passee en parametre. Prend en charge la suppression du contenu du reperrtoire.
	 *
	 * @param pidRepertoire :
	 *            la reference du repertoire a supprimer.
	 */
	public boolean supprimerRepertoire(Integer pidRepertoire) {
		clientGED.reset();
		if (!clientGED.viderRepertoire(pidRepertoire.intValue())) {
			return false;
		}
		return clientGED.supprimerDocument(pidRepertoire.intValue());
	}

	/**
	 * Suppression du contenu du repertoire
	 *
	 * @param pidRepertoire :
	 *            la reference du repertoire a supprimer
	 */
	public boolean viderRepertoire(Integer pidRepertoire) {
		clientGED.reset();
		return clientGED.viderRepertoire(pidRepertoire.intValue());
	}

	/**
	 * Enregistrement du repertoire passe en parametre. L'objet passe en parametre doit etre un repertoire, sinon aucune operation n'est
	 * realisee.
	 *
	 * @param fichier :
	 *            le repertoire a enrgistrer
	 * @param titre :
	 *            le titre (voir GEDServeur) du document
	 * @param categorie :
	 *            la categorie (voir GEDServeur) dans laquelle le reprtoire doit etre enregistre
	 * @return Un Integer contenant le pid du repertoire enregistre ou -1 en cas d'erreur.
	 */
	public Integer enregistrerRepertoire(File fichier, String titre, String categorie) {
		// Verifications
		if (!fichier.exists()) {
			app.showErrorDialog("enregistrerRepertoire() - Le fichier n'existe pas.");
			return new Integer(-1);
		}

		if (!fichier.isDirectory()) {
			app.showErrorDialog("enregistrerRepertoire() - Le fichier n'est pas un repertoire.");
			return new Integer(-1);
		}

		// Creation du repertoire
		Integer pidRepertoire;
		pidRepertoire = creerRepertoire(categorie);
		if (pidRepertoire.intValue() == -1) {
			app.showErrorDialog("enregistrerRepertoire() - Le repertoire n'a pas ete cree");
		}

		boolean result = enregistrerContenuRepertoire(titre, fichier, pidRepertoire, categorie);
		// Si echec, supprimer le repertoire
		if (!result) {
			app.showErrorDialog("enregistrerRepertoire() - Le contenu du repertoire n'a pas ete enregsitre, suppression du repertoire cree.");
			supprimerRepertoire(pidRepertoire);
		}

		// Tout s'est bien passe...
		return (result ? pidRepertoire : new Integer(-1));

	}

	/**
	 * Remplacement du contenu du repertoire dont la reference (pid) est passee en parametre par le contenu du repertoire local.
	 *
	 * @param pidRepertoire :
	 *            la reference du repertoire a remplacer
	 * @param repertoire :
	 *            repertoire a enregistrer a la place
	 * @return un Boolean qui contient true si l'operation a reussie, false sinon.
	 */
	public boolean remplacerRepertoire(Integer pidRepertoire, File repertoire) {
		// Verifications
		if (!repertoire.exists()) {
			app.showErrorDialog("remplacerRepertoire() - Le repertoire n'existe pas.");
			return false;
		}

		// Recherche des informations sur le repertoire a modifier
		GEDDescription description = descriptionDocument(pidRepertoire);
		if (description == null) {
			app.showErrorDialog("remplacerRepertoire() - Impossible de recuperer le description du document.");
			return false;
		}

		// Suppression de l'ancien contenu
		if (!viderRepertoire(pidRepertoire)) {
			app.showErrorDialog("remplacerRepertoire() - Erreur lors de la suppression du contenu du repertoire pid : " + pidRepertoire);
			return false;
		}

		// Enregistrement
		return enregistrerContenuRepertoire(description.title, repertoire, pidRepertoire, description.category);
	}

	/**
	 * Enregistrement du contenu du repertoire passe en parametre sur le document reserve identifie par pidRepertoire.
	 *
	 * @param titre
	 * @param repertoire
	 * @param pidRepertoire
	 * @param categorie
	 * @return
	 */
	private boolean enregistrerContenuRepertoire(String titre, File repertoire, Integer pidRepertoire, String categorie) {
		// Verifications
		if (!repertoire.exists()) {
			app.showErrorDialog("enregistrerContenuRepertoire() - Erreur le repertoire n'existe pas.");
			return false;
		}

		// Recuperation de la liste des fichiers
		NSArray listeFichier = getTreeFileList(repertoire);

		// Enregistrement des fichiers
		String cheminRepertoire = repertoire.getPath();

		for (int i = 0; i < listeFichier.count(); i++) {
			String cheminFichier = ((File) listeFichier.objectAtIndex(i)).getPath();
			Integer returnedValue = enregistrerDocument(cheminFichier, titre, cheminRepertoire, pidRepertoire, categorie);
			if (returnedValue.intValue() == -1) {
				app.showErrorDialog("enregistrerContenuRepertoire() - Une erreur est survenue lors de l'ecriture du repertoire "
						+ cheminRepertoire + " sur le fichier " + cheminFichier + ". Suppression de tout le contenu");
				viderRepertoire(pidRepertoire);
				return false;
			}
		}
		return true;
	}

	/**
	 * Convertit un chemin type Windows avec separateur "\\" par un chemin avec separateur "/" de type Unix L'application utilise des chemins de
	 * type Unix.
	 */
	protected String convertWindowsPathSeparator(String windowsPath) {
		// Si le chemin n'est pas un chemin Windows, on retourne le chemin complet
		if (windowsPath.indexOf(WINDOWS_FILE_PATH_SEPARATOR) < 0) {
			return windowsPath;
		}

		StringTokenizer st = new StringTokenizer(windowsPath, WINDOWS_FILE_PATH_SEPARATOR);
		StringBuffer unixPath = new StringBuffer();
		while (st.hasMoreTokens()) {
			unixPath.append(st.nextToken() + APPLICATION_FILE_PATH_SEPARATOR);
		}
		return unixPath.toString();
	}

	public NSArray getTreeFileList(File directory) {
		if ((directory == null) || (!directory.exists())) {
			return new NSArray();
	    }

	    NSMutableArray returnedList = new NSMutableArray();
	    File[] dirFileList = (File[]) directory.listFiles();
	    for (int i = 0; i < dirFileList.length; i++) {
	    	if (dirFileList[i].isDirectory()) {
	    		returnedList.addObjectsFromArray(getTreeFileList(dirFileList[i]));
	    	} else {
	    		returnedList.addObject(dirFileList[i]);
	    	}
	    }

	    return returnedList;
	}

}