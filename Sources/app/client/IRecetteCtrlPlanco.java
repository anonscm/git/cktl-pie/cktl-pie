package app.client;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kava.client.metier.EOPlanComptable;
import org.cocktail.kava.client.metier.EORecetteCtrlPlanco;

public interface IRecetteCtrlPlanco {
	public void didAddNewPlanco(EOPlanComptable planComptable, EOExercice exerecice);

	public void onSelectionChanged(EORecetteCtrlPlanco ctrlPlanco);
}
