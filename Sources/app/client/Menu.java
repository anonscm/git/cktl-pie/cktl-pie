/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kava.client.metier.EOFonction;
import org.cocktail.kava.client.metier.EOParametres;

import app.client.tools.Constants;

import com.webobjects.eoapplication.EOApplication;

public class Menu extends JMenuBar {
	ApplicationClient app;
	JMenu menuApplication, menuOutils, menuAide;
	public Action actionAppPreferences, actionAppReset, actionQuit;
	public Action actionStatistiques, actionImportCatalogues, actionPIFromCommande, actionDuplicateFacturePapier, actionChangementExercice,actionConsultationPaiementWeb,actionFactureCumulative;
	public Action actionShowLog, actionAbout;

	public Menu() {
		app = (ApplicationClient) EOApplication.sharedApplication();

		actionAppPreferences = new ActionAppPreferences();
		actionAppReset = new ActionAppReset();
		actionQuit = new ActionQuit();

		actionStatistiques = new ActionStatistiques();
		actionImportCatalogues = new ActionImportCatalogues();
		actionPIFromCommande = new ActionPIFromCommande();
		actionDuplicateFacturePapier = new ActionDuplicateFacturePapier();
		actionChangementExercice = new ActionChangementExercice();
		actionConsultationPaiementWeb = new ActionConsultationPaiementWeb();
		actionFactureCumulative = new ActionFactureCumulative();

		actionShowLog = new ActionShowLog();
		actionAbout = new ActionAbout();

		// APPLICATION
		menuApplication = new JMenu("Application");
		menuApplication.add(new JMenuItem(actionAppReset));
		menuApplication.addSeparator();
		JMenuItem menuItemQuit = new JMenuItem(actionQuit);
		menuItemQuit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
		menuApplication.add(menuItemQuit);

		// OUTILS
		menuOutils = new JMenu("Outils");
		menuOutils.add(new JMenuItem(actionPIFromCommande));
		menuOutils.addSeparator();
		menuOutils.add(new JMenuItem(actionDuplicateFacturePapier));
		menuOutils.add(new JMenuItem(actionFactureCumulative));
		menuOutils.addSeparator();
		menuOutils.add(new JMenuItem(actionChangementExercice));
		menuOutils.addSeparator();
		menuOutils.add(new JMenuItem(actionConsultationPaiementWeb));

		// AIDE
		menuAide = new JMenu("Aide");
		menuAide.add(new JMenuItem(actionShowLog));
		menuAide.addSeparator();
		menuAide.add(new JMenuItem(actionAbout));

		add(menuApplication);
		add(menuOutils);
		add(menuAide);
	}

	public void update() {
		actionPIFromCommande.setEnabled(false);
		actionChangementExercice.setEnabled(false);
		actionConsultationPaiementWeb.setEnabled(false);
		actionFactureCumulative.setEnabled(false);
		EOExercice exercice = app.superviseur().currentExercice();
		if (exercice != null) {
			actionConsultationPaiementWeb.setEnabled(app.canUseFonction(EOFonction.DROIT_VOIR_PAIEMENT_WEB, exercice));
			actionFactureCumulative.setEnabled(app.canUseFonction(EOFonction.DROIT_CREER_PRESTATION_CUMULATIVE, exercice));
			if (app.getParamBoolean(EOParametres.PARAM_AUTORISE_PI_FROM_COMMANDE, exercice) && app.canUseFonction(EOFonction.DROIT_GERER_PRESTATION, exercice)) {
				if (exercice.exeStatFac().equalsIgnoreCase(EOExercice.EXERCICE_OUVERT)) {
					actionPIFromCommande.setEnabled(true);
				}
				else {
					if (exercice.exeStatFac().equalsIgnoreCase(EOExercice.EXERCICE_RESTREINT)) {
						if (app.canUseFonction(EOFonction.DROIT_FACTURER_PERIODE_BLOCAGE, exercice)) {
							actionPIFromCommande.setEnabled(true);
						}
					}
				}
			}
			if (exercice.exeStatFac().equalsIgnoreCase(EOExercice.EXERCICE_RESTREINT)) {
				actionChangementExercice.setEnabled(true);
			} else {
				actionChangementExercice.setEnabled(false);
			}
		}
		updateDuplicataFacturePapier(true);
	}

	public void updateDuplicataFacturePapier(boolean hasPermissionOrgan) {
		boolean hasPermissionDuplicata = false;
		EOExercice exercice = app.superviseur().currentExercice();
		if (exercice != null) {
			if (exercice.exeStatFac().equalsIgnoreCase(EOExercice.EXERCICE_OUVERT)
					&& app.canUseFonction(EOFonction.DROIT_FACTURER, exercice)
					&& hasPermissionOrgan) {
				hasPermissionDuplicata = true;
			}
			if (exercice.exeStatFac().equalsIgnoreCase(EOExercice.EXERCICE_RESTREINT)
					&& app.canUseFonction(EOFonction.DROIT_FACTURER_PERIODE_BLOCAGE, exercice)
					&& hasPermissionOrgan) {
				hasPermissionDuplicata = true;
			}
		}
		actionDuplicateFacturePapier.setEnabled(hasPermissionDuplicata);
	}

	// app

	private class ActionAppPreferences extends AbstractAction {
		public ActionAppPreferences() {
			super("Pr\u00E9f\u00E9rences...");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_SETTINGS_16);
		}

		public void actionPerformed(ActionEvent event) {
			if (isEnabled()) {
				app.appPreferences();
			}
		}
	}

	private class ActionAppReset extends AbstractAction {
		public ActionAppReset() {
			super("Reset");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_REFRESH_16);
		}

		public void actionPerformed(ActionEvent event) {
			if (isEnabled()) {
				app.appReset();
			}
		}
	}

	private class ActionQuit extends AbstractAction {
		public ActionQuit() {
			super("Quitter");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_EXIT_16);
		}

		public void actionPerformed(ActionEvent event) {
			if (isEnabled()) {
				app.quit();
			}
		}
	}

	// outils

	private class ActionDuplicateFacturePapier extends AbstractAction {
		public ActionDuplicateFacturePapier() {
			super("Dupliquer la facture pour un nouveau client...");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_EXECUTE_16);
		}

		public void actionPerformed(ActionEvent event) {
			if (isEnabled()) {
				app.outilsDuplicateFacturePapier();
			}
		}
	}

	private class ActionPIFromCommande extends AbstractAction {
		public ActionPIFromCommande() {
			super("G\u00E9n\u00E9rer une prestation interne depuis une commande Carambole...");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_EXECUTE_16);
		}

		public void actionPerformed(ActionEvent event) {
			if (isEnabled()) {
				app.outilsPIFromCommande();
			}
		}
	}

	private class ActionStatistiques extends AbstractAction {
		public ActionStatistiques() {
			super("Statistiques...");
		}

		public void actionPerformed(ActionEvent event) {
			if (isEnabled()) {
				app.outilsStatistiques();
			}
		}
	}

	private class ActionImportCatalogues extends AbstractAction {
		public ActionImportCatalogues() {
			super("Import/Gestion des catalogues...");
		}

		public void actionPerformed(ActionEvent event) {
			if (isEnabled()) {
				app.importCatalogues();
			}
		}
	}

	private class ActionChangementExercice extends AbstractAction {
		public ActionChangementExercice() {
			super("Bascule Prestation entre exercice n vers n+1");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_EXECUTE_16);
		}

		public void actionPerformed(ActionEvent event) {
			if (isEnabled()) {
				app.interfaceMigration();
			}
		}
	}

	private class ActionConsultationPaiementWeb extends AbstractAction {
		public ActionConsultationPaiementWeb() {
			super("Consultation Paiement Web");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_EXECUTE_16);
		}

		public void actionPerformed(ActionEvent event) {
			if (isEnabled()) {
				app.paiementWeb();
			}
		}
	}

	private class ActionFactureCumulative extends AbstractAction {
		public ActionFactureCumulative() {
			super("Prestation cumulative");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_EXECUTE_16);
		}

		public void actionPerformed(ActionEvent event) {
			if (isEnabled()) {
				app.factureCumulative();
			}
		}
	}
	
	// aide
	private class ActionShowLog extends AbstractAction {
		public ActionShowLog() {
			super("Voir les logs...");
		}

		public void actionPerformed(ActionEvent event) {
			if (isEnabled()) {
				app.showLog();
			}
		}
	}

	private class ActionAbout extends AbstractAction {
		public ActionAbout() {
			super("A propos...");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INFO_16);
		}

		public void actionPerformed(ActionEvent event) {
			if (isEnabled()) {
				app.showAboutDialog();
			}
		}
	}

}