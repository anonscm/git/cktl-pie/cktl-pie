/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client.tools;

import java.awt.Color;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.ImageIcon;

import com.webobjects.eoapplication.client.EOClientResourceBundle;

public class Constants {
	public static final String			IMAGE_APP_LOGO				= "app_logo";

	public static final String			IMAGE_ADD_16				= "add_16";
	public static final String			IMAGE_ADD_16_LIGHTENED		= "add_lightened_16";
	public static final String			IMAGE_UPDATE_16				= "update_16";
	public static final String			IMAGE_UPDATE_16_LIGHTENED	= "update_lightened_16";
	public static final String			IMAGE_DELETE_16				= "delete_16";
	public static final String			IMAGE_DELETE_16_LIGHTENED	= "delete_lightened_16";
	public static final String			IMAGE_VALID_16				= "valid_16";
	public static final String			IMAGE_CANCEL_16				= "cancel_16";
	public static final String			IMAGE_CLOSE_16				= "close_16";
	public static final String			IMAGE_SAVE_16				= "save_16";
	public static final String			IMAGE_EXIT_16				= "exit_16";
	public static final String			IMAGE_NOTVALID_16			= "notvalid_16";
	public static final String			IMAGE_NOTVALID_WARNING_16	= "notvalid_warning_16";

	public static final String			IMAGE_PRINT_16				= "printer_16";
	public static final String			IMAGE_PRINT_32				= "printer_32";

	public static final String			IMAGE_NOTE_16				= "notepad_16";
	public static final String			IMAGE_NOTE_32				= "notepad_32";
	public static final String			IMAGE_NOTE2_16				= "notepad2_16";
	public static final String			IMAGE_NOTE2_32				= "notepad2_32";
	public static final String			IMAGE_EDIT_16				= "edit_16";
	public static final String			IMAGE_EDIT2_16				= "edit2_16";
	public static final String			IMAGE_REDUIRE_16			= "reduire_16";

	public static final String			IMAGE_EMAIL_16				= "mail_send_16";
	public static final String			IMAGE_ENVELOP_32			= "enveloppe_32";

	public static final String			IMAGE_LOUPE_16				= "loupe_16";
	public static final String			IMAGE_INTERROGER_16			= "why_16";
	public static final String			IMAGE_INTERROGER_12			= "why_12";
	public static final String			IMAGE_DELETE_12				= "delete_12";
	public static final String			IMAGE_INFO_16				= "info_16";
	public static final String			IMAGE_INFO_32				= "info_32";
	public static final String			IMAGE_REFRESH_16			= "refresh_16";
	public static final String			IMAGE_SEARCH_16				= "search_16";
	public static final String			IMAGE_HELP_64				= "help_64";

	public static final String			IMAGE_ALERT_INFO			= "alert-message";
	public static final String			IMAGE_ALERT_ERROR			= "alert-error";
	public static final String			IMAGE_ALERT_EXCLAM			= "alert-exclam";
	public static final String			IMAGE_ALERT_QUESTION		= "alert-question";

	public static final String			IMAGE_WILE1					= "wile1";
	public static final String			IMAGE_WILE2					= "wile2";

	public static final String			IMAGE_CALCULER_16			= "calculatrice_16";
	public static final String			IMAGE_EXECUTE_16			= "execute_16";
	public static final String			IMAGE_SETTINGS_16			= "settings_16";

	public static final String			IMAGE_COLORS_16				= "colors_16";

	public static final String          IMAGE_ITEM_VALID            = "item_valid";
	public static final String          IMAGE_ITEM_WARNING          = "item_warning";
	public static final String          IMAGE_ITEM_INVALID          = "item_invalid";
	public static final String          IMAGE_WARNING               = "warning_14";


	// IMAGE_ICON
	public static final ImageIcon		ICON_APP_LOGO				= getIconForName(IMAGE_APP_LOGO);

	public static final ImageIcon		ICON_ADD_16					= getIconForName(IMAGE_ADD_16);
	public static final ImageIcon		ICON_ADD_16_LIGHTENED		= getIconForName(IMAGE_ADD_16_LIGHTENED);
	public static final ImageIcon		ICON_UPDATE_16				= getIconForName(IMAGE_UPDATE_16);
	public static final ImageIcon		ICON_UPDATE_16_LIGHTENED	= getIconForName(IMAGE_UPDATE_16_LIGHTENED);
	public static final ImageIcon		ICON_DELETE_16				= getIconForName(IMAGE_DELETE_16);
	public static final ImageIcon		ICON_DELETE_16_LIGHTENED	= getIconForName(IMAGE_DELETE_16_LIGHTENED);
	public static final ImageIcon		ICON_VALID_16				= getIconForName(IMAGE_VALID_16);
	public static final ImageIcon		ICON_CANCEL_16				= getIconForName(IMAGE_CANCEL_16);
	public static final ImageIcon		ICON_CLOSE_16				= getIconForName(IMAGE_CLOSE_16);
	public static final ImageIcon		ICON_SAVE_16				= getIconForName(IMAGE_SAVE_16);
	public static final ImageIcon		ICON_EXIT_16				= getIconForName(IMAGE_EXIT_16);
	public static final ImageIcon		ICON_NOTVALID_16			= getIconForName(IMAGE_NOTVALID_16);
	public static final ImageIcon		ICON_NOTVALID_WARNING_16	= getIconForName(IMAGE_NOTVALID_WARNING_16);

	public static final ImageIcon		ICON_PRINT_16				= getIconForName(IMAGE_PRINT_16);
	public static final ImageIcon		ICON_PRINT_32				= getIconForName(IMAGE_PRINT_32);

	public static final ImageIcon		ICON_NOTE_16				= getIconForName(IMAGE_NOTE_16);
	public static final ImageIcon		ICON_NOTE_32				= getIconForName(IMAGE_NOTE_32);
	public static final ImageIcon		ICON_NOTE2_16				= getIconForName(IMAGE_NOTE2_16);
	public static final ImageIcon		ICON_NOTE2_32				= getIconForName(IMAGE_NOTE2_32);
	public static final ImageIcon		ICON_EDIT_16				= getIconForName(IMAGE_EDIT_16);
	public static final ImageIcon		ICON_EDIT2_16				= getIconForName(IMAGE_EDIT2_16);
	public static final ImageIcon		ICON_REDUIRE_16				= getIconForName(IMAGE_REDUIRE_16);

	public static final ImageIcon		ICON_EMAIL_16				= getIconForName(IMAGE_EMAIL_16);
	public static final ImageIcon		ICON_ENVELOP_32				= getIconForName(IMAGE_ENVELOP_32);

	public static final ImageIcon		ICON_LOUPE_16				= getIconForName(IMAGE_LOUPE_16);
	public static final ImageIcon		ICON_INTERROGER_16			= getIconForName(IMAGE_INTERROGER_16);
	public static final ImageIcon		ICON_INTERROGER_12			= getIconForName(IMAGE_INTERROGER_12);
	public static final ImageIcon		ICON_DELETE_12				= getIconForName(IMAGE_DELETE_12);
	public static final ImageIcon		ICON_INFO_16				= getIconForName(IMAGE_INFO_16);
	public static final ImageIcon		ICON_INFO_32				= getIconForName(IMAGE_INFO_32);
	public static final ImageIcon		ICON_REFRESH_16				= getIconForName(IMAGE_REFRESH_16);
	public static final ImageIcon		ICON_SEARCH_16				= getIconForName(IMAGE_SEARCH_16);
	public static final ImageIcon		ICON_HELP_64				= getIconForName(IMAGE_HELP_64);

	public static final ImageIcon		ICON_ALERT_INFO				= getIconForName(IMAGE_ALERT_INFO);
	public static final ImageIcon		ICON_ALERT_ERROR			= getIconForName(IMAGE_ALERT_ERROR);
	public static final ImageIcon		ICON_ALERT_EXCLAM			= getIconForName(IMAGE_ALERT_EXCLAM);
	public static final ImageIcon		ICON_ALERT_QUESTION			= getIconForName(IMAGE_ALERT_QUESTION);

	public static final ImageIcon		ICON_WILE1					= getIconForName(IMAGE_WILE1);
	public static final ImageIcon		ICON_WILE2					= getIconForName(IMAGE_WILE2);

	public static final ImageIcon		ICON_CALCULER_16			= getIconForName(IMAGE_CALCULER_16);
	public static final ImageIcon		ICON_EXECUTE_16				= getIconForName(IMAGE_EXECUTE_16);
	public static final ImageIcon		ICON_SETTINGS_16			= getIconForName(IMAGE_SETTINGS_16);

	public static final ImageIcon		ICON_COLORS_16				= getIconForName(IMAGE_COLORS_16);

	public static final ImageIcon       ICON_ITEM_VALID             = getIconForName(IMAGE_ITEM_VALID);
	public static final ImageIcon       ICON_ITEM_WARNING           = getIconForName(IMAGE_ITEM_WARNING);
	public static final ImageIcon       ICON_ITEM_INVALID           = getIconForName(IMAGE_ITEM_INVALID);
	public static final ImageIcon       ICON_WARNING                = getIconForName(IMAGE_WARNING);

	// COULEURS
	public static final Color			COLOR_TRANSPARENT			= new Color(0, 0, 0, 0);

	public static final Color			COLOR_COL_FGD_NORMAL		= new Color(0, 0, 0);
	public static final Color			COLOR_COL_FGD_SELECTED		= new Color(0, 0, 0);
	public static final Color			COLOR_COL_BGD_NORMAL		= new Color(235, 235, 235);
	public static final Color			COLOR_COL_BGD_SELECTED		= new Color(200, 191, 225);

	public static final Color			COLOR_TF_BGD_ENABLED		= Color.white;
	public static final Color			COLOR_TF_BGD_DISABLED		= new Color(222, 222, 222);
	public static final Color			COLOR_TF_FGD_ENABLED		= Color.blue;
	public static final Color			COLOR_TF_FGD_DISABLED		= Color.blue;

	public static final Color			COLOR_BGD_REQUIRED			= Color.cyan;

	public static final Color			COLOR_LABEL_FGD_NORMAL		= Color.black;
	public static final Color			COLOR_LABEL_FGD_LIGHT		= Color.gray;
	public static final Color			COLOR_LABEL_FGD_INFO		= Color.red;

	// FORMATS
	public static final NumberFormat	FORMAT_NUMBER				= NumberFormat.getNumberInstance();
	public static final NumberFormat	FORMAT_INTEGER				= NumberFormat.getIntegerInstance();
	public static final DecimalFormat	FORMAT_DECIMAL_DISPLAY		= new DecimalFormat("#,##0.00");
	public static final DecimalFormat	FORMAT_DECIMAL_EDIT			= new DecimalFormat("0.00");
	public static final DateFormat		FORMAT_DATESHORT			= DateFormat.getDateInstance(DateFormat.SHORT);
	public static final DateFormat		FORMAT_TIMESHORT			= DateFormat.getTimeInstance(DateFormat.SHORT);
	public static final DateFormat		FORMAT_DATETIMESHORT		= DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);

	public static ImageIcon getIconForName(String name) {
		try {
			return (ImageIcon) new EOClientResourceBundle().getObject(name);
		}
		catch (Exception e) {
			return null;
		}
	}
}