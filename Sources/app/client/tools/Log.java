/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client.tools;

import java.util.Iterator;
import java.util.Map;

public class Log {
	public static final boolean	DEFAULTSHOWOUTPUT	= true;
	public static final boolean	DEFAULTSHOWTRACE	= true;

	public int					lenForSeparator		= 80;
	public int					defaultKeyLength	= 35;
	private boolean				showOutput;
	private boolean				showTrace;

	public Log() {
		setShowOutput(DEFAULTSHOWOUTPUT);
		setShowTrace(DEFAULTSHOWTRACE);
	}

	public void appendSeparator(String sep) {
		if (sep == null) {
			sep = "-";
		}
		directWriteln(extendWithChars("", sep, lenForSeparator, false));
	}

	public void appendTitle(String str) {
		directWriteln("");
		directWriteln(capitalizedString(str));
		appendSeparator(null);
	}

	public void appendWarning(String str) {
		append(":-(   ATTENTION >>>>" + str);
	}

	public void appendSuccess(String str) {
		append(":-)   " + str);
	}

	public void appendKeyValue(String key, Object value) {
		appendKeyValue(key, value, defaultKeyLength);
	}

	public void appendKeyValue(String key, Object value, int keyLength) {
		if (key.length() > keyLength) {
			keyLength = key.length();
		}
		append(extendWithChars(key, " ", keyLength, false) + " = " + value);
	}

	public void appendBigTitle(String str) {
		directWriteln("");
		appendSeparator("*");
		int nb = lenForSeparator / 2 - str.length() / 2 - 1;
		String sep = "";
		if (nb > 0) {
			sep = extendWithChars("", "*", nb, false);
		}
		directWriteln(sep + " " + str + " " + sep);
		appendSeparator("*");
		directWriteln("");
	}

	public void trace(Object pObj) {
		if (this.showTrace) {
			StringBuffer vLine = new StringBuffer();
			vLine.append("-> ");
			vLine.append(getTime());
			vLine = vLine.append("> ");
			vLine = vLine.append(this.getCallMethod(0));
			vLine = vLine.append(" >>> ");
			if (pObj != null) {
				vLine = vLine.append(pObj.toString());
			}
			else {
				vLine = vLine.append("null");
			}
			if (pObj != null) {
				vLine = vLine.append("(" + pObj.getClass().getName() + ")");
			}
			writeln(vLine.toString());
		}
	}

	public void trace(String pVarName, Object pObj) {
		if (this.showTrace) {
			StringBuffer vLine = new StringBuffer();
			vLine.append("-> ");
			vLine.append(getTime());
			vLine = vLine.append("> ");
			vLine = vLine.append(this.getCallMethod(0));
			vLine = vLine.append(" >>> ");
			if (pVarName != null) {
				vLine = vLine.append(pVarName);
				vLine = vLine.append(" = ");
			}
			if (pObj != null) {
				vLine = vLine.append(pObj.toString());
			}
			else {
				vLine = vLine.append("null");
			}
			if (pObj != null) {
				vLine = vLine.append("(" + pObj.getClass().getName() + ")");
			}
			writeln(vLine.toString());
		}
	}

	protected String getTime() {
		return java.text.DateFormat.getTimeInstance().format(new java.util.Date());
	}

	public void append(String string) {
		writeln(string);
	}

	public void appendForceOutput(String string) {
		writeln(string, true);
	}

	public void setShowOutput(boolean b) {
		showOutput = b;
	}

	private String getCallMethod(int profondeur) {
		Throwable tmp = new Throwable();
		return tmp.getStackTrace()[profondeur + 2].toString();
	}

	public void setShowTrace(boolean b) {
		showTrace = b;
	}

	protected void writeln(String s) {
		writeln(s, showOutput);
	}

	protected void writeln(String s, boolean show) {
		if (show) {
			System.out.println(s);
		}
	}

	protected void directWriteln(String s) {
		System.out.println(s);
	}

	public boolean isShowOutput() {
		return showOutput;
	}

	public boolean isShowTrace() {
		return showTrace;
	}

	public void appendMap(Map map) {
		String key;
		int max = 0;
		for (Iterator iter = map.keySet().iterator(); iter.hasNext();) {
			key = iter.next().toString();
			if (key.length() > max) {
				max = key.length();
			}
		}
		for (Iterator iter = map.keySet().iterator(); iter.hasNext();) {
			key = iter.next().toString();
			appendKeyValue(key, map.get(key).toString(), max);
		}
	}

	private String extendWithChars(String s, String addChars, int length, boolean inFront) {
		for (; s.length() < length; s = ((inFront) ? (addChars + s) : (s + addChars)))
			;
		return s;
	}

	private String capitalizedString(String aString) {
		if ("".equals(aString))
			return "";
		String debut = (aString.substring(0, 1)).toUpperCase();
		String fin = (aString.substring(1, aString.length())).toLowerCase();
		return debut.concat(fin);
	}

}
