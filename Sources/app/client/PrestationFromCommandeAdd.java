/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.Border;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kava.client.finder.FinderFacturePapier;
import org.cocktail.kava.client.finder.FinderFournisUlr;
import org.cocktail.kava.client.finder.FinderPrestation;
import org.cocktail.kava.client.metier.EOCommandesPourPi;
import org.cocktail.kava.client.metier.EOFacturePapier;
import org.cocktail.kava.client.metier.EOFonction;
import org.cocktail.kava.client.metier.EOFournisUlr;
import org.cocktail.kava.client.metier.EOPlanComptable;
import org.cocktail.kava.client.metier.EOPrestation;
import org.cocktail.kava.client.procedures.ApiPrestation;

import app.client.select.CommandeSelect;
import app.client.select.FournisUlrInterneSelect;
import app.client.select.PlancoCreditRecSelect;
import app.client.tools.Constants;
import app.client.ui.UIUtilities;
import app.client.ui.ZLabel;
import app.client.ui.ZTextField;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;

public class PrestationFromCommandeAdd extends JDialog {

	// private static final Dimension WINDOW_DIMENSION = new Dimension(620, 640);
	private static final String					WINDOW_TITLE	= "Prestation interne depuis une commande Carambole";

	protected ApplicationClient					app;
	protected EOEditingContext					ec;

	private static PrestationFromCommandeAdd	sharedInstance;

	private Action								actionClose, actionValid, actionCancel;
	private Action								actionShowRequired, actionUnshowRequired;
	private Action								actionCommandeSelect;
	private Action								actionFournisUlrSelect;
	private Action								actionPlancoCreditRecSelect;

	private ZTextField							tfCommande;
	private ZTextField							tfFournisUlr;
	private ZTextField							tfPlancoCreditRec;
	private ZLabel								labelInfosCommande;

	private JButton								btValid, btCancel;

	private CommandeSelect						commandeSelect;
	private FournisUlrInterneSelect				fournisUlrInterneSelect;
	private PlancoCreditRecSelect				planComptable;

	private EOCommandesPourPi					commandeSelected;
	private EOFournisUlr						fournisUlrSelected;
	private EOPlanComptable						plancoCreditRecSelected;

	private JCheckBox							checkBoxFacturePapier;
	private Action								actionCheckFacturePapier;

	private ZLabel								labelInfo;

	private boolean								result;

	public PrestationFromCommandeAdd() {
		super((JFrame) null, WINDOW_TITLE, true);

		app = (ApplicationClient) EOApplication.sharedApplication();
		ec = app.editingContext();

		setGlassPane(new BusyGlassPanel());
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new LocalWindowListener());

		actionClose = new ActionClose();
		actionValid = new ActionValid();
		actionCancel = new ActionCancel();
		actionShowRequired = new ActionShowRequired();
		actionUnshowRequired = new ActionUnshowRequired();

		actionCommandeSelect = new ActionCommandeSelect();
		actionFournisUlrSelect = new ActionFournisUlrSelect();
		actionPlancoCreditRecSelect = new ActionPlancoCreditRecSelect();

		tfCommande = new ZTextField(60);
		tfCommande.setViewOnly();

		tfFournisUlr = new ZTextField(60);
		tfFournisUlr.setViewOnly();

		tfPlancoCreditRec = new ZTextField(50);
		tfPlancoCreditRec.setViewOnly();

		labelInfosCommande = new ZLabel("\n\n");

		actionCheckFacturePapier = new ActionCheckFacturePapier();
		checkBoxFacturePapier = new JCheckBox(actionCheckFacturePapier);

		btCancel = new JButton(actionCancel);
		btValid = new JButton(actionValid);

		initGUI();

		commandeSelected = null;
		fournisUlrSelected = null;
		plancoCreditRecSelected = null;

		setEditable(true, false);
	}

	public static PrestationFromCommandeAdd sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new PrestationFromCommandeAdd();
		}
		return sharedInstance;
	}

	public boolean openNew() {
		setTitle(WINDOW_TITLE);
		setEditable(true, false);
		commandeSelected = null;
		fournisUlrSelected = null;
		plancoCreditRecSelected = null;

		boolean autoriseFacturePapier = false;
		EOExercice exercice = app.superviseur().currentExercice();
		if (exercice.exeStatFac().equalsIgnoreCase(EOExercice.EXERCICE_OUVERT)
				&& app.canUseFonction(EOFonction.DROIT_FACTURER, exercice)) {
			autoriseFacturePapier = true;
		}
		if (exercice.exeStatFac().equalsIgnoreCase(EOExercice.EXERCICE_RESTREINT)
				&& app.canUseFonction(EOFonction.DROIT_FACTURER_PERIODE_BLOCAGE, exercice)) {
			autoriseFacturePapier = true;
		}
		checkBoxFacturePapier.setSelected(autoriseFacturePapier);
		checkBoxFacturePapier.setEnabled(autoriseFacturePapier);
		if (autoriseFacturePapier) {
			actionCheckFacturePapier.putValue(AbstractAction.SHORT_DESCRIPTION,
					"G\u00E9n\u00E9rer aussi la facture interne en m\u00EAme temps que la prestation interne");
		} else {
			actionCheckFacturePapier.putValue(AbstractAction.SHORT_DESCRIPTION,
					"Vous n'avez pas les droits pour cr\u00E9er une facture !");
		}
		updateData();

		return open();
	}

	public boolean canQuit() {
		if (isShowing()) {
			actionCancel.actionPerformed(null);
		}
		return !isShowing();
	}

	private boolean open() {
		result = false;
		int screenWidth = (int) this.getGraphicsConfiguration().getBounds().getWidth();
		int screenHeight = (int) this.getGraphicsConfiguration().getBounds().getHeight();
		this.setLocation((screenWidth / 2) - ((int) this.getSize().getWidth() / 2),
				((screenHeight / 2) - ((int) this.getSize().getHeight() / 2)));

		this.validate();
		this.pack();

		app.superviseur().setWaitCursor(this, false);
		setVisible(true);

		return result;
	}

	private void onValid() {
		try {
			if (commandeSelected == null) {
				throw new Exception("Il faut s\u00E9lectionner une commande !");
			}
			if (fournisUlrSelected == null) {
				throw new Exception(
						"Il faut s\u00E9lectionner un fournisseur client ( = fournisseur de l'engagement) !");
			}
			NSDictionary returnedDico = null;
			if (checkBoxFacturePapier.isSelected()) {
				returnedDico = ApiPrestation.facturePapierFromCommande(
						ec, commandeSelected, app.appUserInfo().utilisateur(), fournisUlrSelected, null);
			} else {
				returnedDico = ApiPrestation.prestationFromCommande(
						ec, commandeSelected, app.appUserInfo().utilisateur(), fournisUlrSelected, null);
			}
			result = true;
			if (returnedDico != null) {
				EOPrestation prestation =
						FinderPrestation.findByPrimaryKey(ec, returnedDico.valueForKey("110aPrestId"));
				if (prestation != null) {
					String msg = "Prestation interne No " + prestation.prestNumero() + " g\u00E9n\u00E9r\u00E9e !";
					if (checkBoxFacturePapier.isSelected()) {
						EOFacturePapier facturePapier = FinderFacturePapier.findByPrimaryKey(ec, returnedDico.valueForKey("120aFapId"));
						if (facturePapier != null) {
							msg = msg + "\nFacture interne No " + facturePapier.fapNumero() + " g\u00E9n\u00E9r\u00E9e !";
						}
					}
					app.showInfoDialog(msg);
				} else {
					throw new Exception("Probl\u00E8me pour r\u00E9cup\u00E9rer le num\u00E9ro de la prestation interne cr\u00E9\u00E9e, v\u00E9rifiez qu'elle a bien \u00E9t\u00E9 cr\u00E9\u00E9e !");
				}
			}
			setVisible(false);
		}
		catch (Exception e) {
			app.showErrorDialog(e);
		}
	}

	private void onCancel() {
		result = false;
		setVisible(false);
	}

	private void onClose() {
		actionCancel.actionPerformed(null);
	}

	private void updateData() {
		if (commandeSelected != null) {
			tfCommande.setText(commandeSelected.commNumero() + " - " + commandeSelected.commLibelle());
			labelInfosCommande.setTextHtml("Fournisseur (prestataire) : <b>" + commandeSelected.fournisUlr().personne_persNomPrenom() + "</b>\n"
					+ "Engagement No : <b>" + commandeSelected.engNumero() + "</b> - Total TTC : <b>" + commandeSelected.engTtcSaisie()
					+ "</b>\n");
		} else {
			tfCommande.setText(null);
			labelInfosCommande.setTextHtml("\n\n");
		}

		if (fournisUlrSelected != null) {
			tfFournisUlr.setText(fournisUlrSelected.personne().persNomPrenom());
		} else {
			tfFournisUlr.setText(null);
		}

		if (plancoCreditRecSelected != null) {
			tfPlancoCreditRec.setText(plancoCreditRecSelected.pcoNum() + " - " + plancoCreditRecSelected.pcoLibelle());
		} else {
			tfPlancoCreditRec.setText(null);
		}

		updateInterfaceEnabling();
	}

	private void updateInterfaceEnabling() {
	}

	private void setEditable(boolean editable, boolean forUpdate) {
		actionCancel.setEnabled(editable);
		actionValid.setEnabled(editable);
		actionClose.setEnabled(!editable);

		actionCommandeSelect.setEnabled(editable && !forUpdate);
		actionFournisUlrSelect.setEnabled(editable && !forUpdate);
		actionPlancoCreditRecSelect.setEnabled(editable);
	}

	private void showRequired(boolean show) {
		tfCommande.showRequired(show);
		tfFournisUlr.showRequired(show);
	}

	private void onCommandeSelect() {
		if (commandeSelect == null) {
			commandeSelect = new CommandeSelect();
		}
		if (commandeSelect.open(app.appUserInfo().utilisateur(), app.superviseur().currentExercice())) {
			commandeSelected = commandeSelect.getSelected();
			updateData();
			// initialise le fournisseur client (= celui qui engage) par defaut si possible...
			if (fournisUlrSelected == null && commandeSelected != null) {
				if (commandeSelected.organ() != null && commandeSelected.organ().structureUlr() != null
						&& commandeSelected.organ().structureUlr().personne() != null) {
					fournisUlrSelected = FinderFournisUlr.find(ec, commandeSelected.organ().structureUlr().personne());
					updateData();
				}
			}
		}
	}

	private void onFournisUlrSelect() {
		if (fournisUlrInterneSelect == null) {
			fournisUlrInterneSelect = new FournisUlrInterneSelect();
		}
		if (fournisUlrInterneSelect.open(app.appUserInfo().utilisateur(), null, fournisUlrSelected)) {
			fournisUlrSelected = fournisUlrInterneSelect.getSelected();
			updateData();
		}
	}

	private void onPlancoCreditRecSelect() {
		if (planComptable == null) {
			planComptable = new PlancoCreditRecSelect();
		}
		if (planComptable.open(app.appUserInfo().utilisateur(), app.superviseur().currentExercice(),
				plancoCreditRecSelected.pcoNum())) {
			plancoCreditRecSelected = planComptable.getSelected();
			updateData();
		}
		updateInterfaceEnabling();
	}

	private void initGUI() {
		JPanel contentPanel = new JPanel(new BorderLayout());
		contentPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		// contentPanel.setPreferredSize(WINDOW_DIMENSION);
		setContentPane(contentPanel);

		Border commonEmptyThinBorder = BorderFactory.createEmptyBorder(0, 3, 0, 3);

		labelInfo = new ZLabel("", Constants.ICON_INFO_32, SwingConstants.LEADING);
		labelInfo
				.setTextHtml("<b>S\u00E9lectionnez une commande Carambole\nPuis indiquez le fournisseur interne <u>client</u> (celui de la ligne budg\u00E9taire de l'engagement).</b>");
		labelInfo.setBackground(Color.white);
		labelInfo.setOpaque(true);

		// commande
		JPanel panelCommande = UIUtilities.labeledComponent("Commande Carambole", tfCommande, actionCommandeSelect, true);
		panelCommande.setBorder(commonEmptyThinBorder);
		JPanel panelInfos = UIUtilities.labeledComponent(null, labelInfosCommande, null);
		panelInfos.setBorder(commonEmptyThinBorder);

		// fournisseur
		JPanel panelFournis = UIUtilities.labeledComponent("Fournisseur client (= celui qui \u00E9met l'engagement)", tfFournisUlr,
				actionFournisUlrSelect, true);
		panelFournis.setBorder(commonEmptyThinBorder);

		// G\u00E9n\u00E9re facture papier ?
		checkBoxFacturePapier.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);
		checkBoxFacturePapier.setVerticalTextPosition(SwingConstants.CENTER);
		checkBoxFacturePapier.setHorizontalTextPosition(SwingConstants.TRAILING);
		checkBoxFacturePapier.setIconTextGap(0);
		checkBoxFacturePapier.setFocusPainted(false);
		checkBoxFacturePapier.setBorderPaintedFlat(true);
		JPanel panelGenereFacturePapier = UIUtilities.labeledComponent("Option", checkBoxFacturePapier, null, true);
		panelGenereFacturePapier.setBorder(commonEmptyThinBorder);

		// planco
		JPanel panelPlanco = UIUtilities.labeledComponent("Imputation recette (facultatif)", tfPlancoCreditRec, actionPlancoCreditRecSelect,
				true);
		panelPlanco.setBorder(commonEmptyThinBorder);

		Box center = Box.createVerticalBox();
		center.setBorder(BorderFactory.createEmptyBorder());
		center.add(panelCommande);
		center.add(panelInfos);
		center.add(panelFournis);
		center.add(panelGenereFacturePapier);
		// center.add(panelPlanco);

		getContentPane().add(labelInfo, BorderLayout.NORTH);
		getContentPane().add(center, BorderLayout.CENTER);
		getContentPane().add(buildBottomPanel(), BorderLayout.PAGE_END);

		initInputMap();

		this.validate();
		this.pack();
	}

	private final JPanel buildBottomPanel() {
		JPanel p = new JPanel();
		p.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.add(Box.createHorizontalGlue());
		p.add(btCancel);
		p.add(Box.createRigidArea(new Dimension(5, 0)));
		p.add(btValid);
		return p;
	}

	private void initInputMap() {
		// ((JComponent) getContentPane()).getActionMap().put("F1", new ActionHelp());
		// ((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0), "F1");

		((JComponent) getContentPane()).getActionMap().put("CTRL_Q", app.superviseur().mainMenu.actionQuit);
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
				KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK), "CTRL_Q");

		((JComponent) getContentPane()).getActionMap().put("CTRL_S", actionValid);
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
				KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK), "CTRL_S");

		((JComponent) getContentPane()).getActionMap().put("F10", actionValid);
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F10, 0), "F10");

		((JComponent) getContentPane()).getActionMap().put("ESCAPE", actionCancel);
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"ESCAPE");

		((JComponent) getContentPane()).getActionMap().put("F8", actionShowRequired);
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("F8"), "F8");

		((JComponent) getContentPane()).getActionMap().put("ALT_F8", actionUnshowRequired);
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("released F8"), "ALT_F8");
	}

	private class ActionValid extends AbstractAction {
		public ActionValid() {
			super("Valider");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onValid();
			}
		}
	}

	private class ActionCancel extends AbstractAction {
		public ActionCancel() {
			super("Annuler");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CANCEL_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onCancel();
			}
		}
	}

	private class ActionClose extends AbstractAction {
		public ActionClose() {
			super("Fermer");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CLOSE_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onClose();
			}
		}
	}

	private class ActionShowRequired extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				showRequired(true);
			}
		}
	}

	private class ActionUnshowRequired extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				showRequired(false);
			}
		}
	}

	private class ActionCommandeSelect extends AbstractAction {
		public ActionCommandeSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onCommandeSelect();
			}
		}
	}

	private class ActionFournisUlrSelect extends AbstractAction {
		public ActionFournisUlrSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onFournisUlrSelect();
			}
		}
	}

	private class ActionPlancoCreditRecSelect extends AbstractAction {
		public ActionPlancoCreditRecSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onPlancoCreditRecSelect();
			}
		}
	}

	private class ActionCheckFacturePapier extends AbstractAction {
		public ActionCheckFacturePapier() {
			super(" G\u00E9n\u00E9rer la facture interne en m\u00EAme temps que la prestation interne...");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_INTERROGER_12);
			putValue(AbstractAction.SHORT_DESCRIPTION, "G\u00E9n\u00E9rer aussi la facture interne en m\u00EAme temps que la prestation interne");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
			}
		}
	}

	private class LocalWindowListener implements WindowListener {
		public void windowActivated(WindowEvent arg0) {
		}

		public void windowClosed(WindowEvent arg0) {
		}

		public void windowClosing(WindowEvent arg0) {
			actionClose.actionPerformed(null);
		}

		public void windowDeactivated(WindowEvent arg0) {
		}

		public void windowDeiconified(WindowEvent arg0) {
		}

		public void windowIconified(WindowEvent arg0) {
		}

		public void windowOpened(WindowEvent arg0) {
		}
	}

	public void setWaitCursor(boolean bool) {
		if (bool) {
			this.getGlassPane().setVisible(true);
		}
		else {
			this.getGlassPane().setVisible(false);
		}
	}

	/**
	 * Permet de definir un panel qui sert a afficher un waitcursor et a interdire toute modif sur le panel.
	 *
	 * @author rprin
	 */
	public final class BusyGlassPanel extends JPanel {
		public final Color	COLOR_WASH	= new Color(64, 64, 64, 32);

		public BusyGlassPanel() {
			super.setOpaque(false);
			super.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

			// Suck up them events!!!
			super.addKeyListener((new KeyAdapter() {
			}));
			super.addMouseListener((new MouseAdapter() {
			}));
			super.addMouseMotionListener((new MouseMotionAdapter() {
			}));
		}

		public final void paintComponent(Graphics p_graphics) {
			Dimension l_size = super.getSize();

			// Wash the pane with translucent gray.
			p_graphics.setColor(COLOR_WASH);
			p_graphics.fillRect(0, 0, l_size.width, l_size.height);

			// Paint a grid of white/black dots.
			p_graphics.setColor(Color.white);
			for (int j = 3; j < l_size.height; j += 8) {
				for (int i = 3; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
			p_graphics.setColor(Color.black);
			for (int j = 4; j < l_size.height; j += 8) {
				for (int i = 4; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
		}
	}

}
