/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import app.client.tools.Constants;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;

public class Logs extends JFrame {

	protected ApplicationClient		app;
	protected EOEditingContext		ec;

	private static Logs				sharedInstance;

	private static final String		WINDOW_TITLE		= "Logs client/serveur";
	private static final Dimension	WINDOW_DIMENSION	= new Dimension(700, 500);

	private static final String		ONGLET1				= "Logs client";
	private static final String		ONGLET2				= "Logs serveur";

	public JTabbedPane				jtp;
	public JTextArea				jtaClientOutLog, jtaClientErrLog, jtaServerOutLog, jtaServerErrLog;

	public Logs(String windowTitle) {
		super(windowTitle);
		setGlassPane(new BusyGlassPanel());
		app = (ApplicationClient) EOApplication.sharedApplication();
		ec = app.editingContext();
		initGUI();
	}

	public static Logs sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new Logs(WINDOW_TITLE);
		}
		return sharedInstance;
	}

	public void initGUI() {
		JPanel contentPanel = new JPanel(new BorderLayout());
		contentPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		contentPanel.setPreferredSize(WINDOW_DIMENSION);
		setContentPane(contentPanel);

		jtp = new JTabbedPane();
		jtp.addChangeListener(new OngletChangeListener());
		jtp.add(ONGLET1, buildClientLogsPanel());
		jtp.add(ONGLET2, buildServerLogsPanel());
		jtp.setSize(new Dimension(getContentPane().getWidth() - 2, getContentPane().getHeight() - 2));

		getContentPane().add(jtp, BorderLayout.CENTER);
		getContentPane().add(buildBottomPanel(), BorderLayout.PAGE_END);

		setLocation(app.getMainWindow().getX() + 20, app.getMainWindow().getY() + 20);

		this.validate();
		this.pack();
	}

	public void open() {
		this.refreshLog();
		this.show();
	}

	private final JPanel buildClientLogsPanel() {
		jtaClientOutLog = new JTextArea();
		jtaClientOutLog.setEditable(false);
		jtaClientOutLog.setFont(new Font("SansSerif", Font.PLAIN, 11));
		jtaClientErrLog = new JTextArea();
		jtaClientErrLog.setEditable(false);
		jtaClientErrLog.setFont(new Font("SansSerif", Font.PLAIN, 11));

		return buildLogsPanel(jtaClientOutLog, jtaClientErrLog);
	}

	private final JPanel buildServerLogsPanel() {
		jtaServerOutLog = new JTextArea();
		jtaServerOutLog.setEditable(false);
		jtaServerOutLog.setFont(new Font("SansSerif", Font.PLAIN, 11));
		jtaServerErrLog = new JTextArea();
		jtaServerErrLog.setEditable(false);
		jtaServerErrLog.setFont(new Font("SansSerif", Font.PLAIN, 11));

		return buildLogsPanel(jtaServerOutLog, jtaServerErrLog);
	}

	private final JPanel buildLogsPanel(JTextArea out, JTextArea err) {
		JScrollPane spOut = new JScrollPane(out);
		spOut.setBorder(BorderFactory.createTitledBorder("Sortie standard"));
		spOut.setMinimumSize(new Dimension(0, 100));
		JScrollPane spErr = new JScrollPane(err);
		spErr.setBorder(BorderFactory.createTitledBorder("Sortie erreur"));
		spErr.setMinimumSize(new Dimension(0, 100));

		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, spOut, spErr);
		splitPane.setBorder(BorderFactory.createEmptyBorder());
		splitPane.setOneTouchExpandable(true);
		splitPane.setDividerSize(6);
		splitPane.setResizeWeight(0.5);

		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
		p.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		p.add(splitPane);

		return p;
	}

	private final JPanel buildBottomPanel() {
		JPanel p = new JPanel();
		p.add(new JButton(new ActionRefresh()));
		p.add(new JButton(new ActionSendLog()));
		return p;
	}

	public final class ActionRefresh extends AbstractAction {
		public ActionRefresh() {
			super("Rafraichir");
			this.putValue(AbstractAction.SMALL_ICON, Constants.ICON_REFRESH_16);
		}

		public void actionPerformed(ActionEvent e) {
			refreshLog();
		}
	}

	public final class ActionSendLog extends AbstractAction {
		public ActionSendLog() {
			super("Envoyer les logs par mail");
			this.putValue(AbstractAction.SMALL_ICON, Constants.ICON_EMAIL_16);
		}

		public void actionPerformed(ActionEvent e) {
			sendLog();
		}
	}

	public void refreshLog() {
		this.jtaClientOutLog.setText(app.clientOutLog());
		this.jtaClientErrLog.setText(app.clientErrLog());
		this.jtaServerOutLog.setText(app.serverOutLog());
		this.jtaServerErrLog.setText(app.serverErrLog());
	}

	public void sendLog() {
		String me = "pdm@univ-lr.fr";
		String clientLog = "Output log :\n\n" + jtaClientOutLog.getText() + "\n\n\nError log :\n\n" + jtaClientErrLog.getText();
		String serverLog = "Init log :\n\n" + ServerProxy.serverInitLog(ec) + "\n\n\nOutput log :\n\n" + jtaServerOutLog.getText()
				+ "\n\n\nError log :\n\n" + jtaServerErrLog.getText();
		try {
			StringBuffer mail = new StringBuffer();
			mail.append("\n\n\n\n\n\n\n\t********************  ABOUT  **********************\n\n\n\n\n\n\n");
			mail.append(app.aboutMsg());
			mail.append("\n\n\n\n\n\n\n\t*************  OUT & ERR log client  **************\n\n\n\n\n\n\n");
			mail.append(clientLog);
			mail.append("\n\n\n\n\n\n\n\t**********  INIT & OUT & ERR log server  **********\n\n\n\n\n\n\n");
			mail.append(serverLog);
			mail.append("\n\n\n\n\n\n\n\t*******************  .CONFIG  *********************\n\n\n\n\n\n\n");
			mail.append(ServerProxy.serverConfigFileContent(ec));

			ServerProxy.sendMail(ec, app.appUserInfo().email(), me, null, "Log " + ServerProxy.serverAppliId(ec), mail.toString(), null, null);

			app.showInfoDialog("Log envoy\u00E9...");
		}
		catch (Exception e) {
			System.out.println("Erreur : " + e);
		}
	}

	public class OngletChangeListener implements ChangeListener {
		public void stateChanged(ChangeEvent e) {
			// for (int i=0; i<onglets.getTabCount(); i++)
			// onglets.setBackgroundAt(i, Color.lightGray);
			// onglets.setBackgroundAt(onglets.getSelectedIndex(),new Color(186, 221, 175));
		}
	}

	public int getSelectedOnglet() {
		return jtp.getSelectedIndex();
	}

	public void setWaitCursor(boolean bool) {
		if (bool) {
			this.getGlassPane().setVisible(true);
		}
		else {
			this.getGlassPane().setVisible(false);
		}
	}

	/**
	 * Permet de definir un panel qui sert a afficher un waitcursor et a interdire toute modif sur le panel.
	 * 
	 * @author rprin
	 */
	public final class BusyGlassPanel extends JPanel {
		public final Color	COLOR_WASH	= new Color(64, 64, 64, 32);

		public BusyGlassPanel() {
			super.setOpaque(false);
			super.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

			// Suck up them events!!!
			super.addKeyListener((new KeyAdapter() {
			}));
			super.addMouseListener((new MouseAdapter() {
			}));
			super.addMouseMotionListener((new MouseMotionAdapter() {
			}));
		}

		public final void paintComponent(Graphics p_graphics) {
			Dimension l_size = super.getSize();

			// Wash the pane with translucent gray.
			p_graphics.setColor(COLOR_WASH);
			p_graphics.fillRect(0, 0, l_size.width, l_size.height);

			// Paint a grid of white/black dots.
			p_graphics.setColor(Color.white);
			for (int j = 3; j < l_size.height; j += 8) {
				for (int i = 3; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
			p_graphics.setColor(Color.black);
			for (int j = 4; j < l_size.height; j += 8) {
				for (int i = 4; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
		}
	}
}
