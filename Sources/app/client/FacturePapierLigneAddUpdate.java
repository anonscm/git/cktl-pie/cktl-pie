/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package app.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.math.BigDecimal;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.kava.client.factory.FactoryFacturePapierLigne;
import org.cocktail.kava.client.finder.FinderTva;
import org.cocktail.kava.client.finder.FinderTypeArticle;
import org.cocktail.kava.client.metier.EOFacturePapier;
import org.cocktail.kava.client.metier.EOFacturePapierLigne;
import org.cocktail.kava.client.metier.EOTva;
import org.cocktail.kava.client.metier.EOTypeArticle;

import app.client.tools.Computation;
import app.client.tools.Constants;
import app.client.ui.UIUtilities;
import app.client.ui.ZEOComboBox;
import app.client.ui.ZEOMatrix;
import app.client.ui.ZNumberField;
import app.client.ui.ZTextArea;
import app.client.ui.ZTextField;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;

public class FacturePapierLigneAddUpdate extends JDialog {

	// private static final Dimension WINDOW_DIMENSION = new Dimension(450, 350);
	private static final String					WINDOW_TITLE	= "Ligne de facture";

	protected ApplicationClient					app;
	protected EOEditingContext					ec;

	private static FacturePapierLigneAddUpdate	sharedInstance;

	private EOFacturePapierLigne				eoFacturePapierLigne;
	private EOFacturePapier						eoFacturePapierPere;
	private EOFacturePapierLigne				eoFacturePapierLignePere;

	private Action								actionClose, actionValid, actionCancel;
	private Action								actionShowRequired, actionUnshowRequired;

	private ZEOMatrix							matrixTypeArticle;

	private CbActionListener					cbActionListener;

	private ZTextArea							tfLibelle;
	private ZTextField							tfReference;
	private ZNumberField						tfHt, tfTtc;
	private ZEOComboBox							cbTva;
	private JCheckBox							cbCalculAutoHt, cbCalculAutoTtc;

	private JButton								btValid, btCancel;

	private DocumentListener					htDocumentListener, ttcDocumentListener;

	private boolean								result;

	public FacturePapierLigneAddUpdate() {
		super((JFrame) null, WINDOW_TITLE, true);

		app = (ApplicationClient) EOApplication.sharedApplication();
		ec = app.editingContext();

		setGlassPane(new BusyGlassPanel());
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new LocalWindowListener());

		actionClose = new ActionClose();
		actionValid = new ActionValid();
		actionCancel = new ActionCancel();
		actionShowRequired = new ActionShowRequired();
		actionUnshowRequired = new ActionUnshowRequired();

		cbActionListener = new CbActionListener();

		matrixTypeArticle = new ZEOMatrix(FinderTypeArticle.findAll(ec), EOTypeArticle.TYAR_LIBELLE_KEY, null, null, SwingConstants.HORIZONTAL);

		tfLibelle = new ZTextArea(4, 60);
		tfReference = new ZTextField(40);

		htDocumentListener = new PrixDocumentListener();
		ttcDocumentListener = new PrixDocumentListener();
		tfHt = new ZNumberField(10, app.getCurrencyPrecisionFormatEdit());
		tfHt.getDocument().addDocumentListener(htDocumentListener);
		tfTtc = new ZNumberField(10, app.getCurrencyPrecisionFormatEdit());

		cbTva = new ZEOComboBox(FinderTva.find(ec), EOTva.TVA_TAUX_KEY, "", Constants.FORMAT_DECIMAL_DISPLAY, null, 80);
		cbTva.addActionListener(cbActionListener);

		cbCalculAutoHt = new JCheckBox(new ActionCheckCalculAutoHT());
		cbCalculAutoTtc = new JCheckBox(new ActionCheckCalculAutoTTC());

		btCancel = new JButton(actionCancel);
		btValid = new JButton(actionValid);

		initGUI();

		cbCalculAutoTtc.setSelected(true);

		setEditable(true, false);
	}

	public static FacturePapierLigneAddUpdate sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new FacturePapierLigneAddUpdate();
		}
		return sharedInstance;
	}

	/**
	 * Cr\u00E9ation d'une nouvelle ligne de type article
	 *
	 * @param facturePapier
	 * @return
	 */
	public boolean openNew(EOFacturePapier facturePapier) {
		if (facturePapier == null) {
			System.err.println("[FacturePapierLigneAddUpdate:open(EOFacturePapier)] Aucuna facture papier pass\u00E9e pour ouvrir en cr\u00E9ation !");
			return false;
		}

		setTitle(WINDOW_TITLE + " - Cr\u00E9ation");
		setEditable(true, false);

		this.eoFacturePapierLigne = null;
		this.eoFacturePapierPere = facturePapier;
		this.eoFacturePapierLignePere = null;
		cleanData();
		matrixTypeArticle.selectRadioButtonByEo(FinderTypeArticle.typeArticleArticle(ec));
		matrixTypeArticle.setEnabled(false);

		return open();
	}

	/**
	 * Cr\u00E9ation d'une nouvelle ligne de type option ou remise
	 *
	 * @param facturePapierLigne
	 *            La ligne p\u00E9re...
	 * @return
	 */
	public boolean openNew(EOFacturePapierLigne facturePapierLigne) {
		if (facturePapierLigne == null) {
			System.err.println("[FacturePapierLigneAddUpdate:open(EOFacturePapierLigne)] Aucune ligne pass\u00E9e pour ouvrir en cr\u00E9ation !");
			return false;
		}

		setTitle(WINDOW_TITLE + " - Cr\u00E9ation");
		setEditable(true, false);

		this.eoFacturePapierLigne = null;
		this.eoFacturePapierPere = null;
		this.eoFacturePapierLignePere = facturePapierLigne;
		cleanData();
		matrixTypeArticle.selectRadioButtonByEo(FinderTypeArticle.typeArticleOption(ec));
		matrixTypeArticle.setEnabled(true);
		matrixTypeArticle.getRadioButtonByEo(FinderTypeArticle.typeArticleArticle(ec)).setEnabled(false);

		cbTva.setSelectedEOObject(facturePapierLigne.tva());

		return open();
	}

	public boolean open(EOFacturePapierLigne facturePapierLigne) {
		if (facturePapierLigne == null) {
			System.err.println("[FacturePapierLigneAddUpdate:open(EOFacturePapierLigne)] Aucune ligne pass\u00E9e pour ouvrir en modif !");
			return false;
		}

		setTitle(WINDOW_TITLE + " - Modification");
		setEditable(true, true);

		this.eoFacturePapierLigne = facturePapierLigne;
		this.eoFacturePapierPere = null;
		this.eoFacturePapierLignePere = null;
		updateData();

		return open();
	}

	public boolean canQuit() {
		if (isShowing()) {
			actionCancel.actionPerformed(null);
		}
		return !isShowing();
	}

	private boolean open() {
		result = false;
		int screenWidth = (int) this.getGraphicsConfiguration().getBounds().getWidth();
		int screenHeight = (int) this.getGraphicsConfiguration().getBounds().getHeight();
		this.setLocation((screenWidth / 2) - ((int) this.getSize().getWidth() / 2),
				((screenHeight / 2) - ((int) this.getSize().getHeight() / 2)));
		show();

		return result;
	}

	private void onValid() {
		try {
			if (tfLibelle.getText() == null) {
				throw new Exception("Il faut un libell\u00E9 pour les lignes de la facture papier!");
			}
			if (tfHt.getBigDecimal() == null) {
				throw new Exception("Il faut un prix HT pour les lignes de la facture papier!");
			}
			if (tfTtc.getBigDecimal() == null) {
				throw new Exception("Il faut un prix TTC pour les lignes de la facture papier!");
			}
			if (tfHt.getBigDecimal().abs().compareTo(tfTtc.getBigDecimal().abs()) == 1) {
				throw new Exception("Le prix HT ne peut \u00EAtre sup\u00E9rieur au prix TTC!");
			}

			if (eoFacturePapierLigne == null) {
				if (eoFacturePapierLignePere != null) {
					eoFacturePapierLigne = FactoryFacturePapierLigne.newObject(ec, eoFacturePapierLignePere);
					eoFacturePapierLigne.companion().modifierQuantite(eoFacturePapierLignePere.fligQuantite());
				}
				else {
					eoFacturePapierLigne = FactoryFacturePapierLigne.newObject(ec, eoFacturePapierPere);
					eoFacturePapierLigne.companion().modifierQuantite(BigDecimal.ONE);
				}
			}
			eoFacturePapierLigne.setTypeArticleRelationship((EOTypeArticle) matrixTypeArticle.getSelectedEOObject());

			eoFacturePapierLigne.setFligDescription(tfLibelle.getText());
			eoFacturePapierLigne.setFligReference(tfReference.getText());
			eoFacturePapierLigne.setFligArtHt(tfHt.getBigDecimal());
			eoFacturePapierLigne.setFligArtTtc(tfTtc.getBigDecimal());
			eoFacturePapierLigne.setFligArtTtcInitial(eoFacturePapierLigne.fligArtTtc());

			eoFacturePapierLigne.setTvaRelationship((EOTva) cbTva.getSelectedEOObject());
			eoFacturePapierLigne.setTvaInitialRelationship(eoFacturePapierLigne.tva());

			if (eoFacturePapierLigne.typeArticle().equals(FinderTypeArticle.typeArticleRemise(ec))) {
				// verif que les montants soient negatifs
				if (eoFacturePapierLigne.fligArtHt() != null && eoFacturePapierLigne.fligArtTtc() != null) {
					if (eoFacturePapierLigne.fligArtHt().signum() == 1 || eoFacturePapierLigne.fligArtTtc().signum() == 1) {
						if (!app.showConfirmationDialog("ATTENTION",
								"Pour une remise, les montants doivent \u00EAtre n\u00E9gatifs... convertir en n\u00E9gatif?", "OUI", "NON")) {
							return;
						}
						if (eoFacturePapierLigne.fligArtHt().signum() == 1) {
							eoFacturePapierLigne.setFligArtHt(eoFacturePapierLigne.fligArtHt().negate());
						}
						if (eoFacturePapierLigne.fligArtTtc().signum() == 1) {
							eoFacturePapierLigne.setFligArtTtc(eoFacturePapierLigne.fligArtTtc().negate());
							eoFacturePapierLigne.setFligArtTtcInitial(eoFacturePapierLigne.fligArtTtc());
						}
					}
				}
			}
			else {
				// v\u00E9rif que les montants soient positifs
				if (eoFacturePapierLigne.fligArtHt() != null && eoFacturePapierLigne.fligArtTtc() != null) {
					if (eoFacturePapierLigne.fligArtHt().signum() == -1 || eoFacturePapierLigne.fligArtTtc().signum() == -1) {
						if (!app
								.showConfirmationDialog("ATTENTION", "Les montants doivent \u00EAtre positifs... convertir en positif?", "OUI", "NON")) {
							return;
						}
						if (eoFacturePapierLigne.fligArtHt().signum() == -1) {
							eoFacturePapierLigne.setFligArtHt(eoFacturePapierLigne.fligArtHt().negate());
						}
						if (eoFacturePapierLigne.fligArtTtc().signum() == -1) {
							eoFacturePapierLigne.setFligArtTtc(eoFacturePapierLigne.fligArtTtc().negate());
							eoFacturePapierLigne.setFligArtTtcInitial(eoFacturePapierLigne.fligArtTtc());
						}
					}
				}
			}

			// v\u00E9rif si on applique la tva
			if (eoFacturePapierLigne.facturePapier() != null && "N".equalsIgnoreCase(eoFacturePapierLigne.facturePapier().fapApplyTva())) {
				eoFacturePapierLigne.setTvaRelationship(null);
				eoFacturePapierLigne.setFligArtTtc(eoFacturePapierLigne.fligArtHt());
			}

			result = true;
			hide();
		}
		catch (Exception e) {
			app.showErrorDialog(e);
		}
	}

	private void onCancel() {
		if (app.showConfirmationDialog("ATTENTION", "Annuler???", "OUI!", "NON")) {
			result = false;
			hide();
		}
	}

	private void onClose() {
		actionCancel.actionPerformed(null);
	}

	private void updateMontants() {
		if (cbCalculAutoTtc.isSelected()) {
			if (tfHt.getNumber() == null) {
				tfTtc.setNumber(null);
				return;
			}
			if (cbTva.getSelectedEOObject() == null) {
				tfTtc.setNumber(tfHt.getNumber());
				return;
			}
			tfTtc.setNumber(Computation.getTtc(tfHt.getBigDecimal(), (EOTva) cbTva.getSelectedEOObject()));
		}
		else {
			if (cbCalculAutoHt.isSelected()) {
				if (tfTtc.getNumber() == null) {
					tfHt.setNumber(null);
					return;
				}
				if (cbTva.getSelectedEOObject() == null) {
					tfHt.setNumber(tfTtc.getNumber());
					return;
				}
				tfHt.setNumber(Computation.getHt(tfTtc.getBigDecimal(), (EOTva) cbTva.getSelectedEOObject()));
			}
		}
	}

	private void updateData() {
		cleanData();
		if (eoFacturePapierLigne == null) {
			return;
		}

		matrixTypeArticle.selectRadioButtonByEo(eoFacturePapierLigne.typeArticle());

		tfLibelle.setText(eoFacturePapierLigne.fligDescription());
		tfReference.setText(eoFacturePapierLigne.fligReference());
		tfHt.setNumber(eoFacturePapierLigne.fligArtHt());
		tfTtc.setNumber(eoFacturePapierLigne.fligArtTtc());

		cbTva.removeActionListener(cbActionListener);
		if (eoFacturePapierLigne.tva() != null) {
			cbTva.setSelectedEOObject(eoFacturePapierLigne.tva());
		}
		else {
			cbTva.clean();
		}
		cbTva.addActionListener(cbActionListener);

		// active/desactive ce qui va bien en fct du type d'article...
		EOEnterpriseObject eo = FinderTypeArticle.typeArticleArticle(ec);
		if (eoFacturePapierLigne.typeArticle().equals(eo)) {
			matrixTypeArticle.setEnabled(false);
		}
		else {
			matrixTypeArticle.setEnabled(true);
			matrixTypeArticle.getRadioButtonByEo(eo).setEnabled(false);
		}
	}

	private void setEditable(boolean editable, boolean forUpdate) {

		matrixTypeArticle.setEnabled(editable);

		tfLibelle.setEditable(editable);
		tfReference.setEditable(editable);
		tfHt.setEditable(editable && !cbCalculAutoHt.isSelected());
		tfTtc.setEditable(editable & !cbCalculAutoTtc.isSelected());

		cbTva.setEnabled(editable);

		actionCancel.setEnabled(editable);
		actionValid.setEnabled(editable);
		actionClose.setEnabled(!editable);
	}

	private void cleanData() {
		matrixTypeArticle.clean();
		tfLibelle.clean();
		tfReference.clean();
		tfHt.clean();
		tfTtc.setText(null);
		cbTva.clean();
	}

	private void showRequired(boolean show) {
		matrixTypeArticle.showRequired(show);
		tfLibelle.showRequired(show);
		tfReference.showRequired(show);
		tfHt.showRequired(show);
		tfTtc.showRequired(show);
		// cbTva.showRequired(show);
	}

	private void initGUI() {
		JPanel contentPanel = new JPanel(new BorderLayout());
		contentPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		// contentPanel.setPreferredSize(WINDOW_DIMENSION);
		setContentPane(contentPanel);

		// Border commonEmptyThickBorder = BorderFactory.createEmptyBorder(12, 3, 12, 3);
		Border commonEmptyThinBorder = BorderFactory.createEmptyBorder(0, 3, 0, 3);

		// type
		JPanel panelTypeArticle = UIUtilities.labeledComponent("Type d'article", matrixTypeArticle, null, true);

		// prix
		JPanel panelTemp2 = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		panelTemp2.setBorder(commonEmptyThinBorder);

		cbCalculAutoHt.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);
		cbCalculAutoHt.setVerticalTextPosition(SwingConstants.TOP);
		cbCalculAutoHt.setHorizontalTextPosition(SwingConstants.CENTER);
		cbCalculAutoHt.setIconTextGap(0);
		cbCalculAutoHt.setFocusPainted(false);
		cbCalculAutoHt.setBorderPaintedFlat(true);

		cbCalculAutoTtc.setForeground(Constants.COLOR_LABEL_FGD_LIGHT);
		cbCalculAutoTtc.setVerticalTextPosition(SwingConstants.TOP);
		cbCalculAutoTtc.setHorizontalTextPosition(SwingConstants.CENTER);
		cbCalculAutoTtc.setIconTextGap(0);
		cbCalculAutoTtc.setFocusPainted(false);
		cbCalculAutoTtc.setBorderPaintedFlat(true);

		panelTemp2.add(UIUtilities.labeledComponent("HT", tfHt, null));
		panelTemp2.add(UIUtilities.labeledComponent(" ", cbCalculAutoHt, null));
		panelTemp2.add(Box.createHorizontalStrut(20));
		panelTemp2.add(UIUtilities.labeledComponent("Tva", cbTva, null));
		panelTemp2.add(Box.createHorizontalStrut(20));
		panelTemp2.add(UIUtilities.labeledComponent("TTC", tfTtc, null));
		panelTemp2.add(UIUtilities.labeledComponent(" ", cbCalculAutoTtc, null));

		JPanel panelPrix = UIUtilities.labeledComponent("Tarif", panelTemp2, null, true);

		// libell\u00E9
		JPanel panelLibelle = UIUtilities.labeledComponent("Description", tfLibelle, null);
		panelLibelle.setBorder(commonEmptyThinBorder);

		// r\u00E9f\u00E9rence
		JPanel panelReference = UIUtilities.labeledComponent("R\u00E9f\u00E9rence", tfReference, null);
		panelReference.setBorder(commonEmptyThinBorder);

		Box center = Box.createVerticalBox();
		center.setBorder(BorderFactory.createEmptyBorder());

		center.add(panelTypeArticle);
		center.add(panelPrix);
		center.add(panelLibelle);
		center.add(panelReference);

		getContentPane().add(center, BorderLayout.CENTER);
		getContentPane().add(buildBottomPanel(), BorderLayout.PAGE_END);

		initInputMap();

		this.validate();
		this.pack();
	}

	private final JPanel buildBottomPanel() {
		JPanel p = new JPanel();
		p.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.add(Box.createHorizontalGlue());
		p.add(btCancel);
		p.add(Box.createRigidArea(new Dimension(5, 0)));
		p.add(btValid);
		return p;
	}

	private void initInputMap() {
		((JComponent) getContentPane()).getActionMap().put("CTRL_Q", app.superviseur().mainMenu.actionQuit);
		((JComponent) getContentPane()).getActionMap().put("CTRL_S", actionValid);
		((JComponent) getContentPane()).getActionMap().put("F10", actionValid);
		((JComponent) getContentPane()).getActionMap().put("ESCAPE", actionCancel);
		((JComponent) getContentPane()).getActionMap().put("F8", actionShowRequired);
		((JComponent) getContentPane()).getActionMap().put("ALT_F8", actionUnshowRequired);
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
				KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK), "CTRL_Q");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
				KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK), "CTRL_S");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F10, 0), "F10");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"ESCAPE");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("F8"), "F8");
		((JComponent) getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("released F8"), "ALT_F8");
	}

	private class CbActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JComboBox cb = (JComboBox) e.getSource();
			if (cb == cbTva) {
				updateMontants();
			}
		}
	}

	private class PrixDocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent arg0) {
			updateMontants();
		}

		public void insertUpdate(DocumentEvent arg0) {
			updateMontants();
		}

		public void removeUpdate(DocumentEvent arg0) {
			updateMontants();
		}
	}

	private class ActionCheckCalculAutoHT extends AbstractAction {
		public ActionCheckCalculAutoHT() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CALCULER_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Calculer automatiquement le HT depuis le TTC");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				if (cbCalculAutoHt.isSelected()) {
					tfHt.setEditable(false);
					tfTtc.getDocument().addDocumentListener(ttcDocumentListener);
					if (cbCalculAutoTtc.isSelected()) {
						cbCalculAutoTtc.setSelected(false);
						tfTtc.setEditable(true);
						tfHt.getDocument().removeDocumentListener(htDocumentListener);
					}
					updateMontants();
				}
				else {
					tfHt.setEditable(true);
					tfTtc.getDocument().removeDocumentListener(ttcDocumentListener);
				}
			}
		}
	}

	private class ActionCheckCalculAutoTTC extends AbstractAction {
		public ActionCheckCalculAutoTTC() {
			super();
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CALCULER_16);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Calculer automatiquement le TTC depuis le HT");
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				if (cbCalculAutoTtc.isSelected()) {
					tfTtc.setEditable(false);
					tfHt.getDocument().addDocumentListener(htDocumentListener);
					if (cbCalculAutoHt.isSelected()) {
						cbCalculAutoHt.setSelected(false);
						tfHt.setEditable(false);
						tfTtc.getDocument().removeDocumentListener(ttcDocumentListener);
					}
					updateMontants();
				}
				else {
					tfTtc.setEditable(true);
					tfHt.getDocument().removeDocumentListener(htDocumentListener);
				}
			}
		}
	}

	private class ActionValid extends AbstractAction {
		public ActionValid() {
			super("Valider");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_VALID_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onValid();
			}
		}
	}

	private class ActionCancel extends AbstractAction {
		public ActionCancel() {
			super("Annuler");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CANCEL_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onCancel();
			}
		}
	}

	private class ActionClose extends AbstractAction {
		public ActionClose() {
			super("Fermer");
			putValue(AbstractAction.SMALL_ICON, Constants.ICON_CLOSE_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				onClose();
			}
		}
	}

	private class ActionShowRequired extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				showRequired(true);
			}
		}
	}

	private class ActionUnshowRequired extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			if (isEnabled()) {
				showRequired(false);
			}
		}
	}

	private class LocalWindowListener implements WindowListener {
		public void windowActivated(WindowEvent arg0) {
		}

		public void windowClosed(WindowEvent arg0) {
		}

		public void windowClosing(WindowEvent arg0) {
			actionClose.actionPerformed(null);
		}

		public void windowDeactivated(WindowEvent arg0) {
		}

		public void windowDeiconified(WindowEvent arg0) {
		}

		public void windowIconified(WindowEvent arg0) {
		}

		public void windowOpened(WindowEvent arg0) {
		}
	}

	public void setWaitCursor(boolean bool) {
		if (bool) {
			this.getGlassPane().setVisible(true);
		}
		else {
			this.getGlassPane().setVisible(false);
		}
	}

	/**
	 * Permet de d\u00E9finir un panel qui sert a afficher un waitcursor et a interdire toute modif sur le panel.
	 *
	 * @author rprin
	 */
	public final class BusyGlassPanel extends JPanel {
		public final Color	COLOR_WASH	= new Color(64, 64, 64, 32);

		public BusyGlassPanel() {
			super.setOpaque(false);
			super.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

			// Suck up them events!!!
			super.addKeyListener((new KeyAdapter() {
			}));
			super.addMouseListener((new MouseAdapter() {
			}));
			super.addMouseMotionListener((new MouseMotionAdapter() {
			}));
		}

		public final void paintComponent(Graphics p_graphics) {
			Dimension l_size = super.getSize();

			// Wash the pane with translucent gray.
			p_graphics.setColor(COLOR_WASH);
			p_graphics.fillRect(0, 0, l_size.width, l_size.height);

			// Paint a grid of white/black dots.
			p_graphics.setColor(Color.white);
			for (int j = 3; j < l_size.height; j += 8) {
				for (int i = 3; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
			p_graphics.setColor(Color.black);
			for (int j = 4; j < l_size.height; j += 8) {
				for (int i = 4; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
		}
	}

}
