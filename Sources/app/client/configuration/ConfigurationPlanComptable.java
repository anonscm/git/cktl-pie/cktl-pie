/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */
package app.client.configuration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.cocktail.application.client.eof.EOExercice;

import app.client.ApplicationClient;

/**
 * Classe gérant la configuration applicative liee aux plans comptables.
 *
 * @author flagouey
 */
public class ConfigurationPlanComptable implements Serializable {

	/** Serial version Id. */
	private static final long serialVersionUID = 1L;

	/** Parametre gerant les classes bloquees. */
	public static final String PARAM_PLAN_COMPTABLE_CTP_CLASSES_INTERDITES = "PCO_CTP_CLASSE_SAISIE_BLOQUEE";

	/** Separateur utilise pour le parametre de gestion des classes interdites. */
	public static final String SEPARATOR_PLAN_COMPTABLE_CLASSES_INTERDITES = ",";

	/** Singleton.
	 * @return singleton instance.
	 */
	public static final ConfigurationPlanComptable instance() {
		return INSTANCE;
	}

	/** Singleton instance. */
	private static final ConfigurationPlanComptable INSTANCE = new ConfigurationPlanComptable();

	/** Liste des classes interdites en contrepartie par exercice (cache). */
	private Map<Number, List<String>> classesInterditesContrepartieParExercice;

	/**
	 * Constructeur privé.
	 */
	private ConfigurationPlanComptable() {
		this.classesInterditesContrepartieParExercice = new Hashtable<Number, List<String>>();
	}

	/**
	 * Vide les valeurs mises en cache.
	 */
	public void clearValues() {
		this.classesInterditesContrepartieParExercice.clear();
	}

	/**
	 * Retourne la liste des classes interdites en contrepartie (String).
	 * @param applicationClient application client.
	 * @return la liste des classes interdites en contrepartie.
	 */
	public List<String> getClassesInterditesContrepartie(ApplicationClient applicationClient) {
		EOExercice exercice = applicationClient.getCurrentExercice();
		Number exerciceAsNumber = exercice.exeExercice();
		if (this.classesInterditesContrepartieParExercice.containsKey(exerciceAsNumber)) {
			return this.classesInterditesContrepartieParExercice.get(exerciceAsNumber);
		}
		List<String> classesInterdites = new ArrayList<String>();
		String paramClassesInterdites = applicationClient.getParam(
				PARAM_PLAN_COMPTABLE_CTP_CLASSES_INTERDITES, exercice);
		if (paramClassesInterdites != null && paramClassesInterdites.length() > 0) {
			String[] tableauClassesIntrdites =
					paramClassesInterdites.split(SEPARATOR_PLAN_COMPTABLE_CLASSES_INTERDITES);
			for (String classeInterdite : tableauClassesIntrdites) {
				if (classeInterdite != null && classeInterdite.trim().length() > 0) {
					classesInterdites.add(classeInterdite.trim());
				}
			}
		}
		this.classesInterditesContrepartieParExercice.put(exerciceAsNumber, classesInterdites);
		return classesInterdites;
	}
}
