/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */
package app.client.configuration;

import java.awt.Color;

import app.client.ApplicationClient;

/**
 * Classe gérant la configuration applicative liee aux couleurs.
 *
 * @author flagouey
 */
public class ConfigurationCouleur {

	/** ---- ONGLET PRESTATION ----. */
	/** Parametre de configuration : couleur prestation archivee. */
	public static final String PARAM_COULEUR_TXT_PRESTATION_ARCHIVEE = "PRESTATION.COLOR.TEXT.ARCHIVEE";
	/** Parametre de configuration : couleur prestation non valide client. */
	public static final String PARAM_COULEUR_TXT_PRESTATION_NON_VALIDE_CLIENT =
			"PRESTATION.COLOR.TEXT.NON_VALIDE_CLIENT";
	/** Parametre de configuration : couleur prestation valide client. */
	public static final String PARAM_COULEUR_TXT_PRESTATION_VALIDE_CLIENT = "PRESTATION.COLOR.TXT.VALIDE_CLIENT";
	/** Parametre de configuration : couleur prestation tout valide. */
	public static final String PARAM_COULEUR_TXT_PRESTATION_VALIDE = "PRESTATION.COLOR.TEXT.VALIDE";
	/** Parametre de configuration : couleur prestation facturee. */
	public static final String PARAM_COULEUR_BG_PRESTATION_FACTUREE = "PRESTATION.COLOR.BG.FACTUREE";

	/** Prestation archivee : couleur par defaut. */
	public static final Color COLOR_FGD_PRESTATION_ARCHIVE_DEFAULT = Color.gray;
	/** Prestation non valide client : couleur par defaut. */
	public static final Color COLOR_FGD_PRESTATION_NON_VALIDE_CLIENT_DEFAULT = Color.red;
	/** Prestation valide client : couleur par defaut. */
	public static final Color COLOR_FGD_PRESTATION_VALIDE_CLIENT_DEFAULT = Color.decode("#000077");
	/** Prestation valide : couleur par defaut. */
	public static final Color COLOR_FGD_PRESTATION_TOUT_VALIDE_DEFAULT = Color.decode("#009900");
	/** Prestation facturee : couleur par defaut. */
	public static final Color COLOR_BGD_PRESTATION_FACTUREE_DEFAULT = Color.decode("#FEFFC3");

	/** ---- ONGLET FACTURE ----. */
	/** Parametre de configuration : couleur facture non valide client. */
	public static final String PARAM_COULEUR_TXT_FACTURE_NON_VALIDE_CLIENT = "FACTURE.COLOR.TEXT.NON_VALIDE_CLIENT";
	/** Parametre de configuration : couleur facture valide client. */
	public static final String PARAM_COULEUR_TXT_FACTURE_VALIDE_CLIENT = "FACTURE.COLOR.TEXT.VALIDE_CLIENT";
	/** Parametre de configuration : couleur facture valide prestataire. */
	public static final String PARAM_COULEUR_BG_FACTURE_VALIDE_PREST = "FACTURE.COLOR.BG.VALIDE_PRESTATAIRE";

	/** Facture non valide client : couleur par defaut. */
	public static final Color COLOR_FGD_FACTURE_NON_VALIDE_CLIENT_DEFAULT = Color.red;
	/** Facture valide client : couleur par defaut. */
	public static final Color COLOR_FGD_FACTURE_VALIDE_CLIENT_DEFAULT = Color.decode("#007700");
	/** Facture valide prestataire : couleur par defaut. */
	public static final Color COLOR_BGD_FACTURE_VALIDE_PREST_DEFAULT = Color.decode("#FEFFC3");

	/** ---- ONGLET RECETTE ----. */
	/** Parametre de configuration : couleur recette non titree. */
	public static final String PARAM_COULEUR_TXT_RECETTE_NON_TITREE = "RECETTE.COLOR.TEXT.NON_TITREE";
	/** Parametre de configuration : couleur recette titree. */
	public static final String PARAM_COULEUR_TXT_RECETTE_TITREE = "RECETTE.COLOR.TEXT.TITREE";
	/** Parametre de configuration : couleur reduction non titree. */
	public static final String PARAM_COULEUR_TXT_REDUCTION_NON_TITREE = "REDUCTION.COLOR.TEXT.NON_TITREE";
	/** Parametre de configuration : couleur recette titree. */
	public static final String PARAM_COULEUR_TXT_REDUCTION_TITREE = "REDUCTION.COLOR.TEXT.TITREE";

	/** Recette non titree : couleur par defaut. */
	public static final Color COLOR_FGD_RECETTE_NON_TITREE_DEFAULT = Color.red;
	/** Recette titree : couleur par defaut. */
	public static final Color COLOR_FGD_RECETTE_TITREE_DEFAULT = Color.decode("#007700");
	/** Reduction non titree : couleur par defaut. */
	public static final Color COLOR_FGD_REDUCTION_NON_TITREE_DEFAULT = Color.decode("#FF8888");
	/** Reduction titree : culeur par defaut. */
	public static final Color COLOR_FGD_REDUCTION_TITREE_DEFAULT = Color.decode("#00BB00");

	/** Parametre gerant les classes bloquees. */
	public static final String PARAM_PLAN_COMPTABLE_CLASSES_INTERDITES = "PCOCLASSE_SAISIE_BLOQUEE";

	/** Separateur utilise pour le parametre de gestion des classes interdites. */
	public static final String SEPARATOR_PLAN_COMPTABLE_CLASSES_INTERDITES = ",";

	/**
	 * Singleton.
	 * @return singleton instance.
	 */
	public static final ConfigurationCouleur instance() {
		return INSTANCE;
	}

	/** Singleton instance. */
	private static final ConfigurationCouleur INSTANCE = new ConfigurationCouleur();

	/** Couleur cache : prestation archivee. */
	private Color prestationArchiveeForegroundCache;
	/** Couleur cache : prestation non valide client. */
	private Color prestationNonValideClientForegroundCache;
	/** Couleur cache : prestation valide client. */
	private Color prestationValideClientForegroundCache;
	/** Couleur cache : prestation valide. */
	private Color prestationValideForegroundCache;
	/** Couleur cache : prestation facturee. */
	private Color prestationFactureeBackgroundCache;

	/** Couleur cache : facture non valide client. */
	private Color factureNonValideClientForegroundCache;
	/** Couleur cache : facture valide client. */
	private Color factureValideClientForegroundCache;
	/** Couleur cache : facture valide prestataire. */
	private Color factureValidePrestataireBackgroundCache;

	/** Couleur Cache : recette non titree. */
	private Color recetteNonTitreeForegroundCache;
	/** Couleur cache : recette titree. */
	private Color recetteTitreeForegroundCache;
	/** Couleur cache : reduction non titree. */
	private Color reductionNonTitreeForegroundCache;
	/** Couleur cache : reduction titree. */
	private Color reductionTitreeForegroundCache;

	/**
	 * Constructeur privé.
	 */
	private ConfigurationCouleur() {
		clearValues();
	}

	public void clearValues() {
		this.prestationArchiveeForegroundCache = null;
		this.prestationNonValideClientForegroundCache = null;
		this.prestationValideClientForegroundCache = null;
		this.prestationValideForegroundCache = null;
		this.prestationFactureeBackgroundCache = null;

		this.factureNonValideClientForegroundCache = null;
		this.factureValideClientForegroundCache = null;
		this.factureValidePrestataireBackgroundCache = null;

		this.recetteNonTitreeForegroundCache = null;
		this.recetteTitreeForegroundCache = null;
		this.reductionNonTitreeForegroundCache = null;
		this.reductionTitreeForegroundCache = null;
	}

	public Color prestationArchiveeForeground(ApplicationClient appClient) {
		if (this.prestationArchiveeForegroundCache != null) {
			return this.prestationArchiveeForegroundCache;
		}

		this.prestationArchiveeForegroundCache = chooseColor(
				appClient.getParam(PARAM_COULEUR_TXT_PRESTATION_ARCHIVEE),
				COLOR_FGD_PRESTATION_ARCHIVE_DEFAULT);
		return this.prestationArchiveeForegroundCache;
	}

	public Color prestationNonValideClientForeground(ApplicationClient appClient) {
		if (this.prestationNonValideClientForegroundCache != null) {
			return this.prestationNonValideClientForegroundCache;
		}

		this.prestationNonValideClientForegroundCache = chooseColor(
				appClient.getParam(PARAM_COULEUR_TXT_PRESTATION_NON_VALIDE_CLIENT),
				COLOR_FGD_PRESTATION_NON_VALIDE_CLIENT_DEFAULT);
		return this.prestationNonValideClientForegroundCache;
	}

	public Color prestationValideClientForeground(ApplicationClient appClient) {
		if (this.prestationValideClientForegroundCache != null) {
			return this.prestationValideClientForegroundCache;
		}

		this.prestationValideClientForegroundCache = chooseColor(
				appClient.getParam(PARAM_COULEUR_TXT_PRESTATION_VALIDE_CLIENT),
				COLOR_FGD_PRESTATION_VALIDE_CLIENT_DEFAULT);
		return this.prestationValideClientForegroundCache;
	}

	public Color prestationValideForeground(ApplicationClient appClient) {
		if (this.prestationValideForegroundCache != null) {
			return this.prestationValideForegroundCache;
		}

		this.prestationValideForegroundCache = chooseColor(
				appClient.getParam(PARAM_COULEUR_TXT_PRESTATION_VALIDE),
				COLOR_FGD_PRESTATION_TOUT_VALIDE_DEFAULT);
		return this.prestationValideForegroundCache;
	}

	public Color prestationFactureeBackground(ApplicationClient appClient) {
		if (this.prestationFactureeBackgroundCache != null) {
			return this.prestationFactureeBackgroundCache;
		}

		this.prestationFactureeBackgroundCache = chooseColor(
				appClient.getParam(PARAM_COULEUR_BG_PRESTATION_FACTUREE),
				COLOR_BGD_PRESTATION_FACTUREE_DEFAULT);
		return this.prestationFactureeBackgroundCache;
	}

	public Color factureNonValideClientForeground(ApplicationClient appClient) {
		if (this.factureNonValideClientForegroundCache != null) {
			return this.factureNonValideClientForegroundCache;
		}

		this.factureNonValideClientForegroundCache = chooseColor(
				appClient.getParam(PARAM_COULEUR_TXT_FACTURE_NON_VALIDE_CLIENT),
				COLOR_FGD_FACTURE_NON_VALIDE_CLIENT_DEFAULT);
		return this.factureNonValideClientForegroundCache;
	}

	public Color factureValideClientForeground(ApplicationClient appClient) {
		if (this.factureValideClientForegroundCache != null) {
			return this.factureValideClientForegroundCache;
		}

		this.factureValideClientForegroundCache = chooseColor(
				appClient.getParam(PARAM_COULEUR_TXT_FACTURE_VALIDE_CLIENT),
				COLOR_FGD_FACTURE_VALIDE_CLIENT_DEFAULT);
		return this.factureValideClientForegroundCache;
	}

	public Color factureValidePrestataireBackground(ApplicationClient appClient) {
		if (this.factureValidePrestataireBackgroundCache != null) {
			return this.factureValidePrestataireBackgroundCache;
		}

		this.factureValidePrestataireBackgroundCache = chooseColor(
				appClient.getParam(PARAM_COULEUR_BG_FACTURE_VALIDE_PREST),
				COLOR_BGD_FACTURE_VALIDE_PREST_DEFAULT);
		return this.factureValidePrestataireBackgroundCache;

	}

	public Color recetteNonTitreeForeground(ApplicationClient appClient) {
		if (this.recetteNonTitreeForegroundCache != null) {
			return this.recetteNonTitreeForegroundCache;
		}

		this.recetteNonTitreeForegroundCache = chooseColor(
				appClient.getParam(PARAM_COULEUR_TXT_RECETTE_NON_TITREE), COLOR_FGD_RECETTE_NON_TITREE_DEFAULT);
		return this.recetteNonTitreeForegroundCache;
	}

	public Color recetteTitreeForeground(ApplicationClient appClient) {
		if (this.recetteTitreeForegroundCache != null) {
			return this.recetteTitreeForegroundCache;
		}

		this.recetteTitreeForegroundCache = chooseColor(
				appClient.getParam(PARAM_COULEUR_TXT_RECETTE_TITREE), COLOR_FGD_RECETTE_TITREE_DEFAULT);
		return this.recetteTitreeForegroundCache;
	}

	public Color reductionNonTitreeForeground(ApplicationClient appClient) {
		if (this.reductionNonTitreeForegroundCache != null) {
			return this.reductionNonTitreeForegroundCache;
		}

		this.reductionNonTitreeForegroundCache = chooseColor(
				appClient.getParam(PARAM_COULEUR_TXT_REDUCTION_NON_TITREE), COLOR_FGD_REDUCTION_NON_TITREE_DEFAULT);
		return this.reductionNonTitreeForegroundCache;
	}

	public Color reductionTitreeForeground(ApplicationClient appClient) {
		if (this.reductionTitreeForegroundCache != null) {
			return this.reductionTitreeForegroundCache;
		}
		this.reductionTitreeForegroundCache = chooseColor(
				appClient.getParam(PARAM_COULEUR_TXT_REDUCTION_TITREE), COLOR_FGD_REDUCTION_TITREE_DEFAULT);
		return this.reductionTitreeForegroundCache;
	}

	protected Color chooseColor(String overridenHexColorCode, Color defaultColor) {
		Color finalColor = defaultColor;
		if (overridenHexColorCode != null && overridenHexColorCode.trim().length() > 0) {
			finalColor = Color.decode(overridenHexColorCode);
		}
		return finalColor;
	}
}
