-- Mise à jour 1.3.2 de la base pour Pie et PieFwk
-- A EXECUTER A PARTIR DU USER GRHUM
--------------------------------------------------

grant select on maracuja.plan_comptable_exer to jefy_recette;

CREATE OR REPLACE FORCE VIEW "JEFY_RECETTE"."V_PLAN_COMPTABLE" ("PCO_BUDGETAIRE", "PCO_EMARGEMENT", "PCO_LIBELLE", "PCO_NATURE", "PCO_NIVEAU", "PCO_NUM", "PCO_SENS_EMARGEMENT", "PCO_VALIDITE", "PCO_J_EXERCICE", "PCO_J_FIN_EXERCICE", "PCO_J_BE","EXE_ORDRE") AS
SELECT
   PCO_BUDGETAIRE, PCO_EMARGEMENT, PCO_LIBELLE,
   PCO_NATURE, PCO_NIVEAU, PCO_NUM,
   PCO_SENS_EMARGEMENT, PCO_VALIDITE, PCO_J_EXERCICE,
   PCO_J_FIN_EXERCICE, PCO_J_BE,EXE_ORDRE
FROM MARACUJA.PLAN_COMPTABLE_EXER;


CREATE OR REPLACE FORCE VIEW "JEFY_RECETTE"."V_PLAN_COMPTABLE_CTP" ("PCO_NUM", "PCO_VALIDITE","EXE_ORDRE") AS
SELECT pco_num, pco_validite,exe_ordre
FROM maracuja.PLAN_COMPTABLE_EXER
WHERE (pco_num LIKE '139%' OR pco_num LIKE '28%' OR pco_num LIKE '29%' OR pco_num LIKE '3%' OR pco_num LIKE '4%' OR pco_num LIKE '59%');

insert into jefy_recette.db_version (db_version,db_date,db_comment) values ('1.3.2',to_date('03/09/2009','dd/mm/yyyy'),'Plan comptable par exercice');

commit;