PIE (Prestations Internes Externes)
===================================

1.Description générale
----------------------

Le module PIE de la sphère GFC du PGI Cocktail est un outil permettant la génération de prestations internes et externes, ainsi que leurs factures. L'application permet également de générer les recettes qui seront titrées à partir du module ABRICOT. Il permet également aux services d'un établissement de vendre des prestations de biens ou services à destination d'autres services appartenant à d'autres établissements. Pour cela l'application s'appuie sur la gestion des Catalogues/Articles/Options.

2.Principales fonctionnalités
-----------------------------

### Catalogues et Prestations :

- Définition d’articles, avec comprenant les notions d’options et de remises,
- Gestion de prestations internes et/ou externes sur des articles  en catalogue,
- Prestations internes liées à des commandes Carambole,
- Edition de devis.

### Factures et Recettes :

- Gestion et édition de factures issues des prestations ou non,
- Gestion d’échéanciers de facturation,
- Gestion et édition des recettes issues des factures ou non,
- Réductions de recettes,
- Visibilité sur les titres de recettes.

### Pilotage :

- Pilotage et suivi des échéanciers de facturation (Gestion des alertes).
