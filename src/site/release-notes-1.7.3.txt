Pie Release Note - 1.7.3.0
==========================
# Correctifs
- [DT 5203] Sur l'ecran Prestation, le type de crédit est automatiquement affecté à la prestation (comme sur les autres écrans).
L'ouverture de la fenêtre Convention prend de suite en compte le type de crédit affecté à la prestation en cours.

- [DT 5204] : Amélioration des méthodes de calcul des HT / TTC des prestations et factures avec modification de la prise en charge du taxu de TVA.
En prestation :
	-> le taux de TVA utilisé provient du catalogue dans tous les cas.
	-> le total HT de la prestation est calculé en réalisant la somme des Ht de chacune des lignes de prestation.
	-> le total TTC est calculé est réalisant la somme des TTC recalculés dynamiquement de chacune des prestations. Le TTC = HT x Quantité x Taux de TVA.
En facturation :
	-> nous utilisons prioritairement le taux de TVA sélectionné sur l'écran de saisie libre de l'article.
	Si celui-ci est laissé à vide alors Taux de TVA = TTC / HT arrondi à 3 décimales.
	-> si l'article est sélectionné depuis un catalogue alors nous utilisons le taux de TVA du catalogue.
	ATTENTION : Même en cas de modification sur l'écran de saisi de l'article du taux de TVA, le calcul prendra en compte le taux du catalogue.
	Cela peut prêter à confusion au niveau de l'affichage graphique. Ceci sera amélioré lors d'une prochaine version de Pie.
	-> le total HT et le total TTC sont calculés en utilisant la même méthode que celle décrite pour les prestations.

De nouvelles évolutions auront lieu sur cette gestion (méthodes de calcul et affichage graphique) lors de la mise en place de la gestion des multiples taux de TVA
(total par taux, affichage par taux, ...).

- [DT 5570] : Voir DT 5204
