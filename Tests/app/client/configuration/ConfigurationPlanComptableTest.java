package app.client.configuration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import org.cocktail.application.client.eof.EOExercice;
import org.junit.Test;
import org.mockito.Mock;

import app.client.ApplicationClient;
import app.test.core.CocktailBaseTestCase;

/**
 * Classe de test pour {@link ConfigurationPlanComptable}.
 *
 * @author flagouey
 */
public class ConfigurationPlanComptableTest extends CocktailBaseTestCase {

	/** Application client. */
	@Mock
	private ApplicationClient applicationClient;

	@Test
	public void testInstance() {
		assertNotNull(ConfigurationPlanComptable.instance());

		// TODO voir Mockito pour verifier l'initialisation du singleton.
	}

	@Test
	public void testGetClassesInterditesContrepartie() {
		List<String> classesInterdites = null;
		ConfigurationPlanComptable configPlancoSingleton = ConfigurationPlanComptable.instance();
		EOExercice exercice2011 = mock(EOExercice.class);
		EOExercice exercice2012 = mock(EOExercice.class);

		when(exercice2011.exeExercice()).thenReturn(Integer.valueOf(2011));
		when(exercice2012.exeExercice()).thenReturn(Integer.valueOf(2012));

		// TODO FLA : voir si l'utilisation de Matchers permet d'ameliorer ce fonctionnement
		when(applicationClient.getCurrentExercice()).thenReturn(exercice2011);
		when(applicationClient.getParam(
				ConfigurationPlanComptable.PARAM_PLAN_COMPTABLE_CTP_CLASSES_INTERDITES, exercice2011))
					.thenReturn(null, "", "1", "2,3", "4 ,  , 5 ", "6");

		// null : liste vide en retour.
		configPlancoSingleton.clearValues();
		classesInterdites = configPlancoSingleton.getClassesInterditesContrepartie(applicationClient);
		assertNotNull(classesInterdites);
		assertTrue(classesInterdites.isEmpty());

		// chaine vide : liste vide en retour
		configPlancoSingleton.clearValues();
		classesInterdites = configPlancoSingleton.getClassesInterditesContrepartie(applicationClient);
		assertNotNull(classesInterdites);
		assertTrue(classesInterdites.isEmpty());

		// "1" : liste avec 1.
		configPlancoSingleton.clearValues();
		classesInterdites = configPlancoSingleton.getClassesInterditesContrepartie(applicationClient);
		assertNotNull(classesInterdites);
		assertEquals(1, classesInterdites.size());
		assertEquals("1", classesInterdites.get(0));

		// "2,3" : liste de deux éléments (2, 3)
		configPlancoSingleton.clearValues();
		classesInterdites = configPlancoSingleton.getClassesInterditesContrepartie(applicationClient);
		assertNotNull(classesInterdites);
		assertEquals(2, classesInterdites.size());
		assertEquals("2", classesInterdites.get(0));
		assertEquals("3", classesInterdites.get(1));

		// "4 ,  , 5 " : liste de deux éléments (4, 5)
		configPlancoSingleton.clearValues();
		classesInterdites = configPlancoSingleton.getClassesInterditesContrepartie(applicationClient);
		assertNotNull(classesInterdites);
		assertEquals(classesInterdites.size(), 2);
		assertEquals("4", classesInterdites.get(0));
		assertEquals("5", classesInterdites.get(1));

		// Test le cache
		classesInterdites = configPlancoSingleton.getClassesInterditesContrepartie(applicationClient);
		assertNotNull(classesInterdites);
		assertEquals(2, classesInterdites.size());
		assertEquals("4", classesInterdites.get(0));
		assertEquals("5", classesInterdites.get(1));

		// Test simple avec un nouvel exercice.
		when(applicationClient.getCurrentExercice()).thenReturn(exercice2012);
		when(applicationClient.getParam(
				ConfigurationPlanComptable.PARAM_PLAN_COMPTABLE_CTP_CLASSES_INTERDITES, exercice2012))
					.thenReturn("8");
		configPlancoSingleton.clearValues();
		classesInterdites = configPlancoSingleton.getClassesInterditesContrepartie(applicationClient);
		assertNotNull(classesInterdites);
		assertEquals(1, classesInterdites.size());
		assertEquals("8", classesInterdites.get(0));
	}
}
