package app.client.configuration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.awt.Color;

import org.junit.Test;
import org.mockito.Mock;

import app.client.ApplicationClient;
import app.test.core.CocktailBaseTestCase;

/**
 * Classe de test pour {@link ConfigurationCouleur}.
 *
 * @author flagouey
 */
public class ConfigurationCouleurTest extends CocktailBaseTestCase {

	/** Color black au format Hex. */
	private static final String COLOR_BLACK_HEX = "#000000";

	/** Color black. */
	private static final Color COLOR_BLACK = Color.decode(COLOR_BLACK_HEX);

	/** Application Client. */
	@Mock
	private ApplicationClient appClient;

	@Test
	public void testInstance() {
		assertNotNull(ConfigurationCouleur.instance());

		// TODO voir Mockito pour verifier l'initialisation du singleton.
	}

	@Test
	public void testPrestationArchiveeForeground() {
		when(appClient.getParam(ConfigurationCouleur.PARAM_COULEUR_TXT_PRESTATION_ARCHIVEE))
			.thenReturn(null, "", COLOR_BLACK_HEX);

		ConfigurationCouleur configCouleurSingleton = ConfigurationCouleur.instance();
		// null : on utilise valeur par defaut
		configCouleurSingleton.clearValues();
		assertEquals(
				ConfigurationCouleur.COLOR_FGD_PRESTATION_ARCHIVE_DEFAULT,
				configCouleurSingleton.prestationArchiveeForeground(appClient));

		// vide : on utilise la valeur par defaut.
		configCouleurSingleton.clearValues();
		assertEquals(
				ConfigurationCouleur.COLOR_FGD_PRESTATION_ARCHIVE_DEFAULT,
				configCouleurSingleton.prestationArchiveeForeground(appClient));

		// valeur correcte : on l'utilise
		configCouleurSingleton.clearValues();
		assertEquals(
				COLOR_BLACK,
				configCouleurSingleton.prestationArchiveeForeground(appClient));
	}

	@Test
	public void testPrestationNonValideClientForeground() {
		when(appClient.getParam(ConfigurationCouleur.PARAM_COULEUR_TXT_PRESTATION_NON_VALIDE_CLIENT))
			.thenReturn(null, "", COLOR_BLACK_HEX);

		ConfigurationCouleur configCouleurSingleton = ConfigurationCouleur.instance();
		// null : on utilise valeur par defaut
		configCouleurSingleton.clearValues();
		assertEquals(
				ConfigurationCouleur.COLOR_FGD_PRESTATION_NON_VALIDE_CLIENT_DEFAULT,
				configCouleurSingleton.prestationNonValideClientForeground(appClient));

		// vide : on utilise la valeur par defaut.
		configCouleurSingleton.clearValues();
		assertEquals(
				ConfigurationCouleur.COLOR_FGD_PRESTATION_NON_VALIDE_CLIENT_DEFAULT,
				configCouleurSingleton.prestationNonValideClientForeground(appClient));

		// valeur correcte : on l'utilise
		configCouleurSingleton.clearValues();
		assertEquals(
				COLOR_BLACK,
				configCouleurSingleton.prestationNonValideClientForeground(appClient));
	}

	@Test
	public void testPrestationValideClientForeground() {
		when(appClient.getParam(ConfigurationCouleur.PARAM_COULEUR_TXT_PRESTATION_VALIDE_CLIENT))
			.thenReturn(null, "", COLOR_BLACK_HEX);

		ConfigurationCouleur configCouleurSingleton = ConfigurationCouleur.instance();
		// null : on utilise valeur par defaut
		configCouleurSingleton.clearValues();
		assertEquals(
				ConfigurationCouleur.COLOR_FGD_PRESTATION_VALIDE_CLIENT_DEFAULT,
				configCouleurSingleton.prestationValideClientForeground(appClient));

		// vide : on utilise la valeur par defaut.
		configCouleurSingleton.clearValues();
		assertEquals(
				ConfigurationCouleur.COLOR_FGD_PRESTATION_VALIDE_CLIENT_DEFAULT,
				configCouleurSingleton.prestationValideClientForeground(appClient));

		// valeur correcte : on l'utilise
		configCouleurSingleton.clearValues();
		assertEquals(
				COLOR_BLACK,
				configCouleurSingleton.prestationValideClientForeground(appClient));
	}

	@Test
	public void testPrestationValideForeground() {
		when(appClient.getParam(ConfigurationCouleur.PARAM_COULEUR_TXT_PRESTATION_VALIDE))
			.thenReturn(null, "", COLOR_BLACK_HEX);

		ConfigurationCouleur configCouleurSingleton = ConfigurationCouleur.instance();
		// null : on utilise valeur par defaut
		configCouleurSingleton.clearValues();
		assertEquals(
				ConfigurationCouleur.COLOR_FGD_PRESTATION_TOUT_VALIDE_DEFAULT,
				configCouleurSingleton.prestationValideForeground(appClient));

		// vide : on utilise la valeur par defaut.
		configCouleurSingleton.clearValues();
		assertEquals(
				ConfigurationCouleur.COLOR_FGD_PRESTATION_TOUT_VALIDE_DEFAULT,
				configCouleurSingleton.prestationValideForeground(appClient));

		// valeur correcte : on l'utilise
		configCouleurSingleton.clearValues();
		assertEquals(
				COLOR_BLACK,
				configCouleurSingleton.prestationValideForeground(appClient));
	}

	@Test
	public void testPrestationFactureeBackground() {
		when(appClient.getParam(ConfigurationCouleur.PARAM_COULEUR_BG_PRESTATION_FACTUREE))
			.thenReturn(null, "", COLOR_BLACK_HEX);

		ConfigurationCouleur configCouleurSingleton = ConfigurationCouleur.instance();
		// null : on utilise valeur par defaut
		configCouleurSingleton.clearValues();
		assertEquals(
				ConfigurationCouleur.COLOR_BGD_PRESTATION_FACTUREE_DEFAULT,
				configCouleurSingleton.prestationFactureeBackground(appClient));

		// vide : on utilise la valeur par defaut.
		configCouleurSingleton.clearValues();
		assertEquals(
				ConfigurationCouleur.COLOR_BGD_PRESTATION_FACTUREE_DEFAULT,
				configCouleurSingleton.prestationFactureeBackground(appClient));

		// valeur correcte : on l'utilise
		configCouleurSingleton.clearValues();
		assertEquals(
				COLOR_BLACK,
				configCouleurSingleton.prestationFactureeBackground(appClient));
	}

	@Test
	public void testFactureNonValideClientForeground() {
		when(appClient.getParam(ConfigurationCouleur.PARAM_COULEUR_TXT_FACTURE_NON_VALIDE_CLIENT))
			.thenReturn(null, "", COLOR_BLACK_HEX);

		ConfigurationCouleur configCouleurSingleton = ConfigurationCouleur.instance();
		// null : on utilise valeur par defaut
		configCouleurSingleton.clearValues();
		assertEquals(
				ConfigurationCouleur.COLOR_FGD_FACTURE_NON_VALIDE_CLIENT_DEFAULT,
				configCouleurSingleton.factureNonValideClientForeground(appClient));

		// vide : on utilise la valeur par defaut.
		configCouleurSingleton.clearValues();
		assertEquals(
				ConfigurationCouleur.COLOR_FGD_FACTURE_NON_VALIDE_CLIENT_DEFAULT,
				configCouleurSingleton.factureNonValideClientForeground(appClient));

		// valeur correcte : on l'utilise
		configCouleurSingleton.clearValues();
		assertEquals(
				COLOR_BLACK,
				configCouleurSingleton.factureNonValideClientForeground(appClient));
	}

	@Test()
	public void testFactureValideClientForeground() {
		when(appClient.getParam(ConfigurationCouleur.PARAM_COULEUR_TXT_FACTURE_VALIDE_CLIENT))
			.thenReturn(null, "", COLOR_BLACK_HEX);

		ConfigurationCouleur configCouleurSingleton = ConfigurationCouleur.instance();
		// null : on utilise valeur par defaut
		configCouleurSingleton.clearValues();
		assertEquals(
				ConfigurationCouleur.COLOR_FGD_FACTURE_VALIDE_CLIENT_DEFAULT,
				configCouleurSingleton.factureValideClientForeground(appClient));

		// vide : on utilise la valeur par defaut.
		configCouleurSingleton.clearValues();
		assertEquals(
				ConfigurationCouleur.COLOR_FGD_FACTURE_VALIDE_CLIENT_DEFAULT,
				configCouleurSingleton.factureValideClientForeground(appClient));

		// valeur correcte : on l'utilise
		configCouleurSingleton.clearValues();
		assertEquals(
				COLOR_BLACK,
				configCouleurSingleton.factureValideClientForeground(appClient));
	}

	@Test()
	public void testFactureValidePrestataireBackground() {
		when(appClient.getParam(ConfigurationCouleur.PARAM_COULEUR_BG_FACTURE_VALIDE_PREST))
			.thenReturn(null, "", COLOR_BLACK_HEX);

		ConfigurationCouleur configCouleurSingleton = ConfigurationCouleur.instance();
		// null : on utilise valeur par defaut
		configCouleurSingleton.clearValues();
		assertEquals(
				ConfigurationCouleur.COLOR_BGD_FACTURE_VALIDE_PREST_DEFAULT,
				configCouleurSingleton.factureValidePrestataireBackground(appClient));

		// vide : on utilise la valeur par defaut.
		configCouleurSingleton.clearValues();
		assertEquals(
				ConfigurationCouleur.COLOR_BGD_FACTURE_VALIDE_PREST_DEFAULT,
				configCouleurSingleton.factureValidePrestataireBackground(appClient));

		// valeur correcte : on l'utilise
		configCouleurSingleton.clearValues();
		assertEquals(
				COLOR_BLACK,
				configCouleurSingleton.factureValidePrestataireBackground(appClient));
	}

	@Test()
	public void testRecetteNonTitreeForeground() {
		when(appClient.getParam(ConfigurationCouleur.PARAM_COULEUR_TXT_RECETTE_NON_TITREE))
			.thenReturn(null, "", COLOR_BLACK_HEX);

		ConfigurationCouleur configCouleurSingleton = ConfigurationCouleur.instance();
		// null : on utilise valeur par defaut
		configCouleurSingleton.clearValues();
		assertEquals(
				ConfigurationCouleur.COLOR_FGD_RECETTE_NON_TITREE_DEFAULT,
				configCouleurSingleton.recetteNonTitreeForeground(appClient));

		// vide : on utilise la valeur par defaut.
		configCouleurSingleton.clearValues();
		assertEquals(
				ConfigurationCouleur.COLOR_FGD_RECETTE_NON_TITREE_DEFAULT,
				configCouleurSingleton.recetteNonTitreeForeground(appClient));

		// valeur correcte : on l'utilise
		configCouleurSingleton.clearValues();
		assertEquals(
				COLOR_BLACK,
				configCouleurSingleton.recetteNonTitreeForeground(appClient));
	}

	@Test
	public void testRecetteTitreeForeground() {
		when(appClient.getParam(ConfigurationCouleur.PARAM_COULEUR_TXT_RECETTE_TITREE))
			.thenReturn(null, "", COLOR_BLACK_HEX);

		ConfigurationCouleur configCouleurSingleton = ConfigurationCouleur.instance();
		// null : on utilise valeur par defaut
		configCouleurSingleton.clearValues();
		assertEquals(
				ConfigurationCouleur.COLOR_FGD_RECETTE_TITREE_DEFAULT,
				configCouleurSingleton.recetteTitreeForeground(appClient));

		// vide : on utilise la valeur par defaut.
		configCouleurSingleton.clearValues();
		assertEquals(
				ConfigurationCouleur.COLOR_FGD_RECETTE_TITREE_DEFAULT,
				configCouleurSingleton.recetteTitreeForeground(appClient));

		// valeur correcte : on l'utilise
		configCouleurSingleton.clearValues();
		assertEquals(
				COLOR_BLACK,
				configCouleurSingleton.recetteTitreeForeground(appClient));
	}

	@Test
	public void testReductionNonTitreeForeground() {
		when(appClient.getParam(ConfigurationCouleur.PARAM_COULEUR_TXT_REDUCTION_NON_TITREE))
			.thenReturn(null, "", COLOR_BLACK_HEX);

		ConfigurationCouleur configCouleurSingleton = ConfigurationCouleur.instance();
		// null : on utilise valeur par defaut
		configCouleurSingleton.clearValues();
		assertEquals(
				ConfigurationCouleur.COLOR_FGD_REDUCTION_NON_TITREE_DEFAULT,
				configCouleurSingleton.reductionNonTitreeForeground(appClient));

		// vide : on utilise la valeur par defaut.
		configCouleurSingleton.clearValues();
		assertEquals(
				ConfigurationCouleur.COLOR_FGD_REDUCTION_NON_TITREE_DEFAULT,
				configCouleurSingleton.reductionNonTitreeForeground(appClient));

		// valeur correcte : on l'utilise
		configCouleurSingleton.clearValues();
		assertEquals(
				COLOR_BLACK,
				configCouleurSingleton.reductionNonTitreeForeground(appClient));
	}

	@Test
	public void testReductionTitreeForeground() {
		when(appClient.getParam(ConfigurationCouleur.PARAM_COULEUR_TXT_REDUCTION_TITREE))
			.thenReturn(null, "", COLOR_BLACK_HEX);

		ConfigurationCouleur configCouleurSingleton = ConfigurationCouleur.instance();
		// null : on utilise valeur par defaut
		configCouleurSingleton.clearValues();
		assertEquals(
				ConfigurationCouleur.COLOR_FGD_REDUCTION_TITREE_DEFAULT,
				configCouleurSingleton.reductionTitreeForeground(appClient));

		// vide : on utilise la valeur par defaut.
		configCouleurSingleton.clearValues();
		assertEquals(
				ConfigurationCouleur.COLOR_FGD_REDUCTION_TITREE_DEFAULT,
				configCouleurSingleton.reductionTitreeForeground(appClient));

		// valeur correcte : on l'utilise
		configCouleurSingleton.clearValues();
		assertEquals(
				COLOR_BLACK,
				configCouleurSingleton.reductionTitreeForeground(appClient));
	}
}
