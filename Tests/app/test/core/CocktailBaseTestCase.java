package app.test.core;

import org.junit.Before;
import org.mockito.MockitoAnnotations;

/**
 * Classe de base pour les tests.
 *
 * @author flagouey
 *
 */
public class CocktailBaseTestCase {

	/** Init mockito mocks. */
	@Before
	public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

}
